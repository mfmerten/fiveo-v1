# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
module PrawnExtensions
   
   def col(number, pos = nil)
      return 0 if number.nil? || !number.is_a?(Integer)
      one_col_width = (bounds.width - 6).to_i
      two_col_width = ((bounds.width - 12) / 2).to_i
      three_col_width = ((bounds.width - 6) / 3).to_i
      four_col_width = ((two_col_width - 3) / 2).to_i
      case number
      when 1
         case pos
         when nil
            return one_col_width
         when 1
            return 3
         else
            return nil
         end
      when 2
         case pos
         when nil
            return two_col_width
         when 1
            return 3
         when 2
            return 3 + two_col_width + 6
         else
            return nil
         end
      when 3
         case pos
         when nil
            return three_col_width
         when 1
            return 3
         when 2
            return 3 + three_col_width
         when 3
            return 3 + three_col_width + three_col_width
         else
            return nil
         end
      when 4
         case pos
         when nil
            return four_col_width
         when 1
            return 3
         when 2
            return 3 + four_col_width + 4
         when 3
            return 3 + four_col_width + 4 + four_col_width + 6
         when 4
            return 3 + four_col_width + 4 + four_col_width + 6 + four_col_width + 4
         else
            return nil
         end
      else
         return nil
      end
   end
   
   def text_field(coords = [0,0], width = 100, label = 'Test', text = 'Test Results', border_color=nil, txt_color=nil, rows=nil)
      return 0 unless (rows.nil? || rows.to_i > 0) && width.is_a?(Integer)
      if border_color.nil?
         border_color = SiteConfig.pdf_header_bg
      end
      if txt_color.nil?
         txt_color = SiteConfig.pdf_header_txt
      end
      if rows.nil?
         rows = 1
      else
         rows = rows.to_i
      end
      
      text = text.to_s
      bounding_box(coords, :width => width, :height => 25 * rows) do
         # draw the bounding box from coords width wide and 20 high
         stroke_color border_color
         stroke_bounds
         stroke_color '000000'
         # Write the label (tiny) in the upper left corner
         fill_color txt_color
         draw_text label, :at => [bounds.left + 1, bounds.top - 6], :size => 8
         fill_color '000000'
         # Write the text in the box
         unless text.nil?
            bounding_box([bounds.left + 10, bounds.top - 8], :width => bounds.width - 10, :height => bounds.height - 8) do
               lines = text.split(/\n/).compact
               lines.each do |l|
                  if rows == 1 || (rows > 1 && lines.size > 1)
                     while width_of(l, :size => 14) > bounds.width - 12
                        l = l.chop
                     end
                  end
                  text l, :size => 14
               end
            end
         end
      end
      return 25 * rows
   end
end