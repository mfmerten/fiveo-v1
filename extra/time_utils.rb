# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
module TimeUtils
   
   def self.included(base)
      base.extend ClassMethods
   end
   
   module ClassMethods
      def datetime_diff(time1, time2=DateTime.now)
         return nil unless time1.is_a?(Time) || time1.is_a?(DateTime)
         return nil unless time2.is_a?(Time) || time2.is_a?(DateTime)
         adjusted_time1 = DateTime.parse("#{time1}")
         adjusted_time2 = DateTime.parse("#{time2}")
         if adjusted_time1 > adjusted_time2
            adjusted_time1 - adjusted_time2
         else
            adjusted_time2 - adjusted_time1
         end
      end
   
      def seconds_diff(time1, time2=DateTime.now)
         (datetime_diff(time1,time2) * 86400.0).round
      end
      
      def minutes_diff(time1, time2=DateTime.now)
         (datetime_diff(time1,time2) * 1440.0).round
      end
   
      def hours_diff(time1, time2=DateTime.now)
         (datetime_diff(time1,time2) * 24.0).round
      end
      
      def days_diff(time1, time2=DateTime.now)
         datetime_diff(time1,time2).round
      end
      
      def months_diff(time1, time2=DateTime.now)
         (datetime_diff(time1,time2) / 30.0).round
      end
      
      def years_diff(time1, time2=DateTime.now)
         (datetime_diff(time1,time2) / 365.0).round
      end
   
      def format_date(d, *opts)
         options = HashWithIndifferentAccess.new(:timestamp => false, :long => false)
         options.update(opts.pop) if opts.last.is_a?(Hash)
         t = opts.first
         return " " if d.nil?
         if d.is_a?(String)
            if d == 'now'
               d = DateTime.now
            elsif d == 'today'
               d = Date.today
            end
         end
         return " " unless d.is_a?(Date) || d.is_a?(DateTime) || d.is_a?(Time)
         if t.nil?
            date = d
         else
            # allow formatting date values with separate time string
            date = self.get_datetime(d,t)
         end
         if options[:timestamp] == true
            date.to_s(:timestamp)
         elsif options[:long] == true
            date.to_s(:us_long)
         else
            date.to_s(:us)
         end
      end
   
      # accepts a Date, a Date + string time value, DateTime or Time and returns
      # the corresponding DateTime value
      def get_datetime(date=nil,time=nil)
         zone = DateTime.now.zone
         date = DateTime.now if date.nil?
         return nil unless (date.is_a?(Date) && (time.nil? || (time.is_a?(String) && time =~ /\d\d:\d\d/))) || date.is_a?(DateTime) || date.is_a?(Time)
         if date.is_a?(DateTime) || date.is_a?(Time)
            DateTime.parse("#{date}")
         else
            time = "00:00" if time.nil?
            hrs, mins = time.split(/:/)
            hrs = time.slice(0,2).to_i
            DateTime.parse("#{date} #{hrs}:#{mins} #{zone}")
         end
      end
      
      # accepts a Time or DateTime value and returns a Date and a time string
      # if no argument, use current date and time
      def get_date_and_time(datetime=nil)
         datetime = DateTime.now if datetime.nil?
         return [nil,nil] unless datetime.is_a?(Time) || datetime.is_a?(DateTime)
         return [datetime.to_date, datetime.strftime("%H:%M")]
      end
      
      # accepts list of [start_time,stop_time] range arrays and returns total
      # number of hours served taking into consideration any overlap of ranges.
      def calculate_time_served(ranges, *opts)
         options = HashWithIndifferentAccess.new(:returns => 'hours',:skip_nils => false)
         options.update(opts.pop) if opts.last.is_a?(Hash)
         # return value defaults to hours
         unless ['hours','days','sentence'].include?(options[:returns])
            options[:returns] = 'hours'
         end
         total_hours = 0
         return 0 unless ranges.is_a?(Array)
         ranges = ranges.compact
         return 0 if ranges.empty?
         # remove double blank entries and set single blanks to now
         ranges.each do |r|
            # remove entry if both values blank
            if r[0].blank? && r[1].blank?
               ranges.delete(r)
               next
            end
            if options[:skip_nils] == true
               if r[0].blank? || r[1].blank?
                  ranges.delete(r)
                  next
               end
            else
               # if single values blank, replace with now
               if r[0].blank?
                  r[0] = DateTime.now
               end
               if r[1].blank?
                  r[1] = DateTime.now
               end
            end
         end
         # sort by start date/stop date and merge any overlapping entries
         # normalize each value to DateTime
         ranges = ranges.sort{|a,b| a[0].to_s(:db) + a[1].to_s(:db) <=> b[0].to_s(:db) + b[1].to_s(:db)}
         merged_ranges = []
         start_time = get_datetime(ranges[0][0])
         stop_time = get_datetime(ranges[0][1])
         if ranges.size > 1
            (1..ranges.size-1).each do |x|
               stime = get_datetime(ranges[x][0])
               etime = get_datetime(ranges[x][1])
               if stime >= start_time && stime <= stop_time && etime > stop_time
                  stop_time = etime
               elsif stime > stop_time
                  # start a new period
                  merged_ranges.push([start_time,stop_time])
                  start_time = stime
                  stop_time = etime
               else
                  # end time must also fall within previous range
                  # (subset instead of overlap) so skip
               end
            end
            unless !merged_ranges.empty? && start_time == merged_ranges.last[0]
               merged_ranges.push([start_time,stop_time])
            end
         else
            merged_ranges.push([start_time,stop_time])
         end
         # now have normalized set of non-overlapping time ranges,
         # add up the difference for each and return the requested value
         total_hours = 0
         merged_ranges.each do |r|
            total_hours += hours_diff(r[0],r[1])
         end
         if options[:returns] == 'sentence'
            return hours_to_sentence(total_hours)
         elsif options[:returns] == 'days'
            return (total_hours / 24.0).round
         else
            return total_hours
         end
      end
      
      # accepts total hours and returns [hours, days, months, years, negative?]
      # (sentence array).  Note that this calculation uses standard length
      # years and months rather than actual calendar lengths - this is
      # for sentencing purposes
      def hours_to_sentence(total_hours, *opts)
         options = HashWithIndifferentAccess.new(:returns => 'array')
         options.update(opts.pop) if opts.last.is_a?(Hash)
         unless ['array','string'].include?(options[:returns])
            options[:returns] = 'array'
         end
         return [0,0,0,0] if total_hours.to_i == 0
         negative = (total_hours.to_i < 0)
         total_hours = total_hours.to_i.abs
         years = 0
         months = 0
         days = 0
         hours = total_hours
         while hours >= 8760 do
            hours -= 8760
            years += 1
         end
         while hours >= 720 do
            hours -= 720
            months += 1
         end
         while hours >= 24 do
            hours -= 24
            days += 1
         end
         if options[:returns] == 'array'
            return [hours,days,months,years,negative]
         else
            return sentence_to_string(hours,days,months,years,negative)
         end
      end

      # accepts hours, days, months and years and returns the total number of days
      # calculation is based off of standardized 30 day months and is appropriate
      # for sentencing only.
      def sentence_to_hours(hours, *opts)
         options = HashWithIndifferentAccess.new(:returns => 'number')
         options.update(opts.pop) if opts.last.is_a?(Hash)
         unless ['number','string'].include?(options[:returns])
            options[:returns] = 'number'
         end
         days,months,years,negative = opts
         if hours.is_a?(Array)
            hours,days,months,years,negative = hours
         end
         hours = hours.to_i
         days = days.to_i
         months = months.to_i
         years = years.to_i
         total = 0
         total += hours
         total += (days * 24)
         total += (months * 30 * 24)
         total += (years * 365 * 24)
         if options[:returns] == 'number'
            if negative == true
               return -total
            else
               return total
            end
         else
            return hours_to_string(total)
         end
      end
      
      # accepts hours, days, months and years and returns a summary string or nil
      def sentence_to_string(hours, *opts)
         options = HashWithIndifferentAccess.new(:short => false, :include_zeros => false)
         options.update(opts.pop) if opts.last.is_a?(Hash)
         days, months, years, negative = opts
         if hours.is_a?(Array)
            hours,days,months,years,negative = hours
         end
         hours = hours.to_i
         days = days.to_i
         months = months.to_i
         years = years.to_i
         txt = ''
         if years > 0 || (years == 0 && options[:include_zeros] == true)
            txt << "#{years} Year#{years == 1 ? '' : 's'}"
         end
         if months > 0 || (months == 0 && options[:include_zeros] == true)
            unless txt.empty?
               txt << ", "
            end
            txt << "#{months} Month#{months == 1 ? '' : 's'}"
         end
         if days > 0 || (days == 0 && options[:include_zeros] == true)
            unless txt.empty?
               txt << ", "
            end
            txt << "#{days} Day#{days == 1 ? '' : 's'}"
         end
         if hours > 0 || (hours == 0 && options[:include_zeros] == true)
            unless txt.empty?
               txt << ", "
            end
            txt << "#{hours} Hour#{hours == 1 ? '' : 's'}"
         end
         if options[:short] == true
            txt = txt.sub(/ Hours?/,'H').sub(/ Days?/,'D').sub(/ Months?/, 'M').sub(/ Years?/,'Y')
         end
         if negative == true
            return "-#{txt}"
         end
         return txt
      end
      
      # accepts number of hours and returns days, switches to hours if number
      # of hours < a day
      def hours_to_string(hours)
         hours = hours.to_i
         days = hours / 24.0
         if days >= 1
            return "#{days.round} Day#{days.round == 1 ? '' : 's'}"
         else
            return "#{hours} Hour#{hours == 1 ? '' : 's'}"
         end
      end
      
      # accepts a string representation of date and returns a valid date
      # object if it can be parsed, or nil if not
      def valid_date(datestring, *opts)
         options = HashWithIndifferentAccess.new(:allow_nil => true)
         options.update(opts.pop) if opts.last.is_a?(Hash)
         if options[:allow_nil] == false
            blank_value = Date.today
         else
            blank_value = nil
         end
         return blank_value if datestring.blank?
         return datestring if datestring.is_a?(Date)
         return datestring.to_date if datestring.is_a?(Time) || datestring.is_a?(DateTime)
         return blank_value unless datestring.is_a?(String)
         if datestring =~ /^\d{1,2}-\d{1,2}-\d{4}$/
            datestring = datestring.gsub(/-/,'/')
         end
         begin
            return Date.parse(datestring)
         rescue
            return blank_value
         end
      end
      
      # accepts a string representation of date and time and returns a valid DateTime
      # object if it can be parsed, or nil if not
      def valid_datetime(datestring, *opts)
         options = HashWithIndifferentAccess.new(:allow_nil => true)
         options.update(opts.pop) if opts.last.is_a?(Hash)
         if options[:allow_nil] == false
            blank_value = DateTime.now
         else
            blank_value = nil
         end
         return blank_value if datestring.blank?
         return DateTime.parse("#{datestring} 00:00 #{DateTime.now.zone}") if datestring.is_a?(Date)
         return DateTime.parse("#{datestring}") if datestring.is_a?(Time) || datestring.is_a?(DateTime)
         return blank_value unless datestring.is_a?(String)
         if datestring =~ /^\d{1,2}-\d{1,2}-\d{4}.*$/
            datestring = datestring.gsub(/-/,'/')
         end
         begin
            temp = DateTime.parse(datestring)
         rescue
            return blank_value
         end
         # redo with correct time zone if necessary
         unless temp.zone == DateTime.now.zone
            temp = DateTime.parse("#{temp.to_s(:us)} #{DateTime.now.zone}")
         end
         return temp
      end
      
      # checks a supplied time value... if it is a 4 digit string, it separates
      # the first two digits from the last two by inserting a colon (:).
      def valid_time(timestring, *opts)
         options = HashWithIndifferentAccess.new(:allow_nil => true)
         options.update(opts.pop) if opts.last.is_a?(Hash)
         if options[:allow_nil] == false
            blank_value = "00:00"
         else
            blank_value = nil
         end
         return blank_value if timestring.blank?
         return blank_value unless timestring.is_a?(String)
         return blank_value unless timestring =~ /^\d{2}:?\d{2}:?\d{0,2}$/
         # strip seconds
         timestring = timestring.sub(/^(\d{2}:?\d{2}).*$/, '\1')
         hours = timestring.slice(0,2).to_i
         minutes = timestring.slice(-2,2).to_i
         if hours > 23
            hours = 23
         end
         if minutes > 59
            minutes = 59
         end
         return "#{hours.to_s.rjust(2,'0')}:#{minutes.to_s.rjust(2,'0')}"
      end
      
      # returns string representing time portion of DateTime.now
      def current_time
         DateTime.now.strftime("%H:%M")
      end
      
      # accept two datetimes and return the difference in days, hours and minutes
      # as a string
      def duration_in_words(from_datetime, to_datetime = DateTime.now)
         return 0 unless from_datetime.is_a?(Time) || from_datetime.is_a?(DateTime)
         minutes = minutes_diff(from_datetime, to_datetime)
         days = 0
         hours = 0
         while minutes >= 1440 do
            minutes -= 1440
            days += 1
         end
         while minutes >= 60 do
            minutes -= 60
            hours += 1
         end
         txt = ""
         if days > 0
            unless txt.empty?
               txt << ", "
            end
            txt << "#{days} Day#{days == 1 ? '' : 's'}"
         end
         if hours > 0
            unless txt.empty?
               txt << ", "
            end
            txt << "#{hours} Hour#{hours == 1 ? '' : 's'}"
         end
         if minutes > 0
            unless txt.empty?
               txt << ", "
            end
            txt << "#{minutes} Minute#{minutes == 1 ? '' : 's'}"
         end
         return txt
      end
   end
   
   def datetime_diff(time1, time2=DateTime.now)
      self.class.datetime_diff(time1, time2)
   end

   def seconds_diff(time1, time2=DateTime.now)
      self.class.seconds_diff(time1,time2)
   end
   
   def minutes_diff(time1, time2=DateTime.now)
      self.class.minutes_diff(time1, time2)
   end

   def hours_diff(time1, time2=DateTime.now)
      self.class.hours_diff(time1, time2)
   end
   
   def days_diff(time1, time2=DateTime.now)
      self.class.days_diff(time1,time2)
   end
   
   def months_diff(time1, time2=DateTime.now)
      self.class.months_diff(time1,time2)
   end
   
   def years_diff(time1, time2=DateTime.now)
      self.class.years_diff(time1,time2)
   end

   def format_date(d,*args)
      opts = args.pop if args.last.is_a?(Hash)
      t = args.first
      self.class.format_date(d,t,opts)
   end

   def get_datetime(date=nil,time=nil)
      self.class.get_datetime(date,time)
   end
   
   def calculate_time_served(ranges,*args)
      opts = args.pop if args.last.is_a?(Hash)
      self.class.calculate_time_served(ranges,opts)
   end
   
   def hours_to_sentence(total_hours, *args)
      opts = args.pop if args.last.is_a?(Hash)
      self.class.hours_to_sentence(total_hours,opts)
   end
   
   def sentence_to_hours(hours, *args)
      opts = args.pop if args.last.is_a?(Hash)
      days, months, years, negative = args
      self.class.sentence_to_hours(hours, days, months, years, negative, opts)
   end
   
   def sentence_to_string(hours, *args)
      opts = args.pop if args.last.is_a?(Hash)
      days, months, years, negative = args
      self.class.sentence_to_string(hours, days, months, years, negative, opts)
   end
   
   def hours_to_string(hours)
      self.class.hours_to_string(hours)
   end
   
   def get_date_and_time(datetime=nil)
      self.class.get_date_and_time(datetime)
   end
   
   def valid_date(datestring, *args)
      opts = args.pop if args.last.is_a?(Hash)
      self.class.valid_date(datestring, opts)
   end
   
   def valid_datetime(datestring, *args)
      opts = args.pop if args.last.is_a?(Hash)
      self.class.valid_datetime(datestring, opts)
   end
   
   def valid_time(timestring, *args)
      opts = args.pop if args.last.is_a?(Hash)
      self.class.valid_time(timestring, opts)
   end
   
   def current_time
      self.class.current_time
   end
   
   def duration_in_words(from_datetime, to_datetime = DateTime.now)
      self.class.duration_in_words(from_datetime, to_datetime)
   end
end