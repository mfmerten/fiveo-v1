# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
module FiveoModelUtilities

   include ActiveSupport::CoreExtensions::String::Inflections

   def self.included(base)
      base.extend ClassMethods
   end

   module ClassMethods
      # validates that a time entry consists of 2 digits, a colon and another
      # 2 digits, and that the hours and minutes values are in the proper range.
      def validates_time(*attr_names)
         configuration = {}
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "cannot be blank!")
               end
            else
               hours = value.slice(0,2).to_i
               mins = value.slice(-2.2).to_i
               unless value =~ /^\d{2}:\d{2}$/
                  record.errors.add(attr_name, "must be entered as HH:MM!")
               end
               unless hours >= 0 && hours < 24
                  record.errors.add(attr_name, "hour must be from 00 to 23!")
               end
               unless mins >= 0 && mins < 60
                  record.errors.add(attr_name, "minute must be from 00 to 59!")
               end
            end
         end
      end

      # validates that an option is valid
      def validates_option(*attr_names)
         configuration = {
            :option_type => nil
         }
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "is required!")
               end
            else
               if opt = Option.find_by_id(value)
                  unless configuration[:option_type].nil?
                     unless opt.type_name == configuration[:option_type]
                        record.errors.add(attr_name, "is not a valid #{configuration[:option_type]} option!")
                     end
                  end
               else
                  record.errors.add(attr_name, "is not a valid option value!")
               end
            end
         end
      end

      # validates that a person is valid
      def validates_person(*attr_names)
         configuration = {}
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "is required!")
               end
            else
               unless Person.find_by_id(value)
                  record.errors.add(attr_name, "is not a valid Person ID!")
               end
            end
         end
      end

      # validates that a booking is valid
      def validates_booking(*attr_names)
         configuration = {}
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "is required!")
               end
            else
               unless Booking.find_by_id(value)
                  record.errors.add(attr_name, "is not a valid Booking ID!")
               end
            end
         end
      end

      # validates that a arrest is valid
      def validates_arrest(*attr_names)
         configuration = {}
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "is required!")
               end
            else
               unless Arrest.find_by_id(value)
                  record.errors.add(attr_name, "is not a valid Arrest ID!")
               end
            end
         end
      end

      # validates that a call is valid
      def validates_call(*attr_names)
         configuration = {}
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "is required!")
               end
            else
               unless Call.find_by_id(value)
                  record.errors.add(attr_name, "is not a valid Call ID!")
               end
            end
         end
      end

      # validates that a probation is valid
      def validates_probation(*attr_names)
         configuration = {}
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "is required!")
               end
            else
               unless Probation.find_by_id(value)
                  record.errors.add(attr_name, "is not a valid Probation ID!")
               end
            end
         end
      end

      # validates that an offense is valid
      def validates_offense(*attr_names)
         configuration = {}
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "is required!")
               end
            else
               unless Offense.find_by_id(value)
                  record.errors.add(attr_name, "is not a valid Offense ID!")
               end
            end
         end
      end

      # validates that a warrant is valid
      def validates_warrant(*attr_names)
         configuration = {}
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "is required!")
               end
            else
               unless Warrant.find_by_id(value)
                  record.errors.add(attr_name, "is not a valid Warrant ID!")
               end
            end
         end
      end

      # validates that a docket is valid
      def validates_docket(*attr_names)
         configuration = {}
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "is required!")
               end
            else
               unless Docket.find_by_id(value)
                  record.errors.add(attr_name, "is not a valid Docket ID!")
               end
            end
         end
      end

      # validates that a contact is valie
      def validates_contact(*attr_names)
         configuration = {}
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "is required!")
               end
            else
               unless Contact.find_by_id(value)
                  record.errors.add(attr_name, "is not a valid Contact ID!")
               end
            end
         end
      end

      # validates that a user is valid
      def validates_user(*attr_names)
         configuration = {
            :allow_locked => false
         }
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_each(attr_names, configuration) do |record, attr_name, value|
            if value.nil? or value.blank?
               unless configuration[:allow_nil] || configuration[:allow_blank]
                  record.errors.add(attr_name, "is required!")
               end
            else
               if u = User.find_by_id(value)
                  unless configuration[:allow_locked]
                     if u.locked? || u.disabled_text?
                        record.errors.add(attr_name, "must not be a locked or disabled user account!")
                     end
                  end
               else
                  record.errors.add(attr_name, "is not a valid Person ID!")
               end
            end
         end
      end

      # validates that a url is in the proper format
      def validates_url(*attr_names)
         configuration = {
            :message => "must be valid URL format (http://site.host.domain/path/)",
            :with => /\Ahttps?:\/\/([-\w\.]+)+(:\d+)?(\/([\w\/_\.]*(\?\S+)?)?)?\Z/
         }
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_format_of attr_names, configuration
      end

      # validates that zip codes are in the proper 5 digit form
      def validates_zip(*attr_names)
         configuration = {
            :message => "must be ZIP or ZIP+4 format",
            :with => /\A[\d]{5}(-[\d]{4})?\Z/
         }
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_format_of attr_names, configuration
      end

      # validates that email addresses are of the proper form as specified
      # by RFC822
      def validates_email(*attr_names)
         configuration = {
            :message => "must be of the format name@host.domain",
            :with => RFC822::EmailAddress
         }
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_format_of attr_names, configuration
      end

      # validates that social security numbers are of the proper format.
      def validates_ssn(*attr_names)
         configuration = {
            :message => "must be of format 999-99-9999",
            :with => /\A[\d]{3}-[\d]{2}-[\d]{4}\Z/
         }
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_format_of attr_names, configuration 
      end

      # validates that phone numbers are of the proper format
      def validates_phone(*attr_names)
         configuration = {
            :with => /\A[\d]{3}-[\d]{3}-[\d]{4}\Z/,
            :message => "must be of format 999-999-9999"
         }
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_format_of attr_names, configuration
      end

      #--
      # had to comment these out for now.
      # cannot validate a datetime :before or :after if using US dates
      # this is a bug in the validate date time plugin.
      # def validates_datetime_in_past(*attr_names)
      #  configuration = {
      #        :before => 1.second.from_now,
      #        :before_message => "cannot be in the future!",
      #        :allow_nil => false }
      #    configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
      #    validates_date_time attr_names, configuration
      #end
      #++

      # validates that a date is in the past
      def validates_date_in_past(*attr_names)
         configuration = {
            :before => Date.tomorrow,
            :before_message => "cannot be in the future!"
         }
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_date attr_names, configuration
      end

      #--
      #def validates_datetime_in_future(*attr_names)
      #    configuration = {
      #        :after => DateTime.now,
      #        :after_message => "cannot be in the past!",
      #        :allow_nil => false }
      #    configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
      #    validates_date_time attr_names, configuration
      #end
      #++

      # validates that a date is in the future
      def validates_date_in_future(*attr_names)
         configuration = {
            :after => Date.yesterday,
            :after_message => "cannot be in the past!"
         }
         configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)
         validates_date attr_names, configuration
      end
   
      # returns string quoted for sql conditions (can handle strings, arrays
      # and hashes)
      def quoted(object)
         ob = object.clone
         if ob.is_a?(String)
            return nil if ob.strip.blank?
            return ob.gsub(/\\/,'\&\&').gsub(/'/,"''")
         elsif ob.is_a?(HashWithIndifferentAccess)
            tmp = HashWithIndifferentAccess.new
            ob.keys.each do |k|
               if ob[k].is_a?(String)
                  unless ob[k].blank?
                     tmp[k] = ob[k].gsub(/\\/,'\&\&').gsub(/'/,"''")
                  end
               else
                  tmp[k] = ob[k]
               end
            end
            return nil if tmp.empty?
            return tmp
         elsif ob.is_a?(Hash)
            tmp = Hash.new
            ob.keys.each do |k|
               if ob[k].is_a?(String)
                  unless ob[k].blank?
                     tmp[k] = ob[k].gsub(/\\/,'\&\&').gsub(/'/,"''")
                  end
               else
                  tmp[k] = ob[k]
               end
            end
            return nil if tmp.empty?
            return tmp
         elsif ob.is_a?(Array)
            tmp = Array.new
            ob.each do |o|
               if o.is_a?(String)
                  unless o.blank?
                     tmp.push(o.gsub(/\\/,'\&\&').gsub(/'/,"''"))
                  end
               else
                  tmp.push(o)
               end
            end
            return nil if tmp.empty?
            return tmp
         end
         return ob
      end
   
      # this takes a HashWithIndifferentAccess (as produced by the
      # parse_name method) and turns it back into a full name,
      # optionally in last-name-first order. Hash must include
      # :first and :last keys, and can optionally include :middle
      # and :suffix.  No other keys are recognized.
      #
      def unparse_name(nameparts, reverse=false)
         # must have first and last name parts or this won't work...
         # return what we have separated by spaces
         if nameparts[:last].blank? || nameparts[:first].blank?
            # not enough to unparse... return what we can
            if nameparts[:fullname].blank?
               return "#{nameparts[:first]} #{nameparts[:middle]} #{nameparts[:last]} #{nameparts[:suffix]}".gsub(/\s+/,' ')
            else
               return nameparts[:fullname]
            end
         end

         name = ""
         if reverse
            name << nameparts[:last].capitalize
            name.concat(", ")
            name.concat(nameparts[:first].capitalize)
            unless nameparts[:middle].blank?
               if nameparts.key? "middle"
                  name.concat(" ")
                  name.concat(nameparts[:middle].titleize)
               end
            end
         else
            name << nameparts[:first].capitalize
            unless nameparts[:middle].blank?
               if nameparts.key? "middle"
                  name.concat(" ")
                  name.concat(nameparts[:middle].titleize)
               end
            end
            name.concat(" ")
            name.concat(nameparts[:last].capitalize)
         end
         unless nameparts[:suffix].blank?
            if nameparts.key? "suffix"
               if reverse
                  name.concat(" ")
               else
                  name.concat(", ")
               end
               name.concat(nameparts[:suffix].upcase)
            end
         end
         # return the result stripped of all disallowed characters
         return name.gsub(/[^A-Za-z0-9 ,-]/,'')
      end

       # this attempts to take a full name in forward or reverse order
       # and split it into up to 4 pieces (first, middle, last, suffix)
       # Middle name may be multiple parts. Last name must be one word.
       #
       # modified to strip non-name characters and proper case the results
       def parse_name(fullname)
           # these are the only suffixes we will recognize
           suffix_list = ["sr","jr","ii","2nd","iii","3rd","iv","4th","v","5th"]

           # the hash to return
           nameparts = HashWithIndifferentAccess.new
           
           # return blank hash if name is not a string
           return nameparts unless fullname.is_a?(String)
           
           full_name = fullname.clone.gsub(/[^A-Za-z0-9 \.,-]/,'').gsub(/\./,' ').gsub(/\s+/,' ').strip
          
           # return blank hash if processed name is blank
           return nameparts if full_name.blank?

           # first, add fullname into hash as :fullname
           nameparts[:fullname] = full_name

           # check for lastname-first order (if it doesn't include a , it won't
           # work!!!
           myname = full_name.split
           if myname.length == 1
              # one word name (Cher)
              nameparts[:last] = myname[0].capitalize
              nameparts[:first] = ""
              nameparts[:middle] = ""
              nameparts[:suffix] = ""
              return nameparts
           end
           # reverse order if first word has a comma
           reverse = myname[0].include?(",")
           # now, remove commas from all words
           myname = myname.collect{|w| w.sub(/,/,'').capitalize}

           if reverse
               # first word will be last name
               nameparts[:last] = myname.shift
               nameparts[:first] = myname.shift
               # check to see if last part is a suffix
               if myname.empty?
                  nameparts[:suffix] = ""
               else
                  temp = myname[-1].downcase
                  if suffix_list.include?(temp)
                     nameparts[:suffix] = myname.pop.upcase
                  else
                     nameparts[:suffix] = ""
                  end
               end
               if myname.empty?
                  nameparts[:middle] = ""
               else
                  # all thats left is middle
                  nameparts[:middle] = myname.join(" ")
               end
           else
               # first word will be first name, rest are conditional
               nameparts[:first] = myname.shift
               if myname.empty?
                  nameparts[:suffix] = ""
               else
                  # check for the suffix
                  temp = myname[-1].downcase
                  if suffix_list.include?(temp)
                     nameparts[:suffix] = myname.pop.upcase
                  else
                     nameparts[:suffix] = ""
                  end
               end
               if myname.empty?
                  nameparts[:last] = ""
               else
                  # pop the last name
                  nameparts[:last] = myname.pop
               end
               if myname.empty?
                  nameparts[:middle] = ""
               else
                  # remainder is middle name(s)
                  nameparts[:middle] = myname.join(" ")
               end
            end
           return nameparts
       end
   
       # checks that the value is a valid non-negative string representation
       # of an integer (all CSV values are read as strings).
       def valid_integer(value,min=0,max=nil)
          # can't be nil
          return false if value.nil?
          # must contain only digits
          return false unless value =~ /^\d+$/
          # value can't be less than min
          if min.is_a?(Integer) && value.to_i < min
             return false
          end
          # value can't be more than max (if specified)
          if max.is_a?(Integer) && value.to_i > max
             return false
          end
          return true
       end

       # checks that the value is a valid non-blank string
       def valid_string(value)
          !value.blank? && value.is_a?(String)
       end

       # looks up option id by type and long or short name
       def get_option(type,name)
          if ot = OptionType.find_by_name(type)
             if o = Option.find(:first, :conditions => {:option_type_id => ot.id, :long_name => name})
                return o.id
             elsif o = Option.find(:first, :conditions => {:option_type_id => ot.id, :short_name => name})
                return o.id
             end
          end
          return nil
       end

       # strips all characters from case_no except for numbers, letters, and -
       def fix_case(case_no)
          return nil unless case_no.is_a?(String)
          case_no.gsub(/[^\dA-Za-z-]/, '')
       end
   end
   
   def parse_name(fullname)
      self.class.parse_name(fullname)
   end
   
   def unparse_name(nameparts, reverse=false)
      self.class.unparse_name(nameparts, reverse)
   end
   
   def quoted(object)
      self.class.quoted(object)
   end
   
   # strips all characters from case_no except for numbers, letters, and -
   def fix_case(case_no)
      self.class.fix_case(case_no)
   end
   
   # checks that the value is a valid non-negative string representation
   # of an integer (all CSV values are read as strings).
   def valid_integer(value,min=0,max=nil)
      self.class.valid_integer(value,min,max)
   end
   
   # checks that the value is a valid non-blank string
   def valid_string(value)
      self.class.valid_string(value)
   end
   
   # looks up option id by type and long or short name
   def get_option(type,name)
      self.class.get_option(type,name)
   end
end