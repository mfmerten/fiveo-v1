# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# This file is NOT UPDATED AUTOMATICALLY on deployments!
# It must be installed manully and requires root access!

# This file is currently only valid for the SPSO Server

# for apache access log
# skip 100 (info) - 200 (success) - 300 (redirect) codes... only want to know about client / server errors
[0-9.]{7,15} - - \[[ :/[:alnum:]-]+\] ".*" [123][0-9][0-9] .*$

# clean out misc stuff from system log
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ /USR/SBIN/CRON\[[0-9]+\]: \(root\) CMD \(  \[ -x /usr/lib/php5/maxlifetime \] .*$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ /USR/SBIN/CRON\[[0-9]+\]: \(root\) CMD \(   cd / && run-parts --report /etc/cron.*$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ /USR/SBIN/CRON\[[0-9]+\]: \(logcheck\) CMD \(   if \[ -x /usr/sbin/logcheck \]; then nice.*$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ /USR/SBIN/CRON\[[0-9]+\]: \(fiveoadm\) CMD \(cd /srv/rails/fiveo/releases.* fiveo:update_sentences\)$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ CRON\[[0-9]+\]: pam_unix\(cron:session\): session opened for user root by \(uid=0\)$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ CRON\[[0-9]+\]: pam_unix\(cron:session\): session closed for user root$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ CRON\[[0-9]+\]: pam_unix\(cron:session\): session opened for user logcheck by \(uid=0\)$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ CRON\[[0-9]+\]: pam_unix\(cron:session\): session closed for user logcheck$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ postfix/pickup\[[0-9]+\]: \w{11}: uid=[0-9]{1,6} from=<\w+>$
^\w{3} [ :0-9]{11} [._[:alnum:]-]+ postfix/cleanup\[[0-9]+\]: \w{11}: message-id=<[0-9]{14}\.\w+@\w+>$

# attempt to reduce production.log
^Completed in
^\[paperclip\] Saving attachments\.$
^\[paperclip\] Paperclip attachment .* initialized\.$
^\[paperclip\] Saving files for
^\[paperclip\] Deleting files for
^\[paperclip\] Writing files for
^[    ]+Session ID:
^[    ]+Parameters:
^Rendering
^Filter chain halted as .* rendered_or_redirected\.$
^User Activity: .* Logged In .*$
^User Activity: .* Logged Out .*$
^Processing
^Redirected to

# finally, ignore blank lines (even though it squishes up a lot)
^[    ]*$

