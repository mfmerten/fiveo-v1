# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
ActionController::Routing::Routes.draw do |map|
   # Jail 1
   map.edit_jail1 'jail1/edit', :controller => 'jail1s', :action => 'edit'
   map.jail1 'jail1/:id', :controller => 'jail1s', :action => 'show', :conditions => {:method => :get}
   map.connect 'jail1/:id', :controller => 'jail1s', :action => 'destroy', :conditions => {:method => :delete}
   map.jail1s 'jail1s', :controller => 'jail1s', :action => 'index', :conditions => {:method => :get}
   
   # Jail 2
   map.edit_jail2 'jail2/edit', :controller => 'jail2s', :action => 'edit'
   map.jail2 'jail2/:id', :controller => 'jail2s', :action => 'show', :conditions => {:method => :get}
   map.connect 'jail2/:id', :controller => 'jail2s', :action => 'destroy', :conditions => {:method => :delete}
   map.jail2s 'jail2s', :controller => 'jail2s', :action => 'index', :conditions => {:method => :get}
   
   # Jail 3
   map.edit_jail3 'jail3/edit', :controller => 'jail3s', :action => 'edit'
   map.jail3 'jail3/:id', :controller => 'jail3s', :action => 'show', :conditions => {:method => :get}
   map.connect 'jail3/:id', :controller => 'jail3s', :action => 'destroy', :conditions => {:method => :delete}
   map.jail3s 'jail3s', :controller => 'jail3s', :action => 'index', :conditions => {:method => :get}
   
   # message logs
   map.message_log 'message_log/:id', :controller => 'message_logs', :action => 'show', :conditions => {:method => :get}
   map.message_logs 'message_logs', :controller => 'message_logs', :action => 'index', :conditions => {:method => :get}
   
   # transfers
   map.transfer 'transfer/:id', :controller => 'transfers', :action => 'show', :conditions => {:method => :get}
   map.transfer 'transfer/:id', :controller => 'transfers', :action => 'destroy', :conditions => {:method => :delete}
   map.transfers 'transfers', :controller => 'transfers', :action => 'index', :conditions => {:method => :get}
   
   # investigations
   map.edit_invest_crime 'invest_crime/edit', :controller => 'investigations', :action => 'edit_crime'
   map.invest_crime 'invest_crime/:id', :controller => 'investigations', :action => 'show_crime', :conditions => {:method => :get}
   map.connect 'invest_crime/:id', :controller => 'investigations', :action => 'destroy_crime', :conditions => {:method => :delete}
   map.edit_invest_report 'invest_report/edit', :controller => 'investigations', :action => 'edit_report'
   map.invest_report 'invest_report/:id', :controller => 'investigations', :action => 'show_report', :conditions => {:method => :get}
   map.connect 'invest_report/:id', :controller => 'investigations', :action => 'destroy_report', :conditions => {:method => :delete}
   map.edit_invest_criminal 'invest_criminal/edit', :controller => 'investigations', :action => 'edit_criminal'
   map.invest_criminal 'invest_criminal/:id', :controller => 'investigations', :action => 'show_criminal', :conditions => {:method => :get}
   map.connect 'invest_criminal/:id', :controller => 'investigations', :action => 'destroy_criminal', :conditions => {:method => :delete}
   map.edit_invest_vehicle 'invest_vehicle/edit', :controller => 'investigations', :action => 'edit_vehicle'
   map.invest_vehicle 'invest_vehicle/:id', :controller => 'investigations', :action => 'show_vehicle', :conditions => {:method => :get}
   map.connect 'invest_vehicle/:id', :controller => 'investigations', :action => 'destroy_vehicle', :conditions => {:method => :delete}
   map.investigation_photo 'invest_photo/investigation_photo', :controller => 'investigations', :action => 'investigation_photo'
   map.edit_invest_photo 'invest_photo/edit', :controller => 'investigations', :action => 'edit_photo'
   map.invest_photo 'invest_photo/:id', :controller => 'investigations', :action => 'show_photo', :conditions => {:method => :get}
   map.connect 'invest_photo/:id', :controller => 'investigations', :action => 'destroy_photo', :conditions => {:method => :delete}
   map.edit_invest_case 'invest_case/edit', :controller => 'investigations', :action => 'edit_case'
   map.invest_case 'invest_case/:id', :controller => 'investigations', :action => 'show_case', :conditions => {:method => :get}
   map.connect 'invest_case/:id', :controller => 'investigations', :action => 'destroy_case', :conditions => {:method => :delete}
   map.edit_investigation 'investigation/edit', :controller => 'investigations', :action => 'edit'
   map.investigation 'investigation/:id', :controller => 'investigations', :action => 'show', :conditions => {:method => :get}
   map.connect 'investigation/:id', :controller => 'investigations', :action => 'destroy', :conditions => {:method => :delete}
   map.investigations 'investigations', :controller => 'investigations', :action => 'index', :conditions => {:method => :get}
   
   # history
   map.history 'history/:id', :controller => 'histories', :action => 'show', :conditions => {:method => :get}
   map.connect 'history/:id', :controller => 'histories', :action => 'destroy', :conditions => {:method => :delete}
   map.histories 'histories', :controller => 'histories', :action => 'index', :conditions => {:method => :get}

   # citations
   map.edit_citation 'citation/edit', :controller => 'citations', :action => 'edit'
   map.citation 'citation/:id', :controller => 'citations', :action => 'show', :conditions => {:method => :get}
   map.connect 'citation/:id', :controller => 'citations', :action => 'destroy', :conditions => {:method => :delete}
   map.citations 'citations', :controller => 'citations', :action => 'index', :conditions => {:method => :get}
   
   # dockets
   map.edit_revocation 'revocation/edit', :controller => 'dockets', :action => 'edit_revocation'
   map.revocation 'revocation/:id', :controller => 'dockets', :action => 'show_revocation', :conditions => {:method => :get}
   map.connect 'revocation/:id', :controller => 'dockets', :action => 'destroy_revocation', :conditions => {:method => :delete}
   map.edit_sentence 'sentence/edit', :controller => 'dockets', :action => 'edit_sentence'
   map.sentence 'sentence/:id', :controller => 'dockets', :action => 'show_sentence', :conditions => {:method => :get}
   map.connect 'sentence/:id', :controller => 'dockets', :action => 'destroy_sentence', :conditions => {:method => :delete}
   map.docket_worksheet 'docket/worksheets', :controller => 'dockets', :action => 'worksheets'
   map.edit_court 'court/edit', :controller => 'dockets', :action => 'edit_court'
   map.court 'court/:id', :controller => 'dockets', :action => 'show_court', :conditions => {:method => :get}
   map.connect 'court/:id', :controller => 'dockets', :action => 'destroy_court', :conditions => {:method => :delete}
   map.edit_docket 'docket/edit', :controller => 'dockets', :action => 'edit'
   map.docket 'docket/:id', :controller => 'dockets', :action => 'show', :conditions => {:method => :get}
   map.connect 'docket/:id', :controller => 'dockets', :action => 'destroy', :conditions => {:method => :delete}
   map.dockets 'dockets', :controller => 'dockets', :action => 'index', :conditions => {:method => :get}
   
   # stolen items
   map.edit_stolen 'stolen/edit', :controller => 'stolens', :action => 'edit'
   map.stolen 'stolen/:id', :controller => 'stolens', :action => 'show', :conditions => {:method => :get}
   map.connect 'stolen/:id', :controller => 'stolens', :action => 'destroy', :conditions => {:method => :delete}
   map.stolens 'stolens', :controller => 'stolens', :action => 'index', :conditions => {:method => :get}

   # paper service
   map.paper_payment 'paper_payment/:id', :controller => 'papers', :action => 'show_payment', :conditions => {:method => :get}
   map.connect 'paper_payment/:id', :controller => 'papers', :action => 'destroy_payment', :conditions => {:method => :delete}
   map.edit_paper_type 'paper_type/edit', :controller => 'papers', :action => 'edit_paper_type'
   map.paper_type 'paper_type/:id', :controller => 'papers', :action => 'show_paper_type', :conditions => {:method => :get}
   map.connect 'paper_type/:id', :controller => 'papers', :action => 'destroy_paper_type', :conditions => {:method => :delete}
   map.paper_types 'paper_types', :controller => 'papers', :action => 'index_paper_type', :conditions => {:method => :get}
   map.edit_paper_customer 'paper_customer/edit', :controller => 'papers', :action => 'edit_customer'
   map.paper_customer 'paper_customer/:id', :controller => 'papers', :action => 'show_customer', :conditions => {:method => :get}
   map.connect 'paper_customer/:id', :controller => 'papers', :action => 'destroy_customer', :conditions => {:method => :delete}
   map.paper_customers 'paper_customers', :controller => 'papers', :action => 'index_customers', :conditions => {:method => :get}
   map.edit_paper 'paper/edit', :controller => 'papers', :action => 'edit'
   map.paper 'paper/:id', :controller => 'papers', :action => 'show', :conditions => {:method => :get}
   map.connect 'paper/:id', :controller => 'papers', :action => 'destroy', :conditions => {:method => :delete}
   map.papers 'papers', :controller => 'papers', :action => 'index', :conditions => {:method => :get}

   # fax cover sheets
   map.edit_fax_cover 'fax_cover/edit', :controller => 'fax_covers', :action => 'edit'
   map.fax_cover 'fax_cover/:id', :controller => 'fax_covers', :action => 'show', :conditions => {:method => :get}
   map.connect 'fax_cover/:id', :controller => 'fax_covers', :action => 'destroy', :conditions => {:method => :delete}
   map.fax_covers 'fax_covers', :controller => 'fax_covers', :action => 'index', :conditions => {:method => :get}

   # commissary functions/commissary items
   map.fund_balance 'commissary/fund_balance', :controller => 'commissary', :action => 'fund_balance'
   map.posting_report 'commissary/posting_report', :controller => 'commissary', :action => 'posting_report'
   map.sales_sheet 'commissary/sales_sheet', :controller => 'commissary', :action => 'sales_sheet'
   map.transaction_report 'commissary/transaction_report', :controller => 'commissary', :action => 'transaction_report'
   map.edit_commissary 'commissary/edit', :controller => 'commissary', :action => 'commissary'
   map.commissary 'commissary/:id', :controller => 'commissary', :action => 'show_commissary', :conditions => {:method => :get}
   map.connect 'commissary/:id', :controller => 'commissary', :action => 'destroy_commissary', :conditions => {:method => :delete}
   map.edit_commissary_item 'commissary_item/edit', :controller => 'commissary', :action => 'edit'
   map.commissary_item 'commissary_item/:id', :controller => 'commissary', :action => 'show', :conditions => {:method => :get}
   map.connect 'commissary_item/:id', :controller => 'commissary', :action => 'destroy', :conditions => {:method => :delete}
   map.commissaries 'commissaries', :controller => 'commissary', :action => 'index', :conditions => {:method => :get}
   
   # probations
   map.edit_probation_payment 'probation/payment/edit', :controller => 'probations', :action => 'edit_payment' 
   map.probation_payment 'probation/payment/:id', :controller => 'probations', :action => 'show_payment', :conditions => {:method => :get}
   map.connect 'probation/payment/:id', :controller => 'probations', :action => 'destroy_payment', :conditions => {:method => :delete}
   map.edit_probation 'probation/edit_payment', :controller => 'probations', :action => 'edit'
   map.probation 'probation/:id', :controller => 'probations', :action => 'show', :conditions => {:method => :get}
   map.connect 'probation/:id', :controller => 'probations', :action => 'destroy', :conditions => {:method => :delete}
   map.probations 'probations', :controller => 'probations', :action => 'index', :conditions => {:method => :get}

   # activities
   map.activities 'activities', :controller => 'activities', :action => 'index', :conditions => {:method => :get}
   
   # help pages
   map.display_help 'help/display', :controller => 'helps', :action => 'popup'
   map.edit_help 'help/edit', :controller => 'helps', :action => 'edit'
   map.help 'help/:id', :controller => 'helps', :action => 'show', :conditions => {:method => :get}
   map.connect 'help/:id', :controller => 'helps', :action => 'destroy', :conditions => {:method => :delete}
   map.helps 'helps', :controller => 'helps', :action => 'index', :conditions => {:method => :get}

   # bug reports
   map.comment_bug 'bug/comment', :controller => 'bugs', :action => 'comment'
   map.watch_bug 'bug/watch', :controller => 'bugs', :action => 'watch'
   map.edit_bug 'bug/edit', :controller => 'bugs', :action => 'edit'
   map.bug 'bug/:id', :controller => 'bugs', :action => 'show', :conditions => {:method => :get}
   map.connect 'bug/:id', :controller => 'bugs', :action => 'destroy', :conditions => {:method => :delete}
   map.bugs 'bugs', :controller => 'bugs', :action => 'index', :conditions => {:method => :get}

   # absents
   map.absent_leave 'absent/leave', :controller => 'absents', :action => 'leave'
   map.absent_return 'absent/return', :controller => 'absents', :action => 'come_back'
   map.copy_absent 'absent/copy', :controller => 'absents', :action => 'copy'
   map.edit_absent 'absent/edit', :controller => 'absents', :action => 'edit'
   map.absent 'absent/:id', :controller => 'absents', :action => 'show', :conditions => {:method => :get}
   map.connect 'absent/:id', :controller => 'absents', :action => 'destroy', :conditions => {:method => :delete}
   map.absents 'absents', :controller => 'absents', :action => 'index', :conditions => {:method => :get}

   # offense photos
   map.crime_scene_photo 'offense_photo/crime_scene_photo', :controller => 'offense_photos', :action => 'crime_scene_photo'
   map.edit_offense_photo 'offense_photo/edit', :controller => 'offense_photos', :action => 'edit'
   map.offense_photo 'offense_photo/:id', :controller => 'offense_photos', :action => 'show', :conditions => {:method => :get}
   map.connect 'offense_photo/:id', :controller => 'offense_photos', :action => 'destroy', :conditions => {:method => :delete}
   map.offense_photos 'offense_photos', :controller => 'offense_photos', :action => 'index', :conditions => {:method => :get}
   
   # holds
   map.clear_hold 'hold/clear', :controller => 'holds', :action => 'clear'
   map.edit_hold 'hold/edit', :controller => 'holds', :action => 'edit'
   map.hold 'hold/:id', :controller => 'holds', :action => 'show', :conditions => {:method => :get}
   map.connect 'hold/:id', :controller => 'holds', :action => 'destroy', :conditions => {:method => :delete}
   map.holds 'holds', :controller => 'holds', :action => 'index', :conditions => {:method => :get}
   
   # transports
   map.transport_voucher 'transport/voucher', :controller => 'transports', :action => 'voucher'
   map.edit_transport 'transport/edit', :controller => 'transports', :action => 'edit'
   map.transport 'transport/:id', :controller => 'transports', :action => 'show', :conditions => {:method => :get}
   map.connect 'transport/:id', :controller => 'transports', :action => 'destroy', :conditions => {:method => :delete}
   map.transports 'transports', :controller => 'transports', :action => 'index', :conditions => {:method => :get}

   # bonds
   map.edit_bond 'bond/edit', :controller => 'bonds', :action => 'edit'
   map.bond 'bond/:id', :controller => 'bonds', :action => 'show', :conditions => {:method => :get}
   map.connect 'bond/:id', :controller => 'bonds', :action => 'destroy', :conditions => {:method => :delete}
   map.bonds 'bonds', :controller => 'bonds', :action => 'index', :conditions => {:method => :get}

   # bondsmen
   map.edit_bondsman 'bondsman/edit', :controller => 'bondsmen', :action => 'edit'
   map.bondsman 'bondsman/:id', :controller => 'bondsmen', :action => 'show', :conditions => {:method => :get}
   map.connect 'bondsman/:id', :controller => 'bondsmen', :action => 'destroy', :conditions => {:method => :delete}
   map.bondsmen 'bondsmen', :controller => 'bondsmen', :action => 'index', :conditions => {:method => :get}

   # arrests
   map.seventytwo 'arrest/seventytwo', :controller => 'arrests', :action => 'seventytwo'
   map.id_card 'arrest/id_card', :controller => 'arrests', :action => 'id_card'
   map.edit_arrest 'arrest/edit', :controller => 'arrests', :action => 'edit'
   map.arrest 'arrest/:id', :controller => 'arrests', :action => 'show', :conditions => {:method => :get}
   map.connect 'arrest/:id', :controller => 'arrests', :action => 'destroy', :conditions => {:method => :delete}
   map.arrests 'arrests', :controller => 'arrests', :action => 'index', :conditions => {:method => :get}

   # bookings
   map.edit_doc 'doc/edit', :controller => 'bookings', :action => 'edit_doc'
   map.doc 'doc/:id', :controller => 'bookings', :action => 'show_doc', :conditions => {:method => :get}
   map.connect 'doc/:id', :controller => 'bookings', :action => 'destroy_doc', :conditions => {:method => :delete}
   map.docs 'docs', :controller => 'bookings', :action => 'index_doc', :condition => {:method => :get}
   map.toggle_goodtime 'booking/toggle_goodtime', :controller => 'bookings', :action => 'toggle_goodtime'
   map.connect 'booking/lookup', :controller => 'bookings', :action => 'lookup'
   map.connect 'booking/validate_id', :controller => 'bookings', :action => 'validate_id'
   map.booking_reopen 'booking/clear_release', :controller => 'bookings', :action => 'clear_release'
   map.booking_release 'booking/release', :controller => 'bookings', :action => 'release'
   map.visitor_booking 'booking/visitor', :controller => 'bookings', :action => 'visitor_in'
   map.edit_booking 'booking/edit', :controller => 'bookings', :action => 'edit'
   map.booking 'booking/:id', :controller => 'bookings', :action => 'show', :conditions => {:method => :get}
   map.connect 'booking/:id', :controller => 'bookings', :action => 'destroy', :conditions => {:method => :delete}
   map.bookings 'bookings', :controller => 'bookings', :action => 'index', :conditions => {:method => :get}

   # forbids
   map.edit_forbid 'forbid/edit', :controller => 'forbids', :action => 'edit'
   map.forbid 'forbid/:id', :controller => 'forbids', :action => 'show', :conditions => {:method => :get}
   map.connect 'forbid/:id', :controller => 'forbids', :action => 'destroy', :conditions => {:method => :delete}
   map.forbids 'forbids', :controller => 'forbids', :action => 'index', :conditions => {:method => :get}

   # offenses
   map.edit_offense 'offense/edit', :controller => 'offenses', :action => 'edit'
   map.offense 'offense/:id', :controller => 'offenses', :action => 'show', :conditions => {:method => :get}
   map.connect 'offense/:id', :controller => 'offenses', :action => 'destroy', :conditions => {:method => :delete}
   map.offenses 'offenses', :controller => 'offenses', :action => 'index', :conditions => {:method => :get}

   # evidences
   map.photo_evidence 'evidence/photo', :controller => 'evidences', :action => 'photo'
   map.evidence_location 'evidence/location', :controller => 'evidences', :action => 'location'
   map.edit_evidence 'evidence/edit', :controller => 'evidences', :action => 'edit'
   map.evidence 'evidence/:id', :controller => 'evidences', :action => 'show', :conditions => {:method => :get}
   map.connect 'evidence/:id', :controller => 'evidences', :action => 'destroy', :conditions => {:method => :delete}
   map.evidences 'evidences', :controller => 'evidences', :action => 'index', :conditions => {:method => :get}

   # calendar events
   map.edit_event 'event/edit', :controller => 'events', :action => 'edit'
   map.purge_events 'events/purge', :controller => 'events', :action => 'purge', :conditions => {:method => :delete}
   map.connect 'event/:id', :controller => 'events', :action => 'destroy', :conditions => {:method => :delete}
   map.event 'event/:id', :controller => 'events', :action => 'show', :conditions => {:method => :get}
   map.events 'events', :controller => 'events', :action => 'index', :conditions => {:method => :get}

   # announcements
   map.edit_announcement 'announcement/edit', :controller => 'announcements', :action => 'edit'
   map.connect 'announcement/:id', :controller => 'announcements', :action => 'destroy', :conditions => {:method => :delete}
   map.announcement 'announcement/:id', :controller => 'announcements', :action => 'show', :conditions => {:method => :get}
   map.announcements 'announcements', :controller => 'announcements', :action => 'index', :conditions => {:method => :get}

   # case notes
   map.edit_case_note 'case_note/edit', :controller => 'case_notes', :action => 'edit'
   map.connect 'case_note/:id', :controller => 'case_notes', :action => 'destroy', :conditions => {:method => :delete}
   map.case_note 'case_note/:id', :controller => 'case_notes', :action => 'show', :conditions => {:method => :get}
   map.case_notes 'case_notes', :controller => 'case_notes', :action => 'index'
   
   # Zip codes controller
   map.edit_zip 'zip/edit', :controller => 'zips', :action => 'edit'
   map.zip 'zip/:id', :controller => 'zips', :action => 'show', :conditions => {:method => :get}
   map.connect 'zip/:id', :controller => 'zips', :action => 'destroy', :conditions => {:method => :delete}
   map.zips 'zips', :controller => 'zips', :action => 'index', :conditions => {:method => :get}

   # warrants controller
   map.edit_warrant 'warrants/edit', :controller => 'warrants', :action => 'edit'
   map.warrant 'warrant/:id', :controller => 'warrants', :action => 'show', :conditions => {:method => :get}
   map.connect 'warrant/:id', :controller => 'warrants', :action => 'destroy', :conditions => {:method => :delete}
   map.warrants 'warrants', :controller => 'warrants', :action => 'index', :conditions => {:method => :get}

   # calls controller
   map.signals 'calls/signals', :controller => 'calls', :action => 'signals'
   map.connect 'calls/validate_signal', :controller => 'calls', :action => 'validate_signal'
   map.edit_call 'calls/edit', :controller => 'calls', :action => 'edit'
   map.call 'call/:id', :controller => 'calls', :action => 'show', :conditions => {:method => :get}
   map.connect 'call/:id', :controller => 'calls', :action => 'destroy', :conditions => {:method => :delete}
   map.calls 'calls', :controller => 'calls', :action => 'index', :conditions => {:method => :get}

   # pawns controller
   map.edit_pawn 'pawn/edit', :controller => 'pawns', :action => 'edit'
   map.pawn 'pawn/:id', :controller => 'pawns', :action => 'show', :conditions => {:method => :get}
   map.connect 'pawn/:id', :controller => 'pawns', :action => 'destroy', :conditions => {:method => :delete}
   map.pawns 'pawns', :controller => 'pawns', :action => 'index', :conditions => {:method => :get}

   # statutes controller
   map.edit_statute 'statute/edit', :controller => 'statutes', :action => 'edit'
   map.statute 'statute/:id', :controller => 'statutes', :action => 'show', :conditions => {:method => :get}
   map.connect 'statute/:id', :controller => 'statutes', :action => 'destroy', :conditions => {:method => :delete}
   map.statutes 'statutes', :controller => 'statutes', :action => 'index', :conditions => {:method => :get}

   # contacts controller
   map.connect 'contact/validate', :controller => 'contacts', :action => 'validate'
   map.edit_contact 'contact/edit', :controller => 'contacts', :action => 'edit'
   map.contact 'contact/:id', :controller => 'contacts', :action => 'show', :conditions => {:method => :get}
   map.connect 'contact/:id', :controller => 'contacts', :action => 'destroy', :conditions => {:method => :delete}
   map.contacts 'contacts', :controller => 'contacts', :action => 'index', :conditions => {:method => :get}

   # people controller
   map.edit_medication 'medication/edit', :controller => 'people', :action => 'edit_med'
   map.medication 'medication/:id', :controller => 'people', :action => 'show_med', :conditions => {:method => :get}
   map.connect 'medication/:id', :controller => 'people', :action => 'destroy_med', :conditions => {:method => :delete}
   map.medications 'medications', :controller => 'people', :action => 'index_meds', :conditions => {:method => :get}
   map.front_mugshot 'person/front_mugshot', :controller => 'people', :action => 'front_mugshot'
   map.side_mugshot 'person/side_mugshot', :controller => 'people', :action => 'side_mugshot'
   map.connect 'person/validate_id', :controller => 'people', :action => 'validate_id'
   map.connect 'person/validate', :controller => 'people', :action => 'validate'
   map.edit_person 'person/edit', :controller => 'people', :action => 'edit'
   map.person 'person/:id', :controller => 'people', :action => 'show', :conditions => {:method => :get}
   map.connect 'person/:id', :controller => 'people', :action => 'destroy', :conditions => {:method => :delete}
   map.people 'people', :controller => 'people', :action => 'index', :conditions => {:method => :get}

   # database controller
   map.update_docs 'database/update_api_docs', :controller => 'database', :action => 'update_api_docs', :conditions => {:method => :post}
   map.clear_receipts 'database/clear_receipts', :controller => 'database', :action => 'clear_receipts', :conditions => {:method => :post}
   map.clear_cases 'database/clear_cases', :controller => 'database', :action => 'clear_cases', :conditions => {:method => :post}
   map.fix_zips 'database/fix_zip', :controller => 'database', :action => 'fix_zip', :conditions => {:method => :post}
   map.clear_sessions 'database/clear_sessions', :controller => 'database', :action => 'clear_sessions', :conditions => {:method => :post}
   map.databases 'databases', :controller => 'database', :action => 'index', :conditions => {:method => :get}

   # user routes
   map.photo_user 'users/photo', :controller => 'users', :action => 'photo'
   map.login 'users/login', :controller => 'users', :action => 'login'
   map.logout 'users/logout', :controller => 'users', :action => 'logout'
   map.connect 'users/change_password', :controller => 'users', :action => 'change_password'
   map.edit_user_settings 'user/settings/edit', :controller => 'users', :action => 'edit_settings'
      
   # user messages routes
   map.print_message 'users/:user_id/messages/:id/print', :controller => 'messages', :action => 'print_message'
   map.print_digest 'users/:user_id/messages/print_digest', :controller => 'messages', :action => 'print_digest'
   map.destroy_selected 'users/:user_id/messages/destroy_selected', :controller => 'messages', :action => 'destroy_selected'
   map.select_messages 'users/:user_id/messages/select_messages', :controller => 'messages', :action => 'select_messages', :conditions => {:method => :post}
   map.inbox_user_messages 'users/:user_id/messages/inbox', :controller => 'messages', :action => 'inbox', :conditions => {:method => :get}
   map.outbox_user_messages 'users/:user_id/messages/outbox', :controller => 'messages', :action => 'outbox', :conditions => {:method => :get}
   map.trashbin_user_messages 'users/:user_id/messages/trashbin', :controller => 'messages', :action => 'trashbin', :conditions => {:method => :get}
   map.user_messages 'users/:user_id/messages', :controller => 'messages', :action => 'index', :conditions => {:method => :get}
   map.connect 'users/:user_id/messages', :controller => 'messages', :action => 'create', :conditions => {:method => :post}
   map.new_user_message 'users/:user_id/messages/new', :controller => 'messages', :action => 'new', :conditions => {:method => :get}
   map.edit_user_message 'users/:user_id/messages/:id/edit', :controller => 'messages', :action => 'edit', :conditions => {:method => :get}
   map.reply_user_message 'users/:user_id/messages/:id/reply', :controller => 'messages', :action => 'reply', :conditions => {:method => :get}
   map.user_message 'users/:user_id/messages/:id', :controller => 'messages', :action => 'show', :conditions => {:method => :get}
   map.connect 'users/:user_id/messages/:id', :controller => 'messages', :action => 'update', :conditions => {:method => :put}
   map.connect 'users/:user_id/messages/:id', :controller => 'messages', :action => 'destroy', :conditions => {:method => :delete}

   # user admin routes
   map.toggle_user 'user/:id/toggle', :controller => 'users', :action => 'toggle', :conditions => {:method => :post}
   map.reset_user 'user/:id/reset', :controller => 'users', :action => 'reset', :conditions => {:method => :post}
   map.edit_user 'user/edit', :controller => 'users', :action => 'edit'
   map.user 'user/:id', :controller => 'users', :action => 'show', :conditions => {:method => :get}
   map.connect 'user/:id', :controller => 'users', :action => 'destroy', :conditions => {:method => :delete}
   map.users 'users', :controller => 'users', :action => 'index', :conditions => {:method => :get}
   
   # group routes
   map.edit_group 'group/edit', :controller => 'groups', :action => 'edit'
   map.group 'group/:id', :controller => 'groups', :action => 'show', :conditions => {:method => :get}
   map.connect 'group/:id', :controller => 'groups', :action => 'destroy', :conditions => {:method => :delete}
   map.groups 'groups', :controller => 'groups', :action => 'index', :conditions => {:method => :get}
   
   # role routes
   map.edit_role 'role/edit', :controller => 'roles', :action => 'edit'
   map.role 'role/:id', :controller => 'roles', :action => 'show', :conditions => {:method => :get}
   map.connect 'role/:id', :controller => 'roles', :action => 'destroy', :conditions => {:method => :delete}
   map.roles 'roles', :controller => 'roles', :action => 'index', :conditions => {:method => :get}
   
   # perm routes
   map.perms 'perms', :controller => 'perms', :action => 'index', :conditions => {:method => :get}

   # options routes
   map.toggle_option 'option/:id/toggle', :controller => 'options', :action => 'toggle', :conditions => {:method => :post}
   map.edit_option 'option/edit', :controller => 'options', :action => 'edit'
   map.option 'option/:id', :controller => 'options', :action => 'show', :conditions => {:method => :get}
   map.connect 'option/:id', :controller => 'options', :action => 'destroy', :conditions => {:method => :delete}
   map.options 'options', :controller => 'options', :action => 'index', :conditions => {:method => :get}

   # root routes
   map.case "show_case", :controller => 'root', :action => 'show_case', :conditions => {:method => :get}
   map.site_css 'site.css', :controller => 'root', :action => 'site', :conditions => {:method => :get}
   map.connect ":action", :controller => "root"
   map.root :controller => "root"

   # Install the default routes as the lowest priority.
   map.connect ':controller/:action/:id'
   map.connect ':controller/:action/:id.:format'
end
#== Route Map
# Generated on 09 May 2010 00:22
#
#               transfer GET    /transfer/:id                             {:action=>"show", :controller=>"transfers"}
#              transfers GET    /transfers                                {:action=>"index", :controller=>"transfers"}
#      edit_invest_crime        /invest_crime/edit                        {:action=>"edit_crime", :controller=>"investigations"}
#           invest_crime GET    /invest_crime/:id                         {:action=>"show_crime", :controller=>"investigations"}
#                        DELETE /invest_crime/:id                         {:action=>"destroy_crime", :controller=>"investigations"}
#     edit_invest_report        /invest_report/edit                       {:action=>"edit_report", :controller=>"investigations"}
#          invest_report GET    /invest_report/:id                        {:action=>"show_report", :controller=>"investigations"}
#                        DELETE /invest_report/:id                        {:action=>"destroy_report", :controller=>"investigations"}
#   edit_invest_criminal        /invest_criminal/edit                     {:action=>"edit_criminal", :controller=>"investigations"}
#        invest_criminal GET    /invest_criminal/:id                      {:action=>"show_criminal", :controller=>"investigations"}
#                        DELETE /invest_criminal/:id                      {:action=>"destroy_criminal", :controller=>"investigations"}
#    edit_invest_vehicle        /invest_vehicle/edit                      {:action=>"edit_vehicle", :controller=>"investigations"}
#         invest_vehicle GET    /invest_vehicle/:id                       {:action=>"show_vehicle", :controller=>"investigations"}
#                        DELETE /invest_vehicle/:id                       {:action=>"destroy_vehicle", :controller=>"investigations"}
#    investigation_photo        /invest_photo/investigation_photo         {:action=>"investigation_photo", :controller=>"investigations"}
#      edit_invest_photo        /invest_photo/edit                        {:action=>"edit_photo", :controller=>"investigations"}
#           invest_photo GET    /invest_photo/:id                         {:action=>"show_photo", :controller=>"investigations"}
#                        DELETE /invest_photo/:id                         {:action=>"destroy_photo", :controller=>"investigations"}
#       edit_invest_case        /invest_case/edit                         {:action=>"edit_case", :controller=>"investigations"}
#            invest_case GET    /invest_case/:id                          {:action=>"show_case", :controller=>"investigations"}
#                        DELETE /invest_case/:id                          {:action=>"destroy_case", :controller=>"investigations"}
#     edit_investigation        /investigation/edit                       {:action=>"edit", :controller=>"investigations"}
#          investigation GET    /investigation/:id                        {:action=>"show", :controller=>"investigations"}
#                        DELETE /investigation/:id                        {:action=>"destroy", :controller=>"investigations"}
#         investigations GET    /investigations                           {:action=>"index", :controller=>"investigations"}
#                history GET    /history/:id                              {:action=>"show", :controller=>"histories"}
#                        DELETE /history/:id                              {:action=>"destroy", :controller=>"histories"}
#              histories GET    /histories                                {:action=>"index", :controller=>"histories"}
#                  error GET    /error/:id                                {:action=>"error", :controller=>"error_codes"}
#             error_code GET    /error_code/:id                           {:action=>"show", :controller=>"error_codes"}
#            error_codes GET    /error_codes                              {:action=>"index", :controller=>"error_codes"}
#          edit_citation        /citation/edit                            {:action=>"edit", :controller=>"citations"}
#               citation GET    /citation/:id                             {:action=>"show", :controller=>"citations"}
#                        DELETE /citation/:id                             {:action=>"destroy", :controller=>"citations"}
#              citations GET    /citations                                {:action=>"index", :controller=>"citations"}
#        edit_revocation        /revocation/edit                          {:action=>"edit_revocation", :controller=>"dockets"}
#             revocation GET    /revocation/:id                           {:action=>"show_revocation", :controller=>"dockets"}
#                        DELETE /revocation/:id                           {:action=>"destroy_revocation", :controller=>"dockets"}
#          edit_sentence        /sentence/edit                            {:action=>"edit_sentence", :controller=>"dockets"}
#               sentence GET    /sentence/:id                             {:action=>"show_sentence", :controller=>"dockets"}
#                        DELETE /sentence/:id                             {:action=>"destroy_sentence", :controller=>"dockets"}
#       docket_worksheet        /docket/worksheets                        {:action=>"worksheets", :controller=>"dockets"}
#             edit_court        /court/edit                               {:action=>"edit_court", :controller=>"dockets"}
#                  court GET    /court/:id                                {:action=>"show_court", :controller=>"dockets"}
#                        DELETE /court/:id                                {:action=>"destroy_court", :controller=>"dockets"}
#            edit_docket        /docket/edit                              {:action=>"edit", :controller=>"dockets"}
#                 docket GET    /docket/:id                               {:action=>"show", :controller=>"dockets"}
#                        DELETE /docket/:id                               {:action=>"destroy", :controller=>"dockets"}
#                dockets GET    /dockets                                  {:action=>"index", :controller=>"dockets"}
#            edit_stolen        /stolen/edit                              {:action=>"edit", :controller=>"stolens"}
#                 stolen GET    /stolen/:id                               {:action=>"show", :controller=>"stolens"}
#                        DELETE /stolen/:id                               {:action=>"destroy", :controller=>"stolens"}
#                stolens GET    /stolens                                  {:action=>"index", :controller=>"stolens"}
#    edit_civil_customer        /civil_customer/edit                      {:action=>"edit_customer", :controller=>"suits"}
#         civil_customer GET    /civil_customer/:id                       {:action=>"show_customer", :controller=>"suits"}
#                        DELETE /civil_customer/:id                       {:action=>"destroy_customer", :controller=>"suits"}
#        civil_customers GET    /civil_customers                          {:action=>"index_customers", :controller=>"suits"}
#     edit_civil_invoice        /civil_invoice/edit                       {:action=>"edit_invoice", :controller=>"suits"}
#          civil_invoice GET    /civil_invoice/:id                        {:action=>"show_invoice", :controller=>"suits"}
#                        DELETE /civil_invoice/:id                        {:action=>"destroy_invoice", :controller=>"suits"}
#         civil_invoices GET    /civil_invoices                           {:action=>"index_invoice", :controller=>"suits"}
#             edit_paper        /paper/edit                               {:action=>"edit_paper", :controller=>"suits"}
#                  paper GET    /paper/:id                                {:action=>"show_paper", :controller=>"suits"}
#                        DELETE /paper/:id                                {:action=>"destroy_paper", :controller=>"suits"}
#              edit_suit        /suit/edit                                {:action=>"edit", :controller=>"suits"}
#                   suit GET    /suit/:id                                 {:action=>"show", :controller=>"suits"}
#                        DELETE /suit/:id                                 {:action=>"destroy", :controller=>"suits"}
#                  suits GET    /suits                                    {:action=>"index", :controller=>"suits"}
#         edit_fax_cover        /fax_cover/edit                           {:action=>"edit", :controller=>"fax_covers"}
#              fax_cover GET    /fax_cover/:id                            {:action=>"show", :controller=>"fax_covers"}
#                        DELETE /fax_cover/:id                            {:action=>"destroy", :controller=>"fax_covers"}
#             fax_covers GET    /fax_covers                               {:action=>"index", :controller=>"fax_covers"}
#                               /commissary/commissary                    {:action=>"commissary", :controller=>"commissary"}
#           fund_balance        /commissary/fund_balance                  {:action=>"fund_balance", :controller=>"commissary"}
#         posting_report        /commissary/posting_report                {:action=>"posting_report", :controller=>"commissary"}
#            sales_sheet        /commissary/sales_sheet                   {:action=>"sales_sheet", :controller=>"commissary"}
#        summary_balance        /commissary/summary_balance               {:action=>"summary_balance", :controller=>"commissary"}
#       detailed_balance        /commissary/detailed_balance              {:action=>"detailed_balance", :controller=>"commissary"}
#             commissary GET    /commissary                               {:action=>"index", :controller=>"commissary"}
#   edit_commissary_item        /commissary/edit                          {:action=>"edit", :controller=>"commissary"}
#        commissary_item GET    /commissary/:id                           {:action=>"show", :controller=>"commissary"}
#                        DELETE /commissary/:id                           {:action=>"destroy", :controller=>"commissary"}
#      payment_probation        /probation/payment                        {:action=>"payment", :controller=>"probations"}
#         edit_probation        /probation/edit                           {:action=>"edit", :controller=>"probations"}
#              probation GET    /probation/:id                            {:action=>"show", :controller=>"probations"}
#                        DELETE /probation/:id                            {:action=>"destroy", :controller=>"probations"}
#             probations GET    /probations                               {:action=>"index", :controller=>"probations"}
#             activities GET    /activities                               {:action=>"index", :controller=>"activities"}
#           display_help        /help/display                             {:action=>"popup", :controller=>"helps"}
#              edit_help        /help/edit                                {:action=>"edit", :controller=>"helps"}
#                   help GET    /help/:id                                 {:action=>"show", :controller=>"helps"}
#                        DELETE /help/:id                                 {:action=>"destroy", :controller=>"helps"}
#                  helps GET    /helps                                    {:action=>"index", :controller=>"helps"}
#            comment_bug        /bug/comment                              {:action=>"comment", :controller=>"bugs"}
#              watch_bug        /bug/watch                                {:action=>"watch", :controller=>"bugs"}
#               edit_bug        /bug/edit                                 {:action=>"edit", :controller=>"bugs"}
#                    bug GET    /bug/:id                                  {:action=>"show", :controller=>"bugs"}
#                        DELETE /bug/:id                                  {:action=>"destroy", :controller=>"bugs"}
#                   bugs GET    /bugs                                     {:action=>"index", :controller=>"bugs"}
#           absent_leave        /absent/leave                             {:action=>"leave", :controller=>"absents"}
#          absent_return        /absent/return                            {:action=>"come_back", :controller=>"absents"}
#            copy_absent        /absent/copy                              {:action=>"copy", :controller=>"absents"}
#            edit_absent        /absent/edit                              {:action=>"edit", :controller=>"absents"}
#                 absent GET    /absent/:id                               {:action=>"show", :controller=>"absents"}
#                        DELETE /absent/:id                               {:action=>"destroy", :controller=>"absents"}
#                absents GET    /absents                                  {:action=>"index", :controller=>"absents"}
#      crime_scene_photo        /offense_photo/crime_scene_photo          {:action=>"crime_scene_photo", :controller=>"offense_photos"}
#     edit_offense_photo        /offense_photo/edit                       {:action=>"edit", :controller=>"offense_photos"}
#          offense_photo GET    /offense_photo/:id                        {:action=>"show", :controller=>"offense_photos"}
#                        DELETE /offense_photo/:id                        {:action=>"destroy", :controller=>"offense_photos"}
#         offense_photos GET    /offense_photos                           {:action=>"index", :controller=>"offense_photos"}
#              edit_hold        /hold/edit                                {:action=>"edit", :controller=>"holds"}
#                   hold GET    /hold/:id                                 {:action=>"show", :controller=>"holds"}
#                        DELETE /hold/:id                                 {:action=>"destroy", :controller=>"holds"}
#                  holds GET    /holds                                    {:action=>"index", :controller=>"holds"}
#      transport_voucher        /transport/voucher                        {:action=>"voucher", :controller=>"transports"}
#         edit_transport        /transport/edit                           {:action=>"edit", :controller=>"transports"}
#              transport GET    /transport/:id                            {:action=>"show", :controller=>"transports"}
#                        DELETE /transport/:id                            {:action=>"destroy", :controller=>"transports"}
#             transports GET    /transports                               {:action=>"index", :controller=>"transports"}
#              edit_bond        /bond/edit                                {:action=>"edit", :controller=>"bonds"}
#                   bond GET    /bond/:id                                 {:action=>"show", :controller=>"bonds"}
#                        DELETE /bond/:id                                 {:action=>"destroy", :controller=>"bonds"}
#                  bonds GET    /bonds                                    {:action=>"index", :controller=>"bonds"}
#          edit_bondsman        /bondsman/edit                            {:action=>"edit", :controller=>"bondsmen"}
#               bondsman GET    /bondsman/:id                             {:action=>"show", :controller=>"bondsmen"}
#                        DELETE /bondsman/:id                             {:action=>"destroy", :controller=>"bondsmen"}
#               bondsmen GET    /bondsmen                                 {:action=>"index", :controller=>"bondsmen"}
#             seventytwo        /arrest/seventytwo                        {:action=>"seventytwo", :controller=>"arrests"}
#                id_card        /arrest/id_card                           {:action=>"id_card", :controller=>"arrests"}
#            edit_arrest        /arrest/edit                              {:action=>"edit", :controller=>"arrests"}
#                 arrest GET    /arrest/:id                               {:action=>"show", :controller=>"arrests"}
#                        DELETE /arrest/:id                               {:action=>"destroy", :controller=>"arrests"}
#                arrests GET    /arrests                                  {:action=>"index", :controller=>"arrests"}
#               edit_doc        /doc/edit                                 {:action=>"edit_doc", :controller=>"bookings"}
#                    doc GET    /doc/:id                                  {:action=>"show_doc", :controller=>"bookings"}
#                        DELETE /doc/:id                                  {:action=>"destroy_doc", :controller=>"bookings"}
#                   docs        /docs                                     {:action=>"index_doc", :controller=>"bookings", :condition=>{:method=>:get}}
#        toggle_goodtime        /booking/toggle_goodtime                  {:action=>"toggle_goodtime", :controller=>"bookings"}
#                               /booking/lookup                           {:action=>"lookup", :controller=>"bookings"}
#                               /booking/validate_id                      {:action=>"validate_id", :controller=>"bookings"}
#         booking_reopen        /booking/clear_release                    {:action=>"clear_release", :controller=>"bookings"}
#        booking_release        /booking/release                          {:action=>"release", :controller=>"bookings"}
#        visitor_booking        /booking/visitor                          {:action=>"visitor_in", :controller=>"bookings"}
#           edit_booking        /booking/edit                             {:action=>"edit", :controller=>"bookings"}
#                booking GET    /booking/:id                              {:action=>"show", :controller=>"bookings"}
#                        DELETE /booking/:id                              {:action=>"destroy", :controller=>"bookings"}
#               bookings GET    /bookings                                 {:action=>"index", :controller=>"bookings"}
#            edit_forbid        /forbid/edit                              {:action=>"edit", :controller=>"forbids"}
#                 forbid GET    /forbid/:id                               {:action=>"show", :controller=>"forbids"}
#                        DELETE /forbid/:id                               {:action=>"destroy", :controller=>"forbids"}
#                forbids GET    /forbids                                  {:action=>"index", :controller=>"forbids"}
#           edit_offense        /offense/edit                             {:action=>"edit", :controller=>"offenses"}
#                offense GET    /offense/:id                              {:action=>"show", :controller=>"offenses"}
#                        DELETE /offense/:id                              {:action=>"destroy", :controller=>"offenses"}
#               offenses GET    /offenses                                 {:action=>"index", :controller=>"offenses"}
#         photo_evidence        /evidence/photo                           {:action=>"photo", :controller=>"evidences"}
#      evidence_location        /evidence/location                        {:action=>"location", :controller=>"evidences"}
#          edit_evidence        /evidence/edit                            {:action=>"edit", :controller=>"evidences"}
#               evidence GET    /evidence/:id                             {:action=>"show", :controller=>"evidences"}
#                        DELETE /evidence/:id                             {:action=>"destroy", :controller=>"evidences"}
#              evidences GET    /evidences                                {:action=>"index", :controller=>"evidences"}
#             edit_event        /event/edit                               {:action=>"edit", :controller=>"events"}
#           purge_events DELETE /events/purge                             {:action=>"purge", :controller=>"events"}
#                        DELETE /event/:id                                {:action=>"destroy", :controller=>"events"}
#                  event GET    /event/:id                                {:action=>"show", :controller=>"events"}
#                 events GET    /events                                   {:action=>"index", :controller=>"events"}
#      edit_announcement        /announcement/edit                        {:action=>"edit", :controller=>"announcements"}
#    purge_announcements DELETE /announcements/purge                      {:action=>"purge", :controller=>"announcements"}
#                        DELETE /announcement/:id                         {:action=>"destroy", :controller=>"announcements"}
#           announcement GET    /announcement/:id                         {:action=>"show", :controller=>"announcements"}
#          announcements GET    /announcements                            {:action=>"index", :controller=>"announcements"}
#         edit_case_note        /case_note/edit                           {:action=>"edit", :controller=>"case_notes"}
#                        DELETE /case_note/:id                            {:action=>"destroy", :controller=>"case_notes"}
#              case_note GET    /case_note/:id                            {:action=>"show", :controller=>"case_notes"}
#             case_notes        /case_notes                               {:action=>"index", :controller=>"case_notes"}
#               edit_zip        /zip/edit                                 {:action=>"edit", :controller=>"zips"}
#                    zip GET    /zip/:id                                  {:action=>"show", :controller=>"zips"}
#                        DELETE /zip/:id                                  {:action=>"destroy", :controller=>"zips"}
#                   zips GET    /zips                                     {:action=>"index", :controller=>"zips"}
#           edit_warrant        /warrants/edit                            {:action=>"edit", :controller=>"warrants"}
#                warrant GET    /warrant/:id                              {:action=>"show", :controller=>"warrants"}
#                        DELETE /warrant/:id                              {:action=>"destroy", :controller=>"warrants"}
#               warrants GET    /warrants                                 {:action=>"index", :controller=>"warrants"}
#                signals        /calls/signals                            {:action=>"signals", :controller=>"calls"}
#                               /calls/validate_signal                    {:action=>"validate_signal", :controller=>"calls"}
#              edit_call        /calls/edit                               {:action=>"edit", :controller=>"calls"}
#                   call GET    /call/:id                                 {:action=>"show", :controller=>"calls"}
#                        DELETE /call/:id                                 {:action=>"destroy", :controller=>"calls"}
#                  calls GET    /calls                                    {:action=>"index", :controller=>"calls"}
#              edit_pawn        /pawn/edit                                {:action=>"edit", :controller=>"pawns"}
#                   pawn GET    /pawn/:id                                 {:action=>"show", :controller=>"pawns"}
#                        DELETE /pawn/:id                                 {:action=>"destroy", :controller=>"pawns"}
#                  pawns GET    /pawns                                    {:action=>"index", :controller=>"pawns"}
#           edit_statute        /statute/edit                             {:action=>"edit", :controller=>"statutes"}
#                statute GET    /statute/:id                              {:action=>"show", :controller=>"statutes"}
#                        DELETE /statute/:id                              {:action=>"destroy", :controller=>"statutes"}
#               statutes GET    /statutes                                 {:action=>"index", :controller=>"statutes"}
#                               /contact/validate                         {:action=>"validate", :controller=>"contacts"}
#           edit_contact        /contact/edit                             {:action=>"edit", :controller=>"contacts"}
#                contact GET    /contact/:id                              {:action=>"show", :controller=>"contacts"}
#                        DELETE /contact/:id                              {:action=>"destroy", :controller=>"contacts"}
#               contacts GET    /contacts                                 {:action=>"index", :controller=>"contacts"}
#        edit_medication        /medication/edit                          {:action=>"edit_med", :controller=>"people"}
#             medication GET    /medication/:id                           {:action=>"show_med", :controller=>"people"}
#                        DELETE /medication/:id                           {:action=>"destroy_med", :controller=>"people"}
#            medications GET    /medications                              {:action=>"index_meds", :controller=>"people"}
#          front_mugshot        /person/front_mugshot                     {:action=>"front_mugshot", :controller=>"people"}
#           side_mugshot        /person/side_mugshot                      {:action=>"side_mugshot", :controller=>"people"}
#                               /person/validate_id                       {:action=>"validate_id", :controller=>"people"}
#                               /person/validate                          {:action=>"validate", :controller=>"people"}
#            edit_person        /person/edit                              {:action=>"edit", :controller=>"people"}
#                 person GET    /person/:id                               {:action=>"show", :controller=>"people"}
#                        DELETE /person/:id                               {:action=>"destroy", :controller=>"people"}
#                 people GET    /people                                   {:action=>"index", :controller=>"people"}
#            update_docs POST   /database/update_api_docs                 {:action=>"update_api_docs", :controller=>"database"}
#         clear_receipts POST   /database/clear_receipts                  {:action=>"clear_receipts", :controller=>"database"}
#            clear_cases POST   /database/clear_cases                     {:action=>"clear_cases", :controller=>"database"}
#               fix_zips POST   /database/fix_zip                         {:action=>"fix_zip", :controller=>"database"}
#         clear_sessions POST   /database/clear_sessions                  {:action=>"clear_sessions", :controller=>"database"}
#              databases GET    /databases                                {:action=>"index", :controller=>"database"}
#             photo_user        /users/photo                              {:action=>"photo", :controller=>"users"}
#                  login        /users/login                              {:action=>"login", :controller=>"users"}
#                 logout        /users/logout                             {:action=>"logout", :controller=>"users"}
#                               /users/change_password                    {:action=>"change_password", :controller=>"users"}
#     edit_user_settings        /user/settings/edit                       {:action=>"edit_settings", :controller=>"users"}
#          print_message        /users/:user_id/messages/:id/print        {:action=>"print_message", :controller=>"messages"}
#           print_digest        /users/:user_id/messages/print_digest     {:action=>"print_digest", :controller=>"messages"}
#       destroy_selected        /users/:user_id/messages/destroy_selected {:action=>"destroy_selected", :controller=>"messages"}
#        select_messages POST   /users/:user_id/messages/select_messages  {:action=>"select_messages", :controller=>"messages"}
#    inbox_user_messages GET    /users/:user_id/messages/inbox            {:action=>"inbox", :controller=>"messages"}
#   outbox_user_messages GET    /users/:user_id/messages/outbox           {:action=>"outbox", :controller=>"messages"}
# trashbin_user_messages GET    /users/:user_id/messages/trashbin         {:action=>"trashbin", :controller=>"messages"}
#          user_messages GET    /users/:user_id/messages                  {:action=>"index", :controller=>"messages"}
#                        POST   /users/:user_id/messages                  {:action=>"create", :controller=>"messages"}
#       new_user_message GET    /users/:user_id/messages/new              {:action=>"new", :controller=>"messages"}
#      edit_user_message GET    /users/:user_id/messages/:id/edit         {:action=>"edit", :controller=>"messages"}
#     reply_user_message GET    /users/:user_id/messages/:id/reply        {:action=>"reply", :controller=>"messages"}
#           user_message GET    /users/:user_id/messages/:id              {:action=>"show", :controller=>"messages"}
#                        PUT    /users/:user_id/messages/:id              {:action=>"update", :controller=>"messages"}
#                        DELETE /users/:user_id/messages/:id              {:action=>"destroy", :controller=>"messages"}
#            toggle_user POST   /user/:id/toggle                          {:action=>"toggle", :controller=>"users"}
#             reset_user POST   /user/:id/reset                           {:action=>"reset", :controller=>"users"}
#              edit_user        /user/edit                                {:action=>"edit", :controller=>"users"}
#                   user GET    /user/:id                                 {:action=>"show", :controller=>"users"}
#                        DELETE /user/:id                                 {:action=>"destroy", :controller=>"users"}
#                  users GET    /users                                    {:action=>"index", :controller=>"users"}
#             edit_group        /group/edit                               {:action=>"edit", :controller=>"groups"}
#                  group GET    /group/:id                                {:action=>"show", :controller=>"groups"}
#                        DELETE /group/:id                                {:action=>"destroy", :controller=>"groups"}
#                 groups GET    /groups                                   {:action=>"index", :controller=>"groups"}
#              edit_role        /role/edit                                {:action=>"edit", :controller=>"roles"}
#                   role GET    /role/:id                                 {:action=>"show", :controller=>"roles"}
#                        DELETE /role/:id                                 {:action=>"destroy", :controller=>"roles"}
#                  roles GET    /roles                                    {:action=>"index", :controller=>"roles"}
#                  perms GET    /perms                                    {:action=>"index", :controller=>"perms"}
#          toggle_option POST   /option/:id/toggle                        {:action=>"toggle", :controller=>"options"}
#            edit_option        /option/edit                              {:action=>"edit", :controller=>"options"}
#                 option GET    /option/:id                               {:action=>"show", :controller=>"options"}
#                        DELETE /option/:id                               {:action=>"destroy", :controller=>"options"}
#                options GET    /options                                  {:action=>"index", :controller=>"options"}
#                   case GET    /show_case                                {:action=>"show_case", :controller=>"root"}
#              edit_site        /edit_site_config                         {:action=>"edit_site_config", :controller=>"root"}
#                   site GET    /site_config                              {:action=>"site_config", :controller=>"root"}
#               site_css GET    /site.css                                 {:action=>"site", :controller=>"root"}
#                   root        /                                         {:action=>"index", :controller=>"root"}
#                               /:controller/:action/:id                  
#                               /:controller/:action/:id(.:format)        
