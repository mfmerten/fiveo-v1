# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# special settings for FiveO
require 'ostruct'
unless File.exist?("#{RAILS_ROOT}/public/system/site_configuration.yml")
   if File.exist?("#{RAILS_ROOT}/config/defaults.yml")
      FileUtils.cp("#{RAILS_ROOT}/config/defaults.yml","#{RAILS_ROOT}/public/system/site_configuration.yml")
   end
end
if File.exist?("#{RAILS_ROOT}/config/defaults.yml")
   ::SiteConfigDefault = OpenStruct.new(YAML.load_file("#{RAILS_ROOT}/config/defaults.yml")).freeze
else
   throw "Missing Site Defaults File!"
end
if File.exist?("#{RAILS_ROOT}/public/system/site_configuration.yml")
   ::SiteConfig = OpenStruct.new(YAML.load_file("#{RAILS_ROOT}/public/system/site_configuration.yml"))
else
   throw "Missing Site Configuration File!"
end

ValidatesDateTime.us_date_format = true

CalendarDateSelect.format = :american

ActiveRecord::Base.class_eval do
   include FiveoModelUtilities
   include TimeUtils
end

ActionController::Base.class_eval do
   include FiveoModelUtilities
   include TimeUtils
end

ActionView::Base.class_eval do
   include FiveoModelUtilities
   include TimeUtils
end

Prawn::Document.extensions << PrawnExtensions

::Perm = Permissions::PermList.new.to_h.freeze