# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
desc "Deploy fiveo to the SPSO web server using push via VPN."
task :spso do
   default_run_options[:pty] = true
   set :application, "fiveo"
   set :domain, "fiveoadm@192.168.1.5"
   set :scm, :git
   set :repository, "git@midlaketech.dyndns.org:fiveo.git"
   set :branch, 'origin/fiveo-v1'
   set :deploy_via, :remote_cache
   set :deploy_to, "/srv/rails/#{application}"
   set :use_sudo, false
   role :app, domain
   role :web, domain
   role :db,  domain, :primary => true
   
   task :before_update_code, :roles => [:app] do
      run "cd #{current_release} && rake ts:stop RAILS_ENV=production"
   end

   task :after_update_code, :roles => [:app] do
      db_config = "#{shared_path}/conf/database.yml"
      www_root = "/srv/www"
      www_image = "/srv/www/images"
      run "cp #{db_config} #{release_path}/config/database.yml"
      run "cp #{release_path}/db/018_sabine_parish_so_la.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/019_louisiana_statutes.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/020_louisiana_wildlife_statutes.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/021_louisiana_title47.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/20081003024939_spso_signal_codes.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/20090427013758_spso_commissary_items.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/20110108133358_spso_civil_customers.rb #{release_path}/db/migrate/"
      run "mv #{release_path}/index.html #{www_root}/index.html"
      run "mkdir -p #{www_image}/calendar_date_select"
      run "cp #{release_path}/public/images/favicon.png #{www_image}/favicon.png"
      run "cp #{release_path}/public/images/calendar_date_select/calendar.gif #{www_image}/calendar_date_select/calendar.gif"
      run "echo 'spso@midlaketech.com' > #{release_path}/site_mail_name.txt"
   end

   after 'deploy:restart', 'deploy:restart_sphinx'
   after 'deploy:restart', 'deploy:cleanup'
   before 'deploy:symlink', 'deploy:migrate'
   after 'deploy:symlink', 'deploy:update_crontab'
end

desc "Deploy fiveo to the SPSO training server using push via VPN."
task :spsotrain do
   default_run_options[:pty] = true
   set :application, "fiveo"
   set :domain, "fiveoadm@192.168.1.17"
   set :scm, :git
   set :repository, "git@midlaketech.dyndns.org:fiveo.git"
   set :branch, 'origin/fiveo-v1'
   set :deploy_via, :remote_cache
   set :deploy_to, "/srv/rails/#{application}"
   set :use_sudo, false
   role :app, domain
   role :web, domain
   role :db,  domain, :primary => true
   
   task :before_update_code, :roles => [:app] do
      run "cd #{current_release} && rake ts:stop RAILS_ENV=production"
   end

   task :after_update_code, :roles => [:app] do
      db_config = "#{shared_path}/conf/database.yml"
      www_root = "/srv/www"
      www_image = "/srv/www/images"
      run "cp #{db_config} #{release_path}/config/database.yml"
      run "cp #{release_path}/db/018_sabine_parish_so_la.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/019_louisiana_statutes.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/020_louisiana_wildlife_statutes.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/021_louisiana_title47.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/20081003024939_spso_signal_codes.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/20090427013758_spso_commissary_items.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/20110108133358_spso_civil_customers.rb #{release_path}/db/migrate/"
      run "mv #{release_path}/index.html #{www_root}/index.html"
      run "mkdir -p #{www_image}/calendar_date_select"
      run "cp #{release_path}/public/images/favicon.png #{www_image}/favicon.png"
      run "cp #{release_path}/public/images/calendar_date_select/calendar.gif #{www_image}/calendar_date_select/calendar.gif"
      run "echo 'spsotrain@midlaketech.com' > #{release_path}/site_mail_name.txt"
   end

   after 'deploy:restart', 'deploy:restart_sphinx'
   after 'deploy:restart', 'deploy:cleanup'
   before 'deploy:symlink', 'deploy:migrate'
   after 'deploy:symlink', 'deploy:update_crontab'
end

desc "Deploy fiveo to the appserver."
task :appserver do
   set :application, "fiveo"
   set :domain, "fiveoadm@10.0.0.129"
   set :scm, :git
   set :repository, "git@midlaketech.dyndns.org:fiveo.git"
   set :branch, 'origin/fiveo-v1'
   set :deploy_via, :remote_cache
   set :deploy_to, "/srv/rails/#{application}"
   set :use_sudo, false
   role :app, domain
   role :web, domain
   role :db,  domain, :primary => true

   task :before_update_code, :roles => [:app] do
      run "cd #{current_release} && rake ts:stop RAILS_ENV=production"
   end
   
   task :after_update_code, :roles => [:app] do
      db_config = "#{shared_path}/conf/database.yml"
      www_root = "/var/www"
      www_image = "/var/www/images"
      run "cp #{db_config} #{release_path}/config/database.yml"
      run "cp #{release_path}/db/018_sabine_parish_so_la.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/019_louisiana_statutes.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/020_louisiana_wildlife_statutes.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/021_louisiana_title47.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/20081003024939_spso_signal_codes.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/20090427013758_spso_commissary_items.rb #{release_path}/db/migrate/"
      run "cp #{release_path}/db/20110108133358_spso_civil_customers.rb #{release_path}/db/migrate/"
      run "mkdir -p #{www_image}/calendar_date_select"
      run "cp #{release_path}/public/images/favicon.png #{www_image}/favicon.png"
      run "cp #{release_path}/public/images/calendar_date_select/calendar.gif #{www_image}/calendar_date_select/calendar.gif"
      run "echo 'appserver@midlaketech.com' > #{release_path}/site_mail_name.txt"
   end
   
   after 'deploy:restart', 'deploy:restart_sphinx'
   after 'deploy:restart', 'deploy:cleanup'
   before 'deploy:symlink', 'deploy:migrate'
   after 'deploy:symlink', 'deploy:update_crontab'
end

namespace :deploy do
   desc "Restart Application"
   task :restart, :roles => :app do
      run "touch #{current_path}/tmp/restart.txt"
   end
   
   desc "Override Deploy"
   task :migrate, :roles => :app do
      run "cd #{release_path} && rake --trace db:migrate RAILS_ENV=production"
   end
   
   desc "Restart Sphinx"
   task :restart_sphinx, :roles => :app do
      run "cd #{release_path} && rake ts:index RAILS_ENV=production"
      run "cd #{release_path} && rake ts:start RAILS_ENV=production"
   end

   desc "Update the crontab file"
   task :update_crontab, :roles => :db do
      run "cd #{release_path} && whenever --update-crontab #{application}"
   end
end
