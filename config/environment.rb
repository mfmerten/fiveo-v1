# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Be sure to restart your server when you modify this file

# Uncomment below to force Rails into production mode when
# you don't control web/app server and can't set it the proper way
# ENV['RAILS_ENV'] ||= 'production'

# Specifies gem version of Rails to use when vendor/rails is not present
RAILS_GEM_VERSION = '2.3.5' unless defined? RAILS_GEM_VERSION

# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')

Rails::Initializer.run do |config|
  # Settings in config/environments/* take precedence over those specified here.
  # Application configuration should go into files in config/initializers
  # -- all .rb files in that directory are automatically loaded.
  # See Rails::Configuration for more options.
  
  # config.gem 'whenever', :lib => false, :source => 'http://gemcutter.org/'
  config.gem 'thinking-sphinx', :lib => 'thinking_sphinx', :version => '1.3.16'
  config.gem 'mysql'
  config.gem 'fastercsv'
  config.gem 'prawn', :version => '>= 0.8.4'
  config.gem 'RedCloth', :version => '>= 4.2.3'
  
  # Skip frameworks you're not going to use (only works if using vendor/rails).
  # To use Rails without a database, you must remove the Active Record framework
  # config.frameworks -= [ :active_record, :active_resource, :action_mailer ]

  # Only load the plugins named here, in the order given. By default, all plugins 
  # in vendor/plugins are loaded in alphabetical order.
  # :all can be used as a placeholder for all plugins not explicitly named
  # config.plugins = [ :all, :validation_reflection ]

  # Add additional load paths for your own custom dirs
  # config.load_paths += %W( #{RAILS_ROOT}/extras )
  config.load_paths += %W( #{RAILS_ROOT}/extra )
  config.load_once_paths += %W( #{RAILS_ROOT}/extra )

  # Force all environments to use the same logger level
  # (by default production uses :info, the others :debug)
  # config.log_level = :debug

  # Your secret key for verifying cookie session data integrity.
  # If you change this key, all old sessions will become invalid!
  # Make sure the secret is at least 30 characters and all random, 
  # no regular words or you'll be exposed to dictionary attacks.
  config.action_controller.session = {
    :session_key => '_fiveo_session',
    :secret      => 'PBOuYKyvsxsA6B1cUglB3G5VrCD2TgZ7ftsW0Xa1ODHOISzA4XGNPUAwgOnU8TwEr1J1LLlnyRiMsqSxfNu2Eg4ZhhNAE7Qn7VVYO2kifiKj2KkW7S7PEb0WiQQyd2Bu'
  }

  # Use the database for sessions instead of the cookie-based default,
  # which shouldn't be used to store highly confidential information
  # (create the session table with 'rake db:sessions:create')
  config.action_controller.session_store = :active_record_store

  # Use SQL instead of Active Record's schema dumper when creating the test database.
  # This is necessary if your schema can't be completely dumped by the schema dumper,
  # like if you have constraints or database-specific column types
  # config.active_record.schema_format = :sql

  # Activate observers that should always be running
  # config.active_record.observers = :cacher, :garbage_collector

  # Make Active Record use UTC-base instead of local time
  # config.active_record.default_timezone = :utc
end

ActionMailer::Base.smtp_settings[:enable_starttls_auto] = false
ExceptionNotification::Notifier.exception_recipients = %w(developer@midlaketech.com)
if File::exists?("site_mail_name.txt")
   if efile = File.new("site_mail_name.txt")
      email = efile.gets.chomp
      ExceptionNotification::Notifier.sender_address = %("Application Error" <#{email}>)
      efile.close
   end
end
ExceptionNotification::Notifier.email_prefix = "[FiveO] "
