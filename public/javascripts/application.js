// FiveO Law Enforcement Software
// Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
//
// This file is part of FiveO.
// 
// FiveO is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// FiveO is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with FiveO. If not, see <http://www.gnu.org/licenses/>.
// 
// ===========================================================================
// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

var newwindow = '';

function peopleLookup(url, caller_id, caller_name) {
  var path=encodeURI(url) + '?caller_id=' + encodeURIComponent(caller_id) + '&caller_name=' + encodeURIComponent(caller_name);
  var name=prompt('Name to Search:','');
  if (name!=null && name!='') {
    path = path + '&name=' + encodeURIComponent(name);
    if (!newwindow.closed && newwindow.location) {
        newwindow.location.href = path;
    }
    else {
        newwindow=window.open(path,'name','height=500,width=800');
        if (!newwindow.opener) newwindow.opener = self;
    }
    if (window.focus) newwindow.focus();
  }
}

function contactRowToggle(select_id,name_id,unit_id,badge_id) {
  if($F(select_id) != '') {
     $(name_id).clear();
     $(name_id).disabled = true;
     if(unit_id != '') {
       $(unit_id).clear();
       $(unit_id).disabled = true;
     }
     if(badge_id != '') {
       $(badge_id).clear();
       $(badge_id).disabled = true;
     }
  }
  else {
     $(name_id).disabled = false;
     if(unit_id != '') $(unit_id).disabled = false;
     if(badge_id != '') $(badge_id).disabled = false;
  }
}

function selectWarrant(id, caller_id, status, caller_status, charges, caller_charges) {
  window.opener.document.getElementById(caller_id).value = id;
  window.opener.document.getElementById(caller_status).innerHTML = status;
  window.opener.document.getElementById(caller_charges).innerHTML = charges;
  window.close();
  return false;
}

function selectPerson(id,caller_id,name,caller_name,status,status_name) {
  if(status == ''){
    status = '&nbsp;';
  }
  window.opener.document.getElementById(caller_id).value = id;
  if(caller_name != ''){
    window.opener.document.getElementById(caller_name).value = name;
  }
  if(status_name != ''){
    window.opener.document.getElementById(status_name).innerHTML = status;
  }
  window.close();
  return false;
}

function selectBooking(id,caller_id,name,caller_name) {
  window.opener.document.getElementById(caller_id).value = id;
  if(caller_name != ''){
    window.opener.document.getElementById(caller_name).innerHTML = name;
  }
  window.close();
  return false;
}

function selectSignal(code, value) {
  window.opener.document.getElementById('call_signal').value = code;
  window.opener.document.getElementById('signal_name').innerHTML = value;
  window.close();
  return false;
}

function contactLookup(url, caller_unit, caller_name) {
  var path=encodeURI(url) + '?caller_unit=' + encodeURIComponent(caller_unit) + '&caller_name=' + encodeURIComponent(caller_name);
  var name=prompt('Name to Search:','');
  if (name!=null && name!='') {
    path = path + '&name=' + encodeURIComponent(name);
    if (!newwindow.closed && newwindow.location) {
        newwindow.location.href = path;
    }
    else {
        newwindow=window.open(path,'name','height=500,width=800');
        if (!newwindow.opener) newwindow.opener = self;
    }
    if (window.focus) newwindow.focus();
  }
}

function selectContact(unit,caller_unit,name,caller_name) {
  window.opener.document.getElementById(caller_unit).value = unit;
   window.opener.document.getElementById(caller_name).value = name;
   window.close();
   return false;
}
