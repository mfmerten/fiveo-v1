/*
FiveO Law Enforcement Software
Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>

This file is part of FiveO.

FiveO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FiveO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FiveO. If not, see <http://www.gnu.org/licenses/>.

===========================================================================
*/
drop database if exists fiveo_v1_development;
drop database if exists fiveo_v1_test;
drop database if exists fiveo_v1_production;
create database fiveo_v1_development;
grant all on fiveo_v1_development.* to fiveo_user@localhost identified by 'fiveo_123';
create database fiveo_v1_test;
grant all on fiveo_v1_test.* to fiveo_user@localhost identified by 'fiveo_123';
create database fiveo_v1_production;
grant all on fiveo_v1_production.* to fiveo_user@localhost identified by 'fiveo_123';
flush privileges;
