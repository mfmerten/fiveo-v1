# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class SpsoSignalCodes < ActiveRecord::Migration
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => "SpsoSignalCodes::Option"
   end
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => "SpsoSignalCodes::OptionType"
      validates_presence_of :long_name, :option_type_id
   end

   def self.up
      unless ot = OptionType.find_by_name("Signal Code")
         ot = OptionType.create(:name => "Signal Code")
      end
      
      Option.create :short_name => "01", :long_name => "Misdemeanor Summons", :option_type_id => ot.id
      Option.create :short_name => "02", :long_name => "Traffic Ticket", :option_type_id => ot.id
      Option.create :short_name => "10", :long_name => "Public Assist", :option_type_id => ot.id
      Option.create :short_name => "100", :long_name => "Hit And Run", :option_type_id => ot.id
      Option.create :short_name => "101", :long_name => "Desecration Of Graves", :option_type_id => ot.id
      Option.create :short_name => "102", :long_name => "Animal Complaints", :option_type_id => ot.id
      Option.create :short_name => "103D", :long_name => "Disturbance", :option_type_id => ot.id
      Option.create :short_name => "103F", :long_name => "Fight", :option_type_id => ot.id
      Option.create :short_name => "103M", :long_name => "Mental", :option_type_id => ot.id
      Option.create :short_name => "103R", :long_name => "Riot", :option_type_id => ot.id
      Option.create :short_name => "104", :long_name => "Domestic Disturbance", :option_type_id => ot.id
      Option.create :short_name => "104A", :long_name => "Domestic Assistance", :option_type_id => ot.id
      Option.create :short_name => "105", :long_name => "Missing Person", :option_type_id => ot.id
      Option.create :short_name => "105W", :long_name => "Welfare Check", :option_type_id => ot.id
      Option.create :short_name => "106", :long_name => "Obscenity (Exposing Person, Etc.)", :option_type_id => ot.id
      Option.create :short_name => "107", :long_name => "Suspicious Person/Activity/Vehicle", :option_type_id => ot.id
      Option.create :short_name => "108", :long_name => "Officer Needs Assistance/Life In Danger", :option_type_id => ot.id
      Option.create :short_name => "109", :long_name => "Aggravated Escape", :option_type_id => ot.id
      Option.create :short_name => "110", :long_name => "Simple Escape", :option_type_id => ot.id
      Option.create :short_name => "112", :long_name => "Impersonating An Officer", :option_type_id => ot.id
      Option.create :short_name => "113", :long_name => "Resisting/Flight From An Officer", :option_type_id => ot.id
      Option.create :short_name => "116", :long_name => "Flag", :option_type_id => ot.id
      Option.create :short_name => "118", :long_name => "Public Bribery", :option_type_id => ot.id
      Option.create :short_name => "12", :long_name => "Peace Bond", :option_type_id => ot.id
      Option.create :short_name => "123", :long_name => "Keys Locked In Vehicle", :option_type_id => ot.id
      Option.create :short_name => "13", :long_name => "Lost And Found", :option_type_id => ot.id
      Option.create :short_name => "14", :long_name => "Out Of State Warrant", :option_type_id => ot.id
      Option.create :short_name => "1468", :long_name => "Unauthorized Use Of A Moveable", :option_type_id => ot.id
      Option.create :short_name => "15", :long_name => "Citizen Holding A Suspect", :option_type_id => ot.id
      Option.create :short_name => "16", :long_name => "Four Wheelers", :option_type_id => ot.id
      Option.create :short_name => "17F", :long_name => "Fugitive Attachment", :option_type_id => ot.id
      Option.create :short_name => "17J", :long_name => "Juvenile", :option_type_id => ot.id
      Option.create :short_name => "17M", :long_name => "Municipal Court Attachment", :option_type_id => ot.id
      Option.create :short_name => "17T", :long_name => "Traffic Attachment", :option_type_id => ot.id
      Option.create :short_name => "18", :long_name => "Traffic Incidents", :option_type_id => ot.id
      Option.create :short_name => "19", :long_name => "Drunk", :option_type_id => ot.id
      Option.create :short_name => "20", :long_name => "Auto Accident", :option_type_id => ot.id
      Option.create :short_name => "20A", :long_name => "Accident Involving Animals", :option_type_id => ot.id
      Option.create :short_name => "20B", :long_name => "Boating Accident", :option_type_id => ot.id
      Option.create :short_name => "20F", :long_name => "Fatal Accident", :option_type_id => ot.id
      Option.create :short_name => "22", :long_name => "Bench Warrants", :option_type_id => ot.id
      Option.create :short_name => "23", :long_name => "Traffic Congestion", :option_type_id => ot.id
      Option.create :short_name => "24", :long_name => "Medical Emergency", :option_type_id => ot.id
      Option.create :short_name => "25", :long_name => "Forbidden To Enter", :option_type_id => ot.id
      Option.create :short_name => "26", :long_name => "Vandalism", :option_type_id => ot.id
      Option.create :short_name => "27", :long_name => "Attempted 2nd Degree Murder", :option_type_id => ot.id
      Option.create :short_name => "284", :long_name => "Peeping Tom", :option_type_id => ot.id
      Option.create :short_name => "29", :long_name => "Death", :option_type_id => ot.id
      Option.create :short_name => "29A", :long_name => "Attempted Suicide", :option_type_id => ot.id
      Option.create :short_name => "29S", :long_name => "Suicide", :option_type_id => ot.id
      Option.create :short_name => "30", :long_name => "Homicide", :option_type_id => ot.id
      Option.create :short_name => "30C", :long_name => "Homicide By Cutting", :option_type_id => ot.id
      Option.create :short_name => "30S", :long_name => "Homicide By Shooting", :option_type_id => ot.id
      Option.create :short_name => "31", :long_name => "Attempted Manslaughter", :option_type_id => ot.id
      Option.create :short_name => "3289", :long_name => "Littering", :option_type_id => ot.id
      Option.create :short_name => "33", :long_name => "Unauthorized Entry", :option_type_id => ot.id
      Option.create :short_name => "34", :long_name => "Aggravated Battery", :option_type_id => ot.id
      Option.create :short_name => "34C", :long_name => "Cutting", :option_type_id => ot.id
      Option.create :short_name => "34S", :long_name => "Shooting", :option_type_id => ot.id
      Option.create :short_name => "35", :long_name => "Simple Battery", :option_type_id => ot.id
      Option.create :short_name => "36", :long_name => "Harrassment", :option_type_id => ot.id
      Option.create :short_name => "37", :long_name => "Aggravated Assault", :option_type_id => ot.id
      Option.create :short_name => "38", :long_name => "Simple Assault", :option_type_id => ot.id
      Option.create :short_name => "39", :long_name => "Negligent Injuries", :option_type_id => ot.id
      Option.create :short_name => "42", :long_name => "Aggravated Rape", :option_type_id => ot.id
      Option.create :short_name => "43", :long_name => "Simple Rape", :option_type_id => ot.id
      Option.create :short_name => "44", :long_name => "Aggravated Kidnapping", :option_type_id => ot.id
      Option.create :short_name => "45", :long_name => "Simple Kidnapping", :option_type_id => ot.id
      Option.create :short_name => "51", :long_name => "Aggravated Arson", :option_type_id => ot.id
      Option.create :short_name => "51B", :long_name => "Bomb Scare", :option_type_id => ot.id
      Option.create :short_name => "52", :long_name => "Simple Arson", :option_type_id => ot.id
      Option.create :short_name => "52E", :long_name => "Explosion", :option_type_id => ot.id
      Option.create :short_name => "52F", :long_name => "Fire", :option_type_id => ot.id
      Option.create :short_name => "53", :long_name => "Hazardous Material Incident", :option_type_id => ot.id
      Option.create :short_name => "54", :long_name => "Placing Combustible Materials (Fire Bomb)", :option_type_id => ot.id
      Option.create :short_name => "56", :long_name => "Damage To Property", :option_type_id => ot.id
      Option.create :short_name => "58", :long_name => "Contaminating Water Supplies", :option_type_id => ot.id
      Option.create :short_name => "59", :long_name => "Criminal Mischief", :option_type_id => ot.id
      Option.create :short_name => "60", :long_name => "Aggravated Burglary", :option_type_id => ot.id
      Option.create :short_name => "62", :long_name => "Simple Burglary", :option_type_id => ot.id
      Option.create :short_name => "62A", :long_name => "Alarm Response", :option_type_id => ot.id
      Option.create :short_name => "62AR", :long_name => "Attempted Residential Break-In", :option_type_id => ot.id
      Option.create :short_name => "62B", :long_name => "Business Burglary", :option_type_id => ot.id
      Option.create :short_name => "62C", :long_name => "Theft From Interior Of Auto", :option_type_id => ot.id
      Option.create :short_name => "62R", :long_name => "Resident Burglary", :option_type_id => ot.id
      Option.create :short_name => "63", :long_name => "Criminal Trespass", :option_type_id => ot.id
      Option.create :short_name => "63A", :long_name => "Aiding", :option_type_id => ot.id
      Option.create :short_name => "63D", :long_name => "Demonstration", :option_type_id => ot.id
      Option.create :short_name => "63S", :long_name => "Sit-Ins", :option_type_id => ot.id
      Option.create :short_name => "64", :long_name => "Armed Robbery", :option_type_id => ot.id
      Option.create :short_name => "64G", :long_name => "Armed Robbery (Gun)", :option_type_id => ot.id
      Option.create :short_name => "64K", :long_name => "Armed Robbery (Knife)", :option_type_id => ot.id
      Option.create :short_name => "65", :long_name => "Simple Robbery", :option_type_id => ot.id
      Option.create :short_name => "66", :long_name => "Extortion (Threats)", :option_type_id => ot.id
      Option.create :short_name => "67", :long_name => "Theft", :option_type_id => ot.id
      Option.create :short_name => "67A", :long_name => "Attempted Theft", :option_type_id => ot.id
      Option.create :short_name => "67B", :long_name => "Theft of Bicycle", :option_type_id => ot.id
      Option.create :short_name => "67C", :long_name => "Theft of Exterior of Vehicle", :option_type_id => ot.id
      Option.create :short_name => "67P", :long_name => "Pickpocket", :option_type_id => ot.id
      Option.create :short_name => "68", :long_name => "Possession Of Stolen Property", :option_type_id => ot.id
      Option.create :short_name => "71", :long_name => "Issuing Worthless Checks", :option_type_id => ot.id
      Option.create :short_name => "72", :long_name => "Forgery", :option_type_id => ot.id
      Option.create :short_name => "78", :long_name => "Incest", :option_type_id => ot.id
      Option.create :short_name => "80", :long_name => "Carnal Knowledge Of A Juvenile", :option_type_id => ot.id
      Option.create :short_name => "81", :long_name => "Indecent Behavior With Juvenile", :option_type_id => ot.id
      Option.create :short_name => "82", :long_name => "Prostitution", :option_type_id => ot.id
      Option.create :short_name => "83", :long_name => "Soliciting For Prostitution", :option_type_id => ot.id
      Option.create :short_name => "84", :long_name => "Pandering", :option_type_id => ot.id
      Option.create :short_name => "87", :long_name => "Child Endangerment", :option_type_id => ot.id
      Option.create :short_name => "88", :long_name => "Abuse of Elderly", :option_type_id => ot.id
      Option.create :short_name => "89", :long_name => "Crime Against Nature", :option_type_id => ot.id
      Option.create :short_name => "90", :long_name => "Gambling", :option_type_id => ot.id
      Option.create :short_name => "90C", :long_name => "Gambling By Cards", :option_type_id => ot.id
      Option.create :short_name => "90D", :long_name => "Gambling By Dice", :option_type_id => ot.id
      Option.create :short_name => "90H", :long_name => "Gambling By Handbook", :option_type_id => ot.id
      Option.create :short_name => "90L", :long_name => "Gambling By Lottery", :option_type_id => ot.id
      Option.create :short_name => "90P", :long_name => "Gambling By Pinball", :option_type_id => ot.id
      Option.create :short_name => "91", :long_name => "Unlawful Sales To Minors", :option_type_id => ot.id
      Option.create :short_name => "92", :long_name => "Contributing To The Delinquency", :option_type_id => ot.id
      Option.create :short_name => "93", :long_name => "Cruelty To Juvenile", :option_type_id => ot.id
      Option.create :short_name => "94", :long_name => "Illegal Use Of Weapons(Disch. Firearm)", :option_type_id => ot.id
      Option.create :short_name => "94F", :long_name => "Fireworks", :option_type_id => ot.id
      Option.create :short_name => "95", :long_name => "Illegal Carrying of Weapons", :option_type_id => ot.id
      Option.create :short_name => "95G", :long_name => "Illegal Carrying of Weapons (Gun)", :option_type_id => ot.id
      Option.create :short_name => "95K", :long_name => "Illegal Carrying of Weapons (Knife)", :option_type_id => ot.id
      Option.create :short_name => "966", :long_name => "Drug Law Violation", :option_type_id => ot.id
      Option.create :short_name => "97", :long_name => "Picketing", :option_type_id => ot.id
      Option.create :short_name => "98", :long_name => "DWI", :option_type_id => ot.id
      Option.create :short_name => "99", :long_name => "Reckless Driving", :option_type_id => ot.id
      Option.create :short_name => "999", :long_name => "Miscellaneous", :option_type_id => ot.id
      
   end

   def self.down
   end
end
