# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110722012648) do

  create_table "absents", :force => true do |t|
    t.date     "leave_date"
    t.string   "leave_time"
    t.integer  "leave_type_id"
    t.integer  "facility_id"
    t.date     "return_date"
    t.string   "return_time"
    t.string   "escort_officer"
    t.string   "escort_officer_unit"
    t.string   "escort_officer_badge"
    t.integer  "escort_officer_id"
    t.integer  "created_by",           :default => 1
    t.integer  "updated_by",           :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "absents", ["created_by"], :name => "index_absents_on_created_by"
  add_index "absents", ["facility_id"], :name => "index_absents_on_facility_id"
  add_index "absents", ["leave_type_id"], :name => "index_absents_on_leave_type_id"
  add_index "absents", ["updated_by"], :name => "index_absents_on_updated_by"

  create_table "absents_bookings", :id => false, :force => true do |t|
    t.integer "absent_id"
    t.integer "booking_id"
  end

  add_index "absents_bookings", ["absent_id"], :name => "index_absents_bookings_on_absent_id"
  add_index "absents_bookings", ["booking_id"], :name => "index_absents_bookings_on_booking_id"

  create_table "activities", :force => true do |t|
    t.integer  "user_id"
    t.string   "section"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["section"], :name => "index_activities_on_section"
  add_index "activities", ["user_id"], :name => "index_activities_on_user_id"

  create_table "announce_ignores", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "announcement_id"
  end

  add_index "announce_ignores", ["announcement_id"], :name => "index_announce_ignores_on_announcement_id"
  add_index "announce_ignores", ["user_id"], :name => "index_announce_ignores_on_user_id"

  create_table "announcements", :force => true do |t|
    t.string   "subject"
    t.text     "body"
    t.integer  "created_by",  :default => 1
    t.integer  "updated_by",  :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "unignorable", :default => false
  end

  add_index "announcements", ["created_by"], :name => "index_announcements_on_created_by"
  add_index "announcements", ["updated_by"], :name => "index_announcements_on_updated_by"

  create_table "arrests", :force => true do |t|
    t.integer  "person_id"
    t.integer  "offense_id"
    t.string   "case_no"
    t.date     "arrest_date"
    t.string   "arrest_time"
    t.integer  "arrest_type",                                           :default => 0
    t.string   "officer1"
    t.string   "officer1_unit"
    t.string   "officer1_badge"
    t.integer  "officer1_id"
    t.string   "officer2"
    t.string   "officer2_unit"
    t.string   "officer2_badge"
    t.integer  "officer2_id"
    t.string   "officer3"
    t.string   "officer3_unit"
    t.string   "officer3_badge"
    t.integer  "officer3_id"
    t.string   "agency"
    t.integer  "agency_id"
    t.string   "atn"
    t.integer  "ward"
    t.integer  "district"
    t.boolean  "victim_notify"
    t.string   "dwi_test_officer"
    t.string   "dwi_test_officer_unit"
    t.string   "dwi_test_officer_badge"
    t.integer  "dwi_test_officer_id"
    t.string   "dwi_test_results"
    t.integer  "hour_72_judge_id"
    t.date     "hour_72_date"
    t.string   "hour_72_time"
    t.string   "attorney"
    t.string   "condition_of_bond"
    t.decimal  "bond_amt",               :precision => 12, :scale => 2, :default => 0.0
    t.boolean  "bondable",                                              :default => false
    t.text     "remarks"
    t.boolean  "legacy",                                                :default => false
    t.integer  "created_by",                                            :default => 1
    t.integer  "updated_by",                                            :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "arrests", ["case_no"], :name => "index_arrests_on_case_no"
  add_index "arrests", ["created_by"], :name => "index_arrests_on_created_by"
  add_index "arrests", ["hour_72_judge_id"], :name => "index_arrests_on_hour_72_judge_id"
  add_index "arrests", ["offense_id"], :name => "index_arrests_on_offense_id"
  add_index "arrests", ["person_id"], :name => "index_arrests_on_person_id"
  add_index "arrests", ["updated_by"], :name => "index_arrests_on_updated_by"

  create_table "associates", :force => true do |t|
    t.integer  "person_id"
    t.integer  "relationship_id"
    t.string   "name"
    t.text     "remarks"
    t.integer  "created_by",      :default => 1
    t.integer  "updated_by",      :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "associates", ["created_by"], :name => "index_associates_on_created_by"
  add_index "associates", ["person_id"], :name => "index_associates_on_person_id"
  add_index "associates", ["relationship_id"], :name => "index_associates_on_relationship_id"
  add_index "associates", ["updated_by"], :name => "index_associates_on_updated_by"

  create_table "bj_config", :primary_key => "bj_config_id", :force => true do |t|
    t.text "hostname"
    t.text "key"
    t.text "value"
    t.text "cast"
  end

  create_table "bj_job", :primary_key => "bj_job_id", :force => true do |t|
    t.text     "command"
    t.text     "state"
    t.integer  "priority"
    t.text     "tag"
    t.integer  "is_restartable"
    t.text     "submitter"
    t.text     "runner"
    t.integer  "pid"
    t.datetime "submitted_at"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.text     "env"
    t.text     "stdin"
    t.text     "stdout"
    t.text     "stderr"
    t.integer  "exit_status"
  end

  create_table "bj_job_archive", :primary_key => "bj_job_archive_id", :force => true do |t|
    t.text     "command"
    t.text     "state"
    t.integer  "priority"
    t.text     "tag"
    t.integer  "is_restartable"
    t.text     "submitter"
    t.text     "runner"
    t.integer  "pid"
    t.datetime "submitted_at"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.datetime "archived_at"
    t.text     "env"
    t.text     "stdin"
    t.text     "stdout"
    t.text     "stderr"
    t.integer  "exit_status"
  end

  create_table "bonds", :force => true do |t|
    t.integer  "bondsman_id"
    t.integer  "surety_id"
    t.integer  "person_id"
    t.integer  "arrest_id"
    t.integer  "hold_id"
    t.decimal  "bond_amt",     :precision => 12, :scale => 2, :default => 0.0
    t.date     "issued_date"
    t.string   "issued_time"
    t.integer  "bond_type_id"
    t.text     "remarks"
    t.string   "power"
    t.integer  "status_id"
    t.date     "status_date"
    t.string   "status_time"
    t.string   "bond_no"
    t.integer  "created_by",                                  :default => 1
    t.integer  "updated_by",                                  :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bonds", ["arrest_id"], :name => "index_bonds_on_arrest_id"
  add_index "bonds", ["bond_type_id"], :name => "index_bonds_on_bond_type_id"
  add_index "bonds", ["bondsman_id"], :name => "index_bonds_on_bondsman_id"
  add_index "bonds", ["created_by"], :name => "index_bonds_on_created_by"
  add_index "bonds", ["hold_id"], :name => "index_bonds_on_hold_id"
  add_index "bonds", ["person_id"], :name => "index_bonds_on_person_id"
  add_index "bonds", ["status_id"], :name => "index_bonds_on_status_id"
  add_index "bonds", ["surety_id"], :name => "index_bonds_on_surety_id"
  add_index "bonds", ["updated_by"], :name => "index_bonds_on_updated_by"

  create_table "bondsman_phones", :force => true do |t|
    t.integer  "bondsman_id"
    t.integer  "label_id"
    t.string   "value"
    t.string   "note"
    t.integer  "created_by",  :default => 1
    t.integer  "updated_by",  :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bondsman_phones", ["bondsman_id"], :name => "index_bondsman_phones_on_bondsman_id"
  add_index "bondsman_phones", ["created_by"], :name => "index_bondsman_phones_on_created_by"
  add_index "bondsman_phones", ["label_id"], :name => "index_bondsman_phones_on_label_id"
  add_index "bondsman_phones", ["updated_by"], :name => "index_bondsman_phones_on_updated_by"

  create_table "bondsmen", :force => true do |t|
    t.string   "name"
    t.string   "street"
    t.string   "city"
    t.integer  "state_id"
    t.string   "zip"
    t.string   "company"
    t.boolean  "disabled"
    t.integer  "created_by", :default => 1
    t.integer  "updated_by", :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bondsmen", ["created_by"], :name => "index_bondsmen_on_created_by"
  add_index "bondsmen", ["state_id"], :name => "index_bondsmen_on_state_id"
  add_index "bondsmen", ["updated_by"], :name => "index_bondsmen_on_updated_by"

  create_table "booking_logs", :force => true do |t|
    t.integer  "booking_id"
    t.datetime "start_datetime"
    t.datetime "stop_datetime"
    t.string   "start_officer"
    t.string   "start_officer_badge"
    t.string   "start_officer_unit"
    t.string   "stop_officer"
    t.string   "stop_officer_badge"
    t.string   "stop_officer_unit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "booking_logs", ["booking_id"], :name => "index_booking_logs_on_booking_id"

  create_table "bookings", :force => true do |t|
    t.integer  "person_id"
    t.decimal  "cash_at_booking",       :precision => 12, :scale => 2, :default => 0.0
    t.string   "cash_receipt"
    t.string   "booking_officer"
    t.string   "booking_officer_unit"
    t.string   "booking_officer_badge"
    t.integer  "booking_officer_id"
    t.date     "booking_date"
    t.string   "booking_time"
    t.integer  "facility_id"
    t.integer  "cell_id"
    t.text     "remarks"
    t.string   "phone_called"
    t.string   "attorney"
    t.boolean  "intoxicated"
    t.date     "sched_release_date"
    t.date     "release_date"
    t.string   "release_time"
    t.boolean  "good_time",                                            :default => true
    t.boolean  "disable_timers",                                       :default => false
    t.boolean  "afis"
    t.string   "release_officer"
    t.string   "release_officer_unit"
    t.string   "release_officer_badge"
    t.integer  "release_officer_id"
    t.boolean  "legacy",                                               :default => false
    t.integer  "created_by",                                           :default => 1
    t.integer  "updated_by",                                           :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "released_because_id"
  end

  add_index "bookings", ["cell_id"], :name => "index_bookings_on_cell_id"
  add_index "bookings", ["created_by"], :name => "index_bookings_on_created_by"
  add_index "bookings", ["facility_id"], :name => "index_bookings_on_facility_id"
  add_index "bookings", ["person_id"], :name => "index_bookings_on_person_id"
  add_index "bookings", ["released_because_id"], :name => "index_bookings_on_released_because_id"
  add_index "bookings", ["updated_by"], :name => "index_bookings_on_updated_by"

  create_table "bug_comments", :force => true do |t|
    t.integer  "bug_id"
    t.integer  "user_id"
    t.text     "comments"
    t.integer  "created_by", :default => 1
    t.integer  "updated_by", :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bug_comments", ["bug_id"], :name => "index_bug_comments_on_bug_id"
  add_index "bug_comments", ["created_by"], :name => "index_bug_comments_on_created_by"
  add_index "bug_comments", ["updated_by"], :name => "index_bug_comments_on_updated_by"
  add_index "bug_comments", ["user_id"], :name => "index_bug_comments_on_user_id"

  create_table "bugs", :force => true do |t|
    t.integer  "reporter_id"
    t.date     "reported_date"
    t.string   "reported_time"
    t.integer  "assigned_id"
    t.integer  "admin_id"
    t.integer  "status_id"
    t.integer  "resolution_id"
    t.date     "resolution_date"
    t.string   "resolution_time"
    t.integer  "priority_id"
    t.string   "summary"
    t.text     "details"
    t.integer  "created_by",      :default => 1
    t.integer  "updated_by",      :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bugs", ["admin_id"], :name => "index_bugs_on_admin_id"
  add_index "bugs", ["assigned_id"], :name => "index_bugs_on_assigned_id"
  add_index "bugs", ["created_by"], :name => "index_bugs_on_created_by"
  add_index "bugs", ["priority_id"], :name => "index_bugs_on_priority_id"
  add_index "bugs", ["reporter_id"], :name => "index_bugs_on_reporter_id"
  add_index "bugs", ["resolution_id"], :name => "index_bugs_on_resolution_id"
  add_index "bugs", ["status_id"], :name => "index_bugs_on_status_id"
  add_index "bugs", ["updated_by"], :name => "index_bugs_on_updated_by"

  create_table "call_subjects", :force => true do |t|
    t.integer  "call_id"
    t.integer  "subject_type_id"
    t.string   "name"
    t.string   "address1"
    t.string   "address2"
    t.string   "home_phone"
    t.string   "cell_phone"
    t.integer  "created_by",      :default => 1
    t.integer  "updated_by",      :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "work_phone"
  end

  add_index "call_subjects", ["call_id"], :name => "index_call_subjects_on_call_id"
  add_index "call_subjects", ["created_by"], :name => "index_call_subjects_on_created_by"
  add_index "call_subjects", ["subject_type_id"], :name => "index_call_subjects_on_subject_type_id"
  add_index "call_subjects", ["updated_by"], :name => "index_call_subjects_on_updated_by"

  create_table "call_units", :force => true do |t|
    t.integer  "call_id"
    t.string   "officer"
    t.string   "officer_unit"
    t.string   "officer_badge"
    t.integer  "officer_id"
    t.integer  "created_by",    :default => 1
    t.integer  "updated_by",    :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "call_units", ["call_id"], :name => "index_call_units_on_call_id"
  add_index "call_units", ["created_by"], :name => "index_call_units_on_created_by"
  add_index "call_units", ["updated_by"], :name => "index_call_units_on_updated_by"

  create_table "calls", :force => true do |t|
    t.string   "case_no"
    t.date     "call_date"
    t.string   "call_time"
    t.integer  "signal_code_id"
    t.integer  "received_via_id"
    t.string   "received_by"
    t.string   "received_by_unit"
    t.string   "received_by_badge"
    t.integer  "received_by_id"
    t.text     "details"
    t.date     "dispatch_date"
    t.string   "dispatch_time"
    t.date     "arrival_date"
    t.string   "arrival_time"
    t.date     "concluded_date"
    t.string   "concluded_time"
    t.integer  "disposition_id"
    t.string   "dispatcher"
    t.string   "dispatcher_unit"
    t.string   "dispatcher_badge"
    t.integer  "dispatcher_id"
    t.string   "detective"
    t.integer  "detective_id"
    t.string   "detective_unit"
    t.string   "detective_badge"
    t.integer  "ward"
    t.integer  "district"
    t.text     "remarks"
    t.boolean  "legacy",            :default => false
    t.integer  "created_by",        :default => 1
    t.integer  "updated_by",        :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "calls", ["case_no"], :name => "index_calls_on_case_no"
  add_index "calls", ["created_by"], :name => "index_calls_on_created_by"
  add_index "calls", ["disposition_id"], :name => "index_calls_on_disposition_id"
  add_index "calls", ["received_via_id"], :name => "index_calls_on_received_via_id"
  add_index "calls", ["signal_code_id"], :name => "index_calls_on_signal_code_id"
  add_index "calls", ["updated_by"], :name => "index_calls_on_updated_by"

  create_table "case_notes", :force => true do |t|
    t.string   "case_no"
    t.datetime "note_datetime"
    t.text     "note"
    t.integer  "created_by",    :default => 1
    t.integer  "updated_by",    :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "case_notes", ["case_no"], :name => "index_case_notes_on_case_no"
  add_index "case_notes", ["created_by"], :name => "index_case_notes_on_created_by"
  add_index "case_notes", ["updated_by"], :name => "index_case_notes_on_updated_by"

  create_table "cases", :force => true do |t|
    t.string   "assigned"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "charges", :force => true do |t|
    t.integer  "d_arrest_id"
    t.integer  "c_arrest_id"
    t.integer  "o_arrest_id"
    t.integer  "citation_id"
    t.integer  "warrant_id"
    t.integer  "arrest_warrant_id"
    t.integer  "agency_id"
    t.string   "charge"
    t.integer  "count"
    t.integer  "created_by",        :default => 1
    t.integer  "updated_by",        :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "charges", ["agency_id"], :name => "index_charges_on_agency_id"
  add_index "charges", ["arrest_warrant_id"], :name => "index_charges_on_arrest_warrant_id"
  add_index "charges", ["c_arrest_id"], :name => "index_charges_on_c_arrest_id"
  add_index "charges", ["citation_id"], :name => "index_charges_on_citation_id"
  add_index "charges", ["created_by"], :name => "index_charges_on_created_by"
  add_index "charges", ["d_arrest_id"], :name => "index_charges_on_d_arrest_id"
  add_index "charges", ["o_arrest_id"], :name => "index_charges_on_o_arrest_id"
  add_index "charges", ["updated_by"], :name => "index_charges_on_updated_by"
  add_index "charges", ["warrant_id"], :name => "index_charges_on_warrant_id"

  create_table "citations", :force => true do |t|
    t.date     "citation_date"
    t.integer  "offense_id"
    t.string   "citation_time"
    t.integer  "person_id"
    t.integer  "officer_id"
    t.string   "officer"
    t.string   "officer_unit"
    t.string   "officer_badge"
    t.string   "ticket_no"
    t.date     "paid_date"
    t.date     "recalled_date"
    t.integer  "created_by",    :default => 1
    t.integer  "updated_by",    :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "citations", ["created_by"], :name => "index_citations_on_created_by"
  add_index "citations", ["offense_id"], :name => "index_citations_on_offense_id"
  add_index "citations", ["person_id"], :name => "index_citations_on_person_id"
  add_index "citations", ["updated_by"], :name => "index_citations_on_updated_by"

  create_table "city_warrants", :id => false, :force => true do |t|
    t.integer "arrest_id"
    t.integer "warrant_id"
  end

  add_index "city_warrants", ["arrest_id"], :name => "index_city_warrants_on_arrest_id"
  add_index "city_warrants", ["warrant_id"], :name => "index_city_warrants_on_warrant_id"

  create_table "colorschemes", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "color_dark",        :default => "#3665A3"
    t.string   "color_light",       :default => "#D4E3F7"
    t.string   "color_alt_dark",    :default => "#886655"
    t.string   "color_alt_light",   :default => "#F0ECDB"
    t.string   "color_black",       :default => "#000000"
    t.string   "color_white",       :default => "#FFFFFF"
    t.string   "color_gray_dark",   :default => "#333333"
    t.string   "color_gray_medium", :default => "#777777"
    t.string   "color_gray_light",  :default => "#CCCCCC"
    t.string   "color_message",     :default => "#0000AA"
    t.string   "color_message_bg",  :default => "#EEEEFF"
    t.string   "color_notice",      :default => "#00AA00"
    t.string   "color_notice_bg",   :default => "#EEFFEE"
    t.string   "color_error",       :default => "#AA0000"
    t.string   "color_error_bg",    :default => "#FFEEEE"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "color_gray_bg",     :default => "#efefef"
    t.string   "color_tip_bg",      :default => "#FFFFBB"
    t.string   "color_tip_border",  :default => "#886655"
    t.string   "color_tip",         :default => "#000000"
  end

  add_index "colorschemes", ["name"], :name => "index_colorschemes_on_name"

  create_table "commissaries", :force => true do |t|
    t.integer  "person_id"
    t.date     "transaction_date"
    t.integer  "category_id"
    t.string   "officer"
    t.string   "officer_unit"
    t.string   "officer_badge"
    t.integer  "officer_id"
    t.string   "receipt_no"
    t.decimal  "deposit",          :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "withdraw",         :precision => 12, :scale => 2, :default => 0.0
    t.datetime "report_datetime"
    t.string   "memo"
    t.integer  "created_by",                                      :default => 1
    t.integer  "updated_by",                                      :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "commissaries", ["category_id"], :name => "index_commissaries_on_category_id"
  add_index "commissaries", ["created_by"], :name => "index_commissaries_on_created_by"
  add_index "commissaries", ["person_id"], :name => "index_commissaries_on_person_id"
  add_index "commissaries", ["updated_by"], :name => "index_commissaries_on_updated_by"

  create_table "commissary_facilities", :id => false, :force => true do |t|
    t.integer "commissary_item_id"
    t.integer "option_id"
  end

  add_index "commissary_facilities", ["commissary_item_id"], :name => "index_commissary_facilities_on_commissary_item_id"
  add_index "commissary_facilities", ["option_id"], :name => "index_commissary_facilities_on_option_id"

  create_table "commissary_items", :force => true do |t|
    t.string   "description"
    t.decimal  "price",       :precision => 12, :scale => 2, :default => 0.0
    t.boolean  "active",                                     :default => true
    t.integer  "created_by",                                 :default => 1
    t.integer  "updated_by",                                 :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "commissary_items", ["created_by"], :name => "index_commissary_items_on_created_by"
  add_index "commissary_items", ["updated_by"], :name => "index_commissary_items_on_updated_by"

  create_table "contacts", :force => true do |t|
    t.string   "title"
    t.string   "unit"
    t.string   "street"
    t.string   "street2"
    t.string   "city"
    t.integer  "state_id"
    t.string   "zip"
    t.string   "pri_email"
    t.string   "alt_email"
    t.string   "web_site"
    t.string   "notes"
    t.boolean  "detective",  :default => false
    t.boolean  "officer",    :default => false
    t.string   "badge_no"
    t.boolean  "physician",  :default => false
    t.boolean  "pharmacy",   :default => false
    t.boolean  "active",     :default => false
    t.integer  "created_by", :default => 1
    t.integer  "updated_by", :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "legacy",     :default => false
    t.boolean  "business",   :default => false
    t.string   "fullname"
    t.integer  "agency_id"
    t.boolean  "printable",  :default => true
  end

  add_index "contacts", ["agency_id"], :name => "index_contacts_on_agency_id"
  add_index "contacts", ["created_by"], :name => "index_contacts_on_created_by"
  add_index "contacts", ["state_id"], :name => "index_contacts_on_state_id"
  add_index "contacts", ["updated_by"], :name => "index_contacts_on_updated_by"

  create_table "contacts_investigations", :id => false, :force => true do |t|
    t.integer "contact_id"
    t.integer "investigation_id"
  end

  add_index "contacts_investigations", ["contact_id"], :name => "index_contacts_investigations_on_contact_id"
  add_index "contacts_investigations", ["investigation_id"], :name => "index_contacts_investigations_on_investigation_id"

  create_table "courts", :force => true do |t|
    t.integer  "docket_id"
    t.integer  "judge_id"
    t.date     "court_date"
    t.integer  "court_type_id"
    t.date     "next_court_date"
    t.integer  "next_court_type_id"
    t.integer  "next_judge_id"
    t.boolean  "needs_state_report",     :default => false
    t.boolean  "bench_warrant_issued",   :default => false
    t.boolean  "bond_forfeiture_issued", :default => false
    t.boolean  "is_upset_wo_refix",      :default => false
    t.boolean  "is_refixed",             :default => false
    t.string   "refix_reason"
    t.boolean  "psi",                    :default => false
    t.integer  "plea_type_id"
    t.boolean  "is_dismissed",           :default => false
    t.boolean  "is_overturned",          :default => false
    t.date     "overturned_date"
    t.integer  "created_by",             :default => 1
    t.integer  "updated_by",             :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "processed",              :default => false
    t.integer  "docket_type_id"
    t.integer  "next_docket_type_id"
    t.boolean  "fines_paid",             :default => false
    t.text     "notes"
    t.text     "remarks"
  end

  add_index "courts", ["court_type_id"], :name => "index_courts_on_court_type_id"
  add_index "courts", ["created_by"], :name => "index_courts_on_created_by"
  add_index "courts", ["docket_id"], :name => "index_courts_on_docket_id"
  add_index "courts", ["docket_type_id"], :name => "index_courts_on_docket_type_id"
  add_index "courts", ["judge_id"], :name => "index_courts_on_judge_id"
  add_index "courts", ["next_court_type_id"], :name => "index_courts_on_next_court_type_id"
  add_index "courts", ["next_docket_type_id"], :name => "index_courts_on_next_docket_type_id"
  add_index "courts", ["next_judge_id"], :name => "index_courts_on_next_judge_id"
  add_index "courts", ["plea_type_id"], :name => "index_courts_on_plea_type_id"
  add_index "courts", ["updated_by"], :name => "index_courts_on_updated_by"

  create_table "crime_victims", :id => false, :force => true do |t|
    t.integer "person_id"
    t.integer "invest_crime_id"
  end

  add_index "crime_victims", ["invest_crime_id"], :name => "index_crime_victims_on_invest_crime_id"
  add_index "crime_victims", ["person_id"], :name => "index_crime_victims_on_person_id"

  create_table "crime_witnesses", :id => false, :force => true do |t|
    t.integer "person_id"
    t.integer "invest_crime_id"
  end

  add_index "crime_witnesses", ["invest_crime_id"], :name => "index_crime_witnesses_on_invest_crime_id"
  add_index "crime_witnesses", ["person_id"], :name => "index_crime_witnesses_on_person_id"

  create_table "da_charges", :force => true do |t|
    t.integer  "docket_id"
    t.integer  "charge_id"
    t.integer  "arrest_id"
    t.integer  "count",         :default => 1
    t.string   "charge"
    t.boolean  "dropped_by_da", :default => false
    t.boolean  "processed",     :default => false
    t.integer  "created_by",    :default => 1
    t.integer  "updated_by",    :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "info_only",     :default => false
  end

  add_index "da_charges", ["arrest_id"], :name => "index_da_charges_on_arrest_id"
  add_index "da_charges", ["charge_id"], :name => "index_da_charges_on_charge_id"
  add_index "da_charges", ["created_by"], :name => "index_da_charges_on_created_by"
  add_index "da_charges", ["docket_id"], :name => "index_da_charges_on_docket_id"
  add_index "da_charges", ["updated_by"], :name => "index_da_charges_on_updated_by"

  create_table "district_warrants", :id => false, :force => true do |t|
    t.integer "arrest_id"
    t.integer "warrant_id"
  end

  add_index "district_warrants", ["arrest_id"], :name => "index_district_warrants_on_arrest_id"
  add_index "district_warrants", ["warrant_id"], :name => "index_district_warrants_on_warrant_id"

  create_table "dockets", :force => true do |t|
    t.string   "docket_no"
    t.integer  "person_id"
    t.date     "state_report_date"
    t.boolean  "processed",         :default => false
    t.integer  "created_by",        :default => 1
    t.integer  "updated_by",        :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "notes"
    t.text     "remarks"
    t.boolean  "misc",              :default => false
  end

  add_index "dockets", ["created_by"], :name => "index_dockets_on_created_by"
  add_index "dockets", ["docket_no"], :name => "index_dockets_on_docket_no"
  add_index "dockets", ["person_id"], :name => "index_dockets_on_person_id"
  add_index "dockets", ["updated_by"], :name => "index_dockets_on_updated_by"

  create_table "dockets_revocations", :id => false, :force => true do |t|
    t.integer "docket_id"
    t.integer "revocation_id"
  end

  add_index "dockets_revocations", ["docket_id"], :name => "index_dockets_revocations_on_docket_id"
  add_index "dockets_revocations", ["revocation_id"], :name => "index_dockets_revocations_on_revocation_id"

  create_table "dockets_sentences", :id => false, :force => true do |t|
    t.integer "docket_id"
    t.integer "sentence_id"
  end

  add_index "dockets_sentences", ["docket_id"], :name => "index_dockets_sentences_on_docket_id"
  add_index "dockets_sentences", ["sentence_id"], :name => "index_dockets_sentences_on_sentence_id"

  create_table "docs", :force => true do |t|
    t.integer  "booking_id"
    t.integer  "person_id"
    t.date     "transfer_date"
    t.string   "transfer_time"
    t.integer  "transfer_id"
    t.integer  "hold_id"
    t.integer  "sentence_id"
    t.integer  "revocation_id"
    t.string   "doc_number"
    t.integer  "sentence_days",   :default => 0
    t.integer  "sentence_months", :default => 0
    t.integer  "sentence_years",  :default => 0
    t.integer  "to_serve_days",   :default => 0
    t.integer  "to_serve_months", :default => 0
    t.integer  "to_serve_years",  :default => 0
    t.string   "conviction"
    t.boolean  "billable",        :default => false
    t.date     "bill_from_date"
    t.text     "remarks"
    t.integer  "created_by",      :default => 1
    t.integer  "updated_by",      :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "problems"
    t.integer  "sentence_hours",  :default => 0
    t.integer  "to_serve_hours",  :default => 0
  end

  add_index "docs", ["booking_id"], :name => "index_docs_on_booking_id"
  add_index "docs", ["created_by"], :name => "index_docs_on_created_by"
  add_index "docs", ["hold_id"], :name => "index_docs_on_hold_id"
  add_index "docs", ["person_id"], :name => "index_docs_on_person_id"
  add_index "docs", ["revocation_id"], :name => "index_docs_on_revocation_id"
  add_index "docs", ["sentence_id"], :name => "index_docs_on_sentence_id"
  add_index "docs", ["transfer_id"], :name => "index_docs_on_transfer_id"
  add_index "docs", ["updated_by"], :name => "index_docs_on_updated_by"

  create_table "events", :force => true do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.string   "name"
    t.text     "details"
    t.boolean  "private",          :default => false
    t.integer  "investigation_id"
    t.integer  "owned_by"
    t.integer  "created_by",       :default => 1
    t.integer  "updated_by",       :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "events", ["created_by"], :name => "index_events_on_created_by"
  add_index "events", ["end_date"], :name => "index_events_on_end_date"
  add_index "events", ["investigation_id"], :name => "index_events_on_investigation_id"
  add_index "events", ["owned_by"], :name => "index_events_on_owned_by"
  add_index "events", ["private"], :name => "index_events_on_private"
  add_index "events", ["start_date"], :name => "index_events_on_start_date"
  add_index "events", ["updated_by"], :name => "index_events_on_updated_by"

  create_table "evidence_locations", :force => true do |t|
    t.integer  "evidence_id"
    t.string   "location_from"
    t.string   "location_to"
    t.string   "officer"
    t.string   "officer_unit"
    t.string   "officer_badge"
    t.integer  "officer_id"
    t.date     "storage_date"
    t.string   "storage_time"
    t.text     "remarks"
    t.integer  "created_by",    :default => 1
    t.integer  "updated_by",    :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "evidence_locations", ["created_by"], :name => "index_evidence_locations_on_created_by"
  add_index "evidence_locations", ["evidence_id"], :name => "index_evidence_locations_on_evidence_id"
  add_index "evidence_locations", ["updated_by"], :name => "index_evidence_locations_on_updated_by"

  create_table "evidences", :force => true do |t|
    t.integer  "offense_id"
    t.string   "evidence_number"
    t.string   "case_no"
    t.text     "description"
    t.integer  "owner_id"
    t.text     "remarks"
    t.string   "evidence_photo_file_name"
    t.string   "evidence_photo_content_type"
    t.integer  "evidence_photo_file_size"
    t.datetime "evidence_photo_updated_at"
    t.integer  "investigation_id"
    t.integer  "invest_crime_id"
    t.boolean  "private",                     :default => false
    t.boolean  "legacy",                      :default => false
    t.integer  "owned_by"
    t.integer  "created_by",                  :default => 1
    t.integer  "updated_by",                  :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "dna_profile"
    t.string   "fingerprint_class"
  end

  add_index "evidences", ["case_no"], :name => "index_evidences_on_case_no"
  add_index "evidences", ["created_by"], :name => "index_evidences_on_created_by"
  add_index "evidences", ["invest_crime_id"], :name => "index_evidences_on_invest_crime_id"
  add_index "evidences", ["investigation_id"], :name => "index_evidences_on_investigation_id"
  add_index "evidences", ["offense_id"], :name => "index_evidences_on_offense_id"
  add_index "evidences", ["owned_by"], :name => "index_evidences_on_owned_by"
  add_index "evidences", ["owner_id"], :name => "index_evidences_on_owner_id"
  add_index "evidences", ["private"], :name => "index_evidences_on_private"
  add_index "evidences", ["updated_by"], :name => "index_evidences_on_updated_by"

  create_table "fax_covers", :force => true do |t|
    t.string   "name"
    t.string   "from_name"
    t.string   "from_fax"
    t.string   "from_phone"
    t.boolean  "include_notice", :default => true
    t.string   "notice_title"
    t.text     "notice_body"
    t.integer  "created_by",     :default => 1
    t.integer  "updated_by",     :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "fax_covers", ["created_by"], :name => "index_fax_covers_on_created_by"
  add_index "fax_covers", ["updated_by"], :name => "index_fax_covers_on_updated_by"

  create_table "forbid_exemptions", :id => false, :force => true do |t|
    t.integer "forbid_id"
    t.integer "person_id"
  end

  add_index "forbid_exemptions", ["forbid_id"], :name => "index_forbid_exemptions_on_forbid_id"
  add_index "forbid_exemptions", ["person_id"], :name => "index_forbid_exemptions_on_person_id"

  create_table "forbids", :force => true do |t|
    t.integer  "call_id"
    t.string   "case_no"
    t.string   "complainant"
    t.string   "comp_street"
    t.string   "comp_city_state_zip"
    t.string   "comp_phone"
    t.integer  "forbidden_id"
    t.string   "forbid_last"
    t.string   "forbid_first"
    t.string   "forbid_suffix"
    t.string   "forbid_street"
    t.string   "forbid_city_state_zip"
    t.string   "forbid_phone"
    t.date     "request_date"
    t.string   "receiving_officer"
    t.string   "receiving_officer_unit"
    t.string   "receiving_officer_badge"
    t.integer  "receiving_officer_id"
    t.date     "signed_date"
    t.string   "serving_officer"
    t.string   "serving_officer_unit"
    t.string   "serving_officer_badge"
    t.integer  "serving_officer_id"
    t.date     "recall_date"
    t.text     "details"
    t.text     "remarks"
    t.boolean  "legacy",                  :default => false
    t.integer  "created_by",              :default => 1
    t.integer  "updated_by",              :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "forbid_middle1"
    t.string   "forbid_middle2"
    t.string   "forbid_middle3"
  end

  add_index "forbids", ["call_id"], :name => "index_forbids_on_call_id"
  add_index "forbids", ["created_by"], :name => "index_forbids_on_created_by"
  add_index "forbids", ["forbid_first"], :name => "index_forbids_on_forbid_first"
  add_index "forbids", ["forbid_last"], :name => "index_forbids_on_forbid_last"
  add_index "forbids", ["forbidden_id"], :name => "index_forbids_on_forbidden_id"
  add_index "forbids", ["updated_by"], :name => "index_forbids_on_updated_by"

  create_table "forbids_offenses", :id => false, :force => true do |t|
    t.integer "forbid_id"
    t.integer "offense_id"
  end

  add_index "forbids_offenses", ["forbid_id"], :name => "index_forbids_offenses_on_forbid_id"
  add_index "forbids_offenses", ["offense_id"], :name => "index_forbids_offenses_on_offense_id"

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "permissions"
    t.integer  "created_by",  :default => 1
    t.integer  "updated_by",  :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "groups", ["created_by"], :name => "index_groups_on_created_by"
  add_index "groups", ["name"], :name => "index_groups_on_name"
  add_index "groups", ["updated_by"], :name => "index_groups_on_updated_by"

  create_table "groups_users", :id => false, :force => true do |t|
    t.integer "group_id"
    t.integer "user_id"
  end

  add_index "groups_users", ["group_id"], :name => "index_groups_users_on_group_id"
  add_index "groups_users", ["user_id"], :name => "index_groups_users_on_user_id"

  create_table "helps", :force => true do |t|
    t.string   "section"
    t.string   "page"
    t.string   "title"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "helps", ["page"], :name => "index_helps_on_page"
  add_index "helps", ["section"], :name => "index_helps_on_section"

  create_table "histories", :force => true do |t|
    t.string   "key"
    t.string   "name"
    t.integer  "type_id"
    t.text     "details"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "histories", ["key"], :name => "index_histories_on_key"
  add_index "histories", ["name"], :name => "index_histories_on_name"
  add_index "histories", ["type_id"], :name => "index_histories_on_type_id"

  create_table "hold_blocks", :id => false, :force => true do |t|
    t.integer "hold_id"
    t.integer "blocker_id"
  end

  add_index "hold_blocks", ["blocker_id"], :name => "index_hold_blocks_on_blocker_id"
  add_index "hold_blocks", ["hold_id"], :name => "index_hold_blocks_on_hold_id"

  create_table "holds", :force => true do |t|
    t.integer  "booking_id"
    t.integer  "arrest_id"
    t.integer  "probation_id"
    t.date     "hold_date"
    t.string   "hold_time"
    t.boolean  "doc",                                                   :default => false
    t.boolean  "doc_billable",                                          :default => false
    t.date     "cleared_date"
    t.string   "cleared_time"
    t.integer  "cleared_because_id"
    t.integer  "sentence_id"
    t.integer  "transfer_id"
    t.integer  "revocation_id"
    t.string   "explanation"
    t.date     "bill_from_date"
    t.integer  "hold_type_id"
    t.string   "hold_by"
    t.string   "hold_by_unit"
    t.string   "hold_by_badge"
    t.integer  "hold_by_id"
    t.string   "cleared_by"
    t.string   "cleared_by_unit"
    t.string   "cleared_by_badge"
    t.integer  "cleared_by_id"
    t.date     "billing_report_date"
    t.string   "agency"
    t.integer  "agency_id"
    t.string   "transfer_officer"
    t.string   "transfer_officer_unit"
    t.string   "transfer_officer_badge"
    t.integer  "transfer_officer_id"
    t.decimal  "fines",                  :precision => 12, :scale => 2, :default => 0.0
    t.integer  "hours_served",                                          :default => 0
    t.float    "hours_goodtime",                                        :default => 0.0
    t.datetime "datetime_counted"
    t.text     "remarks"
    t.boolean  "legacy",                                                :default => false
    t.integer  "created_by",                                            :default => 1
    t.integer  "updated_by",                                            :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "billed_retroactive"
    t.integer  "hours_total",                                           :default => 0
    t.integer  "hours_time_served",                                     :default => 0
    t.integer  "temp_release_reason_id"
    t.boolean  "deny_goodtime",                                         :default => false
    t.boolean  "other_billable",                                        :default => false
  end

  add_index "holds", ["arrest_id"], :name => "index_holds_on_arrest_id"
  add_index "holds", ["booking_id"], :name => "index_holds_on_booking_id"
  add_index "holds", ["cleared_because_id"], :name => "index_holds_on_cleared_because_id"
  add_index "holds", ["created_by"], :name => "index_holds_on_created_by"
  add_index "holds", ["hold_type_id"], :name => "index_holds_on_hold_type_id"
  add_index "holds", ["probation_id"], :name => "index_holds_on_probation_id"
  add_index "holds", ["revocation_id"], :name => "index_holds_on_revocation_id"
  add_index "holds", ["sentence_id"], :name => "index_holds_on_sentence_id"
  add_index "holds", ["temp_release_reason_id"], :name => "index_holds_on_temp_release_reason_id"
  add_index "holds", ["transfer_id"], :name => "index_holds_on_transfer_id"
  add_index "holds", ["updated_by"], :name => "index_holds_on_updated_by"

  create_table "invest_cases", :force => true do |t|
    t.integer  "investigation_id"
    t.string   "case_no"
    t.boolean  "private",          :default => true
    t.text     "remarks"
    t.integer  "invest_crime_id"
    t.integer  "owned_by"
    t.integer  "created_by",       :default => 1
    t.integer  "updated_by",       :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_cases", ["case_no"], :name => "index_invest_cases_on_case_no"
  add_index "invest_cases", ["created_by"], :name => "index_invest_cases_on_created_by"
  add_index "invest_cases", ["invest_crime_id"], :name => "index_invest_cases_on_invest_crime_id"
  add_index "invest_cases", ["investigation_id"], :name => "index_invest_cases_on_investigation_id"
  add_index "invest_cases", ["owned_by"], :name => "index_invest_cases_on_owned_by"
  add_index "invest_cases", ["private"], :name => "index_invest_cases_on_private"
  add_index "invest_cases", ["updated_by"], :name => "index_invest_cases_on_updated_by"

  create_table "invest_crime_sources", :id => false, :force => true do |t|
    t.integer "person_id"
    t.integer "invest_crime_id"
  end

  add_index "invest_crime_sources", ["invest_crime_id"], :name => "index_invest_crime_sources_on_invest_crime_id"
  add_index "invest_crime_sources", ["person_id"], :name => "index_invest_crime_sources_on_person_id"

  create_table "invest_crimes", :force => true do |t|
    t.integer  "investigation_id"
    t.boolean  "private",                :default => true
    t.string   "motive"
    t.string   "location"
    t.boolean  "occupied"
    t.string   "entry_gained"
    t.string   "weapons_used"
    t.integer  "victim_relationship_id"
    t.text     "facts_prior"
    t.text     "facts_after"
    t.text     "targets"
    t.string   "security"
    t.string   "impersonated"
    t.text     "actions_taken"
    t.text     "threats_made"
    t.text     "details"
    t.text     "remarks"
    t.integer  "owned_by"
    t.integer  "created_by",             :default => 1
    t.integer  "updated_by",             :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crimes", ["created_by"], :name => "index_invest_crimes_on_created_by"
  add_index "invest_crimes", ["investigation_id"], :name => "index_invest_crimes_on_investigation_id"
  add_index "invest_crimes", ["owned_by"], :name => "index_invest_crimes_on_owned_by"
  add_index "invest_crimes", ["updated_by"], :name => "index_invest_crimes_on_updated_by"
  add_index "invest_crimes", ["victim_relationship_id"], :name => "index_invest_crimes_on_victim_relationship_id"

  create_table "invest_criminals", :force => true do |t|
    t.integer  "investigation_id"
    t.integer  "invest_crime_id"
    t.integer  "invest_report_id"
    t.integer  "person_id"
    t.string   "full_name"
    t.string   "description"
    t.string   "height"
    t.string   "weight"
    t.string   "shoes"
    t.string   "fingerprint_class"
    t.string   "dna_profile"
    t.boolean  "private",           :default => true
    t.text     "remarks"
    t.integer  "owned_by"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_criminals", ["created_by"], :name => "index_invest_criminals_on_created_by"
  add_index "invest_criminals", ["invest_crime_id"], :name => "index_invest_criminals_on_invest_crime_id"
  add_index "invest_criminals", ["invest_report_id"], :name => "index_invest_criminals_on_invest_report_id"
  add_index "invest_criminals", ["investigation_id"], :name => "index_invest_criminals_on_investigation_id"
  add_index "invest_criminals", ["owned_by"], :name => "index_invest_criminals_on_owned_by"
  add_index "invest_criminals", ["person_id"], :name => "index_invest_criminals_on_person_id"
  add_index "invest_criminals", ["private"], :name => "index_invest_criminals_on_private"
  add_index "invest_criminals", ["updated_by"], :name => "index_invest_criminals_on_updated_by"

  create_table "invest_photos", :force => true do |t|
    t.integer  "investigation_id"
    t.integer  "invest_crime_id"
    t.integer  "invest_report_id"
    t.integer  "invest_criminal_id"
    t.integer  "invest_vehicle_id"
    t.string   "invest_photo_file_name"
    t.string   "invest_photo_content_type"
    t.integer  "invest_photo_file_size"
    t.datetime "invest_photo_updated_at"
    t.boolean  "private",                   :default => true
    t.string   "description"
    t.date     "date_taken"
    t.string   "location_taken"
    t.string   "taken_by"
    t.integer  "taken_by_id"
    t.string   "taken_by_unit"
    t.string   "taken_by_badge"
    t.text     "remarks"
    t.integer  "owned_by"
    t.integer  "created_by",                :default => 1
    t.integer  "updated_by",                :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_photos", ["created_by"], :name => "index_invest_photos_on_created_by"
  add_index "invest_photos", ["invest_crime_id"], :name => "index_invest_photos_on_invest_crime_id"
  add_index "invest_photos", ["invest_criminal_id"], :name => "index_invest_photos_on_invest_criminal_id"
  add_index "invest_photos", ["invest_report_id"], :name => "index_invest_photos_on_invest_report_id"
  add_index "invest_photos", ["invest_vehicle_id"], :name => "index_invest_photos_on_invest_vehicle_id"
  add_index "invest_photos", ["investigation_id"], :name => "index_invest_photos_on_investigation_id"
  add_index "invest_photos", ["owned_by"], :name => "index_invest_photos_on_owned_by"
  add_index "invest_photos", ["private"], :name => "index_invest_photos_on_private"
  add_index "invest_photos", ["updated_by"], :name => "index_invest_photos_on_updated_by"

  create_table "invest_report_sources", :id => false, :force => true do |t|
    t.integer "person_id"
    t.integer "invest_report_id"
  end

  add_index "invest_report_sources", ["invest_report_id"], :name => "index_invest_report_sources_on_invest_report_id"
  add_index "invest_report_sources", ["person_id"], :name => "index_invest_report_sources_on_person_id"

  create_table "invest_reports", :force => true do |t|
    t.integer  "investigation_id"
    t.integer  "invest_crime_id"
    t.date     "report_date"
    t.string   "report_time"
    t.text     "details"
    t.text     "remarks"
    t.boolean  "private",          :default => true
    t.integer  "owned_by"
    t.integer  "created_by",       :default => 1
    t.integer  "updated_by",       :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_reports", ["created_by"], :name => "index_invest_reports_on_created_by"
  add_index "invest_reports", ["invest_crime_id"], :name => "index_invest_reports_on_invest_crime_id"
  add_index "invest_reports", ["investigation_id"], :name => "index_invest_reports_on_investigation_id"
  add_index "invest_reports", ["owned_by"], :name => "index_invest_reports_on_owned_by"
  add_index "invest_reports", ["private"], :name => "index_invest_reports_on_private"
  add_index "invest_reports", ["updated_by"], :name => "index_invest_reports_on_updated_by"

  create_table "invest_sources", :id => false, :force => true do |t|
    t.integer "person_id"
    t.integer "investigation_id"
  end

  add_index "invest_sources", ["investigation_id"], :name => "index_invest_sources_on_investigation_id"
  add_index "invest_sources", ["person_id"], :name => "index_invest_sources_on_person_id"

  create_table "invest_vehicles", :force => true do |t|
    t.integer  "investigation_id"
    t.integer  "invest_criminal_id"
    t.integer  "invest_crime_id"
    t.integer  "invest_report_id"
    t.boolean  "private",            :default => true
    t.integer  "owner_id"
    t.string   "make"
    t.string   "model"
    t.string   "vin"
    t.string   "license"
    t.integer  "license_state_id"
    t.string   "color"
    t.string   "tires"
    t.string   "model_year"
    t.integer  "vehicle_type_id"
    t.text     "description"
    t.text     "remarks"
    t.integer  "owned_by"
    t.integer  "created_by",         :default => 1
    t.integer  "updated_by",         :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_vehicles", ["created_by"], :name => "index_invest_vehicles_on_created_by"
  add_index "invest_vehicles", ["invest_crime_id"], :name => "index_invest_vehicles_on_invest_crime_id"
  add_index "invest_vehicles", ["invest_criminal_id"], :name => "index_invest_vehicles_on_invest_criminal_id"
  add_index "invest_vehicles", ["invest_report_id"], :name => "index_invest_vehicles_on_invest_report_id"
  add_index "invest_vehicles", ["investigation_id"], :name => "index_invest_vehicles_on_investigation_id"
  add_index "invest_vehicles", ["license_state_id"], :name => "index_invest_vehicles_on_license_state_id"
  add_index "invest_vehicles", ["owned_by"], :name => "index_invest_vehicles_on_owned_by"
  add_index "invest_vehicles", ["owner_id"], :name => "index_invest_vehicles_on_owner_id"
  add_index "invest_vehicles", ["private"], :name => "index_invest_vehicles_on_private"
  add_index "invest_vehicles", ["updated_by"], :name => "index_invest_vehicles_on_updated_by"
  add_index "invest_vehicles", ["vehicle_type_id"], :name => "index_invest_vehicles_on_vehicle_type_id"

  create_table "invest_victims", :id => false, :force => true do |t|
    t.integer "person_id"
    t.integer "investigation_id"
  end

  add_index "invest_victims", ["investigation_id"], :name => "index_invest_victims_on_investigation_id"
  add_index "invest_victims", ["person_id"], :name => "index_invest_victims_on_person_id"

  create_table "invest_witnesses", :id => false, :force => true do |t|
    t.integer "person_id"
    t.integer "investigation_id"
  end

  add_index "invest_witnesses", ["investigation_id"], :name => "index_invest_witnesses_on_investigation_id"
  add_index "invest_witnesses", ["person_id"], :name => "index_invest_witnesses_on_person_id"

  create_table "investigations", :force => true do |t|
    t.date     "investigation_date"
    t.string   "investigation_time"
    t.string   "detective"
    t.integer  "detective_id"
    t.string   "detective_badge"
    t.string   "detective_unit"
    t.boolean  "private",            :default => true
    t.integer  "status_id"
    t.text     "details"
    t.text     "remarks"
    t.integer  "owned_by"
    t.integer  "created_by",         :default => 1
    t.integer  "updated_by",         :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "investigations", ["created_by"], :name => "index_investigations_on_created_by"
  add_index "investigations", ["detective_id"], :name => "index_investigations_on_detective_id"
  add_index "investigations", ["owned_by"], :name => "index_investigations_on_owned_by"
  add_index "investigations", ["private"], :name => "index_investigations_on_private"
  add_index "investigations", ["status_id"], :name => "index_investigations_on_status_id"
  add_index "investigations", ["updated_by"], :name => "index_investigations_on_updated_by"

  create_table "jail1s", :force => true do |t|
    t.integer  "person_id"
    t.decimal  "cash_at_booking",       :precision => 12, :scale => 2, :default => 0.0
    t.string   "cash_receipt"
    t.string   "booking_officer"
    t.string   "booking_officer_unit"
    t.date     "booking_date"
    t.string   "booking_time"
    t.integer  "cell_id"
    t.text     "remarks"
    t.string   "phone_called"
    t.boolean  "intoxicated"
    t.date     "sched_release_date"
    t.date     "release_date"
    t.string   "release_time"
    t.boolean  "afis"
    t.string   "release_officer"
    t.string   "release_officer_unit"
    t.string   "fingerprint_class"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "booking_officer_id"
    t.string   "booking_officer_badge"
    t.integer  "release_officer_id"
    t.string   "release_officer_badge"
    t.boolean  "good_time",                                            :default => true
    t.boolean  "disable_timers",                                       :default => false
    t.string   "dna_profile"
    t.string   "attorney"
    t.boolean  "legacy",                                               :default => false
    t.integer  "released_because_id"
  end

  add_index "jail1s", ["booking_date"], :name => "index_jail1s_on_booking_date"
  add_index "jail1s", ["cell_id"], :name => "index_jail1s_on_cell_id"
  add_index "jail1s", ["created_by"], :name => "index_jail1s_on_created_by"
  add_index "jail1s", ["fingerprint_class"], :name => "index_jail1s_on_fingerprint_class"
  add_index "jail1s", ["person_id"], :name => "index_jail1s_on_person_id"
  add_index "jail1s", ["released_because_id"], :name => "index_jail1s_on_released_because_id"
  add_index "jail1s", ["updated_by"], :name => "index_jail1s_on_updated_by"

  create_table "jail2s", :force => true do |t|
    t.integer  "person_id"
    t.decimal  "cash_at_booking",       :precision => 12, :scale => 2, :default => 0.0
    t.string   "cash_receipt"
    t.string   "booking_officer"
    t.string   "booking_officer_unit"
    t.date     "booking_date"
    t.string   "booking_time"
    t.integer  "cell_id"
    t.text     "remarks"
    t.string   "phone_called"
    t.boolean  "intoxicated"
    t.date     "sched_release_date"
    t.date     "release_date"
    t.string   "release_time"
    t.boolean  "afis"
    t.string   "release_officer"
    t.string   "release_officer_unit"
    t.string   "fingerprint_class"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "booking_officer_id"
    t.string   "booking_officer_badge"
    t.integer  "release_officer_id"
    t.string   "release_officer_badge"
    t.boolean  "good_time",                                            :default => true
    t.boolean  "disable_timers",                                       :default => false
    t.string   "dna_profile"
    t.string   "attorney"
    t.boolean  "legacy",                                               :default => false
    t.integer  "released_because_id"
  end

  add_index "jail2s", ["booking_date"], :name => "index_jail2s_on_booking_date"
  add_index "jail2s", ["cell_id"], :name => "index_jail2s_on_cell_id"
  add_index "jail2s", ["created_by"], :name => "index_jail2s_on_created_by"
  add_index "jail2s", ["fingerprint_class"], :name => "index_jail2s_on_fingerprint_class"
  add_index "jail2s", ["person_id"], :name => "index_jail2s_on_person_id"
  add_index "jail2s", ["released_because_id"], :name => "index_jail2s_on_released_because_id"
  add_index "jail2s", ["updated_by"], :name => "index_jail2s_on_updated_by"

  create_table "jail3s", :force => true do |t|
    t.integer  "person_id"
    t.decimal  "cash_at_booking",       :precision => 12, :scale => 2, :default => 0.0
    t.string   "cash_receipt"
    t.string   "booking_officer"
    t.string   "booking_officer_unit"
    t.date     "booking_date"
    t.string   "booking_time"
    t.integer  "cell_id"
    t.text     "remarks"
    t.string   "phone_called"
    t.boolean  "intoxicated"
    t.date     "sched_release_date"
    t.date     "release_date"
    t.string   "release_time"
    t.boolean  "afis"
    t.string   "release_officer"
    t.string   "release_officer_unit"
    t.string   "fingerprint_class"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "booking_officer_id"
    t.string   "booking_officer_badge"
    t.integer  "release_officer_id"
    t.string   "release_officer_badge"
    t.boolean  "good_time",                                            :default => true
    t.boolean  "disable_timers",                                       :default => false
    t.string   "dna_profile"
    t.string   "attorney"
    t.boolean  "legacy",                                               :default => false
    t.integer  "released_because_id"
  end

  add_index "jail3s", ["booking_date"], :name => "index_jail3s_on_booking_date"
  add_index "jail3s", ["cell_id"], :name => "index_jail3s_on_cell_id"
  add_index "jail3s", ["created_by"], :name => "index_jail3s_on_created_by"
  add_index "jail3s", ["fingerprint_class"], :name => "index_jail3s_on_fingerprint_class"
  add_index "jail3s", ["person_id"], :name => "index_jail3s_on_person_id"
  add_index "jail3s", ["released_because_id"], :name => "index_jail3s_on_released_because_id"
  add_index "jail3s", ["updated_by"], :name => "index_jail3s_on_updated_by"

  create_table "marks", :force => true do |t|
    t.integer  "person_id"
    t.integer  "location_id"
    t.string   "description"
    t.integer  "created_by",  :default => 1
    t.integer  "updated_by",  :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "marks", ["created_by"], :name => "index_marks_on_created_by"
  add_index "marks", ["location_id"], :name => "index_marks_on_location_id"
  add_index "marks", ["person_id"], :name => "index_marks_on_person_id"
  add_index "marks", ["updated_by"], :name => "index_marks_on_updated_by"

  create_table "med_schedules", :force => true do |t|
    t.integer  "medication_id"
    t.string   "dose"
    t.string   "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "med_schedules", ["medication_id"], :name => "index_med_schedules_on_medication_id"
  add_index "med_schedules", ["time"], :name => "index_med_schedules_on_time"

  create_table "medicals", :force => true do |t|
    t.integer  "booking_id"
    t.date     "screen_date"
    t.string   "screen_time"
    t.string   "arrest_injuries"
    t.string   "officer"
    t.string   "officer_unit"
    t.string   "officer_badge"
    t.integer  "officer_id"
    t.string   "disposition"
    t.string   "rec_med_tmnt"
    t.string   "rec_dental_tmnt"
    t.string   "rec_mental_tmnt"
    t.string   "current_meds"
    t.string   "allergies"
    t.string   "injuries"
    t.string   "infestation"
    t.string   "alcohol"
    t.string   "drug"
    t.string   "withdrawal"
    t.string   "attempt_suicide"
    t.string   "suicide_risk"
    t.string   "danger"
    t.string   "pregnant"
    t.string   "deformities"
    t.boolean  "heart_disease"
    t.boolean  "blood_pressure"
    t.boolean  "diabetes"
    t.boolean  "epilepsy"
    t.boolean  "hepatitis"
    t.boolean  "hiv"
    t.boolean  "tb"
    t.boolean  "ulcers"
    t.boolean  "venereal"
    t.integer  "created_by",      :default => 1
    t.integer  "updated_by",      :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "medicals", ["booking_id"], :name => "index_medicals_on_booking_id"
  add_index "medicals", ["created_by"], :name => "index_medicals_on_created_by"
  add_index "medicals", ["updated_by"], :name => "index_medicals_on_updated_by"

  create_table "medications", :force => true do |t|
    t.integer  "person_id"
    t.integer  "prescription_no"
    t.integer  "physician_id"
    t.integer  "pharmacy_id"
    t.string   "drug_name"
    t.string   "dosage"
    t.string   "warnings"
    t.integer  "created_by",      :default => 1
    t.integer  "updated_by",      :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "medications", ["created_by"], :name => "index_medications_on_created_by"
  add_index "medications", ["person_id"], :name => "index_medications_on_person_id"
  add_index "medications", ["pharmacy_id"], :name => "index_medications_on_pharmacy_id"
  add_index "medications", ["physician_id"], :name => "index_medications_on_physician_id"
  add_index "medications", ["prescription_no"], :name => "index_medications_on_prescription_no"
  add_index "medications", ["updated_by"], :name => "index_medications_on_updated_by"

  create_table "message_logs", :force => true do |t|
    t.boolean  "receiver_deleted"
    t.boolean  "receiver_purged"
    t.boolean  "sender_deleted"
    t.boolean  "sender_purged"
    t.datetime "read_at"
    t.integer  "receiver_id"
    t.integer  "sender_id"
    t.string   "subject"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "message_logs", ["receiver_id"], :name => "index_message_logs_on_receiver_id"
  add_index "message_logs", ["sender_id"], :name => "index_message_logs_on_sender_id"

  create_table "messages", :force => true do |t|
    t.boolean  "receiver_deleted"
    t.boolean  "receiver_purged"
    t.boolean  "sender_deleted"
    t.boolean  "sender_purged"
    t.datetime "read_at"
    t.integer  "receiver_id"
    t.integer  "sender_id"
    t.string   "subject",          :null => false
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["receiver_id"], :name => "index_messages_on_receiver_id"
  add_index "messages", ["sender_id"], :name => "index_messages_on_sender_id"

  create_table "offense_photos", :force => true do |t|
    t.integer  "offense_id"
    t.string   "case_no"
    t.string   "crime_scene_photo_file_name"
    t.string   "crime_scene_photo_content_type"
    t.integer  "crime_scene_photo_file_size"
    t.datetime "crime_scene_photo_updated_at"
    t.string   "description"
    t.date     "date_taken"
    t.string   "location_taken"
    t.string   "taken_by"
    t.string   "taken_by_unit"
    t.string   "taken_by_badge"
    t.integer  "taken_by_id"
    t.text     "remarks"
    t.integer  "created_by",                     :default => 1
    t.integer  "updated_by",                     :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offense_photos", ["case_no"], :name => "index_offense_photos_on_case_no"
  add_index "offense_photos", ["created_by"], :name => "index_offense_photos_on_created_by"
  add_index "offense_photos", ["offense_id"], :name => "index_offense_photos_on_offense_id"
  add_index "offense_photos", ["updated_by"], :name => "index_offense_photos_on_updated_by"

  create_table "offenses", :force => true do |t|
    t.integer  "call_id"
    t.string   "case_no"
    t.date     "offense_date"
    t.string   "offense_time"
    t.string   "location"
    t.text     "details"
    t.boolean  "pictures"
    t.boolean  "restitution_form"
    t.boolean  "victim_notification"
    t.boolean  "perm_to_search"
    t.boolean  "misd_summons"
    t.boolean  "fortyeight_hour"
    t.boolean  "medical_release"
    t.text     "other_documents"
    t.string   "officer"
    t.string   "officer_unit"
    t.string   "officer_badge"
    t.integer  "officer_id"
    t.text     "remarks"
    t.boolean  "legacy",              :default => false
    t.integer  "created_by",          :default => 1
    t.integer  "updated_by",          :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offenses", ["call_id"], :name => "index_offenses_on_call_id"
  add_index "offenses", ["case_no"], :name => "index_offenses_on_case_no"
  add_index "offenses", ["created_by"], :name => "index_offenses_on_created_by"
  add_index "offenses", ["updated_by"], :name => "index_offenses_on_updated_by"

  create_table "option_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "option_types", ["name"], :name => "index_option_types_on_name"

  create_table "options", :force => true do |t|
    t.integer  "option_type_id"
    t.string   "long_name"
    t.string   "short_name"
    t.boolean  "disabled",       :default => false
    t.boolean  "permanent",      :default => false
    t.integer  "created_by",     :default => 1
    t.integer  "updated_by",     :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "options", ["created_by"], :name => "index_options_on_created_by"
  add_index "options", ["long_name"], :name => "index_options_on_long_name"
  add_index "options", ["option_type_id"], :name => "index_options_on_option_type_id"
  add_index "options", ["updated_by"], :name => "index_options_on_updated_by"

  create_table "other_warrants", :id => false, :force => true do |t|
    t.integer "arrest_id"
    t.integer "warrant_id"
  end

  add_index "other_warrants", ["arrest_id"], :name => "index_other_warrants_on_arrest_id"
  add_index "other_warrants", ["warrant_id"], :name => "index_other_warrants_on_warrant_id"

  create_table "paper_customers", :force => true do |t|
    t.string   "name"
    t.string   "street"
    t.string   "city"
    t.integer  "state_id"
    t.string   "zip"
    t.string   "phone"
    t.string   "attention"
    t.text     "remarks"
    t.boolean  "disabled",                                     :default => false
    t.integer  "created_by",                                   :default => 1
    t.integer  "updated_by",                                   :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "balance_due",   :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "noservice_fee", :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "mileage_fee",   :precision => 12, :scale => 2, :default => 0.0
    t.string   "fax"
    t.string   "street2"
  end

  add_index "paper_customers", ["created_by"], :name => "index_paper_customers_on_created_by"
  add_index "paper_customers", ["name"], :name => "index_paper_customers_on_name"
  add_index "paper_customers", ["state_id"], :name => "index_paper_customers_on_state_id"
  add_index "paper_customers", ["updated_by"], :name => "index_paper_customers_on_updated_by"

  create_table "paper_payments", :force => true do |t|
    t.integer  "paper_id"
    t.integer  "paper_customer_id"
    t.date     "transaction_date"
    t.string   "officer"
    t.string   "officer_unit"
    t.string   "officer_badge"
    t.integer  "officer_id"
    t.string   "receipt_no"
    t.decimal  "payment",             :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "charge",              :precision => 12, :scale => 2, :default => 0.0
    t.datetime "report_datetime"
    t.string   "memo"
    t.integer  "created_by",                                         :default => 1
    t.integer  "updated_by",                                         :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "payment_method"
    t.boolean  "trans_not_deletable",                                :default => false
  end

  add_index "paper_payments", ["created_by"], :name => "index_paper_payments_on_created_by"
  add_index "paper_payments", ["paper_customer_id"], :name => "index_paper_payments_on_paper_customer_id"
  add_index "paper_payments", ["paper_id"], :name => "index_paper_payments_on_paper_id"
  add_index "paper_payments", ["updated_by"], :name => "index_paper_payments_on_updated_by"

  create_table "paper_types", :force => true do |t|
    t.string   "name"
    t.decimal  "cost",       :precision => 12, :scale => 2, :default => 0.0
    t.boolean  "disabled",                                  :default => false
    t.integer  "created_by",                                :default => 1
    t.integer  "updated_by",                                :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "paper_types", ["created_by"], :name => "index_paper_types_on_created_by"
  add_index "paper_types", ["updated_by"], :name => "index_paper_types_on_updated_by"

  create_table "papers", :force => true do |t|
    t.string   "suit_no"
    t.string   "plaintiff"
    t.string   "defendant"
    t.integer  "paper_customer_id"
    t.string   "serve_to"
    t.string   "street"
    t.string   "city"
    t.integer  "state_id"
    t.string   "zip"
    t.string   "phone"
    t.date     "received_date"
    t.date     "served_date"
    t.string   "served_time"
    t.date     "returned_date"
    t.integer  "service_type_id"
    t.integer  "noservice_type_id"
    t.string   "noservice_extra"
    t.string   "served_to"
    t.integer  "officer_id"
    t.string   "officer"
    t.string   "officer_badge"
    t.string   "officer_unit"
    t.text     "comments"
    t.boolean  "billable",                                         :default => true
    t.datetime "invoice_datetime"
    t.date     "paid_date"
    t.decimal  "invoice_total",     :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "noservice_fee",     :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "mileage_fee",       :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "service_fee",       :precision => 12, :scale => 2, :default => 0.0
    t.string   "memo"
    t.text     "remarks"
    t.integer  "created_by",                                       :default => 1
    t.integer  "updated_by",                                       :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "court_date"
    t.decimal  "mileage",           :precision => 5,  :scale => 1, :default => 0.0
    t.datetime "report_datetime"
    t.integer  "paper_type_id"
    t.boolean  "mileage_only",                                     :default => false
  end

  add_index "papers", ["created_by"], :name => "index_papers_on_created_by"
  add_index "papers", ["noservice_type_id"], :name => "index_papers_on_noservice_type_id"
  add_index "papers", ["officer_id"], :name => "index_papers_on_officer_id"
  add_index "papers", ["paper_customer_id"], :name => "index_papers_on_paper_customer_id"
  add_index "papers", ["paper_type_id"], :name => "index_papers_on_paper_type_id"
  add_index "papers", ["service_type_id"], :name => "index_papers_on_service_type_id"
  add_index "papers", ["state_id"], :name => "index_papers_on_state_id"
  add_index "papers", ["updated_by"], :name => "index_papers_on_updated_by"

  create_table "pawn_items", :force => true do |t|
    t.integer  "pawn_id"
    t.integer  "pawn_type_id"
    t.string   "model_no"
    t.string   "serial_no"
    t.string   "description"
    t.integer  "created_by",   :default => 1
    t.integer  "updated_by",   :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pawn_items", ["created_by"], :name => "index_pawn_items_on_created_by"
  add_index "pawn_items", ["pawn_id"], :name => "index_pawn_items_on_pawn_id"
  add_index "pawn_items", ["pawn_type_id"], :name => "index_pawn_items_on_pawn_type_id"
  add_index "pawn_items", ["updated_by"], :name => "index_pawn_items_on_updated_by"

  create_table "pawns", :force => true do |t|
    t.integer  "pawn_co_id"
    t.string   "full_name"
    t.string   "address1"
    t.string   "address2"
    t.string   "oln"
    t.integer  "oln_state_id"
    t.string   "ssn"
    t.string   "ticket_no"
    t.date     "ticket_date"
    t.integer  "ticket_type_id"
    t.string   "case_no"
    t.text     "remarks"
    t.boolean  "legacy",         :default => false
    t.integer  "created_by",     :default => 1
    t.integer  "updated_by",     :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pawns", ["case_no"], :name => "index_pawns_on_case_no"
  add_index "pawns", ["created_by"], :name => "index_pawns_on_created_by"
  add_index "pawns", ["full_name"], :name => "index_pawns_on_full_name"
  add_index "pawns", ["oln_state_id"], :name => "index_pawns_on_oln_state_id"
  add_index "pawns", ["pawn_co_id"], :name => "index_pawns_on_pawn_co_id"
  add_index "pawns", ["ticket_type_id"], :name => "index_pawns_on_ticket_type_id"
  add_index "pawns", ["updated_by"], :name => "index_pawns_on_updated_by"

  create_table "people", :force => true do |t|
    t.string   "lastname"
    t.string   "firstname"
    t.string   "middlename"
    t.string   "suffix"
    t.string   "aka"
    t.integer  "is_alias_for"
    t.string   "phys_street"
    t.string   "phys_city"
    t.integer  "phys_state_id"
    t.string   "phys_zip"
    t.string   "mail_street"
    t.string   "mail_city"
    t.integer  "mail_state_id"
    t.string   "mail_zip"
    t.date     "date_of_birth"
    t.string   "place_of_birth"
    t.integer  "race_id"
    t.integer  "sex"
    t.string   "fbi"
    t.string   "sid"
    t.string   "ssn"
    t.string   "oln"
    t.integer  "oln_state_id"
    t.string   "home_phone"
    t.string   "cell_phone"
    t.string   "email"
    t.integer  "build_id"
    t.integer  "height_ft"
    t.integer  "height_in"
    t.integer  "weight"
    t.integer  "hair_color_id"
    t.integer  "hair_type_id"
    t.integer  "eye_color_id"
    t.boolean  "glasses"
    t.string   "shoe_size"
    t.integer  "complexion_id"
    t.integer  "facial_hair_id"
    t.string   "occupation"
    t.string   "employer"
    t.string   "emergency_contact"
    t.integer  "em_relationship_id"
    t.string   "emergency_address"
    t.string   "emergency_phone"
    t.string   "med_allergies"
    t.boolean  "hepatitis"
    t.boolean  "hiv"
    t.boolean  "tb"
    t.boolean  "deceased"
    t.date     "dna_swab_date"
    t.string   "gang_affiliation"
    t.text     "remarks"
    t.string   "doc_number"
    t.string   "front_mugshot_file_name"
    t.string   "side_mugshot_file_name"
    t.string   "front_mugshot_content_type"
    t.string   "side_mugshot_content_type"
    t.integer  "front_mugshot_file_size"
    t.integer  "side_mugshot_file_size"
    t.datetime "front_mugshot_updated_at"
    t.datetime "side_mugshot_updated_at"
    t.boolean  "merge_locked",               :default => false
    t.boolean  "legacy",                     :default => false
    t.integer  "created_by",                 :default => 1
    t.integer  "updated_by",                 :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "commissary_facility_id"
    t.date     "dna_blood_date"
    t.string   "dna_profile"
    t.string   "fingerprint_class"
  end

  add_index "people", ["build_id"], :name => "index_people_on_build_id"
  add_index "people", ["commissary_facility_id"], :name => "index_people_on_commissary_facility_id"
  add_index "people", ["complexion_id"], :name => "index_people_on_complexion_id"
  add_index "people", ["created_by"], :name => "index_people_on_created_by"
  add_index "people", ["em_relationship_id"], :name => "index_people_on_em_relationship_id"
  add_index "people", ["eye_color_id"], :name => "index_people_on_eye_color_id"
  add_index "people", ["facial_hair_id"], :name => "index_people_on_facial_hair_id"
  add_index "people", ["firstname"], :name => "index_people_on_firstname"
  add_index "people", ["hair_color_id"], :name => "index_people_on_hair_color_id"
  add_index "people", ["hair_type_id"], :name => "index_people_on_hair_type_id"
  add_index "people", ["is_alias_for"], :name => "index_people_on_is_alias_for"
  add_index "people", ["lastname"], :name => "index_people_on_lastname"
  add_index "people", ["mail_state_id"], :name => "index_people_on_mail_state_id"
  add_index "people", ["middlename"], :name => "index_people_on_middlename"
  add_index "people", ["oln"], :name => "index_people_on_oln"
  add_index "people", ["oln_state_id"], :name => "index_people_on_oln_state_id"
  add_index "people", ["phys_state_id"], :name => "index_people_on_phys_state_id"
  add_index "people", ["race_id"], :name => "index_people_on_race_id"
  add_index "people", ["ssn"], :name => "index_people_on_ssn"
  add_index "people", ["suffix"], :name => "index_people_on_suffix"
  add_index "people", ["updated_by"], :name => "index_people_on_updated_by"

  create_table "phones", :force => true do |t|
    t.integer  "contact_id"
    t.integer  "label_id"
    t.string   "value"
    t.string   "note"
    t.integer  "created_by", :default => 1
    t.integer  "updated_by", :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "unlisted",   :default => false
  end

  add_index "phones", ["contact_id"], :name => "index_phones_on_contact_id"
  add_index "phones", ["created_by"], :name => "index_phones_on_created_by"
  add_index "phones", ["label_id"], :name => "index_phones_on_label_id"
  add_index "phones", ["updated_by"], :name => "index_phones_on_updated_by"

  create_table "probation_payments", :force => true do |t|
    t.integer  "probation_id"
    t.date     "transaction_date"
    t.string   "officer"
    t.string   "officer_unit"
    t.string   "officer_badge"
    t.integer  "officer_id"
    t.string   "receipt_no"
    t.string   "memo"
    t.datetime "report_datetime"
    t.integer  "created_by",                                      :default => 1
    t.integer  "updated_by",                                      :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "fund1_name"
    t.decimal  "fund1_charge",     :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund1_payment",    :precision => 12, :scale => 2, :default => 0.0
    t.string   "fund2_name"
    t.decimal  "fund2_charge",     :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund2_payment",    :precision => 12, :scale => 2, :default => 0.0
    t.string   "fund3_name"
    t.decimal  "fund3_charge",     :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund3_payment",    :precision => 12, :scale => 2, :default => 0.0
    t.string   "fund4_name"
    t.decimal  "fund4_charge",     :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund4_payment",    :precision => 12, :scale => 2, :default => 0.0
    t.string   "fund5_name"
    t.decimal  "fund5_charge",     :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund5_payment",    :precision => 12, :scale => 2, :default => 0.0
    t.string   "fund6_name"
    t.decimal  "fund6_charge",     :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund6_payment",    :precision => 12, :scale => 2, :default => 0.0
    t.string   "fund7_name"
    t.decimal  "fund7_charge",     :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund7_payment",    :precision => 12, :scale => 2, :default => 0.0
    t.string   "fund8_name"
    t.decimal  "fund8_charge",     :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund8_payment",    :precision => 12, :scale => 2, :default => 0.0
    t.string   "fund9_name"
    t.decimal  "fund9_charge",     :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund9_payment",    :precision => 12, :scale => 2, :default => 0.0
  end

  add_index "probation_payments", ["created_by"], :name => "index_probation_payments_on_created_by"
  add_index "probation_payments", ["probation_id"], :name => "index_probation_payments_on_probation_id"
  add_index "probation_payments", ["updated_by"], :name => "index_probation_payments_on_updated_by"

  create_table "probations", :force => true do |t|
    t.integer  "person_id"
    t.integer  "status_id"
    t.date     "start_date"
    t.date     "end_date"
    t.date     "completed_date"
    t.date     "revoked_date"
    t.string   "revoked_for"
    t.integer  "docket_id"
    t.string   "docket_no"
    t.integer  "court_id"
    t.integer  "da_charge_id"
    t.string   "conviction"
    t.integer  "conviction_count"
    t.boolean  "doc",                                                    :default => false
    t.boolean  "parish_jail",                                            :default => false
    t.string   "trial_result"
    t.integer  "default_days",                                           :default => 0
    t.integer  "default_months",                                         :default => 0
    t.integer  "default_years",                                          :default => 0
    t.date     "pay_by_date"
    t.integer  "sentence_days",                                          :default => 0
    t.integer  "sentence_months",                                        :default => 0
    t.integer  "sentence_years",                                         :default => 0
    t.boolean  "sentence_suspended",                                     :default => false
    t.integer  "suspended_except_days",                                  :default => 0
    t.integer  "suspended_except_months",                                :default => 0
    t.integer  "suspended_except_years",                                 :default => 0
    t.boolean  "reverts_to_unsup",                                       :default => false
    t.string   "reverts_conditions"
    t.boolean  "credit_served",                                          :default => false
    t.integer  "service_days",                                           :default => 0
    t.integer  "service_served",                                         :default => 0
    t.boolean  "sap",                                                    :default => false
    t.boolean  "sap_complete",                                           :default => false
    t.boolean  "driver",                                                 :default => false
    t.boolean  "driver_complete",                                        :default => false
    t.boolean  "anger",                                                  :default => false
    t.boolean  "anger_complete",                                         :default => false
    t.boolean  "sat",                                                    :default => false
    t.boolean  "sat_complete",                                           :default => false
    t.boolean  "random_screens",                                         :default => false
    t.boolean  "no_victim_contact",                                      :default => false
    t.boolean  "art893",                                                 :default => false
    t.boolean  "art894",                                                 :default => false
    t.boolean  "art895",                                                 :default => false
    t.text     "sentence_notes"
    t.boolean  "hold_if_arrested",                                       :default => true
    t.integer  "probation_days",                                         :default => 0
    t.integer  "probation_months",                                       :default => 0
    t.integer  "probation_years",                                        :default => 0
    t.decimal  "costs",                   :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fines",                   :precision => 12, :scale => 2, :default => 0.0
    t.text     "notes"
    t.text     "remarks"
    t.decimal  "probation_fees",          :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "idf_amount",              :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "dare_amount",             :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "restitution_amount",      :precision => 12, :scale => 2, :default => 0.0
    t.date     "restitution_date"
    t.string   "probation_officer"
    t.string   "probation_officer_unit"
    t.string   "probation_officer_badge"
    t.integer  "probation_officer_id"
    t.integer  "created_by",                                             :default => 1
    t.integer  "updated_by",                                             :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "probation_hours",                                        :default => 0
    t.integer  "default_hours",                                          :default => 0
    t.integer  "sentence_hours",                                         :default => 0
    t.integer  "suspended_except_hours",                                 :default => 0
    t.integer  "service_hours",                                          :default => 0
    t.string   "fund1_name"
    t.string   "fund2_name"
    t.string   "fund3_name"
    t.string   "fund4_name"
    t.string   "fund5_name"
    t.string   "fund6_name"
    t.string   "fund7_name"
    t.string   "fund8_name"
    t.string   "fund9_name"
    t.decimal  "fund1_charged",           :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund2_charged",           :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund3_charged",           :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund4_charged",           :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund5_charged",           :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund6_charged",           :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund7_charged",           :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund8_charged",           :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund9_charged",           :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund1_paid",              :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund2_paid",              :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund3_paid",              :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund4_paid",              :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund5_paid",              :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund6_paid",              :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund7_paid",              :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund8_paid",              :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "fund9_paid",              :precision => 12, :scale => 2, :default => 0.0
  end

  add_index "probations", ["court_id"], :name => "index_probations_on_court_id"
  add_index "probations", ["created_by"], :name => "index_probations_on_created_by"
  add_index "probations", ["da_charge_id"], :name => "index_probations_on_da_charge_id"
  add_index "probations", ["docket_id"], :name => "index_probations_on_docket_id"
  add_index "probations", ["person_id"], :name => "index_probations_on_person_id"
  add_index "probations", ["status_id"], :name => "index_probations_on_status_id"
  add_index "probations", ["updated_by"], :name => "index_probations_on_updated_by"

  create_table "properties", :force => true do |t|
    t.integer  "booking_id"
    t.integer  "quantity"
    t.string   "description"
    t.integer  "created_by",  :default => 1
    t.integer  "updated_by",  :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "properties", ["booking_id"], :name => "index_properties_on_booking_id"
  add_index "properties", ["created_by"], :name => "index_properties_on_created_by"
  add_index "properties", ["updated_by"], :name => "index_properties_on_updated_by"

  create_table "report_victims", :id => false, :force => true do |t|
    t.integer "person_id"
    t.integer "invest_report_id"
  end

  add_index "report_victims", ["invest_report_id"], :name => "index_report_victims_on_invest_report_id"
  add_index "report_victims", ["person_id"], :name => "index_report_victims_on_person_id"

  create_table "report_witnesses", :id => false, :force => true do |t|
    t.integer "person_id"
    t.integer "invest_report_id"
  end

  add_index "report_witnesses", ["invest_report_id"], :name => "index_report_witnesses_on_invest_report_id"
  add_index "report_witnesses", ["person_id"], :name => "index_report_witnesses_on_person_id"

  create_table "revocations", :force => true do |t|
    t.date     "revocation_date"
    t.integer  "docket_id"
    t.integer  "judge_id"
    t.integer  "arrest_id"
    t.boolean  "revoked"
    t.boolean  "time_served"
    t.integer  "consecutive_type_id"
    t.integer  "total_days",          :default => 0
    t.integer  "total_months",        :default => 0
    t.integer  "total_years",         :default => 0
    t.boolean  "doc"
    t.integer  "parish_days",         :default => 0
    t.integer  "parish_months",       :default => 0
    t.integer  "parish_years",        :default => 0
    t.boolean  "processed"
    t.text     "remarks"
    t.integer  "created_by",          :default => 1
    t.integer  "updated_by",          :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "process_status"
    t.integer  "total_hours",         :default => 0
    t.integer  "parish_hours",        :default => 0
    t.boolean  "delay_execution",     :default => false
    t.boolean  "deny_goodtime",       :default => false
  end

  add_index "revocations", ["arrest_id"], :name => "index_revocations_on_arrest_id"
  add_index "revocations", ["consecutive_type_id"], :name => "index_revocations_on_consecutive_type_id"
  add_index "revocations", ["created_by"], :name => "index_revocations_on_created_by"
  add_index "revocations", ["docket_id"], :name => "index_revocations_on_docket_id"
  add_index "revocations", ["judge_id"], :name => "index_revocations_on_judge_id"
  add_index "revocations", ["updated_by"], :name => "index_revocations_on_updated_by"

  create_table "sentences", :force => true do |t|
    t.integer  "docket_id"
    t.integer  "court_id"
    t.integer  "probation_id"
    t.integer  "da_charge_id"
    t.integer  "result_id"
    t.string   "conviction"
    t.integer  "conviction_count"
    t.boolean  "doc",                                                        :default => false
    t.decimal  "fines",                       :precision => 12, :scale => 2, :default => 0.0
    t.integer  "court_cost_type_id"
    t.decimal  "costs",                       :precision => 12, :scale => 2, :default => 0.0
    t.integer  "default_days",                                               :default => 0
    t.integer  "default_months",                                             :default => 0
    t.integer  "default_years",                                              :default => 0
    t.date     "pay_by_date"
    t.integer  "sentence_days",                                              :default => 0
    t.integer  "sentence_months",                                            :default => 0
    t.integer  "sentence_years",                                             :default => 0
    t.boolean  "suspended",                                                  :default => false
    t.integer  "suspended_except_days",                                      :default => 0
    t.integer  "suspended_except_months",                                    :default => 0
    t.integer  "suspended_except_years",                                     :default => 0
    t.integer  "probation_days",                                             :default => 0
    t.integer  "probation_months",                                           :default => 0
    t.integer  "probation_years",                                            :default => 0
    t.integer  "probation_type_id"
    t.date     "restitution_date"
    t.boolean  "credit_time_served",                                         :default => false
    t.integer  "community_service_days"
    t.boolean  "substance_abuse_program",                                    :default => false
    t.boolean  "driver_improvement",                                         :default => false
    t.decimal  "probation_fees",              :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "idf_amount",                  :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "dare_amount",                 :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "restitution_amount",          :precision => 12, :scale => 2, :default => 0.0
    t.boolean  "anger_management",                                           :default => false
    t.boolean  "substance_abuse_treatment",                                  :default => false
    t.boolean  "random_drug_screens",                                        :default => false
    t.boolean  "no_victim_contact",                                          :default => false
    t.boolean  "art_893",                                                    :default => false
    t.boolean  "art_894",                                                    :default => false
    t.boolean  "art_895",                                                    :default => false
    t.boolean  "probation_reverts",                                          :default => false
    t.string   "probation_revert_conditions"
    t.boolean  "wo_benefit",                                                 :default => false
    t.boolean  "parish_jail",                                                :default => false
    t.integer  "consecutive_type_id"
    t.boolean  "sentence_consecutive",                                       :default => false
    t.string   "sentence_notes"
    t.boolean  "fines_consecutive",                                          :default => false
    t.string   "fines_notes"
    t.boolean  "costs_consecutive",                                          :default => false
    t.string   "costs_notes"
    t.text     "notes"
    t.text     "remarks"
    t.boolean  "processed",                                                  :default => false
    t.boolean  "legacy",                                                     :default => false
    t.integer  "created_by",                                                 :default => 1
    t.integer  "updated_by",                                                 :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "process_status"
    t.integer  "default_hours",                                              :default => 0
    t.integer  "sentence_hours",                                             :default => 0
    t.integer  "suspended_except_hours",                                     :default => 0
    t.integer  "probation_hours",                                            :default => 0
    t.boolean  "delay_execution",                                            :default => false
    t.integer  "community_service_hours",                                    :default => 0
    t.boolean  "deny_goodtime",                                              :default => false
  end

  add_index "sentences", ["consecutive_type_id"], :name => "index_sentences_on_consecutive_type_id"
  add_index "sentences", ["court_cost_type_id"], :name => "index_sentences_on_court_cost_type_id"
  add_index "sentences", ["court_id"], :name => "index_sentences_on_court_id"
  add_index "sentences", ["created_by"], :name => "index_sentences_on_created_by"
  add_index "sentences", ["da_charge_id"], :name => "index_sentences_on_da_charge_id"
  add_index "sentences", ["docket_id"], :name => "index_sentences_on_docket_id"
  add_index "sentences", ["probation_id"], :name => "index_sentences_on_probation_id"
  add_index "sentences", ["probation_type_id"], :name => "index_sentences_on_probation_type_id"
  add_index "sentences", ["result_id"], :name => "index_sentences_on_result_id"
  add_index "sentences", ["updated_by"], :name => "index_sentences_on_updated_by"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "statutes", :force => true do |t|
    t.string   "name"
    t.integer  "created_by", :default => 1
    t.integer  "updated_by", :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "statutes", ["created_by"], :name => "index_statutes_on_created_by"
  add_index "statutes", ["name"], :name => "index_statutes_on_name"
  add_index "statutes", ["updated_by"], :name => "index_statutes_on_updated_by"

  create_table "stolens", :force => true do |t|
    t.string   "case_no"
    t.integer  "person_id"
    t.date     "stolen_date"
    t.string   "model_no"
    t.string   "serial_no"
    t.decimal  "item_value",         :precision => 12, :scale => 2, :default => 0.0
    t.string   "item"
    t.text     "item_description"
    t.date     "recovered_date"
    t.date     "returned_date"
    t.text     "remarks"
    t.boolean  "private",                                           :default => false
    t.integer  "investigation_id"
    t.integer  "invest_crime_id"
    t.integer  "invest_criminal_id"
    t.integer  "owned_by"
    t.integer  "created_by",                                        :default => 1
    t.integer  "updated_by",                                        :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "stolens", ["case_no"], :name => "index_stolens_on_case_no"
  add_index "stolens", ["created_by"], :name => "index_stolens_on_created_by"
  add_index "stolens", ["invest_crime_id"], :name => "index_stolens_on_invest_crime_id"
  add_index "stolens", ["invest_criminal_id"], :name => "index_stolens_on_invest_criminal_id"
  add_index "stolens", ["investigation_id"], :name => "index_stolens_on_investigation_id"
  add_index "stolens", ["owned_by"], :name => "index_stolens_on_owned_by"
  add_index "stolens", ["person_id"], :name => "index_stolens_on_person_id"
  add_index "stolens", ["private"], :name => "index_stolens_on_private"
  add_index "stolens", ["updated_by"], :name => "index_stolens_on_updated_by"

  create_table "suspects", :id => false, :force => true do |t|
    t.integer "offense_id"
    t.integer "person_id"
  end

  add_index "suspects", ["offense_id"], :name => "index_suspects_on_offense_id"
  add_index "suspects", ["person_id"], :name => "index_suspects_on_person_id"

  create_table "transfers", :force => true do |t|
    t.date     "transfer_date"
    t.string   "transfer_time"
    t.integer  "booking_id"
    t.integer  "person_id"
    t.integer  "transfer_type_id"
    t.string   "from_agency"
    t.integer  "from_agency_id"
    t.string   "from_officer"
    t.integer  "from_officer_id"
    t.string   "from_officer_unit"
    t.string   "from_officer_badge"
    t.string   "to_agency"
    t.integer  "to_agency_id"
    t.string   "to_officer"
    t.integer  "to_officer_id"
    t.string   "to_officer_unit"
    t.string   "to_officer_badge"
    t.text     "remarks"
    t.datetime "reported_datetime"
    t.integer  "created_by",         :default => 1
    t.integer  "updated_by",         :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "transfers", ["booking_id"], :name => "index_transfers_on_booking_id"
  add_index "transfers", ["created_by"], :name => "index_transfers_on_created_by"
  add_index "transfers", ["person_id"], :name => "index_transfers_on_person_id"
  add_index "transfers", ["transfer_type_id"], :name => "index_transfers_on_transfer_type_id"
  add_index "transfers", ["updated_by"], :name => "index_transfers_on_updated_by"

  create_table "transports", :force => true do |t|
    t.integer  "person_id"
    t.string   "officer1"
    t.string   "officer1_unit"
    t.string   "officer1_badge"
    t.integer  "officer1_id"
    t.string   "officer2"
    t.string   "officer2_unit"
    t.string   "officer2_badge"
    t.integer  "officer2_id"
    t.date     "begin_date"
    t.string   "begin_time"
    t.integer  "begin_miles"
    t.date     "end_date"
    t.string   "end_time"
    t.integer  "end_miles"
    t.integer  "estimated_miles"
    t.string   "from"
    t.integer  "from_id"
    t.string   "from_street"
    t.string   "from_city"
    t.integer  "from_state_id"
    t.string   "from_zip"
    t.string   "to"
    t.integer  "to_id"
    t.string   "to_street"
    t.string   "to_city"
    t.integer  "to_state_id"
    t.string   "to_zip"
    t.text     "remarks"
    t.integer  "created_by",      :default => 1
    t.integer  "updated_by",      :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "transports", ["created_by"], :name => "index_transports_on_created_by"
  add_index "transports", ["from_state_id"], :name => "index_transports_on_from_state_id"
  add_index "transports", ["person_id"], :name => "index_transports_on_person_id"
  add_index "transports", ["to_state_id"], :name => "index_transports_on_to_state_id"
  add_index "transports", ["updated_by"], :name => "index_transports_on_updated_by"

  create_table "users", :force => true do |t|
    t.string   "login"
    t.integer  "contact_id"
    t.boolean  "locked",                  :default => false
    t.string   "hashed_password"
    t.string   "salt"
    t.integer  "items_per_page",          :default => 15
    t.date     "expires"
    t.string   "disabled_text"
    t.string   "user_photo_file_name"
    t.string   "user_photo_content_type"
    t.integer  "user_photo_file_size"
    t.datetime "user_photo_updated_at"
    t.boolean  "send_email",              :default => false
    t.boolean  "no_self_bug_messages",    :default => true
    t.integer  "last_call",               :default => 0
    t.integer  "last_pawn",               :default => 0
    t.integer  "last_arrest",             :default => 0
    t.integer  "last_bug",                :default => 0
    t.integer  "last_forbid",             :default => 0
    t.boolean  "review_forbid",           :default => true
    t.boolean  "review_arrest",           :default => true
    t.boolean  "review_pawn",             :default => true
    t.boolean  "review_bug",              :default => true
    t.boolean  "review_call",             :default => true
    t.integer  "last_warrant",            :default => 0
    t.boolean  "review_warrant",          :default => true
    t.integer  "created_by",              :default => 1
    t.integer  "updated_by",              :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "show_tips",               :default => true
    t.boolean  "notify_release",          :default => false
  end

  add_index "users", ["contact_id"], :name => "index_users_on_contact_id"
  add_index "users", ["created_by"], :name => "index_users_on_created_by"
  add_index "users", ["login"], :name => "index_users_on_login"
  add_index "users", ["updated_by"], :name => "index_users_on_updated_by"

  create_table "victims", :id => false, :force => true do |t|
    t.integer "offense_id"
    t.integer "person_id"
  end

  add_index "victims", ["offense_id"], :name => "index_victims_on_offense_id"
  add_index "victims", ["person_id"], :name => "index_victims_on_person_id"

  create_table "visitors", :force => true do |t|
    t.integer  "booking_id"
    t.string   "name"
    t.string   "street"
    t.string   "city"
    t.integer  "state_id"
    t.string   "zip"
    t.string   "phone"
    t.date     "visit_date"
    t.string   "sign_in_time"
    t.string   "sign_out_time"
    t.integer  "relationship_id"
    t.text     "remarks"
    t.boolean  "legacy",          :default => false
    t.integer  "created_by",      :default => 1
    t.integer  "updated_by",      :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "visitors", ["booking_id"], :name => "index_visitors_on_booking_id"
  add_index "visitors", ["created_by"], :name => "index_visitors_on_created_by"
  add_index "visitors", ["relationship_id"], :name => "index_visitors_on_relationship_id"
  add_index "visitors", ["state_id"], :name => "index_visitors_on_state_id"
  add_index "visitors", ["updated_by"], :name => "index_visitors_on_updated_by"

  create_table "warrant_exemptions", :id => false, :force => true do |t|
    t.integer "person_id"
    t.integer "warrant_id"
  end

  add_index "warrant_exemptions", ["person_id"], :name => "index_warrant_exemptions_on_person_id"
  add_index "warrant_exemptions", ["warrant_id"], :name => "index_warrant_exemptions_on_warrant_id"

  create_table "warrants", :force => true do |t|
    t.integer  "person_id"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "suffix"
    t.string   "ssn"
    t.date     "dob"
    t.integer  "race_id"
    t.integer  "sex"
    t.string   "street"
    t.string   "city"
    t.integer  "state_id"
    t.string   "zip"
    t.string   "oln"
    t.integer  "oln_state_id"
    t.string   "warrant_no"
    t.integer  "special_type",                                      :default => 0
    t.integer  "jurisdiction_id"
    t.date     "received_date"
    t.date     "issued_date"
    t.decimal  "payable",            :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "bond_amt",           :precision => 12, :scale => 2, :default => 0.0
    t.integer  "disposition_id"
    t.date     "dispo_date"
    t.text     "remarks"
    t.integer  "investigation_id"
    t.integer  "invest_crime_id"
    t.integer  "invest_criminal_id"
    t.boolean  "private",                                           :default => false
    t.boolean  "legacy",                                            :default => false
    t.integer  "owned_by"
    t.integer  "created_by",                                        :default => 1
    t.integer  "updated_by",                                        :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "middle1"
    t.string   "middle2"
    t.string   "middle3"
  end

  add_index "warrants", ["created_by"], :name => "index_warrants_on_created_by"
  add_index "warrants", ["dispo_date"], :name => "index_warrants_on_dispo_date"
  add_index "warrants", ["disposition_id"], :name => "index_warrants_on_disposition_id"
  add_index "warrants", ["firstname"], :name => "index_warrants_on_firstname"
  add_index "warrants", ["invest_crime_id"], :name => "index_warrants_on_invest_crime_id"
  add_index "warrants", ["invest_criminal_id"], :name => "index_warrants_on_invest_criminal_id"
  add_index "warrants", ["investigation_id"], :name => "index_warrants_on_investigation_id"
  add_index "warrants", ["jurisdiction_id"], :name => "index_warrants_on_jurisdiction_id"
  add_index "warrants", ["lastname"], :name => "index_warrants_on_lastname"
  add_index "warrants", ["oln_state_id"], :name => "index_warrants_on_oln_state_id"
  add_index "warrants", ["owned_by"], :name => "index_warrants_on_owned_by"
  add_index "warrants", ["person_id"], :name => "index_warrants_on_person_id"
  add_index "warrants", ["private"], :name => "index_warrants_on_private"
  add_index "warrants", ["race_id"], :name => "index_warrants_on_race_id"
  add_index "warrants", ["state_id"], :name => "index_warrants_on_state_id"
  add_index "warrants", ["updated_by"], :name => "index_warrants_on_updated_by"
  add_index "warrants", ["zip"], :name => "index_warrants_on_zip"

  create_table "watchers", :id => false, :force => true do |t|
    t.integer "bug_id"
    t.integer "user_id"
  end

  add_index "watchers", ["bug_id"], :name => "index_watchers_on_bug_id"
  add_index "watchers", ["user_id"], :name => "index_watchers_on_user_id"

  create_table "witnesses", :id => false, :force => true do |t|
    t.integer "offense_id"
    t.integer "person_id"
  end

  add_index "witnesses", ["offense_id"], :name => "index_witnesses_on_offense_id"
  add_index "witnesses", ["person_id"], :name => "index_witnesses_on_person_id"

  create_table "wreckers", :force => true do |t|
    t.integer  "offense_id"
    t.string   "name"
    t.boolean  "by_request", :default => false
    t.string   "location"
    t.string   "vin"
    t.string   "tag"
    t.integer  "created_by", :default => 1
    t.integer  "updated_by", :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "wreckers", ["created_by"], :name => "index_wreckers_on_created_by"
  add_index "wreckers", ["offense_id"], :name => "index_wreckers_on_offense_id"
  add_index "wreckers", ["updated_by"], :name => "index_wreckers_on_updated_by"

  create_table "zips", :force => true do |t|
    t.string   "zip_code"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "state"
    t.string   "city"
    t.string   "county"
    t.integer  "created_by", :default => 1
    t.integer  "updated_by", :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "zips", ["created_by"], :name => "index_zips_on_created_by"
  add_index "zips", ["updated_by"], :name => "index_zips_on_updated_by"
  add_index "zips", ["zip_code"], :name => "index_zips_on_zip_code"

end
