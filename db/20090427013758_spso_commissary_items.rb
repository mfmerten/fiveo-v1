# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class SpsoCommissaryItems < ActiveRecord::Migration
   class CommissaryItem < ActiveRecord::Base
      has_and_belongs_to_many :facilities, :class_name => 'SpsoCommissaryItems::Option', :join_table => 'commissary_facilities'
   end
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => "SpsoCommissaryItems::Option", :dependent => :destroy
      validates_presence_of :name
      validates_uniqueness_of :name
   end
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => "SpsoCommissaryItems::OptionType"
      validates_presence_of :long_name, :option_type_id
   end
   
   def self.up
      # prepare for the data
      ot = OptionType.find_by_name("Facility")
      spdc = Option.find(:first, :conditions => {:long_name => "Sabine Parish Detention Center", :option_type_id => ot.id})
      spwf = Option.find(:first, :conditions => {:long_name => "Sabine Parish Womens Facility", :option_type_id => ot.id})
      CommissaryItem.delete_all
      
      # Men's Facility
      CommissaryItem.create(:description => 'SODAS', :price => 1).facilities << spdc
      CommissaryItem.create(:description => 'COOKIES (ALL)', :price => 0.8).facilities << spdc
      CommissaryItem.create(:description => 'CHIPS', :price => 1.3).facilities << spdc
      CommissaryItem.create(:description => 'HONEY BUNS', :price => 1).facilities << spdc
      CommissaryItem.create(:description => 'PEANUTS', :price => 0.75).facilities << spdc
      CommissaryItem.create(:description => 'SOUP BOWL', :price => 1).facilities << spdc
      CommissaryItem.create(:description => 'COFFEE (8 OZ BAG)', :price => 8).facilities << spdc
      CommissaryItem.create(:description => 'SALTINE CRACKERS', :price => 2).facilities << spdc
      CommissaryItem.create(:description => 'CHEESE DIP', :price => 1.1).facilities << spdc
      CommissaryItem.create(:description => 'SANDWICH SPREAD', :price => 3.5).facilities << spdc
      CommissaryItem.create(:description => 'PEANUT BUTTER', :price => 3).facilities << spdc
      CommissaryItem.create(:description => 'CHEESE SAUCE', :price => 3.25).facilities << spdc
      CommissaryItem.create(:description => 'TOWN HOUSE CRACKER ROLL', :price => 1.5).facilities << spdc
      CommissaryItem.create(:description => 'OATMEAL CAKES (BOX)', :price => 2).facilities << spdc
      CommissaryItem.create(:description => 'SOAP', :price => 1.15).facilities << spdc
      CommissaryItem.create(:description => 'DEODORANT (STICK)', :price => 3.15).facilities << spdc
      CommissaryItem.create(:description => 'BABY OIL', :price => 3.15).facilities << spdc
      CommissaryItem.create(:description => 'SOAP DISH', :price => 0.65).facilities << spdc
      CommissaryItem.create(:description => 'PAPER', :price => 1.15).facilities << spdc
      CommissaryItem.create(:description => 'ENVELOPES', :price => 0.05).facilities << spdc
      CommissaryItem.create(:description => 'CARMEX', :price => 1.65).facilities << spdc
      CommissaryItem.create(:description => 'SANDALS (PAIR)', :price => 3.15).facilities << spdc
      CommissaryItem.create(:description => 'SOUPS', :price => 0.5).facilities << spdc
      CommissaryItem.create(:description => 'PENS', :price => 0.35).facilities << spdc
      CommissaryItem.create(:description => 'CHEESE CRACKERS', :price => 0.35).facilities << spdc
      CommissaryItem.create(:description => 'THERMAL TOP', :price => 5.65).facilities << spdc
      CommissaryItem.create(:description => 'BOXER SHORTS (EACH)', :price => 3.15).facilities << spdc
      CommissaryItem.create(:description => 'SOCKS (PAIR)', :price => 1.05).facilities << spdc
      CommissaryItem.create(:description => 'CHEWING TOBACCO', :price => 4).facilities << spdc
      CommissaryItem.create(:description => 'EAR PHONES', :price => 3).facilities << spdc
      CommissaryItem.create(:description => 'T-SHIRTS (2-XLG-4-XLG)', :price => 4.15).facilities << spdc
      CommissaryItem.create(:description => 'TENNIS SHOES (8-13)', :price => 7.65).facilities << spdc
      CommissaryItem.create(:description => 'LOTION', :price => 2.15).facilities << spdc
      CommissaryItem.create(:description => 'KOOL-AID (PACKAGE)', :price => 0.75).facilities << spdc
      CommissaryItem.create(:description => 'SUMMER SAUSAGE', :price => 3.5).facilities << spdc
      CommissaryItem.create(:description => 'TOOTH PASTE', :price => 2.65).facilities << spdc
      CommissaryItem.create(:description => 'SHAMPOO', :price => 2.4).facilities << spdc
      CommissaryItem.create(:description => 'POWDER', :price => 2.15).facilities << spdc
      CommissaryItem.create(:description => 'TOOTHBRUSH', :price => 1.40).facilities << spdc
      CommissaryItem.create(:description => 'BLUE MAGIC', :price => 2.65).facilities << spdc
      CommissaryItem.create(:description => 'STAMPS (LIMIT 2)', :price => 0.42).facilities << spdc
      CommissaryItem.create(:description => 'BATTERIES', :price => 1.40).facilities << spdc
      CommissaryItem.create(:description => 'RADIO', :price => 25.15).facilities << spdc
      CommissaryItem.create(:description => 'TUNA-N-BAG', :price => 3).facilities << spdc
      CommissaryItem.create(:description => 'ICE CREAM CONE', :price => 0.5).facilities << spdc
      CommissaryItem.create(:description => 'RED SEAL (DIP)', :price => 3).facilities << spdc
      CommissaryItem.create(:description => 'TOOTH BRUSH HOLDER', :price => 0.65).facilities << spdc
      CommissaryItem.create(:description => 'THERMAL BOTTOM', :price => 5.65).facilities << spdc
      CommissaryItem.create(:description => 'T-SHIRTS (SMALL-XLG)', :price => 3.65).facilities << spdc
      CommissaryItem.create(:description => 'COCOA (BOX)', :price => 3.5).facilities << spdc
      CommissaryItem.create(:description => 'LOCK', :price => 5).facilities << spdc
      CommissaryItem.create(:description => 'PLAYING CARDS', :price => 2.15).facilities << spdc
      CommissaryItem.create(:description => 'COMB', :price => 0.1).facilities << spdc
      CommissaryItem.create(:description => 'PAIN KILLERS (LIMIT 3)', :price => 1).facilities << spdc
      CommissaryItem.create(:description => 'KOOL-AID (BOX)', :price => 0.75).facilities << spdc
      CommissaryItem.create(:description => 'CASHEWS', :price => 1.5).facilities << spdc
      
      # women's facility
      CommissaryItem.create(:description => 'SODA', :price => 1.75).facilities << spwf
      CommissaryItem.create(:description => 'FLAV. WATER', :price => 1.5).facilities << spwf
      CommissaryItem.create(:description => 'WATER', :price => 1).facilities << spwf
      CommissaryItem.create(:description => 'SM.JUICE', :price => 1).facilities << spwf
      CommissaryItem.create(:description => 'TEA BAGS', :price => 0.05).facilities << spwf
      CommissaryItem.create(:description => 'CAPRI SUN', :price => 0.5).facilities << spwf
      CommissaryItem.create(:description => 'COFFEE (JAR)', :price => 8).facilities << spwf
      CommissaryItem.create(:description => 'COCOA (BOX)', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'SN. PK CANDYBARS', :price => 1.25).facilities << spwf
      CommissaryItem.create(:description => 'ASST. HARD CANDY', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'JOLLY RANCHERS', :price => 1.25).facilities << spwf
      CommissaryItem.create(:description => 'HOT TAMALES', :price => 2.75).facilities << spwf
      CommissaryItem.create(:description => 'DUM-DUMS', :price => 2.75).facilities << spwf
      CommissaryItem.create(:description => 'YARN', :price => 3).facilities << spwf
      CommissaryItem.create(:description => 'NEEDLE', :price => 1.25).facilities << spwf
      CommissaryItem.create(:description => 'SM.CANVAS', :price => 0.5).facilities << spwf
      CommissaryItem.create(:description => 'LG.CANVAS', :price => 1).facilities << spwf
      CommissaryItem.create(:description => 'POLY FILL', :price => 0).facilities << spwf
      CommissaryItem.create(:description => 'CROCHET HOOK', :price => 0).facilities << spwf
      CommissaryItem.create(:description => 'STAR', :price => 3.25).facilities << spwf
      CommissaryItem.create(:description => 'STAMPS', :price => 0.41).facilities << spwf
      CommissaryItem.create(:description => 'PAPER', :price => 1).facilities << spwf
      CommissaryItem.create(:description => 'ENVELOPES', :price => 0.05).facilities << spwf
      CommissaryItem.create(:description => 'INK PEN', :price => 0.2).facilities << spwf
      CommissaryItem.create(:description => 'LOCK', :price => 2.5).facilities << spwf
      CommissaryItem.create(:description => 'PLAYING CARDS', :price => 2.5).facilities << spwf
      CommissaryItem.create(:description => 'SOUP BOWL', :price => 1).facilities << spwf
      CommissaryItem.create(:description => 'RADIO', :price => 25).facilities << spwf
      CommissaryItem.create(:description => 'BATTERIES', :price => 0.95).facilities << spwf
      CommissaryItem.create(:description => 'COOKIES (LG ASST)', :price => 3.5).facilities << spwf
      CommissaryItem.create(:description => 'CAKES (BOX)', :price => 2.25).facilities << spwf
      CommissaryItem.create(:description => 'CHIPS', :price => 1.25).facilities << spwf
      CommissaryItem.create(:description => 'CHILI', :price => 3).facilities << spwf
      CommissaryItem.create(:description => 'MAYO', :price => 1.5).facilities << spwf
      CommissaryItem.create(:description => 'PEANUT BUTTER', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'HONEY BUN', :price => 1).facilities << spwf
      CommissaryItem.create(:description => 'SM.COOKIES', :price => 0.5).facilities << spwf
      CommissaryItem.create(:description => 'SALTINE CRACKERS', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'RITZ CRACKERS', :price => 0.85).facilities << spwf
      CommissaryItem.create(:description => 'CHEESE CRACKERS', :price => 0.35).facilities << spwf
      CommissaryItem.create(:description => 'VANILLA WAFERS', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'CHEESE SAUCE', :price => 3).facilities << spwf
      CommissaryItem.create(:description => 'SOUPS', :price => 0.5).facilities << spwf
      CommissaryItem.create(:description => 'PEPPERS', :price => 2.5).facilities << spwf
      CommissaryItem.create(:description => 'RANCH DRESSING', :price => 4.5).facilities << spwf
      CommissaryItem.create(:description => 'CREAMER', :price => 2.5).facilities << spwf
      CommissaryItem.create(:description => 'SUGAR', :price => 1).facilities << spwf
      CommissaryItem.create(:description => 'RICE CRISPIES', :price => 0.4).facilities << spwf
      CommissaryItem.create(:description => 'VIENNA', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'SARDINES', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'TUNA', :price => 1.5).facilities << spwf
      CommissaryItem.create(:description => 'POPCORN', :price => 0.5).facilities << spwf
      CommissaryItem.create(:description => 'PICKELS', :price => 1.25).facilities << spwf
      CommissaryItem.create(:description => 'CHICKEN', :price => 3).facilities << spwf
      CommissaryItem.create(:description => 'MAC-N-CHEESE', :price => 1).facilities << spwf
      CommissaryItem.create(:description => 'HOT SAUCE', :price => 1.25).facilities << spwf
      CommissaryItem.create(:description => 'CRUNCH-A-MUNCH', :price => 1.5).facilities << spwf
      CommissaryItem.create(:description => 'TISSUE', :price => 0.4).facilities << spwf
      CommissaryItem.create(:description => 'PADS', :price => 1).facilities << spwf
      CommissaryItem.create(:description => 'BAR SOAP', :price => 0.4).facilities << spwf
      CommissaryItem.create(:description => 'THERMAL TOP', :price => 5).facilities << spwf
      CommissaryItem.create(:description => 'THERMAL BOTTOM', :price => 5).facilities << spwf
      CommissaryItem.create(:description => 'PANTIES', :price => 2.5).facilities << spwf
      CommissaryItem.create(:description => 'BOXERS', :price => 3).facilities << spwf
      CommissaryItem.create(:description => 'BRAS', :price => 2.75).facilities << spwf
      CommissaryItem.create(:description => 'TENNIS SHOES', :price => 5).facilities << spwf
      CommissaryItem.create(:description => 'T-SHIRTS', :price => 2.5).facilities << spwf
      CommissaryItem.create(:description => 'SANDALS', :price => 3).facilities << spwf
      CommissaryItem.create(:description => 'SOCKS', :price => 0.9).facilities << spwf
      CommissaryItem.create(:description => 'SOAP', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'BODY WASH', :price => 3.5).facilities << spwf
      CommissaryItem.create(:description => 'DEODORANT', :price => 2.5).facilities << spwf
      CommissaryItem.create(:description => 'BABY OIL', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'SOAP DISH', :price => 1).facilities << spwf
      CommissaryItem.create(:description => 'T/B HOLDER', :price => 1).facilities << spwf
      CommissaryItem.create(:description => 'BRUSH', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'COMB', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'TOOTHPASTE', :price => 2.5).facilities << spwf
      CommissaryItem.create(:description => 'CARMEX', :price => 1.5).facilities << spwf
      CommissaryItem.create(:description => 'TAMPONS', :price => 7).facilities << spwf
      CommissaryItem.create(:description => 'PERM', :price => 10).facilities << spwf
      CommissaryItem.create(:description => 'VASELINE', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'PANTENE', :price => 4.5).facilities << spwf
      CommissaryItem.create(:description => 'SUAVE SHAMPOO', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'SUAVE CONDITIONER', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'COCOA BUTTER', :price => 2.75).facilities << spwf
      CommissaryItem.create(:description => 'B&B GREASE', :price => 3.5).facilities << spwf
      CommissaryItem.create(:description => 'LOTION', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'POWDER', :price => 2).facilities << spwf
      CommissaryItem.create(:description => 'HAIR GEL', :price => 0).facilities << spwf
      CommissaryItem.create(:description => 'HAIR SPRAY', :price => 3.5).facilities << spwf
   end

   def self.down
      CommissaryItem.delete_all
   end
end