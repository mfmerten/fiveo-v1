# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class FixStoppedTimers < ActiveRecord::Migration
   class Booking < ActiveRecord::Base
      has_many :holds, :class_name => 'FixStoppedTimers::Hold'
      has_many :booking_logs, :class_name => 'FixStoppedTimers::BookingLog'
   end
   class Hold < ActiveRecord::Base
      belongs_to :booking, :class_name => 'FixStoppedTimers::Booking'
   end
   class BookingLog < ActiveRecord::Base
      belongs_to :booking, :class_name => 'FixStoppedTimers::Booking'
   end
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => 'FixStoppedTimers::Option'
   end
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => 'FixStoppedTimers::OptionType'
   end
   
   def self.up
      if rrot = OptionType.find_by_name("Temporary Release Reason")
         if rro = Option.find(:first, :conditions => {:option_type_id => rrot.id, :long_name => 'Awaiting Execution Of Sentence'})
            if htot = OptionType.find_by_name("Hold Type")
               if hto = Option.find(:first, :conditions => {:option_type_id => htot.id, :long_name => 'Temporary Release'})
                  Booking.find(:all, :conditions => 'release_date IS NULL').each do |b|
                     if b.disable_timers?
                        if b.booking_logs.empty?
                           # create a 0-time log entry
                           b.booking_logs.create(
                              :start_datetime => Time.now,
                              :start_officer => b.booking_officer,
                              :start_officer_unit => b.booking_officer_unit,
                              :start_officer_badge => b.booking_officer_badge,
                              :stop_datetime => Time.now,
                              :stop_officer => b.booking_officer,
                              :stop_officer_unit => b.booking_officer_unit,
                              :stop_officer_badge => b.booking_officer_badge
                           )
                        elsif !b.booking_logs.last.stop_datetime?
                           # close the last entry because time is disabled
                           l = b.booking-logs.last
                           l.stop_datetime = Time.now
                           l.stop_officer = b.booking_officer
                           l.stop_officer_unit = b.booking_officer_unit
                           l.save
                        end
                     else
                        if b.booking_logs.empty? || b.booking_logs.last.stop_datetime?
                           # start a new entry because timers are enabled
                           b.booking_logs.create(
                              :start_datetime => Time.now,
                              :start_officer => b.booking_officer,
                              :start_officer_unit => b.booking_officer_unit,
                              :start_officer_badge => b.booking_officer_badge
                           )
                        end
                     end
                     if b.booking_logs(true).last.stop_datetime?
                        # time is stopped.  add a temp release so it can be started again
                        b.holds.create(
                           :booking_id => b.id,
                           :hold_type_id => hto.id,
                           :hold_by => b.booking_logs.last.stop_officer,
                           :hold_by_unit => b.booking_logs.last.stop_officer_unit,
                           :hold_by_badge => b.booking_logs.last.stop_officer_badge,
                           :temp_release_reason_id => rro.id,
                           :hold_date => b.booking_logs.last.stop_datetime.to_date,
                           :hold_time => b.booking_logs.last.stop_datetime.strftime("%H:%M"),
                           :created_by => b.updated_by,
                           :updated_by => b.updated_by,
                           :remarks => 'Added Automatically by server update because timekeeping was paused on this booking.'
                        )
                     end
                  end
               end
            end
         end
      end
   end

   def self.down
   end
end
