# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateHelps < ActiveRecord::Migration
   def self.up
      create_table :helps, :force => true do |t|
         t.string :section
         t.string :page
         t.string :title
         t.text :body
         t.timestamps
      end
      add_index :helps, :section
      add_index :helps, :page

      Help.create(
      :section => %(Help Pages),
      :page => %(edit),
      :title => %(Help Page Help),
      :body => %(Help pages are pages of text (textile formatted) that are automatically linked to the application pages as specified by the *Section:* and *Page:* fields (see above).

<h4>Title</h4>

The title is the name of the page that will be shown at the top when the page is displayed.

<h4>Section and Page</h4>

The section identifies the part of the application where the page will be displayed.  The section should be entered exactly as it appears in the navigation bar at the left. For instance, a help page for Booking should have a section name of 'Booking'.

The page identifies the actual page within the specified section that will show the help link.  The proper value for the Page field is the name of the action that is displaying the page.  This is not always apparent, but here are some guidelines:

Look at the page address (on the page you are writing the help for). Typically, you will see something like:  https://192.168.1.5/booking/edit?id=1

* The first part of this address (https://192.168.1.5) is the server address. On other sites, it might look more like http://www.google.com.  For the purposes of this instruction, you can ignore the server address.
* The next part (the /booking) is usually the name of the controller (or section), but not always. When choosing the section, always refer instead to the menu name exactly as it appears in the navigation bar at the left.
* The next part of the address (following /booking) is /edit. In this case, the proper value for the Page field is in fact 'edit'. Some pages will not be as obvious.  If the page you are looking at has only a number shown after the controller part (like /booking/1), you should use 'show' as the Page. If instead, it only has the controller name (/bookings), use 'index' for the Page.

Another example:  https://192.168.1.5/bugs/comment?bug_id=1

* In this case, the Section should be entered as "Bug Reports" (see the menu name on the left, not the controller portion of the address!)
* The Page should be entered as 'comment' (notice the lower case... case is important for both Section and Page.)

If you are in doubt, and your best guess doesn't produce an "Instructions" link on the top right of the desired page, you can file a Bug Report, being sure to copy the complete address from the browser address bar and paste it into the Problem Page: field on the bug report.

<h4>Body</h4>

This is where you enter the text of the help page exactly like you want it to appear to the user.  In the Body section, you can use both HTML and Textile formatting to make the page look nicer.  For help with textile formatting, see the note at the top of your edit page about the special formatting rules for fields with the green background (textile fields).  For help with HTML formatting, please Google "HTML Tutorial". You will receive a large number of free online tutorial pages that will teach you the basics of HTML (the language of the internet).

<h4>Submit and Cancel</h4>

These buttons are fairly self explanatory, but if you are confused, pressing the "Submit" button will save your information in the database, while pressing the "Cancel" button will discard your changes.))
   end

   def self.down
      drop_table :helps
   end
end
