# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class FixDna < ActiveRecord::Migration
  class Person < ActiveRecord::Base
     has_many :bookings, :class_name => 'FixDna::Booking'
  end
  class Booking < ActiveRecord::Base
     belongs_to :person, :class_name => 'FixDna::Person'
  end
  
  def self.up
   rename_column :people, :dna_date, :dna_swab_date
   add_column :people, :dna_blood_date, :date
   add_column :people, :dna_profile, :string
   add_column :people, :fingerprint_class, :string
   
   add_column :evidences, :dna_profile, :string
   add_column :evidences, :fingerprint_class, :string
   
   Booking.find(:all, :include => :person, :order => 'booking_date DESC', :conditions => 'bookings.fingerprint_class is not null and length(bookings.fingerprint_class) > 0').each do |b|
      if !b.person.nil? && b.person.fingerprint_class.blank?
         b.person.update_attribute(:fingerprint_class, b.fingerprint_class)
      end
   end
   
   Booking.find(:all, :include => :person, :order => 'booking_date DESC', :conditions => 'bookings.dna_profile is not null and length(bookings.dna_profile) > 0').each do |b|
      if !b.person.nil? && b.person.dna_profile.blank?
         b.person.update_attribute(:dna_profile, b.dna_profile)
      end
   end
   
   remove_column :bookings, :dna_profile
   remove_column :bookings, :fingerprint_class
  end

  def self.down
     add_column :bookings, :dna_profile, :string
     add_column :bookings, :fingerprint_class, :string
     
     Person.find(:all, :include => :bookings, :conditions => 'people.fingerprint_class is not null and length(people.fingerprint_class) > 0').each do |p|
        unless p.bookings.empty?
           p.bookings.last.update_attribute(:fingerprint_class, p.fingerprint_class)
        end
     end
     
     Person.find(:all, :include => :bookings, :conditions => 'people.dna_profile is not null and length(people.dna_profile) > 0').each do |p|
        unless p.bookings.empty?
           p.bookings.last.update_attribute(:dna_profile, p.dna_profile)
        end
     end
     
     remove_column :evidences, :dna_profile
     remove_column :evidences, :fingerprint_class
     
     remove_column :people, :dna_profile
     remove_column :people, :fingerprint_class
     remove_column :people, :dna_blood_date
     rename_column :people, :dna_swab_date, :dna_date
  end
end
