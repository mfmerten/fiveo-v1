# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateCharges < ActiveRecord::Migration
   def self.up
      create_table :charges, :force => true do |t|
         t.integer :d_arrest_id
         t.integer :c_arrest_id
         t.integer :o_arrest_id
         t.integer :citation_id
         t.integer :warrant_id
         t.integer :arrest_warrant_id
         t.integer :agency_id
         t.string :charge
         t.integer :count
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :charges, :d_arrest_id
      add_index :charges, :c_arrest_id
      add_index :charges, :o_arrest_id
      add_index :charges, :arrest_warrant_id
      add_index :charges, :agency_id
      add_index :charges, :citation_id
      add_index :charges, :warrant_id
      add_index :charges, :created_by
      add_index :charges, :updated_by
   end

   def self.down
      drop_table :charges
   end
end
