# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateBugs < ActiveRecord::Migration
   def self.up
      create_table :bugs, :force => true do |t|
         t.integer :reporter_id
         t.date :reported_date
         t.string :reported_time
         t.integer :assigned_id
         t.integer :admin_id
         t.integer :status_id
         t.integer :resolution_id
         t.date :resolution_date
         t.string :resolution_time
         t.integer :priority_id
         t.string :summary
         t.text :details
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :bugs, :reporter_id
      add_index :bugs, :assigned_id
      add_index :bugs, :admin_id
      add_index :bugs, :status_id
      add_index :bugs, :resolution_id
      add_index :bugs, :priority_id
      add_index :bugs, :created_by
      add_index :bugs, :updated_by

      create_table :watchers, :id => false, :force => true do |t|
         t.integer :bug_id
         t.integer :user_id
      end
      add_index :watchers, :bug_id
      add_index :watchers, :user_id

      create_table :bug_comments, :force => true do |t|
         t.integer :bug_id
         t.integer :user_id
         t.text :comments
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :bug_comments, :bug_id
      add_index :bug_comments, :user_id
      add_index :bug_comments, :created_by
      add_index :bug_comments, :updated_by
   end

   def self.down
      drop_table :bugs
      drop_table :watchers
      drop_table :bug_comments
   end
end
