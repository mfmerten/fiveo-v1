# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddFeesToCustomers < ActiveRecord::Migration
   class PaperCustomer < ActiveRecord::Base
   end
   
   def self.up
      add_column :paper_customers, :noservice_fee, :decimal, :precision => 12, :scale => 2, :default => 0
      add_column :paper_customers, :mileage_fee, :decimal, :precision => 12, :scale => 2, :default => 0
   
      PaperCustomer.update_all("mileage_fee = #{SiteConfig.mileage_fee}, noservice_fee = #{SiteConfig.noservice_fee}")
   end

   def self.down
      remove_column :paper_customers, :noservice_fee
      remove_column :paper_customers, :mileage_fee
   end
end
