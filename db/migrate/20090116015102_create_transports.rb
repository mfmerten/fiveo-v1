# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateTransports < ActiveRecord::Migration
   def self.up
      create_table :transports, :force => true do |t|
         t.integer :person_id
         t.string :officer1
         t.string :officer1_unit
         t.string :officer1_badge
         t.integer :officer1_id
         t.string :officer2
         t.string :officer2_unit
         t.string :officer2_badge
         t.integer :officer2_id
         t.date :begin_date
         t.string :begin_time
         t.integer :begin_miles
         t.date :end_date
         t.string :end_time
         t.integer :end_miles
         t.integer :estimated_miles
         t.string :from
         t.integer :from_id
         t.string :from_street
         t.string :from_city
         t.integer :from_state_id
         t.string :from_zip
         t.string :to
         t.integer :to_id
         t.string :to_street
         t.string :to_city
         t.integer :to_state_id
         t.string :to_zip
         t.text :remarks
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :transports, :person_id
      add_index :transports, :from_state_id
      add_index :transports, :to_state_id
      add_index :transports, :created_by
      add_index :transports, :updated_by
   end

   def self.down
      drop_table :transports
   end
end
