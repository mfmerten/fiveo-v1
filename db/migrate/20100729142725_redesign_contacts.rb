# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class RedesignContacts < ActiveRecord::Migration
   class Contact < ActiveRecord::Base
      attr_accessor :name
      # returns the contact full name (or agency name) optionally in last name
      # first order.
      def full_name(reverse = false)
         namehash = HashWithIndifferentAccess.new
         namehash[:first] = self.first
         namehash[:middle] = self.middle
         namehash[:last] = self.last
         namehash[:suffix] = self.suffix
         thename = unparse_name(namehash, reverse)
         if thename.strip.empty? && self.agency?
            return agency
         end
         thename.strip
      end
      
      # splits the entered name into its component parts
      def split_name
         if self.name.empty?
            self.first = nil
            self.middle = nil
            self.last = nil
            self.suffix = nil
         else
            nameparts = parse_name(self.name)
            self.first = nameparts[:first]
            self.middle = nameparts[:middle]
            self.last = nameparts[:last]
            self.suffix = nameparts[:suffix]
         end
      end
   end
   
   def self.up
      # 1. Add a business boolean field, a fullname field and an agency_id field
      add_column :contacts, :business, :boolean, :default => false
      add_column :contacts, :fullname, :string
      add_column :contacts, :agency_id, :integer
      add_index :contacts, :agency_id
      
      # 2. Run through all contacts and update the new fields
      Contact.find(:all).each do |c|
         unless c.last?
            c.update_attribute(:business, true)
         end
         c.update_attribute(:fullname, c.full_name(true))
         # attempt to link agencies unless this is a business record
         unless c.business?
            if temp = Contact.find(:first, :conditions => {:agency => c.agency, :business => true})
               # temp should be the actual agency contact
               c.update_attribute(:agency_id, temp.id)
            end
         end
      end
      
      # 3. remove the replaced fields.  This is the killer if everything is
      # not correct!
      remove_column :contacts, :last
      remove_column :contacts, :first
      remove_column :contacts, :middle
      remove_column :contacts, :suffix
      remove_column :contacts, :agency
   end

   def self.down
      # do the same in reverse
      add_column :contacts, :last, :string
      add_column :contacts, :first, :string
      add_column :contacts, :middle, :string
      add_column :contacts, :suffix, :string
      add_column :contacts, :agency, :string
      
      Contact.find(:all).each do |c|
         if c.business?
            c.agency = c.fullname
         else
            c.name = c.fullname
            c.split_name
            if temp = Contact.find_by_id(c.agency_id)
               c.agency = temp.fullname
            end
         end
         c.save
      end
      
      remove_column :contacts, :fullname
      remove_column :contacts, :business
      remove_column :contacts, :agency_id
   end
end
