# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class FixCourtDockets < ActiveRecord::Migration
   
   class Docket < ActiveRecord::Base
      has_many :courts, :class_name => 'FixCourtDockets::Court'
   end
   class Court < ActiveRecord::Base
      belongs_to :docket, :class_name => 'FixCourtDockets::Docket'
   end
   
   def self.up
      add_column :courts, :docket_type_id, :integer
      add_column :courts, :next_docket_type_id, :integer
      add_index :courts, :docket_type_id
      add_index :courts, :next_docket_type_id
      
      Docket.find(:all).each do |d|
         unless d.next_court_type_id.nil? || d.next_court_type_id == 0
            unless d.next_court_date.nil?
               unless d.next_judge_id.nil? || d.next_judge_id == 0
                  unless d.docket_type_id.nil? || d.docket_type_id == 0
                     unless court = Court.find(:first, :conditions => {:court_type_id => d.next_court_type_id, :court_date => d.next_court_date, :judge_id => d.next_judge_id, :docket_type_id => d.docket_type_id})
                        Court.new(:court_type_id => d.next_court_type_id, :court_date => d.next_court_date, :judge_id => d.next_judge_id, :docket_type_id => d.docket_type_id, :docket_id => d.id)
                     end
                  end
               end
            end
         end
      end
      
      remove_column :dockets, :next_court_type_id
      remove_column :dockets, :next_court_date
      remove_column :dockets, :next_judge_id
      remove_column :dockets, :docket_type_id
      remove_column :dockets, :needs_state_report
   end

   def self.down
      remove_column :courts, :docket_type_id
      remove_column :courts, :next_docket_type_id
      add_column :dockets, :next_court_type_id, :integer
      add_column :dockets, :next_court_date, :date
      add_column :dockets, :next_judge_id, :integer
      add_column :dockets, :docket_type_id, :integer
      add_column :dockets, :needs_state_report, :boolean
      add_index :dockets, :next_court_type_id
      add_index :dockets, :next_judge_id
      add_index :dockets, :docket_type_id
   end
end
