# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class FixesFor090 < ActiveRecord::Migration
   class Person < ActiveRecord::Base
      has_many :bookings, :class_name => 'FixesFor090::Booking'
   end
   class Booking < ActiveRecord::Base
      belongs_to :person, :class_name => 'FixesFor090::Person'
   end
   
   def self.up
      add_column :contacts, :legacy, :boolean, :default => false
      add_column :people, :commissary_facility_id, :integer
      add_index :people, :commissary_facility_id
      
      Person.find(:all).each do |p|
         next if p.bookings.empty?
         unless p.commissary_facility_id == p.bookings.last.facility_id
            p.update_attribute(:commissary_facility_id, p.bookings.last.facility_id)
         end
      end
   end

   def self.down
      remove_column :contacts, :legacy
      remove_column :people, :commissary_facility_id
   end
end
