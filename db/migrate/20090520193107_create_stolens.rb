# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateStolens < ActiveRecord::Migration
   def self.up
      create_table :stolens, :force => true do |t|
         t.string :case_no
         t.integer :person_id
         t.date :stolen_date
         t.string :model_no
         t.string :serial_no
         t.decimal :item_value, :precision => 12, :scale => 2, :default => 0
         t.string :item
         t.text :item_description
         t.date :recovered_date
         t.date :returned_date
         t.text :remarks
         t.boolean :private, :default => false
         t.integer :investigation_id
         t.integer :invest_crime_id
         t.integer :invest_criminal_id
         t.integer :owned_by
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :stolens, :case_no
      add_index :stolens, :person_id
      add_index :stolens, :created_by
      add_index :stolens, :updated_by
      add_index :stolens, :private
      add_index :stolens, :investigation_id
      add_index :stolens, :invest_crime_id
      add_index :stolens, :invest_criminal_id
      add_index :stolens, :owned_by
   end

   def self.down
      drop_table :stolens
   end
end
