# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddDocketsEtAl < ActiveRecord::Migration
   def self.up
      create_table :dockets, :force => true do |t|
         t.string :docket_no
         t.integer :person_id
         t.integer :next_court_type_id
         t.date :next_court_date
         t.integer :next_judge_id
         t.integer :docket_type_id
         t.boolean :needs_state_report, :default => false
         t.date :state_report_date
         t.boolean :processed, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :dockets, :docket_no
      add_index :dockets, :person_id
      add_index :dockets, :next_court_type_id
      add_index :dockets, :next_judge_id
      add_index :dockets, :docket_type_id
      add_index :dockets, :created_by
      add_index :dockets, :updated_by

      create_table :courts, :force => true do |t|
         t.integer :docket_id
         t.integer :judge_id
         t.date :court_date
         t.integer :court_type_id
         t.date :next_court_date
         t.integer :next_court_type_id
         t.integer :next_judge_id
         t.boolean :needs_state_report, :default => false
         t.boolean :bench_warrant_issued, :default => false
         t.boolean :bond_forfeiture_issued, :default => false
         t.boolean :is_upset_wo_refix, :default => false
         t.boolean :is_refixed, :default => false
         t.string :refix_reason
         t.boolean :psi, :default => false
         t.integer :plea_type_id
         t.boolean :is_dismissed, :default => false
         t.boolean :is_overturned, :default => false
         t.date :overturned_date
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :courts, :docket_id
      add_index :courts, :judge_id
      add_index :courts, :court_type_id
      add_index :courts, :next_court_type_id
      add_index :courts, :next_judge_id
      add_index :courts, :plea_type_id
      add_index :courts, :created_by
      add_index :courts, :updated_by

      create_table :citations, :force => true do |t|
         t.date :citation_date
         t.integer :offense_id
         t.string :citation_time
         t.integer :person_id
         t.integer :officer_id
         t.string :officer
         t.string :officer_unit
         t.string :officer_badge
         t.string :ticket_no
         t.date :paid_date
         t.date :recalled_date
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :citations, :person_id
      add_index :citations, :offense_id
      add_index :citations, :created_by
      add_index :citations, :updated_by

      create_table :sentences, :force => true do |t|
         t.integer :docket_id
         t.integer :court_id
         t.integer :probation_id
         t.integer :da_charge_id
         t.integer :result_id
         t.string :conviction
         t.integer :conviction_count
         t.boolean :doc, :default => false
         t.decimal :fines, :precision => 12, :scale => 2, :default => 0
         t.integer :court_cost_type_id
         t.decimal :costs, :precision => 12, :scale => 2, :default => 0
         t.integer :default_days
         t.integer :default_months
         t.integer :default_years
         t.date :pay_by_date
         t.integer :sentence_days
         t.integer :sentence_months
         t.integer :sentence_years
         t.boolean :suspended, :default => false
         t.integer :suspended_except_days
         t.integer :suspended_except_months
         t.integer :suspended_except_years
         t.integer :probation_days
         t.integer :probation_months
         t.integer :probation_years
         t.integer :probation_type_id
         t.date :restitution_date
         t.boolean :credit_time_served, :default => false
         t.integer :community_service_days
         t.boolean :substance_abuse_program, :default => false
         t.boolean :driver_improvement, :default => false
         t.decimal :probation_fees, :precision => 12, :scale => 2, :default => 0
         t.decimal :idf_amount, :precision => 12, :scale => 2, :default => 0
         t.decimal :dare_amount, :precision => 12, :scale => 2, :default => 0
         t.decimal :restitution_amount, :precision => 12, :scale => 2, :default => 0
         t.boolean :anger_management, :default => false
         t.boolean :substance_abuse_treatment, :default => false
         t.boolean :random_drug_screens, :default => false
         t.boolean :no_victim_contact, :default => false
         t.boolean :art_893, :default => false
         t.boolean :art_894, :default => false
         t.boolean :art_895, :default => false
         t.boolean :probation_reverts, :default => false
         t.string :probation_revert_conditions
         t.boolean :wo_benefit, :default => false
         t.boolean :parish_jail, :default => false
         t.integer :consecutive_type_id
         t.boolean :sentence_consecutive, :default => false
         t.string :sentence_notes
         t.boolean :fines_consecutive, :default => false
         t.string :fines_notes
         t.boolean :costs_consecutive, :default => false
         t.string :costs_notes
         t.text :notes
         t.text :remarks
         t.boolean :processed, :default => false
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :sentences, :docket_id
      add_index :sentences, :court_id
      add_index :sentences, :probation_id
      add_index :sentences, :da_charge_id
      add_index :sentences, :result_id
      add_index :sentences, :created_by
      add_index :sentences, :updated_by
      add_index :sentences, :court_cost_type_id
      add_index :sentences, :consecutive_type_id
      add_index :sentences, :probation_type_id
   
      create_table :dockets_sentences, :force => true, :id => false do |t|
         t.integer :docket_id
         t.integer :sentence_id
      end
      add_index :dockets_sentences, :docket_id
      add_index :dockets_sentences, :sentence_id
   
      create_table :da_charges, :force => true do |t|
         t.integer :docket_id
         t.integer :charge_id
         t.integer :arrest_id
         t.integer :count, :default => 1
         t.string :charge
         t.boolean :dropped_by_da, :default => false
         t.boolean :processed, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :da_charges, :docket_id
      add_index :da_charges, :charge_id
      add_index :da_charges, :arrest_id
      add_index :da_charges, :created_by
      add_index :da_charges, :updated_by
   
      create_table :revocations, :force => true do |t|
         t.date :revocation_date
         t.string :revocation_time
         t.integer :docket_id
         t.integer :judge_id
         t.integer :arrest_id
         t.boolean :revoked
         t.boolean :time_served
         t.integer :consecutive_type_id
         t.integer :total_days
         t.integer :total_months
         t.integer :total_years
         t.boolean :doc
         t.integer :parish_days
         t.integer :parish_months
         t.integer :parish_years
         t.boolean :processed
         t.text :remarks
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :revocations, :docket_id
      add_index :revocations, :judge_id
      add_index :revocations, :arrest_id
      add_index :revocations, :consecutive_type_id
      add_index :revocations, :created_by
      add_index :revocations, :updated_by
      
      create_table :dockets_revocations, :force => true, :id => false do |t|
         t.integer :docket_id
         t.integer :revocation_id
      end
      add_index :dockets_revocations, :docket_id
      add_index :dockets_revocations, :revocation_id
   end

   def self.down
      drop_table :dockets
      drop_table :courts
      drop_table :citations
      drop_table :sentences
      drop_table :dockets_sentences
      drop_table :da_charges
      drop_table :revocations
      drop_table :dockets_revocations
   end
end
