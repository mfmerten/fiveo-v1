# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateMessages < ActiveRecord::Migration
   def self.up
      create_table :messages, :force => true do |t|
         t.boolean  :receiver_deleted, :receiver_purged, :sender_deleted, :sender_purged
         t.datetime :read_at
         t.integer  :receiver_id, :sender_id
         t.string   :subject, :null => false
         t.text     :body
         t.timestamps 
      end
      add_index :messages, :sender_id
      add_index :messages, :receiver_id
   end

   def self.down
      drop_table :messages
   end
end
