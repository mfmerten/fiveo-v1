# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddSentenceHours < ActiveRecord::Migration
   def self.up
      add_column :docs, :sentence_hours, :integer, :default => 0
      change_column_default :docs, :sentence_days, 0
      change_column_default :docs, :sentence_months, 0
      change_column_default :docs, :sentence_years, 0
      add_column :docs, :to_serve_hours, :integer, :default => 0
      change_column_default :docs, :to_serve_days, 0
      change_column_default :docs, :to_serve_months, 0
      change_column_default :docs, :to_serve_years, 0
      add_column :probations, :probation_hours, :integer, :default => 0
      add_column :probations, :default_hours, :integer, :default => 0
      add_column :probations, :sentence_hours, :integer, :default => 0
      add_column :probations, :suspended_except_hours, :integer, :default => 0
      add_column :revocations, :total_hours, :integer, :default => 0
      change_column_default :revocations, :total_days, 0
      change_column_default :revocations, :total_months, 0
      change_column_default :revocations, :total_years, 0
      add_column :revocations, :parish_hours, :integer, :default => 0
      change_column_default :revocations, :parish_days, 0
      change_column_default :revocations, :parish_months, 0
      change_column_default :revocations, :parish_years, 0
      add_column :sentences, :default_hours, :integer, :default => 0
      change_column_default :sentences, :default_days, 0
      change_column_default :sentences, :default_months, 0
      change_column_default :sentences, :default_years, 0
      add_column :sentences, :sentence_hours, :integer, :default => 0
      change_column_default :sentences, :sentence_days, 0
      change_column_default :sentences, :sentence_months, 0
      change_column_default :sentences, :sentence_years, 0
      add_column :sentences, :suspended_except_hours, :integer, :default => 0
      change_column_default :sentences, :suspended_except_days, 0
      change_column_default :sentences, :suspended_except_months, 0
      change_column_default :sentences, :suspended_except_years, 0
      add_column :sentences, :probation_hours, :integer, :default => 0
      change_column_default :sentences, :probation_days, 0
      change_column_default :sentences, :probation_months, 0
      change_column_default :sentences, :probation_years, 0
   end

   def self.down
      remove_column :docs, :sentence_hours
      remove_column :docs, :to_serve_hours
      remove_column :probations, :probation_hours
      remove_column :probations, :default_hours
      remove_column :probations, :sentence_hours
      remove_column :probations, :suspended_except_hours
      remove_column :revocations, :total_hours
      remove_column :revocations, :parish_hours
      remove_column :sentences, :default_hours
      remove_column :sentences, :sentence_hours
      remove_column :sentences, :suspended_except_hours
      remove_column :sentences, :probation_hours
   end
end
