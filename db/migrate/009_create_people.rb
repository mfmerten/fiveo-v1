# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreatePeople < ActiveRecord::Migration
   def self.up
      create_table :people, :force => true do |t|
         t.string :lastname
         t.string :firstname
         t.string :middlename
         t.string :suffix
         t.string :aka
         t.integer :is_alias_for
         t.string :phys_street
         t.string :phys_city
         t.integer :phys_state_id
         t.string :phys_zip
         t.string :mail_street
         t.string :mail_city
         t.integer :mail_state_id
         t.string :mail_zip
         t.date :date_of_birth
         t.string :place_of_birth
         t.integer :race_id
         t.integer :sex
         t.string :fbi
         t.string :sid
         t.string :ssn
         t.string :oln
         t.integer :oln_state_id
         t.string :home_phone
         t.string :cell_phone
         t.string :email
         t.integer :build_id
         t.integer :height_ft
         t.integer :height_in
         t.integer :weight
         t.integer :hair_color_id
         t.integer :hair_type_id
         t.integer :eye_color_id
         t.boolean :glasses
         t.string :shoe_size
         t.integer :complexion_id
         t.integer :facial_hair_id
         t.string :occupation
         t.string :employer
         t.string :emergency_contact
         t.integer :em_relationship_id
         t.string :emergency_address
         t.string :emergency_phone
         t.string :med_allergies
         t.boolean :hepatitis
         t.boolean :hiv
         t.boolean :tb
         t.boolean :deceased
         t.date :dna_date
         t.string :gang_affiliation
         t.text :remarks
         t.string :doc_number
         t.string :front_mugshot_file_name
         t.string :side_mugshot_file_name
         t.string :front_mugshot_content_type
         t.string :side_mugshot_content_type
         t.integer :front_mugshot_file_size
         t.integer :side_mugshot_file_size
         t.datetime :front_mugshot_updated_at
         t.datetime :side_mugshot_updated_at
         t.decimal :commissary_balance, :precision => 12, :scale => 2, :default => 0
         t.boolean :merge_locked, :default => false
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :people, :is_alias_for
      add_index :people, :phys_state_id
      add_index :people, :mail_state_id
      add_index :people, :race_id
      add_index :people, :oln_state_id
      add_index :people, :build_id
      add_index :people, :hair_color_id
      add_index :people, :hair_type_id
      add_index :people, :eye_color_id
      add_index :people, :complexion_id
      add_index :people, :facial_hair_id
      add_index :people, :em_relationship_id
      add_index :people, :lastname
      add_index :people, :firstname
      add_index :people, :middlename
      add_index :people, :suffix
      add_index :people, :ssn
      add_index :people, :oln
      add_index :people, :created_by
      add_index :people, :updated_by

      create_table :associates, :force => true do |t|
         t.integer :person_id
         t.integer :relationship_id
         t.string :name
         t.text :remarks
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :associates, :relationship_id
      add_index :associates, :person_id
      add_index :associates, :created_by
      add_index :associates, :updated_by

      create_table :marks, :force => true do |t|
         t.integer :person_id
         t.integer :location_id
         t.string :description
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :marks, :location_id
      add_index :marks, :person_id
      add_index :marks, :created_by
      add_index :marks, :updated_by
   
      create_table :medications, :force => true do |t|
         t.integer :person_id
         t.integer :prescription_no
         t.integer :physician_id
         t.integer :pharmacy_id
         t.string :drug_name
         t.string :dosage
         t.string :warnings
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :medications, :person_id
      add_index :medications, :prescription_no
      add_index :medications, :physician_id
      add_index :medications, :pharmacy_id
      add_index :medications, :created_by
      add_index :medications, :updated_by
      
      create_table :med_schedules, :force => true do |t|
         t.integer :medication_id
         t.string :dose
         t.string :time
         t.timestamps
      end
      add_index :med_schedules, :medication_id
      add_index :med_schedules, :time
   
      create_table :people_sentences, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :sentence_id
      end
      add_index :people_sentences, :person_id
      add_index :people_sentences, :sentence_id
      
      create_table :people_revocations, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :revocation_id
      end
      add_index :people_revocations, :person_id
      add_index :people_revocations, :revocation_id
      
      create_table :people_docs, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :doc_id
      end
      add_index :people_docs, :person_id
      add_index :people_docs, :doc_id
   end

   def self.down
      drop_table :people
      drop_table :associates
      drop_table :marks
      drop_table :medications
      drop_table :med_schedules
      drop_table :people_sentences
      drop_table :people_revocations
      drop_table :people_docs
   end
end
