# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class RemoveReceipt < ActiveRecord::Migration
   def self.up
      drop_table :receipts
      remove_column :bookings, :prop_return_date
      remove_column :bookings, :prop_return_time
      remove_column :bookings, :prop_destroy_date
      remove_column :bookings, :prop_destroy_time
      remove_column :bookings, :prop_dispo_officer
      remove_column :bookings, :prop_dispo_officer_id
      remove_column :bookings, :prop_dispo_officer_unit
      remove_column :bookings, :prop_dispo_officer_badge
   end

   def self.down
      create_table :receipts, :force => true do |t|
         t.string :assigned
         t.timestamps
      end
      add_column :bookings, :prop_return_date, :date
      add_column :bookings, :prop_return_time, :string
      add_column :bookings, :prop_destroy_date, :date
      add_column :bookings, :prop_destroy_time, :string
      add_column :bookings, :prop_dispo_officer, :string
      add_column :bookings, :prop_dispo_officer_id, :integer
      add_column :bookings, :prop_dispo_officer_unit, :string
      add_column :bookings, :prop_dispo_officer_badge, :string
   end
end
