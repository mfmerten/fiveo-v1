# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddMileageToSite < ActiveRecord::Migration
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => "AddMileageToSite::Option"
   end
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => "AddMileageToSite::OptionType"
   end
  def self.up
     # add mileage to site configuration
     SiteConfig.mileage_fee = SiteConfigDefault.mileage_fee
     SiteConfig.noservice_fee = SiteConfigDefault.noservice_fee
     if conf = File.open("#{RAILS_ROOT}/public/system/site_configuration.yml","w")
        conf.puts(SiteConfig.marshal_dump.to_yaml)
        conf.close
     end
     
     # remove mileage from paper type options
     if ot = OptionType.find_by_name("Paper Type")
        if o = Option.find(:first, :conditions => {:option_type_id => ot.id, :long_name => 'Mileage'})
           o.destroy
        end
        if o = Option.find(:first, :conditions => {:option_type_id => ot.id, :long_name => 'Unable To Locate'})
           o.destroy
        end
     end
  end

  def self.down
  end
end
