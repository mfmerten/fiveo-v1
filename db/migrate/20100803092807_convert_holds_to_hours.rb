# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class ConvertHoldsToHours < ActiveRecord::Migration
  class Hold < ActiveRecord::Base
  end
  def self.up
     add_column :holds, :hours_total, :integer
     add_column :holds, :hours_time_served, :integer
     
     Hold.find(:all).each do |h|
        unless h.days_total.nil?
           h.hours_total = h.days_total * 24
        end
        unless h.days_time_served.nil?
           h.hours_time_served = h.days_time_served * 24
        end
        unless h.days_served.nil?
           day_hours = h.days_served * 24
           if day_hours > h.hours_served
              h.hours_served = day_hours
            end
         end
         unless h.days_goodtime.nil?
            day_hours = h.days_goodtime * 24
            if day_hours > h.hours_goodtime
               h.hours_goodtime = day_hours
            end
         end
         h.save
      end
      
      remove_column :holds, :days_total
      remove_column :holds, :days_served
      remove_column :holds, :days_goodtime
      remove_column :holds, :days_time_served
  end

  def self.down
  end
end
