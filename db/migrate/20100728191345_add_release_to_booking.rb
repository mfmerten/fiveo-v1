# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddReleaseToBooking < ActiveRecord::Migration
  class Booking < ActiveRecord::Base
  end
  class Option < ActiveRecord::Base
     belongs_to :option_type, :class_name => 'AddReleaseToBooking::OptionType'
  end
  class OptionType < ActiveRecord::Base
     has_many :options, :class_name => 'AddReleaseToBooking::Option', :dependent => :destroy
  end
  
  def self.up
   add_column :bookings, :released_because_id, :integer
   add_index :bookings, :released_because_id
   
   unless ot = OptionType.find_by_name("Release Reason")
      ot = OptionType.create(:name => "Release Reason")
   end
   
   legacy = Option.create(:option_type_id => ot.id, :long_name => "Legacy Release", :permanent => true, :disabled => true)
   ot.options.create(:long_name => "Bonded")
   ot.options.create(:long_name => "Time Served")
   ot.options.create(:long_name => "Transferred")
   ot.options.create(:long_name => "Escaped")
   ot.options.create(:long_name => "Deceased")
   ot.options.create(:long_name => "Released W/O Charges")
   ot.options.create(:long_name => "48 Hour Expired")
   ot.options.create(:long_name => "Released By Judge")
   ot.options.create(:long_name => "Paroled")
   ot.options.create(:long_name => "Pardoned")
   ot.options.create(:long_name => "Unknown")
   
   Booking.find(:all).reject{|b| b.release_date.nil?}.each{|b| b.update_attribute(:released_because_id, legacy.id)}
  end

  def self.down
     if ot = OptionType.find_by_name("Release Reason")
        ot.destroy
     end
     remove_column :bookings, :released_because_id
  end
end
