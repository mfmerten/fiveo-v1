# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddDispatchShifts < ActiveRecord::Migration
  def self.up
     SiteConfig.dispatch_shift1_start = SiteConfigDefault.dispatch_shift1_start
     SiteConfig.dispatch_shift1_stop = SiteConfigDefault.dispatch_shift1_stop
     SiteConfig.dispatch_shift2_start = SiteConfigDefault.dispatch_shift2_start
     SiteConfig.dispatch_shift2_stop = SiteConfigDefault.dispatch_shift2_stop
     SiteConfig.dispatch_shift3_start = SiteConfigDefault.dispatch_shift3_start
     SiteConfig.dispatch_shift3_stop = SiteConfigDefault.dispatch_shift3_stop
     if conf = File.open("#{RAILS_ROOT}/public/system/site_configuration.yml","w")
        conf.puts(SiteConfig.marshal_dump.to_yaml)
        conf.close
     end
  end

  def self.down
  end
end
