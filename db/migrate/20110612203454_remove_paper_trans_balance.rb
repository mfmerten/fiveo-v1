# ===================================================================
# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
#
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# =====================================================================
class RemovePaperTransBalance < ActiveRecord::Migration
   class PaperPayment < ActiveRecord::Base
   end

   def self.up
      remove_column :paper_payments, :balance
      
      # adding this to prevent users from deleting any existing transactions
      # because the way transactions were handled prior to this commit is not
      # compatible with the new delete feature.  If this is not done, existing
      # unposted transactions may exist that when deleted would cause incorrect
      # balance_due for the customer.
      add_column :paper_payments, :trans_not_deletable, :boolean, :default => false
      PaperPayment.reset_column_information
      PaperPayment.update_all('trans_not_deletable = true')
   end

   def self.down
      add_column :paper_payments, :balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
      remove_column :paper_payments, :trans_not_deletable
   end
end
