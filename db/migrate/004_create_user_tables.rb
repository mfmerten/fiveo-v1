# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateUserTables < ActiveRecord::Migration
   def self.up
      create_table :groups_users, :id => false, :force => true do |t|
         t.integer :group_id
         t.integer :user_id
      end
      add_index :groups_users, :group_id
      add_index :groups_users, :user_id

      create_table :groups, :force => true do |t|
         t.string :name
         t.string :description
         t.string :permissions
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :groups, :name
      add_index :groups, :created_by
      add_index :groups, :updated_by

      create_table :users, :force => true do |t|
         t.string :login
         t.integer :contact_id
         t.boolean :locked, :default => false
         t.string :hashed_password
         t.string :salt
         t.integer :items_per_page, :default => 15
         t.date :expires
         t.string :disabled_text
         t.string :user_photo_file_name
         t.string :user_photo_content_type
         t.integer :user_photo_file_size
         t.datetime :user_photo_updated_at
         t.boolean :send_email, :default => false
         t.boolean :no_self_bug_messages, :default => true
         t.integer :last_call, :default => 0
         t.integer :last_pawn, :default => 0
         t.integer :last_arrest, :default => 0
         t.integer :last_bug, :default => 0
         t.integer :last_forbid, :default => 0
         t.boolean :review_forbid, :default => true
         t.boolean :review_arrest, :default => true
         t.boolean :review_pawn, :default => true
         t.boolean :review_bug, :default => true
         t.boolean :review_call, :default => true
         t.integer :last_warrant, :default => 0
         t.boolean :review_warrant, :default => true
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :users, :login
      add_index :users, :contact_id
      add_index :users, :created_by
      add_index :users, :updated_by
   end

   def self.down
      drop_table :users
      drop_table :groups_users
      drop_table :groups
   end
end
