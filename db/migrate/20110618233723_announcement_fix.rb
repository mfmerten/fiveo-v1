# ===================================================================
# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
#
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# =====================================================================
class AnnouncementFix < ActiveRecord::Migration
   class Announcement < ActiveRecord::Base
   end
   def self.up
      # remove dates
      remove_column :announcements, :start_date
      remove_column :announcements, :stop_date
      add_column :announcements, :unignorable, :boolean, :default => false
      
      # add user join table
      create_table :announce_ignores, :id => false, :force => true do |t|
         t.integer :user_id
         t.integer :announcement_id
      end
      add_index :announce_ignores, :user_id
      add_index :announce_ignores, :announcement_id
   end

   def self.down
      add_column :announcements, :start_date, :date
      add_column :announcements, :stop_date, :date
      drop_table :announce_ignores
      
      # add required start date
      Announcement.reset_column_information
      Announcement.update_all("start_date = '#{Date.today}'")
   end
end
