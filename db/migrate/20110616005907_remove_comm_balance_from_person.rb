# ===================================================================
# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
#
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# =====================================================================
class RemoveCommBalanceFromPerson < ActiveRecord::Migration
   class Person < ActiveRecord::Base
      has_many :commissaries, :class_name => 'RemoveCommBalanceFromPerson::Commissary'
   end
   class Commissary < ActiveRecord::Base
      belongs_to :person, :class_name => 'RemoveCommBalanceFromPerson::Person'
   end

   def self.up
      remove_column :people, :commissary_balance
   end

   def self.down
      add_column :people, :commissary_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
      People.reset_column_information

      # recalculate balances from existing transactions
      trans = Commissary.find(:all, :select => 'person_id, sum(deposit - withdraw) as balance', :group => 'person_id')
      trans.each do |t|
         if p = Person.find_by_id(t.person_id)
            p.update_attribute(:commissary_balance, t.balance.to_d)
         end
      end
   end
end
