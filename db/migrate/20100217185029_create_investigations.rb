# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateInvestigations < ActiveRecord::Migration
   def self.up
      create_table :investigations, :force => true do |t|
         t.date :investigation_date
         t.string :investigation_time
         t.string :detective
         t.integer :detective_id
         t.string :detective_badge
         t.string :detective_unit
         t.boolean :private, :default => true
         t.integer :status_id
         t.text :details
         t.text :remarks
         t.integer :owned_by
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :investigations, :detective_id
      add_index :investigations, :status_id
      add_index :investigations, :created_by
      add_index :investigations, :updated_by
      add_index :investigations, :private
      add_index :investigations, :owned_by
      
      create_table :invest_reports, :force => true do |t|
         t.integer :investigation_id
         t.integer :invest_crime_id
         t.date :report_date
         t.string :report_time
         t.text :details
         t.text :remarks
         t.boolean :private, :default => true
         t.integer :owned_by
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :invest_reports, :investigation_id
      add_index :invest_reports, :invest_crime_id
      add_index :invest_reports, :private
      add_index :invest_reports, :created_by
      add_index :invest_reports, :updated_by
      add_index :invest_reports, :owned_by
      
      create_table :invest_cases, :force => true do |t|
         t.integer :investigation_id
         t.string :case_no
         t.boolean :private, :default => true
         t.text :remarks
         t.integer :invest_crime_id
         t.integer :owned_by
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :invest_cases, :investigation_id
      add_index :invest_cases, :invest_crime_id
      add_index :invest_cases, :case_no
      add_index :invest_cases, :private
      add_index :invest_cases, :created_by
      add_index :invest_cases, :updated_by
      add_index :invest_cases, :owned_by
      
      create_table :contacts_investigations, :force => true, :id => false do |t|
         t.integer :contact_id
         t.integer :investigation_id
      end
      add_index :contacts_investigations, :contact_id
      add_index :contacts_investigations, :investigation_id
      
      create_table :invest_victims, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :investigation_id
      end
      add_index :invest_victims, :person_id
      add_index :invest_victims, :investigation_id
      
      create_table :invest_witnesses, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :investigation_id
      end
      add_index :invest_witnesses, :person_id
      add_index :invest_witnesses, :investigation_id
      
      create_table :invest_sources, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :investigation_id
      end
      add_index :invest_sources, :person_id
      add_index :invest_sources, :investigation_id
      
      create_table :invest_report_sources, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :invest_report_id
      end
      add_index :invest_report_sources, :person_id
      add_index :invest_report_sources, :invest_report_id
      
      create_table :invest_crime_sources, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :invest_crime_id
      end
      add_index :invest_crime_sources, :person_id
      add_index :invest_crime_sources, :invest_crime_id
      
      create_table :invest_photos, :force => true do |t|
         t.integer :investigation_id
         t.integer :invest_crime_id
         t.integer :invest_report_id
         t.integer :invest_criminal_id
         t.integer :invest_vehicle_id
         t.string :invest_photo_file_name
         t.string :invest_photo_content_type
         t.integer :invest_photo_file_size
         t.datetime :invest_photo_updated_at
         t.boolean :private, :default => true
         t.string :description
         t.date :date_taken
         t.string :location_taken
         t.string :taken_by
         t.integer :taken_by_id
         t.string :taken_by_unit
         t.string :taken_by_badge
         t.text :remarks
         t.integer :owned_by
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :invest_photos, :investigation_id
      add_index :invest_photos, :invest_crime_id
      add_index :invest_photos, :invest_report_id
      add_index :invest_photos, :invest_criminal_id
      add_index :invest_photos, :invest_vehicle_id
      add_index :invest_photos, :private
      add_index :invest_photos, :created_by
      add_index :invest_photos, :updated_by
      add_index :invest_photos, :owned_by
      
      create_table :invest_vehicles, :force => true do |t|
         t.integer :investigation_id
         t.integer :invest_criminal_id
         t.integer :invest_crime_id
         t.integer :invest_report_id
         t.boolean :private, :default => true
         t.integer :owner_id
         t.string :make
         t.string :model
         t.string :vin
         t.string :license
         t.integer :license_state_id
         t.string :color
         t.string :tires
         t.string :model_year
         t.integer :vehicle_type_id
         t.text :description
         t.text :remarks
         t.integer :owned_by
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :invest_vehicles, :investigation_id
      add_index :invest_vehicles, :invest_criminal_id
      add_index :invest_vehicles, :invest_crime_id
      add_index :invest_vehicles, :invest_report_id
      add_index :invest_vehicles, :owner_id
      add_index :invest_vehicles, :private
      add_index :invest_vehicles, :license_state_id
      add_index :invest_vehicles, :vehicle_type_id
      add_index :invest_vehicles, :created_by
      add_index :invest_vehicles, :updated_by
      add_index :invest_vehicles, :owned_by
   
      create_table :invest_crimes, :force => true do |t|
         t.integer :investigation_id
         t.boolean :private, :default => true
         t.string :motive
         t.string :location
         t.boolean :occupied
         t.string :entry_gained
         t.string :weapons_used
         t.integer :victim_relationship_id
         t.text :facts_prior
         t.text :facts_after
         t.text :targets
         t.string :security
         t.string :impersonated
         t.text :actions_taken
         t.text :threats_made
         t.text :details
         t.text :remarks
         t.integer :owned_by
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :invest_crimes, :investigation_id
      add_index :invest_crimes, :victim_relationship_id
      add_index :invest_crimes, :created_by
      add_index :invest_crimes, :updated_by
      add_index :invest_crimes, :owned_by
      
      create_table :invest_criminals, :force => true do |t|
         t.integer :investigation_id
         t.integer :invest_crime_id
         t.integer :invest_report_id
         t.integer :person_id
         t.string :full_name
         t.string :description
         t.string :height
         t.string :weight
         t.string :shoes
         t.string :fingerprint_class
         t.string :dna_profile
         t.boolean :private, :default => true
         t.text :remarks
         t.integer :owned_by
         t.integer :created_by
         t.integer :updated_by
         t.timestamps
      end
      add_index :invest_criminals, :investigation_id
      add_index :invest_criminals, :invest_crime_id
      add_index :invest_criminals, :invest_report_id
      add_index :invest_criminals, :person_id
      add_index :invest_criminals, :private
      add_index :invest_criminals, :created_by
      add_index :invest_criminals, :updated_by
      add_index :invest_criminals, :owned_by
      
      create_table :crime_victims, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :invest_crime_id
      end
      add_index :crime_victims, :person_id
      add_index :crime_victims, :invest_crime_id
      
      create_table :crime_witnesses, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :invest_crime_id
      end
      add_index :crime_witnesses, :person_id
      add_index :crime_witnesses, :invest_crime_id
      
      create_table :report_victims, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :invest_report_id
      end
      add_index :report_victims, :person_id
      add_index :report_victims, :invest_report_id
      
      create_table :report_witnesses, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :invest_report_id
      end
      add_index :report_witnesses, :person_id
      add_index :report_witnesses, :invest_report_id
   end

   def self.down
      drop_table :invest_criminals
      drop_table :invest_crimes
      drop_table :invest_vehicles
      drop_table :invest_photos
      drop_table :invest_crime_sources
      drop_table :invest_report_sources
      drop_table :invest_sources
      drop_table :invest_witnesses
      drop_table :invest_victims
      drop_table :contacts_investigations
      drop_table :invest_cases
      drop_table :invest_reports
      drop_table :investigations
      drop_table :report_witnesses
      drop_table :report_victims
      drop_table :crime_witnesses
      drop_table :crime_victims
   end
end
