# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateEvidences < ActiveRecord::Migration
   def self.up
      create_table :evidences, :force => true do |t|
         t.integer :offense_id
         t.string :evidence_number
         t.string :case_no
         t.text :description
         t.integer :owner_id
         t.text :remarks
         t.string :evidence_photo_file_name
         t.string :evidence_photo_content_type
         t.integer :evidence_photo_file_size
         t.datetime :evidence_photo_updated_at
         t.integer :investigation_id
         t.integer :invest_crime_id
         t.boolean :private, :default => false
         t.boolean :legacy, :default => false
         t.integer :owned_by
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :evidences, :offense_id
      add_index :evidences, :case_no
      add_index :evidences, :owner_id
      add_index :evidences, :investigation_id
      add_index :evidences, :invest_crime_id
      add_index :evidences, :private
      add_index :evidences, :created_by
      add_index :evidences, :updated_by
      add_index :evidences, :owned_by

      create_table :evidence_locations, :force => true do |t|
         t.integer :evidence_id
         t.string :location_from
         t.string :location_to
         t.string :officer
         t.string :officer_unit
         t.string :officer_badge
         t.integer :officer_id
         t.date :storage_date
         t.string :storage_time
         t.text :remarks
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :evidence_locations, :evidence_id
      add_index :evidence_locations, :created_by
      add_index :evidence_locations, :updated_by
   end

   def self.down
      drop_table :evidences
      drop_table :evidence_locations
   end
end
