# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddTemporaryRelease < ActiveRecord::Migration
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => 'AddTemporaryRelease::Option', :dependent => :destroy
   end
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => 'AddTemporaryRelease::OptionType'
   end
   
   def self.up
      if ot = OptionType.find_by_name('Hold Type')
         unless o = Option.find(:first, :conditions => {:option_type_id => ot.id, :long_name => 'Temporary Release'})
            o = Option.create(:option_type_id => ot.id, :long_name => 'Temporary Release', :permanent => true, :disabled => true, :created_by => 1, :updated_by => 1)
         end
      end
      unless ot = OptionType.find_by_name('Temporary Release Reason')
         ot = OptionType.create(:name => 'Temporary Release Reason')
      end
      ot.options.create(:long_name => 'Work Release')
      ot.options.create(:long_name => 'Awaiting Execution Of Sentence')
      ot.options.create(:long_name => 'Escaped')

      remove_column :bookings, :serve_by_the_hour
      add_column :holds, :temp_release_reason_id, :integer
      add_index :holds, :temp_release_reason_id
      add_column :sentences, :delay_execution, :boolean, :default => false
      add_column :revocations, :delay_execution, :boolean, :default => false
   end

   def self.down
   end
end
