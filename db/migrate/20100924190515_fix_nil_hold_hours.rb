# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class FixNilHoldHours < ActiveRecord::Migration
  class Hold < ActiveRecord::Base
  end
  
  def self.up
   Hold.update_all("hours_total = 0","hours_total IS NULL")
   Hold.update_all("hours_served = 0","hours_served IS NULL")
   Hold.update_all("hours_goodtime = 0","hours_goodtime IS NULL")
   Hold.update_all("hours_time_served = 0","hours_time_served IS NULL")
  end

  def self.down
  end
end
