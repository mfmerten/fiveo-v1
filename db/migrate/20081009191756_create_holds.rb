# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateHolds < ActiveRecord::Migration
   def self.up
      create_table :holds, :force => true do |t|
         t.integer :booking_id
         t.integer :arrest_id
         t.integer :probation_id
         t.date :hold_date
         t.string :hold_time
         t.boolean :doc, :default => false
         t.boolean :doc_billable, :default => false
         t.date :cleared_date
         t.string :cleared_time
         t.integer :cleared_because_id
         t.integer :sentence_id
         t.integer :transfer_id
         t.integer :revocation_id
         t.string :explanation
         t.date :bill_from_date
         t.boolean :billed_retroactive, :default => false
         t.integer :hold_type_id
         t.string :hold_by
         t.string :hold_by_unit
         t.string :hold_by_badge
         t.integer :hold_by_id
         t.string :cleared_by
         t.string :cleared_by_unit
         t.string :cleared_by_badge
         t.integer :cleared_by_id
         t.date :billing_report_date
         t.string :agency
         t.integer :agency_id
         t.string :transfer_officer
         t.string :transfer_officer_unit
         t.string :transfer_officer_badge
         t.integer :transfer_officer_id
         t.decimal :fines, :precision => 12, :scale => 2, :default => 0
         t.integer :days_served, :default => 0
         t.integer :days_goodtime, :default => 0
         t.integer :days_total, :default => 0
         t.integer :days_time_served, :default => 0
         t.integer :hours_served, :default => 0
         t.integer :hours_goodtime, :default => 0
         t.datetime :datetime_counted
         t.text :remarks
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :holds, :booking_id
      add_index :holds, :arrest_id
      add_index :holds, :probation_id
      add_index :holds, :sentence_id
      add_index :holds, :transfer_id
      add_index :holds, :revocation_id
      add_index :holds, :cleared_because_id
      add_index :holds, :hold_type_id
      add_index :holds, :created_by
      add_index :holds, :updated_by
   
      create_table :hold_blocks, :force => true, :id => false do |t|
         t.integer :hold_id
         t.integer :blocker_id
      end
      add_index :hold_blocks, :hold_id
      add_index :hold_blocks, :blocker_id
   end

   def self.down
      drop_table :holds
      drop_table :hold_blocks
   end
end
