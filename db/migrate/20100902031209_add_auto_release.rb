# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddAutoRelease < ActiveRecord::Migration
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => 'AddAutoRelease::Option'
   end
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => 'AddAutoRelease::OptionType'
   end
  def self.up
     if ot = OptionType.find_by_name("Release Reason")
        if o = Option.find(:first, :conditions => {:option_type_id => ot.id, :long_name => 'Automatic Release'})
           o.update_attribute(:permanent, true)
           o.update_attribute(:disabled, true)
        else
           Option.create(:option_type_id => ot.id, :long_name => 'Automatic Release', :permanent => true, :disabled => true, :created_by => 1, :updated_by => 1)
        end
     end
  end

  def self.down
  end
end
