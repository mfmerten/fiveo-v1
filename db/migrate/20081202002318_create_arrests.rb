# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateArrests < ActiveRecord::Migration
   def self.up
      create_table :arrests, :force => true do |t|
         t.integer :person_id
         t.integer :offense_id
         t.string :case_no
         t.date :arrest_date
         t.string :arrest_time
         t.integer :arrest_type, :default => 0
         t.string :officer1
         t.string :officer1_unit
         t.string :officer1_badge
         t.integer :officer1_id
         t.string :officer2
         t.string :officer2_unit
         t.string :officer2_badge
         t.integer :officer2_id
         t.string :officer3
         t.string :officer3_unit
         t.string :officer3_badge
         t.integer :officer3_id
         t.string :agency
         t.integer :agency_id
         t.string :atn
         t.integer :ward
         t.integer :district
         t.boolean :victim_notify
         t.string :dwi_test_officer
         t.string :dwi_test_officer_unit
         t.string :dwi_test_officer_badge
         t.integer :dwi_test_officer_id
         t.string :dwi_test_results
         t.integer :hour_72_judge_id
         t.date :hour_72_date
         t.string :hour_72_time
         t.string :attorney
         t.string :condition_of_bond
         t.decimal :bond_amt, :precision => 12, :scale => 2, :default => 0
         t.boolean :bondable, :default => false
         t.text :remarks
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :arrests, :person_id
      add_index :arrests, :offense_id
      add_index :arrests, :case_no
      add_index :arrests, :hour_72_judge_id
      add_index :arrests, :created_by
      add_index :arrests, :updated_by
      
      create_table :district_warrants, :force => true, :id => false do |t|
         t.integer :arrest_id
         t.integer :warrant_id
      end
      add_index :district_warrants, :arrest_id
      add_index :district_warrants, :warrant_id
      
      create_table :city_warrants, :force => true, :id => false do |t|
         t.integer :arrest_id
         t.integer :warrant_id
      end
      add_index :city_warrants, :arrest_id
      add_index :city_warrants, :warrant_id
      
      create_table :other_warrants, :force => true, :id => false do |t|
         t.integer :arrest_id
         t.integer :warrant_id
      end
      add_index :other_warrants, :arrest_id
      add_index :other_warrants, :warrant_id
   end

   def self.down
      drop_table :arrests
      drop_table :district_warrants
      drop_table :city_warrants
      drop_table :other_warrants
   end
end
