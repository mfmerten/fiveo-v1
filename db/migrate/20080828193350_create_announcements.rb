# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateAnnouncements < ActiveRecord::Migration
   class Announcement < ActiveRecord::Base
   end

   def self.up
      create_table :announcements, :force => true do |t|
         t.date :start_date
         t.date :stop_date
         t.string :subject
         t.text :body
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :announcements, :start_date
      add_index :announcements, :stop_date
      add_index :announcements, :created_by
      add_index :announcements, :updated_by

      Announcement.create(
         :start_date => Date.today,
         :subject => "Welcome",
         :created_by => 1,
         :updated_by => 1,
         :body => "Welcome to the Home Page of the FiveO Law Enforcement web-based software application.\n\nYou can return to this page at any time by clicking on the _Home_ button in the navigation bar on the left."
      )

      Announcement.create(
         :start_date => Date.today,
         :subject => "Demo",
         :created_by => 1,
         :updated_by => 1,
         :body => "This is a demonstration installation of the FiveO Software. It is *not* configured to handle data securely! While you are on this site, *do not submit actual real personal information*. All data entered for this site should be imaginary (made-up)."
      )

      Announcement.create(
         :start_date => Date.today,
         :subject => "Important Security Information",
         :created_by => 1,
         :updated_by => 1,
         :body => "Please keep the following security issues in mind while you access this facility:\n\n* You will notice your name at the top of the menu bar on the left. Everything that is done on this system while your name is displayed will be attributed to you. Therefore, it is in your best interest that any time you leave your computer, you log out of the application by clicking the Logout button below your name.\n* You should never configure your web browser to remember your login and password, unless you are certain that you are the only one to have access to your computer. If your web browser logs you in to this application automatically, then anyone with access to your computer can do bad things that you will be blamed for.\n* You should never give your password to anyone. If a person needs access to this program, they should contact their supervisor to arrange for their own login account.\n* If you are denied access when trying to do something you think you should be able to do, contact your supervisor to have your permissions updated, or to find out why you are not permitted that function. You should also file a bug report because the software should never offer you the option to do something if you don't have adequate permissions."
      )
      
      Announcement.create(
         :start_date => Date.today,
         :subject => "FiveO Website",
         :created_by => 1,
         :updated_by => 1,
         :body => "If you have not already done so, you are encouraged to register an account on the \"Midlake Technical Services Website.\":http://midlaketech.com .  Fill in the registration form and submit it.  You will be notified by email when your registration has been approved.\n\nContact one of the FiveO developers if you wish access to the \"FiveO Developer Portal\":http://midlaketech.com/dev (accounts there are issued on request).  This will be necessary if you wish to access our issue tracker, project plans, developer discussions and the documentation Wiki."
      )
      
      Announcement.create(
         :start_date => Date.today,
         :subject => "Reporting Errors",
         :created_by => 1,
         :updated_by => 1,
         :body => "This software application is under heavy development.  There will be errors that occur from time to time. There will also be things you need to do that this software does not allow.  To report a problem or other program deficiency, or simply to ask a question, please use your in-house bug reporting tool (see Bug Reports in the menu bar on the left of your screen).\n\nOn the Bug Reports screen you will be able to view existing bug reports or create new ones.  Each bug report should pertain to only one problem, so be sure to create a separate report for each issue. Valid issues are:\n\n# *Bugs* - these are errors that occur. When reporting bugs, please try to include as much information as you can about what you were doing when the error occurred, and the complete text of any error messages you received.\n# *Functionality* - this is where you *need* to do something, but there doesn't seem to be a way to do it.  Try to describe the missing functionality in as much detail as you can.\n# *Wishlist* - these are things you think might be a good idea, but are not really required to perform your job. Again, please try to describe what you want using as much detail as possible.\n# *User Interface* - this is what you see on the screen and interact with using the keyboard and mouse. Screens that look weird or are confusing... controls that don't do what you expect them to do... colors that get on your nerves after a while... these are all user interface issues. When reporting these issues, it would help tremendously if you can describe what is bothering you *and* what you think would fix the problem.\n# *Questions* - bug reports are also appropriate if you just want to ask a question about the program.\n# *Comments* - for good or bad, we *want* to know what you think of the application overall, and in particular the specific parts of it you deal with on a regular basis."
      )
   end

   def self.down
      drop_table :announcements
   end
end
