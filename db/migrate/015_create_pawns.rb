# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreatePawns < ActiveRecord::Migration
   def self.up
      create_table :pawns, :force => true do |t|
         t.integer :pawn_co_id
         t.string :full_name
         t.string :address1
         t.string :address2
         t.string :oln
         t.integer :oln_state_id
         t.string :ssn
         t.string :ticket_no
         t.date :ticket_date
         t.integer :ticket_type_id
         t.string :case_no
         t.text :remarks
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :pawns, :pawn_co_id
      add_index :pawns, :full_name
      add_index :pawns, :oln_state_id
      add_index :pawns, :ticket_type_id
      add_index :pawns, :case_no
      add_index :pawns, :created_by
      add_index :pawns, :updated_by

      create_table :pawn_items, :force => true do |t|
         t.integer :pawn_id
         t.integer :pawn_type_id
         t.string :model_no
         t.string :serial_no
         t.string :description
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :pawn_items, :pawn_id
      add_index :pawn_items, :pawn_type_id
      add_index :pawn_items, :created_by
      add_index :pawn_items, :updated_by
   end

   def self.down
      drop_table :pawns
      drop_table :pawn_items
   end
end
