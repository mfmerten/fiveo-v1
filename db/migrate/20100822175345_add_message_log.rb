# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddMessageLog < ActiveRecord::Migration
   def self.up
      create_table :message_logs, :force => true do |t|
         t.boolean :receiver_deleted
         t.boolean :receiver_purged
         t.boolean :sender_deleted
         t.boolean :sender_purged
         t.datetime :read_at
         t.integer :receiver_id
         t.integer :sender_id
         t.string :subject
         t.text :body
         t.timestamps
      end
      add_index :message_logs, :receiver_id
      add_index :message_logs, :sender_id
      
      # set default message retention period (days)
      SiteConfig.message_retention = SiteConfigDefault.message_retention
      if conf = File.open("#{RAILS_ROOT}/public/system/site_configuration.yml","w")
         conf.puts(SiteConfig.marshal_dump.to_yaml)
         conf.close
      end
   end

   def self.down
      drop_table :message_logs
   end
end
