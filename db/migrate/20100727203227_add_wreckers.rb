# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddWreckers < ActiveRecord::Migration
   class Offense < ActiveRecord::Base
      has_many :wreckers, :class_name => 'AddWreckers::Wrecker', :dependent => :destroy
   end
   class Wrecker < ActiveRecord::Base
      belongs_to :offense, :class_name => 'AddWreckers::Offense'
   end
   def self.up
      create_table :wreckers, :force => true do |t|
         t.integer :offense_id
         t.string :name
         t.boolean :by_request, :default => false
         t.string :location
         t.string :vin
         t.string :tag
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :wreckers, :offense_id
      add_index :wreckers, :created_by
      add_index :wreckers, :updated_by
      
      Offense.find(:all).each do |o|
         if o.wrecker_used?
            Wrecker.create(:offense_id => o.id, :name => o.wrecker_used, :by_request => o.wrecker_by_request, :location => o.vehicle_location, :vin => o.vehicle_vin, :tag => o.vehicle_lic, :created_by => o.created_by, :updated_by => o.updated_by)
         elsif o.vehicle_vin? || o.vehicle_lic? || o.vehicle_location?
            Wrecker.create(:offense_id => o.id, :name => "Unspecified", :by_request => o.wrecker_by_request, :location => o.vehicle_location, :vin => o.vehicle_vin, :tag => o.vehicle_lic, :created_by => o.created_by, :updated_by => o.updated_by)
         end
      end
      
      remove_column :offenses, :wrecker_used
      remove_column :offenses, :wrecker_by_request
      remove_column :offenses, :vehicle_location
      remove_column :offenses, :vehicle_vin
      remove_column :offenses, :vehicle_lic
   end

   def self.down
      add_column :offenses, :wrecker_used, :string
      add_column :offenses, :wrecker_by_request, :boolean
      add_column :offenses, :vehicle_location, :string
      add_column :offenses, :vehicle_vin, :string
      add_column :offenses, :vehicle_lic, :string
      
      Wrecker.find(:all).each do |w|
         if o = Offense.find_by_id(w.id)
            unless o.wrecker_used?
               o.wrecker_used = w.name
               o.wrecker_by_request = w.by_request
               o.vehicle_location = w.location
               o.vehicle_vin = w.vin
               o.vehicle_lic = w.tag
            end
         end
      end

      drop_table :wreckers
   end
end
