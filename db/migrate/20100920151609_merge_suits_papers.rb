# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class MergeSuitsPapers < ActiveRecord::Migration
   def self.up
      # drop original tables
      drop_table :suits
      drop_table :papers
      drop_table :civil_payments
      drop_table :civil_invoices
      drop_table :civil_invoice_items
      drop_table :civil_customers
      
      # create new tables with indexes
      create_table "papers", :force => true do |t|
        t.string   "suit_no"
        t.string   "plaintiff"
        t.string   "defendant"
        t.integer  "paper_customer_id"
        t.string   "serve_to"
        t.string   "street"
        t.string   "city"
        t.integer  "state_id"
        t.string   "zip"
        t.string   "phone"
        t.integer  "paper_type_id"
        t.date     "received_date"
        t.date     "served_date"
        t.string   "served_time"
        t.date     "returned_date"
        t.integer  "service_type_id"
        t.integer  "noservice_type_id"
        t.string   "noservice_extra"
        t.string   "served_to"
        t.integer  "officer_id"
        t.string   "officer"
        t.string   "officer_badge"
        t.string   "officer_unit"
        t.text     "comments"
        t.boolean  "billable",                                        :default => true
        t.datetime "invoice_datetime"
        t.date     "paid_date"
        t.decimal  "invoice_total",    :precision => 12, :scale => 2, :default => 0
        t.decimal  "noservice_fee",    :precision => 12, :scale => 2, :default => 0
        t.decimal  "mileage_fee",      :precision => 12, :scale => 2, :default => 0
        t.decimal  "service_fee",      :precision => 12, :scale => 2, :default => 0
        t.string   "memo"
        t.text     "remarks"
        t.integer  "created_by",                                      :default => 1
        t.integer  "updated_by",                                      :default => 1
        t.datetime "created_at"
        t.datetime "updated_at"
        t.date     "court_date"
        t.decimal  "mileage",           :precision => 5, :scale => 1, :default => 0.0
        t.datetime "report_datetime"
      end
      
      add_index "papers", ["created_by"], :name => "index_papers_on_created_by"
      add_index "papers", ["paper_customer_id"], :name => "index_papers_on_paper_customer_id"
      add_index "papers", ["noservice_type_id"], :name => "index_papers_on_noservice_type_id"
      add_index "papers", ["paper_type_id"], :name => "index_papers_on_paper_type_id"
      add_index "papers", ["service_type_id"], :name => "index_papers_on_service_type_id"
      add_index "papers", ["state_id"], :name => "index_papers_on_state_id"
      add_index "papers", ["updated_by"], :name => "index_papers_on_updated_by"
      add_index "papers", ["officer_id"], :name => "index_papers_on_officer_id"
      
      create_table "paper_payments", :force => true do |t|
        t.integer  "paper_id"
        t.integer  "paper_customer_id"
        t.integer  "payment_type_id"
        t.date     "transaction_date"
        t.string   "officer"
        t.string   "officer_unit"
        t.string   "officer_badge"
        t.integer  "officer_id"
        t.string   "receipt_no"
        t.decimal  "payment",           :precision => 12, :scale => 2, :default => 0.0
        t.decimal  "charge",            :precision => 12, :scale => 2, :default => 0.0
        t.decimal  "balance",           :precision => 12, :scale => 2, :default => 0.0
        t.datetime "report_datetime"
        t.string   "memo"
        t.integer  "created_by",                                       :default => 1
        t.integer  "updated_by",                                       :default => 1
        t.datetime "created_at"
        t.datetime "updated_at"
      end

      add_index "paper_payments", ["paper_id"], :name => "index_paper_payments_on_paper_id"
      add_index "paper_payments", ["paper_customer_id"], :name => "index_paper_payments_on_paper_customer_id"
      add_index "paper_payments", ["created_by"], :name => "index_paper_payments_on_created_by"
      add_index "paper_payments", ["payment_type_id"], :name => "index_paper_payments_on_payment_type_id"
      add_index "paper_payments", ["updated_by"], :name => "index_paper_payments_on_updated_by"
      
      create_table "paper_customers", :force => true do |t|
        t.string   "name"
        t.string   "street"
        t.string   "street2"
        t.string   "city"
        t.integer  "state_id"
        t.string   "zip"
        t.string   "phone"
        t.string   "fax"
        t.string   "attention"
        t.text     "remarks"
        t.boolean  "disabled",                                   :default => false
        t.integer  "created_by",                                 :default => 1
        t.integer  "updated_by",                                 :default => 1
        t.datetime "created_at"
        t.datetime "updated_at"
        t.decimal  "balance_due", :precision => 12, :scale => 2, :default => 0.0
      end

      add_index "paper_customers", ["created_by"], :name => "index_paper_customers_on_created_by"
      add_index "paper_customers", ["name"], :name => "index_paper_customers_on_name"
      add_index "paper_customers", ["state_id"], :name => "index_paper_customers_on_state_id"
      add_index "paper_customers", ["updated_by"], :name => "index_paper_customers_on_updated_by"
   end

   def self.down
      # drop new tables
      drop_table :papers
      drop_table :paper_customers
      drop_table :paper_payments
      
      # re-create old tables
      create_table "suits", :force => true do |t|
        t.integer  "civil_customer_id"
        t.string   "suit_no"
        t.string   "plaintiff"
        t.string   "defendant"
        t.boolean  "billable",          :default => true
        t.text     "remarks"
        t.integer  "created_by",        :default => 1
        t.integer  "updated_by",        :default => 1
        t.datetime "created_at"
        t.datetime "updated_at"
      end

      add_index "suits", ["civil_customer_id"], :name => "index_suits_on_civil_customer_id"
      add_index "suits", ["created_by"], :name => "index_suits_on_created_by"
      add_index "suits", ["updated_by"], :name => "index_suits_on_updated_by"

      create_table "papers", :force => true do |t|
        t.string   "serve_to"
        t.string   "street"
        t.string   "city"
        t.integer  "state_id"
        t.string   "zip"
        t.string   "phone"
        t.integer  "paper_type_id"
        t.date     "received_date"
        t.date     "served_date"
        t.string   "served_time"
        t.date     "returned_date"
        t.integer  "service_type_id"
        t.integer  "noservice_type_id"
        t.string   "noservice_extra"
        t.string   "served_to"
        t.integer  "officer_id"
        t.string   "officer"
        t.string   "officer_badge"
        t.string   "officer_unit"
        t.text     "comments"
        t.boolean  "billable",                                        :default => true
        t.datetime "invoice_datetime"
        t.date     "paid_date"
        t.string   "memo"
        t.integer  "created_by",                                      :default => 1
        t.integer  "updated_by",                                      :default => 1
        t.datetime "created_at"
        t.datetime "updated_at"
        t.date     "court_date"
        t.decimal  "mileage",           :precision => 5, :scale => 1, :default => 0.0
        t.datetime "report_datetime"
        t.integer  "suit_id"
      end
      
      add_index "papers", ["created_by"], :name => "index_papers_on_created_by"
      add_index "papers", ["noservice_type_id"], :name => "index_papers_on_noservice_type_id"
      add_index "papers", ["paper_type_id"], :name => "index_papers_on_paper_type_id"
      add_index "papers", ["service_type_id"], :name => "index_papers_on_service_type_id"
      add_index "papers", ["state_id"], :name => "index_papers_on_state_id"
      add_index "papers", ["updated_by"], :name => "index_papers_on_updated_by"

      create_table "civil_payments", :force => true do |t|
        t.integer  "civil_customer_id"
        t.integer  "payment_type_id"
        t.date     "transaction_date"
        t.string   "officer"
        t.string   "officer_unit"
        t.string   "officer_badge"
        t.integer  "officer_id"
        t.string   "receipt_no"
        t.decimal  "payment",           :precision => 12, :scale => 2, :default => 0.0
        t.decimal  "charge",            :precision => 12, :scale => 2, :default => 0.0
        t.decimal  "balance",           :precision => 12, :scale => 2, :default => 0.0
        t.datetime "report_datetime"
        t.string   "memo"
        t.integer  "created_by",                                       :default => 1
        t.integer  "updated_by",                                       :default => 1
        t.datetime "created_at"
        t.datetime "updated_at"
        t.integer  "civil_invoice_id"
      end

      add_index "civil_payments", ["civil_customer_id"], :name => "index_civil_payments_on_civil_customer_id"
      add_index "civil_payments", ["created_by"], :name => "index_civil_payments_on_created_by"
      add_index "civil_payments", ["payment_type_id"], :name => "index_civil_payments_on_payment_type_id"
      add_index "civil_payments", ["updated_by"], :name => "index_civil_payments_on_updated_by"

      create_table "civil_invoices", :force => true do |t|
        t.datetime "invoice_datetime"
        t.integer  "civil_customer_id"
        t.date     "paid_date"
        t.decimal  "invoice_total",     :precision => 12, :scale => 2, :default => 0.0
        t.datetime "report_datetime"
        t.integer  "created_by",                                       :default => 1
        t.integer  "updated_by",                                       :default => 1
        t.datetime "created_at"
        t.datetime "updated_at"
        t.decimal  "noservice_fee",     :precision => 12, :scale => 2, :default => 0.0
        t.decimal  "mileage_fee",       :precision => 12, :scale => 2, :default => 0.0
        t.decimal  "balance_due",       :precision => 12, :scale => 2, :default => 0.0
      end

      add_index "civil_invoices", ["civil_customer_id"], :name => "index_civil_invoices_on_civil_customer_id"
      add_index "civil_invoices", ["created_by"], :name => "index_civil_invoices_on_created_by"
      add_index "civil_invoices", ["invoice_datetime"], :name => "index_civil_invoices_on_invoice_datetime"
      add_index "civil_invoices", ["updated_by"], :name => "index_civil_invoices_on_updated_by"

      create_table "civil_invoice_items", :id => false, :force => true do |t|
        t.integer "civil_invoice_id"
        t.integer "paper_id"
      end

      add_index "civil_invoice_items", ["civil_invoice_id"], :name => "index_civil_invoice_items_on_civil_invoice_id"
      add_index "civil_invoice_items", ["paper_id"], :name => "index_civil_invoice_items_on_paper_id"

      create_table "civil_customers", :force => true do |t|
        t.string   "name"
        t.string   "street"
        t.string   "city"
        t.integer  "state_id"
        t.string   "zip"
        t.string   "phone"
        t.string   "attention"
        t.text     "remarks"
        t.boolean  "disabled",                                   :default => false
        t.integer  "created_by",                                 :default => 1
        t.integer  "updated_by",                                 :default => 1
        t.datetime "created_at"
        t.datetime "updated_at"
        t.decimal  "balance_due", :precision => 12, :scale => 2, :default => 0.0
      end

      add_index "civil_customers", ["created_by"], :name => "index_civil_customers_on_created_by"
      add_index "civil_customers", ["name"], :name => "index_civil_customers_on_name"
      add_index "civil_customers", ["state_id"], :name => "index_civil_customers_on_state_id"
      add_index "civil_customers", ["updated_by"], :name => "index_civil_customers_on_updated_by"
   end
end
