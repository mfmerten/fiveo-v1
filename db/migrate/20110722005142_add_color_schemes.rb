class AddColorSchemes < ActiveRecord::Migration
   class Colorscheme < ActiveRecord::Base
   end
   
  def self.up
     Colorscheme.create(
        :name => "Green/Gray", :description => "Green/Gray Colorscheme",
        :color_dark => "#005500",
        :color_light => "#aed9ae",
        :color_alt_dark => "#555555",
        :color_alt_light => "#cccccc",
        :color_white => "#FFFFFF",
        :color_black => "#000000",
        :color_gray_dark => "#333333",
        :color_gray_medium => "#777777",
        :color_gray_light => "#CCCCCC",
        :color_notice => "#00AA00",
        :color_notice_bg => "#EEFFEE",
        :color_error => "#AA0000",
        :color_error_bg => "#FFEEEE",
        :color_message => "#0000AA",
        :color_message_bg => "#EEEEFF"
     )
     Colorscheme.create(
        :name => "Gray/Green", :description => "Gray/Green Colorscheme",
        :color_dark => "#555555",
        :color_light => "#cccccc",
        :color_alt_dark => "#005500",
        :color_alt_light => "#aed9ae",
        :color_white => "#FFFFFF",
        :color_black => "#000000",
        :color_gray_dark => "#333333",
        :color_gray_medium => "#777777",
        :color_gray_light => "#CCCCCC",
        :color_notice => "#00AA00",
        :color_notice_bg => "#EEFFEE",
        :color_error => "#AA0000",
        :color_error_bg => "#FFEEEE",
        :color_message => "#0000AA",
        :color_message_bg => "#EEEEFF"
     )
     Colorscheme.create(
        :name => "Yellow/Gray", :description => "Yellow/Gray Colorscheme",
        :color_dark => "#555500",
        :color_light => "#ffffd5",
        :color_alt_dark => "#555555",
        :color_alt_light => "#cccccc",
        :color_white => "#FFFFFF",
        :color_black => "#000000",
        :color_gray_dark => "#333333",
        :color_gray_medium => "#777777",
        :color_gray_light => "#CCCCCC",
        :color_notice => "#00AA00",
        :color_notice_bg => "#EEFFEE",
        :color_error => "#AA0000",
        :color_error_bg => "#FFEEEE",
        :color_message => "#0000AA",
        :color_message_bg => "#EEEEFF"
     )
     Colorscheme.create(
        :name => "Default-alt", :description => "Alternate Default Colorscheme (Brown/Blue)",
        :color_dark => "#886655",
        :color_light => "#F0ECDB",
        :color_alt_dark => "#3665A3",
        :color_alt_light => "#D4E3F7",
        :color_white => "#FFFFFF",
        :color_black => "#000000",
        :color_gray_dark => "#333333",
        :color_gray_medium => "#777777",
        :color_gray_light => "#CCCCCC",
        :color_notice => "#00AA00",
        :color_notice_bg => "#EEFFEE",
        :color_error => "#AA0000",
        :color_error_bg => "#FFEEEE",
        :color_message => "#0000AA",
        :color_message_bg => "#EEEEFF"
     )
     Colorscheme.create(
        :name => "Blue/Gray", :description => "Blue/Gray Colorscheme",
        :color_dark => "#3665A3",
        :color_light => "#D4E3F7",
        :color_alt_dark => "#555555",
        :color_alt_light => "#cccccc",
        :color_white => "#FFFFFF",
        :color_black => "#000000",
        :color_gray_dark => "#333333",
        :color_gray_medium => "#777777",
        :color_gray_light => "#CCCCCC",
        :color_notice => "#00AA00",
        :color_notice_bg => "#EEFFEE",
        :color_error => "#AA0000",
        :color_error_bg => "#FFEEEE",
        :color_message => "#0000AA",
        :color_message_bg => "#EEEEFF"
     )
     Colorscheme.create(
        :name => "Green/Brown", :description => "Green/Brown Colorscheme",
        :color_dark => "#005500",
        :color_light => "#aed9ae",
        :color_alt_dark => "#886655",
        :color_alt_light => "#F0ECDB",
        :color_white => "#FFFFFF",
        :color_black => "#000000",
        :color_gray_dark => "#333333",
        :color_gray_medium => "#777777",
        :color_gray_light => "#CCCCCC",
        :color_notice => "#00AA00",
        :color_notice_bg => "#EEFFEE",
        :color_error => "#AA0000",
        :color_error_bg => "#FFEEEE",
        :color_message => "#0000AA",
        :color_message_bg => "#EEEEFF"
     )
  end

  def self.down
     ["Green/Gray","Gray/Green","Yellow/Gray","Default-alt","Blue/Gray","Green/Brown"].each do |c|
        if cscheme = Colorscheme.find_by_name(c)
           cscheme.destroy
        end
     end
  end
end
