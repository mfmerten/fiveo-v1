# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class SplitMiddleNames < ActiveRecord::Migration
   class Warrant < ActiveRecord::Base
   end
   
   class Forbid < ActiveRecord::Base
   end
   
   def self.up
      add_column :warrants, :middle1, :string
      add_column :warrants, :middle2, :string
      add_column :warrants, :middle3, :string
      add_column :forbids, :forbid_middle1, :string
      add_column :forbids, :forbid_middle2, :string
      add_column :forbids, :forbid_middle3, :string
      
      Warrant.find(:all).each do |w|
         if w.middlename?
            parts = w.middlename.split
            w.update_attribute(:middle1, parts[0])
            w.update_attribute(:middle2, parts[1])
            w.update_attribute(:middle3, parts[2])
         end
      end
      Forbid.find(:all).each do |f|
         if f.forbid_middle?
            parts = f.forbid_middle.split
            f.update_attribute(:forbid_middle1, parts[0])
            f.update_attribute(:forbid_middle2, parts[1])
            f.update_attribute(:forbid_middle3, parts[2])
         end
      end
      
      remove_column :warrants, :middlename
      remove_column :forbids, :forbid_middle
   end

   def self.down
      add_column :warrants, :middlename, :string
      add_column :forbids, :forbid_middle, :string
      
      Warrant.find(:all).each do |w|
         if w.middle1?
            w.update_attribute(:middlename, "#{w.middle1} #{w.middle2} #{w.middle3}".strip)
         end
      end
      Forbid.find(:all).each do |f|
         if f.forbid_middle1?
            f.update_attribute(:forbid_middle, "#{f.forbid_middle1} #{f.forbid_middle2} #{f.forbid_middle3}".strip)
         end
      end
      
      remove_column :warrants, :middle1
      remove_column :warrants, :middle2
      remove_column :warrants, :middle3
      remove_column :forbids, :middle1
      remove_column :forbids, :middle2
      remove_column :forbids, :middle3
   end
end
