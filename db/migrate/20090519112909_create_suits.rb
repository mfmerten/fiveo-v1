# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateSuits < ActiveRecord::Migration
   def self.up
      create_table :suits, :force => true do |t|
         t.integer :civil_customer_id
         t.integer :suit_type_id
         t.string :suit_no
         t.string :plaintiff
         t.string :defendant
         t.boolean :billable, :default => true
         t.text :remarks
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :suits, :civil_customer_id
      add_index :suits, :suit_type_id
      add_index :suits, :created_by
      add_index :suits, :updated_by

      create_table :papers, :force => true do |t|
         t.integer :suit_id
         t.string :serve_to
         t.string :street
         t.string :city
         t.integer :state_id
         t.string :zip
         t.string :phone
         t.integer :paper_type_id
         t.date :court_date
         t.date :received_date
         t.date :served_date
         t.string :served_time
         t.date :returned_date
         t.integer :service_type_id
         t.integer :noservice_type_id
         t.string :noservice_extra
         t.string :served_to
         t.decimal :mileage, :precision => 5, :scale => 1, :default => 0
         t.integer :officer_id
         t.string :officer
         t.string :officer_badge
         t.string :officer_unit
         t.text :comments
         t.boolean :billable, :default => true
         t.datetime :invoice_datetime
         t.datetime :report_datetime
         t.date :paid_date
         t.string :memo
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :papers, :suit_id
      add_index :papers, :state_id
      add_index :papers, :paper_type_id
      add_index :papers, :service_type_id
      add_index :papers, :noservice_type_id
      add_index :papers, :created_by
      add_index :papers, :updated_by

      create_table :civil_invoices, :force => true do |t|
         t.datetime :invoice_datetime
         t.integer :civil_customer_id
         t.date :paid_date
         t.decimal :invoice_total, :precision => 12, :scale => 2, :default => 0
         t.integer :payment_method_id
         t.string :memo
         t.decimal :noservice_fee, :precision => 12, :scale => 2, :default => 0
         t.decimal :mileage_fee, :precision => 12, :scale => 2, :default => 0
         t.datetime :report_datetime
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :civil_invoices, :civil_customer_id
      add_index :civil_invoices, :payment_method_id
      add_index :civil_invoices, :created_by
      add_index :civil_invoices, :updated_by

      create_table :civil_customers, :force => true do |t|
         t.string :name
         t.string :street
         t.string :city
         t.integer :state_id
         t.string :zip
         t.string :phone
         t.string :attention
         t.text :remarks
         t.boolean :disabled, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :civil_customers, :state_id
      add_index :civil_customers, :created_by
      add_index :civil_customers, :updated_by

      create_table :civil_invoice_items, :force => true, :id => false do |t|
         t.integer :civil_invoice_id
         t.integer :paper_id
      end
      add_index :civil_invoice_items, :civil_invoice_id
      add_index :civil_invoice_items, :paper_id
   end

   def self.down
      drop_table :suits
      drop_table :papers
      drop_table :civil_invoices
      drop_table :civil_customers
      drop_table :civil_invoice_items
   end
end
