# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class RemovePendingJoins < ActiveRecord::Migration
  def self.up
     drop_table :people_sentences
     drop_table :people_revocations
     drop_table :people_docs
  end

  def self.down
     create_table :people_sentences, :force => true, :id => false do |t|
        t.integer :person_id
        t.integer :sentence_id
     end
     add_index :people_sentences, :person_id
     add_index :people_sentences, :sentence_id
     
     create_table :people_revocations, :force => true, :id => false do |t|
        t.integer :person_id
        t.integer :revocation_id
     end
     add_index :people_revocations, :person_id
     add_index :people_revocations, :revocation_id
     
     create_table :people_docs, :force => true, :id => false do |t|
        t.integer :person_id
        t.integer :doc_id
     end
     add_index :people_docs, :person_id
     add_index :people_docs, :doc_id
  end
end
