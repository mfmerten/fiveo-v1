# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class RemoveStateFromDoc < ActiveRecord::Migration
   def self.up
      remove_column :docs, :processed
      remove_column :docs, :release_date
      remove_column :docs, :release_time
      remove_column :docs, :release_by_transfer
      remove_column :docs, :to_agency
      rename_column :docs, :process_status, :problems
   end

   def self.down
      add_column :docs, :processed, :boolean
      add_column :docs, :release_date, :date
      add_column :docs, :release_time, :string
      add_column :docs, :release_by_transfer, :boolean
      add_column :docs, :to_agency, :string
      rename_column :docs, :problems, :process_status
   end
end
