# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateContacts < ActiveRecord::Migration
   def self.up
      create_table :contacts, :force => true do |t|
         t.string :first
         t.string :last
         t.string :middle
         t.string :suffix
         t.string :title
         t.string :unit
         t.string :agency
         t.string :street
         t.string :street2
         t.string :city
         t.integer :state_id
         t.string :zip
         t.string :pri_email
         t.string :alt_email
         t.string :web_site
         t.string :notes
         t.boolean :detective, :default => false
         t.boolean :officer, :default => false
         t.string :badge_no
         t.boolean :physician, :default => false
         t.boolean :pharmacy, :default => false
         t.boolean :active, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :contacts, :first
      add_index :contacts, :middle
      add_index :contacts, :last
      add_index :contacts, :suffix
      add_index :contacts, :agency
      add_index :contacts, :state_id
      add_index :contacts, :created_by
      add_index :contacts, :updated_by

      create_table :phones, :force => true do |t|
         t.integer :contact_id
         t.integer :label_id
         t.string :value
         t.string :note
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :phones, :contact_id
      add_index :phones, :label_id
      add_index :phones, :created_by
      add_index :phones, :updated_by
   end

   def self.down
      drop_table :contacts
      drop_table :phones
   end
end
