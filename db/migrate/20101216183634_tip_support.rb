# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class TipSupport < ActiveRecord::Migration
  def self.up
     add_column :users, :show_tips, :boolean, :default => true
     add_column :colorschemes, :color_tip_bg, :string, :default => "#FFFFBB"
     add_column :colorschemes, :color_tip_border, :string, :default => "#886655"
     add_column :colorschemes, :color_tip, :string, :default => "#000000"
     change_column_default :colorschemes, :color_notice, "#00AA00"
     change_column_default :colorschemes, :color_message, "#0000AA"
     change_column_default :colorschemes, :color_gray_light, "#CCCCCC"
     change_column_default :colorschemes, :color_gray_dark, "#333333"
     change_column_default :colorschemes, :color_error_bg, "#FFEEEE"
     change_column_default :colorschemes, :color_message_bg, "#EEEEFF"
     change_column_default :colorschemes, :color_error, "#AA0000"
     change_column_default :colorschemes, :color_white, "#FFFFFF"
     change_column_default :colorschemes, :color_black, "#000000"
     change_column_default :colorschemes, :color_alt_light, "#F0ECDB"
     change_column_default :colorschemes, :color_notice_bg, "#EEFFEE"
     change_column_default :colorschemes, :color_gray_bg, "#efefef"
     change_column_default :colorschemes, :color_dark, "#3665A3"
     change_column_default :colorschemes, :color_light, "#D4E3F7"
     change_column_default :colorschemes, :color_alt_dark, "#886655"
     change_column_default :colorschemes, :color_gray_medium, "#777777"
  end

  def self.down
     remove_column :users, :show_tips
     remove_column :colorschemes, :color_tip_bg
     remove_column :colorschemes, :color_tip_border
     remove_column :colorschemes, :color_tip
  end
end
