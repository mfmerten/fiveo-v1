# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateForbids < ActiveRecord::Migration
   def self.up
      create_table :forbids, :force => true do |t|
         t.integer :call_id
         t.string :case_no
         t.string :complainant
         t.string :comp_street
         t.string :comp_city_state_zip
         t.string :comp_phone
         t.integer :forbidden_id
         t.string :forbid_last
         t.string :forbid_first
         t.string :forbid_middle
         t.string :forbid_suffix
         t.string :forbid_street
         t.string :forbid_city_state_zip
         t.string :forbid_phone
         t.date :request_date
         t.string :receiving_officer
         t.string :receiving_officer_unit
         t.string :receiving_officer_badge
         t.integer :receiving_officer_id
         t.date :signed_date
         t.string :serving_officer
         t.string :serving_officer_unit
         t.string :serving_officer_badge
         t.integer :serving_officer_id
         t.date :recall_date
         t.text :details
         t.text :remarks
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :forbids, :call_id
      add_index :forbids, :forbidden_id
      add_index :forbids, :forbid_last
      add_index :forbids, :forbid_first
      add_index :forbids, :forbid_middle
      add_index :forbids, :created_by
      add_index :forbids, :updated_by
   
      create_table :forbid_exemptions, :force => true, :id => false do |t|
         t.integer :forbid_id
         t.integer :person_id
      end
      add_index :forbid_exemptions, :forbid_id
      add_index :forbid_exemptions, :person_id
      
      create_table :forbids_offenses, :force => true, :id => false do |t|
         t.integer :forbid_id
         t.integer :offense_id
      end
      add_index :forbids_offenses, :forbid_id
      add_index :forbids_offenses, :offense_id
   end

   def self.down
      drop_table :forbids
      drop_table :forbid_exemptions
      drop_table :forbids_offenses
   end
end
