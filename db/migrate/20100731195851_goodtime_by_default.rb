# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class GoodtimeByDefault < ActiveRecord::Migration
  class Booking < ActiveRecord::Base
  end
  
  def self.up
   change_column_default :bookings, :good_time, true
   
   Booking.find(:all, :conditions => 'release_date IS NULL').each do |b|
      b.update_attribute(:good_time, true)
   end
  end

  def self.down
     change_column_default :bookings, :good_time, false
  end
end
