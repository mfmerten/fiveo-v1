# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddThreeJails < ActiveRecord::Migration
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => 'AddThreeJails::Option', :dependent => :destroy
   end
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => 'AddThreeJails::OptionType'
   end
   
   def self.up
      # update site config file
      conf = SiteConfiguration.new or raise ActiveRecord::ActiveRecordError.new "Could not get new SiteConfiguration object."
      conf.load_site_file or raise ActiveRecord::ActiveRecordError.new "Could not load site configuration file."
      conf.write_site_file or raise ActiveRecord::ActiveRecordError.new "Could not write site configuration file."

      # jail 1
      create_table :jail1s, :force => true do |t|
        t.integer  :person_id
        t.decimal  :cash_at_booking, :precision => 12, :scale => 2, :default => 0.0
        t.string   :cash_receipt
        t.string   :booking_officer
        t.string   :booking_officer_unit
        t.date     :booking_date
        t.string   :booking_time
        t.integer  :cell_id
        t.text     :remarks
        t.string   :phone_called
        t.boolean  :intoxicated
        t.date     :sched_release_date
        t.date     :release_date
        t.string   :release_time
        t.boolean  :afis
        t.string   :release_officer
        t.string   :release_officer_unit
        t.string   :fingerprint_class
        t.integer  :created_by
        t.integer  :updated_by
        t.datetime :created_at
        t.datetime :updated_at
        t.integer  :booking_officer_id
        t.string   :booking_officer_badge
        t.integer  :release_officer_id
        t.string   :release_officer_badge
        t.boolean  :good_time, :default => true
        t.boolean  :disable_timers, :default => false
        t.string   :dna_profile
        t.string   :attorney
        t.boolean  :legacy, :default => false
        t.integer  :released_because_id
      end

      add_index :jail1s, :booking_date
      add_index :jail1s, :cell_id
      add_index :jail1s, :created_by
      add_index :jail1s, :fingerprint_class
      add_index :jail1s, :person_id
      add_index :jail1s, :released_because_id
      add_index :jail1s, :updated_by

      # jail 2
      create_table :jail2s, :force => true do |t|
        t.integer  :person_id
        t.decimal  :cash_at_booking, :precision => 12, :scale => 2, :default => 0.0
        t.string   :cash_receipt
        t.string   :booking_officer
        t.string   :booking_officer_unit
        t.date     :booking_date
        t.string   :booking_time
        t.integer  :cell_id
        t.text     :remarks
        t.string   :phone_called
        t.boolean  :intoxicated
        t.date     :sched_release_date
        t.date     :release_date
        t.string   :release_time
        t.boolean  :afis
        t.string   :release_officer
        t.string   :release_officer_unit
        t.string   :fingerprint_class
        t.integer  :created_by
        t.integer  :updated_by
        t.datetime :created_at
        t.datetime :updated_at
        t.integer  :booking_officer_id
        t.string   :booking_officer_badge
        t.integer  :release_officer_id
        t.string   :release_officer_badge
        t.boolean  :good_time, :default => true
        t.boolean  :disable_timers, :default => false
        t.string   :dna_profile
        t.string   :attorney
        t.boolean  :legacy, :default => false
        t.integer  :released_because_id
      end

      add_index :jail2s, :booking_date
      add_index :jail2s, :cell_id
      add_index :jail2s, :created_by
      add_index :jail2s, :fingerprint_class
      add_index :jail2s, :person_id
      add_index :jail2s, :released_because_id
      add_index :jail2s, :updated_by

      # jail 3
      create_table :jail3s, :force => true do |t|
        t.integer  :person_id
        t.decimal  :cash_at_booking, :precision => 12, :scale => 2, :default => 0.0
        t.string   :cash_receipt
        t.string   :booking_officer
        t.string   :booking_officer_unit
        t.date     :booking_date
        t.string   :booking_time
        t.integer  :cell_id
        t.text     :remarks
        t.string   :phone_called
        t.boolean  :intoxicated
        t.date     :sched_release_date
        t.date     :release_date
        t.string   :release_time
        t.boolean  :afis
        t.string   :release_officer
        t.string   :release_officer_unit
        t.string   :fingerprint_class
        t.integer  :created_by
        t.integer  :updated_by
        t.datetime :created_at
        t.datetime :updated_at
        t.integer  :booking_officer_id
        t.string   :booking_officer_badge
        t.integer  :release_officer_id
        t.string   :release_officer_badge
        t.boolean  :good_time, :default => true
        t.boolean  :disable_timers, :default => false
        t.string   :dna_profile
        t.string   :attorney
        t.boolean  :legacy, :default => false
        t.integer  :released_because_id
      end

      add_index :jail3s, :booking_date
      add_index :jail3s, :cell_id
      add_index :jail3s, :created_by
      add_index :jail3s, :fingerprint_class
      add_index :jail3s, :person_id
      add_index :jail3s, :released_because_id
      add_index :jail3s, :updated_by
      
      # options
      # create cell options for each jail and add seed data
      unless ot = OptionType.find_by_name("Jail1 Cell")
         ot = OptionType.create(:name => "Jail1 Cell")
      end
      ot.options.create(:long_name => 'Holding', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'A Block', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'B Block', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'C Block', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'D Block', :created_by => 1, :updated_by => 1)
      unless ot = OptionType.find_by_name("Jail2 Cell")
         ot = OptionType.create(:name => "Jail2 Cell")
      end
      ot.options.create(:long_name => 'Holding', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'A Block', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'B Block', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'C Block', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'D Block', :created_by => 1, :updated_by => 1)
      unless ot = OptionType.find_by_name("Jail3 Cell")
         ot = OptionType.create(:name => "Jail3 Cell")
      end
      ot.options.create(:long_name => 'Holding', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'A Block', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'B Block', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'C Block', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'D Block', :created_by => 1, :updated_by => 1)
      
   end

   def self.down
      drop_table :jail1s
      drop_table :jail2s
      drop_table :jail3s
      if ot = OptionType.find_by_name("Jail1 Cell")
         ot.destroy
      end
      if ot = OptionType.find_by_name("Jail2 Cell")
         ot.destroy
      end
      if ot = OptionType.find_by_name("Jail3 Cell")
         ot.destroy
      end
      say "Site configuration files not rolled back!  May need to do that manually."
   end
end
