# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateCalls < ActiveRecord::Migration
   def self.up
      create_table :calls, :Force => true do |t|
         t.string :case_no
         t.date :call_date
         t.string :call_time
         t.integer :signal_code_id
         t.integer :received_via_id
         t.string :received_by
         t.string :received_by_unit
         t.string :received_by_badge
         t.integer :received_by_id
         t.text :details
         t.date :dispatch_date
         t.string :dispatch_time
         t.date :arrival_date
         t.string :arrival_time
         t.date :concluded_date
         t.string :concluded_time
         t.integer :disposition_id
         t.string :dispatcher
         t.string :dispatcher_unit
         t.string :dispatcher_badge
         t.integer :dispatcher_id
         t.string :detective
         t.integer :detective_id
         t.string :detective_unit
         t.string :detective_badge
         t.integer :ward
         t.integer :district
         t.text :remarks
         t.datetime :reported_datetime
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :calls, :case_no
      add_index :calls, :signal_code_id
      add_index :calls, :received_via_id
      add_index :calls, :disposition_id
      add_index :calls, :created_by
      add_index :calls, :updated_by
      add_index :calls, :reported_datetime
      
      create_table :call_units, :force => true do |t|
         t.integer :call_id
         t.string :officer
         t.string :officer_unit
         t.string :officer_badge
         t.integer :officer_id
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :call_units, :call_id
      add_index :call_units, :created_by
      add_index :call_units, :updated_by
      
      create_table :call_subjects, :force => true do |t|
         t.integer :call_id
         t.integer :subject_type_id
         t.string :name
         t.string :address1
         t.string :address2
         t.string :home_phone
         t.string :cell_phone
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :call_subjects, :call_id
      add_index :call_subjects, :subject_type_id
      add_index :call_subjects, :created_by
      add_index :call_subjects, :updated_by
      
      create_table :cases, :force => true do |t|
         t.string :assigned
         t.timestamps
      end
   end

   def self.down
      drop_table :calls
      drop_table :call_units
      drop_table :call_subjects
      drop_table :cases
   end
end