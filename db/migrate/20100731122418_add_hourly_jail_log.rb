# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddHourlyJailLog < ActiveRecord::Migration
   class Booking < ActiveRecord::Base
      has_many :booking_logs, :class_name => "AddHourlyJailLog::BookingLog", :dependent => :destroy
   end
   class BookingLog < ActiveRecord::Base
      belongs_to :booking, :class_name => "AddHourlyJailLog::Booking"
   end
  def self.up
     create_table :booking_logs, :force => true do |t|
        t.integer :booking_id
        t.datetime :start_datetime
        t.datetime :stop_datetime
        t.string :start_officer
        t.string :start_officer_badge
        t.string :start_officer_unit
        t.string :stop_officer
        t.string :stop_officer_badge
        t.string :stop_officer_unit
        t.timestamps
     end
     add_index :booking_logs, :booking_id
     
     Booking.find(:all).each do |b|
        begin
           start = Time.parse("#{b.booking_date} #{b.booking_time} #{Time.now.zone}")
        rescue
           start = nil
        end
        unless start.nil?
           l = b.booking_logs.create(:start_datetime => start, :start_officer => b.booking_officer, :start_officer_badge => b.booking_officer_badge, :start_officer_unit => b.booking_officer_unit)
           if b.disable_timers?
              l.update_attributes(:stop_datetime => start, :stop_officer => b.booking_officer, :stop_officer_badge => b.booking_officer_badge, :stop_officer_unit => b.booking_officer_unit)
              l.save
           elsif b.release_date?
              begin
                 stop = Time.parse("#{b.release_date} #{b.release_time} #{Time.now.zone}")
              rescue
                 stop = nil
              end
              unless stop.nil?
                 l.update_attributes(:stop_datetime => stop, :stop_officer => b.release_officer, :stop_officer_badge => b.release_officer_badge, :stop_officer_unit => b.release_officer_unit)
                 l.save
              end
           end
        end
     end
  end

  def self.down
     drop_table :booking_logs
  end
end
