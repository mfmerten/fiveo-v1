# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateOptionTables < ActiveRecord::Migration
   def self.up
      create_table :option_types, :force => true do |t|
         t.string :name
         t.timestamps
      end
      add_index :option_types, :name

      create_table :options, :force => true do |t|
         t.integer :option_type_id
         t.string :long_name
         t.string :short_name
         t.boolean :disabled, :default => false
         t.boolean :permanent, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :options, :option_type_id
      add_index :options, :long_name
      add_index :options, :created_by
      add_index :options, :updated_by
   end

   def self.down
      drop_table :option_types
      drop_table :options
   end
end
