# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class LoadDefaultOptions < ActiveRecord::Migration
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => 'LoadDefaultOptions::OptionType', :dependent => :destroy
   end
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => 'LoadDefaultOptions::Option'
   end

   def self.up
      # Absent Type
      ot = OptionType.create(:name => "Absent Type")
      ot.options.build(:long_name => 'Hill Crew')
      ot.options.build(:long_name => 'Waste Crew')
      ot.options.build(:long_name => 'Lawn Care Crew')
      ot.options.build(:long_name => 'Police Jury I Crew')
      ot.options.build(:long_name => 'Police Jury II Crew')
      ot.options.build(:long_name => 'SRA Crew')
      ot.options.build(:long_name => 'Misc. Work Crew')
      ot.options.build(:long_name => 'Court')
      ot.options.build(:long_name => 'Medical')
      ot.options.build(:long_name => 'Dental')
      ot.options.build(:long_name => 'Mental')
      ot.options.build(:long_name => 'Substance Abuse')
      ot.options.build(:long_name => 'OCS')
      ot.options.build(:long_name => 'Funeral')
      ot.options.build(:long_name => 'Work Release')
      ot.options.each{|o| o.save}

      # Anatomy
      @option_type = OptionType.create :name => 'Anatomy'
      Option.create :long_name => "Abdomen", :option_type_id => @option_type.id
      Option.create :long_name => "Back", :option_type_id => @option_type.id
      Option.create :long_name => "Chest", :option_type_id => @option_type.id
      Option.create :long_name => "Left Arm", :option_type_id => @option_type.id
      Option.create :long_name => "Left Foot", :option_type_id => @option_type.id
      Option.create :long_name => "Left Hand", :option_type_id => @option_type.id
      Option.create :long_name => "Left Leg", :option_type_id => @option_type.id
      Option.create :long_name => "Neck", :option_type_id => @option_type.id
      Option.create :long_name => "Right Arm", :option_type_id => @option_type.id
      Option.create :long_name => "Right Foot", :option_type_id => @option_type.id
      Option.create :long_name => "Right Hand", :option_type_id => @option_type.id
      Option.create :long_name => "Right Leg", :option_type_id => @option_type.id
      Option.create :long_name => "Buttock", :option_type_id => @option_type.id
      Option.create :long_name => "Face", :option_type_id => @option_type.id

      # Associations
      @option_type = OptionType.create :name => 'Associate Type'
      Option.create :long_name => "Associate", :option_type_id => @option_type.id
      Option.create :long_name => "Aunt", :option_type_id => @option_type.id
      Option.create :long_name => "Boyfriend", :option_type_id => @option_type.id
      Option.create :long_name => "Brother", :option_type_id => @option_type.id
      Option.create :long_name => "Brother-in-Law", :option_type_id => @option_type.id
      Option.create :long_name => "Cousin", :option_type_id => @option_type.id
      Option.create :long_name => "Daughter", :option_type_id => @option_type.id
      Option.create :long_name => "Daughter-in-Law", :option_type_id => @option_type.id
      Option.create :long_name => "Employee", :option_type_id => @option_type.id
      Option.create :long_name => "Employer", :option_type_id => @option_type.id
      Option.create :long_name => "Ex-Husband", :option_type_id => @option_type.id
      Option.create :long_name => "Ex-Wife", :option_type_id => @option_type.id
      Option.create :long_name => "Father", :option_type_id => @option_type.id
      Option.create :long_name => "Father-in-Law", :option_type_id => @option_type.id
      Option.create :long_name => "Friend", :option_type_id => @option_type.id
      Option.create :long_name => "Girlfriend", :option_type_id => @option_type.id
      Option.create :long_name => "Grandchild", :option_type_id => @option_type.id
      Option.create :long_name => "Grandfather", :option_type_id => @option_type.id
      Option.create :long_name => "Grandmother", :option_type_id => @option_type.id
      Option.create :long_name => "Husband", :option_type_id => @option_type.id
      Option.create :long_name => "Mother", :option_type_id => @option_type.id
      Option.create :long_name => "Mother-in-Law", :option_type_id => @option_type.id
      Option.create :long_name => "Nephew", :option_type_id => @option_type.id
      Option.create :long_name => "Niece", :option_type_id => @option_type.id
      Option.create :long_name => "Roommate", :option_type_id => @option_type.id
      Option.create :long_name => "Sister", :option_type_id => @option_type.id
      Option.create :long_name => "Sister-in-Law", :option_type_id => @option_type.id
      Option.create :long_name => "Step-Brother", :option_type_id => @option_type.id
      Option.create :long_name => "Step-Daughter", :option_type_id => @option_type.id
      Option.create :long_name => "Step-Father", :option_type_id => @option_type.id
      Option.create :long_name => "Step-Mother", :option_type_id => @option_type.id
      Option.create :long_name => "Step-Sister", :option_type_id => @option_type.id
      Option.create :long_name => "Step-Son", :option_type_id => @option_type.id
      Option.create :long_name => "Son", :option_type_id => @option_type.id
      Option.create :long_name => "Son-in-Law", :option_type_id => @option_type.id
      Option.create :long_name => "Uncle", :option_type_id => @option_type.id
      Option.create :long_name => "Wife", :option_type_id => @option_type.id

      # Bond Status
      ot = OptionType.create(:name => "Bond Status")
      ot.options.build(:long_name => 'Active', :permanent => true)
      ot.options.build(:long_name => 'Final', :permanent => true)
      ot.options.build(:long_name => 'Forfeited', :permanent => true)
      ot.options.build(:long_name => 'Revoked', :permanent => true)
      ot.options.build(:long_name => 'Surrendered', :permanent => true)
      ot.options.build(:long_name => 'For Other Agency', :permanent => true)
      ot.options.each{|o| o.save}

      # Bond Type
      ot = OptionType.create(:name => 'Bond Type')
      ot.options.build(:long_name => 'Cash Bond')
      ot.options.build(:long_name => 'City Charges')
      ot.options.build(:long_name => 'No Bond')
      ot.options.build(:long_name => 'O.R. Bond')
      ot.options.build(:long_name => 'Out of Parish')
      ot.options.build(:long_name => 'Professional')
      ot.options.build(:long_name => 'Property')
      ot.options.build(:long_name => 'Ticket')
      ot.options.build(:long_name => 'Unsecured')
      ot.options.each{|o| o.save}

      # Bug Priority
      ot = OptionType.create(:name => 'Bug Priority')
      ot.options.build(:long_name => '1 - Critical', :permanent => true)
      ot.options.build(:long_name => '2 - High', :permanent => true)
      ot.options.build(:long_name => '3 - Normal', :permanent => true)
      ot.options.build(:long_name => '4 - Low', :permanent => true)
      ot.options.build(:long_name => '5 - Wishlist', :permanent => true)
      ot.options.build(:long_name => '6 - Question', :permanent => true)
      ot.options.build(:long_name => '7 - Suggestion', :permanent => true)
      ot.options.each{|o| o.save}

      # Bug Resolution
      ot = OptionType.create(:name => 'Bug Resolution')
      ot.options.build(:long_name => 'Fixed', :permanent => true)
      ot.options.build(:long_name => 'Not A Bug', :permanent => true)
      ot.options.build(:long_name => 'Duplicate Bug', :permanent => true)
      ot.options.build(:long_name => 'Not Satisfied', :permanent => true)
      ot.options.each{|o| o.save}

      # Bug Status
      ot = OptionType.create(:name => 'Bug Status')
      ot.options.build(:long_name => 'New', :short_name => 'bug-new', :permanent => true)
      ot.options.build(:long_name => 'Working On', :short_name => 'bug-working', :permanent => true)
      ot.options.build(:long_name => 'Hold', :short_name => 'bug-hold', :permanent => true)
      ot.options.build(:long_name => 'Resolved', :short_name => 'bug-fixed', :permanent => true)
      ot.options.build(:long_name => 'Reopened', :short_name => 'bug-new', :permanent => true)
      ot.options.each{|o| o.save}

      # Build Type
      @option_type = OptionType.create :name => 'Build'
      Option.create :long_name => "Slender", :option_type_id => @option_type.id
      Option.create :long_name => "Light", :option_type_id => @option_type.id
      Option.create :long_name => "Medium", :option_type_id => @option_type.id
      Option.create :long_name => "Heavy", :option_type_id => @option_type.id
      Option.create :long_name => "Obese", :option_type_id => @option_type.id

      # Call Disposition
      @option_type = OptionType.create :name => 'Call Disposition'
      Option.create :long_name => "Officer Report(s)", :short_name => "OFC", :option_type_id => @option_type.id
      Option.create :long_name => "Report Only", :short_name => "RPT",:option_type_id => @option_type.id
      Option.create :long_name => "Referred to Detective", :short_name => "DET", :option_type_id => @option_type.id

      # Cell
      ot = OptionType.create(:name => "Cell")
      ot.options.create(:long_name => 'Other', :permanent => true)

      # Commissary Type
      ot = OptionType.create(:name => 'Commissary Type')
      ot.options.build(:long_name => 'Deposit')
      ot.options.build(:long_name => 'Commissary Sales')
      ot.options.build(:long_name => 'Medical')
      ot.options.build(:long_name => 'Refund Check')
      ot.options.build(:long_name => 'Balance Adjustment')
      ot.save

      # Complexion
      @option_type = OptionType.create :name => 'Complexion'
      Option.create :long_name => 'Acne', :option_type_id => @option_type.id
      Option.create :long_name => 'Dark', :option_type_id => @option_type.id
      Option.create :long_name => 'Fair', :option_type_id => @option_type.id
      Option.create :long_name => 'Freckled', :option_type_id => @option_type.id
      Option.create :long_name => 'Medium', :option_type_id => @option_type.id
      Option.create :long_name => 'Olive', :option_type_id => @option_type.id
      Option.create :long_name => 'Pale', :option_type_id => @option_type.id
      Option.create :long_name => 'Pocked', :option_type_id => @option_type.id
      Option.create :long_name => 'Ruddy', :option_type_id => @option_type.id
      Option.create :long_name => 'Tanned', :option_type_id => @option_type.id

      ot = OptionType.create(:name => 'Consecutive Type')
      ot.options.create(:long_name => 'This Docket', :permanent => 'true', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'Other Dockets', :permanent => 'true', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'Any Current', :permanent => 'true', :created_by => 1, :updated_by => 1)

      # Court Costs
      OptionType.create(:name => "Court Costs")

      # Court Type
      ot = OptionType.create(:name => "Court Type")
      ot.options.build(:long_name => 'Misdemeanor')
      ot.options.build(:long_name => 'Arraignment')
      ot.options.build(:long_name => 'Motion Hearing')
      ot.options.build(:long_name => 'Pre-Trial')
      ot.options.build(:long_name => 'Trial')
      ot.options.build(:long_name => 'Sentencing')
      ot.options.build(:long_name => 'Probation/Parole')
      ot.save(false)

      # Docket Type
      ot = OptionType.create(:name => 'Docket Type')
      ot.options.create(:long_name => 'Arraignment', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'Misdemeanor', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'Motion', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'Pre-Trial', :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'Trial', :created_by => 1, :updated_by => 1)

      # Eye Color
      @option_type = OptionType.create :name => 'Eye Color'
      Option.create :long_name => 'Blue', :option_type_id => @option_type.id
      Option.create :long_name => 'Brown', :option_type_id => @option_type.id
      Option.create :long_name => 'Green', :option_type_id => @option_type.id
      Option.create :long_name => 'Hazel', :option_type_id => @option_type.id
      Option.create :long_name => 'Gray', :option_type_id => @option_type.id

      # Facial Hair
      @option_type = OptionType.create :name => 'Facial Hair'
      Option.create :long_name => 'Beard', :option_type_id => @option_type.id
      Option.create :long_name => 'Beard & Mustache', :option_type_id => @option_type.id
      Option.create :long_name => 'Bushy Eyebrows', :option_type_id => @option_type.id
      Option.create :long_name => 'Fu-manchu', :option_type_id => @option_type.id
      Option.create :long_name => 'Goatee', :option_type_id => @option_type.id
      Option.create :long_name => 'Long Beard', :option_type_id => @option_type.id
      Option.create :long_name => 'Mustache', :option_type_id => @option_type.id
      Option.create :long_name => 'Short Beard', :option_type_id => @option_type.id
      Option.create :long_name => 'Sideburns', :option_type_id => @option_type.id
      Option.create :long_name => 'Sideburns & Bushy Eyebrows', :option_type_id => @option_type.id
      Option.create :long_name => 'Sideburns & Mustache', :option_type_id => @option_type.id
      Option.create :long_name => 'Sideburns, Mustache & Bushy Eyebrows', :option_type_id => @option_type.id
      Option.create :long_name => 'Unshaven', :option_type_id => @option_type.id

      # Facility
      OptionType.create(:name => "Facility")

      # Hair Color
      @option_type = OptionType.create :name => 'Hair Color'
      Option.create :long_name => 'Black', :option_type_id => @option_type.id
      Option.create :long_name => 'Blond', :option_type_id => @option_type.id
      Option.create :long_name => 'Brown', :option_type_id => @option_type.id
      Option.create :long_name => 'Gray', :option_type_id => @option_type.id
      Option.create :long_name => 'Red', :option_type_id => @option_type.id

      # Hair Type
      @option_type = OptionType.create :name => 'Hair Type'
      Option.create :long_name => 'Afro', :option_type_id => @option_type.id
      Option.create :long_name => 'Bald', :option_type_id => @option_type.id
      Option.create :long_name => 'Full Afro', :option_type_id => @option_type.id
      Option.create :long_name => 'Long', :option_type_id => @option_type.id
      Option.create :long_name => 'Medium', :option_type_id => @option_type.id
      Option.create :long_name => 'Partial Bald', :option_type_id => @option_type.id
      Option.create :long_name => 'Short', :option_type_id => @option_type.id
      Option.create :long_name => 'Short Afro', :option_type_id => @option_type.id

      # History Type
      ot = OptionType.create(:name => "History Type")
      ot.options.build(:long_name => 'Rap Sheet')
      ot.options.build(:long_name => 'Call Sheet')
      ot.options.build(:long_name => 'Pawn Ticket')
      ot.options.build(:long_name => 'Warrant')
      ot.save(false)

      # Hold Type        
      mytype = OptionType.create(:name => "Hold Type")
      Option.create :long_name => '72 Hour', :option_type_id => mytype.id, :permanent => true
      Option.create :long_name => '48 Hour', :option_type_id => mytype.id, :permanent => true
      Option.create :long_name => 'No Bond', :option_type_id => mytype.id, :permanent => true
      Option.create :long_name => 'Bondable', :option_type_id => mytype.id, :permanent => true
      Option.create :long_name => 'Pending Court',:short_name => 'manual', :option_type_id => mytype.id
      Option.create :long_name => 'Pending Sentence', :short_name => 'manual', :option_type_id => mytype.id, :permanent => true
      Option.create :long_name => 'Fines/Costs', :option_type_id => mytype.id, :permanent => true
      Option.create :long_name => 'Serving Sentence', :option_type_id => mytype.id, :permanent => true
      Option.create :long_name => 'Probation/Parole (State)', :short_name => 'manual', :option_type_id => mytype.id
      Option.create :long_name => 'Probation (Parish)', :short_name => 'manual', :option_type_id => mytype.id
      Option.create :long_name => 'Hold for Questioning', :short_name => 'manual', :option_type_id => mytype.id
      Option.create :long_name => 'Temporary Housing', :option_type_id => mytype.id, :permanent => true
      Option.create :long_name => 'Temporary Transfer', :option_type_id => mytype.id, :permanent => true, :disabled => true
      Option.create :long_name => 'Hold for Other Agency', :short_name => 'manual', :option_type_id => mytype.id, :permanent => true
      Option.create :long_name => 'Hold for City', :short_name => 'manual', :option_type_id => mytype.id, :permanent => true
      Option.create :long_name => 'Legacy Hold', :option_type_id => mytype.id, :permanent => true, :disabled => true
      
      # Investigation Status
      ot = OptionType.create(:name => "Investigation Status")
      ot.options.build(:long_name => 'Active')
      ot.options.build(:long_name => 'Trial')
      ot.options.build(:long_name => 'Solved')
      ot.options.build(:long_name => 'Inactive')
      ot.save(false)

      # Judge
      ot = OptionType.create(:name => "Judge")
      ot.options.build(:long_name => 'Preset Bond', :permanent => true)
      ot.options.each{|o| o.save}

      # Noservice Type
      ot = OptionType.create(:name => "Noservice Type")
      ot.options.build(:long_name => 'Moved, Address Unknown')
      ot.options.build(:long_name => 'Does Not Live at This Address')
      ot.options.build(:long_name => 'Please Provide Physical Address')
      ot.options.build(:long_name => 'Address Not In Sabine Parish')
      ot.options.build(:long_name => 'Unable to Locate')
      ot.options.build(:long_name => 'Vacant House/Lot/Camp')
      ot.options.build(:long_name => 'Works Out of State')
      ot.options.build(:long_name => 'In Jail Out of Sabine Parish')
      ot.options.build(:long_name => 'No Such Number or Address')
      ot.options.build(:long_name => 'Tagged and Sent To Clerks ON')
      ot.options.build(:long_name => 'Received Too Late for Service')
      ot.options.build(:long_name => 'Paid Per DAs Office')
      ot.options.build(:long_name => 'Deceased')
      ot.options.build(:long_name => 'Moved To:', :permanent => true)
      ot.options.build(:long_name => 'Military Reasons')
      ot.save(false)

      # Paper Type
      ot = OptionType.create(:name => "Paper Type")
      ot.options.build(:long_name => 'Adoption Notice', :short_name => '20.00')
      ot.options.build(:long_name => 'Applying For Intrafamily Adoption', :short_name => '20.00')
      ot.options.build(:long_name => 'Appraisal Notice', :short_name => '20.00')
      ot.options.build(:long_name => 'Arbitration Claim', :short_name => '50.00')
      ot.options.build(:long_name => 'Case Review Judgment', :short_name => '20.00')
      ot.options.build(:long_name => 'Child Support Notice', :short_name => '20.00')
      ot.options.build(:long_name => 'Child Support Rule To Show Cause', :short_name => '20.00')
      ot.options.build(:long_name => 'Citation', :short_name => '20.00')
      ot.options.build(:long_name => 'Citation and Notice Of Trial', :short_name => '20.00')
      ot.options.build(:long_name => 'Citation for Judgment Debtor Rule', :short_name => '20.00')
      ot.options.build(:long_name => 'Citation On Rule', :short_name => '20.00')
      ot.options.build(:long_name => 'Citation On Rule To Show Cause', :short_name => '20.00')
      ot.options.build(:long_name => 'Citation To Garnishee', :short_name => '20.00')
      ot.options.build(:long_name => 'Citation To Judgment Debtor', :short_name => '20.00')
      ot.options.build(:long_name => 'Civil Citation', :short_name => '20.00')
      ot.options.build(:long_name => 'Civil Subpoena', :short_name => '20.00')
      ot.options.build(:long_name => 'Civil Suit', :short_name => '20.00')
      ot.options.build(:long_name => 'Civil Summons', :short_name => '20.00')
      ot.options.build(:long_name => 'Clerks Suit Citation', :short_name => '20.00')
      ot.options.build(:long_name => 'Criminal Subpoena', :short_name => '20.00')
      ot.options.build(:long_name => 'Deposition Subpoena', :short_name => '20.00')
      ot.options.build(:long_name => 'Deposition Subpoena and Subpoena D', :short_name => '20.00')
      ot.options.build(:long_name => 'Divorce Action With Rules', :short_name => '20.00')
      ot.options.build(:long_name => 'Divorce Citation', :short_name => '20.00')
      ot.options.build(:long_name => 'Examination of Judgment Debtor', :short_name => '20.00')
      ot.options.build(:long_name => 'Garnishment', :short_name => '20.00')
      ot.options.build(:long_name => 'Hearing Reset', :short_name => '20.00')
      ot.options.build(:long_name => 'Interragotories', :short_name => '20.00')
      ot.options.build(:long_name => 'Judgment', :short_name => '0.00')
      ot.options.build(:long_name => 'Judgment Debtor Rule', :short_name => '20.00')
      ot.options.build(:long_name => 'Judgment Debtor Summons', :short_name => '20.00')
      ot.options.build(:long_name => 'Mileage', :short_name => '0.88', :permanent => true)
      ot.options.build(:long_name => 'Motion And Order', :short_name => '20.00')
      ot.options.build(:long_name => 'Motion And Order to Produce Recor', :short_name => '20.00')
      ot.options.build(:long_name => 'Motion for Summary Judgment', :short_name => '20.00')
      ot.options.build(:long_name => 'NASD Dispute Resolution Subpoena', :short_name => '50.00')
      ot.options.build(:long_name => 'Notice', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice / Pretrial Conference Order', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice In Accordance w/ LA R.S. 48:44', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice Of Appointment', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice Of Court Date', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Deposition With Subpoena', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Divorce', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Divorce Action', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Filing', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Fixing Case', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Fixing Jury Trial Bond', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Hearing', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Judgment', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Judgment for Mortgage R', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Paternity Tests Results', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Pleadings Filed', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Protective Order Judgment', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Refixing', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Refixing Court Date', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Rule to Show Cause', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Seizure', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Seizure - No Appraisal', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Seizure in Garnishment', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Sheriffs Sale', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Signing Judgment', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Status or Pretrial Conference', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Trial', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice of Trial Fixing', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice to Appoint Appraiser', :short_name => '20.00')
      ot.options.build(:long_name => 'Notice to Produce', :short_name => '20.00')
      ot.options.build(:long_name => 'Order', :short_name => '20.00')
      ot.options.build(:long_name => 'Order to Produce', :short_name => '20.00')
      ot.options.build(:long_name => 'Out of State', :short_name => '50.00')
      ot.options.build(:long_name => 'Pauper Suit', :short_name => '0.00')
      ot.options.build(:long_name => 'Permanent Restraining Order', :short_name => '20.00')
      ot.options.build(:long_name => 'Petition For Divorce', :short_name => '20.00')
      ot.options.build(:long_name => 'Petition For Interpleader Citation', :short_name => '20.00')
      ot.options.build(:long_name => 'Petition For Order For Protection', :short_name => '20.00')
      ot.options.build(:long_name => 'Petition To Establish Paternity and S', :short_name => '33.00')
      ot.options.build(:long_name => 'Protective Order', :short_name => '20.00')
      ot.options.build(:long_name => 'Records Deposition', :short_name => '20.00')
      ot.options.build(:long_name => 'Request for Admissions', :short_name => '20.00')
      ot.options.build(:long_name => 'Restraining Order', :short_name => '20.00')
      ot.options.build(:long_name => 'Rule', :short_name => '20.00')
      ot.options.build(:long_name => 'Rule and Orders To Show Cause', :short_name => '20.00')
      ot.options.build(:long_name => 'Rule NISI', :short_name => '20.00')
      ot.options.build(:long_name => 'Rule to Show Cause', :short_name => '20.00')
      ot.options.build(:long_name => 'Rule to Show Cause And Order to Pr', :short_name => '20.00')
      ot.options.build(:long_name => 'SC Citation', :short_name => '20.00')
      ot.options.build(:long_name => 'Sheriffs Notice', :short_name => '20.00')
      ot.options.build(:long_name => 'Subpoena', :short_name => '20.00')
      ot.options.build(:long_name => 'Subpoena Ad Testificandum', :short_name => '20.00')
      ot.options.build(:long_name => 'Subpoena And Subpoena Duces Tecum', :short_name => '20.00')
      ot.options.build(:long_name => 'Subpoena Duces Tecum', :short_name => '20.00')
      ot.options.build(:long_name => 'Subpoena Duces Tecum for Depo', :short_name => '0.00')
      ot.options.build(:long_name => 'Subpoena for Deposition', :short_name => '20.00')
      ot.options.build(:long_name => 'Subpoena for Deposition/Subpoena D', :short_name => '20.00')
      ot.options.build(:long_name => 'Subpoena for Witness', :short_name => '20.00')
      ot.options.build(:long_name => 'Subpoena for Witness and Subpoena', :short_name => '20.00')
      ot.options.build(:long_name => 'Subpoena/Subpoena Duces Tecum', :short_name => '20.00')
      ot.options.build(:long_name => 'Summons', :short_name => '20.00')
      ot.options.build(:long_name => 'Summons for Deposition', :short_name => '20.00')
      ot.options.build(:long_name => 'Summons to Judgment Debtor', :short_name => '20.00')
      ot.options.build(:long_name => 'Supplemental Citation', :short_name => '20.00')
      ot.options.build(:long_name => 'Temp Rest Order and Rule to Show C', :short_name => '20.00')
      ot.options.build(:long_name => 'Temporary Restraining Order', :short_name => '20.00')
      ot.options.build(:long_name => 'Trial Subpoena', :short_name => '20.00')
      ot.options.build(:long_name => 'Unable To Locate', :short_name => '5.00')
      ot.options.build(:long_name => 'Witness Subpoena', :short_name => '20.00')
      ot.options.build(:long_name => 'Writ of Attachment', :short_name => '20.00')
      ot.options.build(:long_name => 'Writ of Fieri Facias', :short_name => '20.00')
      ot.options.build(:long_name => 'Writ of Injunction', :short_name => '20.00')
      ot.options.build(:long_name => 'Writ of Possession', :short_name => '20.00')
      ot.options.build(:long_name => 'Writ of Preliminary Injunction', :short_name => '20.00')
      ot.options.build(:long_name => 'Writ of Seizure and Sale', :short_name => '20.00')
      ot.options.build(:long_name => 'Writ of Subpoena', :short_name => '20.00')
      ot.options.build(:long_name => 'Writ of Subpoena Duces Tecum', :short_name => '20.00')
      ot.save(false)

      # Pawn Company
      OptionType.create :name => 'Pawn Company'

      # Pawn Ticket Type
      @option_type = OptionType.create :name => 'Pawn Ticket Type'
      Option.create :long_name => "Pawned", :option_type_id => @option_type.id
      Option.create :long_name => "Purchased", :option_type_id => @option_type.id
      Option.create :long_name => "Picked Up", :option_type_id => @option_type.id

      # Pawn Type
      @option_type = OptionType.create :name => 'Pawn Type'
      Option.create :long_name => "Animals / Supplies", :option_type_id => @option_type.id
      Option.create :long_name => "Appliances", :option_type_id => @option_type.id
      Option.create :long_name => "Audio / Video", :option_type_id => @option_type.id
      Option.create :long_name => "Fishing Tackle", :option_type_id => @option_type.id
      Option.create :long_name => "Furniture", :option_type_id => @option_type.id
      Option.create :long_name => "Handgun", :option_type_id => @option_type.id
      Option.create :long_name => "Household Items", :option_type_id => @option_type.id
      Option.create :long_name => "Jewelry", :option_type_id => @option_type.id
      Option.create :long_name => "Lawn & Garden Equipment", :option_type_id => @option_type.id
      Option.create :long_name => "Marine Equipment", :option_type_id => @option_type.id
      Option.create :long_name => "Miscellaneous", :option_type_id => @option_type.id
      Option.create :long_name => "Musical Instruments", :option_type_id => @option_type.id
      Option.create :long_name => "Photographic Equipment", :option_type_id => @option_type.id
      Option.create :long_name => "Rifle", :option_type_id => @option_type.id
      Option.create :long_name => "Shotgun", :option_type_id => @option_type.id
      Option.create :long_name => "Tac / Saddles", :option_type_id => @option_type.id
      Option.create :long_name => "Timber Devices", :option_type_id => @option_type.id
      Option.create :long_name => "Tools / Tool Boxes", :option_type_id => @option_type.id
      Option.create :long_name => "Toys / Bikes", :option_type_id => @option_type.id
      Option.create :long_name => "Vehicles", :option_type_id => @option_type.id

      # Payment Type
      ot = OptionType.create(:name => "Payment Type")
      ot.options.build(:long_name => 'Check')
      ot.options.build(:long_name => 'Cash')
      ot.options.build(:long_name => 'Money Order')
      ot.save(false)

      # Phone Type
      @option_type = OptionType.create :name => 'Phone Type'
      Option.create :long_name => "Home", :option_type_id => @option_type.id
      Option.create :long_name => "Work", :option_type_id => @option_type.id
      Option.create :long_name => "Cell", :option_type_id => @option_type.id
      Option.create :long_name => "Pager", :option_type_id => @option_type.id
      Option.create :long_name => "FAX", :option_type_id => @option_type.id
      Option.create :long_name => "Voice Mail", :option_type_id => @option_type.id
      Option.create :long_name => "Primary", :option_type_id => @option_type.id
      Option.create :long_name => "Alternate", :option_type_id => @option_type.id
      Option.create :long_name => "Toll Free", :option_type_id => @option_type.id
      Option.create :long_name => "Emergency", :option_type_id => @option_type.id
      Option.create :long_name => "Unlisted", :option_type_id => @option_type.id

      # Plea Type
      ot = OptionType.create(:name => "Plea Type")
      ot.options.build(:long_name => 'Guilty')
      ot.options.build(:long_name => 'Not Guilty')
      ot.save(false)

      # Probation Status
      ot = OptionType.create(:name => 'Probation Status')
      ot.options.build(:long_name => 'Pending', :permanent => true)
      ot.options.build(:long_name => 'Active', :permanent => true)
      ot.options.build(:long_name => 'Completed', :permanent => true)
      ot.options.build(:long_name => 'Revoked', :permanent => true)
      ot.options.build(:long_name => 'Unsupervised', :permanent => true)
      ot.options.build(:long_name => 'State', :permanent => true)
      ot.options.each{|o| o.save}

      # Probation Type
      ot = OptionType.create(:name => "Probation Type")
      ot.options.create(:long_name => 'Parish', :permanent => true)
      ot.options.create(:long_name => 'State', :permanent => true)
      ot.options.create(:long_name => 'Unsupervised', :permanent => true)

      # Race
      @option_type = OptionType.create :name => 'Race'
      Option.create :long_name => 'Black', :short_name => 'B', :option_type_id => @option_type.id
      Option.create :long_name => 'White', :short_name => 'W', :option_type_id => @option_type.id
      Option.create :long_name => 'Asian', :short_name => 'A', :option_type_id => @option_type.id
      Option.create :long_name => 'Hispanic', :short_name => 'H', :option_type_id => @option_type.id
      Option.create :long_name => 'Am. Indian', :short_name => 'I', :option_type_id => @option_type.id

      # Received Via
      @option_type = OptionType.create :name => 'Call Via'
      Option.create :long_name => "911", :option_type_id => @option_type.id
      Option.create :long_name => "Phone", :option_type_id => @option_type.id
      Option.create :long_name => "Radio", :option_type_id => @option_type.id
      Option.create :long_name => "Person", :option_type_id => @option_type.id
      Option.create :long_name => "Alarm", :option_type_id => @option_type.id
      Option.create :long_name => "E-Mail", :option_type_id => @option_type.id
      Option.create :long_name => "Text Message", :option_type_id => @option_type.id
      Option.create :long_name => "FAX", :option_type_id => @option_type.id

      # Release Type
      ot = OptionType.create(:name => 'Release Type')
      ot.options.build(:long_name => '48 Hour Expired Per Judge')
      ot.options.build(:long_name => '48 Hour Signed By Judge')
      ot.options.build(:long_name => '72 Hour Hearing Completed', :permanent => true)
      ot.options.build(:long_name => 'Bonded')
      ot.options.build(:long_name => 'Fines/Costs Paid')
      ot.options.build(:long_name => 'Escaped')
      ot.options.build(:long_name => 'Released By Judge')
      ot.options.build(:long_name => 'Released By Probation/Parole')
      ot.options.build(:long_name => 'Time Served', :permanent => true)
      ot.options.build(:long_name => 'Transferred', :permanent => true)
      ot.options.build(:long_name => 'Released W/O Charges')
      ot.options.build(:long_name => 'Manual Release')
      ot.options.build(:long_name => 'Preset Bond', :permanent => true)
      ot.options.build(:long_name => 'Returned From Transfer', :permanent => true)
      ot.options.build(:long_name => 'Automatic Release', :permanent => true, :disabled => true)
      ot.options.each{|o| o.save}

      # Service Type
      ot = OptionType.create(:name => "Service Type")
      ot.options.build(:long_name => 'Domiciliary', :permanent => true)
      ot.options.build(:long_name => 'No Service', :permanent => true)
      ot.options.build(:long_name => 'Personal', :permanent => true)
      ot.save(false)

      # States
      @option_type = OptionType.create :name => 'State'
      Option.create :long_name => 'Alabama', :short_name => 'AL', :option_type_id => @option_type.id
      Option.create :long_name => 'Alaska', :short_name => 'AK', :option_type_id => @option_type.id
      Option.create :long_name => 'Arizona', :short_name => 'AZ', :option_type_id => @option_type.id
      Option.create :long_name => 'Arkansas', :short_name => 'AR', :option_type_id => @option_type.id
      Option.create :long_name => 'California', :short_name => 'CA', :option_type_id => @option_type.id
      Option.create :long_name => 'Colorado', :short_name => 'CO', :option_type_id => @option_type.id
      Option.create :long_name => 'Connecticut', :short_name => 'CT', :option_type_id => @option_type.id
      Option.create :long_name => 'Delaware', :short_name => 'DE', :option_type_id => @option_type.id
      Option.create :long_name => 'District Of Columbia', :short_name => 'DC', :option_type_id => @option_type.id
      Option.create :long_name => 'Florida', :short_name => 'FL', :option_type_id => @option_type.id
      Option.create :long_name => 'Georgia', :short_name => 'GA', :option_type_id => @option_type.id
      Option.create :long_name => 'Hawaii', :short_name => 'HI', :option_type_id => @option_type.id
      Option.create :long_name => 'Idaho', :short_name => 'ID', :option_type_id => @option_type.id
      Option.create :long_name => 'Illinois', :short_name => 'IL', :option_type_id => @option_type.id
      Option.create :long_name => 'Indiana', :short_name => 'IN', :option_type_id => @option_type.id
      Option.create :long_name => 'Iowa', :short_name => 'IA', :option_type_id => @option_type.id
      Option.create :long_name => 'Kansas', :short_name => 'KS', :option_type_id => @option_type.id
      Option.create :long_name => 'Kentucky', :short_name => 'KY', :option_type_id => @option_type.id
      Option.create :long_name => 'Louisiana', :short_name => 'LA', :option_type_id => @option_type.id
      Option.create :long_name => 'Maine', :short_name => 'ME', :option_type_id => @option_type.id
      Option.create :long_name => 'Maryland', :short_name => 'MD', :option_type_id => @option_type.id
      Option.create :long_name => 'Massachusetts', :short_name => 'MA', :option_type_id => @option_type.id
      Option.create :long_name => 'Michigan', :short_name => 'MI', :option_type_id => @option_type.id
      Option.create :long_name => 'Minnesota', :short_name => 'MN', :option_type_id => @option_type.id
      Option.create :long_name => 'Mississippi', :short_name => 'MS', :option_type_id => @option_type.id
      Option.create :long_name => 'Missouri', :short_name => 'MO', :option_type_id => @option_type.id
      Option.create :long_name => 'Montana', :short_name => 'MT', :option_type_id => @option_type.id
      Option.create :long_name => 'Nebraska', :short_name => 'NE', :option_type_id => @option_type.id
      Option.create :long_name => 'Nevada', :short_name => 'NV', :option_type_id => @option_type.id
      Option.create :long_name => 'New Hampshire', :short_name => 'NH', :option_type_id => @option_type.id
      Option.create :long_name => 'New Jersey', :short_name => 'NJ', :option_type_id => @option_type.id
      Option.create :long_name => 'New Mexico', :short_name => 'NM', :option_type_id => @option_type.id
      Option.create :long_name => 'New York', :short_name => 'NY', :option_type_id => @option_type.id
      Option.create :long_name => 'North Carolina', :short_name => 'NC', :option_type_id => @option_type.id
      Option.create :long_name => 'North Dakota', :short_name => 'ND', :option_type_id => @option_type.id
      Option.create :long_name => 'Ohio', :short_name => 'OH', :option_type_id => @option_type.id
      Option.create :long_name => 'Oklahoma', :short_name => 'OK', :option_type_id => @option_type.id
      Option.create :long_name => 'Oregon', :short_name => 'OR', :option_type_id => @option_type.id
      Option.create :long_name => 'Pennsylvania', :short_name => 'PA', :option_type_id => @option_type.id
      Option.create :long_name => 'Rhode Island', :short_name => 'RI', :option_type_id => @option_type.id
      Option.create :long_name => 'South Carolina', :short_name => 'SC', :option_type_id => @option_type.id
      Option.create :long_name => 'South Dakota', :short_name => 'SD', :option_type_id => @option_type.id
      Option.create :long_name => 'Tennessee', :short_name => 'TN', :option_type_id => @option_type.id
      Option.create :long_name => 'Texas', :short_name => 'TX', :option_type_id => @option_type.id
      Option.create :long_name => 'Utah', :short_name => 'UT', :option_type_id => @option_type.id
      Option.create :long_name => 'Vermont', :short_name => 'VT', :option_type_id => @option_type.id
      Option.create :long_name => 'Virginia', :short_name => 'VA', :option_type_id => @option_type.id
      Option.create :long_name => 'Washington', :short_name => 'WA', :option_type_id => @option_type.id
      Option.create :long_name => 'West Virginia', :short_name => 'WV', :option_type_id => @option_type.id
      Option.create :long_name => 'Wisconsin', :short_name => 'WI', :option_type_id => @option_type.id
      Option.create :long_name => 'Wyoming', :short_name => 'WY', :option_type_id => @option_type.id

      # Subject Type
      ot = OptionType.create(:name => "Subject Type")
      Option.create(:option_type_id => ot.id, :long_name => 'Complainant', :created_by => 1, :updated_by => 1)
      Option.create(:option_type_id => ot.id, :long_name => 'Reported By', :created_by => 1, :updated_by => 1)
      Option.create(:option_type_id => ot.id, :long_name => 'Subject', :created_by => 1, :updated_by => 1)
      Option.create(:option_type_id => ot.id, :long_name => 'Victim', :created_by => 1, :updated_by => 1)
      Option.create(:option_type_id => ot.id, :long_name => 'Witness', :created_by => 1, :updated_by => 1)
      Option.create(:option_type_id => ot.id, :long_name => 'Arrested', :created_by => 1, :updated_by => 1)

      # Suit Type
      ot = OptionType.create(:name => "Suit Type")
      ot.options.build(:long_name => 'Child Support/Notice of Judgment')
      ot.options.build(:long_name => 'Child Support/Paternity Test (Notice Of)')
      ot.options.build(:long_name => 'Child Support/Rule to Show Cause')
      ot.options.build(:long_name => 'Citation')
      ot.options.build(:long_name => 'Citation and Rule to Show Cause')
      ot.options.build(:long_name => 'Citation to Judgment Debtor')
      ot.options.build(:long_name => 'Clerks Suit Citation')
      ot.options.build(:long_name => 'Clerks Suit Citation and Rule to Show Cause')
      ot.options.build(:long_name => 'Criminal Subpoena')
      ot.options.build(:long_name => 'DA Subpoena')
      ot.options.build(:long_name => 'Deposition Subpoena and Subpoena Duces Tecum')
      ot.options.build(:long_name => 'Grand Jury Subpoena')
      ot.options.build(:long_name => 'Judgment')
      ot.options.build(:long_name => 'Notice of Fixing Case for Arraignment')
      ot.options.build(:long_name => 'Notice of Hearing')
      ot.options.build(:long_name => 'Notice of Hearing on MT Reduce Bond')
      ot.options.build(:long_name => 'Notice of Judgment')
      ot.options.build(:long_name => 'Notice of Motion to Run Sentence Concurrent')
      ot.options.build(:long_name => 'Notice of Motion to Suppress Hearing')
      ot.options.build(:long_name => 'Notice of Paternity Tests Results / DSS')
      ot.options.build(:long_name => 'Notice of Preliminary Examination')
      ot.options.build(:long_name => 'Notice of Prob/Revo Hearing')
      ot.options.build(:long_name => 'Notice of Refixing')
      ot.options.build(:long_name => 'Notice of Restitution Hearing')
      ot.options.build(:long_name => 'Notice of Rule to Show Cause')
      ot.options.build(:long_name => 'Notice of Seizure')
      ot.options.build(:long_name => 'Notice of Seizure in Garnishment')
      ot.options.build(:long_name => 'Notice of Sentencing')
      ot.options.build(:long_name => 'Notice of Status Hearing')
      ot.options.build(:long_name => 'Notice of Trial')
      ot.options.build(:long_name => 'Notice of Trial By Jury')
      ot.options.build(:long_name => 'Notice to Appoint Appraiser')
      ot.options.build(:long_name => 'Protective Order')
      ot.options.build(:long_name => 'Restraining Order')
      ot.options.build(:long_name => 'Rule to Show Cause')
      ot.options.build(:long_name => 'Rule to Show Cause and Notice')
      ot.options.build(:long_name => 'Subpoena')
      ot.options.build(:long_name => 'Subpoena and Subpoena Duces Tecum')
      ot.options.build(:long_name => 'Subpoena Duces Tecum')
      ot.options.build(:long_name => 'Subpeona for Deposition')
      ot.options.build(:long_name => 'Subpeona for Witness')
      ot.options.build(:long_name => 'Subpoena for Witness and Subpoena Duces Tucem - DE')
      ot.options.build(:long_name => 'Summons')
      ot.options.build(:long_name => 'Summons to Judgment Debtor')
      ot.options.build(:long_name => 'Temp Rest Order and Rule to Show Cause')
      ot.options.build(:long_name => 'Temporary Restraining Order')
      ot.options.build(:long_name => 'Writ of Attachment')
      ot.options.build(:long_name => 'Writ of Injunction')
      ot.save(false)

      # Transfer Type
      ot = OptionType.create(:name => 'Transfer Type')
      ot.options.create(:long_name => 'Temporary Transfer (OUT)', :permanent => true, :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'Temporary Transfer Return (IN)', :permanent => true, :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'Temporary Housing (IN)', :permanent => true, :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'Temporary Housing Return (OUT)', :permanent => true, :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'DOC Transfer (IN)', :permanent => true, :created_by => 1, :updated_by => 1)
      ot.options.create(:long_name => 'Permanent Transfer (OUT)', :permanent => true, :created_by => 1, :updated_by => 1)

      # Trial Result
      ot = OptionType.create(:name => "Trial Result")
      ot.options.build(:long_name => 'Pled Guilty', :permanent => true)
      ot.options.build(:long_name => 'Found Guilty', :permanent => true)
      ot.options.build(:long_name => 'Not Guilty', :permanent => true)
      ot.options.build(:long_name => 'Dismissed', :permanent => true)
      ot.save(false)

      # Vehicle Type
      ot = OptionType.create(:name => "Vehicle Type")
      ot.options.build(:long_name => 'Truck')
      ot.options.build(:long_name => 'Van')
      ot.options.build(:long_name => 'Car')
      ot.options.build(:long_name => 'Motorcycle')
      ot.options.build(:long_name => 'Boat')
      ot.options.build(:long_name => 'Bicycle')
      ot.save(false)

      # Warrant Disposition
      @option_type = OptionType.create :name => 'Warrant Disposition'
      Option.create :long_name => "Served", :option_type_id => @option_type.id, :permanent => true
      Option.create :long_name => "Recalled", :option_type_id => @option_type.id
      Option.create :long_name => "Paid", :option_type_id => @option_type.id
      Option.create :long_name => "Returned", :option_type_id => @option_type.id
   end

   def self.down
      OptionType.destroy_all
   end
end
