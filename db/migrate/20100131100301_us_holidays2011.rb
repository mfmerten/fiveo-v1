# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class UsHolidays2011 < ActiveRecord::Migration
   class Event < ActiveRecord::Base
       before_validation :fix_end_date
       def fix_end_date
           unless self.end_date?
               self.end_date = self.start_date
           end
           if self.start_date > self.end_date
               tdate = self.start_date
               self.start_date = self.end_date
               self.end_date = tdate
           end
           return true
       end
   end

   def self.up
       # US Federal Holidays with some Secular added
       Event.create :name => "New Year's Day", :start_date => Date.new(2011,1,1), :created_by => 1, :updated_by => 1
       Event.create :name => "Martin Luther King Jr. Birthday", :start_date => Date.new(2011,1,17), :created_by => 1, :updated_by => 1
       Event.create :name => "President's Day", :start_date => Date.new(2011,2,21), :created_by => 1, :updated_by => 1
       Event.create :name => "Easter", :start_date => Date.new(2011,4,24), :created_by => 1, :updated_by => 1
       Event.create :name => "Mother's Day", :start_date => Date.new(2011,5,8), :created_by => 1, :updated_by => 1
       Event.create :name => "Memorial Day", :start_date => Date.new(2011,5,30), :created_by => 1, :updated_by => 1
       Event.create :name => "Father's Day", :start_date => Date.new(2011,6,19), :created_by => 1, :updated_by => 1
       Event.create :name => "Independence Day", :start_date => Date.new(2011,7,4), :created_by => 1, :updated_by => 1
       Event.create :name => "Labor Day", :start_date => Date.new(2011,9,5), :created_by => 1, :updated_by => 1
       Event.create :name => "Columbus Day", :start_date => Date.new(2011,10,10), :created_by => 1, :updated_by => 1
       Event.create :name => "Veterans Day", :start_date => Date.new(2011,11,11), :created_by => 1, :updated_by => 1
       Event.create :name => "Thanksgiving Day", :start_date => Date.new(2011,11,24), :created_by => 1, :updated_by => 1
       Event.create :name => "Christmas Day", :start_date => Date.new(2011,12,25), :created_by => 1, :updated_by => 1
   end

   def self.down
   end
end
