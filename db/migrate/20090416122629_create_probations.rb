# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateProbations < ActiveRecord::Migration
   def self.up
      create_table :probations, :force => true do |t|
         t.integer :person_id
         t.integer :status_id
         t.date :start_date
         t.date :end_date
         t.date :completed_date
         t.date :revoked_date
         t.string :revoked_for
         t.integer :docket_id
         t.string :docket_no
         t.integer :court_id
         t.integer :da_charge_id
         t.string :conviction
         t.integer :conviction_count
         t.boolean :doc, :default => false
         t.boolean :parish_jail, :default => false
         t.string :trial_result
         t.integer :default_days, :default => 0
         t.integer :default_months, :default => 0
         t.integer :default_years, :default => 0
         t.date :pay_by_date
         t.integer :sentence_days, :default => 0
         t.integer :sentence_months, :default => 0
         t.integer :sentence_years, :default => 0
         t.boolean :sentence_suspended, :default => false
         t.integer :suspended_except_days, :default => 0
         t.integer :suspended_except_months, :default => 0
         t.integer :suspended_except_years, :default => 0
         t.boolean :reverts_to_unsup, :default => false
         t.string :reverts_conditions
         t.boolean :credit_served, :default => false
         t.integer :service_days, :default => 0
         t.integer :service_served, :default => 0
         t.boolean :sap, :default => false
         t.boolean :sap_complete, :default => false
         t.boolean :driver, :default => false
         t.boolean :driver_complete, :default => false
         t.boolean :anger, :default => false
         t.boolean :anger_complete, :default => false
         t.boolean :sat, :default => false
         t.boolean :sat_complete, :default => false
         t.boolean :random_screens, :default => false
         t.boolean :no_victim_contact, :default => false
         t.boolean :art893, :default => false
         t.boolean :art894, :default => false
         t.boolean :art895, :default => false
         t.text :sentence_notes
         t.boolean :hold_if_arrested, :default => true
         t.integer :probation_days, :default => 0
         t.integer :probation_months, :default => 0
         t.integer :probation_years, :default => 0
         t.decimal :costs, :precision => 12, :scale => 2, :default => 0
         t.decimal :costs_balance, :precision => 12, :scale => 2, :default => 0
         t.decimal :fines, :precision => 12, :scale => 2, :default => 0
         t.decimal :fines_balance, :precision => 12, :scale => 2, :default => 0
         t.text :notes
         t.text :remarks
         t.decimal :probation_balance, :precision => 12, :scale => 2, :default => 0
         t.decimal :probation_monthly, :precision => 12, :scale => 2, :default => 0
         t.decimal :idf, :precision => 12, :scale => 2, :default => 0
         t.decimal :idf_balance, :precision => 12, :scale => 2, :default => 0
         t.decimal :dare, :precision => 12, :scale => 2, :default => 0
         t.decimal :dare_balance, :precision => 12, :scale => 2, :default => 0
         t.decimal :restitution, :precision => 12, :scale => 2, :default => 0
         t.decimal :restitution_balance, :precision => 12, :scale => 2, :default => 0
         t.date :restitution_date
         t.string :probation_officer
         t.string :probation_officer_unit
         t.string :probation_officer_badge
         t.integer :probation_officer_id
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :probations, :person_id
      add_index :probations, :status_id
      add_index :probations, :created_by
      add_index :probations, :updated_by
      add_index :probations, :docket_id
      add_index :probations, :court_id
      add_index :probations, :da_charge_id
      
      create_table :probation_payments, :force => true do |t|
         t.integer :probation_id
         t.date :transaction_date
         t.string :officer
         t.string :officer_unit
         t.string :officer_badge
         t.integer :officer_id
         t.string :receipt_no
         t.string :memo
         t.decimal :costs_payment, :precision => 12, :scale => 2, :default => 0
         t.decimal :costs_charge, :precision => 12, :scale => 2, :default => 0
         t.decimal :costs_balance, :precision => 12, :scale => 2, :default => 0
         t.decimal :fines_payment, :precision => 12, :scale => 2, :default => 0
         t.decimal :fines_charge, :precision => 12, :scale => 2, :default => 0
         t.decimal :fines_balance, :precision => 12, :scale => 2, :default => 0
         t.decimal :probation_payment, :precision => 12, :scale => 2, :default => 0
         t.decimal :probation_charge, :precision => 12, :scale => 2, :default => 0
         t.decimal :probation_balance, :precision => 12, :scale => 2, :default => 0
         t.decimal :idf_payment, :precision => 12, :scale => 2, :default => 0
         t.decimal :idf_charge, :precision => 12, :scale => 2, :default => 0
         t.decimal :idf_balance, :precision => 12, :scale => 2, :default => 0
         t.decimal :dare_payment, :precision => 12, :scale => 2, :default => 0
         t.decimal :dare_charge, :precision => 12, :scale => 2, :default => 0
         t.decimal :dare_balance, :precision => 12, :scale => 2, :default => 0
         t.decimal :restitution_payment, :precision => 12, :scale => 2, :default => 0
         t.decimal :restitution_charge, :precision => 12, :scale => 2, :default => 0
         t.decimal :restitution_balance, :precision => 12, :scale => 2, :default => 0
         t.datetime :report_datetime
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :probation_payments, :probation_id
      add_index :probation_payments, :created_by
      add_index :probation_payments, :updated_by
   end

   def self.down
      drop_table :probations
      drop_table :probation_payments
   end
end
