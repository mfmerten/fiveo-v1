class AddSiteColorOption < ActiveRecord::Migration
  def self.up
     conf = SiteConfiguration.new or raise ActiveRecord::ActiveRecordError.new "Could not get new SiteConfiguration object."
     conf.load_site_file or raise ActiveRecord::ActiveRecordError.new "Could not load site configuration file."
     conf.write_site_file or raise ActiveRecord::ActiveRecordError.new "Could not write site configuration file."
  end

  def self.down
  end
end
