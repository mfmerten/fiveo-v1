# ===================================================================
# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
#
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# =====================================================================
class UpdateSiteConfig < ActiveRecord::Migration
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => 'UpdateSiteConfig::Option'
   end
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => 'UpdateSiteConfig::Option'
   end
   class PaperPayment < ActiveRecord::Base
      belongs_to :payment_type, :class_name => 'UpdateSiteConfig::Option', :foreign_key => 'payment_type_id'
   end

   def self.up
      # update site config file
      conf = SiteConfiguration.new or raise ActiveRecord::ActiveRecordError.new "Could not get new SiteConfiguration object."
      conf.load_site_file or raise ActiveRecord::ActiveRecordError.new "Could not load site configuration file."
      conf.write_site_file or raise ActiveRecord::ActiveRecordError.new "Could not write site configuration file."

      add_column :paper_payments, :payment_method, :string
      PaperPayment.reset_column_information
      
      PaperPayment.find(:all).each do |p|
         unless p.payment_type.nil?
            p.update_attribute(:payment_method, p.payment_type.long_name)
         end
      end
      
      remove_column :paper_payments, :payment_type_id
   end

   def self.down
      add_column :paper_payments, :payment_type_id, :integer
      add_index :paper_payments, :payment_type_id
      
      PaperPayment.reset_column_information
      
      if ot = OptionType.find_by_name("Payment Type")
         PaperPayment.find(:all).each do |p|
            unless p.payment_method.blank?
               if o = Option.find(:option_type_id => ot, :long_name => p.payment_method)
                  p.update_attribute(:payment_type_id, o.id)
               end
            end
         end
      end
      
      remove_column :paper_payments, :payment_method         
      say "Site configuration files not rolled back!  May need to do that manually."
   end
end
