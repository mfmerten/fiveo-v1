# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateColorschemes < ActiveRecord::Migration
   class Colorscheme < ActiveRecord::Base
   end
   
   def self.up
      create_table :colorschemes do |t|
         t.string :name
         t.string :description
         t.string :color_dark
         t.string :color_light
         t.string :color_alt_dark
         t.string :color_alt_light
         t.string :color_black
         t.string :color_white
         t.string :color_gray_dark
         t.string :color_gray_medium
         t.string :color_gray_light
         t.string :color_message
         t.string :color_message_bg
         t.string :color_notice
         t.string :color_notice_bg
         t.string :color_error
         t.string :color_error_bg
         t.timestamps
      end
      add_index :colorschemes, :name

      Colorscheme.create(
         :name => "Default", :description => "Default Colorscheme",
         :color_dark => "#3665A3",
         :color_light => "#D4E3F7",
         :color_alt_dark => "#886655",
         :color_alt_light => "#F0ECDB",
         :color_white => "#FFFFFF",
         :color_black => "#000000",
         :color_gray_dark => "#333333",
         :color_gray_medium => "#777777",
         :color_gray_light => "#CCCCCC",
         :color_notice => "#00AA00",
         :color_notice_bg => "#EEFFEE",
         :color_error => "#AA0000",
         :color_error_bg => "#FFEEEE",
         :color_message => "#0000AA",
         :color_message_bg => "#EEEEFF"
      )
   end

   def self.down
      drop_table :colorschemes
   end
end
