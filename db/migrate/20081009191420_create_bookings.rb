# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateBookings < ActiveRecord::Migration
   def self.up
      create_table :bookings, :force => true do |t|
         t.integer :person_id
         t.decimal :cash_at_booking, :precision => 12, :scale => 2, :default => 0
         t.string :cash_receipt
         t.string :booking_officer
         t.string :booking_officer_unit
         t.string :booking_officer_badge
         t.integer :booking_officer_id
         t.date :booking_date
         t.string :booking_time
         t.integer :facility_id
         t.integer :cell_id
         t.text :remarks
         t.string :phone_called
         t.string :attorney
         t.boolean :intoxicated
         t.date :sched_release_date
         t.date :release_date
         t.string :release_time
         t.boolean :good_time, :default => false
         t.boolean :serve_by_the_hour, :default => false
         t.boolean :disable_timers, :default => false
         t.boolean :afis
         t.string :release_officer
         t.string :release_officer_unit
         t.string :release_officer_badge
         t.integer :release_officer_id
         t.date :prop_return_date
         t.string :prop_return_time
         t.date :prop_destroy_date
         t.string :prop_destroy_time
         t.string :prop_dispo_officer
         t.string :prop_dispo_officer_unit
         t.string :prop_dispo_officer_badge
         t.integer :prop_dispo_officer_id
         t.string :fingerprint_class
         t.string :dna_profile
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :bookings, :person_id
      add_index :bookings, :facility_id
      add_index :bookings, :cell_id
      add_index :bookings, :created_by
      add_index :bookings, :updated_by

      create_table :medicals, :force => true do |t|
         t.integer :booking_id
         t.date :screen_date
         t.string :screen_time
         t.string :arrest_injuries
         t.string :officer
         t.string :officer_unit
         t.string :officer_badge
         t.integer :officer_id
         t.string :disposition
         t.string :rec_med_tmnt
         t.string :rec_dental_tmnt
         t.string :rec_mental_tmnt
         t.string :current_meds
         t.string :allergies
         t.string :injuries
         t.string :infestation
         t.string :alcohol
         t.string :drug
         t.string :withdrawal
         t.string :attempt_suicide
         t.string :suicide_risk
         t.string :danger
         t.string :pregnant
         t.string :deformities
         t.boolean :heart_disease
         t.boolean :blood_pressure
         t.boolean :diabetes
         t.boolean :epilepsy
         t.boolean :hepatitis
         t.boolean :hiv
         t.boolean :tb
         t.boolean :ulcers
         t.boolean :venereal
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :medicals, :booking_id
      add_index :medicals, :created_by
      add_index :medicals, :updated_by

      create_table :properties, :force => true do |t|
         t.integer :booking_id
         t.integer :quantity
         t.string :description
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :properties, :booking_id
      add_index :properties, :created_by
      add_index :properties, :updated_by

      create_table :visitors, :force => true do |t|
         t.integer :booking_id
         t.string :name
         t.string :street
         t.string :city
         t.integer :state_id
         t.string :zip
         t.string :phone
         t.date :visit_date
         t.string :sign_in_time
         t.string :sign_out_time
         t.integer :relationship_id
         t.text :remarks
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :visitors, :booking_id
      add_index :visitors, :state_id
      add_index :visitors, :relationship_id
      add_index :visitors, :created_by
      add_index :visitors, :updated_by

      create_table :receipts, :force => true do |t|
         t.string :assigned
         t.timestamps
      end
   
      create_table :absents, :force => true do |t|
         t.date :leave_date
         t.string :leave_time
         t.integer :leave_type_id
         t.integer :facility_id
         t.date :return_date
         t.string :return_time
         t.string :escort_officer
         t.string :escort_officer_unit
         t.string :escort_officer_badge
         t.integer :escort_officer_id
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :absents, :leave_type_id
      add_index :absents, :facility_id
      add_index :absents, :created_by
      add_index :absents, :updated_by

      create_table :absents_bookings, :force => true, :id => false do |t|
         t.integer :absent_id
         t.integer :booking_id
      end
      add_index :absents_bookings, :absent_id
      add_index :absents_bookings, :booking_id
   
      create_table :transfers, :force => true do |t|
         t.date :transfer_date
         t.string :transfer_time
         t.integer :arrest_id
         t.integer :booking_id
         t.integer :person_id
         t.integer :transfer_type_id
         t.string :from_agency
         t.integer :from_agency_id
         t.string :from_officer
         t.integer :from_officer_id
         t.string :from_officer_unit
         t.string :from_officer_badge
         t.string :to_agency
         t.integer :to_agency_id
         t.string :to_officer
         t.integer :to_officer_id
         t.string :to_officer_unit
         t.string :to_officer_badge
         t.text :remarks
         t.datetime :reported_datetime
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :transfers, :arrest_id
      add_index :transfers, :booking_id
      add_index :transfers, :person_id
      add_index :transfers, :transfer_type_id
      add_index :transfers, :created_by
      add_index :transfers, :updated_by
   
      create_table :docs, :force => true do |t|
         t.integer :booking_id
         t.integer :person_id
         t.date :transfer_date
         t.string :transfer_time
         t.integer :transfer_id
         t.integer :hold_id
         t.integer :sentence_id
         t.integer :revocation_id
         t.string :doc_number
         t.boolean :processed, :default => false
         t.integer :sentence_days
         t.integer :sentence_months
         t.integer :sentence_years
         t.integer :to_serve_days
         t.integer :to_serve_months
         t.integer :to_serve_years
         t.string :conviction
         t.boolean :billable, :default => false
         t.date :bill_from_date
         t.boolean :billed_retroactive, :default => false
         t.date :release_date
         t.string :release_time
         t.boolean :release_by_transfer, :default => false
         t.string :to_agency
         t.text :remarks
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :docs, :person_id
      add_index :docs, :booking_id
      add_index :docs, :transfer_id
      add_index :docs, :hold_id
      add_index :docs, :sentence_id
      add_index :docs, :revocation_id
      add_index :docs, :created_by
      add_index :docs, :updated_by
   end

   def self.down
      drop_table :bookings
      drop_table :medicals
      drop_table :properties
      drop_table :visitors
      drop_table :receipts
      drop_table :absents
      drop_table :absents_bookings
      drop_table :transfers
      drop_table :docs
   end
end
