# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddCsHours < ActiveRecord::Migration
   class Booking < ActiveRecord::Base
   end
   def self.up
      # sentence
      add_column :sentences, :community_service_hours, :integer, :default => 0
      # probation
      add_column :probations, :service_hours, :integer, :default => 0
      
      # fix a problem with extra booking fields on the production servers
      # that no longer exist on the dev server
      cnames = Booking.column_names
      if cnames.include?('other_agency')
         remove_column :bookings, :other_agency
      end
      if cnames.include?('other_agency_id')
         remove_column :bookings, :other_agency_id
      end
   end

   def self.down
      remove_column :sentences, :community_service_hours
      remove_column :probations, :service_hours
   end
end
