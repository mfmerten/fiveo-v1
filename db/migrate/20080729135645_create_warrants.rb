# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateWarrants < ActiveRecord::Migration
   def self.up
      create_table :warrants, :force => true do |t|
         t.integer :person_id
         t.string :firstname
         t.string :middlename
         t.string :lastname
         t.string :suffix
         t.string :ssn
         t.date :dob
         t.integer :race_id
         t.integer :sex
         t.string :street
         t.string :city
         t.integer :state_id
         t.string :zip
         t.string :oln
         t.integer :oln_state_id
         t.string :warrant_no
         t.integer :special_type, :default => 0
         t.integer :jurisdiction_id
         t.date :received_date
         t.date :issued_date
         t.decimal :payable, :precision => 12, :scale => 2, :default => 0
         t.decimal :bond_amt, :precision => 12, :scale => 2, :default => 0
         t.integer :disposition_id
         t.date :dispo_date
         t.text :remarks
         t.integer :investigation_id
         t.integer :invest_crime_id
         t.integer :invest_criminal_id
         t.boolean :private, :default => false
         t.boolean :legacy, :default => false
         t.integer :owned_by
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :warrants, :person_id
      add_index :warrants, :firstname
      add_index :warrants, :middlename
      add_index :warrants, :lastname
      add_index :warrants, :state_id
      add_index :warrants, :zip
      add_index :warrants, :race_id
      add_index :warrants, :jurisdiction_id
      add_index :warrants, :disposition_id
      add_index :warrants, :created_by
      add_index :warrants, :updated_by
      add_index :warrants, :oln_state_id
      add_index :warrants, :investigation_id
      add_index :warrants, :invest_crime_id
      add_index :warrants, :invest_criminal_id
      add_index :warrants, :private
      add_index :warrants, :owned_by

      create_table :warrant_exemptions, :force => true, :id => false do |t|
         t.integer :person_id
         t.integer :warrant_id
      end
      add_index :warrant_exemptions, :person_id
      add_index :warrant_exemptions, :warrant_id
   end

   def self.down
      drop_table :warrants
      drop_table :warrant_exemptions
   end
end
