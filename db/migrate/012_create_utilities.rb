# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateUtilities < ActiveRecord::Migration
   def self.up
      create_table :statutes, :force => true do |t|
         t.string :name
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :statutes, :name
      add_index :statutes, :created_by
      add_index :statutes, :updated_by
   
      create_table :zips do |t|
         t.string :zip_code
         t.string :latitude
         t.string :longitude
         t.string :state
         t.string :city
         t.string :county
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :zips, :zip_code
      add_index :zips, :created_by
      add_index :zips, :updated_by
   
      create_table :case_notes, :force => true do |t|
         t.string :case_no
         t.datetime :note_datetime
         t.text :note
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :case_notes, :case_no
      add_index :case_notes, :created_by
      add_index :case_notes, :updated_by
   
      create_table :events, :force => true do |t|
         t.date :start_date
         t.date :end_date
         t.string :name
         t.text :details
         t.boolean :private, :default => false
         t.integer :investigation_id
         t.integer :owned_by
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :events, :start_date
      add_index :events, :end_date
      add_index :events, :investigation_id
      add_index :events, :private
      add_index :events, :created_by
      add_index :events, :updated_by
      add_index :events, :owned_by
   
      create_table :activities, :force => true do |t|
         t.integer :user_id
         t.string :section
         t.string :description
         t.timestamps
      end
      add_index :activities, :user_id
      add_index :activities, :section
   
      create_table :fax_covers, :force => true do |t|
         t.string :name
         t.string :from_name
         t.string :from_fax
         t.string :from_phone
         t.boolean :include_notice, :default => true
         t.string :notice_title
         t.text :notice_body
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :fax_covers, :created_by
      add_index :fax_covers, :updated_by
   
      create_table :histories, :force => true do |t|
         t.string :key
         t.string :name
         t.integer :type_id
         t.text :details
         t.timestamps
      end
      add_index :histories, :key
      add_index :histories, :name
      add_index :histories, :type_id
   end

   def self.down
      drop_table :statutes
      drop_table :zips
      drop_table :case_notes
      drop_table :events
      drop_table :activities
      drop_table :fax_covers
      drop_table :histories
   end
end
