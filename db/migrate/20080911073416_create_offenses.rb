# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateOffenses < ActiveRecord::Migration
   def self.up
      create_table :offenses, :force => true do |t|
         t.integer :call_id
         t.string :case_no
         t.date :offense_date
         t.string :offense_time
         t.string :location
         t.text :details
         t.string :wrecker_used
         t.boolean :wrecker_by_request
         t.string :vehicle_location
         t.string :vehicle_vin
         t.string :vehicle_lic
         t.boolean :pictures
         t.boolean :restitution_form
         t.boolean :victim_notification
         t.boolean :perm_to_search
         t.boolean :misd_summons
         t.boolean :fortyeight_hour
         t.boolean :medical_release
         t.text :other_documents
         t.string :officer
         t.string :officer_unit
         t.string :officer_badge
         t.integer :officer_id
         t.text :remarks
         t.boolean :legacy, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :offenses, :call_id
      add_index :offenses, :case_no
      add_index :offenses, :created_by
      add_index :offenses, :updated_by

      create_table :victims, :id => false, :force => true do |t|
         t.integer :offense_id
         t.integer :person_id
      end
      add_index :victims, :offense_id
      add_index :victims, :person_id

      create_table :witnesses, :id => false, :force => true do |t|
         t.integer :offense_id
         t.integer :person_id
      end
      add_index :witnesses, :offense_id
      add_index :witnesses, :person_id

      create_table :suspects, :id => false, :force => true do |t|
         t.integer :offense_id
         t.integer :person_id
      end
      add_index :suspects, :offense_id
      add_index :suspects, :person_id

      create_table :offense_photos, :force => true do |t|
         t.integer :offense_id
         t.string :case_no
         t.string :crime_scene_photo_file_name
         t.string :crime_scene_photo_content_type
         t.integer :crime_scene_photo_file_size
         t.datetime :crime_scene_photo_updated_at
         t.string :description
         t.date :date_taken
         t.string :location_taken
         t.string :taken_by
         t.string :taken_by_unit
         t.string :taken_by_badge
         t.integer :taken_by_id
         t.text :remarks
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :offense_photos, :offense_id
      add_index :offense_photos, :case_no
      add_index :offense_photos, :created_by
      add_index :offense_photos, :updated_by
   end

   def self.down
      drop_table :offense_photos
      drop_table :offenses
      drop_table :victims
      drop_table :witnesses
      drop_table :suspects
   end
end
