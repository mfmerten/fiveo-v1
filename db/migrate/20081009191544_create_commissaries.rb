# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateCommissaries < ActiveRecord::Migration
   def self.up
      create_table :commissaries, :force => true do |t|
         t.integer :person_id
         t.date :transaction_date
         t.integer :category_id
         t.string :officer
         t.string :officer_unit
         t.string :officer_badge
         t.integer :officer_id
         t.string :receipt_no
         t.decimal :deposit, :precision => 12, :scale => 2, :default => 0
         t.decimal :withdraw, :precision => 12, :scale => 2, :default => 0
         t.decimal :balance, :precision => 12, :scale => 2, :default => 0
         t.datetime :report_datetime
         t.string :memo
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :commissaries, :person_id
      add_index :commissaries, :category_id
      add_index :commissaries, :created_by
      add_index :commissaries, :updated_by

      create_table :commissary_items, :force => true do |t|
         t.string :description
         t.decimal :price, :precision => 12, :scale => 2, :default => 0
         t.boolean :active, :default => true
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :commissary_items, :created_by
      add_index :commissary_items, :updated_by
      
      create_table :commissary_facilities, :force => true, :id => false do |t|
         t.integer :commissary_item_id
         t.integer :option_id
      end
      add_index :commissary_facilities, :commissary_item_id
      add_index :commissary_facilities, :option_id
   end

   def self.down
      drop_table :commissaries
      drop_table :commissary_items
      drop_table :commissary_facilities
   end
end
