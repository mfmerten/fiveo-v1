# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
require 'digest/sha1'
class CreateAdminUser < ActiveRecord::Migration
   class User < ActiveRecord::Base
      has_and_belongs_to_many :groups, :class_name => 'CreateAdminUser::Group'

      attr_protected :id, :salt, :expires

      attr_accessor :password, :password_confirmation

      def password=(pass)
         @password=pass
         self.salt = User.random_string(10)
         self.hashed_password = User.encrypt(@password, self.salt)
         if @password == "pa$$word"
            self.expires = Date.today
         else
            self.expires = Date.today + 180.days
         end
      end

      protected

      def self.encrypt(pass, salt)
         Digest::SHA1.hexdigest(pass+salt)
      end

      def self.random_string(len)
         #generat a random password consisting of strings and digits
         chars = ("a".."z").to_a + ("A".."Z").to_a + ("0".."9").to_a
         newpass = ""
         1.upto(len) { |i| newpass << chars[rand(chars.size-1)] }
         return newpass
      end
   end

   class Group < ActiveRecord::Base
      has_and_belongs_to_many :users, :class_name => 'CreateAdminUser::User'
   end

   def self.up
      user =  User.create :login => "admin", :password => "admin", :password_confirmation => "admin"

      # assign admin group to admin user
      group = Group.find(:first, :conditions => {:name => "Admin"})
      user.groups << group
   end

   def self.down
      User.find(:all).each do |u|
         u.groups.clear
         u.destroy
      end
   end
end
