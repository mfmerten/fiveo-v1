# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class UpdateDockets < ActiveRecord::Migration
  def self.up
     add_column :dockets, :misc, :boolean, :default => false
     add_column :da_charges, :info_only, :boolean, :default => false
     change_column_default :dockets, :processed, false
  end

  def self.down
     remove_column :dockets, :misc
     remove_column :da_charges, :info_only
     change_column_default :dockets, :processed, true
  end
end
