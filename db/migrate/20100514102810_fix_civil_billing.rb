# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class FixCivilBilling < ActiveRecord::Migration
   class CivilInvoice < ActiveRecord::Base
      has_and_belongs_to_many :items, :class_name => 'FixCivilBilling::Paper', :join_table => 'civil_invoice_items'
   end
   class Paper < ActiveRecord::Base
      has_and_belongs_to_many :invoices, :class_name => 'FixCivilBilling::CivilInvoice', :join_table => 'civil_invoice_items'
   end
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => 'FixCivilBilling::Option', :dependent => :destroy
   end
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => 'FixCivilBilling::OptionType'
   end
   def self.up
      create_table :civil_payments, :force => true do |t|
         t.integer :civil_customer_id
         t.integer :civil_invoice_id
         t.integer :payment_type_id
         t.date :transaction_date
         t.string :officer
         t.string :officer_unit
         t.string :officer_badge
         t.integer :officer_id
         t.string :receipt_no
         t.decimal :payment, :precision => 12, :scale => 2, :default => 0
         t.decimal :charge, :precision => 12, :scale => 2, :default => 0
         t.decimal :balance, :precision => 12, :scale => 2, :default => 0
         t.datetime :report_datetime
         t.string :memo
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :civil_payments, :civil_customer_id
      add_index :civil_payments, :civil_invoice_id
      add_index :civil_payments, :payment_type_id
      add_index :civil_payments, :created_by
      add_index :civil_payments, :updated_by
      
      add_column :civil_customers, :balance_due, :decimal, :precision => 12, :scale => 2, :default => 0
      
      add_column :civil_invoices, :balance_due, :decimal, :precision => 12, :scale => 2, :default => 0
      remove_column :civil_invoices, :payment_method_id
      remove_column :civil_invoices, :memo
      
      # because of changes made here, must clear invoices and update papers
      CivilInvoice.find(:all).each do |i|
         i.items.each do |p|
            p.update_attribute(:invoice_datetime, nil)
            p.update_attribute(:paid_date, nil)
         end
         i.items.clear
         i.destroy
      end
      
      # removing suit type and options
      remove_column :suits, :suit_type_id
      if ot = OptionType.find_by_name("Suit Type")
         ot.destroy
      end
   end

   def self.down
      drop_table :civil_payments
      remove_column :civil_customers, :balance_due
      remove_column :civil_invoices, :balance_due
      add_column :civil_invoices, :payment_method_id, :integer
      add_column :civil_invoices, :memo, :string
      add_column :suits, :suit_type_id, :integer
      
      unless OptionType.find_by_name("Suit Type")
         OptionType.create(:name => "Suit Type")
      end
   end
end
