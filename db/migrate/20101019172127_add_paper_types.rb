# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class AddPaperTypes < ActiveRecord::Migration
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => 'AddPaperTypes::Option', :dependent => :destroy
   end
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => 'AddPaperTypes::OptionType'
   end
   class PaperType < ActiveRecord::Base
      has_many :papers, :class_name => 'AddPaperTypes::Paper'
   end
   class Paper < ActiveRecord::Base
      belongs_to :paper_type, :class_name => 'AddPaperTypes::PaperType', :foreign_key => 'type_id'
      belongs_to :old_paper_type, :class_name => 'AddPaperTypes::Option', :foreign_key => 'paper_type_id'
   end

   def self.up
      # create paper types table
      create_table :paper_types, :force => true do |t|
         t.string :name
         t.decimal :cost, :precision => 12, :scale => 2, :default => 0
         t.boolean :disabled, :default => false
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :paper_types, :created_by
      add_index :paper_types, :updated_by

      # add column to papers
      add_column :papers, :type_id, :integer

      # create a paper type for each paper type option
      if ot = OptionType.find_by_name("Paper Type")
         ot.options.each do |o|
            pt = PaperType.create(:created_by => 1, :updated_by => 1, :name => o.long_name, :cost => BigDecimal.new(o.short_name,2), :disabled => o.disabled)
            Paper.update_all("type_id = #{pt.id}", "paper_type_id = #{o.id}")
         end
      end
      
      # delete paper type option
      ot.destroy
      
      # remove old paper_type_id column from papers
      remove_column :papers, :paper_type_id
      
      # rename type_id to paper_type_id
      rename_column :papers, :type_id, :paper_type_id
      add_index :papers, :paper_type_id
   end

   def self.down
      remove_index :papers, :column => :paper_type_id
      rename_column :papers, :paper_type_id, :type_id
      add_column :papers, :paper_type_id, :integer
      add_index :papers, :paper_type_id
      
      unless ot = OptionType.find_by_name("Paper Type")
         ot = OptionType.create(:name => 'Paper Type')
      end
      
      PaperType.find(:all).each do |pt|
         o = ot.options.create(:long_name => pt.name, :short_name => pt.cost.to_s, :disabled => pt.disabled, :created_by => pt.created_by, :updated_by => pt.updated_by)
         Paper.update_all("paper_type_id = #{o.id}","type_id = #{pt.id}")
      end
      
      remove_column :papers, :type_id
      drop_table :paper_types
   end
end
