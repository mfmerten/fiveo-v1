# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CreateBonds < ActiveRecord::Migration
   def self.up
      create_table :bonds, :force => true do |t|
         t.integer :bondsman_id
         t.integer :surety_id
         t.integer :person_id
         t.integer :arrest_id
         t.integer :hold_id
         t.decimal :bond_amt, :precision => 12, :scale => 2, :default => 0
         t.date :issued_date
         t.string :issued_time
         t.integer :bond_type_id
         t.text :remarks
         t.string :power
         t.integer :status_id
         t.date :status_date
         t.string :status_time
         t.string :bond_no
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :bonds, :bondsman_id
      add_index :bonds, :surety_id
      add_index :bonds, :person_id
      add_index :bonds, :arrest_id
      add_index :bonds, :hold_id
      add_index :bonds, :bond_type_id
      add_index :bonds, :status_id
      add_index :bonds, :created_by
      add_index :bonds, :updated_by

      create_table :bondsmen, :force => true do |t|
         t.string :name
         t.string :street
         t.string :city
         t.integer :state_id
         t.string :zip
         t.string :company
         t.boolean :disabled
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :bondsmen, :state_id
      add_index :bondsmen, :created_by
      add_index :bondsmen, :updated_by

      create_table :bondsman_phones, :force => true do |t|
         t.integer :bondsman_id
         t.integer :label_id
         t.string :value
         t.string :note
         t.integer :created_by, :default => 1
         t.integer :updated_by, :default => 1
         t.timestamps
      end
      add_index :bondsman_phones, :bondsman_id
      add_index :bondsman_phones, :label_id
      add_index :bondsman_phones, :created_by
      add_index :bondsman_phones, :updated_by
   end

   def self.down
      drop_table :bonds
      drop_table :bondsmen
      drop_table :bondsman_phones
   end
end
