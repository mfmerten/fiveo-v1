# ===================================================================
# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
#
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# =====================================================================
class ModifyProbationFunds < ActiveRecord::Migration
  class Probation < ActiveRecord::Base
     has_many :payments, :class_name => "ModifyProbationFunds::ProbationPayment"
     has_one :sentence, :class_name => "ModifyProbationFunds::Sentence"
  end
  class ProbationPayment < ActiveRecord::Base
     belongs_to :probation, :class_name => "ModifyProbationFunds::Probation"
  end
  class Sentence < ActiveRecord::Base
     belongs_to :probation, :class_name => "ModifyProbationFunds::Probation"
  end
  def self.up
     # clear probation payments table
     ProbationPayment.delete_all
     
     # update probation payments columns
     remove_column :probation_payments, :costs_payment
     remove_column :probation_payments, :costs_charge
     remove_column :probation_payments, :costs_balance
     remove_column :probation_payments, :probation_payment
     remove_column :probation_payments, :probation_charge
     remove_column :probation_payments, :probation_balance
     remove_column :probation_payments, :idf_payment
     remove_column :probation_payments, :idf_charge
     remove_column :probation_payments, :idf_balance
     remove_column :probation_payments, :dare_payment
     remove_column :probation_payments, :dare_charge
     remove_column :probation_payments, :dare_balance
     remove_column :probation_payments, :restitution_payment
     remove_column :probation_payments, :restitution_charge
     remove_column :probation_payments, :restitution_balance
     remove_column :probation_payments, :fines_payment
     remove_column :probation_payments, :fines_charge
     remove_column :probation_payments, :fines_balance
     add_column :probation_payments, :fund1_name, :string
     add_column :probation_payments, :fund1_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund1_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund2_name, :string
     add_column :probation_payments, :fund2_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund2_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund3_name, :string
     add_column :probation_payments, :fund3_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund3_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund4_name, :string
     add_column :probation_payments, :fund4_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund4_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund5_name, :string
     add_column :probation_payments, :fund5_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund5_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund6_name, :string
     add_column :probation_payments, :fund6_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund6_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund7_name, :string
     add_column :probation_payments, :fund7_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund7_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund8_name, :string
     add_column :probation_payments, :fund8_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund8_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund9_name, :string
     add_column :probation_payments, :fund9_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fund9_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     
     # check and restore original sentence values to probation
     Probation.find(:all).each do |p|
        unless p.sentence.nil?
           if p.fines != p.sentence.fines
              p.update_attribute(:fines, p.sentence.fines)
           end
           if p.costs != p.sentence.costs
              p.update_attribute(:costs, p.sentence.costs)
           end
           if p.probation_monthly != p.sentence.probation_fees
              p.update_attribute(:probation_monthly, p.sentence.probation_fees)
           end
           if p.idf != p.sentence.idf_amount
              p.update_attribute(:idf, p.sentence.idf_amount)
           end
           if p.dare != p.sentence.dare_amount
              p.update_attribute(:dare, p.sentence.dare_amount)
           end
           if p.restitution != p.sentence.restitution_amount
              p.update_attribute(:restitution, p.sentence.restitution_amount)
           end
        end
     end
     
     # update probation fields
     add_column :probations, :fund1_name, :string
     add_column :probations, :fund2_name, :string
     add_column :probations, :fund3_name, :string
     add_column :probations, :fund4_name, :string
     add_column :probations, :fund5_name, :string
     add_column :probations, :fund6_name, :string
     add_column :probations, :fund7_name, :string
     add_column :probations, :fund8_name, :string
     add_column :probations, :fund9_name, :string
     add_column :probations, :fund1_charged, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund2_charged, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund3_charged, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund4_charged, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund5_charged, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund6_charged, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund7_charged, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund8_charged, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund9_charged, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund1_paid, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund2_paid, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund3_paid, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund4_paid, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund5_paid, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund6_paid, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund7_paid, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund8_paid, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :fund9_paid, :decimal, :precision => 12, :scale => 2, :default => 0.0
     remove_column :probations, :fines_balance
     remove_column :probations, :costs_balance
     remove_column :probations, :probation_balance
     remove_column :probations, :idf_balance
     remove_column :probations, :dare_balance
     remove_column :probations, :restitution_balance
     # column renames to make them identical to sentence
     rename_column :probations, :idf, :idf_amount
     rename_column :probations, :dare, :dare_amount
     rename_column :probations, :restitution, :restitution_amount
     rename_column :probations, :probation_monthly, :probation_fees
     
     # reset columns
     Probation.reset_column_information
     
     # update fund names
     Probation.find(:all).each do |p|
        p.fund1_name = "Fines"
        p.fund2_name = "Costs"
        p.fund3_name = "Probation Fees"
        p.fund4_name = "IDF"
        p.fund5_name = "DARE"
        p.fund6_name = "Restitution"
        p.fund7_name = "DA Fees"
        p.save
     end
     
     say "WARNING"
     say "WARNING"
     say "WARNING: Don't forget to log in manually and update site configuration!"
     say "WARNING"
     say "WARNING"
  end

  def self.down
     remove_column :probations, :fund1_name
     remove_column :probations, :fund2_name
     remove_column :probations, :fund3_name
     remove_column :probations, :fund4_name
     remove_column :probations, :fund5_name
     remove_column :probations, :fund6_name
     remove_column :probations, :fund7_name
     remove_column :probations, :fund8_name
     remove_column :probations, :fund9_name
     remove_column :probations, :fund1_charged
     remove_column :probations, :fund2_charged
     remove_column :probations, :fund3_charged
     remove_column :probations, :fund4_charged
     remove_column :probations, :fund5_charged
     remove_column :probations, :fund6_charged
     remove_column :probations, :fund7_charged
     remove_column :probations, :fund8_charged
     remove_column :probations, :fund9_charged
     remove_column :probations, :fund1_paid
     remove_column :probations, :fund2_paid
     remove_column :probations, :fund3_paid
     remove_column :probations, :fund4_paid
     remove_column :probations, :fund5_paid
     remove_column :probations, :fund6_paid
     remove_column :probations, :fund7_paid
     remove_column :probations, :fund8_paid
     remove_column :probations, :fund9_paid
     add_column :probations, :fines_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :costs_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :probation_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :idf_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :dare_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :restitution_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probations, :idf_amount, :idf
     add_column :probations, :dare_amount, :dare
     add_column :probations, :restitution_amount, :restitution
     add_column :probations, :probation_fees, :probation_monthly
     add_column :probation_payments, :costs_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :costs_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :costs_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :probation_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :probation_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :probation_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :idf_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :idf_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :idf_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :dare_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :dare_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :dare_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :restitution_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :restitution_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :restitution_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fines_payment, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fines_charge, :decimal, :precision => 12, :scale => 2, :default => 0.0
     add_column :probation_payments, :fines_balance, :decimal, :precision => 12, :scale => 2, :default => 0.0
     remove_column :probation_payments, :fund1_name
     remove_column :probation_payments, :fund1_charge
     remove_column :probation_payments, :fund1_payment
     remove_column :probation_payments, :fund2_name
     remove_column :probation_payments, :fund2_charge
     remove_column :probation_payments, :fund2_payment
     remove_column :probation_payments, :fund3_name
     remove_column :probation_payments, :fund3_charge
     remove_column :probation_payments, :fund3_payment
     remove_column :probation_payments, :fund4_name
     remove_column :probation_payments, :fund4_charge
     remove_column :probation_payments, :fund4_payment
     remove_column :probation_payments, :fund5_name
     remove_column :probation_payments, :fund5_charge
     remove_column :probation_payments, :fund5_payment
     remove_column :probation_payments, :fund6_name
     remove_column :probation_payments, :fund6_charge
     remove_column :probation_payments, :fund6_payment
     remove_column :probation_payments, :fund7_name
     remove_column :probation_payments, :fund7_charge
     remove_column :probation_payments, :fund7_payment
     remove_column :probation_payments, :fund8_name
     remove_column :probation_payments, :fund8_charge
     remove_column :probation_payments, :fund8_payment
     remove_column :probation_payments, :fund9_name
     remove_column :probation_payments, :fund9_charge
     remove_column :probation_payments, :fund9_payment
  end
end
