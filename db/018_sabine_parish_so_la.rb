# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class SabineParishSoLa < ActiveRecord::Migration
    class Option < ActiveRecord::Base
        belongs_to :option_type, :class_name => 'SabineParishSoLa::OptionType'
    end
    class OptionType < ActiveRecord::Base
        has_many :options, :class_name => 'SabineParishSoLa::Option'
        validates_presence_of :name
        validates_uniqueness_of :name
    end

    def self.up
        # Note: we create option types if necessary, but otherwise do not
        # touch existing data. If migration errors occur, there may be
        # pre-existing options by the same name (must be unique).
        
        # Judge
        unless ot = OptionType.find_by_name("Judge")
           ot = OptionType.create :name => "Judge"
        end
        ot.options.build(:long_name => 'Adams', :short_name => 'B')
        ot.options.build(:long_name => 'Burgess', :short_name => 'A')
        ot.options.build(:long_name => 'Beasley', :short_name => 'C')
        ot.options.each{|o| o.save}
        
        # pawn companies for spso
        unless @option_type = OptionType.find(:first, :conditions => {:name => "Pawn Company"})
            @option_type = OptionType.create :name => "Pawn Company"
        end
        Option.create :long_name => "Many Pawn Shop", :option_type_id => @option_type.id
        Option.create :long_name => "Out of Parish", :option_type_id => @option_type.id
        Option.create :long_name => "Pat & Jerry Pawn Shop", :option_type_id => @option_type.id
        Option.create :long_name => "Patrick Enterprise", :option_type_id => @option_type.id
        Option.create :long_name => "Pendleton Gun and Pawn", :option_type_id => @option_type.id
        Option.create :long_name => "Po-Boys Pawn Shop", :option_type_id => @option_type.id
        Option.create :long_name => "Toledo Pawn Shop", :option_type_id => @option_type.id
        Option.create :long_name => "Zwolle Pawn Shop", :option_type_id => @option_type.id

        # Cell options
        unless @option_type = OptionType.find(:first, :conditions => {:name => "Cell"})
            @option_type = OptionType.create :name => "Cell"
        end
        Option.create :long_name => "A Dorm", :option_type_id => @option_type.id
        Option.create :long_name => "B Dorm", :option_type_id => @option_type.id
        Option.create :long_name => "C Dorm", :option_type_id => @option_type.id
        Option.create :long_name => "D Dorm", :option_type_id => @option_type.id
        Option.create :long_name => "E Dorm", :option_type_id => @option_type.id
        Option.create :long_name => "Holding Cell", :option_type_id => @option_type.id
        Option.create :long_name => "Bullpen 1", :option_type_id => @option_type.id
        Option.create :long_name => "Bullpen 2", :option_type_id => @option_type.id
        Option.create :long_name => "Cell 1", :option_type_id => @option_type.id
        Option.create :long_name => "Cell 2", :option_type_id => @option_type.id
        Option.create :long_name => "Cell 3", :option_type_id => @option_type.id
        Option.create :long_name => "Cell 4", :option_type_id => @option_type.id
        Option.create :long_name => "Trustee 1", :option_type_id => @option_type.id
        Option.create :long_name => "Trustee 2", :option_type_id => @option_type.id
        Option.create :long_name => "Womens Area", :option_type_id => @option_type.id

        # Court Cost options
        unless ot = OptionType.find_by_name("Court Costs")
           ot = OptionType.create(:name => 'Court Costs')
        end
        ot.options.create(:long_name => 'DWI 1st W/O Analysis', :short_name => '266.00', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'DWI 2nd W/O Analysis', :short_name => '291.00', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'DWI 3rd W/O Analysis', :short_name => '383.50', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'DWI 4th W/O Analysis', :short_name => '533.50', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'DWI 1st W/ Analysis', :short_name => '341.00', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'DWI 2nd W/ Analysis', :short_name => '366.00', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'DWI 3rd W/ Analysis', :short_name => '458.50', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'DWI 4th W/ Analysis', :short_name => '608.50', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'Traffic, Title 14, Misd.', :short_name => '156.00', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'Traffic, Title 14, Felony', :short_name => '193.50', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'Traffic, Speed & Reck. Op.', :short_name => '133.50', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'Traffic, DL viol & Exp MVI', :short_name => '128.50', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'Traffic, All Other', :short_name => '128.50', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'CDS, Felony', :short_name => '233.50', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'CDS, Misd.', :short_name => '191.00', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'All Other Felony', :short_name => '193.50', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'All Other Misd.', :short_name => '151.00', :created_by => 1, :updated_by => 1)
        ot.options.create(:long_name => 'Wildlife', :short_name => '141.00', :created_by => 1, :updated_by => 1)

        # Facility options
        unless @option_type = OptionType.find(:first, :conditions => {:name => "Facility"})
            @option_type = OptionType.create :name => "Facility"
        end
        Option.create :long_name => "Sabine Parish Detention Center", :short_name => "SPDC", :option_type_id => @option_type.id
        Option.create :long_name => "Sabine Parish Womens Facility", :short_name => "SPWF", :option_type_id => @option_type.id

        # Judge options
        unless @option_type = OptionType.find(:first, :conditions => {:name => "Judge"})
            @option_type = OptionType.create :name => "Judge"
        end
        Option.create :long_name => "Adams", :short_name => "B", :option_type_id => @option_type.id
        Option.create :long_name => "Burgess", :short_name => "A", :option_type_id => @option_type.id
        Option.create :long_name => "Beasley", :short_name => "C", :option_type_id => @option_type.id
    end

    def self.down
    end
end
