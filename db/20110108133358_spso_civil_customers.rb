# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class SpsoCivilCustomers < ActiveRecord::Migration
   require 'csv'
   class OptionType < ActiveRecord::Base
      has_many :options, :class_name => 'SpsoCivilCustomers::Option', :dependent => :destroy
   end
   class Option < ActiveRecord::Base
      belongs_to :option_type, :class_name => 'SpsoCivilCustomers::OptionType'

      # customized for this migration to look up states by short name
      def self.lookup(value)
         return nil if value.nil? || value.blank?
         option = nil
         if option_type = OptionType.find_by_name("State")
            if value.size != 2
               option = find(:first, :conditions => ["long_name = ? and option_type_id = ?", value.titleize, option_type.id])
            else
               option = find(:first, :conditions => ["short_name = ? and option_type_id = ?", value.upcase, option_type.id])
            end
         end
         return nil if option.nil?
         return option.id
      end
   end
   class PaperCustomer < ActiveRecord::Base
      belongs_to :state, :class_name => 'SpsoCivilCustomers::Option', :foreign_key => 'state_id'
   end
   class PaperType < ActiveRecord::Base
   end
   
   def self.up
      PaperCustomer.reset_column_information
      
      # only want to perform this action if the CSV file exists
      fname = "#{RAILS_ROOT}/doc/other/SPSO-utils/paper_customer.csv"
      if File.exist?(fname)   
         # first, clear out all existing entries and reset id number sequence
         # clear out the existing test papers and payments as well
         sql = PaperCustomer.connection
         sql.execute "truncate table paper_customers"
         sql.execute "truncate table papers"
         sql.execute "truncate table paper_payments"
      
         # process the customer CSV file
         CSV.open(fname,"r") do |row|
            # create customers
            p = PaperCustomer.new(
               :name => row[1].titleize,
               :street => (row[2].nil? ? nil : row[2].titleize),
               :street2 => (row[3].nil? ? nil : row[3].titleize),
               :city => (row[4].nil? ? nil : row[4].titleize),
               :state_id => Option.lookup(row[5]),
               :zip => row[6],
               :phone => row[7],
               :fax => row[8],
               :attention => (row[9].nil? ? nil : row[9].titleize),
               :created_by => 1,
               :updated_by => 1,
               :noservice_fee => SiteConfig.noservice_fee,
               :mileage_fee => SiteConfig.mileage_fee
            )
            p.id = row[0].to_i
            p.save
         end
         say "Processed #{PaperCustomer.count} Records!"
      else
         say "Migration skipped, no CSV file exists!"
      end
      
      # Add paper types
      # only want to perform this action if the CSV file exists
      fname = "#{RAILS_ROOT}/doc/other/SPSO-utils/paper_types.csv"
      # only want to perform this action if the CSV file exists
      if File.exist?(fname)
         # clear existing entries and reset table index
         sql = PaperType.connection
         sql.execute "truncate table paper_types"
         
         # process the paper type CSV file
         CSV.open(fname,'r') do |row|
            p = PaperType.new(:created_by => 1, :updated_by => 1)
            unless row[0].nil?
               p.name = row[0].titleize
            end
            unless row[1].nil?
               p.cost = BigDecimal.new(row[1].sub(/\$/,''),2)
            end
            p.save
         end
         say "Processed #{PaperType.count} Paper Type records!"
      else
         say "Paper Types Skipped: no CSV file!"
      end
      
   end

   def self.down
   end
end

