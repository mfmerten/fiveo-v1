# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
module Permissions
   class PermList
      # WARNING!  Permission numbers should NEVER be reused. To remove a permission,
      # comment out the number and clearly mark it as obsolete.
      def initialize
         @forward = {
            1 => :admin,
            # 2 => :destroy_all -- Obsolete, do not use
            3 => :author_all,
            4 => :view_database,
            5 => :update_database,
            6 => :view_user,
            7 => :reset_user,
            8 => :update_user,
            9 => :destroy_user,
            10 => :view_group,
            11 => :update_group,
            12 => :destroy_group,
            13 => :view_option,
            14 => :update_option,
            15 => :edit_option,
            16 => :destroy_option,
            17 => :view_person,
            18 => :update_person,
            19 => :destroy_person,
            20 => :merge_person,
            21 => :set_person_id,
            22 => :view_pawn,
            23 => :update_pawn,
            24 => :destroy_pawn,
            25 => :view_contact,
            26 => :update_contact,
            27 => :destroy_contact,
            28 => :view_call,
            29 => :update_call,
            30 => :destroy_call,
            31 => :view_warrant,
            32 => :update_warrant,
            33 => :destroy_warrant,
            34 => :view_case,
            35 => :update_case,
            36 => :destroy_case,
            37 => :view_announcement,
            38 => :update_announcement,
            39 => :destroy_announcement,
            40 => :view_event,
            41 => :update_event,
            42 => :destroy_event,
            43 => :view_offense,
            44 => :update_offense,
            45 => :destroy_offense,
            46 => :view_evidence,
            47 => :update_evidence,
            48 => :destroy_evidence,
            49 => :view_forbid,
            50 => :update_forbid,
            51 => :destroy_forbid,
            52 => :view_booking,
            53 => :update_booking,
            54 => :destroy_booking,
            55 => :reopen_booking,
            56 => :view_commissary,
            57 => :update_commissary,
            58 => :destroy_commissary,
            59 => :view_arrest,
            60 => :update_arrest,
            61 => :destroy_arrest,
            62 => :view_bond,
            63 => :update_bond,
            64 => :destroy_bond,
            65 => :view_transport,
            66 => :update_transport,
            67 => :destroy_transport,
            68 => :view_bug,
            69 => :update_bug,
            70 => :destroy_bug,
            71 => :view_probation,
            72 => :update_probation,
            73 => :destroy_probation,
            74 => :view_statute,
            75 => :update_statute,
            76 => :destroy_statute,
            77 => :view_billing,
            78 => :update_billing,
            79 => :view_civil,
            80 => :update_civil,
            81 => :destroy_civil,
            82 => :view_stolen,
            83 => :update_stolen,
            84 => :destroy_stolen,
            85 => :view_court,
            86 => :update_court,
            87 => :destroy_court,
            88 => :view_citation,
            89 => :update_citation,
            90 => :destroy_citation,
            91 => :view_history,
            92 => :destroy_history,
            93 => :view_investigation,
            94 => :update_investigation,
            95 => :destroy_investigation,
            96 => :view_jail1,
            97 => :update_jail1,
            98 => :destroy_jail1,
            99 => :reopen_jail1,
            100 => :view_jail2,
            101 => :update_jail2,
            102 => :destroy_jail2,
            103 => :reopen_jail2,
            104 => :view_jail3,
            105 => :update_jail3,
            106 => :destroy_jail3,
            107 => :reopen_jail3
            # next unused number is 108
         }
         @reverse = @forward.invert
         # for rename of legacy permission
         @reverse[:author_override] = @reverse[:author_all]
      end
      
      def size
         @forward.size
      end
      
      def to_h
         @reverse.merge(@forward)
      end
   end
end








































