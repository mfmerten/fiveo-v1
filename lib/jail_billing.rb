# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
module JailBilling
   module MonthlyReport
      class TableData
         def initialize(facility_id, start_date, stop_date=nil)
            @start_date = start_date
            if stop_date.nil?
               @stop_date = start_date
            else
               @stop_date = stop_date
            end
            # order of btypes is fixed by return value of Booking.headcount method
            @btypes = ['Temporary Release','DOC','DOC Billable','City','Parish','Other','Not Billable']
            @items = {}
            @start_date.upto(@stop_date) do |d|
               raw_data = Booking.headcount(d,'ALL',facility_id)
               @btypes.size.times do |x|
                  if @items[@btypes[x]].nil?
                     @items[@btypes[x]] = []
                  end
                  raw_data[x].each do |b|
                     @items[@btypes[x]].push(JailBilling::MonthlyReport::Item.new(d,@btypes[x],b))
                  end
               end
            end
         end
         
         def count(status=nil,date=nil)
            if date.nil?
               if status.nil?
                  @items.values.flatten.compact.size
               elsif @items[status].nil?
                  0
               else               
                  @items[status].size
               end
            else
               if status.nil?
                  @items.values.collect{|v| v.reject{|i| i.date != date}}.flatten.compact.size
               elsif @items[status].nil?
                  0
               else
                  @items[status].reject{|i| i.date != date}.size
               end
            end
         end
         
         def billable(status=nil,date=nil)
            if date.nil?
               if status.nil?
                  @items.values.collect{|v| v.reject{|i| !i.billable?}}.flatten.compact.size
               elsif @items[status].nil?
                  0
               else
                  @items[status].reject{|i| !i.billable?}.size
               end
            else
               if status.nil?
                  @items.values.collect{|v| v.reject{|i| i.date != date || !i.billable?}}.flatten.compact.size
               elsif @items[status].nil?
                  return 0
               else
                  @items[status].reject{|i| i.date != date || !i.billable?}.compact.size
               end
            end
         end
         
         def physical(status=nil,date=nil)
            if date.nil?
               if status.nil?
                  @items.values.collect{|v| v.reject{|i| !i.physical?}}.flatten.compact.size
               elsif @items[status].nil?
                  0
               else
                  @items[status].reject{|i| !i.physical?}.size
               end
            else
               if status.nil?
                  @items.values.collect{|v| v.reject{|i| i.date != date || !i.physical?}}.flatten.compact.size
               elsif @items[status].nil?
                  return 0
               else
                  @items[status].reject{|i| i.date != date || !i.physical?}.compact.size
               end
            end
         end
         
         def booked(status=nil,date=nil)
            if date.nil?
               if status.nil?
                  @items.values.collect{|v| v.reject{|i| !i.booked?}}.flatten.compact.size
               elsif @items[status].nil?
                  0
               else
                  @items[status].reject{|i| !i.booked?}.size
               end
            else
               if status.nil?
                  @items.values.collect{|v| v.reject{|i| i.date != date || !i.booked?}}.flatten.compact.size
               elsif @items[status].nil?
                  return 0
               else
                  @items[status].reject{|i| i.date != date || !i.booked?}.compact.size
               end
            end
         end
         
         def released(status=nil,date=nil)
            if date.nil?
               if status.nil?
                  @items.values.collect{|v| v.reject{|i| !i.released?}}.flatten.compact.size
               elsif @items[status].nil?
                  0
               else
                  @items[status].reject{|i| !i.released?}.size
               end
            else
               if status.nil?
                  @items.values.collect{|v| v.reject{|i| i.date != date || !i.released?}}.flatten.compact.size
               elsif @items[status].nil?
                  return 0
               else
                  @items[status].reject{|i| i.date != date || !i.released?}.compact.size
               end
            end
         end
         
         def transferred(status=nil,date=nil)
            if date.nil?
               if status.nil?
                  @items.values.collect{|v| v.reject{|i| !i.transferred?}}.flatten.compact.size
               elsif @items[status].nil?
                  0
               else
                  @items[status].reject{|i| !i.transferred?}.size
               end
            else
               if status.nil?
                  @items.values.collect{|v| v.reject{|i| i.date != date || !i.transferred?}}.flatten.compact.size
               elsif @items[status].nil?
                  return 0
               else
                  @items[status].reject{|i| i.date != date || !i.transferred?}.compact.size
               end
            end
         end
         
         def items(status)
            @items[status].sort{|a,b| a.name + a.id.to_s.rjust(10,'0') + a.date.to_s(:db) <=> b.name + b.id.to_s.rjust(10,'0') + a.date.to_s(:db)}
         end
         
         def rows(status)
            return [] unless status.is_a?(String)
            return [] if @items[status].nil? || @items[status].empty?
            item_rows = []
            last_booking = nil
            start_date = nil
            billing_count = 0
            billing_total = 0
            physical_count = 0
            physical_total = 0
            lines = @items[status].sort{|a,b| a.name + a.id.to_s.rjust(10,'0') + a.date.to_s(:db) <=> b.name + b.id.to_s.rjust(10,'0') + b.date.to_s(:db)}
            lines.size.times do |x|
               if last_booking.nil?
                  last_booking = lines[x].id
                  start_date = lines[x].date
               end
               if last_booking != lines[x].id
                  # new booking id, write the previous record
                  item_rows.push([lines[x-1].id.to_s, lines[x-1].person_id.to_s, lines[x-1].name, Booking.format_date(lines[x-1].booked), Booking.format_date(lines[x-1].released), Booking.format_date(start_date), Booking.format_date(lines[x-1].date), physical_count.to_s, billing_count.to_s])
                  if lines[x].billable?
                     billing_count = 1
                     billing_total += 1
                  else
                     billing_count = 0
                  end
                  if lines[x].physical?
                     physical_count = 1
                     physical_total += 1
                  else
                     physical_count = 0
                  end
                  last_booking = lines[x].id
                  start_date = lines[x].date
               else
                  if lines[x].billable?
                     billing_count += 1
                     billing_total += 1
                  end
                  if lines[x].physical?
                     physical_count += 1
                     physical_total += 1
                  end
               end
            end
            # push the last line
            item_rows.push([lines.last.id.to_s, lines.last.person_id.to_s, lines.last.name, Booking.format_date(lines.last.booked), Booking.format_date(lines.last.released), Booking.format_date(start_date), Booking.format_date(lines.last.date), physical_count.to_s, billing_count.to_s])
            # add a blank line
            item_rows.push([' ',' ',' ',' ',' ',' ','Totals:',physical_total.to_s,billing_total.to_s])
            return item_rows
         end
         
         def summary
            day_rows = []
            @start_date.upto(@stop_date) do |d|
               day_rows.push([
                  Booking.format_date(d),
                  ' ',
                  booked('DOC Billable',d).to_s,
                  (booked('DOC',d) + booked('Parish',d)).to_s,
                  booked('City',d).to_s,
                  booked('Other',d).to_s,
                  ' ',
                  released('DOC Billable',d).to_s,
                  (released('DOC',d) + released('Parish',d)).to_s,
                  released('City',d).to_s,
                  released('Other',d).to_s,
                  count('Temporary Release',d).to_s,
                  ' ',
                  count('Not Billable',d).to_s,
                  ' ',
                  physical('DOC Billable',d).to_s,
                  (physical('DOC',d) + physical('Parish',d)).to_s,
                  physical('City',d).to_s,
                  physical('Other',d).to_s,
                  physical(nil,d).to_s,
                  ' ',
                  billable('DOC Billable',d).to_s,
                  (billable('DOC',d) + billable('Parish',d)).to_s,
                  billable('City',d).to_s,
                  billable('Other',d).to_s,
                  billable(nil,d).to_s
               ])
            end
            blank_line = []
            26.times do
               blank_line.push(' ')
            end
            day_rows.push(blank_line)
            day_rows.push([
               'Totals:',
               ' ',
               booked('DOC Billable').to_s,
               (booked('DOC') + booked('Parish')).to_s,
               booked('City').to_s,
               booked('Other').to_s,
               ' ',
               released('DOC Billable').to_s,
               (released('DOC') + released('Parish')).to_s,
               released('City').to_s,
               released('Other').to_s,
               count('Temporary Release').to_s,
               ' ',
               count('Not Billable').to_s,
               ' ',
               physical('DOC Billable').to_s,
               (physical('DOC') + physical('Parish')).to_s,
               physical('City').to_s,
               physical('Other').to_s,
               physical.to_s,
               ' ',
               billable('DOC Billable').to_s,
               (billable('DOC') + billable('Parish')).to_s,
               billable('City').to_s,
               billable('Other').to_s,
               billable.to_s
            ])
         end
      end
      
      class Item
         def initialize(date, status, booking)
            @date = date
            @status = status
            @id = booking.id
            @person_id = booking.person.id
            @name = booking.person.full_name
            @booked = booking.booking_date
            @released = booking.release_date
            @agency = booking.is_transferred(Booking.get_datetime(@date,"23:59"))
         end
         
         def date
            @date
         end
         
         def status
            @status
         end
         
         def id
            @id
         end
         
         def person_id
            @person_id
         end
         
         def name
            @name
         end
         
         def booked
            @booked
         end
         
         def released
            @released
         end
         
         def agency
            @agency
         end
         
         def transferred?
            !@agency.blank?
         end
         
         def billable?
            return false if @status == 'Temporary Release' || @status == 'Not Billable'
            return false if @status == 'DOC Billable' && released?
            true
         end
         
         def physical?
            return false if @status == 'Temporary Release' || @status == 'Not Billable'
            return false if released?
            return false if transferred?
            true
         end
         
         def booked?
            @booked == @date
         end
         
         def released?
            return false if @released.nil?
            @released == @date
         end
      end
   end
end