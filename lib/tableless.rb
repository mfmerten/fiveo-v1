# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# added to allow using active record models without an actual table for
# when you would like to validate an input form but not save it directly

# I copied this from a project on github (http://github.com/willrjmarshall/Tableless)
# by Will Marshall who got it from a comment on stackoverflow.com that was posted by John Topley

module Tableless
  def self.included(base)
    base.extend(ClassMethods)
    base.send(:include,InstanceMethods)
  end
  
  module InstanceMethods
    def save(validate = true)
      validate ? valid? : true
    end
  end
  
  module ClassMethods
    # added to allow annotate_models plugin to skip Tableless models
    def tableless?
       true
    end
    
    def desc
       # included to automatically exclude Tableless models from Database
       # Controller index view (table list)
       nil
    end
       
    def column(name, sql_type = nil, default = nil, null = true)
      columns << ActiveRecord::ConnectionAdapters::Column.new(name.to_s, default,
        sql_type.to_s, null)
    end
    
    def columns
      @columns ||= [];
    end
  end
end