# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
namespace :fiveo do
   desc "Run all hourly tasks"
   task(:update_hourly => :environment) do
      Booking.update_sentences_by_hour
      Docket.process_dockets
      Activity.purge_old_records
      MessageLog.purge
   end
   
   desc "Process any waiting sentences/revocations"
   task(:process_dockets => :environment) do
      Docket.process_dockets
   end
   
   desc "Reap old sessions"
   task(:reap_sessions => :environment) do
      Session.reap_expired_sessions
   end
   
   desc "Update Cashless Systems Database"
   task(:update_cashless_systems => :environment) do
     Booking.update_cashless_systems
   end
end

      
               
         