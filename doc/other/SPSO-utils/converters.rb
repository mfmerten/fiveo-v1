module Converters
   
   # Stuff (pre-evidence) ----------------------------------------------------
   class Stuffs
      def self.load
         puts "   Processing Stuff..."
         record_count = skip_count = case_count = desc_count = 0
         CSV.open('Evidence.txt','r') do |row|
            # need case_no, description
            if sanitize_case(row[1]).nil? || sanitize_case(row[1]) == "45678"
               skip_count += 1
               case_count += 1
               next
            end
            
            if sanitize_string(row[3]).nil?
               skip_count += 1
               desc_count += 1
               next
            end
            Stuff.create(
               :case_no => sanitize_case(row[1]),
               :item_no => sanitize_string(row[2]),
               :description => sanitize_string(row[3]),
               :location => sanitize_string(row[4]),
               :suspect => sanitize_name(row[5]),
               :complainant => sanitize_name(row[6]),
               :case_type => sanitize_string(row[7])
            )
            record_count += 1
         end
         puts "      Added #{record_count} Stuffs, skipped #{skip_count} (#{case_count} for case, #{desc_count} for description)"
      end
      
      def self.write_csv
         if Stuff.count == 0
            Stuffs.load
         end
         puts "      Writing output file..."
         outfile = File.open('stuffs.csv','w')
         outfile.puts(Stuff.to_csv)
         outfile.close
      end
   end
   
   # Evidence ----------------------------------------------------------------
   class Evidences
      def self.load
         if Stuff.count == 0
            Stuffs.load
         end
         puts "   Processing Evidence..."
         record_count = skip_count = 0
         Stuff.find(:all).each do |s|
            
            eno = "#{s.case_no}#{s.item_no? ? '-' + s.item_no : ''}"
            desc = ""
            if s.suspect?
               desc << "Suspect: #{s.suspect}\n"
            end
            if s.complainant?
               desc << "Victim: #{s.complainant}\n"
            end
            if s.case_type?
               desc << "Case Type: #{s.case_type}\n"
            end
            desc << s.description
            
            Evidence.create(
               :evidence_number => eno,
               :case_number => s.case_no,
               :description => desc,
               :location => s.location
            )
            record_count += 1
         end
         puts "      Added #{record_count} Evidence records, skipped #{skip_count}."
      end
      
      def self.write_csv
         if Evidence.count == 0
            Evidences.load
         end
         puts "      Writing output file..."
         outfile = File.open('evidences.csv','w')
         outfile.puts(Evidence.to_csv)
         outfile.close
      end
   end
   
   # Jails (pre-booking)------------------------------------------------------
   class Jails
      def self.load
         if Param.count == 0
            Params.load
         end
         if Criminal.count == 0
            Criminals.load
         end
         record_count = skip_count = date_skip_count = person_skip_count = 0
         puts "   Processing Jails..."
         ["bookasc.txt","bookasc2.txt"].each do |file|
            infile = File.open(file, 'r')
            if file == 'bookasc.txt'
               puts "      Processing Men's Jail..."
            else
               puts "      Processing Women's Jail..."
            end
            while line = infile.gets
               linebuf = StringIO.new(line, "r")
               row = []
               [6,8,70,70,7,7,4,30,40,30,30,30,70,70,2,4,8,8,4,1,12,80,4,8].each do |chlen|
                  if linebuf.eof?
                     row.push(nil)
                  else
                     row.push(linebuf.read(chlen))
                  end
               end
               linebuf.close
               unless person = Criminal.find_by_person(sanitize_integer(row[0]))
                  skip_count += 1
                  person_skip_count += 1
                  next
               end
               unless book = sanitize_date(row[1], true)
                  book = sanitize_date(row[23], true)
               end
               if book.nil?
                  skip_count += 1
                  date_skip_count += 1
                  next
               end
               # check for duplicate records - use data from later one as the master version
               if existing = Jail.find(:first, :conditions => {:inmate => person.person, :booking_date => book, :booking_time => sanitize_time(row[6])})
                  existing.property1 = proper_case(sanitize_string(row[2]))
                  existing.property2 = proper_case(sanitize_string(row[3]))
                  existing.cash_deposited = sanitize_decimal(row[4])
                  existing.cash_retained = sanitize_decimal(row[5])
                  existing.attorney = sanitize_name(row[7])
                  existing.em_notify = sanitize_string(row[8])
                  existing.visitor1 = sanitize_string(row[9])
                  existing.visitor2 = sanitize_string(row[10])
                  existing.visitor3 = sanitize_string(row[11])
                  existing.medication = sanitize_string(row[12])
                  existing.holds = sanitize_string(row[13])
                  existing.jail = (file == 'bookasc.txt' ? 'Sabine Parish Detention Center' : 'Sabine Parish Womens Facility')
                  existing.cell = sanitize_integer(row[15])
                  existing.sched_release_date = sanitize_date(row[16],true)
                  existing.actual_release_date = sanitize_date(row[17],true)
                  existing.release_time = sanitize_time(row[18])
                  existing.release_cause = sanitize_string(row[19])
                  existing.phone_called = sanitize_phone(row[20])
                  existing.remarks = sanitize_text(row[21])
                  existing.booking_officer = Param.lookup('S-OFFICER', row[22])
                  existing.arrest_date = sanitize_date(row[23], true)
                  existing.save
                  skip_count += 1
               else
                  Jail.create(
                     :inmate => person.person,
                     :booking_date => book,
                     :property1 => proper_case(sanitize_string(row[2])),
                     :property2 => proper_case(sanitize_string(row[3])),
                     :cash_deposited => sanitize_decimal(row[4]),
                     :cash_retained => sanitize_decimal(row[5]),
                     :booking_time => sanitize_time(row[6]),
                     :attorney => sanitize_name(row[7]),
                     :em_notify => proper_case(sanitize_string(row[8])),
                     :visitor1 => proper_case(sanitize_string(row[9])),
                     :visitor2 => proper_case(sanitize_string(row[10])),
                     :visitor3 => proper_case(sanitize_string(row[11])),
                     :medication => sanitize_string(row[12]),
                     :holds => sanitize_string(row[13]),
                     :jail => (file == 'bookasc.txt' ? 'Sabine Parish Detention Center' : 'Sabine Parish Womens Facility'),
                     :cell => sanitize_integer(row[15]),
                     :sched_release_date => sanitize_date(row[16],true),
                     :actual_release_date => sanitize_date(row[17],true),
                     :release_time => sanitize_time(row[18]),
                     :release_cause => sanitize_string(row[19]),
                     :phone_called => sanitize_phone(row[20]),
                     :remarks => sanitize_text(row[21]),
                     :booking_officer => proper_case(Param.lookup('S-OFFICER', row[22])),
                     :arrest_date => sanitize_date(row[23], true)
                  )
                  record_count += 1
               end
            end
            infile.close
         end
         puts "      Added #{record_count} Jail Records, skipped #{skip_count} (#{date_skip_count} for missing date, #{person_skip_count} for invalid person)."
      end
      
      def self.write_csv
         if Jail.count == 0
            Jails.load
         end
         puts "      Writing output file..."
         outfile = File.open('jails.csv','w')
         outfile.puts(Jail.to_csv)
         outfile.close
      end
   end
   
   # Bookings ----------------------------------------------------------------
   class Bookings
      def self.load
         if Jail.count == 0
            Jails.load
         end
         puts "   Processing Bookings..."
         record_count = skip_count = over_count = 0
         Jail.find(:all, :order => 'booking_date ASC, booking_time ASC').each do |jail|
            
            # check to see if we have an overlapping booking
            overlap = Jail.find(:all, :conditions => ["id != ? and inmate = ? and booking_date <= ? AND actual_release_date is not null and actual_release_date > ?",jail.id,jail.inmate,jail.booking_date, jail.booking_date])
            unless overlap.empty?
               # this booking added while another booking active - usually for
               # additional arrest charges. skip it.
               skip_count += 1
               over_count += 1
               next
            end
            
            Booking.create(
               :person => jail.inmate,
               :booking_date => jail.booking_date,
               :booking_time => jail.booking_time,
               :cash_at_booking => jail.cash_deposited,
               :booking_officer => jail.booking_officer,
               :facility => jail.jail,
               :phone_called => jail.phone_called,
               :scheduled_release_date => jail.sched_release_date,
               :actual_release_date => jail.actual_release_date,
               :actual_release_time => jail.release_time,
               :attorney => jail.attorney,
               :em_notify => sanitize_name(jail.em_notify),
               :em_phone => sanitize_phone(jail.em_notify),
               :remarks => jail.remarks,
               :visitors => Booking.visitors_of(jail.visitor1, jail.visitor2, jail.visitor3),
               :property => Booking.property_of(jail.property1, jail.property2)
            )
            record_count += 1
         end
         puts "      Added #{record_count} Booking Records, skipped #{skip_count} (#{over_count} for overlapping dates)."
         puts "      #{Booking.count(:conditions => 'actual_release_date is null')} booking records without release date."
      end
      
      def self.write_csv
         if Booking.count == 0
            Bookings.load
         end
         puts "      Writing output file..."
         outfile = File.open('bookings.csv','w')
         outfile.puts(Booking.to_csv)
         outfile.close
      end
   end
   
   # Arrests -----------------------------------------------------------------
   class Arrests
      def self.load
         if Crime.count == 0
            Crimes.load
         end
         record_count = skip_count = 0
         puts "   Processing Arrests..."
         Crime.find(:all, :order => 'arrest_date ASC, arrest_time ASC').each do |c|
            arr = Arrest.new(
               :person => c.criminal,
               :case_no => c.case1,
               :arrest_date => c.arrest_date,
               :arrest_time => c.arrest_time,
               :officer1 => c.officer1,
               :officer2 => c.officer2,
               :officer3 => c.video,
               :agency => c.agency,
               :ward => c.ward,
               :hour_72_date => c.hour_72_date,
               :remarks => c.remarks,
               :arrest_type => 0,
               :charge1 => Arrest.charge_of(c.charge1_cnt, c.charge1),
               :charge2 => Arrest.charge_of(c.charge2_cnt, c.charge2),
               :charge3 => Arrest.charge_of(c.charge3_cnt, c.charge3),
               :charge4 => Arrest.charge_of(c.charge4_cnt, c.charge4),
               :charge5 => Arrest.charge_of(c.charge5_cnt, c.charge5),
               :charge6 => Arrest.charge_of(c.charge6_cnt, c.charge6),
               :charge7 => Arrest.charge_of(c.charge7_cnt, c.charge7),
               :charge8 => Arrest.charge_of(c.charge8_cnt, c.charge8),
               :charge9 => Arrest.charge_of(c.charge9_cnt, c.charge9),
               :charge10 => Arrest.charge_of(c.charge10_cnt, c.charge10)
            )
            if arr.charge1.nil? && arr.charge2.nil? && arr.charge3.nil? && arr.charge4.nil? && arr.charge5.nil? && arr.charge6.nil? && arr.charge7.nil? && arr.charge8.nil? && arr.charge9.nil? && arr.charge10.nil?
               skip_count += 1
               next
            else
               record_count += 1
               arr.save
            end
         end
         puts "      Added #{record_count} Arrests, Skipped #{skip_count}."
      end

      def self.write_csv
         if Arrest.count == 0
            Arrests.load
         end
         puts "      Writing Output File..."
         outfile = File.open('arrests.csv','w')
         outfile.puts(Arrest.to_csv)
         outfile.close
      end
   end
   
   # Crimes (pre-arrests)-----------------------------------------------------
   class Crimes
      def self.load
         if Criminal.count == 0
            Criminals.load
         end
         if Param.count == 0
            Params.load
         end
         puts "   Processing Crimes..."
         record_count = skip_count = 0
         ["arrasc.txt","arrasc2.txt"].each do |file|
            infile = File.open(file, 'r')
            if file == 'arrasc.txt'
               puts "      Processing Men's Jail..."
            else
               puts "      Processing Women's Jail..."
            end
            while line = infile.gets
               linebuf = StringIO.new(line, "r")
               row = []
               [6,8,2,2,4,4,1,4,4,4,4,4,4,4,4,4,4,2,2,2,2,2,2,2,2,2,2,4,4,1,80,8,8,6,6,13,10,4,6,30,30,30,30,30,20,20,10,2,20,20,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,2,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,2,2,4,6,6,6,6,6,80,80,80,80,80,80,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75,75].each do |chlen|
                  if linebuf.eof?
                     row.push(nil)
                  else
                     row.push(linebuf.read(chlen))
                  end
               end
               linebuf.close
               unless person = Criminal.find_by_person(sanitize_integer(row[0]))
                  skip_count += 1
                  next
               end
               
               Crime.create(
                  :criminal => person.person,
                  :arrest_date => sanitize_date(row[1],true),
                  :zone => sanitize_integer(row[3]),
                  :parish => sanitize_integer(row[4]),
                  :arrest_time => sanitize_time(row[5]),
                  :charge1 => proper_case(Param.lookup("CHG-CRM", row[7])),
                  :charge2 => proper_case(Param.lookup("CHG-CRM", row[8])),
                  :charge3 => proper_case(Param.lookup("CHG-CRM", row[9])),
                  :charge4 => proper_case(Param.lookup("CHG-CRM", row[10])),
                  :charge5 => proper_case(Param.lookup("CHG-CRM", row[11])),
                  :charge6 => proper_case(Param.lookup("CHG-CRM", row[12])),
                  :charge7 => proper_case(Param.lookup("CHG-CRM", row[13])),
                  :charge8 => proper_case(Param.lookup("CHG-CRM", row[14])),
                  :charge9 => proper_case(Param.lookup("CHG-CRM", row[15])),
                  :charge10 => proper_case(Param.lookup("CHG-CRM", row[16])),
                  :officer1 => proper_case(Param.lookup("S-OFFICER", row[27])),
                  :officer2 => proper_case(Param.lookup("S-OFFICER", row[28])),
                  :remarks => sanitize_text(row[30]),
                  :hour_72_date => sanitize_date(row[32],true),
                  :case1 => sanitize_case(row[33]),
                  :video => proper_case(Param.lookup("S-OFFICER", row[38])),
                  :charge1_cnt => sanitize_integer(row[51]),
                  :charge2_cnt => sanitize_integer(row[52]),
                  :charge3_cnt => sanitize_integer(row[53]),
                  :charge4_cnt => sanitize_integer(row[54]),
                  :charge5_cnt => sanitize_integer(row[55]),
                  :charge6_cnt => sanitize_integer(row[56]),
                  :charge7_cnt => sanitize_integer(row[57]),
                  :charge8_cnt => sanitize_integer(row[58]),
                  :charge9_cnt => sanitize_integer(row[59]),
                  :charge10_cnt => sanitize_integer(row[60]),
                  :ward => sanitize_integer(row[114]),
                  :agency => proper_case(Param.lookup("S-OFFICER", row[117]))
               )
               record_count += 1
            end
            infile.close
         end
         puts "      Added #{record_count} Crimes, skipped #{skip_count}."
      end
      
      def self.write_csv
         if Crime.count == 0
            Crimes.load
         end
         puts "      Writing output file..."
         outfile = File.open('crimes.csv', 'w')
         outfile.puts(Crime.to_csv)
         outfile.close
      end
   end
   
   # Parameters --------------------------------------------------------------
   class Params
      def self.load
         puts "   Processing Parameters..."
         infile = File.open("paraasc.txt","r")
         record_count = skip_count = 0
         while line = infile.gets
            if is_blank(sanitize_string(line.slice(0,10))) || is_blank(sanitize_string(line.slice(10,6))) || is_blank(sanitize_string(line.slice(16,55)))
               skip_count += 1
               next
            end
            param = Param.create(
               :category => sanitize_string(line.slice(0,10)).strip,
               :name => sanitize_string(line.slice(10,6)).strip,
               :value => sanitize_string(line.slice(16,55)).strip
            )
            record_count += 1
         end
         infile.close
         puts "      Added #{record_count} Parameters, skipped #{skip_count}."
      end
      
      def self.write_csv
         if Param.count == 0
            Params.load
         end
         puts "      Writing Output File..."
         outfile = File.open('parameters.csv','w')
         outfile.puts(Param.to_csv)
         outfile.close
      end
   end
   
   # Reports (pre-offenses)---------------------------------------------------
   class Reports
      def self.load
         puts "   Processing Reports..."
         record_count = skip_count = date_count = det_count = 0
         CSV.open('Offense.txt','r') do |row|
            # need offense_date, details
            if sanitize_date(row[4]).nil?
               skip_count += 1
               date_count += 1
               next
            end
            
            if sanitize_text(row[11]).nil?
               skip_count += 1
               det_count += 1
               next
            end
            
            unless officer = sanitize_name(row[24])
               officer = "Unknown"
            end
            
            Report.create(
               :case_no => sanitize_case(row[3]),
               :offense_date => sanitize_date(row[4]),
               :offense_time => sanitize_time(row[5]),
               :victim => sanitize_name(row[6]),
               :victim_address => sanitize_address(row[7]),
               :victim_phone => sanitize_phone(row[8]),
               :location => proper_case(sanitize_string(row[9])),
               :suspect => sanitize_name(row[10]),
               :details => sanitize_text(row[11]),
               :pictures => Report.boolean_of(row[12]),
               :restitution => Report.boolean_of(row[13]),
               :victim_notify => Report.boolean_of(row[14]),
               :wrecker => Report.boolean_of(row[15]),
               :vehicle => Report.boolean_of(row[16]),
               :medical => Report.boolean_of(row[17]),
               :hour_48 => Report.boolean_of(row[18]),
               :permission => Report.boolean_of(row[19]),
               :summons => Report.boolean_of(row[20]),
               :evidence => Report.boolean_of(row[21]),
               :statements => proper_case(sanitize_string(row[22])),
               :documents => proper_case(sanitize_string(row[23])),
               :officer => officer
            )
            record_count += 1
         end
         puts "      Added #{record_count} Reports, skipped #{skip_count} (#{date_count} for date, #{det_count} for details)."
      end
      
      def self.write_csv
         if Report.count == 0
            Reports.load
         end
         puts "      Writing output file..."
         outfile = File.open('reports.csv','w')
         outfile.puts(Report.to_csv)
         outfile.close
      end
   end
   
   # Offenses ----------------------------------------------------------------
   class Offenses
      def self.load
         if Report.count == 0
            Reports.load
         end
         puts "   Processing Offenses..."
         record_count = skip_count = 0
         Report.find(:all, :order => 'offense_date, offense_time').each do |rep|
            
            # build Details
            det = ""
            if rep.victim?
               det << "Victim: #{rep.victim}\n"
            end
            if rep.victim_address?
               if rep.victim?
                  det << "        #{rep.victim_address}\n"
               else
                  det << "Victim: #{rep.victim_address}\n"
               end
            end
            if rep.victim_phone?
               if rep.victim? || rep.victim_address?
                  det << "        #{rep.victim_phone}\n"
               else
                  det << "Victim: #{rep.victim_phone}\n"
               end
            end
            if rep.suspect?
               det << "Suspect: #{rep.suspect}\n"
            end
            if rep.wrecker == 1
               det << "Wrecker Requested\n"
            end
            if rep.vehicle == 1
               det << "Vehicle Stored.\n"
            end
            if rep.evidence == 1
               det << "Evidence Collected.\n"
            end
            unless det.empty?
               det << "\n"
            end
            det << rep.details
            
            # list of documents
            docs = ""
            if rep.statements?
               docs << "Statements By: #{rep.statements}\n"
            end
            if rep.documents?
               docs << "Other: #{rep.documents}\n"
            end
            
            Offense.create(
               :offense_date => rep.offense_date,
               :offense_time => rep.offense_time,
               :case_no => rep.case_no,
               :location => rep.location,
               :details => det,
               :pictures => rep.pictures,
               :restitution_form => rep.restitution,
               :victim_notification => rep.victim_notify,
               :perm_to_search => rep.permission,
               :misd_summons => rep.summons,
               :fortyeight_hour => rep.hour_48,
               :medical_release => rep.medical,
               :other_documents => docs,
               :officer => rep.officer
            )
            record_count += 1
         end
         puts "      Added #{record_count} Offenses, skipped #{skip_count}"
      end
      
      def self.write_csv
         if Offense.count == 0
            Offenses.load
         end
         puts "      Writing Output File..."
         outfile = File.open('offenses.csv','w')
         outfile.puts(Offense.to_csv)
         outfile.close
      end
   end
   
   # Forbids -----------------------------------------------------------------
   class Forbids
      def self.load
         # load db table from original csv file
         puts "   Processing Forbids..."
         record_count = 0
         skip_count = 0
         CSV.open('Forbidden.txt','r') do |row|
            # at a minimum, need subject, complainant
            if sanitize_name(row[1]).nil? || sanitize_name(row[7]).nil?
               skip_count += 1
               next
            end
            
            # fix missing dates
            requested, signed, remarks = Forbid.fix_dates(row[6],row[5])

            Forbid.create(
               :subject => sanitize_name(row[1]),
               :sub_street => sanitize_address(row[2]),
               :sub_city_state_zip => sanitize_address(row[3]),
               :sub_phone => sanitize_phone(row[4]),
               :complainant => sanitize_name(row[7]),
               :comp_street => sanitize_address(row[8]),
               :comp_city_state_zip => sanitize_address(row[9]),
               :comp_phone => sanitize_phone(row[10]),
               :requested_date => requested,
               :signed_date => signed,
               :recalled_date => sanitize_date(row[13]),
               :req_officer => sanitize_name(row[11]),
               :details => sanitize_text(row[14]),
               :remarks => remarks
            )
            record_count += 1
         end
         puts "      Added #{record_count} Forbids, Skipped #{skip_count}."
      end
      
      def self.write_csv
         if Forbid.count == 0
            Forbids.load
         end
         puts "      Writing output file..."
         outfile = File.open("forbids.csv","w")
         outfile.puts(Forbid.to_csv)
         outfile.close
      end
   end
   
   # Warrants ----------------------------------------------------------------
   class Warrants
      def self.load
         if Subject.count == 0
            Subjects.load
         end
         puts "   Processing Warrants File..."
         record_count = 0
         skip_count = 0
         CSV.open('Warrants.txt', 'r') do |row|
            
            # must be able to match record with a subject
            if row[1].nil? || row[1].to_i == 0
               if row[2].nil? || row[2].to_i == 0
                  person = nil
               else
                  person = Subject.find_by_subject(row[2].to_i)
               end
            else
               person = Subject.find_by_subject(row[1].to_i)
            end
            if person.nil?
               skip_count += 1
               next
            end
            
            # has to have a warrant number
            if is_blank(row[3])
               skip_count += 1
               next
            end
            
            # determine disposition
            dispo, dispo_date, remarks = Warrant.dispo_of(row[14], row[15])

            Warrant.create(
               :warrant_no => sanitize_string(row[3]).upcase,
               :warrant_type => Warrant.type_of(row[4]),
               :jurisdiction => sanitize_name(row[5]),
               :juris_phone => sanitize_phone(row[6]),
               :juris_fax => sanitize_phone(row[7]),
               :received_date => sanitize_date(row[8]),
               :charge1 => "1: #{proper_case(sanitize_string(row[9]))}",
               :issued_date => sanitize_date(row[11]),
               :bond_amt => sanitize_decimal(row[13]),
               :payable => sanitize_decimal(row[12]),
               :subject => person.full_name,
               :sub_street => person.street,
               :sub_city => person.city,
               :sub_state => person.state,
               :sub_zip => person.zip,
               :sub_ssn => person.ssn,
               :sub_dob => person.dob,
               :sub_oln => person.oln,
               :sub_oln_state => person.oln_state,
               :sub_race => person.race_name,
               :sub_sex => person.sex_number,
               :disposition => dispo,
               :dispo_date => dispo_date,
               :remarks => remarks
            )
            record_count += 1
         end
         puts "      Added #{record_count} Warrants, Skipped #{skip_count}."
      end
      
      def self.write_csv
         if Warrant.count == 0
            Warrants.load
         end
         puts "      Writing Output File..."
         outfile = File.open("warrants.csv","w")
         outfile.puts(Warrant.to_csv)
         outfile.close
      end
   end
   
   # Warrant Subjects --------------------------------------------------------
   class Subjects
      def self.load
         puts "   Processing Subject File..."
         bad_ids = {}
         record_count = skip_count = 0
         CSV.open("WarrantSubject.txt", 'r') do |p|
            
            # determine the actual person id number
            id_no = Subject.id_of(p[0],p[1])
            if id_no == 0
               skip_count += 1
               next
            end

            if person = Subject.find_by_subject(id_no)
               # already have one by this number which means both are bad
               bad_ids[id_no.to_s] = 1
               person.destroy
               skip_count += 1
               next
            end
            
            # skip any we have already determined to be bad
            if bad_ids[id_no.to_s] == 1
               skip_count += 1
               next
            end

            Subject.create(
               :subject => id_no,
               :lastname => sanitize_name(p[2]),
               :middlename => sanitize_name(p[4]),
               :firstname => sanitize_name(p[3]),
               :street => sanitize_address(p[5]),
               :city => sanitize_name(p[6]),
               :state => sanitize_state(p[7]),
               :zip => sanitize_zip(p[8]),
               :race => sanitize_name(p[12]),
               :sex => sanitize_string(p[13]),
               :dob => sanitize_date(p[14]),
               :ssn => sanitize_ssn(p[15]),
               :oln => sanitize_oln(p[16]),
               :oln_state => sanitize_state(p[17])
            )
            record_count += 1
         end
         puts "      Added #{record_count} Warrant Subjects, Skipped #{skip_count}."
      end
      
      def self.write_csv
         if Subject.count == 0
            Subjects.load
         end
         puts "      Writing Output File..."
         outfile = File.open('subjects.csv','w')
         outfile.puts(Subject.to_csv)
         outfile.close
      end
   end
   
   # Pawn Tickets ------------------------------------------------------------
   class Pawns
      def self.load
         if PawnTemp.count == 0
            PawnTemps.load
         end
         puts "   Processing Pawn Tickets..."
         while p = PawnTemp.find(:first)
            pawns = PawnTemp.find(:all, :conditions => {:company => p.company, :ticket_no => p.ticket_no, :name => p.name})
            item_list = pawns.collect{|i| i.items}.join(',')
            Pawn.create(
               :company => p.company,
               :ticket_no => p.ticket_no,
               :ticket_date => p.ticket_date,
               :ticket_type => p.ticket_type,
               :name => p.name,
               :addr1 => p.addr1,
               :addr2 => p.addr2,
               :oln => p.oln,
               :oln_state => p.oln_state,
               :ssn => p.ssn,
               :items => item_list
            )
            pawns.each{|i| i.destroy}
         end
         puts "      Added #{Pawn.count} Pawn Tickets."
      end
      
      def self.write_csv
         if Pawn.count == 0
            Pawns.load
         end
         puts "      Writing output file..."
         outfile = File.open("pawntickets.csv","w")
         outfile.puts(Pawn.to_csv)
         outfile.close
      end
   end
   
   # Pawn Ticket Items (pre-pawntickets)--------------------------------------
   class PawnTemps
      def self.load
         puts "   Processing Pawn Items..."
         record_count = skip_count = 0
         CSV.open('PawnTickets.txt', 'r') do |row|
            
            # need pawn company, name and item list at a minimum
            unless sanitize_string(row[3]) && sanitize_string(row[4]) && (sanitize_string(row[9]) || sanitize_string(row[10]) || sanitize_string(row[11]) || sanitize_string(row[16]))
               skip_count += 1
               next
            end
            
            if sanitize_string(row[3]) == "0"
               skip_count += 1
               next
            end
            
            # determine ticket type from dates
            case
            when !sanitize_date(row[18]).nil? || !sanitize_date(row[17]).nil?
               ttype = 'Picked Up'
            when !sanitize_date(row[15]).nil? || !sanitize_date(row[14]).nil?
               ttype = 'Pawned'
            when !sanitize_date(row[13]).nil? || !sanitize_date(row[12]).nil?
               ttype = 'Purchased'
            else
               ttype = nil
            end
               
            # split oln and oln_state
            oln, olnst = PawnTemp.split_oln(row[7])
            
            PawnTemp.create(
               :company => proper_case(sanitize_string(row[3])),
               :ticket_no => sanitize_string(row[19]),
               :ticket_date => PawnTemp.date_of(row[21],row[20]),
               :ticket_type => ttype,
               :name => sanitize_name(row[4]),
               :addr1 => sanitize_address(row[5]),
               :addr2 => sanitize_address(row[6]),
               :oln => oln,
               :oln_state => olnst,
               :ssn => PawnTemp.ssn_of(row[8],row[7]),
               :items => PawnTemp.item_string_of(row[9],row[10],row[11],row[16])
            )
            record_count += 1
         end
         puts "      Added #{record_count} Pawn Items, Skipped #{skip_count}."
      end
      
      def self.write_csv
         if PawnTemp.count == 0
            PawnTemps.load
         end
         puts "      Writing output file..."
         outfile = File.open('pawnitems.csv','w')
         outfile.puts(PawnTemp.to_csv)
         outfile.close
      end
   end
   
   # Call Sheets -------------------------------------------------------------
   class Calls
      def self.load
         puts "   Processing Calls..."
         record_count = 0
         skip_count = 0
         CSV.open('Callsheets.txt', 'r') do |row|
               
            # check for case, date, time, subject_type
            if sanitize_string(row[1]).nil? || sanitize_date(row[2]).nil? || sanitize_time(row[3]).nil? || sanitize_string(row[8]).nil?
               skip_count += 1
               next
            end
               
            # make sure not a header row
            if sanitize_string(row[1]) == "CaseNo"
               skip_count += 1
               next
            end
            
            # get full name
            name = Call.full_name_of(row[9],row[10],row[11])
            
            # make sure we DO have a name
            if is_blank(name)
               skip_count += 1
               next
            end
            
            # get signal and code
            code, sig = Call.split_signal(row[6])
            
            # split reporter street,city,state,zip into addr1/addr2
            p2addr1, p2addr2 = Call.split_p2address(row[18])
               
            Call.create(
               :case_no => sanitize_case(row[1]),
               :signal_code => code,
               :signal => sig,
               :call_via => proper_case(sanitize_string(row[21])),
               :received_by => sanitize_name(row[20]),
               :call_date => sanitize_date(row[2]),
               :call_time => sanitize_time(row[3]),
               :ward => sanitize_integer(row[5]),
               :district => sanitize_integer(row[4]),
               :person1_type => proper_case(sanitize_string(row[8])),
               :person1_name => name,
               :person1_addr1 => sanitize_address(row[12]),
               :person1_addr2 => Call.p1_addr2_of(row[13],row[14],row[15]),
               :person1_home_phone => sanitize_phone(row[16]),
               :person2_type => 'Reported By',
               :person2_name => sanitize_name(row[17]),
               :person2_addr1 => p2addr1,
               :person2_addr2 => p2addr2,
               :person2_home_phone => sanitize_phone(row[19]),
               :details => Call.build_details(row[28],row[7],row[25],row[30]),
               :dispatch_date => sanitize_date(row[2]),
               :dispatch_time => sanitize_time(row[22]),
               :arrival_date => sanitize_date(row[2]),
               :arrival_time => sanitize_time(row[23]),
               :concluded_date => sanitize_date(row[2]),
               :concluded_time => sanitize_time(row[24]),
               :dispatcher => sanitize_name(row[26]),
               :detective => sanitize_name(row[29])
            )
            record_count += 1
         end
         puts "      Added #{record_count} Call Sheets, Skipped #{skip_count}."
      end
      
      def self.write_csv
         if Call.count == 0
            Calls.load
         end
         puts "      Writing output file..."
         outfile = File.open("callsheets.csv","w")
         outfile.puts(Call.to_csv)
         outfile.close
      end
   end
   
   # People ------------------------------------------------------------------
   class People
      def self.load
         if Criminal.count == 0
            Criminals.load
         end
         puts "   Processing People..."
         record_count = skip_count = 0
         Criminal.find(:all).each do |c|
            Person.create(
               :person => c.person,
               :lastname => c.lastname,
               :firstname => c.firstname,
               :middlename => c.middlename,
               :suffix => c.suffix,
               :aliases => Person.aliases_for(c.alias1,c.alias2,c.alias3,c.alias4),
               :phys_street => c.street,
               :phys_city => c.city,
               :phys_state => c.state,
               :phys_zip => c.zip,
               :dob => c.dob,
               :pob => c.pob,
               :race => c.race,
               :sex => c.sex,
               :sid => c.sid,
               :fbi => c.fbi,
               :ssn => c.ssn,
               :oln => c.oln,
               :oln_state => c.oln_state,
               :home_phone => c.home_phone,
               :height_ft => c.height_ft,
               :height_in => c.height_in,
               :weight => c.weight,
               :hair_color => c.hair_color,
               :hair_type => c.hair_type,
               :eye_color => c.eye_color,
               :glasses => c.glasses,
               :shoe_size => c.shoe_size,
               :complexion => c.complexion,
               :facial_hair => c.facial_hair,
               :occupation => c.occupation,
               :employer => c.employer,
               :remarks => c.remarks,
               :marks => Person.marks_for(c.scar1, c.scar2, c.scar3, c.scar4, c.scar5)
            )
            record_count += 1
         end
         puts "      Added #{record_count} People, Skipped #{skip_count}."
      end
      
      def self.write_csv
         if Person.count == 0
            People.load
         end
         puts "      Writing Output File..."
         outfile = File.open("people.csv", "w")
         outfile.puts(Person.to_csv)
         outfile.close
      end
   end
   
   # Criminals (pre-people)---------------------------------------------------
   class Criminals
      def self.load
         puts "   Processing Criminals..."
         puts "      Processing criminal file..."
         infile = File.open("crimasc.txt")
         record_count = skip_count = 0
         bad_ids = {}
         while line = infile.gets
            linebuf = StringIO.new(line, "r")
            row = []
            [6,17,11,11,1,1,8,4,1,5,30,11,2,30,17,11,11,17,11,11,17,11,11,17,11,11,80,3,3,1,5,2,2,2,2,2,1,1,1,30,30,27,8,20,20,2,12].each do |chlen|
               if linebuf.eof?
                  row.push(nil)
               else
                  row.push(linebuf.read(chlen))
               end
            end
            linebuf.close
            
            # determine the id number
            id_no = sanitize_integer(row[0])
            if id_no.nil? || id_no == 0
               skip_count += 1
               next
            end

            if person = Criminal.find_by_person(id_no)
               # already have one by this number which means both are bad
               bad_ids[id_no.to_s] = 1
               person.destroy
               skip_count += 2
               next
            end
            
            # skip any we have already determined to be bad
            if bad_ids[id_no.to_s] == 1
               skip_count += 1
               next
            end
            
            # must have first and last name
            firstname, suffix = fix_name(row[2], :with_suffix => true)
            if is_blank(suffix)
               lastname, suffix = fix_name(row[1], :with_suffix => true)
            else
               lastname = fix_name(row[1])
            end
            
            if is_blank(firstname) || is_blank(lastname) || (( !firstname.nil? && firstname.size == 1) && ( !lastname.nil? && lastname.size == 1))
               skip_count += 1
               next
            end
            
            # address
            city, state, zip = Criminal.address_of(row[40])
            
            Criminal.create(
               :person => sanitize_integer(row[0]),
               :lastname => lastname,
               :firstname => firstname,
               :middlename => fix_name(row[3], :spaces => true),
               :suffix => suffix,
               :sex => Criminal.sex_of(row[4]),
               :race => Criminal.race_of(row[5]),
               :dob => sanitize_date(row[6]),
               :weight => sanitize_integer(row[7]),
               :height_ft => sanitize_integer(row[8]),
               :height_in => sanitize_integer(row[9]),
               :pob => sanitize_address(row[10]),
               :ssn => sanitize_ssn(row[11]),
               :oln_state => sanitize_state(row[12]),
               :oln => sanitize_oln(row[13]),
               :alias1 => Criminal.name_of(row[14],row[15],row[16]),
               :alias2 => Criminal.name_of(row[17],row[18],row[19]),
               :alias3 => Criminal.name_of(row[20],row[21],row[22]),
               :alias4 => Criminal.name_of(row[23],row[24],row[25]),
               :remarks => sanitize_string(row[26]),
               :hair_color => Criminal.hair_color_of(row[27]),
               :eye_color => Criminal.eye_color_of(row[28]),
               :glasses => Criminal.glasses_of(row[29]),
               :shoe_size => sanitize_decimal(row[30]).to_s,
               :shoe_type => Criminal.shoe_type_of(row[45]),
               :scar1 => sanitize_string(row[31]),
               :scar2 => sanitize_string(row[32]),
               :scar3 => sanitize_string(row[33]),
               :scar4 => sanitize_string(row[34]),
               :scar5 => sanitize_string(row[35]),
               :hair_type => Criminal.hair_type_of(row[36]),
               :complexion => Criminal.complexion_of(row[37]),
               :facial_hair => Criminal.facial_hair_of(row[38]),
               :street => sanitize_address(row[39]),
               :city => city,
               :state => state,
               :zip => zip,
               :occupation => sanitize_name(row[41]),
               :employer => sanitize_name(row[43]),
               :sid => sanitize_string(row[44]),
               :home_phone => sanitize_phone(row[46])
            )
            record_count += 1
         end
         infile.close
         puts "      Added #{record_count} Criminals, skipped #{skip_count}."
         puts "      Processing prisoner file..."
         record_count = skip_count = 0
         ['prisasc.txt','prisasc2.txt'].each do |file|
            if file == 'prisasc.txt'
               puts "         Processing Men's Jail..."
            else
               puts "         Processing Women's Jail..."
            end
            infile = File.open(file)
            while line = infile.gets
               linebuf = StringIO.new(line, "r")
               row = []
               [10,1,20,15,1,1,1,8].each do |chlen|
                  if linebuf.eof?
                     row.push(nil)
                  else
                     row.push(linebuf.read(chlen))
                  end
               end
               linebuf.close
               
               # determine the id number
               id_no = sanitize_integer(row[0])
               if id_no.nil? || id_no == 0
                  skip_count += 1
                  next
               end
            
               if person = Criminal.find_by_person(id_no)
                  # already have one by this number -- skip it
                  skip_count += 1
                  next
               end
            
               # must have first and last name
               firstname, suffix = fix_name(row[3], :with_suffix => true)
               if is_blank(suffix)
                  lastname, suffix = fix_name(row[2], :with_suffix => true)
               else
                  lastname = fix_name(row[2])
               end
               
               if is_blank(firstname) || is_blank(lastname) || (( !firstname.nil? && firstname.size == 1) && ( !lastname.nil? && lastname.size == 1))
                  skip_count += 1
                  next
               end
               
               Criminal.create(
                  :person => sanitize_integer(row[0]),
                  :lastname => lastname,
                  :firstname => firstname,
                  :middlename => sanitize_name(row[4]),
                  :suffix => suffix,
                  :sex => Criminal.sex_of(row[5]),
                  :race => Criminal.race_of(row[6]),
                  :dob => sanitize_date(row[7])
               )
               record_count += 1
            end
            infile.close
         end
         puts "      Added #{record_count} Criminals, skipped #{skip_count}."
         puts "      Processing name file..."
         record_count = skip_count = 0
         CSV.open("Name.txt", 'r') do |p|
            # determine the id number
            id_no = sanitize_integer(p[11])
            if id_no.nil? || id_no == 0
               skip_count += 1
               next
            end

            if person = Criminal.find_by_person(id_no)
               # already have one by this number... skip it
               skip_count += 1
               next
            end
            
            if sanitize_string(p[13]) != "F"
               # only doing females from this file
               skip_count += 1
               next
            end
            
            # must have first and last name
            # first must strip improper text from names
            if sanitize_string(p[1]).nil?
               # no last name
               skip_count += 1
               next
            else
               templname = sanitize_string(p[1]).gsub(/\s*\([^)]*\)\s*/,' ')
            end
            if sanitize_string(p[3]).nil?
               tempmname = nil
            else
               tempmname = sanitize_string(p[3]).gsub(/\s*\([^)]*\)\s*/,' ')
            end
            if sanitize_string(p[2]).nil?
               # no first name
               skip_count += 1
               next
            else
               tempfname = sanitize_string(p[2]).gsub(/\s*\([^)]*\)\s*/,' ')
            end
            firstname, suffix = fix_name(tempfname, :with_suffix => true)
            middlename = fix_name(tempmname, :spaces => true)
            if is_blank(suffix)
               lastname, suffix = fix_name(templname, :with_suffix => true)
            else
               lastname = fix_name(templname)
            end
            if is_blank(firstname) || is_blank(lastname) || (( !firstname.nil? && firstname.size == 1) && ( !lastname.nil? && lastname.size == 1))
               skip_count += 1
               next
            end

            Criminal.create(
               :person => id_no,
               :lastname => lastname,
               :firstname => firstname,
               :middlename => middlename,
               :suffix => suffix,
               :sex => Criminal.sex_of(p[13]),
               :race => Criminal.race_of(p[12]),
               :dob => sanitize_date(p[9]),
               :ssn => sanitize_ssn(p[17]),
               :fbi => sanitize_string(p[15]),
               :oln_state => sanitize_state(p[20]),
               :oln => sanitize_oln(p[19]),
               :street => sanitize_address(p[5]),
               :city => sanitize_address(p[6]),
               :state => sanitize_state(p[7]),
               :zip => sanitize_zip(p[8]),
               :sid => sanitize_string(p[16])
            )
            record_count += 1
         end
         puts "      Added #{record_count} Criminals, skipped #{skip_count}."
      end
      
      def self.write_csv
         if Criminal.count == 0
            Criminals.load
         end
         puts "      Writing output file..."
         outfile = File.new('criminals.csv','w')
         outfile.puts(Criminal.to_csv)
         outfile.close
      end
   end
end