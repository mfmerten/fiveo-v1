module ConvertModels
   
   class Stuff < ActiveRecord::Base
   end
   
   class Evidence < ActiveRecord::Base
   end
   
   class Offense < ActiveRecord::Base
   end
   
   class Report < ActiveRecord::Base
      def self.boolean_of(string)
         return 1 if string == "YES"
         return 0
      end
   end
   
   class Jail < ActiveRecord::Base
   end
   
   class Booking < ActiveRecord::Base
      def self.visitors_of(line1,line2,line3)
         vis = []
         [line1,line2,line3].each do |line|
            unless is_blank(line)
               name = sanitize_name(line)
               phone = sanitize_phone(line)
               unless is_blank(name) && is_blank(phone)
                  vis.push("#{name}:#{phone}")
               end
            end
         end
         return vis.join(';') unless vis.empty?
         nil
      end
      
      def self.property_of(line1,line2)
         prop = []
         [line1,line2].each do |line|
            unless is_blank(line)
               prop.push("1:#{line.gsub(/;/,' ')}")
            end
         end
         return prop.join(';') unless prop.empty?
         nil
      end
   end
   
   class Arrest < ActiveRecord::Base
      def self.charge_of(cnt,charge)
         return nil if is_blank(charge)
         return "[#{cnt}] #{charge}"
      end
   end
   
   class Crime < ActiveRecord::Base
   end
      
   class Param < ActiveRecord::Base
      def self.lookup(typeparam,paramname)
         return nil if is_blank(paramname)
         if p = find(:first, :conditions => {:category => typeparam.strip, :name => paramname.strip})
            return p.value unless is_blank(p.value)
            return nil
         else
            return nil
         end
      end
   end
   
   class Forbid < ActiveRecord::Base
      def self.fix_dates(requested, signed)
         req = sanitize_date(requested)
         sig = sanitize_date(signed)
         rem = nil
         if req.nil?
            if sig.nil?
               req = Date.today
               sig = Date.today
               rem = "Missing Request Date and Signed Date were updated today (#{Date.today}) during conversion to new software. -- MFM"
            else
               req = sig
               rem = "Missing Request Date was updated from Signed Date during conversion to new software. -- MFM"
            end
         elsif sig.nil?
            sig = req
            rem = 'Missing Signed Date was updated from Request Date during conversion to new software. -- MFM'
         end
         return [req, sig, rem]
      end
   end

   class Subject < ActiveRecord::Base
      def self.id_of(no1,no2)
         if no1 != no2
            # replicated record or bogus id stored
            id_no = no1.clone.gsub(/[^\d]/,'').to_i
            if id_no > 4000  || id_no == 0
               id_no = no2.to_i
            end
         else
            id_no = no2.to_i
         end
         return id_no
      end
      
      def full_name
         lname = fname = mname = sname = nil
         if self.lastname?
            lname = fix_name(self.lastname)
            sname = fix_name(self.lastname, :suffix_only => true)
         end
         if self.firstname?
            fname = fix_name(self.firstname)
            if sname.nil?
               sname = fix_name(self.firstname, :suffix_only => true)
            end
         end
         if self.middlename?
            mname = fix_name(self.middlename, :spaces => true)
            if sname.nil?
               sname = fix_name(self.middlename, :suffix_only => true)
            end
         end
         txt = "#{lname.nil? ? '' : lname + ','}#{fname.nil? ? '' : ' ' + fname}#{mname.nil? ? '' : ' ' + mname}#{sname.nil? ? '' : ' ' + sname}"
         return txt unless is_blank(txt)
         nil
      end
      
      def race_name
         types = {"W" => "White", "B" => "Black", "H" => "Hispanic", "O" => "Other"}
         return types[self.race]
      end
      
      def sex_number
         if self.sex?
            txt = self.sex.clone.gsub(/[^A-Za-z]/, '').strip
            return 1 if txt.upcase == 'M' || txt.upcase == 'MALE'
            return 2 if txt.upcase == 'F' || txt.upcase == 'FEMALE'
         end
         0
      end
   end
   
   class Warrant < ActiveRecord::Base
      def self.type_of(string)
         case string
         when "BENCH"
            return 1
         when "WRIT"
            return 2
         else
            return 0
         end
      end
      
      def self.dispo_of(dispo, dispo_date)
         string = sanitize_string(dispo)
         string = string.strip.upcase unless string.nil?
         date = sanitize_date(dispo_date)
         rem = nil
         
         case string
         when nil, ""
            dispo_string = nil
         when "RETURNED", "RETURND"
            dispo_string = "Returned"
         when "SERVED", "ARRESTED"
            dispo_string = "Served"
         when "REMOVED", "RECALLED"
            dispo_string = "Recalled"
         when "PAID"
            dispo_string = "Paid"
         else
            dispo_string = "Unknown"
         end
         
         if date.nil?
            unless dispo_string.nil?
               date = Date.today
               rem = "Missing Disposition Date was updated today(#{Date.today}) during conversion to new software. -- MFM"
            end
         else
            if dispo_string.nil?
               dispo_string = "Unknown"
               rem = "Missing Disposition was updated today(#{Date.today}) during conversion to new software. -- MFM"
            end
         end
         return [dispo_string, date, rem]
      end
   end

   class Pawn < ActiveRecord::Base
   end
   
   class PawnTemp < ActiveRecord::Base
      def self.date_of(date1,date2)
         # figure out where the ticket date is
         tdate = sanitize_date(date1)
         if tdate.nil?
            tdate = sanitize_date(date2)
         end
         return tdate
      end
      
      def self.split_oln(string)
         unless string.nil?
            txt = string.clone.gsub(/[^A-Za-z0-9 \.,-]/,'').gsub(/[\.,]/,' ').strip
            unless (txt == "NO ID" || txt =~ /^SSN[:-]?\s*\d{3}-?\d{2}-?\d{4}$/ || txt =~ /^\d{3}-\d{2}-\d{4}$/)
               return extract_state(txt)
            end
         end
         return [nil, nil]
      end
      
      def self.ssn_of(ssn_string, oln_string)
         ssn = nil
         if is_blank(ssn_string)
            # may be shown in oln field instead
            unless is_blank(oln_string)
               txt = oln_string.clone.gsub(/[^A-Za-z0-9 \.,-]/,'').gsub(/[\.,]/,' ').strip
               if txt =~ /^SSN[:-]?\s*\d{3}-?\d{2}-?\d{4}$/
                  ssn = txt.sub(/^SSN[:-]?\s*(\d{3})-?(\d{2})-?(\d{4})$/, '\1-\2-\3')
               elsif txt =~ /^\d{3}-\d{2}-\d{4}$/
                  ssn = txt
               end
            end
         else
            txt = ssn_string.clone.gsub(/[^\d-]/,'').strip
            if txt =~ /^\d{9}$/
               txt = txt.sub(/^(\d{3})(\d{2})(\d{4})$/, '\1-\2-\3')
            end
            ssn = txt if txt =~ /^\d{3}-\d{2}-\d{4}$/
         end
         return ssn
      end
      
      def self.item_string_of(itype,imodel,iserial,idesc)
         stype = proper_case(sanitize_string(itype))
         unless is_blank(stype)
            stype = stype.gsub(/[,:]/,'')
         end
         smodel = sanitize_string(imodel)
         smodel = smodel.gsub(/[,:]/,'').upcase unless is_blank(smodel)
         sserial = sanitize_string(iserial)
         sserial = sserial.gsub(/[,:]/,'').upcase unless is_blank(sserial)
         sdesc = sanitize_string(idesc)
         sdesc = sdesc.gsub(/[,:]/,'').upcase unless is_blank(sdesc)
         if is_blank(sdesc)
            unless is_blank(smodel)
               sdesc = smodel
               smodel = nil
            end
         end
         tmp = "#{stype}:#{smodel}:#{sserial}:#{sdesc}"
         return tmp unless is_blank(tmp.gsub(/:/,'').strip)
         return nil
      end
   end
   
   class Call < ActiveRecord::Base
      def self.full_name_of(last,first,middle)
         lname = fname = mname = sname = nil
         unless last.nil?
            lname = fix_name(last)
            sname = fix_name(last, :suffix_only => true)
         end
         unless first.nil?
            fname = fix_name(first)
            if sname.nil?
               sname = fix_name(first, :suffix_only => true)
            end
         end
         unless middle.nil?
            mname = fix_name(middle, :spaces => true)
            if sname.nil?
               sname = fix_name(middle, :suffix_only => true)
            end
         end
         return "#{lname.nil? ? '' : lname + ','}#{fname.nil? ? '' : ' ' + fname}#{mname.nil? ? '' : ' ' + mname}#{sname.nil? ? '' : ' ' + sname}"
      end
      
      def self.split_signal(string)
         # split the signal code from the signal name
         scode = ""
         unless string.nil?
            txt = string.clone.sub(/^(\w+).*$/,'\1').strip
            scode = txt.upcase
         end
         ssig = ""
         unless string.nil?
            txt = string.clone.sub(/^\w+\s+(.*)$/,'\1').strip
            ssig = proper_case(txt)
         end
         return [scode, ssig]
      end
      
      def self.p1_addr2_of(city,state,zip)
         p1_c = ""
         unless city.nil?
            txt = city.clone.gsub(/[^A-Za-z\. ]/,'').strip
            p1_c = proper_case(txt)
         end
         p1_s = sanitize_state(state)
         p1_z = sanitize_zip(zip)
         return "#{p1_c.empty? ? '' : p1_c + ','}#{is_blank(p1_s) ? '' : ' ' + p1_s}#{is_blank(p1_z) ? '' : ' ' + p1_z}"
      end
      
      def self.split_p2address(string)
         p2_addr1 = p2_addr2 = ""
         unless string.nil?
            txt = string.clone.gsub(/[^A-Za-z0-9\., -]/,'').gsub(/\./,' ').strip
            parts = txt.split(',')
            if parts.size > 1
               p2_addr1 = proper_case(parts[0])
            else
               parts = txt.split(' ').compact
               parts.delete(parts.last) unless sanitize_zip(parts.last).nil?
               unless parts.empty?
                  parts.delete(parts.last) unless sanitize_state(parts.last).nil?
               end
               if parts.size > 1
                  parts.delete(parts.last)
               end
               unless parts.empty?
                  p2_addr1 = parts.collect{|p| p.capitalize}.join(' ')
               end
            end
            txt = string.clone.gsub(/[^A-Za-z0-9\., -]/,'').gsub(/\./,' ').strip
            parts = txt.split(',')
            if parts.size > 1
               parts.delete(parts.first)
               txt = parts.join(' ')
            end
            p2_addr2 = proper_case(txt)
         end
         return [p2_addr1, p2_addr2]
      end
      
      def self.build_details(idet, ivehicle, iunits, idispo)
         # put together the details
         det = ""
         unless idet.nil?
            det = idet.clone.gsub(/[^[:print:]\n]/,'').gsub(/"/,"'").strip
            det << "\n\n"
         end
         # add Vehicle
         unless ivehicle.nil?
            txt = ivehicle.clone.gsub(/[^[:print:]\n]/,'').gsub(/"/,"'").strip
            unless is_blank(txt)
               det << "Vehicle: #{txt}\n"
            end
         end
         # add units dispatched (can't parse these for unit field)
         unless iunits.nil?
            txt = iunits.clone.gsub(/[^[:print:]\n]/,'').gsub(/"/,"'").strip
            unless is_blank(txt)
               det << "Units Dispatched: #{txt}\n"
            end
         end
         # add disposition
         unless idispo.nil?
            txt = idispo.clone.gsub(/[^[:print:]\n]/,'').gsub(/"/,"'").strip
            unless is_blank(txt)
               det << "Disposition: #{proper_case(txt)}\n"
            end
         end
         return det
      end
   end
   
   class Criminal < ActiveRecord::Base
      def self.sex_of(string)
         types = {"M" => 1, "F" => 2}
         return types[string]
      end
      
      def self.race_of(string)
         types = {"W" => "White", "B" => "Black", "H" => "Hispanic", "O" => "Other"}
         return types[string]
      end
      
      def self.name_of(last,first,middle)
         lname, sname = fix_name(last, :include_suffix => true)
         if is_blank(sname)
            fname, sname = fix_name(first, :include_suffix => true)
         else
            fname = fix_name(first)
         end
         if is_blank(sname)
            mname, sname = fix_name(middle, :include_suffix => true, :spaces => true)
         else
            mname = fix_name(middle, :spaces => true)
         end
         return sanitize_name("#{fname} #{mname} #{lname} #{sname}")
      end
      
      def self.hair_color_of(string)
         types = {"BLD" => "Bald", "BLK" => "Black", "BLN" => "Blonde", "BRN" => "Brown",
            "GRY" => "Grey", "RED" => "Red", "S&P" => "Salt & Pepper", "WHT" => "White"}
         return types[string]
      end
      
      def self.eye_color_of(string)
         types = {"BLU" => "Blue", "BRN" => "Brown", "GRN" => "Green", "GRY" => "Grey", "HZL" => "Hazel"}
         return types[string]
      end
      
      def self.glasses_of(string)
         types = {"Y" => 1, "N" => 0}
         return types[string]
      end
      
      def self.shoe_type_of(string)
         types = {"00" => "Unknown", "01" => "Rubber Boots", "02" => "Western Work Boots",
            "03" => "Western Pleasure Boots", "04" => "Sandals", "05" => "Tennis Shoes Exp.",
            "06" => "Tennis Shoes Inexp.", "07" => "Dress Shoes", "08" => "Bare Feet",
            "09" => "Others", "10" => "Shower Shoes", "11" => "Work Shoes"}
         return types[string]
      end
      
      def self.hair_type_of(string)
         types = {"A" => "Afro", "B" => "Bald", "C" => "Full Afro", "D" => "Short Afro",
            "E" => "Partial Bald", "L" => "Long", "M" => "Medium", "S" => "Short"}
         return types[string]
      end
      
      def self.complexion_of(string)
         types = {"1" => "Acne", "2" => "Freckled", "3" => "Pale", "4" => "Pocked",
            "5" => "Tanned", "6" => "Ruddy", "7" => "Olive", "8" => "Fair", "9" => "Dark",
            "A" => "Medium", "B" => "Other"}
         return types[string]
      end
      
      def self.facial_hair_of(string)
         types = {"1" => "Mustache", "2" => "Short Beard", "3" => "Long Beard", "4" => "Goatee",
            "5" => "Side Burns", "6" => "Bushy Eyebrows", "7" => "Beard", "8" => "Side Burns/Mustache",
            "9" => "Unshaven", "F" => "Fu-Manchu", "G" => "Beard/Mustache",
            "I" => "Side Burns/Mustache/Bushy Eyebrows", "J" => "Side Burns/Bushy Eyebrows",
            "K" => "Mustache/Goatee"}
         return types[string]
      end
      
      def self.address_of(string)
         city = state = zip = nil
         unless string.nil?
            txt = string.clone.gsub(/[^[:print:]]/, '').strip
            tmp, state = extract_state(txt)
            city, zip = extract_zip(tmp)
         end
         return [proper_case(city), state, zip]
      end
   end
   
   class Person < ActiveRecord::Base
      def self.aliases_for(*args)
         return args.compact.join(", ")
      end
      
      def self.marks_for(*args)
         txt = ""
         tattoos = {:A0 => "Face",:A1 => "Chest",:A2 => "Back", :A3 => "Abdomen",
            :A4 => "Left Arm", :A5 => "Right Arm", :A6 => "Left Hand", :A7 => "Right Hand",
            :A8 => "Left Leg", :A9 => "Right Leg", :B0 => "Buttock", :B2 => "Right Bicep",
            :B3 => "Left Shoulder", :B9 => "Other"}
         scars = {:C0 => "Forehead", :C1 => "Left Side Face", :C2 => "Right Side Face",
            :C3 => "Neck", :C4 => "Chest", :C5 => "Back", :C6 => "Abdomen", :C7 => "Right Arm",
            :C8 => "Left Arm", :C9 => "Right Hand", :D0 => "Left Hand", :D1 => "Buttock",
            :D2 => "Right Leg", :D3 => "Left Leg", :D4 => "Right Foot", :D5 => "Left Foot"}
         args.each do |s|
            type = loc = nil
            next if is_blank(s)
            unless tattoos[s.to_sym].nil?
               type = "Tattoo"
               loc = tattoos[s.to_sym]
            end
            unless scars[s.to_sym].nil?
               type = "Scar"
               loc = tattoos[s.to_sym]
            end
            unless type.nil?
               unless is_blank(txt)
                  txt << ", "
               end
               txt << "#{type}:#{loc}"
            end
         end
         return txt unless is_blank(txt)
         nil
      end
   end
end