#!/usr/bin/ruby
#
#
# Usage:   ruby convert.rb option option ...
# Options:
#          all - process all data files
#        calls - process the callsheet files for callsheets.csv
#      arrests - process the criminal files for arrests.csv
#      forbids - process the forbidden files for forbids.csv
#        pawns - process the pawntickets files for pawntickets.csv
#       people - process the criminal files for people.csv
#     warrants - process the warrants files for warrants.csv
#     bookings - process the jails files for bookings.csv
#         meds - process the meds files for meds.csv
#
# Specials - only used for development
#       params - process params to parameters.csv
#       crimes - process crimes to crimes.csv
#    criminals - process criminals to criminal.csv
#        jails - process bookings to jail.csv
#        drugs - process the medasc to drugs.csv
#    pawnitems - process the pawn file to pawnitems.csv
#    
require 'stringio'
require 'rubygems'
require 'csv'
require 'mysql'
require 'active_record'
require 'fastercsv'
require 'convert_migrations'
require 'converters'
require 'convert_models'
require 'convert_utils'

include Converters
include ConvertModels
include ConvertUtils

# mix fastercsv into activerecord
class ActiveRecord::Base
  def self.to_csv(*args)
    find(:all).to_csv(*args)
  end
  
  def export_columns(format = nil)
    self.class.content_columns.map(&:name) - ['created_at', 'updated_at']
  end
  
  def to_row(format = nil)
    export_columns(format).map { |c| self.send(c) }
  end
end

# mix fastercsv into array
class Array
  def to_csv(options = {})
    if all? { |e| e.respond_to?(:to_row) }
      content_rows = map { |e| e.to_row(options[:format]) }.map(&:to_csv)
      content_rows.join
    else
      FasterCSV.generate_line(self, options)
    end
  end
end

# catch SIGINT
trap("INT"){ Process.exit }

# grab options
files = []
ARGV.each do |arg|
   if arg == 'all'
      files.concat(['params','criminals','crimes','jails','reports','pawnitems','subjects','stuffs','calls','people','arrests','bookings','offenses','evidences','forbids','pawns','warrants'])
   else
      files.push(arg)
   end
end

if files.empty?
   txt = "
Usage: ruby convert.rb [all | <file> ...]
Where <file> is one of:
   calls
   arrests
   forbids
   pawns
   people
   warrants
   bookings
   offenses
   evidences

"
   puts txt
   Process.exit
end

puts "Convert: Start of Run."

# create a temporary database
puts "   Creating Database..."
`mysqladmin create spso_export >/dev/null 2>&1`
ActiveRecord::Base.establish_connection(
   :adapter => "mysql",
   :host => "localhost",
   :database => "spso_export",
   :username => "root",
   :password => "maglerk"
)
ActiveRecord::Migration.verbose = false

# run the migrations
ConvertMigrations.constants.each{|c| ConvertMigrations.class_eval(c).migrate(:up)}

# do the requested conversions
begin
   files.uniq.each do |file|
      case file
      when 'calls'
         Calls.write_csv
      when 'arrests'
         Arrests.write_csv
      when 'crimes'
         Crimes.write_csv
      when 'criminals'
         Criminals.write_csv
      when 'forbids'
         Forbids.write_csv
      when 'offenses'
         Offenses.write_csv
      when 'params'
         Params.write_csv
      when 'pawns'
         Pawns.write_csv
      when 'pawnitems'
         PawnTemps.write_csv
      when 'people'
         People.write_csv
      when 'reports'
         Reports.write_csv
      when 'warrants'
         Warrants.write_csv
      when 'subjects'
         Subjects.write_csv
      when 'jails'
         Jails.write_csv
      when 'bookings'
         Bookings.write_csv
      when 'stuffs'
         Stuffs.write_csv
      when 'evidences'
         Evidences.write_csv
      else
         puts "   Do not know how to process #{file}... skipping!"
      end
   end
ensure
   # drop the temporary database
   puts "   Removing Database..."
   `mysqladmin drop spso_export -f >/dev/null 2>&1`
end
puts "Convert: End of Run."
