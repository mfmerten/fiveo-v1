module ConvertUtils
   def valid_suffix
      ["sr","jr","ii","2nd","iii","3rd","iv","4th","v","5th"]
   end
   
   def is_blank(string)
      return true if string.nil?
      return true if string.clone.strip.empty?
      return false
   end
   
   def fix_name(*opts)
      opt = Hash.new(:spaces => false, :suffix_only => false, :with_suffix => false)
      opt.update(opts.pop) if opts.last.is_a?(Hash)
      string = opts.first
      return nil if is_blank(string)
      txt = string.clone.gsub(/[^\w\s-]/, ' ').strip
      parts = txt.split(' ')
      if parts.size > 0
         if valid_suffix.include?(parts.last.clone.downcase.gsub(/1/,'i'))
            suffix = parts.last.clone.downcase.gsub(/1/,'i')
            if opt[:suffix_only] == true
               return suffix.upcase
            end
            if opt[:with_suffix] == true
               suffix = suffix.upcase
            else
               suffix = nil
            end
            parts.delete(parts.last)
         end
         if opt[:spaces] == true
            name = parts.collect{|p| p.capitalize}.join(' ')
         else
            name = parts.collect{|p| p.capitalize}.join('')
         end
      end
      if opt[:suffix_only] == true
         # if we have not already returned a suffix, there isn't one
         return nil
      end
      name = name.gsub(/[^a-zA-Z\s-]/,'') unless name.nil?
      name = nil if name =~ /^(Nmi|Nmn|None|N A)$/
      if opt[:with_suffix] == true
         return [name,suffix]
      else
         return name
      end
   end

   def proper_case(string)
      # detect first letter in each word and upcase it
      return nil if is_blank(string)
      string.clone.strip.capitalize.gsub(/([^a-zA-Z][a-z])/){|match| match.upcase}
   end
   
   def states
      {
         "Alabama" => "AL",
         "Alaska" => "AK",
         "Arizona" => "AZ",
         "Arkansas" => "AR",
         "California" => "CA",
         "Colorado" => "CO",
         "Conneticut" => "CT",
         "Delaware" => "DE",
         "District Of Columbia" => "DC",
         "Florida" => "FL",
         "Georgia" => "GA",
         "Hawaii" => "HI",
         "Idaho" => "ID",
         "Illinois" => "IL",
         "Indiana" => "IN",
         "Iowa" => "IA",
         "Kansas" => "KS",
         "Kentucky" => "KY",
         "Louisiana" => "LA",
         "Maine" => "ME",
         "Maryland" => "MD",
         "Massachusetts" => "MA",
         "Michigan" => "MI",
         "Minnesota" => "MN",
         "Mississippi" => "MS",
         "Missouri" => "MO",
         "Montana" => "MT",
         "Nebraska" => "NE",
         "Nevada" => "NV",
         "New Hampshire" => "NH",
         "New Jersey" => "NJ",
         "New Mexico" => "NM",
         "New York" => "NY",
         "North Carolina" => "NC",
         "North Dakota" => "ND",
         "Ohio" => "OH",
         "Oklahoma" => "OK",
         "Oregon" => "OR",
         "Pennsylvania" => "PA",
         "Rhode Island" => "RI",
         "South Carolina" => "SC",
         "South Dakota" => "SD",
         "Tennessee" => "TN",
         "Texas" => "TX",
         "Utah" => "UT",
         "Vermont" => "VT",
         "Virginia" => "VA",
         "Washington" => "WA",
         "West Virginia" => "WV",
         "Wisconsin" => "WI",
         "Wyoming" => "WY"
      }
   end
   
   def extract_state(string)
      parts = string.clone.split(/\W+/)
      (0..parts.size-1).each do |p|
         if state = sanitize_state(parts[p])
            parts.delete(parts[p])
            txt = parts.join(" ")
            return [txt,state]
         end
      end
      return [string,nil]
   end
   
   def extract_zip(string)
      parts = string.clone.split(/\W+/)
      (0..parts.size-1).each do |p|
         if zip = sanitize_zip(parts[p])
            parts.delete(parts[p])
            txt = parts.join(" ")
            return [txt, zip]
         end
         if parts[p] =~ /^\d{5}/
            # looks like first part of this is zip code
            zip = parts[p].clone.sub(/^(\d{5}).*$/,'\1')
            parts[p] = parts[p].clone.sub(/^\d{5}(.*)$/,'\1')
            txt = parts.join(" ")
            return [txt,zip]
         end
         if parts[p] =~ /\d{5}$/
            # looks like last part of this is zip code
            zip = parts[p].clone.sub(/^.*(\d{5})$/,'\1')
            parts[p] = parts[p].clone.sub(/^(.*)\d{5}$/,'\1')
            txt = parts.join(" ")
            return [txt,zip]
         end
      end
      return [string,nil]
   end
   
   def sanitize_string(string)
      return nil unless string.is_a?(String)
      return nil if string.clone.strip.empty?
      txt = string.clone.gsub(/[^[:print:]]/,'').gsub(/['"]/,'').strip
      return nil if txt.empty?
      return txt
   end
   
   def sanitize_text(string)
      return nil unless string.is_a?(String)
      return nil if string.clone.strip.empty?
      txt = string.clone.gsub(/[^[:print:]\n\r]/,'').gsub(/['"]/,'').strip
      return nil if txt.empty?
      return txt
   end
   
   def sanitize_name(string)
      return nil unless string.is_a?(String)
      # only letters, spaces, commas and dashes
      txt = string.clone.gsub(/[^A-Za-z ,-]/,'').strip
      # remove dashes at the end or beginning of the line
      txt = txt.gsub(/^-*/,'').gsub(/-*$/,'')
      return proper_case(txt) unless txt.empty?
      nil
   end
   
   def sanitize_address(string)
      return nil unless string.is_a?(String)
      # allow letters, numbers, spaces, commas, dashes, periods, pound
      txt = string.clone.gsub(/[^A-Za-z\d ,\.#-]/,'').strip
      return proper_case(txt) unless txt.empty?
      nil
   end
   
   def sanitize_state(string)
      return nil unless string.is_a?(String)
      # allow letters and space only
      txt = string.clone.gsub(/[^A-Za-z ]/,'').strip
      # matches a state long name? convert to abbrev and return
      return nil if string.empty?
      if states.keys.include?(proper_case(txt))
         return states[proper_case(txt)]
      elsif states.values.include?(txt.upcase)
         return txt.upcase
      end
      nil
   end
   
   def sanitize_zip(string)
      return nil unless string.is_a?(String)
      # allow numbers and dash only
      txt = string.clone.gsub(/[^\d-]/,'').strip
      return txt if txt =~/^\d{5}(-\d{4})?$/
      nil
   end
   
   def sanitize_integer(string)
      return nil unless string.is_a?(String)
      txt = string.clone.gsub(/[^\d\.-]/,'').strip
      return txt.to_i unless txt.empty?
      nil
   end
   
   def sanitize_case(string)
      return nil unless string.is_a?(String)
      txt = string.clone.gsub(/[^\d]/,'').strip
      return nil if txt.to_i == 0
      return txt unless txt.empty?
      nil
   end
   
   def sanitize_date(string,y2k=false)
      return nil unless string.is_a?(String)
      txt = string.clone.gsub(/[^\d\/ :-]/,'').sub(/ 0:00:00$/,'').strip
      # for dates as mmddyyyy
      txt = txt.sub(/^(\d{2})(\d{2})(\d{4})$/,'\1/\2/\3')
      # if time included, strip it off
      txt = txt.sub(/ 0:00:00$/,'')
      # if m is 1 digit, add a 0
      txt = txt.sub(/^(\d\/.*)$/, '0\1')
      # if d is 1 digit, add a 0
      txt = txt.sub(/^(\d{2}\/)(\d\/\d{4})$/, '\10\2')
      if y2k
         # check for 2 digit years and adjust for y2k
         # if y is 2 digits and year is <= this year, add 20
         this_year = Date.today.strftime("%y")
         if txt =~ /^\d{2}\/\d{2}\/\d{2}$/
            date_year = txt.clone.slice(-2,2)
            if date_year <= this_year
               txt = txt.sub(/^(\d{2}\/\d{2}\/)(\d{2})$/,'\120\2')
            else
               txt = txt.sub(/^(\d{2}\/\d{2}\/)(\d{2})$/,'\119\2')
            end
         end
      else
         # adjust 2 digit years and assume 20th century (e.g. for dob)
         # if y is 2 digits, add 19
         txt = txt.sub(/^(\d{2}\/\d{2}\/)(\d{2})$/,'\119\2')
      end
      # if not proper american format now, no hope for it
      return nil unless txt =~ /^\d{2}\/\d{2}\/\d{4}$/
      begin
         value = Date.parse(txt)
      rescue
         value = nil
      end
      return value
   end
   
   def sanitize_time(string)
      return nil unless string.is_a?(String)
      txt = string.clone.gsub(/[^\d\/ :-]/,'').sub(/^\d{1,4}[\/-]\d{1,2}[\/-]\d{1,4} /,'')
      return nil if is_blank(txt)
      if txt =~ /^\d{4}$/
         txt = txt.sub(/^(\d{2})(\d{2})$/, '\1:\2')
      end
      if txt =~ /^\d{1,2}:\d{2}:\d{2}$/
         txt = txt.sub(/^(\d{1,2}:\d{2}):\d{2}$/,'\1')
      end
      if txt =~ /^\d:\d{2}$/
         txt = '0' + txt
      end
      return txt if txt =~ /^\d{2}:\d{2}$/
      nil
   end
   
   def sanitize_ssn(string)
      return nil unless string.is_a?(String)
      txt = string.clone.gsub(/[^\d-]/,'').strip
      return txt if txt =~ /^\d{3}-\d{2}-\d{4}$/
      nil
   end
   
   def sanitize_oln(string)
      return nil unless string.is_a?(String)
      txt = string.clone.gsub(/[^\dA-Za-z -]/,'').strip
      return txt.upcase unless txt.empty?
      nil
   end
   
   def sanitize_decimal(string)
      return nil unless string.is_a?(String)
      txt = string.clone.gsub(/[^\d\.-]/,'').sub(/^0+/,'').strip
      return txt if txt =~ /^\d+(\.\d{0,2})?$/
      nil
   end
   
   def sanitize_phone(string)
      return nil unless string.is_a?(String)
      txt = string.clone.gsub(/[^\d\(\)-]/,'').sub(/^(\d{3})(\d)/, '\1-\2').strip
      return nil if txt.empty?
      if txt =~ /^\d{10}$/
         txt = txt.sub(/^(\d{3})(\d{3})(\d{4})$/,'\1-\2-\3')
      end
      if txt =~ /^\d{7}$/
         txt = txt.sub(/^(\d{3})(\d{4})$/,'318-\1-\2')
      end
      txt = txt.sub(/^(\d{3}-\d{4})$/,'318-\1')
      return nil if txt == '000-000-0000'
      return txt if txt =~ /^\d{3}-\d{3}-\d{4}$/
      nil
   end
end