module ConvertMigrations
   class StuffTable < ActiveRecord::Migration
      def self.up
         create_table :stuffs, :force => true do |t|
            t.string :case_no
            t.string :item_no
            t.string :description
            t.string :location
            t.string :suspect
            t.string :complainant
            t.string :case_type
            t.timestamps
         end
         add_index :stuffs, :case_no
      end
      
      def self.down
         drop_table :stuffs
      end
   end
   
   class EvidenceTable < ActiveRecord::Migration
      def self.up
         create_table :evidences, :force => true do |t|
            t.string :evidence_number
            t.string :case_number
            t.string :description
            t.string :owner
            t.string :remarks
            t.string :location
            t.string :officer
            t.string :officer_badge
            t.string :officer_unit
            t.date :storage_date
            t.string :storage_time
            t.timestamps
         end
      end
      
      def self.down
         drop_table :evidences
      end
   end
   
   class OffenseTable < ActiveRecord::Migration
      def self.up
         create_table :offenses, :force => true do |t|
            t.date :offense_date
            t.string :offense_time
            t.string :case_no
            t.string :location
            t.text :details
            t.string :wrecker_used
            t.integer :wrecker_by_request
            t.string :vehicle_location
            t.string :vehicle_vin
            t.string :vehicle_lic
            t.integer :pictures
            t.integer :restitution_form
            t.integer :victim_notification
            t.integer :perm_to_search
            t.integer :misd_summons
            t.integer :fortyeight_hour
            t.integer :medical_release
            t.string :other_documents
            t.string :officer
            t.string :officer_badge
            t.string :officer_unit
            t.text :remarks
            t.timestamps
         end
         add_index :offenses, :offense_date
         add_index :offenses, :offense_time
      end
      
      def self.down
         drop_table :offenses
      end
   end
   
   class ReportTable < ActiveRecord::Migration
      def self.up
         create_table :reports, :force => true do |t|
            t.string :case_no
            t.date :offense_date
            t.string :offense_time
            t.string :victim
            t.string :victim_address
            t.string :victim_phone
            t.string :location
            t.string :suspect
            t.text :details
            t.integer :pictures
            t.integer :restitution
            t.integer :victim_notify
            t.integer :wrecker
            t.integer :vehicle
            t.integer :medical
            t.integer :hour_48
            t.integer :permission
            t.integer :summons
            t.integer :evidence
            t.string :statements
            t.string :documents
            t.string :officer
            t.timestamps
         end
         add_index :reports, :case_no
         add_index :reports, :offense_date
         add_index :reports, :offense_time
      end
      
      def self.down
         drop_table :reports
      end
   end
   
   class ArrestTable < ActiveRecord::Migration
      def self.up
         create_table :arrests, :force => true do |t|
            t.integer :person
            t.string :case_no
            t.date :arrest_date
            t.string :arrest_time
            t.string :officer1
            t.string :officer1_unit
            t.string :officer1_badge
            t.string :officer2
            t.string :officer2_unit
            t.string :officer2_badge
            t.string :officer3
            t.string :officer3_unit
            t.string :officer3_badge
            t.string :agency
            t.integer :ward
            t.integer :district
            t.string :dwi_officer
            t.string :dwi_officer_unit
            t.string :dwi_officer_badge
            t.string :dwi_results
            t.string :hour_72_judge
            t.date :hour_72_date
            t.string :hour_72_time
            t.string :attorney
            t.string :bond_conditions
            t.string :bond_amt
            t.boolean :bondable
            t.text :remarks
            t.integer :arrest_type
            t.string :charge1
            t.string :charge2
            t.string :charge3
            t.string :charge4
            t.string :charge5
            t.string :charge6
            t.string :charge7
            t.string :charge8
            t.string :charge9
            t.string :charge10
            t.timestamps
         end
      end
      
      def self.down
         drop_table :arrests
      end
   end
   
   class CrimeTable < ActiveRecord::Migration
      def self.up
         create_table :crimes, :force => true do |t|
            t.integer :criminal
            t.date :arrest_date
            t.integer :zone
            t.integer :parish
            t.string :arrest_time
            t.string :charge1
            t.string :charge2
            t.string :charge3
            t.string :charge4
            t.string :charge5
            t.string :charge6
            t.string :charge7
            t.string :charge8
            t.string :charge9
            t.string :charge10
            t.string :officer1
            t.string :officer2
            t.text :remarks
            t.date :hour_72_date
            t.string :case1
            t.string :video
            t.integer :charge1_cnt
            t.integer :charge2_cnt
            t.integer :charge3_cnt
            t.integer :charge4_cnt
            t.integer :charge5_cnt
            t.integer :charge6_cnt
            t.integer :charge7_cnt
            t.integer :charge8_cnt
            t.integer :charge9_cnt
            t.integer :charge10_cnt
            t.integer :ward
            t.string :agency
            t.timestamps
         end
         add_index :crimes, :criminal
         add_index :crimes, :arrest_date
         add_index :crimes, :arrest_time
      end
      
      def self.down
         drop_table :crimes
      end
   end
   
   class ParamTable < ActiveRecord::Migration
      def self.up
         create_table :params, :force => true do |t|
            t.string :category
            t.string :name
            t.string :value
            t.timestamps
         end
         add_index :params, :category
         add_index :params, :name
      end
      
      def self.down
         drop_table :params
      end
   end
   
   class ForbidTable < ActiveRecord::Migration
      def self.up
         create_table :forbids, :force => true do |t|
            t.string :subject
            t.string :sub_street
            t.string :sub_city_state_zip
            t.string :sub_phone
            t.string :complainant
            t.string :comp_street
            t.string :comp_city_state_zip
            t.string :comp_phone
            t.string :case_no
            t.date :requested_date
            t.string :req_officer
            t.string :req_officer_unit
            t.string :req_officer_badge
            t.date :signed_date
            t.string :serv_officer
            t.string :serv_officer_unit
            t.string :serv_officer_badge
            t.date :recalled_date
            t.text :details
            t.text :remarks
            t.timestamps
         end
      end
      
      def self.down
         drop_table :forbids
      end
   end
   
   class SubjectTable < ActiveRecord::Migration
      def self.up
         create_table :subjects, :force => true do |t|
            t.integer :subject
            t.string :lastname
            t.string :middlename
            t.string :firstname
            t.string :street
            t.string :city
            t.string :state
            t.string :zip
            t.string :race
            t.string :sex
            t.date :dob
            t.string :ssn
            t.string :oln
            t.string :oln_state
            t.timestamps
         end
         add_index :subjects, :subject
      end
      
      def self.down
         drop_table :subjects
      end
   end
   
   class WarrantTable < ActiveRecord::Migration
      def self.up
         create_table :warrants, :force => true do |t|
            t.string :warrant_no
            t.integer :warrant_type
            t.string :jurisdiction
            t.string :juris_phone
            t.string :juris_fax
            t.date :received_date
            t.string :charge1
            t.string :charge2
            t.string :charge3
            t.date :issued_date
            t.string :bond_amt
            t.string :payable
            t.string :subject
            t.string :sub_street
            t.string :sub_city
            t.string :sub_state
            t.string :sub_zip
            t.string :sub_ssn
            t.date :sub_dob
            t.string :sub_oln
            t.string :sub_oln_state
            t.string :sub_race
            t.integer :sub_sex
            t.string :disposition
            t.date :dispo_date
            t.text :remarks
            t.timestamps
         end
      end
      
      def self.down
         drop_table :warrants
      end
   end
   
   class PawnTable < ActiveRecord::Migration
      def self.up
         create_table :pawns, :force => true do |t|
            t.string :company
            t.string :ticket_no
            t.date :ticket_date
            t.string :ticket_type
            t.string :name
            t.string :addr1
            t.string :addr2
            t.string :oln
            t.string :oln_state
            t.string :ssn
            t.string :case_no
            t.string :items
            t.text :remarks
            t.timestamps
         end
      end
      
      def self.down
         drop_table :pawns
      end
   end
   
   class PawnTempTable < ActiveRecord::Migration
      def self.up
         create_table :pawn_temps, :force => true do |t|
            t.string :company
            t.string :ticket_no
            t.date :ticket_date
            t.string :ticket_type
            t.string :name
            t.string :addr1
            t.string :addr2
            t.string :oln
            t.string :oln_state
            t.string :ssn
            t.string :case_no
            t.string :items
            t.text :remarks
            t.timestamps
         end
         add_index :pawn_temps, :company
         add_index :pawn_temps, :ticket_no
         add_index :pawn_temps, :name
      end

      def self.down
         drop_table :pawn_temps
      end
   end
   
   class CallTable < ActiveRecord::Migration
      def self.up
         create_table :calls, :force => true do |t|
            t.string :case_no
            t.string :signal_code
            t.string :signal
            t.string :call_via
            t.string :received_by
            t.string :received_by_badge
            t.string :received_by_unit
            t.date :call_date
            t.string :call_time
            t.integer :ward
            t.integer :district
            t.string :person1_type
            t.string :person1_name
            t.string :person1_addr1
            t.string :person1_addr2
            t.string :person1_home_phone
            t.string :person1_cell_phone
            t.string :person2_type
            t.string :person2_name
            t.string :person2_addr1
            t.string :person2_addr2
            t.string :person2_home_phone
            t.string :person2_cell_phone
            t.text :details
            t.string :units
            t.date :dispatch_date
            t.string :dispatch_time
            t.date :arrival_date
            t.string :arrival_time
            t.date :concluded_date
            t.string :concluded_time
            t.string :disposition
            t.string :dispatcher
            t.string :dispatcher_badge
            t.string :dispatcher_unit
            t.string :detective
            t.string :detective_badge
            t.string :detective_unit
            t.string :remarks
            t.timestamps
         end
      end
      
      def self.down
         drop_table :calls
      end
   end
   
   class CriminalTable < ActiveRecord::Migration
      def self.up
         create_table :criminals, :force => true do |t|
            t.integer :person
            t.string :lastname
            t.string :firstname
            t.string :middlename
            t.string :suffix
            t.integer :sex
            t.string :race
            t.date :dob
            t.integer :weight
            t.integer :height_ft
            t.integer :height_in
            t.string :pob
            t.string :ssn
            t.string :oln_state
            t.string :oln
            t.string :alias1
            t.string :alias2
            t.string :alias3
            t.string :alias4
            t.text :remarks
            t.string :hair_color
            t.string :eye_color
            t.integer :glasses
            t.string :shoe_size
            t.string :shoe_type
            t.string :scar1
            t.string :scar2
            t.string :scar3
            t.string :scar4
            t.string :scar5
            t.string :hair_type
            t.string :complexion
            t.string :facial_hair
            t.string :street
            t.string :city
            t.string :state
            t.string :zip
            t.string :occupation
            t.string :employer
            t.string :sid
            t.string :fbi
            t.string :home_phone
            t.timestamps
         end
         add_index :criminals, :person
      end
      
      def self.down
         drop_table :criminals
      end
   end
   
   class PersonTable < ActiveRecord::Migration
      def self.up
         create_table :people, :force => true do |t|
            t.integer :person
            t.string :lastname
            t.string :firstname
            t.string :middlename
            t.string :suffix
            t.string :aliases
            t.string :phys_street
            t.string :phys_city
            t.string :phys_state
            t.string :phys_zip
            t.string :mail_street
            t.string :mail_city
            t.string :mail_state
            t.string :mail_zip
            t.date :dob
            t.string :pob
            t.string :race
            t.integer :sex
            t.string :fbi
            t.string :sid
            t.string :ssn
            t.string :oln
            t.string :oln_state
            t.string :home_phone
            t.string :cell_phone
            t.string :email
            t.string :build
            t.integer :height_ft
            t.integer :height_in
            t.integer :weight
            t.string :hair_color
            t.string :hair_type
            t.string :eye_color
            t.integer :glasses
            t.string :shoe_size
            t.string :complexion
            t.string :facial_hair
            t.string :occupation
            t.string :employer
            t.string :em_contact
            t.string :em_relationship
            t.string :em_address
            t.string :em_phone
            t.string :drug_allergies
            t.integer :hep
            t.integer :hiv
            t.integer :tub
            t.integer :deceased
            t.string :gang
            t.text :remarks
            t.string :doc_number
            t.string :marks
            t.timestamps
         end
         add_index :people, :person
      end
      
      def self.down
         drop_table :people
      end
   end
   
   class JailTable < ActiveRecord::Migration
      def self.up
         create_table :jails, :force => true do |t|
            t.integer :inmate
            t.date :booking_date
            t.string :property1
            t.string :property2
            t.string :cash_deposited
            t.string :cash_retained
            t.string :booking_time
            t.string :attorney
            t.string :em_notify
            t.string :visitor1
            t.string :visitor2
            t.string :visitor3
            t.string :medication
            t.string :holds
            t.string :jail
            t.string :cell
            t.date :sched_release_date
            t.date :actual_release_date
            t.string :release_time
            t.string :release_cause
            t.string :phone_called
            t.string :remarks
            t.string :booking_officer
            t.date :arrest_date
            t.timestamps
         end
         add_index :jails, :inmate
         add_index :jails, :booking_date
         add_index :jails, :booking_time
         add_index :jails, :actual_release_date
      end
      
      def self.down
         drop_table :jails
      end
   end
   
   class BookingTable < ActiveRecord::Migration
      def self.up
         create_table :bookings, :force => true do |t|
            t.integer :person
            t.date :booking_date
            t.string :booking_time
            t.string :cash_at_booking
            t.string :booking_officer
            t.string :booking_officer_badge
            t.string :booking_officer_unit
            t.string :facility
            t.string :cell
            t.string :phone_called
            t.integer :intoxicated
            t.date :scheduled_release_date
            t.date :actual_release_date
            t.string :actual_release_time
            t.integer :afis
            t.string :release_officer
            t.string :release_officer_badge
            t.string :release_officer_unit
            t.string :fingerprint
            t.string :dna
            t.string :attorney
            t.string :em_notify
            t.string :em_phone
            t.text :remarks
            t.string :visitors
            t.string :property
            t.timestamps
         end
         add_index :bookings, :person
         add_index :bookings, :booking_date
         add_index :bookings, :booking_time
      end
      
      def self.down
         drop_table :bookings
      end
   end
end