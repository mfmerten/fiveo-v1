# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 30) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.agency}", :align => :center, :size => 20, :style => :bold
      pdf.move_up(16)
      pdf.text format_date(@report_date), :align => :right, :size => 14, :style => :bold
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(@report_date, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "#{SiteConfig.motto}", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 45], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 75) do
   pdf.text @page_title, :align => :center, :size => 24, :style => :bold
   pdf.move_down(8)
   # Build an array and let prawn page the results
   data = []
   @bonds.each do |bond|
     surety = ''
     if bond.bondsman.nil?
        if bond.surety.nil?
           surety = "UNKNOWN"
        else
           surety = bond.surety.full_name
        end
     else
        surety = bond.bondsman.name
     end
     data.push([bond.id, format_date(bond.issued_date), show_person(bond.person), bond.arrest_id, "#{bond.bond_type.nil? ? 'UNK' : bond.bond_type.long_name}", surety, number_to_currency(bond.bond_amt), "#{bond.status.nil? ? ' ' : bond.status.long_name}"])
   end
   pdf.table data, :align => :left, :column_widths => {0 => 55, 1 => 65, 2 => 150, 3 => 55, 4 => 100, 5 => 150, 6 => 75, 7 => 100}, :border_width => 1, :border_style => :underline_header, :headers => ["ID", "Date", "Person", "Arrest", "Type", "Surety", "Amt", "Status"], :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], :vertical_padding => 2, :position => 0
   pdf.move_down(8)
   pdf.text "END OF REPORT", :style => :bold
end

pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
