# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat(:all) do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font("Helvetica")
      pdf.text("#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold)
      pdf.text("#{SiteConfig.header_subtitle1}", :align => :center, :size => 14)
      pdf.text("#{SiteConfig.header_subtitle2}", :align => :center, :size => 10)
      pdf.text("#{SiteConfig.header_subtitle3}", :align => :center, :size => 10)
      pdf.move_up(62)
      pdf.image("#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left)
      pdf.move_up(75)
      pdf.image("#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right)
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font("Times-Roman")
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font("Helvetica")
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text("NO: #{@bond.id}", :align => :left, :size => 12, :style => :bold)
      pdf.move_up(14)
      pdf.text("'#{SiteConfig.footer_text}'", :align => :center, :size => 12)
      pdf.font("Times-Roman")
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text("Criminal Bond", :align => :center, :size => 20, :style => :bold)
   pdf.move_up(5)
   pdf.text("#{show_person(@bond.person)}", :size => 14, :style => :bold, :align => :left)
   pdf.move_up(16)
   pdf.text("Bond Number: #{@bond.bond_no}", :size => 14, :align => :right)
   pdf.text("Arrest ID: #{@bond.arrest.nil? ? 'Invalid Arrest' : @bond.arrest.id}      Race: #{@bond.person.nil? ? '' : (@bond.person.race.nil? ? '' : @bond.person.race.short_name)}      Sex: #{@bond.person.nil? ? '' : @bond.person.sex_name(true)}      DOB: #{@bond.person.nil? ? '' : format_date(@bond.person.date_of_birth)}      SSN: #{@bond.person.nil? ? '' : @bond.person.ssn}", :size => 12, :align => :center)
   pdf.move_down(15)
   pdf.text("STATE OF LOUISIANA", :style => :bold, :align => :left, :size => 14)
   pdf.move_up(16)
   pdf.text("11TH JUDICIAL DISTRICT COURT", :style => :bold, :align => :right, :size => 14)
   pdf.text("PARISH OF SABINE", :style => :bold, :align => :left, :size => 14)
   pdf.move_down(10)
   if @bond.bondsman.nil?
      if @bond.surety.nil?
         surety = "UNKNOWN"
      else
         surety = @bond.surety.full_name
         surety_line1 = @bond.surety.mailing_line1
         surety_line2 = @bond.surety.mailing_line2
         surety_line3 = ""
      end
   else
      surety = @bond.bondsman.company
      surety_line1 = @bond.bondsman.name
      surety_line2 = @bond.bondsman.address1
      surety_line3 = @bond.bondsman.address2
   end
   pdf.text("KNOWN ALL MEN BY THESE PRESENTS, that we   #{show_person(@bond.person, :include_id => false).upcase}   as principal (defendent) and   #{surety.blank? ? '____________________________' : surety.upcase}   as surety, are bound in solido, and acknowledge ourselves to be indebted unto the State of Louisiana, Parish of Sabine, in the sum of   #{number_to_currency(@bond.bond_amt)}   Dollars for the faithful payment of which we bind ourselves, our heirs, executors and assigns, and in faith whereof we have signed these presents this date   #{format_date(@bond.issued_date)}   at Many, Sabine Parish, Louisiana.", :size => 12)
   pdf.move_down(5)
   pdf.text("#{@bond.person.nil? ? '' : @bond.person.full_name.upcase}, having been arrested for the crime of:", :size => 12)
   pdf.move_down(5)
   pdf.span(pdf.bounds.right - 15, :position => 15) do
      if !@bond.hold.nil? && !@bond.hold.hold_type.nil? && @bond.hold.hold_type.long_name =~ /^Probation/
        pdf.text(@bond.hold.hold_type.long_name, :size => 12)
      else
        unless @bond.arrest.nil?
          if @bond.arrest.arrest_type == 0
            @bond.arrest.district_charges.each do |c|
              pdf.text("#{c.charge}#{c.arrest_warrant.nil? ? '' : '(Warrant #' + c.arrest_warrant.warrant_no + ')'}", :size => 12)
            end
          elsif @bond.arrest.arrest_type == 1
            @bond.arrest.city_charges.each do |c|
              pdf.text("#{c.charge}#{c.agency.nil? ? '' : ' (' + c.agency.fullname + ')'}#{c.arrest_warrant.nil? ? '' : '(Warrant #' + c.arrest_warrant.warrant_no + ')'}")
            end
          elsif @bond.arrest.arrest_type == 2
            @bond.arrest.other_charges.each do |c|
              pdf.text("#{c.charge}#{c.arrest_warrant.nil? ? '' : (c.arrest_warrant.jurisdiction.nil? ? '' : ' (' + c.arrest_warrant.jurisdiction.fullname + ')')}#{c.arrest_warrant.nil? ? '' : '(Warrant #' + c.arrest_warrant.warrant_no + ')'}")
            end
          end
        end
      end
   end
   pdf.move_down(5)
   pdf.text("and having been admitted to bail in the aforesaid amount, we hereby undertake that the above named principal will appear at all stages of the proceedings in the ELEVENTH JUDICIAL DISTRICT COURT to answer that charge or any related charge, and will at all times hold himself amenable to the orders and process of the Court, and if convicted will appear for the pronouncement of the verdict and sentence, and will not leave the State without written permission of the Court, and that if he fails to perform any of these conditions, we or I will pay unto the State of Louisiana the aforesaid amount.", :size => 12)
   pdf.move_down(25)
   pdf.stroke_horizontal_line(pdf.col(2,1), pdf.col(2,1) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,1)) do
      pdf.text("Signature of Surety", :size => 9, :style => :italic)
   end
   pdf.move_up(10)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text("Signature of Principal", :size => 9, :style => :italic)
   end
   pdf.move_down(5)
   pos = pdf.y
   pdf.span(pdf.col(2), :position => pdf.col(2,1)) do
      pdf.text(surety, :size => 12)
      pdf.text(surety_line1, :size => 12)
      pdf.text(surety_line2, :size => 12)
      pdf.text(surety_line3, :size => 12)
   end
   pdf.y = pos
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text("#{show_person(@bond.person, :include_id => false)} ", :size => 12)
      pdf.text("#{@bond.person.nil? ? '' : @bond.person.mailing_line1} ", :size => 12)
      pdf.text("#{@bond.person.nil? ? '' : @bond.person.mailing_line2} ", :size => 12)
      pdf.text(" ", :size => 12)
   end
   
   pdf.pad(10){ pdf.stroke_horizontal_rule }

   pdf.text("STATE OF LOUISIANA", :style => :bold, :align => :left, :size => 14)
   pdf.move_up(16)
   pdf.text("PARISH OF SABINE", :style => :bold, :align => :right, :size => 14)
   pdf.move_down(10)
   pdf.text("Personally appeared before me the undersigned authority. Who, being by me duly sworn in, did depose and say that he is a citizen and resident of the State of Louisiana that after the payment of all his debts he is owner in his own right of property, real or personal or both, liable to seizure in an amount equal to the sum named in the above bond, that he is not an attorney at law, a judge, or ministerial officer of my court, and that he is not a surety on any undischarged bail bonds for which the below described property was used as surety.", :size => 12, :style => :bold)
   pdf.move_down(5)
   pdf.text("Legal Description of Property Assigned:", :size => 12)
   pdf.move_down(5)
   pdf.text("(SEE ATTACHED)", :size => 12, :align => :center)
   pdf.move_down(10)
   pdf.text("LOUISIANA REVISED STATUTE 14:351:", :size => 9)
   pdf.span(pdf.bounds.right - 15, :position => 15) do
      pdf.text("No person shall, with intent to defraud, sell, transfer, donate, give, mortgage, hypothecate, or in any way encumber in the predjudice of the State any real estate offered as security to the State on any bail or appearance bond for the release of any person charged with crime.  Whoever violates this Section shall be imprisoned with or without hard labor for not less than six months nor more than twelve months.", :size => 9)
   end
   pdf.move_down(25)
   pdf.stroke_horizontal_line(pdf.col(2,1), pdf.col(2,1) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,1)) do
      pdf.text("Signature of Witness", :size => 9, :style => :italic)
   end
   pdf.move_up(10)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text("Signature of Surety", :size => 9, :style => :italic)
   end
   pdf.move_down(25)
   pdf.stroke_horizontal_line(pdf.col(2,1), pdf.col(2,1) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,1)) do
      pdf.text("Signature of Deputy Sheriff", :size => 9, :style => :italic)
   end
   pdf.move_up(10)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text("Signature of Surety", :size => 9, :style => :italic)
   end
   
   pdf.move_down(25)
   pdf.text("Sworn To and Before Me Subscribed This ________ Day Of _____________________________")
   pdf.move_down(25)
   
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text("NOTARY", :size => 9, :style => :italic)
   end
   pdf.move_up(14)
   pdf.text("Bond Date/Time: #{format_date(@bond.issued_date, @bond.issued_time)}")
   pdf.move_down(5)
   pdf.text("Conditions of Bond: #{@bond.arrest.nil? ? "" : (@bond.arrest.condition_of_bond? ? @bond.arrest.condition_of_bond.upcase : '')}")
end
