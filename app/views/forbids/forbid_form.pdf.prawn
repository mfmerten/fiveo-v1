# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@forbid.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text @page_title, :align => :center, :size => 20, :style => :bold
   pdf.move_down(2)
   pdf.text format_date(@forbid.request_date), :size => 14, :style => :bold, :align => :center
   pdf.text "Case Number: #{@forbid.case_no}", :size => 14, :style => :bold, :align => :center
   data = [
      ["FROM:","#{@forbid.complainant}","TO:","#{@forbid.full_name}"],
      ["","#{@forbid.comp_street}","","#{@forbid.forbid_street}"],
      ["","#{@forbid.comp_city_state_zip}","","#{@forbid.forbid_city_state_zip}"]]
   cols = pdf.bounds.width / 6
   cols = cols - 1
   pdf.table data, :border_width => 0, :column_widths => {0 => cols, 1 => cols * 2, 2 => cols, 3 => cols * 2}, :align => {0 => :right, 2 => :right}
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   pdf.text "This letter is to serve as your one and only notice from #{@forbid.complainant.upcase} that from hence forth, you are forbidden to go into or upon or remain in or upon the property(ies) and/or building(s) of #{@forbid.complainant.upcase} located at:", :size => 14
   pdf.move_down(10)
   pdf.span(pdf.bounds.width - 50, :position => :center) do
      pdf.text "#{@forbid.details}", :size => 14
   end
   pdf.move_down(10)
   pdf.text "This is pursuant to LSA - R.S. 14:63.3 ENTRY ON OR REMAINING IN PLACES OR ON LAND AFTER BEING FORBIDDEN. Failure of the unwanted person to comply with this written notice will result in the complainant, listed herein, seeking a warrant of arrest for the unwanted person along with any other charges should he/she choose to ignore this notice and return to the above listed property(ies) and/or building(s).", :size => 14
   pdf.move_down(40)
   pdf.stroke_horizontal_line(0,250)
   pdf.move_up(1)
   pdf.stroke_horizontal_line(275, pdf.bounds.right)
   pdf.text "Complainant's Signature", :size => 10, :align => :left
   pdf.move_up(12)
   pdf.text "Unwanted Person's Signature", :size => 10, :align => :right
   pdf.move_down(40)
   pdf.stroke_horizontal_line(0,250)
   pdf.move_up(1)
   pdf.stroke_horizontal_line(275, pdf.bounds.right)
   pdf.text "Signature of Deputy Serving Notice", :size => 10, :align => :left
   pdf.move_up(12)
   pdf.text "Date and Time Notice Served On Above", :size => 10, :align => :right
end



 
