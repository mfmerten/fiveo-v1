# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 24) do
      pdf.font "Helvetica"
      pdf.text @page_title, :align => :center, :size => 16, :style => :bold
      pdf.move_up(14)
      pdf.text format_date(@report_time), :align => :right, :size => 12, :style => :bold
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@timestamp}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "#{SiteConfig.agency}", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 35], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 60) do
   grand_charges = grand_payments = grand_net = BigDecimal.new("0.00",2)
   @funds.each do |f|
      total_charges = total_payments = total_net = BigDecimal.new("0.00",2)
      data = []
      @payments.each do |p|
         person_name = " "
         unless p.probation.nil?
            unless p.probation.person.nil?
               person_name = "#{p.probation.person.full_name}"
            end
         end
         (1..9).each do |x|
            if p.send("fund#{x}_name") == f && (p.send("fund#{x}_charge") != 0 || p.send("fund#{x}_payment") != 0)
               pay = p.send("fund#{x}_payment")
               if pay.nil?
                  pay = BigDecimal.new("0.00",2)
               end
               chg = p.send("fund#{x}_charge")
               if chg.nil?
                  chg = BigDecimal.new("0.00",2)
               end
               total_charges += chg
               total_payments += pay
               total_net += (chg - pay)
               data.push([p.id, p.probation_id, format_date(p.transaction_date), person_name, p.memo, p.receipt_no, number_to_currency(chg, :unit => ''), number_to_currency(-pay, :unit => ''), number_to_currency(total_net, :unit => '')])
            end
         end
      end
      unless data.empty?
         if pdf.y < 100
            pdf.start_new_page
         elsif f != @funds.first
            pdf.move_down(10)
         end
         pdf.text "#{f}", :size => 12, :style => :bold
         pdf.table data, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 55, 1 => 55, 2 => 65, 3 => 175, 4 => 95, 5 => 70, 6 => 75, 7 => 75, 8 => 85}, :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :left, 5 => :left, 6 => :right, 7 => :right, 8 => :right}, :headers => ["Trans","Prob", "Date", "Person", "Memo", "Receipt No", "Charge", "Credit", "Net"], :font_size => 11, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :vertical_padding => 1
      end
      grand_charges += total_charges
      grand_payments += total_payments
   end
   
   grand_net = grand_charges - grand_payments
   if pdf.y < 100
      pdf.start_new_page
   else
      pdf.move_down(10)
   end
   pdf.stroke_horizontal_rule
   pdf.move_down(2)
   pdf.stroke_horizontal_rule
   pdf.move_down(5)
   pdf.text "Grand Totals:", :size => 14, :style => :bold
   pdf.move_up(14)
   data = [[" "," "," "," "," "," ",number_to_currency(grand_charges, :unit => ''), number_to_currency(-grand_payments, :unit => ''), number_to_currency(grand_net, :unit => '')]]
   pdf.table data, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 55, 1 => 55, 2 => 65, 3 => 175, 4 => 95, 5 => 70, 6 => 75, 7 => 75, 8 => 85}, :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :left, 5 => :left, 6 => :right, 7 => :right, 8 => :right}, :font_size => 12, :vertical_padding => 1
   pdf.move_down(5)
   pdf.text "---END OF REPORT---", :size => 12
end

pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]

