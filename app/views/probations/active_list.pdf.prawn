# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@timestamp}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Probation Listing", :align => :center, :size => 24, :style => :bold
   pdf.text format_date(@report_datetime), :align => :center, :size => 14, :style => :bold
   pdf.move_down(8)
   current_person = nil
   data = []
   @probations.each do |p|
      if p.person_id != current_person
         # print the prior person's data
         unless data.empty?
            pdf.table data, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 75, 1 => 300, 2 => 65, 3 => 65}, :align => :left, :headers => ["Prob ID", "Conviction", "Start", "End"], :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2, :position => :center
         end
         current_person = p.person_id
         data = []
         if pdf.y < 100
            pdf.start_new_page
         else
            pdf.move_down(4)
            pdf.stroke_horizontal_rule
         end
         pdf.move_down(5)
         pdf.text "#{show_person p.person} -- DoB: #{format_date(p.person.date_of_birth)}", :size => 14, :style => :bold
         alt_txt = ""
         if !p.person.wanted.empty?
           alt_txt << "[WANTED] "
         end
         unless p.docket.nil?
           alt_txt << "#{p.docket.docket_no}"
         end
         unless alt_txt.blank?
            pdf.move_up(16)
            pdf.text alt_txt, :size => 14, :style => :bold, :align => :right
         end
      end
      data.push([p.id, "#{p.conviction} ","#{format_date(p.start_date)} ","#{format_date(p.end_date)} "])
   end
   unless data.empty?
      pdf.table data, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 75, 1 => 300, 2 => 65, 3 => 65}, :align => :left, :headers => ["Prob ID", "Conviction", "Start", "End"], :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2, :position => :center
   end
   pdf.move_down(8)
   pdf.text "END OF REPORT", :size => 12, :style => :bold
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
