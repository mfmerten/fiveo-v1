# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
@report_datetime = DateTime.now

pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(@report_datetime, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

def page_header(pdf)
   if !@person.nil?
      pdf.text "Commissary: #{show_person @person}", :align => :center, :size => 20, :style => :bold
   elsif @facility.nil?
      pdf.text "Commissary: All Facilities", :align => :center, :size => 20, :style => :bold
   else
      pdf.text "Commissary: #{@facility.long_name}", :align => :center, :size => 20, :style => :bold
   end
   pdf.text @page_title, :align => :center, :size => 16, :style => :bold
   pdf.move_down(5)
   pdf.text format_date(@report_datetime), :size => 12, :style => :bold, :align => :center
   pdf.move_down(5)
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   # transaction listing
   page_header(pdf)
   data = []
   running_balance = BigDecimal.new('0.0',2)
   @commissaries.each do |c|
      running_balance += (c.deposit - c.withdraw)
      data.push([c.id,format_date(c.transaction_date),c.person_id,c.category.long_name,c.memo,c.receipt_no, number_to_currency(c.deposit > 0 ? c.deposit : -c.withdraw), number_to_currency(running_balance)])
   end
   data.push(['','','','','','','Net Change =>',number_to_currency(running_balance)])
   pdf.table data, :border_width => 1, :border_style => :underline_header, :width => pdf.bounds.width, :headers => ["ID", "Date/Time", "Inmate", "Type","Memo","Receipt", "Amount", "Net Change"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :left, 5 => :left, 6 => :right, 7 => :right}, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2, :position => :center

   # per-inmate breakdown
   grand_withdraw = BigDecimal.new('0.0',2)
   grand_deposit = BigDecimal.new('0.0',2)
   inmates = 0
   num_dep = 0
   num_wit = 0
   categories = Hash.new
   get_opts("Commissary Type").each do |c|
      categories[c[0]] = 0
   end
   # do the first header
   pdf.start_new_page
   page_header(pdf)
   pdf.text "Breakdown By Inmate", :size => 16, :style => :bold, :align => :center
   pdf.move_down(5)
   if @people.empty?
      pdf.move_down(40)
      pdf.text "No Inmates For Selected Facility", :align => :center, :size => 14, :style => :bold
   else
      # add a section for each person
      @people.each do |p|
         commissaries = p.commissary_transactions(@start_date,@stop_date)
         unless commissaries.empty?
            inmates += 1
            # get balance on day prior to start of transactions
            begin_bal = p.commissary_balance(@start_date - 1.day)
            data = []
            data.push(['','','','','','Begin =>',number_to_currency(begin_bal)])
            commissaries.each do |c|
               categories[c.category.long_name] += 1
               if c.deposit?
                  num_dep += 1
                  begin_bal += c.deposit
                  grand_deposit += c.deposit
               end
               if c.withdraw?
                  num_wit += 1
                  begin_bal -= c.withdraw
                  grand_withdraw -= c.withdraw
               end
               data.push([c.id, format_date(c.transaction_date), c.category.long_name, c.memo, c.receipt_no, number_to_currency(c.deposit > 0 ? c.deposit : -c.withdraw), number_to_currency(begin_bal)])
            end
            data.push(['','','','','','End =>',number_to_currency(begin_bal)])
            # do we need a new page?
            if pdf.y < 100 # estimate... adjust as needed
               pdf.start_new_page
            end
            pdf.text "#{show_person(p)}", :size => 12, :style => :bold
            pdf.move_down(5)
            pdf.table data, :border_width => 1, :border_style => :underline_header, :width => pdf.bounds.width, :headers => ["ID", "Date/Time", "Type","Memo","Receipt", "Amount", "Balance"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :left, 5 => :right, 6 => :right}, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2, :position => :center
            pdf.move_down(10)
         end
      end
   end
   
   # do summary page
   pdf.start_new_page
   page_header(pdf)
   pdf.text "Report Summary", :size => 18, :style => :bold, :align => :center
   pdf.move_down(10)
   tot_trans = num_wit + num_dep
   net_change = grand_deposit + grand_withdraw
   data = [["Deposits:",num_dep,number_to_currency(grand_deposit)],
      ["Withdrawals:",num_wit,number_to_currency(grand_withdraw)],
      ["Net Change:",'',number_to_currency(net_change)]]
   pdf.table data, :border_width => 1, :align => :right, :font_size => 14, :headers => [" ","Number","Total"], :position => :center, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
   pdf.move_down(10)
   pdf.text "Totals", :size => 18, :style => :bold, :align => :center
   data = [["Inmates:",inmates],
      ["Transactions:",tot_trans]]
   pdf.table data, :border_width => 1, :align => :right, :font_size => 14, :position => :center, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
   pdf.move_down(10)
   pdf.text "Totals by Category", :size => 18, :style => :bold, :align => :center
   data = []
   categories.keys.each do |c|
      data.push(["#{c}:",categories[c]])
   end
   pdf.table data, :border_width => 1, :align => :right, :font_size => 14, :position => :center, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
