# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
this_instant = DateTime.now
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(this_instant, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Inmate Account Balances", :align => :center, :size => 20, :style => :bold
   pdf.move_down(5)
   if @facility.blank?
      pdf.text 'All Facilities', :align => :center, :size => 18, :style => :bold
   else
      if o = Option.find_by_id(@facility)
         pdf.text o.long_name, :align => :center, :size => 18, :style => :bold
      else
         pdf.text 'Unknown Facility', :align => :center, :size => 18, :style => :bold
      end
   end
   pdf.move_down(5)
   pdf.text format_date(this_instant), :size => 12, :style => :bold, :align => :center
   pdf.move_down(5)
   end_balance = BigDecimal.new('0',2)
   data = []
   @trans.each do |t|
      p = Person.find_by_id(t.person_id)
      if p.bookings.empty?
         b = nil
      else
         b = p.bookings.last
      end
      if @facility.blank? || p.commissary_facility_id == @facility.to_i
         end_balance += t.balance.to_d
         data.push([p.id, p.full_name, "#{b.nil? ? 'No Bookings' : (b.release_date? ? 'Released ' + format_date(b.release_date) : 'Active')} #{b.nil? ? '' : b.release_time}", number_to_currency(t.balance.to_d)])
      end
   end
   if data.empty?
      pdf.move_down(40)
      pdf.text "Nothing To Report", :size => 14, :style => :bold, :align => :center
      pdf.move_down(40)
   else
      data = data.sort{|a,b| a[1] <=> b[1]}
      data.push(['','','Total =>',number_to_currency(end_balance)])
      pdf.table data, :border_width => 1, :border_style => :underline_header, :headers => ["ID", "Inmate", "Booking Status", "Balance"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :right}, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2, :width => pdf.bounds.width
   end
   pdf.text "END OF REPORT", :style => :bold
   pdf.move_up(13)
   pdf.text "Total Balance: #{number_to_currency(end_balance)}", :style => :bold, :align => :right
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
