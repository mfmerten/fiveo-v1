# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(@report_time, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   # print withdrawals first
   withdraw_count = @withdrawals.size
   withdraw_total = BigDecimal.new('0',2)
   account_total = BigDecimal.new('0',2)
   account_count = 0
   if @status == 'post' || @status == 'reprint'
      pdf.text "Commissary Posting Report", :align => :center, :size => 20, :style => :bold
   else
      pdf.text "Commissary Posting Report (PREVIEW)", :align => :center, :size => 20, :style => :bold
   end
   if @facility.nil? || !(o = Option.find_by_id(@facility))
      pdf.move_down(5)
      pdf.text "All Facilities", :align => :center, :size => 18, :style => :bold
   else
      pdf.move_down(5)
      pdf.text o.long_name, :align => :center, :size => 18, :style => :bold
   end
   pdf.text format_date(@report_time), :size => 12, :style => :bold, :align => :center
   pdf.move_down(10)
   pdf.text "Withdrawals", :size => 20, :style => :bold, :align => :center
   pdf.move_down(5)
   
   data = []
   if @withdrawals.empty?
      pdf.move_down(40)
      pdf.text "No Withdrawals To Report", :size => 12, :style => :bold, :align => :center
   else
      trans = nil
      until @withdrawals.empty?
         if trans.blank? || trans.person_id != @withdrawals[0].person_id
            data.push(["#{show_person @withdrawals[0].person}",
               "(#{@withdrawals[0].person.commissary_facility.nil? ? '' : @withdrawals[0].person.commissary_facility.short_name})",'','',''])
         end
         trans = @withdrawals.shift
         data.push([format_date(trans.transaction_date),trans.memo,trans.receipt_no,(trans.category.nil? ? 'Invalid' : trans.category.long_name),number_to_currency(-trans.withdraw)])
         if @status == 'post'
            trans.update_attribute(:report_datetime, @report_time)
         end
         withdraw_total -= trans.withdraw
         account_total -= trans.withdraw
         account_count += 1
         if @withdrawals.empty? || @withdrawals[0].person_id != trans.person_id
            data.push(['','','','***Account Total:',number_to_currency(account_total)])
            data.push([' ','','','',''])
            account_total = BigDecimal.new('0',2)
            account_count = 0
         end

         if @withdrawals.empty?
            pdf.table data, :border_width => 0, :headers => ["Date", "Memo", "Receipt", "Type", "Amount"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :right}, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2, :width => pdf.bounds.width
            pdf.move_down(5)
            pdf.text "Transactions: #{withdraw_count}  Withdraw Total: #{number_to_currency(withdraw_total)}", :size => 14, :style => :bold, :align => :right
         end
      end
   end
   
   # next, do the deposits
   deposit_count = @deposits.size
   pdf.start_new_page
   deposit_total = BigDecimal.new('0',2)
   account_total = BigDecimal.new('0',2)
   if @status == 'post' || @status == 'reprint'
      pdf.text "Commissary Posting Report", :align => :center, :size => 20, :style => :bold
   else
      pdf.text "Commissary Posting Report (PREVIEW)", :align => :center, :size => 20, :style => :bold
   end
   if @facility.nil? || !(o = Option.find_by_id(@facility))
      pdf.move_down(5)
      pdf.text "All Facilities", :align => :center, :size => 18, :style => :bold
   else
      pdf.move_down(5)
      pdf.text o.long_name, :align => :center, :size => 18, :style => :bold
   end
   pdf.text format_date(@report_time), :size => 12, :style => :bold, :align => :center
   pdf.move_down(10)
   pdf.text "Deposits", :size => 20, :style => :bold, :align => :center
   pdf.move_down(5)
   
   if @deposits.empty?
      pdf.move_down(40)
      pdf.text "No Deposits To Report", :size => 12, :style => :bold, :align => :center
   else
      data = []
      trans = nil
      until @deposits.empty?
         if trans.blank? || trans.person_id != @deposits[0].person_id
            data.push(["#{show_person @deposits[0].person}",
               "(#{@deposits[0].person.commissary_facility.nil? ? '' : @deposits[0].person.commissary_facility.short_name})",'','',''])
         end
         trans = @deposits.shift
         data.push([format_date(trans.transaction_date),trans.memo,trans.receipt_no,"#{trans.category.nil? ? 'Invalid' : trans.category.long_name}",number_to_currency(trans.deposit)])
         if @status == 'post'
            trans.update_attribute(:report_datetime, @report_time)
         end
         deposit_total += trans.deposit
         account_total += trans.deposit
         if @deposits.empty? || @deposits[0].person_id != trans.person_id
            data.push(['','','','***Account Total:',number_to_currency(account_total)])
            data.push([' ','','','',''])
            account_total = BigDecimal.new('0',2)
         end
         if @deposits.empty?
            pdf.table data, :border_width => 0, :headers => ["Date", "Memo", "Receipt", "Type", "Amount"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :right}, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2, :width => pdf.bounds.width
            pdf.move_down(5)
            pdf.text "Transactions: #{deposit_count}   Deposit Total: #{number_to_currency(deposit_total)}", :size => 14, :style => :bold, :align => :right
         end
      end
   end
   # now spit out a last page with summary
   pdf.start_new_page
   if @status == 'post' || @status == 'reprint'
      pdf.text "Commissary Posting Report", :align => :center, :size => 20, :style => :bold
   else
      pdf.text "Commissary Posting Report (PREVIEW)", :align => :center, :size => 20, :style => :bold
   end
   if @facility.nil? || !(o = Option.find_by_id(@facility))
      pdf.move_down(5)
      pdf.text "All Facilities", :align => :center, :size => 18, :style => :bold
   else
      pdf.move_down(5)
      pdf.text o.long_name, :align => :center, :size => 18, :style => :bold
   end
   pdf.text format_date(@report_time), :size => 12, :style => :bold, :align => :center
   pdf.move_down(10)
   pdf.text "Summary", :size => 20, :style => :bold, :align => :center
   pdf.move_down(20)
   data = [
      ["Withdrawal Transactions:",withdraw_count],
      ["Deposit Transactions:",deposit_count],
      ["Total Transactions:",withdraw_count + deposit_count],
      [" ",""],
      ["Total Withdrawn:",number_to_currency(withdraw_total)],
      ["Total Deposited:",number_to_currency(deposit_total)],
      ["Net Change:",number_to_currency(deposit_total + withdraw_total)]
   ]
   pdf.table data, :border_width => 1, :align => {0 => :left, 1 => :right}, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 5, :position => :center
   pdf.move_down(5)
   pdf.text "END OF REPORT", :size => 12, :style => :bold
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]

