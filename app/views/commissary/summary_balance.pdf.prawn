# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
report_datetime = DateTime.now

pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(report_datetime, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Commissary", :align => :center, :size => 20, :style => :bold
   pdf.text @page_title, :align => :center, :size => 16, :style => :bold
   pdf.move_down(5)
   pdf.text DateTime.now.to_s(:us), :size => 12, :style => :bold, :align => :center
   pdf.move_down(15)
   if !@person.nil?
      pdf.text "#{show_person(@person)}", :size => 16, :style => :bold, :align => :center
   elsif @facility.nil?
      pdf.text "-- All Facilities --", :size => 16, :style => :bold, :align => :center
   else
      pdf.text "#{@facility.long_name}", :size => 16, :style => :bold, :align => :center
   end
   pdf.move_down(20)
   pdf.text "Transaction Summary", :size => 18, :style => :bold, :align => :center
   pdf.move_down(5)
   # calculate totals
   inmates = 0
   tot_dep = BigDecimal.new('0',2)
   num_dep = 0
   tot_wit = BigDecimal.new('0',2)
   num_wit = 0
   categories = Hash.new
   get_opts("Commissary Type").each do |c|
      categories[c[0]] = 0
   end
   @people.each do |p|
      commissaries = p.commissary_transactions(@start_date,@stop_date)
      unless commissaries.empty?
         inmates += 1
         commissaries.each do |c|
            categories[c.category.long_name] += 1
            if c.deposit?
               num_dep += 1
               tot_dep += c.deposit
            end
            if c.withdraw?
               num_wit += 1
               tot_wit -= c.withdraw
            end
         end
      end
   end
   tot_trans = num_wit + num_dep
   net_change = tot_dep + tot_wit
   data = [["Deposits:",num_dep,number_to_currency(tot_dep)],
      ["Withdrawals:",num_wit,"#{number_to_currency(tot_wit)}"],
      ["Net Change:",'',number_to_currency(net_change)]]
   pdf.table data, :border_width => 1, :align => :right, :font_size => 14, :headers => [" ","Number","Total"], :position => :center, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
   pdf.move_down(10)
   pdf.text "Totals", :size => 18, :style => :bold, :align => :center
   data = [["Inmates:",inmates],
      ["Transactions:",tot_trans]]
   pdf.table data, :border_width => 1, :align => :right, :font_size => 14, :position => :center, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
   pdf.move_down(10)
   pdf.text "Totals by Category", :size => 18, :style => :bold, :align => :center
   data = []
   categories.keys.each do |c|
      data.push(["#{c}:",categories[c]])
   end
   pdf.table data, :border_width => 1, :align => :right, :font_size => 14, :position => :center, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
end