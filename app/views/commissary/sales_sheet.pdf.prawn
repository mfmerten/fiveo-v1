# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
this_instant = DateTime.now
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(this_instant, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.agency}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 35) do
   # build the data array once
   lpage = @commissary_items.slice!(0, @commissary_items.size / 2)
   rpage = @commissary_items.slice!(0..-1)
   data = []
   (0..lpage.size-1).each do |r|
      data.push([lpage[r].description, number_to_currency(lpage[r].price), "___","$______"," ","#{rpage[r].nil? ? '' : rpage[r].description}", "#{rpage[r].nil? ? '' : number_to_currency(rpage[r].price)}", "#{rpage[r].nil? ? '' : '___'}", "#{rpage[r].nil? ? '' : '$______'}"])
   end
   (1..3).each do |r|
      data.push(["Special: _____________________","$_____","___","$______"," ","Special: _____________________","$_____","___","$______"])
   end
   
   # print a page for each booking
   @people.each do |p|
      if p.bookings.empty?
         b = nil
      else
         b = p.bookings.last
      end
      pdf.text "Commissary Sales Sheet", :align => :center, :size => 24, :style => :bold
      pdf.move_down(5)
      pdf.text "#{@facility.long_name}", :align => :center, :size => 14, :style => :bold
      pdf.text format_date(this_instant), :size => 12, :style => :bold, :align => :center
      pdf.move_down(5)
      row_pos = pdf.bounds.top - 65
      pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), "Name:", p.full_name, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
      pdf.text_field [pdf.col(4,3), row_pos], pdf.col(4), "Account:", p.id.to_s, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
      row_pos -= pdf.text_field [pdf.col(4,4), row_pos], pdf.col(4), "Location:", "#{b.nil? ? 'No Booking' : (b.cell.nil? ? 'Invalid' : b.cell.long_name)}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
      pdf.text_field [pdf.col(4,1), row_pos], pdf.col(4), "Balance:", number_to_currency(p.commissary_balance), SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
      pdf.text_field [pdf.col(4,2), row_pos], pdf.col(4), "Less Medical:", "$ ", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
      pdf.text_field [pdf.col(4,3), row_pos], pdf.col(4), "Adjusted Balance:", "$ ", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
      pdf.text_field [pdf.col(4,4), row_pos], pdf.col(4), "Total Cost:","$ ", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
      pdf.font("Helvetica")
      pdf.table data, :border_width => 1, :border_style => :underline_header, :header_color => SiteConfig.pdf_header_bg, :header_text_color => SiteConfig.pdf_header_txt, :headers => ["Item", "Price", "Qty", "Total", "", "Item", "Price", "Qty", "Total"], :align => {0 => :left, 1 => :right, 2 => :left, 3 => :left, 4 => :left, 5 => :left, 6 => :right, 7 => :left, 8 => :left}, :align_headers => :left, :font_size => 11, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :vertical_padding => 3, :horizontal_padding => 1, :position => :center
      pdf.font("Times-Roman")
      pdf.move_down(10)
      pdf.text "*** ALL PRICES SUBJECT TO CHANGE WITHOUT NOTICE ***", :size => 10, :style => :bold, :align => :center
      pdf.text "*** LARGER SIZED ITEMS MAY HAVE HIGHER PRICE THAN SHOWN ***", :size => 10, :style => :bold, :align => :center
      pdf.move_down(35)
      pdf.stroke_horizontal_line(pdf.col(2,1), pdf.col(2,1) + pdf.col(2))
      pdf.span(pdf.col(2), :position => pdf.col(2,1)) do
         pdf.text "Inmate Signature", :size => 8
      end
      pdf.move_up(9)
      pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
      pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
         pdf.text "Jailer Signature", :size => 8
      end
      unless p == @people.last
         pdf.start_new_page
      end
   end
end
