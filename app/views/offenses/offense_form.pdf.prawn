# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@offense.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Initial Offense Report", :size => 24, :align => :center, :style => :bold
   pdf.text format_date(@offense.offense_date, @offense.offense_time), :size => 14, :style => :bold, :align => :center
   data = [["CASE:", @offense.case_no]]
   unless @offense.victims.empty?
      @offense.victims.each do |v|
         address = v.physical_line1 + ", " + v.physical_line2
         data.push(["Victim:", "#{v.full_name}, #{address}"])
      end
   end
   data.push(["Location:", @offense.location])
   unless @offense.suspects.empty?
      @offense.suspects.each do |s|
         address = s.physical_line1 + ", " + s.physical_line2
         data.push(["Suspect:", "#{s.full_name}, #{address}"])
      end
   end
   pdf.table data, :border_width => 0, :font_size => 12, :align => {0 => :right, 1 => :left}
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   pdf.text "Detail Of Offense", :size => 18, :align => :center, :style => :bold
   pdf.move_down(4)
   pdf.text @offense.details, :size => 12
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   unless @offense.wreckers.empty?
      pdf.text "Wreckers Used", :size => 18, :style => :bold, :align => :center
      pdf.move_down(5)
      data = []
      @offense.wreckers.each do |w|
         data.push([w.name, "#{w.by_request? ? 'Yes' : 'No'}", w.location, w.tag, w.vin])
      end
      unless data.empty?
         pdf.table data, :border_width => 1, :font_size => 12, :align => :left, :headers => ['Wrecker Service','Req?','Storage Location','Vehicle Tag','VIN'], :width => pdf.bounds.width
      end
      pdf.pad(10){ pdf.stroke_horizontal_rule }
   end
   pdf.text "Documentation", :size => 18, :style => :bold, :align => :center
   pdf.move_down(5)
   counter = 1
   if @offense.pictures? || !@offense.offense_photos.empty?
      pdf.text "#{counter.to_s}: Pictures (misc)"
      counter += 1
   end
   if @offense.restitution_form?
      pdf.text "#{counter.to_s}: Restitution Form"
      counter += 1
   end
   if @offense.victim_notification?
      pdf.text "#{counter.to_s}: Victim Notification"
      counter += 1
   end
   if @offense.perm_to_search?
      pdf.text "#{counter.to_s}: Permission To Search Form"
      counter += 1
   end
   if @offense.misd_summons?
      pdf.text "#{counter.to_s}: Misdemeanor Summons"
      counter += 1
   end
   if @offense.fortyeight_hour?
      pdf.text "#{counter.to_s}: 48 Hour"
      counter += 1
   end
   if @offense.medical_release?
      pdf.text "#{counter.to_s}: Medical Release"
      counter += 1
   end
   @offense.witnesses.each do |w|
      pdf.text "#{counter.to_s}: Witness Statement: #{w.full_name}"
      counter += 1
   end
   @offense.citations.each do |c|
      pdf.text "#{counter.to_s}: Citation ##{c.ticket_no} -- To: #{show_person c.person} -- For: #{c.charge_summary}"
      counter += 1
   end
   @offense.forbids.each do |f|
      pdf.text "#{counter.to_s}: Forbidden Form #{f.id} -- By: #{f.complainant}  For: #{f.full_name}"
      counter += 1
   end
   if @offense.other_documents?
      pdf.text "#{counter.to_s}: Other Documents: #{@offense.other_documents}"
      counter += 1
   end
   pdf.move_down(40)
   pdf.stroke_horizontal_line(0,330)
   pdf.text "#{show_contact @offense, :officer}", :size => 14
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]

