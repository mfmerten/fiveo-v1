# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@fax_cover.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.move_down(20)
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "Date: _____________", :size => 18
      pdf.move_down(5)
      pdf.text "Attention:", :size => 18
      pdf.stroke_horizontal_line(80, pdf.bounds.right - 50)
      pdf.move_down(9)
      pdf.text "FAX Number: _________________", :size => 18
      pdf.move_down(5)
      pdf.text "From:", :size => 18
      pdf.stroke_horizontal_line(50, pdf.bounds.right - 50)
      pdf.move_down(9)
      pdf.text "Number of Pages Including This Cover Sheet: _______", :size => 18
      pdf.move_down(20)
      pdf.text @fax_cover.from_name, :size => 18, :style => :bold
      pdf.text "Phone: #{@fax_cover.from_phone}", :size => 18
      pdf.text "FAX: #{@fax_cover.from_fax}", :size => 18
      pdf.move_down(10)
      pdf.text "Message:", :size => 18
      pdf.stroke_horizontal_line(78, pdf.bounds.right - 50)
      (1..4).each do
         pdf.move_down(25)
         pdf.stroke_horizontal_line(0, pdf.bounds.right - 50)
      end
   end
   if @fax_cover.include_notice?
      pdf.move_down(30)
      pdf.text @fax_cover.notice_title, :size => 18, :style => :bold, :align => :center
      pdf.move_down(5)
      pdf.text @fax_cover.notice_body, :size => 14
   end
end
