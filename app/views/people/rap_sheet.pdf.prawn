# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@person.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text @page_title, :align => :center, :size => 20, :style => :bold
   pdf.text @datetime, :size => 14, :style => :bold, :align => :center
   pdf.move_down(10)
   quot = '"'
   pdf.text "#{show_person @person}   #{@person.aka? ? quot + @person.aka + quot : ''}", :size => 18, :style => :bold, :align => :left
   pdf.image "#{RAILS_ROOT}/public#{@person.front_mugshot.url(:medium)}".gsub(/\?\d*$/,''), :at => [306, pdf.bounds.top - 70], :fit => [100,150]
   pdf.image "#{RAILS_ROOT}/public#{@person.side_mugshot.url(:medium)}".gsub(/\?\d*$/,''), :at => [456, pdf.bounds.top - 70], :fit => [100,150]
   
   # start position
   row_position = pdf.bounds.top - 70
   
   # these use: pdf.text_field origin, width, label, value, border_color, label_color, rows
   # and: pdf.col(1) for 1 column width, pdf.col(1,1) for position of first column on one column width
   # date of birth
   row_position -= pdf.text_field [pdf.col(2,1), row_position], pdf.col(2), "Date of Birth:", format_date(@person.date_of_birth), SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   
   # place of birth
   row_position -= pdf.text_field [pdf.col(2,1), row_position], pdf.col(2), "Place of Birth:", @person.place_of_birth, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   
   # ssn
   row_position -= pdf.text_field [pdf.col(2,1), row_position], pdf.col(2), "SSN:", @person.ssn, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   
   # oln
   row_position -= pdf.text_field [pdf.col(2,1), row_position], pdf.col(2), "OLN:", "#{@person.oln} (#{@person.oln_state.nil? ? '' : @person.oln_state.short_name})", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   
   # sid
   row_position -= pdf.text_field [pdf.col(2,1), row_position], pdf.col(2), "SID:", @person.sid, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   
   # fbi ID
   row_position -= pdf.text_field [pdf.col(2,1), row_position], pdf.col(2), "FBI ID:", @person.fbi, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   
   # DOC Number
   row_position -= pdf.text_field [pdf.col(2,1), row_position], pdf.col(2), "DOC No:", @person.doc_number, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   
   # Physical Address
   pdf.text_field [pdf.col(2,1), row_position], pdf.col(2), "Physical Address:", "#{@person.physical_line1}\n#{@person.physical_line2}" , SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 2
   row_position -= pdf.text_field [pdf.col(2,2), row_position], pdf.col(2), "Mailing Address:", "#{@person.mailing_line1}\n#{@person.mailing_line2}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 2

   # home phone, cell phone, emergency contact
   pdf.text_field [pdf.col(2,2), row_position], pdf.col(2), "Emergency Contact:", "#{@person.emergency_contact}\n#{@person.em_relationship.nil? ? '' : @person.em_relationship.long_name}\n#{@person.emergency_phone}", SiteConfig.pdf_em_bg, SiteConfig.pdf_em_txt, 3
   pdf.text_field [pdf.col(4,1), row_position], pdf.col(4), "Home Phone:", @person.home_phone, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   row_position -= pdf.text_field [pdf.col(4,2), row_position], pdf.col(4), "Cell Phone:", @person.cell_phone, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   
   # occupation
   row_position -= pdf.text_field [pdf.col(2,1), row_position], pdf.col(2), "Occupation:", @person.occupation, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   
   # employer
   row_position -= pdf.text_field [pdf.col(2,1), row_position], pdf.col(2), "Employer:", @person.employer, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   
   # Physical Description
   pdf.text_field [pdf.col(2,1), row_position], pdf.col(2), "Description:", @person.person_description, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 3
   row_position -= pdf.text_field [pdf.col(2,2), row_position], pdf.col(2), "Distinguishing Marks:", @person.marks_description, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 3
   
   pdf.move_down(10)
   data = [['']]
   pdf.table data, :width => pdf.bounds.width, :border_width => 0, :header_color => SiteConfig.pdf_section_bg, :header_text_color => SiteConfig.pdf_section_txt, :font_size => 14, :align => :center, :vertical_padding => 1, :headers => ['ACTIVE RECORDS']
   pdf.move_down(5)
   
   unless @vwarrants.empty?
      pdf.fill_color = SiteConfig.pdf_em_txt
      pdf.text "Verified Warrants:", :size => 14, :style => :bold
      pdf.fill_color = '000000'
      data = []
      @vwarrants.each do |w|
         data.push([w.id, w.full_name, "#{format_date(w.dob)}\n#{w.ssn}", "#{w.jurisdiction.nil? ? '' : w.jurisdiction.fullname}\n#{w.warrant_no}", w.charge_summary])
      end
      pdf.table data, :column_widths => {0 => 55, 1 => 100, 2 => 65, 3 => 100, 4 => 232}, :border_width => 1, :border_style => :underline_header, :align => :left, :headers => ['ID','Name','DOB/SSN','Agency/War No','Charge(s)'], :font_size => 10, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :vertical_padding => 2, :position => 20
      pdf.move_down(5)
   end

   unless @pwarrants.empty?
      pdf.fill_color = SiteConfig.pdf_em_txt
      pdf.text "Possible Warrants:", :size => 14, :style => :bold
      pdf.fill_color = '000000'
      data = []
      @pwarrants.each do |w|
         data.push([w.id, w.full_name, "#{format_date(w.dob)}\n#{w.ssn}", "#{w.jurisdiction.nil? ? '' : w.jurisdiction.fullname}\n#{w.warrant_no}", w.charge_summary])
      end
      pdf.table data, :column_widths => {0 => 55, 1 => 100, 2 => 65, 3 => 100, 4 => 232}, :border_width => 1, :border_style => :underline_header, :align => :left, :headers => ['ID','Name','DOB/SSN','Agency/War No','Charge(s)'], :font_size => 10, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :vertical_padding => 2, :position => 20
      pdf.move_down(5)
   end
   
   unless @vforbids.empty?
      pdf.text "Verified Forbids:", :size => 14, :style => :bold
      data = []
      @vforbids.each do |f|
         if !f.signed_date?
            status = "#{format_date(f.request_date)} (NOT SIGNED)"
         else
            status = "#{format_date(f.signed_date)} (ACTIVE)"
         end
         data.push([f.id, f.full_name, f.complainant, status, f.details])
      end
      pdf.table data, :width => pdf.bounds.width - 20, :border_width => 1, :border_style => :underline_header, :align => :left, :headers => ['Forbid No','Name','Complainant','Status','Forbidden Locations'], :font_size => 9, :row_colors => [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], :vertical_padding => 2, :position => 20
      pdf.move_down(5)
   end

   unless @pforbids.empty?
      pdf.text "Possible Forbids:", :size => 14, :style => :bold
      data = []
      @pforbids.each do |f|
         if !f.signed_date?
            status = "#{format_date(f.request_date)} (NOT SIGNED)"
         else
            status = "#{format_date(f.signed_date)} (ACTIVE)"
         end
         data.push([f.id, f.full_name, f.complainant, status, f.details])
      end
      pdf.table data, :width => pdf.bounds.width - 20, :border_width => 1, :border_style => :underline_header, :align => :left, :headers => ['Forbid No','Name','Complainant','Status','Forbidden Locations'], :font_size => 9, :row_colors => [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], :vertical_padding => 2, :position => 20
      pdf.move_down(5)
   end
   
   unless @bonds.empty?
      pdf.text "Bonds:", :size => 14, :style => :bold
      data = []
      @bonds.each do |b|
         data.push([b.id, format_date(b.issued_date, b.issued_time), "#{b.arrest.nil? ? '' : b.arrest.id}", "#{b.bond_type.nil? ? '' : b.bond_type.long_name}", "#{b.bondsman.nil? ? '' : b.bondsman.name}#{b.surety.nil? ? '' : b.surety.full_name}", number_to_currency(b.bond_amt), "#{b.status.nil? ? '' : b.status.long_name}"])
      end
      pdf.table data, :width => pdf.bounds.width - 20, :border_width => 1, :border_style => :underline_header, :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :left, 5 => :right, 6 => :left}, :headers => ['ID','Date/Time','Arrest','Type','Surety','Amount','Status'], :font_size => 9, :row_colors => [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], :vertical_padding => 2, :position => 20
      pdf.move_down(5)
   end
   
   unless @probations.empty?
      pdf.text "Probations:", :size => 14, :style => :bold
      data = []
      @probations.each do |p|
         active = ['Active','State','Unsupervised'].include?(p.status.long_name)
         if active
            if p.status.long_name == 'Active'
               ptype = 'Parish'
            else
               ptype = p.status.long_name
            end
            pstat = 'Active'
            if p.hold_if_arrested?
               pstat << " [Hold If Arrested]"
            end
         else
            if p.doc?
               ptype = 'State'
            else
               ptype = 'Parish'
            end
            if !p.sentence.nil? && p.sentence.unsupervised?
               ptype << " (Unsupervised)"
            end
            pstat = p.status.long_name
         end
         data.push([p.id, ptype, p.probation_string, pstat, "[#{p.conviction_count}] #{p.conviction}]"])
      end
      pdf.table data, :column_widths => {0 => 50, 1 => 40, 2 => 65, 3 => 125, 4 => 272}, :border_width => 1, :border_style => :underline_header, :headers => ['ID','Type','Length','Status','Conviction'], :font_size => 9, :row_colors => [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], :vertical_padding => 2, :position => 20
   end

   pdf.move_down(10)
   data = [['']]
   pdf.table data, :width => pdf.bounds.width, :border_width => 0, :header_color => SiteConfig.pdf_section_bg, :header_text_color => SiteConfig.pdf_section_txt, :font_size => 14, :align => :center, :vertical_padding => 1, :headers => ['CRIMINAL HISTORY']
   pdf.move_down(5)
   
   unless @history.empty?
      sep = "\n"
      @history.each do |hist|
         if pdf.y < 50
            pdf.start_new_page
         end
         data = []
         if hist[1].is_a?(Arrest)
            type = ""
            headers = ["#","Charge","Status"]
            case hist[1].arrest_type
            when 0
               type = "District"
            when 1
               type = "City"
            when 2
               type = "Other"
            end
            pdf.text "#{format_date(hist[0])} ==> Arrest#{type.blank? ? '' : ' [' + type + ']'}", :size => 12, :style => :bold
            hist[1].district_charges.each do |c|
               status = ""
               if hist[1].legacy?
                  data.push([c.count, c.charge, "[Legacy]"])
               elsif c.da_charges.empty?
                  data.push([c.count, c.charge, "Pending"])
               elsif c.da_charges.size == 1
                  status = charge_status_string(c.da_charges.first, c)
                  data.push([c.count, c.charge, status])
               else
                  c.da_charges.each do |d|
                     status << charge_status_string(d,c)
                     unless d == c.da_charges.last
                        status << sep
                     end
                  end
                  data.push([c.count, c.charge, status])
               end
            end
            hist[1].da_charges.each do |c|
               data.push([c.count, "#{c.charge} -- Added by D.A.", charge_status_string(c)])
            end
            hist[1].city_charges.each do |c|
               data.push([c.count, c.charge, "#{c.agency.nil? ? '' : c.agency.fullname}"])
            end
            hist[1].other_charges.each do |c|
               data.push([c.count, c.charge, "#{c.agency.nil? ? '' : c.agency.fullname}"])
            end
         elsif hist[1].is_a?(Citation)
            pdf.text "#{format_date(hist[0])} ==> Citation", :size => 12, :style => :bold
            headers = ["#","Charge","Status"]
            hist[1].charges.each do |c|
               status = ""
               if c.da_charges.empty?
                  data.push([c.count, c.charge, "Pending"])
               elsif c.da_charges.size == 1
                  status = charge_status_string(c.da_charges.first,c)
                  data.push([c.count, c.charge, status])
               else
                  c.da_charges.each do |d|
                     status << charge_status_string(d,c)
                     unless d == c.da_charges.last
                        status << sep
                     end
                  end
                  data.push([c.count, c.charge, status])
               end
            end
         elsif hist[1].is_a?(Transfer)
            pdf.text "#{format_date(hist[0])} ==> Transfer: #{hist[1].transfer_type.nil? ? '' : hist[1].transfer_type.long_name}", :size => 12, :style => :bold
            data.push([" ", "#{hist[1].from_agency}", "#{hist[1].to_agency}"])
            headers = [" ","From","To"]
         elsif hist[1].is_a?(Probation)
            if hist[1].status.nil?
               active = false
            else
               active = ['Active','State','Unsupervised'].include?(hist[1].status.long_name)
            end
            if active
               if hist[1].status.long_name == 'Active'
                  ptype = 'Parish'
               else
                  ptype = hist[1].status.long_name
               end
               pstat = 'Active'
               if hist[1].hold_if_arrested?
                  pstat << " [HOLD IF ARRESTED]"
               end
            else
               if !hist[1].sentence.nil? && !hist[1].sentence.probation_type.nil?
                  ptype = hist[1].sentence.probation_type.long_name
               else
                  ptype = ""
               end
               pstat = hist[1].status.long_name
            end
            pdf.text "#{format_date(hist[0])} ==> Probation: #{ptype} (#{hist[1].probation_string})", :size => 12, :style => :bold
            data.push([hist[1].conviction_count, hist[1].conviction, pstat])
            headers = ['#','Conviction','Probation Status'] 
         end
         pdf.table data, :column_widths => {0 => 10, 1 => ((pdf.bounds.width - 40) / 2), 2 => ((pdf.bounds.width - 40) / 2) + 25}, :border_width => 1, :border_style => :underline_header, :align => :left, :headers => headers, :font_size => 10, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 1
         pdf.pad(5){pdf.stroke_horizontal_rule}
      end
   end
end

pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
