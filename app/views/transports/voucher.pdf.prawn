# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@transport.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Transportation Voucher", :size => 24, :align => :center, :style => :bold
   pdf.text format_date(@transport.begin_date), :size => 14, :style => :bold, :align => :center
   pdf.move_down(30)
   data = []
   unless @transport.person.nil?
      data.push(["Person:", "#{show_person @transport.person}" ])
   end
   if @transport.officer1?
      data.push(["Officer:", "#{show_contact @transport, :officer1}"])
   end
   if @transport.officer2?
      data.push(["Officer:", "#{show_contact @transport, :officer2}"])
   end
   unless data.empty?
      pdf.table data, :border_width => 0, :font_size => 14, :align => {0 => :right, 1 => :left}
   end
   pdf.move_down(20)
   data = [[@transport.from,@transport.to], [@transport.from_address_line1,@transport.to_address_line1], [@transport.from_address_line2, @transport.to_address_line2]]
   width = pdf.bounds.right / 2
   pdf.table data, :border_width => 1, :border_style => :underline_header, :font_size => 14, :align => :left, :column_widths => {0 => width, 1 => width}, :headers => ["Transport From","Transport To"]
   pdf.move_down(30)
   data = [["Date/Time:",format_date(@transport.begin_date, @transport.begin_time), format_date(@transport.end_date, @transport.end_time), "#{@transport.duration}"], ["Odometer:", @transport.begin_miles, @transport.end_miles, "#{@transport.mileage} miles"]]
   width = pdf.bounds.right / 7
   pdf.table data, :border_width => 1, :border_style => :underline_header, :font_size => 14, :align => {0 => :right, 1 => :left, 2 => :left, 3 => :left}, :column_widths => {0 => width, 1 => width * 2, 2 => width * 2, 3 => width * 2}, :headers => ["","Beginning", "Ending", "Difference"]
   pdf.move_down(30)
   data = [[@transport.remarks + " "]]
   width = pdf.bounds.right / 2
   pdf.table data, :border_width => 1, :border_style => :underline_header, :font_size => 14, :align => :left, :column_widths => {0 => pdf.bounds.right}, :headers => ["Remarks"]
end