# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@court.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.agency}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 35) do
   # generate a sentence sheet for each unsentenced charge
   @court.docket.da_charges.reject{|c| !c.sentence.nil? || c.info_only? || c.dropped_by_da? }.each do |chg|
      arrest = nil
      citation = nil
      bonds = []
      if !chg.arrest_charge.nil?
         arrest = chg.arrest_charge.district_arrest
         unless arrest.nil?
            bonds = arrest.bonds
         end
         citation = chg.arrest_charge.citation
      elsif !chg.arrest.nil?
         arrest = chg.arrest
      end
      # -- generate top portion of the worksheet with user/docket information
      pdf.text "The 11th Judicial District Court", :size => 14, :style => :bold
      pdf.move_up(14)
      pdf.text "Judge: #{@court.judge.nil? ? '__________________' : @court.judge.long_name.upcase}", :size => 14, :style => :bold, :align => :right
      pdf.text "Parish of Sabine", :size => 14, :style => :bold
      pdf.move_up(14)
      pdf.text "Docket No: #{@court.docket.docket_no}", :size => 14, :style => :bold, :align => :right
      pdf.text "State Of Louisiana", :size => 14, :style => :bold
      pdf.move_up(14)
      pdf.text "Court Date: #{@court.court_date? ? format_date(@court.court_date) : '___________'}", :size => 14, :style => :bold, :align => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "#{show_person(@court.docket.person).upcase}", :size => 18, :style => :bold
      pdf.text "Charge [#{chg.id}]: (#{chg.count}) #{chg.charge}", :size => 14, :style => :bold
      if !arrest.nil?
         pdf.text "Arrest Date: #{format_date(arrest.arrest_date, arrest.arrest_time)}    Arrest Agency: #{arrest.agency}", :size => 14
         unless chg.arrest_charge.nil?
            pdf.text "DWI Test Officer: #{show_contact arrest, :dwi_test_officer, :invalid => 'N/A'}     DWI Test Results: #{arrest.dwi_test_results? ? arrest.dwi_test_results : 'N/A'}", :size => 14, :style => :bold
         end
      elsif !citation.nil?
         pdf.text "Citation Date: #{format_date(citation.citation_date)}", :size => 14
      end
      pdf.move_down(5)
      pdf.text "Convicted of: ______________________________________________________________", :size => 14
      pdf.move_down(5)
      checkbox = "#{RAILS_ROOT}/public/images/checkbox.png"
      col1 = 55
      col2 = col1 + 5
      # Result (plea or conviction)
      data = [[]]
      OptionType.find_by_name("Trial Result").options.reject(&:disabled?).collect{|o| o.long_name}.each do |o|
         data[0].push(' ')
         data[0].push(o)
      end
      pdf.table data, :font_size => 14, :position => :center
      
      pdf.move_down(10)
      
      pdf.font "Helvetica", :style => :bold, :size => 14
      data = [["FINES/COSTS"]]
      pdf.table data, :border_width => 0, :row_colors => [SiteConfig.pdf_section_bg], :width => pdf.bounds.width, :align => :center, :padding => 1
      pdf.font "Times-Roman"

      pdf.move_down(10)
      
      pdf.span pdf.bounds.right - 15, :position => 15 do
         pdf.text "Fine $_______________ and _______________ C/C $_______________", :size => 14
         pdf.move_down(10)
         pdf.text "For a total of $____________  Must Pay By: ____________ Or Be In Court On: ____________", :size => 14
         pdf.move_down(10)
         pdf.text "OR in default serve _____ Hours _____ Days _____ Months _____Years Parish Jail.", :size => 14
      end
      
      pdf.move_down(10)

      pdf.font "Helvetica", :style => :bold, :size => 14
      data = [["SENTENCE"]]
      pdf.table data, :border_width => 0, :row_colors => [SiteConfig.pdf_section_bg], :width => pdf.bounds.width, :align => :center, :padding => 1
      pdf.font "Times-Roman"
      
      pdf.move_down(10)
      
      pdf.span pdf.bounds.right - 15, :position => 15 do
         pdf.text "_____ Hours _____ Days _____ Months _____ Years        D.O.C.         Parish Jail", :size => 14
         pdf.move_up(14)
         pdf.image checkbox, :position => 311, :width => 12
         pdf.move_up(12)
         pdf.image checkbox, :position => 383, :width => 12
         pdf.move_down(10)
         pdf.span(pdf.bounds.width - 32, :position => 32) do
            pdf.text "Suspended  (Except: _____ Hours _____ Days _____ Months _____ Years)", :size => 14
         end
         pdf.move_up(14)
         pdf.image checkbox, :width => 12
      end
      
      pdf.move_down(10)
      
      pdf.font "Helvetica", :style => :bold, :size => 14
      data = [["PROBATION"]]
      pdf.table data, :border_width => 0, :row_colors => [SiteConfig.pdf_section_bg], :width => pdf.bounds.width, :align => :center, :padding => 1
      pdf.font "Times-Roman"
      
      pdf.move_down(10)
      
      pdf.span pdf.bounds.right - 15, :position => 15 do
         pdf.text "_____ Hours _____ Days _____ Months _____ Years        State        Parish        Unsupervised", :size => 14
         pdf.move_up(14)
         pdf.image checkbox, :position => 311, :width => 12
         pdf.move_up(12)
         pdf.image checkbox, :position => 367, :width => 12
         pdf.move_up(12)
         pdf.image checkbox, :position => 430, :width => 12
         pdf.move_down(10)
         pdf.span(pdf.bounds.width - 32, :position => 32) do
            pdf.text "Reverts to Unsupervised _______________________________________", :size => 14
         end
         pdf.move_up(14)
         pdf.image checkbox, :width => 12
      end
      
      pdf.move_down(10)
      
      pdf.font "Helvetica", :style => :bold, :size => 14
      data = [["CONDITIONS"]]
      pdf.table data, :border_width => 0, :row_colors => [SiteConfig.pdf_section_bg], :width => pdf.bounds.width, :align => :center, :padding => 1
      pdf.font "Times-Roman"

      pdf.move_down 10

      col_start_row = pdf.y
      col = (pdf.bounds.right / 2) - 17
      pdf.span col, :position => 17 do
         pdf.text "     Credit for Time Served", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12
      pdf.move_down 10
      pdf.span col, :position => 17 do
         pdf.text "     W/O Benefit", :size => 13

      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12
      pdf.move_down 10
      pdf.text "_____ Days _____ Hours Community Service.", :size => 14
      pdf.move_down 10
      pdf.span col, :position => 17 do
         pdf.text "     Substance Abuse Program.", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12
      pdf.move_down 10
      pdf.span col, :position => 17 do
         pdf.text "     Driver Improvement Program", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12
      pdf.move_down 10
      pdf.text "Monthly Probation Fees: $____________", :size => 14
      pdf.move_down 10
      pdf.text "Restitution to Victim: $____________", :size => 14
      pdf.move_down 10
      pdf.text "Restitution Paid By: _______________", :size => 14
      pdf.move_down 10
      pdf.span col, :position => 17 do
         pdf.text "Delay execution of sentence (see notes)", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12
      pdf.move_down 10
      pdf.span col, :position => 17 do
         pdf.text "Pre-Sentence Investigation.", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12

      col_end_row = pdf.y
      pdf.move_up(col_start_row - pdf.y)
      pdf.span col, :position => col + 34 do
         pdf.text "Anger Management", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12, :position => col + 17
      pdf.move_down 10
      pdf.span col, :position => col + 34 do
        pdf.text "Domestic Abuse Program", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12, :position => col + 17
      pdf.move_down 10
      pdf.span col, :position => col + 34 do
         pdf.text "Substance Abuse Treatment", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12, :position => col + 17
      pdf.move_down 10
      pdf.span col, :position => col + 34 do
         pdf.text "Random Drug Screens", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12, :position => col + 17
      pdf.move_down 10
      pdf.span col, :position => col + 34 do
         pdf.text "No Contact With Victim", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12, :position => col + 17
      pdf.move_down 10
      pdf.span col, :position => col + 34 do
        pdf.text "No Firearms During Probation", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12, :position => col + 17
      pdf.move_down 10
      pdf.span col, :position => col + 17 do
         pdf.text "Article:       893       894       895", :size => 14
         pdf.move_up(14)
         pdf.image checkbox, :width => 12, :position => 51
         pdf.move_up(12)
         pdf.image checkbox, :width => 12, :position => 96
         pdf.move_up(12)
         pdf.image checkbox, :width => 12, :position => 142
         pdf.move_down 10
         pdf.text "D.A.R.E. Program: $____________", :size => 14
         pdf.move_down 10
         pdf.text "Indigent Defender Fund: $____________", :size => 14
      end
      pdf.move_down 10
      pdf.span col, :position => col + 34 do
         pdf.text "Deny Goodtime", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12, :position => col + 17
      pdf.move_down(pdf.y - col_end_row)
      pdf.move_down 10
      
      pdf.font "Helvetica", :style => :bold, :size => 14
      data = [["CONCURRENCY"]]
      pdf.table data, :border_width => 0, :row_colors => [SiteConfig.pdf_section_bg], :width => pdf.bounds.width, :align => :center, :padding => 1
      pdf.font "Times-Roman"
      
      pdf.move_down 10
      
      pdf.span 65, :position => 15 do
         pdf.text "Sentence:", :size => 14, :align => :right
         pdf.move_down 10
         pdf.text "Fines:", :size => 14, :align => :right
         pdf.move_down 10
         pdf.text "Costs:", :size => 14, :align => :right
      end
      pdf.move_up 65
      pdf.span pdf.bounds.right - 100, :position => 100 do
         pdf.text "Concurrent       Consecutive ___________________________________", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12, :position => 85
      pdf.move_up(12)
      pdf.image checkbox, :width => 12, :position => 173
      pdf.move_down 10
      pdf.span pdf.bounds.right - 100, :position => 100 do
         pdf.text "Concurrent       Consecutive ___________________________________", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12, :position => 85
      pdf.move_up(12)
      pdf.image checkbox, :width => 12, :position => 173
      pdf.move_down 10
      pdf.span pdf.bounds.right - 100, :position => 100 do
         pdf.text "Concurrent       Consecutive ___________________________________", :size => 14
      end
      pdf.move_up(14)
      pdf.image checkbox, :width => 12, :position => 85
      pdf.move_up(12)
      pdf.image checkbox, :width => 12, :position => 173
      
      pdf.move_down 10
      
      pdf.font "Helvetica", :style => :bold, :size => 14
      data = [["NOTES"]]
      pdf.table data, :border_width => 0, :row_colors => [SiteConfig.pdf_section_bg], :width => pdf.bounds.width, :align => :center, :padding => 1
      pdf.font "Times-Roman"
      
      (1..3).each do
         pdf.move_down 20
         pdf.stroke_horizontal_rule
      end
      pdf.move_down(5)
      pdf.text "Bond No: #{bonds.empty? ? 'N/A' : bonds.collect{|b| b.bond_no + ' (' + format_date(b.issued_date) + ')'}.join(',')}", :size => 11, :style => :bold

      unless chg == @court.docket.da_charges.reject{|c| !c.sentence.nil? || c.info_only? || c.dropped_by_da? }.last
         pdf.start_new_page
      end
   end
end
# end of report