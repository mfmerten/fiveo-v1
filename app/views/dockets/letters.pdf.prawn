# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   @bonds.each do |b|
      bond = b[0]
      court = b[1]
      pdf.move_down(80)
      if !bond.bondsman.nil?
         pdf.text bond.bondsman.name, :size => 12
         pdf.text bond.bondsman.company, :size => 12
         pdf.text bond.bondsman.address1, :size => 12
         pdf.text bond.bondsman.address2, :size => 12
      elsif !bond.surety.nil?
         pdf.text bond.surety.full_name(false), :size => 12
         pdf.text bond.surety.physical_line1, :size => 12
         pdf.text bond.surety.physical_line2, :size => 12
      else
         pdf.text "NO Bondsman OR Surety On File!", :size => 16, :style => :bold
      end
      pdf.move_down(30)
      pdf.text "Reference: #{show_person(bond.person, :name_only => true).upcase}", :size => 14
      pdf.move_down(25)
      pdf.text "Dear Sir/Madam:", :size => 14
      pdf.move_up(14)
      pdf.text "Court Date:   #{format_date(@court_date)}", :size => 14, :align => :right
      pdf.move_down(10)
      pdf.text "     I have been instructed by the District Attorney to notify you, as Bondsman, to have above subject in court on above date at #{@time} for #{court.nil? || court.court_type.nil? ? @dt.long_name.upcase : court.court_type.long_name.upcase} on Charge(s) of: #{bond.arrest.district_charge_summary}.", :size => 14
      pdf.move_down(10)
      pdf.text "     Failure of the defendent to appear in Eleventh Judicial District Court in the Sabine Parish Courthouse at the hour and on the date specified may result in an order to forfeit the bond posted in this case.", :size => 14
      pdf.move_down(15)
      pdf.span(pdf.bounds.right - 300, :position => 300) do
         pdf.text "Yours very truly,", :size => 14
         pdf.move_down(15)
         pdf.text SiteConfig.ceo, :size => 14
         pdf.move_down(10)
         pdf.text "Sheriff of Sabine Parish", :size => 14
      end
      pdf.text "By: Guynell Melton", :size => 14
      pdf.move_down(5)
      pdf.text "Deputy Sheriff", :size => 14
      unless b == @bonds.last
         pdf.start_new_page
      end
   end
end