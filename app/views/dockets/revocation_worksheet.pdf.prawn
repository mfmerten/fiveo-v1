# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@court.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.agency}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 35) do
   pdf.text "The 11th Judicial District Court", :size => 16, :style => :bold
   pdf.move_up(16)
   pdf.text "Judge: #{@court.judge.nil? ? '__________________' : @court.judge.long_name.upcase}", :size => 16, :style => :bold, :align => :right
   pdf.text "Parish of Sabine", :size => 16, :style => :bold
   pdf.move_up(16)
   pdf.text "Docket No: #{@court.docket.docket_no}", :size => 16, :style => :bold, :align => :right
   pdf.text "State Of Louisiana", :size => 16, :style => :bold
   pdf.move_up(16)
   pdf.text "Court Date: #{@court.court_date? ? format_date(@court.court_date) : '___________'}", :size => 16, :style => :bold, :align => :right
   pdf.pad(5){pdf.stroke_horizontal_rule}
   pdf.text "PROBATION REVOCATION", :size => 24, :style => :bold, :align => :center
   pdf.pad(5){pdf.stroke_horizontal_rule}
   pdf.move_down(5)
   pdf.text "#{show_person(@court.docket.person).upcase}", :size => 18, :style => :bold

   pdf.move_down(10)
   pdf.font "Helvetica", :style => :bold, :size => 14
   data = [["DOCKET INFORMATION"]]
   pdf.table data, :border_width => 0, :row_colors => [SiteConfig.pdf_section_bg], :width => pdf.bounds.width, :align => :center, :padding => 1
   pdf.font "Times-Roman"
   pdf.move_down(10)

   arrests = []
   citations = []
   unless @court.docket.da_charges.empty?
      @court.docket.da_charges.reject{|c| c.sentence.nil? }.each do |c|
         pdf.text "Charge: #{c.count} #{c.count == 1 ? 'count' : 'counts'} #{c.charge}", :size => 14, :style => :bold
         pdf.move_down(5)
         pdf.span(70){pdf.text "Sentence:", :size => 12, :align => :right, :style => :bold}
         pdf.move_up(12)
         txt = "On #{format_date(c.sentence.sentence_date)}, #{c.sentence.sentence_summary}"
         pdf.span(pdf.bounds.right - 75, :position => 75){pdf.text txt, :size => 12}
         pdf.move_down(5)
         unless c.arrest_charge.nil?
            unless c.arrest_charge.district_arrest.nil?
               arrests.push(c.arrest_charge.district_arrest)
            end
            unless c.arrest_charge.citation.nil?
               citations.push(c.arrest_charge.citation)
            end
         end
      end
   end
   
   pdf.move_down(10)
   pdf.font "Helvetica", :style => :bold, :size => 14
   data = [["ARREST INFORMATION"]]
   pdf.table data, :border_width => 0, :row_colors => [SiteConfig.pdf_section_bg], :width => pdf.bounds.width, :align => :center, :padding => 1
   pdf.font "Times-Roman"
   pdf.move_down(10)
   
   arrests.uniq.each do |a|
      pdf.text "Arrest Date: #{format_date(a.arrest_date, a.arrest_time)}    Arrest Agency: #{a.agency}", :size => 14, :style => :bold
      data = []
      a.district_charges.each do |c|
         data.push(["#{c.count} #{c.count == 1 ? 'cnt' : 'cnts'}", c.charge])
      end
      unless data.empty?
         pdf.move_down(5)
         pdf.span(70) do
            pdf.text"Charges:", :size => 12, :align => :right, :style => :bold
         end
         pdf.move_up(12)
         pdf.table data, :border_width => 0, :align => {0 => :left, 1 => :left}, :column_widths => {0 => 35, 1 => pdf.bounds.right - 110}, :padding => 1, :font_size => 12, :position => 70
      end
      data = []
      a.bonds.each do |b|
         data.push([b.bond_no, format_date(b.issued_date), b.surety_name, number_to_currency(b.bond_amt), "#{b.status.nil? ? '' : b.status.long_name}"])
      end
      unless data.empty?
         pdf.move_down(5)
         pdf.span(70) do
            pdf.text "Bonds:", :size => 12, :align => :right, :style => :bold
         end
         pdf.move_up(12)
         pdf.table data, :border_width => 0, :align => {0 => :left, 1 => :left, 2 => :left, 3 => :right, 4 => :left}, :padding => 1, :font_size => 12, :position => 70
      end
   end
   pdf.move_down(5)
   citations.uniq.each do |m|
      pdf.text "Citation Date: #{format_date(m.citation_date)}    Citation No: #{m.ticket_no}", :size => 14
      data = []
      m.charges.each do |c|
         data.push(["#{c.count} #{c.count == 1 ? 'cnt' : 'cnts'}", c.charge])
      end
      unless data.empty?
         pdf.move_down(5)
         pdf.span(70) do
            pdf.text"Charges:", :size => 12, :align => :right, :style => :bold
         end
         pdf.move_up(12)
         pdf.table data, :border_width => 0, :align => {0 => :left, 1 => :left}, :column_widths => {0 => 35, 1 => pdf.bounds.right - 110}, :padding => 1, :font_size => 12, :position => 70
      end
   end
   
   pdf.move_down(10)
   pdf.font "Helvetica", :style => :bold, :size => 14
   data = [["REVOCATION INFORMATION"]]
   pdf.table data, :border_width => 0, :row_colors => [SiteConfig.pdf_section_bg], :width => pdf.bounds.width, :align => :center, :padding => 1
   pdf.font "Times-Roman"
   pdf.move_down(10)
   
   checkbox = "#{RAILS_ROOT}/public/images/checkbox.png"
   
   pdf.span(pdf.bounds.width - 20, :position => 20) do
      pdf.text "D.O.C.          Parish", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12
   pdf.move_up(12)
   pdf.image checkbox, :width => 12, :position => 75
   pdf.move_down(10)
   pdf.span(pdf.bounds.width - 20, :position => 20) do
      pdf.text "Probation Revoked to serve original sentence with the following conditions:", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12
   pdf.move_down 10
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "Credit for time served", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12, :position => 32
   pdf.move_down 10
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "Deny Goodtime", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12, :position => 32
   pdf.move_down 10
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "Delay execution of sentence (see notes)", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12, :position => 32
   pdf.move_down 10
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "Concurrently with any time now serving", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12, :position => 32
   pdf.move_down 10
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "Consecutively with any time now serving", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12, :position => 32
   pdf.move_down 10
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "Concurrent with _______________________________________", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12, :position => 32
   pdf.move_down 10
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "Consecutive with _______________________________________", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12, :position => 32
   pdf.move_down 10
   pdf.move_down(5)
   pdf.span(pdf.bounds.right - 20, :position => 20) do
      pdf.text "Special Conditions of Probation:", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12
   pdf.move_down 10
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "Serve ______ Hours, ______ Days, ______ Months, ______ Years in Parish Jail", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12, :position => 32
   pdf.move_down 10
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "      Credit for time served", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12, :position => 32
   pdf.move_down 10
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "      Deny Goodtime", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12, :position => 32
   pdf.move_down(10)
   pdf.span(pdf.bounds.right - 50, :position => 50) do
      pdf.text "Delay execution of sentence (see notes)", :size => 14
   end
   pdf.move_up(14)
   pdf.image checkbox, :width => 12, :position => 32
   pdf.move_down 10
   pdf.font "Helvetica", :style => :bold, :size => 14
   data = [["NOTES"]]
   pdf.table data, :border_width => 0, :row_colors => [SiteConfig.pdf_section_bg], :width => pdf.bounds.width, :align => :center, :padding => 1
   pdf.font "Times-Roman"
   
   (1..4).each do
      pdf.move_down 25
      pdf.stroke_horizontal_rule
   end
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
