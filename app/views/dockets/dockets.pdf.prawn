# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 27) do
      pdf.font "Helvetica"
      pdf.text "#{@dt.long_name} Docket", :align => :center, :size => 24, :style => :bold
      pdf.move_up(20)
      pdf.text "#{@j.long_name}", :size => 16, :style => :bold
      pdf.move_up(18)
      pdf.text format_date(@court_date), :align => :right, :size => 16, :style => :bold
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{Date.today.to_s(:timestamp)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.agency}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 37], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 71) do
   data = []
   record = 1
   nl = "\n"
   sep = "\n\n\n\n\n"
   @courts.each do |c|
      blist = c.docket.bonds.collect{|b| "##{b.bond_no} [#{b.bond_type.nil? ? '?' : b.bond_type.long_name}] #{number_to_currency(b.bond_amt)} <#{b.status.nil? ? '?' : b.status.long_name}>"}
      adates = c.docket.arrests.collect{|a| "#{format_date(a.arrest_date, a.arrest_time)}"}.join(", ")
      data.push(["##{record}","#{show_person(c.docket.person, :include_id => false).upcase}#{nl}[#{c.docket.person.id}]","#{c.docket.docket_no}","#{adates.blank? ? '' : 'Arrest Date(s): ' + adates + nl}#{c.docket.charge_summary(true)}#{sep}","#{blist.join(' -- ')}#{sep}","#{c.court_type.nil? ? '' : c.court_type.long_name}"])
      record += 1
   end
   pdf.table data, :border_width => 1, :border_style => :grid, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :font_size => 12, :headers => ['#','Person','Docket','Charge(s)','Bonds','Court'], :column_widths => {0 => 30, 1 => 200, 2 => 55, 3 => 400, 4 => 180, 5 => 100}, :align => :left
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
