# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@call.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Case Number", :size => 9
   pdf.move_up(10)
   pdf.text "Case Number", :size => 9, :align => :right
   pdf.text @call.case_no, :size => 18, :style => :bold
   pdf.move_up(19)
   pdf.text @call.case_no, :size => 18, :style => :bold, :align => :right
   pdf.move_up(29)
   pdf.text "Initial Report", :align => :center, :size => 24, :style => :bold
   pdf.text format_date(@call.call_date, @call.call_time), :size => 14, :style => :bold, :align => :center
   data = [
      ["Signal:", "#{@call.signal_code.nil? ? '' : @call.signal_code.short_name}  #{@call.signal_code.nil? ? '' : @call.signal_code.long_name}" ],
      ["Received VIA:", "#{@call.received_via.nil? ? '' : @call.received_via.long_name}" ],
      ["Received By:", "#{show_contact @call, :received_by}"]
   ]
   @call.call_subjects.each do |s|
      data.push(["",""])
      data.push(["#{s.subject_type.nil? ? 'Unknown' : s.subject_type.long_name}:","#{s.name}\n#{s.address1}, #{s.address2}\nHome Phone: #{s.home_phone}  Cell Phone: #{s.cell_phone}  Work Phone: #{s.work_phone}"])
   end
   pdf.table data, :border_width => 0, :font_size => 12, :align => {0 => :right, 1 => :left}
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   pdf.text "Explanation of Call", :size => 18, :align => :center, :style => :bold
   pdf.move_down(5)
   pdf.text @call.details, :size => 12
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   pdf.text "Unit(s) Sent: #{@call.units}", :size => 12
   pdf.move_down(10)
   cols = pdf.bounds.width / 6
   cols = cols - 1
   data = [["Dispatched:", format_date(@call.dispatch_date, @call.dispatch_time), "Arrived:", format_date(@call.arrival_date, @call.arrival_time), "Concluded:", format_date(@call.concluded_date, @call.concluded_time)]]
   pdf.table data, :border_width => 0, :font_size => 12, :align => {0 => :right, 1 => :left, 2 => :right, 3 => :left, 4 => :right, 5 => :left}, :column_widths => {0 => (cols - 10), 1 => (cols + 14), 2 => (cols - 16), 3 => (cols + 14), 4 => (cols - 10), 5 => (cols + 14)}
   pdf.move_down(10)
   pdf.text "Disposition: #{@call.disposition.nil? ? '' : @call.disposition.long_name}", :size => 12, :style => :bold
   if @call.detective?
      pdf.move_down(10)
      pdf.text "Assigned To: #{@call.detective? ? show_contact(@call, :detective) : ''}", :size => 12
   end
   pdf.move_down(10)
   pdf.text "Dispatcher: #{show_contact @call, :dispatcher}", :size => 12, :style => :bold
   pdf.move_up(14)
   pdf.text "Ward: #{@call.ward}    District: #{@call.district}", :size => 12, :align => :right
end