# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(@time, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "IN-HOUSE MEMO", :align => :center, :size => 24, :style => :bold
   pdf.move_down(5)
   pdf.text "Dispatcher Shift Report", :align => :center, :size => 18, :style => :bold
   pdf.text "#{format_date(@date)} - Shift #{@shift} (#{@start}-#{@stop})", :size => 14, :style => :bold, :align => :center
   data = []
   sep = "\n"
   if @calls.empty?
      pdf.move_down(40)
      pdf.text "No Calls To Report During This Period", :size => 16, :align => :center
   else
      @calls.each do |c|
         txt = "Type: #{c.received_via.nil? ? 'Unknown' : c.received_via.long_name}     Signal: #{c.signal_code.nil? ? 'Unknown' : c.signal_code.short_name}     Dispatcher: #{show_contact c, :dispatcher}#{sep}"
         c.call_subjects.each do |s|
            txt << "#{s.subject_type.nil? ? 'Unknown' : s.subject_type.long_name}: #{s.name}#{s.home_phone? ? ' (Home: ' + s.home_phone + ')' : ''}#{s.cell_phone? ? ' (Cell: ' + s.cell_phone + ')' : ''}#{s.work_phone? ? ' (Work: ' + s.work_phone + ')' : ''}#{sep}"
         end
         unless c.units.blank?
            txt << "Units Dispatched: #{c.units}#{sep}"
         end
         unless c.disposition.nil?
            txt << "Disposition: #{c.disposition.long_name}#{sep}"
         end
         if c.detective?
            txt << "Detective Assigned: #{c.detective? ? show_contact(c, :detective) : ''}#{sep}"
         end
         txt << sep + "Details: " + c.details
         data.push(["#{format_date(c.call_date)}#{sep}#{c.call_time}",txt])
      end
      pdf.table data, :border_width => 1, :border_style => :grid, :headers => ['Date/Time','Call'], :padding => 1, :size => 12, :column_widths => {0 => 75, 1 => pdf.bounds.width - 80}
      pdf.move_down(5)
      pdf.text "End Of Report", :align => :center, :size => 18, :style => :bold
   end
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
