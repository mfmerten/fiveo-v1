# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
if @facility.nil?
   facility = "All Facilities"
else
   facility = @facility.short_name
end

btypes = ['Temporary Release', 'DOC', 'DOC Billable', 'City', 'Parish', 'Other', 'Not Billable']

# define spreadsheet columns
coln = 30
cols = 12
colt = 45
cold = 60
widths =  [ cold, cols, coln, coln, coln, coln, cols, coln, coln, coln, coln,  coln, cols,  coln,  cols, coln, coln, coln, coln,     colt, cols, coln, coln, coln, coln,     colt]
headers = ['Date',  ' ',  'D',  'P',  'C',  'O',  ' ', '*D',  'P',  'C',  'O', '§TR',  ' ', '*TT',   ' ',  'D',  'P',  'C',  'O',  'Total',  ' ',  'D',  'P',  'C',  'O',  'Total']
# positions for headers (and populate blank_line)
blank_line = []
positions = []
widths.size.times do |x|
   blank_line.push(' ')
   if x == 0
      positions.push(0)
   else
      positions.push(positions[x-1] + widths[x-1])
   end
end

# header
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 50) do
      pdf.text "Monthly Billing Report: #{facility}", :align => :center, :size => 18, :style => :bold
      pdf.move_up(18)
      pdf.text "#{@start_date.strftime("%B")} #{@start_date.strftime("%Y")}#{@stop_date == @stop_date.end_of_month ? '' : ' MTD'}", :size => 16
      pdf.move_up(16)
      pdf.text "#{@start_date.strftime("%B")} #{@start_date.strftime("%Y")}#{@stop_date == @stop_date.end_of_month ? '' : ' MTD'}", :size => 16, :align => :right
      
      # leave space for the page title stamp
   end
end

# footer
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(@stop_date, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

# page header stamps
sections = btypes.clone
sections.push("Summary by Date")
sections.each do |section|
   pdf.create_stamp(section) do
      pdf.canvas do
         # calculate size of text
         if section == "Not Billable"
            section_text = "Not Billable (Temporary Transfers)"
         else
            section_text = section
         end
         section_size = pdf.width_of(section_text, :size => 16, :style => :bold)
         section_position = ((pdf.bounds.width / 2) - (section_size / 2)).to_i
         section_y = pdf.bounds.top - 55
         pdf.draw_text(section_text, :at => [section_position, section_y], :size => 16, :style => :bold)
         if section == "Summary by Date"
            # table group headers
            pdf.move_down(65)
            row_pos = pdf.y
            pdf.span(widths[2] + widths[3] + widths[4] + widths[5], :position => positions[2]) do
               pdf.text 'Booked', :style => :bold, :align => :center, :size => 12
               pdf.move_up(2)
               pdf.stroke_horizontal_rule
            end
            pdf.move_up(row_pos - pdf.y)
            pdf.span(widths[7] + widths[8] + widths[9] + widths[10] + widths[11], :position => positions[7]) do
               pdf.text 'Released', :style => :bold, :align => :center, :size => 12
               pdf.move_up(2)
               pdf.stroke_horizontal_rule
            end
            pdf.move_up(row_pos - pdf.y)
            pdf.span(widths[13], :position => positions[13]) do
               pdf.text 'Trans', :style => :bold, :align => :center, :size => 12
               pdf.move_up(2)
               pdf.stroke_horizontal_rule
            end
            pdf.move_up(row_pos - pdf.y)
            pdf.span(widths[15] + widths[16] + widths[17] + widths[18] + widths[19], :position => positions[15]) do
               pdf.text 'Physical Count', :style => :bold, :align => :center, :size => 12
               pdf.move_up(2)
               pdf.stroke_horizontal_rule
            end
            pdf.move_up(row_pos - pdf.y)
            pdf.span(widths[21] + widths[22] + widths[23] + widths[24] + widths[25], :position => positions[21]) do
               pdf.text 'Billing Count', :style => :bold, :align => :center, :size => 12
               pdf.move_up(2)
               pdf.stroke_horizontal_rule
            end
         end
      end
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 70], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 95) do
   btypes.each do |t|
      pdf.on_page_create{ pdf.stamp(t) }
      pdf.start_new_page
      data = @data.rows(t)
      if data.empty?
         pdf.move_down(40)
         pdf.text "Nothing To Report", :align => :center, :size => 14
      else
         pdf.table data, :border_width => 1, :border_style => :underline_header, :font_size => 12, :column_widths => {0 => 70, 1 => 70, 2 => 225, 3 => 70, 4 => 70, 5 => 70, 6 => 70, 7 => 40, 8 => 40}, :headers => ['Book#', 'ID', 'Name', 'Booked', 'Released', 'Start', 'Stop', 'Phys', 'Bill'], :align => {0 => :right, 1 => :right, 2 => :left, 3 => :left, 4 => :left, 5 => :left, 6 => :left, 7 => :right, 8 => :right}, :vertical_padding => 2, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
      end
   end
   
   pdf.on_page_create { pdf.stamp("Summary by Date") }
   pdf.start_new_page

   cwidths = {}
   widths.size.times do |x|
      cwidths[x] = widths[x]
   end

   pdf.table @data.summary, :border_width => 1, :border_style => :underline_header, :column_widths => cwidths, :headers => headers, :align => :right, :padding => 1, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
   pdf.move_down(5)
   pdf.text "* Not Billable: Last Day DOC or Non-billable Temporary Transfer,  § Temporary Release", :size => 10
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
