# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@booking.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Time Log for Booking ID: #{@booking.id}", :align => :center, :size => 20, :style => :bold
   pdf.text "#{show_person @booking.person}", :align => :center, :size => 18, :style => :bold
   pdf.text "As Of: #{DateTime.now.to_s(:us)}", :align => :center, :size => 14, :style => :bold
   pdf.move_down(8)
   # Build an array and let prawn page the results
   data = []
   total_hours = 0
   @booking.booking_logs.each do |l|
      total_hours += l.hours_served
      data.push([format_date(l.start_datetime),"#{show_contact l, :start_officer, :include_badge => false}",format_date(l.stop_datetime),"#{l.stop_datetime? ? show_contact(l, :stop_officer, :include_badge => false) : ' '}",l.hours_served])
   end
   data.push([" "," "," ","Total Hours:",total_hours])
   pdf.table data, :align => :left, :column_widths => {0 => 95, 1 => 170, 2 => 95, 3 => 170, 4 => 50}, :border_width => 1, :border_style => :underline_header, :headers => ["Started", "By", "Stopped", "By", "Hours"], :font_size => 11, :row_colors => [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], :vertical_padding => 2, :position => 0
   pdf.move_down(8)
   pdf.text "END OF REPORT", :style => :bold
end

pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
