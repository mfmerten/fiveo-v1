# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
this_instant = DateTime.now
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(this_instant, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

head_count = @bookings.size
phys_count = head_count

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text @page_title, :align => :center, :size => 24, :style => :bold
   pdf.text format_date(this_instant), :align => :center, :size => 14, :style => :bold
   pdf.move_down(8)
   # Build an array and let prawn page the results
   data = []
   @bookings.each do |book|
      status = ""
      gone = false
      if book.doc?
         status << "[DOC]"
      end
      if book.release_date?
         unless status.empty?
            status << " "
         end
         status << "[Released]"
         gone = true
      end
      if !book.is_transferred.nil?
         unless status.empty?
            status << " "
         end
         status << "[Temp Transfer]"
         gone = true
      end
      if booking_location_or_link(book.id, :supervisor => false, :html => false) =~ /^Absent: /
         gone = true
      end
      if book.temporary_release?
         gone = true
      end
      if gone
         phys_count -= 1
      end
      data.push([book.id, show_person(book.person), booking_location_or_link(book.id, :supervisor => false, :html => false), status])
   end
   pdf.table data, :align => :left, :column_widths => {0 => 75, 1 => 250, 2 => 175, 3 => 75}, :border_width => 1, :border_style => :underline_header, :headers => ["Booking", "Inmate", "Current Location", "Status"], :font_size => 14, :row_colors => [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], :vertical_padding => 2, :position => 0
   pdf.move_down(8)
   pdf.text "END OF REPORT -- Booking Records: #{head_count}, Physical Head Count: #{phys_count}", :style => :bold
end

pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
