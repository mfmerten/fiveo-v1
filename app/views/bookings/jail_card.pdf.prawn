# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@booking.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.agency}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 35) do
   pdf.text "#{@booking.facility.nil? ? "Invalid Facility!" : @booking.facility.long_name}", :align => :center, :size => 18, :style => :bold
   pdf.text "Jail Card", :align => :center, :size => 24, :style => :bold
   pdf.stroke_horizontal_rule
   pdf.move_down(5)
   pdf.text show_person(@person), :size => 18, :style => :bold
   pdf.text "#{@person.aka? ? 'Known Aliases: ' + @person.aka : 'No Known Aliases'}", :size => 14
   pdf.stroke_horizontal_rule
   
   row_pos = pdf.bounds.top - 100
   
   pdf.text_field [pdf.col(2,2), row_pos], pdf.col(2), 'Booking Officer:', "#{show_contact @booking, :booking_officer}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   pdf.text_field [pdf.col(4,1), row_pos], pdf.col(4), 'Booking ID:', "#{@booking.id}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   row_pos -= pdf.text_field [pdf.col(4,2), row_pos], pdf.col(4), 'Booking Date/Time:', format_date(@booking.booking_date, @booking.booking_time), SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   pdf.text_field [pdf.col(2,2), row_pos], pdf.col(2), "Emergency Contact:", "#{@person.emergency_contact}\n#{@person.em_relationship.nil? ? '' : @person.em_relationship.long_name}\n#{@person.emergency_phone}", SiteConfig.pdf_em_bg, SiteConfig.pdf_em_txt, 3
   pdf.text_field [pdf.col(4,1), row_pos], pdf.col(4), "Date of Birth:",format_date(@person.date_of_birth), SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   row_pos -= pdf.text_field [pdf.col(4,2), row_pos], pdf.col(4), 'SSN:', "#{@person.ssn} ", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   pdf.text_field [pdf.col(4,2), row_pos], pdf.col(4), 'Sex:', "#{@person.sex_name}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   row_pos -= pdf.text_field [pdf.col(4,1), row_pos], pdf.col(4), 'Race:', "#{@person.race.nil? ? '' : @person.race.long_name}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   row_pos -= pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), 'Phone Called:', "#{@booking.phone_called}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   row_pos -= pdf.text_field [pdf.col(1,1), row_pos], pdf.col(1), 'Medications:', "#{@person.medication_summary}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 2
   
   pdf.pad(10){pdf.stroke_horizontal_rule}
   
   pdf.text "Cash And Items Received", :size => 16, :style => :bold, :align => :center
   row_pos -= 35
   (-1..@booking.properties.size + 1).each do |i|
      next if i.even?
      if i == -1
         # cash
         pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), 'Cash:', number_to_currency(@booking.cash_at_booking), SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
      else
         # property
         pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), 'Property:', "#{@booking.properties[i].nil? ? '' : '(' + @booking.properties[i].quantity.to_s + ') ' + @booking.properties[i].description}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
      end
      # property
      row_pos -= pdf.text_field [pdf.col(2,2), row_pos], pdf.col(2), 'Property:', "#{@booking.properties[i+1].nil? ? '' : '(' + @booking.properties[i+1].quantity.to_s + ') ' + @booking.properties[i+1].description}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   end
   
   unless @arrests.empty? && @sentences.empty? && @other.empty?
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
   end
   
   unless @arrests.empty?
      pdf.move_down(10)
      pdf.text "Current Arrests", :size => 16, :style => :bold, :align => :center
      arrs = @arrests.collect{|h| h.arrest}.compact.uniq
      data = []
      arrs.each do |a|
         data.push([a.id, format_date(a.arrest_date, a.arrest_time),a.agency, "#{a.charge_summary}\nHolds: #{a.current_holds}"])
      end
      unless data.empty?
         pdf.table data, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 50, 1 => 100, 2 => 125, 3 => 290}, :font_size => 12, :vertical_padding => 1, :headers => ['Arr. ID','Date/Time','Agency','Charges'], :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
      end
   end
   
   unless @sentences.empty?
      pdf.move_down(10)
      pdf.text "Current Sentences as of #{DateTime.now.to_s(:us)}", :size => 16, :style => :bold, :align => :center
      data = []
      @sentences.each do |h|
         text = ""
         if !h.sentence.nil?
            text << "#{h.doc? ? '[DOC] ' : ''}#{h.sentence.conviction}(#{h.sentence.conviction_count})"
         elsif !h.doc_record.nil?
            text << "#{h.doc_record.conviction? ? '[DOC] ' + h.doc_record.conviction : '[DOC] Convictions Not On File'}"
         elsif !h.revocation.nil?
            probs = h.revocation.docket.probations.collect{|p| p.conviction + "(#{p.conviction_count})"}
            if probs.blank?
               text << "Revocation: #{h.doc? ? '[DOC] ' : ''}Convictions Not On File"
            else
               text << "Revocation: #{h.doc? ? '[DOC] ' : ''}#{probs.join(', ')}"
            end
         else
            text << "Convictions Not On File"
         end
         data.push([h.id, format_date(h.hold_date, h.hold_time), text, h.hours_remaining])
      end
      unless data.empty?
         pdf.table data, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 50, 1 => 100, 2 => 350, 3 => 65}, :font_size => 12, :vertical_padding => 1, :headers => ['Hold ID','Date/Time','Convicted Of','Hours Left'], :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
      end
   end
   
   unless @other.empty?
      pdf.move_down(10)
      pdf.text "Other Relevant Holds", :size => 16, :style => :bold, :align => :center
      data = []
      @other.each do |h|
         data.push([h.id, format_date(h.hold_date, h.hold_time), "#{h.hold_type.nil? ? 'Invalid Type' : h.hold_type.long_name} ","#{h.agency? ? h.agency : ' '}","#{h.hold_by_unit} #{h.hold_by}"])
      end
      unless data.empty?
         pdf.table data, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 50, 1 => 100, 2 => 125, 3 => 125, 4 => 165}, :font_size => 12, :vertical_padding => 1, :headers => ['Hold ID','Date/Time','Type','For','By'], :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
      end
   end
   
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   
   pdf.text "Booking Statement", :size => 18, :style => :bold, :align => :center
   pdf.move_down(10)
   pdf.text "(1) I have been advised of the charges against me.", :size => 12, :style => :bold
   pdf.text "(2) All property and/or money listed above is in the custody of #{SiteConfig.agency} and will be returned upon my leaving.", :size => 12, :style => :bold
   pdf.text "(3) I have been allowed my right to make a telephone call.", :size => 12, :style => :bold
   pdf.move_down(10)
   pdf.span(pdf.bounds.width - 40, :position => 20) do
      pdf.text "This will serve as notice to the undersigned that any personal property and/or money not claimed within 90 days after the inmate is released from #{SiteConfig.agency} becomes the property of #{SiteConfig.agency}.  There will be no further notice as the undersigned acknowledges his/her understanding of this provision by their signature.", :size => 12, :style => :bold
   end
   pdf.move_down(35)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text "Inmate Signature", :size => 9
   end
   
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   
   pdf.text "Release Statement", :size => 18, :style => :bold, :align => :center
   pdf.move_down(10)
   pdf.text "I have received all property and/or money from #{SiteConfig.agency}.", :size => 14, :style => :bold
   pdf.move_down(35)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text "Inmate Signature", :size => 9
   end
   pdf.move_up(10)
   pdf.stroke_horizontal_line(pdf.col(4,1), pdf.col(4,1) + pdf.col(4))
   pdf.stroke_horizontal_line(pdf.col(4,2), pdf.col(4,2) + pdf.col(4))
   pdf.span(pdf.col(4), :position => pdf.col(4,1)) do
      pdf.text "Release Date", :size => 9
   end
   pdf.move_up(9)
   pdf.span(pdf.col(4), :position => pdf.col(4,2)) do
      pdf.text "Release Time", :size => 9
   end
end

pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
