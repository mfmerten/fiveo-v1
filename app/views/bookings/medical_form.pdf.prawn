# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@booking.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "#{@medical.booking.nil? ? 'Invalid Booking!' : (@medical.booking.facility.nil? ? 'Invalid Facility!' : @medical.booking.facility.long_name)}", :align => :center, :size => 18, :style => :bold
   pdf.text @page_title, :align => :center, :size => 24, :style => :bold
   pdf.text format_date(@medical.screen_date, @medical.screen_time), :align => :center, :size => 14, :style => :bold
   pdf.move_down(8)
   pdf.text "#{@medical.booking.nil? ? 'Invalid Booking' : show_person(@medical.booking.person)}", :size => 20, :style => :bold, :align => :left
   pdf.move_up(18)
   pdf.text "Booking ID: #{@medical.booking.nil? ? '' : @medical.booking.id}", :size => 16, :align => :right
   
   data = [["#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : (@medical.booking.person.race.nil? ? '' : @medical.booking.person.race.long_name))} ", "#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.sex_name)} ", "#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : format_date(@medical.booking.person.date_of_birth))} ", "#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.ssn)} "]]
   pdf.table data, :border_width => 0, :headers => ['Race','Sex','Date of Birth','Social Security'], :header_text_color => SiteConfig.pdf_header_txt, :header_color => SiteConfig.pdf_header_bg, :width => pdf.bounds.width, :vertical_padding => 1
   pdf.move_down(5)
   
   data = [["#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.emergency_contact)} ","#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : (@medical.booking.person.em_relationship.nil? ? '' : @medical.booking.person.em_relationship.long_name))} ", "#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.emergency_address)} ", "#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.emergency_phone)} "]]
   pdf.table data, :border_width => 0, :headers => ['Emergency Contact','Relationship','Address','Phone'], :header_text_color => SiteConfig.pdf_em_txt, :header_color => SiteConfig.pdf_em_bg, :width => pdf.bounds.width, :vertical_padding => 1
   pdf.move_down(5)
   
   data = [['']]
   pdf.table data, :border_width => 0, :headers => ['CONDITIONS IDENTIFIED'], :align_headers => :center, :header_text_color => SiteConfig.pdf_section_txt, :header_color => SiteConfig.pdf_section_bg, :width => pdf.bounds.width, :font_size => 16, :vertical_padding => 1
   
   contact = false
   data = []
   if @medical.arrest_injuries?
      data.push(["Injuries During Arrest", @medical.arrest_injuries])
      contact = true
   end
   if @medical.rec_med_tmnt?
      data.push(["Receiving Medical Treatment", @medical.rec_med_tmnt])
      contact = true
   end
   if @medical.rec_dental_tmnt?
      data.push(["Receiving Dental Treatment", @medical.rec_dental_tmnt])
      contact = true
   end
   if @medical.rec_mental_tmnt?
      data.push(["Receiving Mental Treatment", @medical.rec_mental_tmnt])
      contact = true
   end
   if @medical.current_meds?
      data.push(["Current Medications", @medical.current_meds])
      contact = true
   end
   if @medical.allergies?
      data.push(["Drug Allergies", @medical.allergies])
      contact = true
   end
   if @medical.injuries?
      data.push(["Bleeding/Injuries/Illness/Pain", @medical.injuries])
      contact = true
   end
   if @medical.infestation?
      data.push(["Infestations/Lesions", @medical.infestation])
      contact = true
   end
   if @medical.alcohol?
      data.push(["Under the Influence of Alcohol", @medical.alcohol])
      contact = true
   end
   if @medical.drug?
      data.push(["Under the Influence of Drugs", @medical.drug])
      contact = true
   end
   if @medical.withdrawal?
      data.push(["Exhibits Withdrawal Symptoms", @medical.withdrawal])
      contact = true
   end
   if @medical.attempt_suicide?
      data.push(["Has Considered or Attempted Suicide", @medical.attempt_suicide])
      contact = true
   end
   if @medical.suicide_risk?
      data.push(["Possible Suicide Risk", @medical.suicide_risk])
      contact = true
   end
   if @medical.danger?
      data.push(["Possible Danger to Self or Others", @medical.danger])
      contact = true
   end
   if @medical.pregnant?
      data.push(["Pregnant", @medical.pregnant])
      contact = true
   end
   if @medical.deformities?
      data.push(["Requires Cane/Crutches/Cast/Braces", @medical.deformities])
      contact = true
   end
   if data.empty?
      data = [['','']]
   end
   pdf.table data, :border_width => 1, :border_style => :underline_header, :headers => ["Condition", "Details"], :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :position => 10, :width => pdf.bounds.width - 20
   pdf.move_down(5)
   
   data = [['']]
   pdf.table data, :border_width => 0, :headers => ['ADDITIONAL CONDITIONS IDENTIFIED'], :align_headers => :center, :header_text_color => SiteConfig.pdf_section_txt, :header_color => SiteConfig.pdf_section_bg, :width => pdf.bounds.width, :vertical_padding => 1
   
   data = []
   data << (@medical.heart_disease? ? "Heart Disease" : nil)
   data << (@medical.blood_pressure? ? "High Blood Pressure" : nil)
   data << (@medical.diabetes? ? "Diabetes" : nil)
   data << (@medical.epilepsy? ? "Epilepsy" : nil)
   data << (@medical.hepatitis? ? "Hepatitis" : nil)
   data << (@medical.hiv? ? "HIV/AIDS" : nil)
   data << (@medical.tb? ? "Tuberculosis" : nil)
   data << (@medical.ulcers? ? "Ulcers" : nil)
   data << (@medical.venereal? ? "Venereal Disease" : nil)
   unless data.compact.empty?
      contact = true
   end
   pdf.span(pdf.bounds.width - 20, :position => 10) do
      pdf.text data.compact.join(', '), :size => 12
   end
   pdf.move_down(5)
   
   pdf.stroke_horizontal_rule
   
   pdf.move_down(20)
   pdf.text "Disposition: #{@medical.disposition}", :size => 14, :style => :bold
   pdf.move_down(10)
   pdf.text "Screening Officer: #{show_contact @medical, :officer}", :size => 14
   if contact == true
      pdf.fill_color = SiteConfig.pdf_em_txt
      pdf.move_down(10)
      pdf.text "Contact the medic!", :size => 20, :style => :bold
      pdf.fill_color = '000000'
   end
end

pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
