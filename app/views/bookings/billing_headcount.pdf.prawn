# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(@report_date, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   btypes = ["Temporary Release","DOC","DOC Billable","City","Parish","Other","Not Billable"]
   totals = {'count' => 0, 'billable' => 0, 'physical' => 0, 'released' => 0, 'booked' => 0, 'transferred' => 0}
   if @facility.nil?
      facility = "All Facilities"
   else
      facility = @facility.long_name
   end
   btypes.each do |t|
      totals[t] = {'count' => 0, 'billable' => 0, 'physical' => 0, 'released' => 0, 'booked' => 0, 'transferred' => 0}
      total = 0
      physical = 0
      billable = 0
      released = 0
      booked = 0
      transferred = 0
      data = []
      @data[t].each do |b|
         total += 1
         totals[t]['count'] += 1
         totals['count'] += 1
         unless t == "Temporary Release" || t == 'Not Billable' || (t == 'DOC Billable' && b.release_date? && b.release_date == @report_date)
            billable += 1
            totals[t]['billable'] += 1
            totals['billable'] += 1
         end
         if b.release_date? && b.release_date == @report_date
            released += 1
            totals[t]['released'] += 1
            totals['released'] += 1
         end
         if b.booking_date? && b.booking_date == @report_date
            booked += 1
            totals[t]['booked'] += 1
            totals['booked'] += 1
         end
         agency = b.is_transferred(@report_datetime)
         if agency.nil?
            agency = ' '
         else
            transferred += 1
            totals[t]['transferred'] += 1
            totals['transferred'] += 1
         end
         if agency == ' ' && (!b.release_date? || b.release_date != @report_date) && t != 'Temporary Release'
            physical += 1
            totals[t]['physical'] += 1
            totals['physical'] += 1
         end
         if t == 'Other' && agency == ' '
            agency = b.billing_status(@report_date)
         end
         if t == "Temporary Release"
            data.push([b.id, show_person(b.person), format_date(b.booking_date), "Temporary Release", b.temporary_release_reason])
         else
            data.push([b.id, show_person(b.person), format_date(b.booking_date), "#{b.release_date? && b.release_date == @report_date ? (b.released_because.nil? ? '' : b.released_because.long_name) + ': ' + format_date(b.release_date) : ' '}", agency])
         end
      end
      pdf.text "Daily Headcount for #{facility}", :align => :center, :size => 20, :style => :bold
      pdf.move_down(5)
      pdf.text "#{t == 'Not Billable' ? 'Not Billable (Temporary Transfers)' : t}", :align => :center, :size => 18, :style => :bold
      pdf.move_down(5)
      pdf.text format_date(@report_date), :size => 12, :style => :bold, :align => :center
      pdf.move_down(5)
      if data.empty?
         pdf.move_down(60)
         pdf.text "Nothing to report.", :size => 12, :style => :bold, :align => :center
         pdf.move_down(60)
      else
         pdf.table data, :border_width => 1, :align => :left, :border_style => :underline_header, :headers => ['Booking#','Inmate','Booked','Released',"#{t == 'Other' ? 'Billable To' : 'Transferred To'}"], :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 1, :width => pdf.bounds.width
         pdf.move_down(5)
         data = [["#{t}:",total,booked,released,transferred,physical,billable]]
         pdf.table data, :border_width => 1, :align => :right, :border_style => :underline_header, :headers => [' ','Count', 'Booked', 'Released', 'Transferred', 'Physical', 'Billable'], :padding => 2, :width => pdf.bounds.width
      end
      pdf.start_new_page
   end

   # do the summary page
   pdf.text "Daily Headcount for #{facility}", :align => :center, :size => 20, :style => :bold
   pdf.move_down(5)
   pdf.text "REPORT SUMMARY", :align => :center, :size => 18, :style => :bold
   pdf.move_down(5)
   pdf.text format_date(@report_date), :size => 12, :style => :bold, :align => :center
   pdf.move_down(5)

   data = []
   btypes.each do |t|
      data.push(["#{t == 'Not Billable' ? '*' : ''}#{t}:",totals[t]['count'],totals[t]['booked'],totals[t]['released'],totals[t]['transferred'],totals[t]['physical'],totals[t]['billable']])
   end
   data.push(["Grand Totals:",totals['count'],totals['booked'],totals['released'],totals['transferred'],totals['physical'],totals['billable']])
   pdf.table data, :border_width => 1, :border_style => :underline_header, :headers => [' ','Count','Booked','Released','Transferred','Physical','Billable'], :align => :right, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 1, :width => pdf.bounds.width
   pdf.move_down(5)
   pdf.text "*These are non-billable temporary transfers.", :size => 10

   # add a page for the transfer log for that day
   pdf.start_new_page
   pdf.text "Daily Headcount for #{facility}", :align => :center, :size => 20, :style => :bold
   pdf.move_down(5)
   pdf.text "TRANSFER LOG", :align => :center, :size => 18, :style => :bold
   pdf.move_down(5)
   pdf.text format_date(@report_date), :size => 12, :style => :bold, :align => :center
   pdf.move_down(5)
   if @data['Transfers'].empty?
      pdf.move_down(50)
      pdf.text "Nothing To Report", :size => 14, :align => :center
   else
      data = []
      @data['Transfers'].each do |t|
         data.push(["#{t.booking.nil? ? '' : t.booking.id}", show_person(t.person), "#{t.transfer_type.nil? ? '' : t.transfer_type.long_name}", t.from_agency, t.to_agency])
      end
      pdf.table data, :border_width => 1, :align => :left, :border_style => :underline_header, :headers => ['Booking','Name','Type','From','To'], :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 1, :width => pdf.bounds.width
   end   
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
