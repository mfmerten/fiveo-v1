# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@booking.person_id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Official Notice / Order to Appear for Arraignment", :size => 24, :style => :bold, :align => :center
   pdf.move_down(5)
   pdf.text "State of Louisiana", :size => 14, :style => :bold
   pdf.text "VS", :size => 14, :style => :bold
   pdf.text @booking.person.full_name.upcase, :size => 14, :style => :bold
   pdf.move_up(48)
   pdf.span(175, :position => pdf.bounds.width - 175) do
      pdf.text "11th Judicial District Court", :size => 14, :style => :bold
      pdf.text "Sabine Parish", :size => 14, :style => :bold
      pdf.text "State Of Louisiana", :size => 14, :style => :bold
   end
   pdf.move_down(20)
   pdf.text "YOU ARE ORDERED", :size => 14, :style => :bold
   pdf.move_up(15)
   pdf.span(pdf.bounds.width - 140, :position => 140) do
      pdf.text "to appear in the Sabine Parish Courthouse in Many, Louisiana,", :size => 14
   end
   pdf.move_down(5)
   pdf.text "on the ______ day of ________________, _________, at 9:00 AM for arraignment on the charge /", :size => 14
   pdf.move_down(5)
   pdf.text "charges of: " + @charges, :size => 14
   pdf.move_down(20)
   pdf.span(pdf.bounds.width - 100, :position => 50) do
      pdf.text "FAILURE TO APPEAR FOR THIS HEARING WILL RESULT IN A BENCH WARRANT BEING ISSUED FOR YOUR ARREST AND YOUR BOND BEING FORFEITED!", :size => 18, :style => :bold
   end
   pdf.move_down(20)
   pdf.text "Thus Done and Signed", :size => 14, :style => :bold
   pdf.move_up(15)
   pdf.span(pdf.bounds.width - 145, :position => 145) do
      pdf.text "on this ______ day of ________________, _________.", :size => 14
   end
   pdf.move_down(50)
   pdf.stroke_horizontal_line(pdf.col(2,1), pdf.col(2,1) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,1)) do
      pdf.text "Defendant", :size => 9
   end
   pdf.move_up(10)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text "Officer / Deputy", :size => 9
   end
   pdf.move_down(40)
   pdf.stroke_horizontal_line(pdf.col(2,1), pdf.col(2,1) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,1)) do
      pdf.text "Bondsman and/or Surety", :size => 9
   end
   pdf.move_down(20)
   pdf.stroke_horizontal_rule
   pdf.move_down(10)
   pdf.text "Bond Information", :size => 18, :style => :bold, :align => :center
   pdf.move_down(5)
   @arrests.each do |a|
      pdf.text "Arrest ID: #{a.id}    Date/Time: #{format_date(a.arrest_date, a.arrest_time)}    Bond Set By: #{a.hour_72_judge.nil? ? '' : a.hour_72_judge.long_name}", :size => 14
      data = []
      total_amt = BigDecimal.new('0.00',2)
      a.bonds.reject{|b| b.status.nil? || b.status.long_name != 'Active'}.each do |b|
         data.push([b.bond_no, b.surety_name, "#{b.bond_type.nil? ? '' : b.bond_type.long_name}", number_to_currency(b.bond_amt)])
         total_amt += b.bond_amt
      end
      if data.empty?
         pdf.span(pdf.bounds.width - 50, :position => 50) do
            pdf.text "No Active Bonds On Record", :size => 18, :style => :bold, :align => :center
         end
      else
         data.push([" "," ","TOTAL BOND =>",number_to_currency(total_amt)])
         pdf.table data, :border_width => 1, :border_style => :underline_header, :align => {0 => :left, 1 => :left, 2 => :left, 3 => :right}, :width => pdf.bounds.width - 50, :position => 50, :headers => ['Bond No', 'Bondsman/Surety', 'Type', 'Amount'], :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]
      end
      pdf.move_down(5)
   end
   pdf.move_down(10)
   pdf.stroke_horizontal_rule
   pdf.move_down(10)
   pdf.text "Address Information", :size => 18, :style => :bold, :align => :center
   pdf.move_down(5)
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.fill_color SiteConfig.pdf_em_txt
      pdf.text "This Section Must Be Completed", :size => 14, :style => :bold, :align => :center
      pdf.fill_color '000000'
   end
   pdf.move_down(5)
   sep = "\n"
   row_pos = pdf.y - 30
   pdf.text_field [pdf.col(2,2), row_pos], pdf.col(2), 'Physical (911) Service Address:', " ", SiteConfig.pdf_em_bg, SiteConfig.pdf_em_txt, 4
   row_pos -= pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), 'Physical Address On File:', "#{@booking.person.physical_line1}#{sep}#{@booking.person.physical_line2}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 2
   row_pos -= pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), 'Mailing Address On File:', "#{@booking.person.mailing_line1}#{sep}#{@booking.person.mailing_line2}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 2
   pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), 'Phone(s) On File:', "#{@booking.person.home_phone? ? 'Home: ' + @booking.person.home_phone + ' ' : ''}#{@booking.person.cell_phone? ? 'Cell: ' + @booking.person.cell_phone : ''}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1
   pdf.text_field [pdf.col(2,2), row_pos], pdf.col(2), 'Valid Phone Number:', " ", SiteConfig.pdf_em_bg, SiteConfig.pdf_em_txt, 1
end

pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
