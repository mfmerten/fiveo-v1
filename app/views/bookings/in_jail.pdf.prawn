# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 30) do
      pdf.font "Helvetica"
      pdf.text "Monthly In-Jail Report", :align => :center, :size => 20, :style => :bold
      pdf.move_up(16)
      pdf.text format_date(@report_date), :align => :right, :size => 14, :style => :bold
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(@report_date, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "#{SiteConfig.agency}", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 45], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 75) do
   data = []
   # populate the table
   @bookings.each do |b|
      # skip this unless there are pending charges (or probation holds)
      valid_holds = ['Probation (Parish)', 'Probation/Parole (State)', '48 Hour', '72 Hour', 'Bondable', 'No Bond', 'Hold for City', 'Hold for Other Agency']
      next if b.holds.reject{|h| h.cleared_date? || (!h.hold_type.nil? && !valid_holds.include?(h.hold_type.long_name))}.empty?

      # collect a list of all active holds
      holds = b.holds.reject{|h| h.cleared_date?}
      
      # collect a unique list of arrests linked by all active holds
      arrests = holds.collect{|h| h.arrest}.compact.uniq
      
      # gather list of holds without arrest
      non_arrest_holds = holds.reject{|h| !h.arrest.nil?}
      
      # Type (0)
      classification = b.billing_status
      if classification == "DOC" || classification == "DOC Billable"
         b_class = "D"
      elsif classification == "City"
         b_class = "C"
      elsif classification == "Parish"
         b_class = "P"
      elsif classification == ""
         b_class = "P"
      else
         b_class = "O"
      end

      # Docket / Court (1)
      # leave blank
   
      # Name (2)
      b_name = show_person(b.person)
      
      h_text = ""
      
      # first, list each active arrest and the holds for them
      unless arrests.empty?
         arrests.each do |a|
            h_text << "Arrested: #{format_date(a.arrest_date, a.arrest_time)} by #{a.agency? ? a.agency : 'Unknown Agency'} on the charges of:\n"
            # list the charges
            a.district_charges.each do |c|
               h_text << "--- #{c.charge}(#{c.count})#{c.arrest_warrant.nil? ? '' : ' [Warrant: ' + c.arrest_warrant.warrant_no + ']'}\n"
            end
            a.city_charges.each do |c|
               case
               when !c.arrest_warrant.nil?
                  h_text << "--- City Warrant: #{c.arrest_warrant.jurisdiction.fullname} Warrant #{c.arrest_warrant.warrant_no}: "
               when !c.agency.nil?
                  h_text << "--- City Charge: #{c.agency.fullname}: "
               end
               h_text << "#{c.charge}(#{c.count})\n"
            end
            a.other_charges.each do |c|
               if c.arrest_warrant.nil?
                  h_text << "--- Unknown Warrant Information: "
               else
                  h_text << "--- Other Warrant: #{c.arrest_warrant.jurisdiction.fullname} Warrant #{c.arrest_warrant.warrant_no}: "
               end
               h_text << "#{c.charge}(#{c.count})\n"
            end
            
            # set up bond string
            if a.hour_72_date?
               if a.bondable?
                  bond_string = "Bond Set at #{number_to_currency(a.bond_amt)}"
               else
                  bond_string = "Bond Denied"
               end
               bond_string << " on #{format_date(a.hour_72_date, a.hour_72_time)} by #{a.hour_72_judge.nil? ? 'Unknown' : 'Judge ' + a.hour_72_judge.long_name}"
            else
               bond_string = "Pending Bond Hearing"
            end
            h_text << "*** Bond Status: #{bond_string}\n"
            
            h_text << ">>> Current holds for this arrest: #{a.current_holds}\n"
         end
      end
      
      # next, list each non-arrest hold
      unless non_arrest_holds.empty?
         h_text << "\nOther Holds:\n"
         non_arrest_holds.each do |h|
            if h.hold_type.nil?
               h_text << "--- Invalid Hold Type!\n"
            elsif h.hold_type.long_name == "Serving Sentence"
               h_text << "--- Serving Sentence#{h.doc? ? ' (DOC)' : ''} for #{h.hold_reason}: #{(h.hours_remaining / 24.0).to_i} Days Remaining.\n"
            elsif h.hold_type.long_name == "Fines/Costs"
               h_text << "--- #{h.hold_reason_name}#{h.hold_reason}\n"
            elsif h.hold_type.long_name == "Probation (Parish)"
               h_text << "--- Hold for Parish Probation\n"
            elsif h.hold_type.long_name == "Probation/Parole (State)"
               h_text << "--- Hold for State Probation/Parole\n"
            elsif h.hold_type.long_name == "Hold for City"
               h_text << "--- Hold for City (#{h.agency? ? h.agency : 'Unknown City'}): manual hold, unknown reason.\n"
            elsif h.hold_type.long_name == "Hold for Other Agency"
               h_text << "--- Hold for Other Agency: #{h.agency? ? h.agency : 'Unknown Agency'}: manual hold, unknown reason.\n"
            elsif h.hold_type.long_name == "Temporary Release"
               h_text << "--- Temporarily Released because: #{h.temp_release_reason.nil? ? 'Unknown' : h.temp_release_reason.long_name}.\n"
            elsif h.hold_type.long_name == "Temporary Housing"
               h_text << "--- Being housed temporarily for #{h.agency? ? h.agency : 'Unknown Agency'}\n"
            elsif h.hold_type.long_name == "Temporary Transfer"
               h_text << "--- Temporarily transferred to #{h.agency? ? h.agency : 'Unknown Agency'}\n"
            else
               h_text << "--- Manual hold: #{h.hold_type.long_name} (unknown reason)\n"
            end
         end
      end
      # finally, add the booking to the table data array
      unless h_text.blank?
         h_text << "\n\n\n\n" # to give a minimum of 4 blank lines per listing
         data.push([b_class," ",b_name,h_text])
      end
   end
   # print the table
   if data.empty?
      pdf.text "Nothing To Report!", :style => :bold, :align => :center
   else
      pdf.table data, :border_width => 1, :border_style => :grid, :headers => ["T", "Docket/Court","Person","Arrest Information"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left}, :column_widths => {0 => 20, 1 => 125, 2 => 175, 3 => 605}, :font_size => 11, :padding => 1, :position => :center
   end
   pdf.text "END OF REPORT", :style => :bold
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
