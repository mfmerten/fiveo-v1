# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "Sabine Parish Sheriffs Office", :align => :center, :size => 20, :style => :bold
      pdf.text "384 Detention Center Road", :align => :center, :size => 14
      pdf.text "Many, Louisiana 71449", :align => :center, :size => 10
      pdf.text "Phone: (318)256-0006   FAX: (318)256-4518", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@timestamp}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Fraud and Child Support (FCS) Report", :size => 16, :style => :bold, :align => :center
   pdf.text "LA0072 RID 3357", :size => 14, :style => :bold, :align => :center
   pdf.move_down(10)
   pdf.text @start_date.strftime("%B %Y"), :size => 20, :style => :bold, :align => :center
   pdf.move_down(5)
   data = []
   @bookings.each do |b|
      if p = b.person
         status = "#{b.released_because.nil? ? 'In Jail' + (b.hold_for_probation? ? ' - Prob Hold' : '') : b.released_because.long_name}"
         if status == "Transferred"
            unless b.transfers.empty?
               status << ": #{b.transfers.last.to_agency}"
            end
         end
         data.push([p.id, p.full_name, "#{format_date(p.date_of_birth)} ", "#{p.ssn} ", "#{p.sex_name(true)} ", "#{p.race.nil? ? ' ' : p.race.short_name}", format_date(b.booking_date), status, format_date(b.release_date)])
      end
   end
   if data.empty?
      pdf.move_down(40)
      pdf.text "Nothing To Report", :size => 16, :align => :center
   else
      pdf.table data, :border_width => 1, :border_style => :underline_header, :headers => ['ID No.', 'Name', "DOB", "SSN", "Sex", "Race", "Arrest Date","Status","Release Date"], :column_widths => {0 => 55, 1 => 200, 2 => 75, 3 => 75, 4 => 35, 5 => 35, 6 => 75, 7 => 125, 8 => 75}, :font_size => 11, :row_colors => [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], :vertical_padding => 2, :position => :center
   end
   pdf.move_down(5)
   pdf.stroke_horizontal_rule
   pdf.text "END OF REPORT", :style => :bold
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
