# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
this_instant = DateTime.now
pdf.repeat(:all) do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 26) do
      pdf.text(@page_title, :align => :center, :size => 24, :style => :bold)
      pdf.move_up(16)
      pdf.text(format_date(this_instant), :size => 14, :style => :bold)
   end
   
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font("Helvetica")
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text("NO: #{DateTime.now.to_s(:timestamp)}", :align => :left, :size => 12, :style => :bold)
      pdf.move_up(14)
      pdf.text("'#{SiteConfig.agency}'", :align => :center, :size => 12)
      pdf.font("Times-Roman")
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 36], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 56) do
   data = []
   @warrants.each do |w|
      data.push([w.full_name, "#{w.address1}, #{w.address2}", "#{w.race.nil? ? '' : w.race.short_name}#{w.race.nil? || w.sex_name.blank? ? '' : "/"}#{w.sex_name(true)}",format_date(w.dob),"#{w.jurisdiction.nil? ? '' : w.jurisdiction.fullname}",w.warrant_no,"#{w.charge_summary.blank? ? '' : w.charge_summary}",number_to_currency(w.bond_amt),number_to_currency(w.payable)])
   end
   pdf.table(data, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 150, 1 => 220, 2 => 35, 3 => 65, 4 => 100, 5 => 70, 6 => 200, 7 => 65, 8 => 65}, :headers => ["Name", "Address", "R/S","DOB","Jurisdiction","War#","Charge","Bond Amt","Payable"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :left, 5 => :left, 6 => :left, 7 => :right, 8 => :right}, :font_size => 10, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2)
   pdf.move_down(5)
   pdf.text("END OF REPORT -- Total Warrants: #{@warrants.size}", :style => :bold)
end
pdf.number_pages("Page: <page> of <total>", [pdf.bounds.right - 65, 5])
