# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@invest_report.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Investigative Report", :size => 24, :align => :center, :style => :bold
   pdf.text format_date(@invest_report.report_date, @invest_report.report_time), :size => 14, :style => :bold, :align => :center
   data = []
   unless @invest_report.investigation.nil? || @invest_report.investigation.cases.empty?
      cases = @invest_report.investigation.cases.collect{|c| c.case_no}.compact.uniq.join(', ')
      data.push(["Case No(s):", cases])
   end
   unless @invest_report.victims.empty?
      @invest_report.victims.each do |v|
         address = (v.physical_line1.blank? ? ' ' : v.physical_line1) + (v.physical_line2.blank? ? '' : ', ' + v.physical_line2)
         data.push(["Victim:", "#{v.full_name}, #{address}"])
      end
   end
   unless @invest_report.criminals.empty?
      @invest_report.criminals.each do |c|
         if c.person.nil?
            name = c.full_name
            if c.description?
               address = "Description: #{c.description}"
            else
               address = " "
            end
         else
            name = show_person(c.person)
            address = (c.person.physical_line1.blank? ? ' ' : c.person.physical_line1) + (c.person.physical_line2.blank? ? '' : ', ' + c.person.physical_line2)
         end
         data.push(["Suspect:", "#{name}, #{address}"])
      end
   end
   unless data.empty?
     pdf.table data, :border_width => 0, :font_size => 12, :align => {0 => :right, 1 => :left}
   end
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   pdf.text "Report Details", :size => 18, :align => :center, :style => :bold
   pdf.move_down(4)
   pdf.text @invest_report.details, :size => 12
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   pdf.move_down(40)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text "#{show_user @invest_report.creator}", :size => 14
   end
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   data = []
   unless @invest_report.witnesses.empty?
      @invest_report.witnesses.each do |w|
         address = (w.physical_line1.blank? ? ' ' : w.physical_line1) + (w.physical_line2.blank? ? '' : ', ' + w.physical_line2)
         data.push(["Witness:", "#{w.full_name}, #{address}"])
      end
   end
   unless data.empty?
     pdf.table data, :border_width => 0, :font_size => 12, :align => {0 => :right, 1 => :left}
   end
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
