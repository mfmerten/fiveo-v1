# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat(:all) do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font("Helvetica")
      pdf.text("#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold)
      pdf.text("#{SiteConfig.header_subtitle1}", :align => :center, :size => 14)
      pdf.text("#{SiteConfig.header_subtitle2}", :align => :center, :size => 10)
      pdf.text("#{SiteConfig.header_subtitle3}", :align => :center, :size => 10)
      pdf.move_up(62)
      pdf.image("#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left)
      pdf.move_up(75)
      pdf.image("#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right)
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font("Times-Roman")
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font("Helvetica")
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text("NO: #{@arrest.id}", :align => :left, :size => 12, :style => :bold)
      pdf.move_up(14)
      pdf.text("'#{SiteConfig.footer_text}'", :align => :center, :size => 12)
      pdf.font("Times-Roman")
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   data = [["STATE OF LOUISIANA"," ","Docket No. ______-72-______"],
      ["VERSUS"," "," "],
      ["#{show_person(@arrest.person, :include_id => false).upcase}"," ", "CASE: #{@arrest.case_no}"]]
   width = ((pdf.bounds.width / 5) - 2).to_i
   pdf.table(data, :border_width => 0, :column_widths => {0 => width*2, 1 => width - 2, 2 => width*2}, :font_size => 14, :align => {0 => :left, 1 => :center, 2 => :right}, :padding => 1)
   pdf.move_down(10)
   pdf.text(@page_title, :align => :center, :size => 18, :style => :bold)
   pdf.move_down(5)
   pdf.text("Pursuant to the provisions and requirements of Article 230.1 of the LA code of Criminal Procedure (Act 700 of 1972), notice is hereby given to the Honorable Eleventh Judicial District Court and the Honorable District Attorney, Eleventh Judicial District, that the Sheriff, Sabine Parish, Louisiana, has custody of the arrested person identified as follows:", :size => 10)
   pdf.move_down(10)
   race = sex = dob = ssn = "Unknown"
   unless @arrest.person.nil?
      race = @arrest.person.race.long_name unless @arrest.person.race.nil?
      sex = @arrest.person.sex_name if @arrest.person.sex?
      dob = @arrest.person.date_of_birth if @arrest.person.date_of_birth?
      ssn = @arrest.person.ssn if @arrest.person.ssn?
   end
   data = [['Address:',"#{@arrest.person.nil? ? '' : @arrest.person.physical_line1}, #{@arrest.person.nil? ? '' : @arrest.person.physical_line2}","SSN:","#{ssn}"],
      ['Phone(s):',"#{@arrest.person.nil? ? '' : (@arrest.person.home_phone? ? @arrest.person.home_phone : '') + '  ' + (@arrest.person.cell_phone? ? @arrest.person.cell_phone : '')}","DOB:","#{dob}"],
      ['Arrest ID:',"#{@arrest.id}","Race:","#{race}"],
      ['Date/Time:',format_date(@arrest.arrest_date, @arrest.arrest_time),"Sex:","#{sex}"]
   ]
   pdf.text("#{show_person(@arrest.person).upcase}", :size => 12, :style => :bold, :align => :left)
   pdf.table(data, :border_width => 0, :column_widths => {0 => 75, 1 => 300, 2 => 40, 3 => 100}, :font_size => 12, :align => {0 => :right, 1 => :left, 2 => :right, 3 => :left}, :padding => 1)
   pdf.move_down(10)
   unless @arrest.district_charge_summary == 'No Charges'
      pdf.text("R.S. Code and Offense(s) [District]:", :size => 12, :style => :bold)
      pdf.span(pdf.bounds.width - 10, :position => 10) do
         @arrest.district_charges.reject{|c| !c.arrest_warrant.nil?}.each do |c|
            pdf.text("#{c.count} count: #{c.charge}", :size => 12)
         end
      end
      @arrest.district_warrants.each do |w|
         pdf.span(pdf.bounds.width - 10, :position => 10) do
            pdf.text("#{w.jurisdiction.nil? ? '' : w.jurisdiction.fullname + ' '}Warrant Number #{w.warrant_no}:", :size => 12)
         end
         pdf.span(pdf.bounds.width - 20, :position => 20) do
            w.charges.each do |c|
               pdf.text("#{c.count} count: #{c.charge}", :size => 12)
            end
         end
      end
   end
   unless @arrest.city_charge_summary == 'No Charges'
      pdf.text("R.S. Code and Offense(s) [City]:", :size => 12, :style => :bold)
      pdf.span(pdf.bounds.width - 10, :position => 10) do
         @arrest.city_charges.reject{|c| !c.arrest_warrant.nil?}.each do |c|
            pdf.text("#{c.count} count: #{c.charge}#{c.agency.nil? ? '' : ' [' + c.agency.fullname + ']'}", :size => 12)
         end
      end
      @arrest.city_warrants.each do |w|
         pdf.span(pdf.bounds.width - 10, :position => 10) do
            pdf.text("#{w.jurisdiction.nil? ? '' : w.jurisdiction.fullname + ' '}Warrant Number #{w.warrant_no}:", :size => 12)
         end
         pdf.span(pdf.bounds.width - 20, :position => 20) do
            w.charges.each do |c|
               pdf.text("#{c.count} count: #{c.charge}", :size => 12)
            end
         end
      end
   end
   unless @arrest.other_charge_summary == 'No Charges'
      pdf.text("R.S. Code and Offense(s) [Other Parish/State]:", :size => 12, :style => :bold)
      pdf.span(pdf.bounds.width - 10, :position => 10) do
         @arrest.other_charges.reject{|c| !c.arrest_warrant.nil?}.each do |c|
            pdf.text("#{c.count} count: #{c.charge}", :size => 12)
         end
      end
      @arrest.other_warrants.each do |w|
         pdf.span(pdf.bounds.width - 10, :position => 10) do
            pdf.text("#{w.jurisdiction.nil? ? '' : w.jurisdiction.fullname + ' '}Warrant Number #{w.warrant_no}:", :size => 12)
         end
         pdf.span(pdf.bounds.width - 20, :position => 20) do
            w.charges.each do |c|
               pdf.text("#{c.count} count: #{c.charge}", :size => 12)
            end
         end
      end
   end
   pdf.move_down(10)
   data = []
   if @arrest.officer1?
      data.push(['Officer(s):',"#{@arrest.officers}"])
   end
   if @arrest.agency?
      data.push(['Agency:',"#{@arrest.agency}"])
   end
   pdf.table(data, :border_width => 0, :font_size => 12, :align => {0 => :right, 1 => :left}, :padding => 1)
   pdf.move_down(10)
   pdf.text("Respectfully signed and submitted on this ______ day of ___________, _______.", :size => 12)
   pdf.move_down(40)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text("SHERIFF-DEPUTY", :size => 10)
   end
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   pdf.text("O R D E R", :size => 18, :style => :bold, :align => :center)
   pdf.move_down(10)
   pdf.text("Pursuant to the above and foregoing notice and the accused having been brought before the court as required by law, the following order is hereby entered:", :size => 10)
   checkbox = "#{RAILS_ROOT}/public/images/checkbox.png"
   width = pdf.bounds.right - 15
   pdf.move_down(20)
   pdf.image(checkbox, :width => 10)
   pdf.move_up(12)
   pdf.span(width, :position => 15) do
      pdf.text("The accused does not qualify to have counsel appointed by the court and none is appointed.", :size => 12)
   end
   pdf.move_down(10)
   pdf.image(checkbox, :width => 10)
   pdf.move_up(12)
   pdf.span(width, :position => 15) do
      pdf.text("The Court finds that the accused qualified as an indigent person and has the right to have", :size => 12)
      pdf.move_down(5)
      pdf.text("the Court appoint legal counsel and ____________________________, Attorney at Law is hereby appointed to defend the accused.", :size => 12)
   end
   pdf.move_down(10)
   pdf.image(checkbox, :width => 10)
   pdf.move_up(12)
   pdf.span(width, :position => 15) do
      pdf.text("The amount of bail not having been determined previously, bail is hereby set at the ammount of:", :size => 12)
      pdf.move_down(5)
      pdf.text("$_______________.", :size => 12)
   end
   pdf.move_down(10)
   pdf.image(checkbox, :width => 10)
   pdf.move_up(12)
   pdf.span(width, :position => 15) do
      pdf.text("Special Condition of Bond:", :size => 12)
      pdf.stroke_horizontal_line(135, pdf.bounds.right)
      pdf.move_down(20)
      pdf.stroke_horizontal_rule
   end
   pdf.move_down(10)
   pdf.image(checkbox, :width => 10)
   pdf.move_up(12)
   pdf.span(width, :position => 15) do
      pdf.text("The amount of bail having been set previously at $_____________ upon review of the prior bail", :size => 12)
      pdf.move_down(5)
      pdf.text("determination it is ordered by the Court that the amount of bail be set at $_____________.", :size => 12)
   end
   pdf.move_down(10)
   pdf.image(checkbox, :width => 10)
   pdf.move_up(12)
   pdf.span(width, :position => 15) do
      pdf.text("Bond is not set, pending hearing which is hereby fixed for ____________ AT ________M.", :size => 12)
   end
   pdf.move_down(10)
   pdf.image(checkbox, :width => 10)
   pdf.move_up(12)
   pdf.span(width, :position => 15) do
      pdf.text("Unsecured Personal Surety meeting the requirements of C.Cr.P.Art317 approved.", :size => 12)
   end
   pdf.move_down(10)
   pdf.image(checkbox, :width => 10)
   pdf.move_up(12)
   pdf.span(width, :position => 15) do
      pdf.text("By Video", :size => 12)
   end
   pdf.move_down(20)
   pdf.text("DONE AND SIGNED at Many, Louisiana, on this ______ day of ___________, _______.", :size => 12, :style => :bold)
   pdf.move_down(50)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text("JUDGE, 11TH JUDICIAL DISTRICT COURT", :size => 10)
   end
end

pdf.number_pages("Page: <page> of <total>", [pdf.bounds.right - 55, 5])
