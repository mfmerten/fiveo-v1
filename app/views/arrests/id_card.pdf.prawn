# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat(:all) do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 95) do
      pdf.font("Times-Roman")
      if @arrest.person.nil? || @arrest.person.bookings.empty? || @arrest.person.bookings.last.release_date? || @arrest.person.bookings.last.facility.nil?
         pdf.text(SiteConfig.agency, :align => :center, :size => 18, :style => :bold)
      else
         pdf.text(@arrest.person.bookings.last.facility.long_name, :align => :center, :size => 18, :style => :bold)
      end
      pdf.move_down(5)
      pdf.text(@page_title, :align => :center, :size => 20, :style => :bold)
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.font("Helvetica")
      if @arrest.person.nil?
         pdf.text("Unknown", :size => 18, :style => :bold)
      else
         pdf.text("#{show_person(@arrest.person)}", :size => 18, :style => :bold)
         pdf.text("#{@arrest.person.aka? ? 'Known Aliases: ' + @arrest.person.aka : 'No Known Aliases'}", :size => 14)
      end
      pdf.stroke_horizontal_rule
      pdf.font("Times-Roman")
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font("Helvetica")
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text("NO: #{@arrest.id}", :align => :left, :size => 12, :style => :bold)
      pdf.move_up(14)
      pdf.text(SiteConfig.agency, :align => :center, :size => 12)
      pdf.font("Times-Roman")
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 110], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 135) do
   if @arrest.person.nil?
      pdf.text("The Person for this Arrest is not valid!", :size => 18, :style => :bold, :align => :center)
      pdf.text("Please fix it then print this ID Card again.", :size => 14, :style => :bold, :align => :center)
   else
      pdf.image("#{RAILS_ROOT}/public#{@arrest.person.front_mugshot.url(:medium)}".gsub(/\?\d*$/,''), :at => [306, pdf.bounds.top - 5], :fit => [100, 150])
      pdf.image("#{RAILS_ROOT}/public#{@arrest.person.side_mugshot.url(:medium)}".gsub(/\?\d*$/,''), :at => [456, pdf.bounds.top - 5], :fit => [100, 150])
      
      # start position
      row_position = pdf.bounds.top - 5

      # these use: pdf.text_field(origin, width, label, value, border_color, label_color, rows)
      # and: pdf.col(1) for width of field with 1 column page, and pdf.col(1,1) for position of first column on 1 column page

      # date of birth
      row_position -= pdf.text_field([pdf.col(2,1), row_position], pdf.col(2), "Date of Birth:", format_date(@arrest.person.date_of_birth), SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # place of birth
      row_position -= pdf.text_field([pdf.col(2,1), row_position], pdf.col(2), "Place of Birth:", @arrest.person.place_of_birth, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # ssn
      row_position -= pdf.text_field([pdf.col(2,1), row_position], pdf.col(2), "SSN:", @arrest.person.ssn, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # oln
      row_position -= pdf.text_field([pdf.col(2,1), row_position], pdf.col(2), "OLN:", "#{@arrest.person.oln} (#{@arrest.person.oln_state.nil? ? '' : @arrest.person.oln_state.short_name})", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # sid
      row_position -= pdf.text_field([pdf.col(2,1), row_position], pdf.col(2), "SID:", @arrest.person.sid, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # fbi ID
      row_position -= pdf.text_field([pdf.col(2,1), row_position], pdf.col(2), "FBI ID:", @arrest.person.fbi, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # DOC Number
      row_position -= pdf.text_field([pdf.col(2,1), row_position], pdf.col(2), "DOC No:", @arrest.person.doc_number, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # Physical Address
      pdf.text_field([pdf.col(2,1), row_position], pdf.col(2), "Physical Address:", "#{@arrest.person.physical_line1}\n#{@arrest.person.physical_line2}" , SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 2)
      row_position -= pdf.text_field([pdf.col(2,2), row_position], pdf.col(2), "Mailing Address:", "#{@arrest.person.mailing_line1}\n#{@arrest.person.mailing_line2}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 2)

      # home phone, cell phone, emergency contact
      pdf.text_field([pdf.col(2,2), row_position], pdf.col(2), "Emergency Contact:", "#{@arrest.person.emergency_contact}\n#{@arrest.person.em_relationship.nil? ? '' : @arrest.person.em_relationship.long_name}\n#{@arrest.person.emergency_phone}", SiteConfig.pdf_em_bg, SiteConfig.pdf_em_txt, 3)
      pdf.text_field([pdf.col(4,1), row_position], pdf.col(4), "Home Phone:", @arrest.person.home_phone, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      row_position -= pdf.text_field([pdf.col(4,2), row_position], pdf.col(4), "Cell Phone:", @arrest.person.cell_phone, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # occupation
      row_position -= pdf.text_field([pdf.col(2,1), row_position], pdf.col(2), "Occupation:", "#{@arrest.person.occupation? ? @arrest.person.occupation.titleize : ' '}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # employer
      row_position -= pdf.text_field([pdf.col(2,1), row_position], pdf.col(2), "Employer:", "#{@arrest.person.employer? ? @arrest.person.employer.titleize : ' '}", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # Physical Description
      pdf.text_field([pdf.col(2,1), row_position], pdf.col(2), "Description:", @arrest.person.person_description, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 3)
      row_position -= pdf.text_field([pdf.col(2,2), row_position], pdf.col(2), "Distinguishing Marks:", @arrest.person.marks_description, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 3)
      
      pdf.move_down(5)
      data = [['']]
      pdf.table(data, :border_width => 0, :header_color => SiteConfig.pdf_section_bg, :header_text_color => SiteConfig.pdf_section_txt, :width => pdf.bounds.width, :font_size => 14, :align => :center, :vertical_padding => 1, :headers => ['ARREST DATA'])
      pdf.move_down(10)
      
      row_position -= 29
      
      # arrest date, case, agency
      pdf.text_field([pdf.col(4,1), row_position], pdf.col(4), "Arrest Date:", format_date(@arrest.arrest_date, @arrest.arrest_time), SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      pdf.text_field([pdf.col(4,2), row_position], pdf.col(4), "Case Number:", @arrest.case_no, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      row_position -= pdf.text_field([pdf.col(2,2), row_position], pdf.col(2), "Agency:", @arrest.agency, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # ATN + future
      pdf.text_field([pdf.col(4,1), row_position], pdf.col(4), "ATN:", @arrest.atn, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      pdf.text_field([pdf.col(4,2), row_position], pdf.col(4), "Leave This Blank:", "", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      row_position -= pdf.text_field([pdf.col(2,2), row_position], pdf.col(2), "Leave This Blank:", "", SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      # officers
      row_position -= pdf.text_field([pdf.col(1,1), row_position], pdf.col(1), "Officer(s):", @arrest.officers, SiteConfig.pdf_header_bg, SiteConfig.pdf_header_txt, 1)
      
      pdf.move_down(5)
      
      data = []
      @arrest.district_charges.each do |c|
         data.push(['District', "#{c.arrest_warrant.nil? ? '' : c.arrest_warrant.warrant_no} ", c.count, c.charge])
      end
      @arrest.city_charges.each do |c|
         data.push(['City', "#{c.arrest_warrant.nil? ? '' : c.arrest_warrant.warrant_no} ", c.count, c.charge])
      end
      @arrest.other_charges.each do |c|
         data.push(['Other', "#{c.arrest_warrant.nil? ? '' : c.arrest_warrant.warrant_no} ", c.count, c.charge])
      end
      if data.empty?
         data = [[' ',' ',' ',' ']]
      end
      pdf.table(data, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 55, 1 => 85, 2 => 35, 3 => pdf.bounds.width - 178}, :font_size => 12, :align => :left, :vertical_padding => 1, :headers => ['Type','Warrant','Cnt','Charge'], :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even])
   end
end

pdf.number_pages("Page: <page> of <total>", [pdf.bounds.right - 55, 5])
