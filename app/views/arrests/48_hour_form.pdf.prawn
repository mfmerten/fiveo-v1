# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat(:all) do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font("Helvetica")
      pdf.text("#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold)
      pdf.text("#{SiteConfig.header_subtitle1}", :align => :center, :size => 14)
      pdf.text("#{SiteConfig.header_subtitle2}", :align => :center, :size => 10)
      pdf.text("#{SiteConfig.header_subtitle3}", :align => :center, :size => 10)
      pdf.move_up(62)
      pdf.image("#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left)
      pdf.move_up(75)
      pdf.image("#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right)
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font("Times-Roman")
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font("Helvetica")
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text("NO: #{@arrest.id}", :align => :left, :size => 12, :style => :bold)
      pdf.move_up(14)
      pdf.text("'#{SiteConfig.footer_text}'", :align => :center, :size => 12)
      pdf.font("Times-Roman")
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text("CASE: #{@arrest.case_no}", :size => 14, :style => :bold, :align => :right)
   pdf.move_down(10)
   pdf.text(@page_title, :align => :center, :size => 20, :style => :bold)
   pdf.move_down(5)
   pdf.text("BEFORE ME, the undersigned authority, personally appeared the undersigned Officer, who being duly sworn, deposed and swears that the following facts in support of the instant Arrest(s) without a warrant of:", :size => 12)
   pdf.move_down(10)
   pdf.span(pdf.bounds.width - 10, :position => 10) do
      pdf.text("#{show_person(@arrest.person)}", :size => 16, :style => :bold, :align => :left)
      pdf.text("Booking ID: #{@arrest.person.nil? ? '' : @arrest.person.bookings.last.id}      Date and Time of Incarceration: #{format_date(@arrest.arrest_date, @arrest.arrest_time)}", :size => 14)
      race = sex = dob = ssn = "Unknown"
      unless @arrest.person.nil?
         race = @arrest.person.race.long_name unless @arrest.person.race.nil?
         sex = @arrest.person.sex_name if @arrest.person.sex?
         dob = @arrest.person.date_of_birth if @arrest.person.date_of_birth?
         ssn = @arrest.person.ssn if @arrest.person.ssn?
      end
      pdf.text("Race: #{race}      Sex: #{sex}      DOB: #{dob}      SSN: #{ssn}", :size => 14)
   end
   pdf.move_down(10)
   pdf.text("For (R.S. Code and Offense):", :size => 12, :style => :bold)
   pdf.span(pdf.bounds.width - 10, :position => 10) do
      @arrest.district_charges.reject{|c| !c.arrest_warrant.nil?}.each do |c|
         pdf.text("    #{c.count} count: #{c.charge}", :size => 12)
      end
   end
   @arrest.district_warrants.each do |w|
      pdf.span(pdf.bounds.width - 10, :position => 10) do
         pdf.text("    #{w.jurisdiction.nil? ? '' : w.jurisdiction.fullname + ' '}Warrant Number #{w.warrant_no}:", :size => 12)
      end
      pdf.span(pdf.bounds.width - 20, :position => 20) do
         w.charges.each do |c|
            pdf.text("        #{c.count} count: #{c.charge}", :size => 12)
         end
      end
   end
   pdf.move_down(10)
   pdf.text("That the probable cause for the arrest(s) without a warrant was as follows:", :size => 12, :style => :bold)
   pdf.move_down(10)
   if @arrest.offense.nil? || @arrest.offense.offense_date.blank? || @arrest.offense.offense_time.blank?
      pdf.text("On the ______ day of ___________, _______, at approximately ____________, in the", :size => 14)
   else
      pdf.text("On the #{@arrest.offense.offense_date.day.ordinalize} day of #{@arrest.offense.offense_date.strftime('%B')}, #{@arrest.offense.offense_date.strftime('%Y')}, at approximately #{time_to_12(@arrest.offense.offense_time)}, in the", :size => 14)
   end
   pdf.move_down(5)
   if @arrest.offense.nil? || @arrest.offense.location.blank?
      pdf.text("vicinity of _____________________________________________________, Sabine Parish, Louisiana, on the above named arrestee:", :size => 14)
   else
      pdf.text("vicinity of #{@arrest.offense.location}, Sabine Parish, Louisiana, on the above named arrestee:", :size => 14)
   end
   pdf.move_down(10)
   if @arrest.offense.nil? || @arrest.offense.details.blank?
      7.times{ pdf.pad(10){ pdf.stroke_horizontal_rule } }
   else
      pdf.text(@arrest.offense.details)
   end
   pdf.move_down(30)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text("AFFIANT", :size => 10)
   end
   pdf.move_down(10)
   pdf.text("Sworn to and subscribed before me this ______ day of ___________, _______.", :size => 14)
   pdf.move_down(40)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text("NOTARY PUBLIC", :size => 10)
   end
   pdf.pad(10){ pdf.stroke_horizontal_rule }
   pdf.text("O R D E R", :size => 18, :style => :bold, :align => :center)
   pdf.move_down(10)
   pdf.text("The foregoing affidavit having been duly considered, IT IS HEREBY ORDERED, ADJUDGED AND DECREED that the defendant has been lawfully arrested upon probable cause, without a warrant.", :size => 12)
   pdf.move_down(10)
   pdf.text("Many, Louisiana, this ______ day of ___________, _______.", :size => 14)
   pdf.move_down(40)
   pdf.stroke_horizontal_line(pdf.col(2,2), pdf.col(2,2) + pdf.col(2))
   pdf.span(pdf.col(2), :position => pdf.col(2,2)) do
      pdf.text("JUDGE, 11TH JUDICIAL DISTRICT COURT", :size => 10)
      pdf.text("SABINE PARISH, LOUISIANA", :size => 10)
   end
end