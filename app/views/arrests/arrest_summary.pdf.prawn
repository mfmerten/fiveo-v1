# ===================================================================
# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
#
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# =====================================================================

# table columns are:
#  officer_id    string   (contact id)
#  officer       string   (contact fullname)
#  unit          string   (contact unit)
#  arrest_date   date
#  arrest_time   string
#  person_id     integer
#  arrest_type   integer  (0=district, 1=city, 2=other)
#
# order is officer, officer_id, arrest_date

pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@timestamp}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Arrest Summary by Officer", :align => :center, :size => 20, :style => :bold
   unless @agency.nil?
      pdf.text("Only #{@agency.fullname} Officers", :align => :center, :size => 14, :style => :bold)
   end
   period = ""
   if @start_date.nil? && @stop_date.nil?
      period = "All Records"
   elsif @start_date.nil?
      period = "On or Before #{format_date(@stop_date)}"
   elsif @stop_date.nil?
      period = "On or After #{format_date(@start_date)}"
   else
      period = "#{format_date(@start_date)} -- #{format_date(@stop_date)}"
   end
   pdf.text period, :align => :center, :size => 12, :style => :bold
   pdf.move_down(5)
   
   data = []
   officer = {}
   @arrests.each do |a|
      if officer[a.officer].nil?
         officer[a.officer] = {:dates => {}, :total => 0, :unit => a.unit}
      end
      if officer[a.officer][:dates][a.arrest_date].nil?
         officer[a.officer][:dates][a.arrest_date] = {:district => 0, :city => 0, :other => 0, :total => 0}
      end
      if a.arrest_type == 0
         officer[a.officer][:dates][a.arrest_date][:district] += 1
      elsif a.arrest_type == 1
         officer[a.officer][:dates][a.arrest_date][:city] += 1
      else
         officer[a.officer][:dates][a.arrest_date][:other] += 1
      end
      officer[a.officer][:total] += 1
      officer[a.officer][:dates][a.arrest_date][:total] += 1
   end
   officer.keys.sort.each do |k|
      data = []
      officer[k][:dates].keys.sort.each do |d|
         data.push(["#{format_date(d)}",officer[k][:dates][d][:district],officer[k][:dates][d][:city],officer[k][:dates][d][:other],officer[k][:dates][d][:total]])
      end
      pdf.move_down(10)
      pdf.text("#{k} (#{officer[k][:unit]}): #{officer[k][:total]}", :size => 14, :style => :bold)
      pdf.table(data, :border_width => 1, :border_style => :underline_header, :align => :left, :headers => ["Date", "District", "City", "Other", "Total"], :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 1, :column_widths => {0 => 75, 1 => 65, 2 => 65, 3 => 65, 4 => 65})
      if pdf.y < 150
         pdf.start_new_page
      end
   end
   pdf.move_down(5)
   pdf.text("END OF REPORT", :style => :bold)
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
