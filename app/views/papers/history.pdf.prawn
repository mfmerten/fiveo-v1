# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{DateTime.now.to_s(:timestamp)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   pdf.text "Paper Service Payment History", :align => :center, :size => 20, :style => :bold
   pdf.text "Customer #{@customer.id}: #{@customer.name}", :align => :center, :size => 16, :style => :bold
   period = ""
   if @from_date.nil? && @to_date.nil?
      period = "All Records"
   elsif @from_date.nil?
      period = "On or Before #{format_date(@to_date)}"
   elsif @to_date.nil?
      period = "On or After #{format_date(@from_date)}"
   else
      period = "#{format_date(@from_date)} -- #{format_date(@to_date)}"
   end
   pdf.text period, :align => :center, :size => 12, :style => :bold
   pdf.move_down(8)
   data = []
   @payments.each do |p|
      data.push([p.id, format_date(p.transaction_date), "#{p.paper.nil? ? 'N/A' : p.paper_id}", p.memo, p.receipt_no, p.payment_method, number_to_currency(p.payment), number_to_currency(p.charge)])
   end
   pdf.table data, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 50, 1 => 65, 2 => 50, 3 => 165, 4 => 75, 5 => 40, 6 => 65, 7 => 65}, :headers => ["Trans ID", "Date", "Invoice","Memo", "Receipt", "Type", "Paid", "Adjusted"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :left, 5 => :left, 6 => :right, 7 => :right}, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2
   pdf.move_down(8)
   pdf.text "END OF REPORT", :style => :bold
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
