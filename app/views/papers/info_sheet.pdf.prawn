# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 50], :width => pdf.margin_box.width, :height => 50) do
      pdf.span(pdf.bounds.right - 100, :position => 50) do
         pdf.text "Questions concerning service information should be directed to the Civil Process Clerk in our office at Phone: (318) 256-5651.  Thank You!", :size => 12, :align => :center, :style => :bold
      end
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@paper.id}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 155) do
   # define column widths
   heading_column = 150
   data_column = pdf.bounds.width - heading_column - 50
   
   pdf.text "Civil Process Information", :align => :center, :size => 20, :style => :bold
   pdf.move_down(5)
   pdf.text DateTime.now.to_s(:us), :size => 12, :style => :bold, :align => :center
   pdf.move_down(15)
   data = []
   data.push(['Suit or Docket #:', "#{@paper.suit_no}"])
   data.push(['Plaintiff:', "#{@paper.plaintiff}"])
   data.push(['Defendant:', "#{@paper.defendant}"])
   pdf.table data, :border_width => 0, :column_widths => {0 => heading_column, 1 => data_column}, :align => {0 => :right, 1 => :left}, :font_size => 14, :position => :left

   pdf.pad(10){pdf.stroke_horizontal_rule}

   data = []
   data.push(['Type of Document:',"#{@paper.paper_type.nil? ? '' : @paper.paper_type.name}"])
   data.push(['Date Received:', format_date(@paper.received_date)])
   if @paper.court_date?
      data.push(['Court Date:', format_date(@paper.court_date)])
   end
   data.push(['Person to be Served:',@paper.serve_to])
   data.push(['Address:',@paper.address_line1])
   data.push([' ',@paper.address_line2])
   if @paper.phone?
      data.push(['Phone:', @paper.phone ])
   end
   pdf.table data, :border_width => 0, :column_widths => {0 => heading_column, 1 => data_column}, :align => {0 => :right, 1 => :left}, :font_size => 14, :position => :left
   
   pdf.pad(10){pdf.stroke_horizontal_rule}
   
   data = []
   data.push(['Service Type:',"#{@paper.service_type.nil? ? '_______________________________' : @paper.service_type.long_name}"])
   data.push(['Date Served:', "#{@paper.served_date? ? format_date(@paper.served_date) + (@paper.served_time? ? '   ' + @paper.served_time : '') : '_________________________'}"])
   data.push(['Person Served:',"#{@paper.served_to? ? @paper.served_to : '_____________________________________________'}"])
   data.push(['Officer:', "#{@paper.officer? ? show_contact(@paper, :officer) : '_____________________________________________'}"])
   data.push(['Returned Date:',"#{@paper.returned_date? ? format_date(@paper.returned_date) : '_________________________'}"])
   if @paper.noservice_type.nil?
      data.push(['Reason Returned:','___________________________________________________'])
   else
      data.push(['Reason Returned:',"#{@paper.noservice_type.long_name}#{@paper.noservice_type.long_name.slice(-1,1) != ':' && @paper.noservice_extra? ? ': ' : ' '}#{@paper.noservice_extra? ? @paper.noservice_extra : ''}"])
   end
   if @paper.comments?
      data.push(['Comments:',@paper.comments])
   else
      data.push(['Comments:', '___________________________________________________'])
      data.push([' ','___________________________________________________'])
      data.push([' ','___________________________________________________'])
   end
   pdf.table data, :border_width => 0, :column_widths => {0 => heading_column, 1 => data_column}, :align => {0 => :right, 1 => :left}, :font_size => 14, :position => :left
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
