# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 52], :width => pdf.margin_box.width, :height => 52) do
      pdf.span(pdf.bounds.right - 100, :position => 50) do
         pdf.text "Include Invoice Number With Payment", :size => 14, :align => :center, :style => :bold
      end
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      invoice_no_position = pdf.y
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 155) do
   @papers.each do |paper|
      unless paper == @papers.first
         pdf.start_new_page
      end
      if @receipt.nil?
        pdf.text "Invoice", :align => :center, :size => 24, :style => :bold
      else
        pdf.text "Receipt", :align => :center, :size => 24, :style => :bold
      end
      pdf.move_up(18)
      pdf.text "No: #{paper.id}", :size => 16, :style => :bold
      pdf.move_up(18)
      pdf.text "No: #{paper.id}", :size => 16, :style => :bold, :align => :right
      pdf.move_down(5)
      pdf.text Date.today.to_s(:us), :size => 16, :style => :bold, :align => :center
      
      row_pos = pdf.bounds.top - 75
      
      sep = "\n"
      
      pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), 'Attention:', "#{paper.customer.nil? ? '' : paper.customer.attention} "
      row_pos -= pdf.text_field [pdf.col(2,2), row_pos], pdf.col(2), 'Suit Or Docket:', "#{paper.suit_no} "
      pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), 'Customer:', "#{paper.customer.nil? ? 'Unknown' : paper.customer.name} #{sep}#{paper.customer.nil? ? '' : paper.customer.address_line1} #{paper.customer.nil? ? '' : (paper.customer.address_line2.blank? ? '' : sep + paper.customer.address_line2)} #{sep}#{paper.customer.nil? ? '' : paper.customer.address_line3} ", nil, nil, 3
      row_pos -= pdf.text_field [pdf.col(2,2), row_pos], pdf.col(2), 'For:', "#{paper.plaintiff} #{sep}-VS-#{sep}#{paper.defendant} ", nil, nil, 3
      pdf.text_field [pdf.col(4,1), row_pos], pdf.col(4), 'Customer Number:', "#{paper.paper_customer_id} "
      pdf.text_field [pdf.col(4,2), row_pos], pdf.col(4), 'Phone:',"#{paper.customer.nil? ? '' : paper.customer.phone} "
      row_pos -= pdf.text_field [pdf.col(2,2), row_pos], pdf.col(2), "Office Use Only:"," "

      pdf.pad(10){pdf.stroke_horizontal_rule}
      
      pdf.text "Invoice Details", :size => 18, :style => :bold, :align => :center
            
      row_pos -= 50
      
      pdf.text_field [pdf.col(4,3), row_pos], pdf.col(4), "Received:", format_date(paper.received_date)
      pdf.text_field [pdf.col(4,4), row_pos], pdf.col(4), "#{paper.served_date? ? 'Served:' : 'Returned:'}", "#{format_date((paper.served_date? ? paper.served_date : paper.returned_date))} "
      row_pos -= pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), "Paper ID:", "#{paper.id} "
      pdf.text_field [pdf.col(2,2), row_pos], pdf.col(2), "#{paper.served_date? ? 'Served By:' : 'Returned Because:'}", "#{paper.served_date? ? show_contact(paper, :officer) : (paper.noservice_type.nil? ? '' : paper.noservice_type.long_name)} "
      row_pos -= pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), "Type:", "#{paper.paper_type.nil? ? 'Unknown' : paper.paper_type.name} "
      pdf.text_field [pdf.col(2,2), row_pos], pdf.col(2), "#{paper.served_date? ? 'Served To:' : 'Note:'}", "#{paper.served_date? ? paper.served_to + (paper.service_type.nil? ? '' : ' (' + paper.service_type.long_name + ')') : paper.noservice_extra} "
      row_pos -= pdf.text_field [pdf.col(2,1), row_pos], pdf.col(2), "Serve To:", "#{paper.serve_to} "
      
      pdf.move_down(20)
      
      mfee = (paper.mileage_fee * paper.mileage).round(2)
      
      data = [
         ["#{paper.served_date? ? 'Service Fee:' : 'Non-Service Fee:'} ","#{paper.mileage_only? ? '$0.00' : (paper.served_date? ? number_to_currency(paper.service_fee) : number_to_currency(paper.noservice_fee))} "],
         ["Mileage Fee: (#{paper.mileage} miles @ #{number_to_currency(paper.mileage_fee)}/mile)","#{number_to_currency(mfee)} "],
         ["Total Due:","#{number_to_currency(paper.invoice_total)} "]
      ]
      unless @receipt.nil?
        data.push(["Amount Paid:","#{number_to_currency(paper.invoice_total)} "])
      end
      
      pdf.table data, :border_width => 1, :border_style => :grid, :align => :right, :font_size => 16, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2, :width => (pdf.bounds.width * 0.75).round, :position => :center
      pdf.move_down(20)
      if @receipt.nil?
        pdf.text "Please pay this amount         #{number_to_currency(paper.invoice_total)}", :style => :bold, :align => :center, :size => 16
        pdf.move_up(22)
        pdf.image "#{RAILS_ROOT}/public/images/icons/forward.png", :height => 20, :width => 20, :position => 330
      else
        pdf.text "Paid In Full - Thank You!", :style => :bold, :align => :center, :size => 16
      end
   end
end
