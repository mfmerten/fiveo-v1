# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{@timestamp}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   charge_count = payment_count = 0
   transaction_count = @payments.size
   charge_total = payment_total = net = BigDecimal.new('0',2)
   accounts = {}
   account_charges = {}
   account_payments = {}

   heading = "Paper Service Posting#{@status == 'preview' ? ' Preview' : ''} Report"

   charge_lines = []
   payment_lines = []
   @payments.each do |p|
      if p.charge? && p.charge != 0
         charge_lines.push(["#{p.payment? && p.payment != 0 ? '*' : ' '}",format_date(p.transaction_date),p.id,p.paper_customer_id,p.memo," "," ",number_to_currency(p.charge)])
         charge_count += 1
         charge_total += p.charge
         net += p.charge
         if accounts[p.paper_customer_id.to_s].nil?
            accounts[p.paper_customer_id.to_s] = p.charge
         else
            accounts[p.paper_customer_id.to_s] += p.charge
         end
         if account_charges[p.paper_customer_id.to_s].nil?
            account_charges[p.paper_customer_id.to_s] = p.charge
         else
            account_charges[p.paper_customer_id.to_s] += p.charge
         end
      end
      if p.payment? && p.payment != 0
         payment_lines.push(["#{p.charge? && p.charge != 0 ? '*' : ' '}",format_date(p.transaction_date),p.id,p.paper_customer_id,p.memo,p.receipt_no,p.payment_method,number_to_currency(p.payment)])
         payment_count += 1
         payment_total -= p.payment
         net -= p.payment
         if accounts[p.paper_customer_id.to_s].nil?
            accounts[p.paper_customer_id.to_s] = -p.payment
         else
            accounts[p.paper_customer_id.to_s] -= p.payment
         end
         if account_payments[p.paper_customer_id.to_s].nil?
            account_payments[p.paper_customer_id.to_s] = -p.payment
         else
            account_payments[p.paper_customer_id.to_s] -= p.payment
         end
      end
      if @status == 'post'
         p.update_posted(@report_time)
      end
   end
   
   # print the charges
   pdf.text heading, :size => 20, :style => :bold, :align => :center
   pdf.move_down(5)
   pdf.text format_date(@report_time), :size => 16, :style => :bold, :align => :center
   pdf.move_down(5)
   pdf.text "Charges", :size => 18, :style => :bold, :align => :center
   pdf.move_down(5)
   if charge_lines.empty?
      pdf.move_down(40)
      pdf.text "No Charges To Report", :size => 12, :style => :bold, :align => :center
   else
      pdf.table charge_lines, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 10, 1 => 65, 2 => 50, 3 => 50, 4 => 175, 5 => 85, 6 => 65, 7 => 70}, :headers => [" ", "Date", "Trans ID", "Acct ID", "Memo", "Receipt", "Type", "Amount"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :left, 5 => :left, 6 => :left, 7 => :right}, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2
      pdf.move_down(5)
      pdf.text "* These transactions also appear in Payments section.", :size => 10
      pdf.move_down(5)
      pdf.text "Charge Count: #{charge_count}  Charge Total: #{number_to_currency(charge_total)}", :size => 14, :style => :bold
   end
   pdf.start_new_page
   
   # print the payments
   pdf.text heading, :size => 20, :style => :bold, :align => :center
   pdf.move_down(5)
   pdf.text format_date(@report_time), :size => 16, :style => :bold, :align => :center
   pdf.move_down(5)
   pdf.text "Payments", :size => 18, :style => :bold, :align => :center
   pdf.move_down(5)
   if payment_lines.empty?
      pdf.move_down(40)
      pdf.text "No Charges To Report", :size => 12, :style => :bold, :align => :center
   else
      pdf.table payment_lines, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 10, 1 => 65, 2 => 50, 3 => 50, 4 => 175, 5 => 85, 6 => 65, 7 => 70}, :headers => [" ", "Date", "Trans ID", "Acct ID", "Memo", "Receipt", "Type", "Amount"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :left, 5 => :left, 6 => :left, 7 => :right}, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2
      pdf.move_down(5)
      pdf.text "* These transactions also appear in Charges section.", :size => 10
      pdf.move_down(5)
      pdf.text "Payment Count: #{payment_count}  Payment Total: #{number_to_currency(-payment_total)}", :size => 14, :style => :bold
   end
   pdf.start_new_page
   
   # print the summary page
   pdf.text heading, :size => 20, :style => :bold, :align => :center
   pdf.move_down(5)
   pdf.text format_date(@report_time), :size => 16, :style => :bold, :align => :center
   pdf.move_down(5)
   pdf.text "Summary", :size => 18, :style => :bold, :align => :center
   pdf.move_down(5)

   data = [
      ["Charge Count:",charge_count],
      ["Payment Count:",payment_count],
      ["Total Count:",charge_count + payment_count],
      ["Transactions:",transaction_count],
      [" ",""],
      ["Total Charged:",number_to_currency(charge_total)],
      ["Total Paid:",number_to_currency(payment_total)],
      ["Net Change:",number_to_currency(payment_total + charge_total)]
   ]
   pdf.table data, :border_width => 1, :border_style => :underline_header, :align => {0 => :right, 1 => :right}, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 5, :font_size => 12, :position => :center
   pdf.start_new_page

   # print summary by customer
   pdf.text heading, :size => 20, :style => :bold, :align => :center
   pdf.move_down(5)
   pdf.text format_date(@report_time), :size => 16, :style => :bold, :align => :center
   pdf.move_down(5)
   pdf.text "Summary by Account", :size => 18, :style => :bold, :align => :center
   pdf.move_down(5)
   data = []
   
   if accounts.empty?
      pdf.move_down(40)
      pdf.text "No Accounts To Report", :size => 12, :style => :bold, :align => :center
   else
      accounts.keys.sort{|a,b| a.rjust(10,'0') <=> b.rjust(10,'0')}.each do |a|
         customer = PaperCustomer.find_by_id(a.to_i)
         data.push(["#{a}", "#{customer.nil? ? 'Unknown' : customer.name}", number_to_currency(account_charges[a]), number_to_currency(account_payments[a]),number_to_currency(accounts[a])])
      end
      pdf.table data, :border_width => 1, :border_style => :underline_header, :column_widths => {0 => 65, 1 => 200, 2 => 70, 3 => 70, 4 => 70 }, :headers => ["Account", "Name","Charges","Payments","Net Change"], :align => {0 => :left, 1 => :left, 2 => :right, 3 => :right, 4 => :right}, :font_size => 12, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 5, :position => :center
   end
   pdf.move_down(5)
   pdf.text "END OF REPORT", :size => 12, :style => :bold
end
pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
