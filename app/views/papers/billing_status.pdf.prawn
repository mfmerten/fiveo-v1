# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
pdf.repeat :all do
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 10], :width => pdf.margin_box.width, :height => 80) do
      pdf.font "Helvetica"
      pdf.text "#{SiteConfig.header_title}", :align => :center, :size => 20, :style => :bold
      pdf.text "#{SiteConfig.header_subtitle1}", :align => :center, :size => 14
      pdf.text "#{SiteConfig.header_subtitle2}", :align => :center, :size => 10
      pdf.text "#{SiteConfig.header_subtitle3}", :align => :center, :size => 10
      pdf.move_up(62)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_left_img}", :height => 75, :width => 75, :position => :left
      pdf.move_up(75)
      pdf.image "#{RAILS_ROOT}/public#{SiteConfig.header_right_img}", :height => 75, :width => 75, :position => :right
      pdf.move_down(5)
      pdf.stroke_horizontal_rule
      pdf.font "Times-Roman"
   end
   pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.bottom + 20], :width => pdf.margin_box.width, :height => 20) do
      pdf.font "Helvetica"
      pdf.stroke_horizontal_rule
      pdf.move_down(5)
      pdf.text "NO: #{format_date(@report_datetime, :timestamp => true)}", :align => :left, :size => 12, :style => :bold
      pdf.move_up(14)
      pdf.text "'#{SiteConfig.footer_text}'", :align => :center, :size => 12
      pdf.font "Times-Roman"
   end
end

pdf.bounding_box([pdf.margin_box.left, pdf.margin_box.top - 100], :width => pdf.margin_box.width, :height => pdf.margin_box.height - 125) do
   # initialize data
   grand_total = customer_total = BigDecimal.new("0.0",2)
   miles_each = BigDecimal.new(SiteConfig.mileage_fee.to_s, 2)
   noservice_fee = BigDecimal.new(SiteConfig.noservice_fee.to_s, 2)
   # Loop through the papers printing customer and suit headers as they change
   last_customer = nil
   data = []
   @papers.each do |paper|
      # see if the customer has changed (or is first record)
      unless paper.paper_customer_id == last_customer
         # if last_customer.nil?, don't need summary
         unless last_customer.nil?
            unless data.empty?
               pdf.table data, :border_width => 1, :border_style => :underline_header, :headers => ["Paper ID","Suit No.","Paper Type","Status","Paper$","Mileage$","Total"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :right, 5 => :right, 6 => :right}, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2, :column_widths => {0 => 65, 1 => 65, 2 => 165, 3 => 50, 4 => 75, 5 => 75, 6 => 75}
            end
            # need to add customer summary lines here
            grand_total += customer_total
            customer_total = BigDecimal.new("0.0",2)
            pdf.start_new_page
         end
         last_customer = paper.paper_customer_id
         data = []
         pdf.text "Paper Service #{@status.titleize} Report", :size => 24, :style => :bold, :align => :center
         pdf.move_down(5)
         pdf.text format_date(@report_datetime), :align => :center, :style => :bold
         pdf.pad(5){pdf.stroke_horizontal_rule}
         pdf.text "#{paper.customer.nil? ? 'Invalid Customer' : paper.customer.name}", :size => 18, :style => :bold, :align => :center
         pdf.move_down(10)
      end
      # add current paper to table data array
      if paper.served_date?
         paper_cost = (paper.paper_type.nil? ? '0.00' : paper.paper_type.cost)
         status = "Served"
      else
         paper_cost = noservice_fee
         status = "Returned"
      end
      mileage_cost = (miles_each * (paper.mileage? ? paper.mileage : 0)).round(2)
      data.push([paper.id,paper.suit_no,"#{paper.paper_type.nil? ? 'Invalid Type' : paper.paper_type.name}",status,number_to_currency(paper_cost),number_to_currency(mileage_cost),number_to_currency(paper_cost + mileage_cost)])
      customer_total += (paper_cost + mileage_cost)
   end
   pdf.table data, :border_width => 1, :border_style => :underline_header, :headers => ["Paper ID","Suit No.","Paper Type","Status","Paper$","Mileage$","Total"], :align => {0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :right, 5 => :right, 6 => :right}, :row_colors => [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], :padding => 2, :column_widths => {0 => 65, 1 => 65, 2 => 165, 3 => 50, 4 => 75, 5 => 75, 6 => 75}
   text = "#{@status.titleize}"
   pdf.move_down(5)
   pdf.text "Customer Total #{text}: #{number_to_currency(customer_total)}", :size => 14, :align => :right
   grand_total += customer_total
   pdf.move_down(5)
   pdf.text "Grand Total #{text}: #{number_to_currency(grand_total)}", :size => 16, :align => :right
   pdf.move_down(5)
   pdf.text "END OF REPORT", :style => :bold
end

pdf.number_pages "Page: <page> of <total>", [pdf.bounds.right - 55, 5]
