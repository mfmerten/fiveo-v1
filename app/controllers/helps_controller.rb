# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Helps are collections of application help pages.  These are administered by
# the Admin group.
class HelpsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Help Listing
   def index
      # all logged in users may browse the help index
      @help_page = ["Help Pages","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:help_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:help_filter] = nil
            redirect_to helps_path and return
         end
      else
         @query = session[:help_filter] || HashWithIndifferentAccess.new
      end
      unless @helps = paged('help',:order => 'section,page')
         @query = HashWithIndifferentAccess.new
         session[:help_filter] = nil
         redirect_to helps_path and return
      end
      if has_permission Perm[:admin]
         @links = [['New', edit_help_path]]
      end
   end

   # Show Help
   def show
      # All logged in users may view help pages
      params[:id] or raise_missing "Help ID"
      @help = Help.find_by_id(params[:id]) or raise_not_found "Help ID #{params[:id]}"
      @help_page = ["Help Pages","show"]
      @links = []
      if has_permission Perm[:admin]
         @links.push(['New', edit_help_path])
         @links.push(['Edit', edit_help_path(:id => @help.id)])
         @links.push(['Delete', @help, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Help?'}])
      end
   end
   
   # Display Help
   def popup
      # all logged in users may call the help popup window
      params[:id] or raise_missing "Help ID"
      @help = Help.find_by_id(params[:id]) or raise_not_found "Help ID #{params[:id]}"
      render :layout => 'popup'
   end

   # Add or Edit Help
   def edit
      # only site admins may add or edit help pages
      require_permission Perm[:admin]
      @help_page = ["Help Pages","edit"]
      if params[:id]
         @help = Help.find_by_id(params[:id]) or raise_not_found "Help ID #{params[:id]}"
         @page_title = "Edit Help ID: #{@help.id}"
      else
         @help = Help.new or raise_db(:new, "Help")
         @page_title = "New Help Page"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @help.new_record?
               redirect_to helps_path
            else
               redirect_to @help
            end
            return
         end
         @help.attributes = params[:help]
         if @help.save
            redirect_to @help
            if params[:id]
               user_action("Help Page","Edited Help Page ID: #{@help.id}.")
            else
               user_action("Help Page", "Added Help Page ID: #{@help.id}.")
            end
         end
      end
   end

   # Destroys the specified help. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:admin]
      require_method :delete
      params[:id] or raise_missing "Help ID"
      @help = Help.find_by_id(params[:id]) or raise_not_found "Help ID #{params[:id]}"
      @help.destroy or raise_db(:destroy, "Help ID #{params[:id]}")
      user_action("Help Page","Destroyed Help Page ID: #{params[:id]}!")
      flash[:notice] = 'Record Deleted!'
      redirect_to helps_path
   end

   protected

   # Ensures that the "Help Pages" menu item is highlighted for all actions in
   # this controller
   def set_tab_name
      @current_tab = "Help Pages"
   end
end
