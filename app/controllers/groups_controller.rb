# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Groups are collections of permissions that can be batch assigned
# to a user. See also UserController.
class GroupsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Group Listing
   def index
      require_permission Perm[:view_group]
      @help_page = ["Groups","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:group_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:group_filter] = nil
            redirect_to groups_path and return
         end
      else
         @query = session[:group_filter] || HashWithIndifferentAccess.new
      end
      unless @groups = paged('group',:order => 'name')
         @query = HashWithIndifferentAccess.new
         session[:group_filter] = nil
         redirect_to groups_path and return
      end
      if has_permission Perm[:update_group]
         @links = [['New', edit_group_path]]
      end
   end

   # Show Group
   def show
      require_permission Perm[:view_group]
      @help_page = ["Groups","show"]
      params[:id] or raise_missing "Group ID"
      @group = Group.find_by_id(params[:id]) or raise_not_found "Group ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_group]
         @links.push(['New', edit_group_path])
         unless @group.name == 'Admin'
            @links.push(['Edit', edit_group_path(:id => @group.id)])
         end
      end
      if has_permission Perm[:destroy_group]
         unless @group.name == 'Admin'
            @links.push(['Delete', @group, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Group?'}])
         end
      end
      @page_links = [[@group.creator,'creator'],[@group.updater,'updater']]
   end

   # Add or Edit Group
   def edit
      require_permission Perm[:update_group]
      @help_page = ["Groups","edit"]
      if params[:id]
         @group = Group.find_by_id(params[:id]) or raise_not_found "Group ID #{params[:id]}"
         if @group.name == "Admin"
            flash[:warning] = "Cannot Edit Admin Group"
            redirect_to @group and return
         end
         @page_title = "Edit Group ID: #{@group.id}"
         @group.updated_by = session[:user]
      else
         @group = Group.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Group")
         @page_title = "New Group"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @group.new_record?
               redirect_to groups_path
            else
               redirect_to @group
            end
            return
         end
         @group.attributes = params[:group]
         if @group.save
            redirect_to @group
            if params[:id]
               user_action("Group","Edited Group ID: #{@group.id}.")
            else
               user_action("Group", "Added Group ID: #{@group.id}.")
            end
         end
      end
   end

   # Destroys the specified group. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_group]
      require_method :delete
      params[:id] or raise_missing "Group ID"
      @group = Group.find_by_id(params[:id]) or raise_not_found "Group ID #{params[:id]}"
      if @group.name == "Admin"
         flash[:warning] = "Cannot Destroy Admin Group"
         redirect_to @group
      end
      @group.destroy or raise_db(:destroy, "Group ID #{params[:id]}")
      user_action("Group","Destroyed Group ID: #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to groups_path
   end

   protected

   # Ensures that the "Groups" menu item is highlighted for all actions in
   # this controller
   def set_tab_name
      @current_tab = "Groups"
   end
end
