# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# A rolodex type system that stores contact information for people and
# agencies, both civilian and law enforcement. Also functions as user
# profiles.
class ContactsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Contact Listing
   def index
      require_permission Perm[:view_contact]
      @help_page = ["Contacts","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:contact_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:contact_filter] = nil
            redirect_to contacts_path and return
         end
      else
         @query = session[:contact_filter] || HashWithIndifferentAccess.new
      end
      unless @contacts = paged('contact',:order => "contacts.fullname", :include => [{:phones => :label}, :agency], :alpha_name => "contacts.fullname")
         @query = HashWithIndifferentAccess.new
         session[:contact_filter] = nil
         redirect_to contacts_path and return
      end
      @links = []
      if has_permission Perm[:update_contact]
         @links.push(['New', edit_contact_path])
      end
   end

   # Show Contact
   def show
      require_permission Perm[:view_contact]
      @help_page = ["Contacts","show"]
      params[:id] or raise_missing "Contact ID"
      @contact = Contact.find_by_id(params[:id]) or raise_not_found "Contact ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_contact]
         @links.push(["New", edit_contact_path])
      end
      if (has_permission(Perm[:update_contact]) && @contact.user.nil?) || (!@contact.user.nil? && (has_permission(Perm[:update_user]) || (@contact.user.id == session[:user] && SiteConfig.user_update_contact)))
         @links.push(['Edit', edit_contact_path(:id => @contact.id)])
      end
      if has_permission(Perm[:destroy_contact]) && @contact.user.nil?
         @links.push(['Delete', @contact, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Contact?'}])
      end
      @page_links = [[@contact.agency,'Agency']]
   end
   
   # Add or Edit Contact
   def edit
      @help_page = ["Contacts","edit"]
      if params[:id]
         @contact = Contact.find_by_id(params[:id]) or raise_not_found "Contact ID #{params[:id]}"
         if @contact.user.nil?
            require_permission Perm[:update_contact]
         else
            allowed_array = [":update_user"]
            if SiteConfig.user_update_contact
               allowed_array.push("Owner")
            end
            (has_permission(Perm[:update_user]) || (@contact.user.id == session[:user] && SiteConfig.user_update_contact)) or raise_not_allowed "Update User Profile", allowed_array
         end
         @page_title = "Edit Contact ID: " + @contact.id.to_s
         if @contact.phones.empty?
            unless request.post?
               @contact.phones.build(:created_by => session[:user], :updated_by => session[:user])
            end
         end
         @contact.updated_by = session[:user]
         @contact.legacy = false
      else
         require_permission Perm[:update_contact]
         @contact = Contact.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Contact")
         unless request.post?
            @contact.phones.build(:created_by => session[:user], :updated_by => session[:user])
         end
         @page_title = "New Contact"
      end

      if request.post?
         if params[:commit] == "Cancel"
            if @contact.new_record?
               redirect_to contacts_path
            else
               redirect_to @contact
            end
            return
         end
         params[:contact][:existing_phone_attributes] ||= {}
         @contact.attributes = params[:contact]
         if @contact.save
            redirect_to(@contact)
            if params[:id]
               user_action("Contact","Edited Contact ID: #{@contact.id} (#{@contact.fullname}).")
            else
               user_action("Contact", "Added Contact ID: #{@contact.id} (#{@contact.fullname}).")
            end
         end
      end
   end

   # Destroys the specified contact. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_contact]
      require_method :delete
      params[:id] or raise_missing "Contact ID"
      @contact = Contact.find_by_id(params[:id]) or raise_not_found "Contact ID #{params[:id]}"
      @contact.user.nil? or raise_not_allowed "Destroy User Profile"
      @contact.destroy or raise_db(:destroy, "Contact ID #{params[:id]}")
      user_action("Contact", "Destroyed Contact ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to contacts_url
   end
   
   # merge two contact records into one
   def merge
      # merge is destructive, hence the destroy permission requirements
      require_permission Perm[:destroy_contact]
      params[:id] or raise_missing "Contact ID"
      @old_contact = Contact.find_by_id(params[:id]) or raise_not_found "Contact ID #{params[:id]}"
      params[:merge_id] or raise_missing "Merge Contact ID"
      @merge_contact = Contact.find_by_id(params[:merge_id]) or raise_not_found "Merge Contact ID #{params[:merge_id]}"
      if @old_contact.business?
         # agency contact
         unless @merge_contact.business?
            flash[:warning] = "Cannot merge business contact into person contact."
            redirect_to @old_contact and return
         end
      else
         # individual contact
         if @merge_contact.business?
            flash[:warning] = "Cannot merge person contact into business contact."
            redirect_to @old_contact and return
         end
      end
      if !@old_contact.user.nil?
         unless @merge_contact.user.nil?
            flash[:warning] = "Cannot merge one user profile into another."
            redirect_to @old_contact and return
         end
      end
      @page_title = "Merge Contacts (#{@old_contact.fullname} &rarr; #{@merge_contact.fullname})"
      @merge_contact.title = @old_contact.title unless @merge_contact.title?
      @merge_contact.unit = @old_contact.unit unless @merge_contact.unit?
      @merge_contact.agency_id = @old_contact.agency_id unless @merge_contact.agency.nil?
      @merge_contact.street = @old_contact.street unless @merge_contact.street?
      @merge_contact.city = @old_contact.city unless @merge_contact.city?
      @merge_contact.state_id = @old_contact.state_id unless @merge_contact.state_id?
      @merge_contact.zip = @old_contact.zip unless @merge_contact.zip?
      @merge_contact.pri_email = @old_contact.pri_email unless @merge_contact.pri_email?
      @merge_contact.alt_email = @old_contact.alt_email unless @merge_contact.alt_email?
      @merge_contact.web_site = @old_contact.web_site unless @merge_contact.web_site?
      @merge_contact.notes = @old_contact.notes unless @merge_contact.notes?
      @merge_contact.officer = true if @old_contact.officer?
      @merge_contact.street2 = @old_contact.street2 unless @merge_contact.street2?
      @merge_contact.badge_no = @old_contact.badge_no unless @merge_contact.badge_no?
      @merge_contact.active = true if @old_contact.active?
      @merge_contact.detective = true if @old_contact.detective?
      @merge_contact.physician = true if @old_contact.physician?
      @merge_contact.pharmacy = true if @old_contact.pharmacy?
      @merge_contact.legacy = false
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to @old_contact and return
         end
         params[:contact][:existing_phone_attributes] ||= {}
         @merge_contact.attributes = params[:contact]
         if @merge_contact.save
            recs, errs = @old_contact.merge_contact(@merge_contact.id, session[:user])
            if recs == -1
               flash[:warning] = "Merge Failed: Contact model returned parameter error!"
               redirect_to @old_contact and return
            elsif !errs.empty?
               flash[:warning] = "Merge Failed: parameter errors returned by these models: #{errs.join(', ')}."
               redirect_to @old_contact and return
            else
               flash[:notice] = "Merge Succeeded: #{recs} records updated, old contact deleted!"
               redirect_to(@merge_contact)
               user_action("Contact","Merged Contact ID: #{@old_contact.id} into Contact ID: #{@merge_contact.id}.")
               @old_contact.destroy or raise_db(:destroy, "Contact ID #{params[:id]}")
            end
         end
      end
   end
   
   # This prints a PDF version of a contact list per selected agency
   def contact_list
      require_permission Perm[:view_contact]
      params[:id] or raise_missing "Contact ID"
      @contact = Contact.find_by_id(params[:id]) or raise_not_found "Contact ID #{params[:id]}"
      unless @contact.business?
         flash[:warning] = 'Contact must be business, not person!'
         redirect_to @contact and return
      end
      @contacts = Contact.find(:all, :order => 'length(unit), unit, fullname', :include => :phones, :conditions => {:agency_id => @contact.id, :active => true, :printable => true})
      @time = DateTime.now
      if @contacts.size < 2
         flash[:warning] = "No Contacts To Report For This Business!"
         redirect_to @contact and return
      end
      @page_title = "Contact List for #{@contact.fullname}"
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Contact_List-#{@contact.fullname.sub(/[ .,]/,'_')}.pdf"
      render :template => 'contacts/contact_list.pdf.prawn', :layout => false
      user_action("Contact","Printed Contact List for #{@contact.fullname}.")
   end
   
   # called from various views using AJAX, it returns a contact name and
   # address, or invalid message if not a valid contact id.
   def validate
      if has_permission Perm[:view_contact]
         c = "INVALID CONTACT ID"
         if cont = Contact.find_by_id(params[:c])
            a2 = cont.address_line2.blank? ? "" : "#{cont.address_line2}<br />"
            c = "<div>#{cont.fullname}<br />#{cont.address_line1}<br />#{a2}#{cont.address_line3}</div>"
         end
         render :text => c
      else
         render :text => ''
      end
   end
   
   # called from various views (via a javascript called from a "find by name"
   # button press), This accepts a name partial (name parameter) and attempts
   # to find matching records in contacts. If name is not specified, a
   # complete contact listing will be used. Matching records are displayed in
   # a popup window. Clicking on the appropriate record will insert (via
   # javascript) the unit and contact name into the specified fields
   # (caller_unit and caller_name parameters). The window then closes.
   def lookup
      if (name = params[:name]) && has_permission(Perm[:view_contact])
         @contacts = Contact.find(:first, :conditions => ['fullname like ?', '%' + params[:name] + '%'])
         @page_title = "Results for '#{name}'"
         @caller_unit = params[:caller_unit]
         @caller_name = params[:caller_name]
         render :layout => 'popup'
      else
         render :text => ''
      end
   end

   protected

   # ensures that the "Contacts" menu item is highlighted for all actions in
   # this controller.
   def set_tab_name
      @current_tab = "Contacts"
   end
end
