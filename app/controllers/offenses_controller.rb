# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Offenses are the offense reports entered by patrol officers.
class OffensesController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Offense Listing
   def index
      require_permission Perm[:view_offense]
      @help_page = ["Offenses","index"]
      if params[:person_id]
         @person = Person.find_by_id(params[:person_id]) or raise_not_found "Person ID #{params[:person_id]}"
         temp = @person.offenses_as_victim
         temp << @person.offenses_as_witness
         temp << @person.offenses_as_suspect
         @offenses = paged_array(temp.uniq.sort{|a,b| b.offense_date <=> a.offense_date})
         @page_title = "Offenses Referencing: #{show_person @person}"
         @links = [['Show All', offenses_path]]
      else
         if @query = params[:search]
            if params[:commit] == "Set Filter"
               session[:offense_filter] = @query
            else
               @query = HashWithIndifferentAccess.new
               session[:offense_filter] = nil
               redirect_to offenses_path and return
            end
         else
            @query = session[:offense_filter] || HashWithIndifferentAccess.new
         end
         unless @offenses = paged('offense',:order => 'offense_date DESC, offense_time DESC', :include => [:wreckers, :call])
            @query = HashWithIndifferentAccess.new
            session[:offense_filter] = nil
            redirect_to offenses_path and return
         end
      end
      if has_permission Perm[:update_offense]
         @links = [['New', edit_offense_path]]
      end
   end

   # Show Offense
   def show
      require_permission Perm[:view_offense]
      @help_page = ["Offenses","show"]
      params[:id] or raise_missing "Offense ID"
      @offense = Offense.find_by_id(params[:id]) or raise_not_found "Offense ID #{params[:id]}"
      @page_title = "Offense ID: #{@offense.id}#{@offense.legacy? ? ' -- [Legacy]' : ''}"
      @links = []
      if has_permission Perm[:update_offense]
         @links.push(['New', edit_offense_path])
         if check_author(@offense)
            @links.push(['Edit', edit_offense_path(:id => @offense.id)])
         end
      end
      if has_permission(Perm[:destroy_offense]) && check_author(@offense)
         @links.push(['Delete', @offense, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Offense?'}])
      end
      if has_permission(Perm[:update_offense]) && check_author(@offense)
         @links.push(['New Photo', edit_offense_photo_path(:offense_id => @offense.id)])
      end
      @links.push(['PDF', url_for(:action => 'offense_report', :offense_id => @offense.id)])
      @linksx = []
      if has_permission(Perm[:update_citation]) && check_author(@offense)
         @linksx.push(['New Citation', edit_citation_path(:offense_id => @offense.id)])
      end
      if has_permission(Perm[:update_evidence]) && check_author(@offense)
         @linksx.push(['New Evidence', edit_evidence_path(:offense_id => @offense.id)])
      end
      if has_permission(Perm[:update_forbid]) && check_author(@offense)
         @linksx.push(['New Forbid', edit_forbid_path(:offense_id => @offense.id)])
      end
      @page_links = [@offense.call,[@offense.officer_id,'officer'],[@offense.creator,'creator'],[@offense.updater,'updater']]
   end

   # Add or Edit Offense
   def edit
      require_permission Perm[:update_offense]
      @help_page = ["Offenses","edit"]
      if params[:id]
         @offense = Offense.find_by_id(params[:id]) or raise_not_found "Offense ID #{params[:id]}"
         enforce_author(@offense)
         @offense.updated_by = session[:user]
         @offense.legacy = false
         @page_title = "Edit Offense ID: #{@offense.id}"
         unless request.post?
            if @offense.victims.empty?
               @offense.victims.build
            end
            if @offense.suspects.empty?
               @offense.suspects.build
            end
            if @offense.witnesses.empty?
               @offense.witnesses.build
            end
            if @offense.wreckers.empty?
               @offense.wreckers.build
            end
         end
      else
         @offense = Offense.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Offense")
         @offense.call_id = params[:call_id]
         if !current_user.contact.nil?
            @offense.officer_id = current_user.contact.id
         else
            @offense.officer = session[:user_name]
            @offense.officer_unit = session[:user_unit]
            @offense.officer_badge = session[:user_badge]
         end
         @offense.offense_date = Date.today
         @page_title = 'New Offense Report'
         unless @offense.call.nil?
            @offense.case_no = @offense.call.case_no
            if @offense.call.arrival_date?
               @offense.offense_date = @offense.call.arrival_date
               @offense.offense_time = @offense.call.arrival_time
            else
               @offense.offense_date = @offense.call.call_date
               @offense.offense_time = @offense.call.call_time
            end
         end
         unless request.post?
            @offense.victims.build
            @offense.suspects.build
            @offense.witnesses.build
            @offense.wreckers.build
         end
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @offense.new_record?
               redirect_to offenses_path
            else
               redirect_to @offense
            end
            return
         end
         params[:offense][:assigned_victims] ||= {}
         params[:offense][:assigned_witnesses] ||= {}
         params[:offense][:assigned_suspects] ||= {}
         params[:offense][:existing_wrecker_attributes] ||= {}
         @offense.attributes = params[:offense]
         if @offense.save
            redirect_to @offense
            if params[:id]
               user_action("Offense","Edited Offense ID: #{@offense.id}.")
            else
               user_action("Offense", "Added Offense ID: #{@offense.id}.")
            end
         end
      end
   end

   # Destroys the specified offense. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_offense]
      require_method :delete
      params[:id] or raise_missing "Offense ID"
      @offense = Offense.find_by_id(params[:id]) or raise_not_found "Offense ID #{params[:id]}"
      enforce_author(@offense)
      @offense.destroy or raise_db(:destroy, "Offense ID #{params[:id]}")
      user_action("Offense","Destroyed Offense ID: #{params[:id]}.")
      flash[:notice] = 'Record Deleted!'
      redirect_to offenses_path
   end
   
   # Generates a PDF offense report form for the specified offense id (via
   # offense_id parameter).
   def offense_report
      require_permission Perm[:view_offense]
      params[:offense_id] or raise_missing "Offense ID"
      @offense = Offense.find_by_id(params[:offense_id]) or raise_not_found "Offense ID #{params[:offense_id]}"
      prawnto :prawn => {
         :page_size => 'LEGAL',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Offense-#{@offense.id}.pdf"
      user_action("Offense","Printed Offense ID: #{@offense.id}.")
      render :template => 'offenses/offense_form.pdf.prawn', :layout => false
   end
   
   protected

   # Ensures that the "Offense" menu item is highlighted for all actions in
   # this controller
   def set_tab_name
      @current_tab = 'Offense'
   end
end
