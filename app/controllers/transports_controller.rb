# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Transports produce the transportation vouchers required by the civil office
class TransportsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Transport Listing
   def index
      require_permission Perm[:view_transport]
      @help_page = ["Transports","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:transport_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:transport_filter] = nil
            redirect_to transports_path and return
         end
      else
         @query = session[:transport_filter] || HashWithIndifferentAccess.new
      end
      unless @transports = paged('transport', :order => 'begin_date DESC, begin_time DESC', :include => [:person])
         @query = HashWithIndifferentAccess.new
         session[:transport_filter] = nil
         redirect_to transports_path and return
      end
      @links = []
      if has_permission(Perm[:update_transport])
         @links.push(['New', edit_transport_path])
      end
   end

   # Show Transport
   def show
      require_permission Perm[:view_transport]
      @help_page = ["Transports","show"]
      params[:id] or raise_missing "Transport ID"
      @transport = Transport.find_by_id(params[:id]) or raise_not_found "Transport ID #{params[:id]}"
      @links = []
      if has_permission(Perm[:update_transport])
         @links.push(["New", edit_transport_path])
         @links.push(["Edit", edit_transport_path(:id => @transport.id)])
      end
      if has_permission Perm[:destroy_transport]
         @links.push(['Delete', @transport, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Transportation Voucher?'}])
      end
      @links.push(["PDF", transport_voucher_path(:transport_id => @transport.id)])
      @page_links = [@transport.person, [@transport.officer1_id,'officer'],[@transport.officer2_id,'officer'],[@transport.from_id,'from'],[@transport.to_id,'to'],[@transport.creator,'creator'],[@transport.updater,'updater']]
   end

   # Add or Edit Transport
   def edit
      require_permission Perm[:update_transport]
      @help_page = ["Transports","edit"]
      if params[:id]
         @transport = Transport.find_by_id(params[:id]) or raise_not_found "Transport ID #{params[:id]}"
         @page_title = "Edit Transport ID: #{@transport.id}"
         @transport.updated_by = session[:user]
      else
         @page_title = "New Transport"
         @transport = Transport.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Transport")
         @transport.from_state_id = Option.lookup("State","Louisiana")
         @transport.to_state_id = Option.lookup("State","Louisiana")
         @transport.person_id = params[:person_id]
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @transport.new_record?
               redirect_to transports_path
            else
               redirect_to @transport
            end
            return
         end
         @transport.attributes = params[:transport]
         if @transport.save
            redirect_to @transport
            if params[:id]
               user_action("Transports","Edited Transport ID: #{@transport.id}.")
            else
               user_action("Transports", "Added Transport ID: #{@transport.id}.")
            end
         end
      end
   end

   # Destroys the specified transport. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_transport]
      require_method :delete
      params[:id] or raise_missing "Transport ID"
      @transport = Transport.find_by_id(params[:id]) or raise_not_found "Transport ID #{params[:id]}"
      @transport.destroy or raise_db(:destroy, "Transport ID #{params[:id]}")
      user_action("Transports","Destroyed Transport ID #{params[:id]}.")
      flash[:notice] = 'Record Deleted!'
      redirect_to transports_path
   end

   # Generates a PDF transportation voucher for the specified transport
   def voucher
      require_permission Perm[:view_transport]
      params[:transport_id] or raise_missing "Transport ID"
      @transport = Transport.find_by_id(params[:transport_id]) or raise_not_found "Transport ID #{params[:id]}"
      user_action("Transports","Printed Voucher for Transport ID #{@transport.id}.")
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Voucher-#{@transport.id}.pdf"
      render :template => 'transports/voucher.pdf.prawn', :layout => false
   end

   protected

   # highlights "Transports" in menu for all actions in this controller.
   def set_tab_name
      @current_tab = "Transports"
   end
end
