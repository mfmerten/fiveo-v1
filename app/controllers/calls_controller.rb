# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# These are the dispatcher call sheets.
class CallsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Call Listing
   def index
      require_permission Perm[:view_call]
      @help_page = ["Calls","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:call_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:call_filter] = nil
            redirect_to calls_path and return
         end
      else
         @query = session[:call_filter] || HashWithIndifferentAccess.new
      end
      unless @calls = paged("call", :order => "calls.call_date DESC, calls.call_time DESC", :include => [:received_via, :call_units, :signal_code, {:call_subjects => :subject_type}])
         @query = HashWithIndifferentAccess.new
         session[:call_filter] = nil
         redirect_to calls_path and return
      end
      @links = []
      if has_permission Perm[:update_call]
         @links.push(['New', edit_call_path])
      end
      if current_user.review_call?
         @links.push(['Review', url_for(:action => 'review')])
      end
      # define shifts for shift report
      @shifts = valid_shifts
   end

   # Show Call
   def show
      require_permission Perm[:view_call]
      @help_page = ["Calls","show"]
      params[:id] or raise_missing "Call ID"
      @call = Call.find_by_id(params[:id]) or raise_not_found "Call ID #{params[:id]}"
      @page_title = "Call ID: #{@call.id}#{@call.legacy? ? ' -- [Legacy]' : ''}"
      @links = []
      if has_permission Perm[:update_call]
         @links.push(["New", edit_call_path])
         @links.push(["Edit", edit_call_path(:id => @call.id)])
      end
      if has_permission Perm[:destroy_call]
         @links.push(['Delete', @call, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Call Sheet?'}])
      end
      @links.push(['PDF', url_for(:action => 'call_sheet', :id => @call.id)])
      if has_permission Perm[:update_offense]
         @links.push(['New Offence', edit_offense_path(:call_id => @call.id)])
      end
      if has_permission Perm[:update_forbid]
         @links.push(['New Forbid', edit_forbid_path(:call_id => @call.id)])
      end
      @page_links = [[@call.received_by_id,'receiver'],[@call.detective_id,'detective'],[@call.dispatcher_id,'dispatcher'],[@call.creator,'creator'],[@call.updater,'updater']]
   end

   # Add or Edit Call
   def edit
      require_permission Perm[:update_call]
      @help_page = ["Calls","edit"]
      if params[:id]
         @call = Call.find_by_id(params[:id]) or raise_not_found "Call ID #{params[:id]}"
         @page_title = "Edit Call ID: " + @call.id.to_s
         @call.updated_by = session[:user]
         @call.legacy = false
         @call.signal = (@call.signal_code.nil? ? "" : @call.signal_code.short_name)
         if @call.call_units.empty?
            unless request.post?
               @call.call_units.build(:created_by => session[:user], :updated_by => session[:user])
            end
         end
         if @call.call_subjects.empty?
            unless request.post?
               @call.call_subjects.build(:created_by => session[:user], :updated_by => session[:user])
            end
         end
      else
         @page_title = "New Call"
         @call = Call.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Call")
         if !current_user.contact.nil?
            @call.dispatcher_id = current_user.contact.id
            @call.received_by_id = current_user.contact.id
         else
            @call.dispatcher = session[:user_name]
            @call.dispatcher_unit = session[:user_unit]
            @call.dispatcher_badge = session[:user_badge]
            @call.received_by = session[:user_name]
            @call.received_by_unit = session[:user_unit]
            @call.received_by_badge = session[:user_badge]
         end
         @call.call_date = Date.today
         @call.followup_no = ""
         unless request.post?
            @call.call_units.build(:created_by => session[:user], :updated_by => session[:user])
            @call.call_subjects.build(:created_by => session[:user], :updated_by => session[:user])
         end
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @call.new_record?
               redirect_to calls_path
            else
               redirect_to @call
            end
            return
         end
         params[:call][:existing_unit_attributes] ||= {}
         params[:call][:existing_subject_attributes] ||= {}         
         @call.attributes = params[:call]
         if @call.save
            redirect_to @call
            if params[:id]
               user_action("Call","Edited Call ID: #{@call.id}.")
            else
               user_action("Call", "Added Call ID: #{@call.id}.")
            end
         end
      end
   end

   # Destroys the specified call sheet. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_call]
      require_method :delete
      params[:id] or raise_missing "Call ID"
      @call = Call.find_by_id(params[:id]) or raise_not_found "Call ID #{params[:id]}"
      @call.destroy or raise_db(:destroy, "Call ID #{params[:id]}")
      user_action("Call", "Destroyed Call ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to calls_path
   end
   
   # This is called from several views via AJAX to validate that a call id
   # number is valid, and to return the call details for display.
   def validate
      unless has_permission Perm[:view_call]
         render :text => ""
         return
      end
      c = params[:c]
      desc_id = params[:call_description_id]
      case_id = params[:case_no_id]
      render :text => "" and return if c.nil?
      render :text => "" and return unless call = Call.find_by_id(c)
      details = %Q(<div class='call_detail'>) + RedCloth.new(call.details).to_html + %Q(</div>)
      render :update do |page|
         page.replace_html desc_id, details
         page[case_id].value = call.case_no
      end
   end
   
   # This is called from the call sheet edit view via AJAX to validate entered
   # signal codes, and return the full signal name.
   def validate_signal
      p = "UNKNOWN"
      if x = Option.lookup("Signal Code", params[:p], "short_name", "long_name")
         p = x
      end
      render :text => p
   end
   
   # This generates a popup window with a list of valid signal codes. Its
   # called from a link on the call edit view.
   def signals
      # no special permission required
      ot = OptionType.find_by_name("Signal Code") or raise_not_found "Option Type 'Signal Code'"
      @signals = Option.find_all_by_option_type_id(ot, :order => "long_name", :conditions => 'disabled != 1')
      @page_title = "Signal Code Reference"
      render :layout => 'popup'
   end
   
   # This prints a PDF version of the call sheet.
   def call_sheet
      require_permission Perm[:view_call]
      params[:id] or raise_missing "Call ID"
      @call = Call.find_by_id(params[:id]) or raise_not_found "Call ID #{params[:id]}"
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Call_Sheet-#{@call.id}.pdf"
      render :template => 'calls/call_sheet.pdf.prawn', :layout => false
      user_action("Call","Printed Call Sheet for Call ID: #{@call.id}.")
   end
   
   # This prints a PDF version of the shift report
   def shift_report
      require_permission Perm[:view_call]
      params[:date] or raise_missing "Date"
      @date = valid_date(params[:date]) or raise_invalid "Date", "'#{params[:date]}' Is Not A Valid Date"
      params[:shift] or raise_missing "Shift"
      @shift = params[:shift].to_i
      valid_shifts.include?(@shift) or raise_invalid "Shift", valid_shifts.collect{|v| v.to_s}
      if @shift == 1
         @start = SiteConfig.dispatch_shift1_start
         @stop = SiteConfig.dispatch_shift1_stop
      elsif @shift == 2
         @start = SiteConfig.dispatch_shift2_start
         @stop = SiteConfig.dispatch_shift2_stop
      else
         @start = SiteConfig.dispatch_shift3_start
         @stop = SiteConfig.dispatch_shift3_stop
      end
      !@start.nil? && !@stop.nil? or raise_invalid "Start or Stop Time", "Check shift #{params[:shift]} settings in Site Configuration"
      if @start < @stop
         conditions = "call_date = '#{@date}' and call_time >= '#{@start}' and call_time < '#{@stop}'"
      else
         conditions = "(call_date = '#{@date}' and call_time >= '#{@start}') OR (call_date = '#{@date + 1.day}' and call_time < '#{@stop}')"
      end
      @calls = Call.find(:all, :order => 'call_date, call_time', :conditions => conditions)
      @time = DateTime.now
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Shift_Report-#{format_date(@time, :timestamp => true)}.pdf"
      render :template => 'calls/shift_report.pdf.prawn', :layout => false
      user_action("Call","Printed Shift Report for Shift #{@shift}.")
   end
   
   # Review latest calls
   def review
      require_permission Perm[:view_call]
      @page_title = "Calls Review"
      @calls = review_paged('call',:include => [:received_via,:signal_code,:call_units,:call_subjects])
      @links = [["View Next Page", url_for(:action => 'review')]]
      @links.push(["Skip All",url_for(:action => 'skip_all')])
   end
   
   # skip all call reviews
   def skip_all
      require_permission Perm[:view_call]
      review_skip_all('call')
      flash[:notice] = "All call reviews skipped!"
      redirect_to root_path
      return
   end
   
   protected

   # Ensures that the "Calls" menu item is highlighted for all actions in
   # this controller.
   def set_tab_name
      @current_tab = "Calls"
   end
end
