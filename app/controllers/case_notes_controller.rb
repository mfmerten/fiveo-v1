# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Case notes are notes entered by officers pertaining to a particular case
# number, with no other association.
class CaseNotesController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Case Note Listing
   def index
      require_permission Perm[:view_case]
      @help_page = ["Case Notes","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:case_note_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:case_note_filter] = nil
            redirect_to case_notes_path and return
         end
      else
         @query = session[:case_note_filter] || HashWithIndifferentAccess.new
      end
      unless @case_notes = paged('case_note', :order => 'case_no DESC, note_datetime DESC', :include => :creator)
         @query = HashWithIndifferentAccess.new
         session[:case_note_filter] = nil
         redirect_to case_notes_path and return
      end
   end

   # Show Case Note
   def show
      require_permission Perm[:view_case]
      @help_page = ["Case Notes","show"]
      params[:id] or raise_missing "Case Note ID" 
      @case_note = CaseNote.find_by_id(params[:id]) or raise_not_found "Case Note ID #{params[:id]}"
      @links = [["Back", case_path(:case => @case_note.case_no)]]
      if has_permission Perm[:update_case]
         @links.push(["New", edit_case_note_path(:case => @case_note.case_no)])
         if @case_note.created_by == session[:user] || has_permission(Perm[:author_all])
            @links.push(["Edit", edit_case_note_path(:id => @case_note.id)])
         end
      end
      if has_permission Perm[:destroy_case]
         @links.push(['Delete', @case_note, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Case Note?'}])
      end
      @page_links = [[@case_note.creator, 'Creator'],[@case_note.updater, 'Updater']]
   end

   # Add or Edit Case Note
   def edit
      require_permission Perm[:update_case]
      @help_page = ["Case Notes","edit"]
      if params[:id]
         @case_note = CaseNote.find_by_id(params[:id]) or raise_not_found "Case Note ID #{params[:id]}"
         enforce_author(@case_note)
         @page_title = "Edit Note ID: #{@case_note.id} for Case: #{@case_note.case_no}"
         @case_note.updated_by = session[:user]
      else
         params[:case] or raise_missing "Case Number"
         @case_note = CaseNote.new(:case_no => params[:case], :note_datetime => DateTime.now, :created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Case Note")
         @page_title = "New Note for Case: " + @case_note.case_no
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @case_note.new_record?
               redirect_to case_path(:case => @case_note.case_no)
            else
               redirect_to @case_note
            end
            return
         end
         @case_note.attributes = params[:case_note]
         if @case_note.save
            redirect_to @case_note
            if params[:id]
               user_action("Case Note","Edited Case Note ID: #{@case_note.id}.")
            else
               user_action("Case Note", "Added Case Note ID: #{@case_note.id}.")
            end
         end
      end
   end

   # Destroys the specified case note. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_case]
      require_method :delete
      params[:id] or raise_missing "Case Note ID"
      @case_note = CaseNote.find_by_id(params[:id]) or raise_not_found "Case Note ID #{params[:id]}"
      @case_note.destroy or raise_db(:destroy, "Case Note ID #{params[:id]}")
      user_action("Case Note","Destroyed Case Note ID: #{params[:id]}.")
      flash[:notice] = "Record Destroyed!"
      redirect_to case_notes_path
   end

   protected

   # ensures that the "Cases" menu item is highlighted for all actions in
   # this controller
   def set_tab_name
      @current_tab = "Case Notes"
   end
end
