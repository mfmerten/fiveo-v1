# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Announcements are news items displayed on the home page. Items are displayed
# in reverse date order (newest first) along with the full name and photo
# thumbnail of the author.
#
class AnnouncementsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Announcement Listing
   def index
      require_permission Perm[:view_announcement]
      @help_page = ["Announcements","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:announcement_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:announcement_filter] = nil
            redirect_to announcements_path and return
         end
      else
         @query = session[:announcement_filter] || HashWithIndifferentAccess.new
      end
      unless @announcements = paged('announcement', :order => 'updated_at DESC', :include => :creator)
         @query = HashWithIndifferentAccess.new
         session[:announcement_filter] = nil
         redirect_to announcements_path and return
      end
      @links = [["New", edit_announcement_path]]
   end

   # Show Announcement
   def show
      require_permission Perm[:view_announcement]
      @help_page = ["Announcements","show"]
      params[:id] or raise_missing "Announcement ID"
      @announcement = Announcement.find_by_id(params[:id]) or raise_not_found "Announcement ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_announcement]
         @links.push(["New", edit_announcement_path])
         @links.push(["Edit", edit_announcement_path(:id => @announcement.id)])
      end
      if has_permission Perm[:destroy_announcement]
         @links.push(["Delete", @announcement, {:method => 'delete', :confirm => "Are you sure you want to destroy this Announcement?"}])
      end
      @page_links = [[@announcement.creator, 'Creator'], [@announcement.updater, 'Updater']]
   end
   
   def announcements
      @announcements = Announcement.find(:all, :order => 'updated_at DESC')
      @page_title = "All Announcements"
      @links = [["Home", root_path]]
   end

   # Add/Edit Announcement
   def edit
      require_permission Perm[:update_announcement]
      @help_page = ["Announcements","edit"]
      if params[:id]
         @announcement = Announcement.find_by_id(params[:id]) or raise_not_found "Announcement ID #{params[:id]}"
         @page_title = "Edit Announcement ID: #{@announcement.id}"
         @announcement.updated_by = session[:user]
      else
         @announcement = Announcement.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Announcement")
         @page_title = "New Announcement"
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @announcement.new_record?
               redirect_to announcements_path
            else
               redirect_to @announcement
            end
            return
         end
         @announcement.attributes = params[:announcement]
         if params[:commit] == "Preview"
            @preview = @announcement.body
            render :action => "edit"
         else
            if @announcement.save
               @announcement.ignorers.clear
               redirect_to @announcement
               if params[:id]
                  user_action("Announcement","Edited Announcement ID: #{@announcement.id}.")
               else
                  user_action("Announcement", "Added Announcement ID: #{@announcement.id}.")
               end
            end
         end
      end
   end

   # Destroys the specified announcement record. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_announcement]
      require_method :delete
      params[:id] or raise_missing "Announcement ID"
      @announcement = Announcement.find_by_id(params[:id]) or raise_not_found "Announcement ID #{params[:id]}"
      @announcement.destroy or raise_db(:destroy, "Announcement ID #{params[:id]}")
      user_action("Announcement","Destroyed Announcement ID: #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to announcements_path
   end
   
   def toggle_visible
      params[:id] or raise_missing "Announcement ID"
      @announcement = Announcement.find_by_id(params[:id]) or raise_not_found "Announcement ID #{params[:id]}"
      unless @announcement.unignorable?
         if @announcement.ignorers.include?(current_user)
            @announcement.ignorers.delete(current_user)
            user_action("Announcement","Unignored Announcement ID: #{params[:id]}!")
         else
            @announcement.ignorers << current_user
            @announcement.save(false)
            user_action("Announcement","Ignored Announcement ID: #{params[:id]}!")
         end
      end
      if params[:page] == "announcements"
         redirect_to url_for(:controller => 'announcements', :action => 'announcements')
      else
         redirect_to root_path
      end
   end
   
   protected

   # ensures that all actions will have the "Announcements" menu item properly
   # highlighted as current.
   def set_tab_name
      @current_tab = "Announcements"
   end
end
