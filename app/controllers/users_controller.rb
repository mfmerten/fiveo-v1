# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Application User Accounts Controller
class UsersController < ApplicationController

   skip_before_filter :login_required, :only => ["login", "change_password", "logout"]
   before_filter :update_session_active, :except => ['login','logout']

   # user login.
   def login
      unless flash[:message] || flash[:notice] || flash[:warning]
         flash.now[:notice] = "Please login to continue."
      end
      unless session[:user].nil?
         flash[:notice] = "You are already logged in."
         redirect_to root_path and return
      end
      @page_title = "Please Log In"
      if request.post?
         if temp = User.authenticate(params[:user][:login], params[:user][:password])
            # authenticated correctly, check account settings
            if temp.locked?
               user_action("Users","Failed Login to locked account for #{params[:user][:login]} from #{request.remote_ip}!")
               flash[:warning] = "Account Locked! Contact Your Supervisor"
               redirect_to :action => 'login' and return
            end
            if temp.disabled_text?
               user_action("Users","Failed Login to disabled account for #{params[:user][:login]} from #{request.remote_ip}!")
               flash[:warning] = "Account Disabled! Contact Your Supervisor"
               redirect_to :action => 'login' and return
            end
            # not locked or disabled
            session[:started_at] = Time.now
            session[:active_at] = Time.now
            session[:user] = temp.id
            # login successful, load perm list
            session[:perms] = temp.pids
            session[:user_name] = temp.name
            session[:user_unit] = temp.unit
            session[:user_badge] = temp.badge
            session[:user_login] = temp.login
            session[:user_expires] = temp.expires
            session[:items_per_page] = temp.items_per_page
            session[:user_show_tips] = temp.show_tips
            user_action("Users","Logged In from #{request.remote_ip}!")
            if temp.expires <= Date.today
               flash[:warning] = "Password Expired!"
               redirect_to(:action => 'change_password')
            else
               redirect_to root_path
            end
         else
            reset_session
            flash[:warning] = "Login unsuccessful"
            user_action("Users","Failed Login for #{params[:user][:login]} from #{request.remote_ip}!")
            redirect_to(:action => 'login')
         end
      end
   end

   # Logs the user out of the system (clears the session information) and
   # redirects the user to the login view.
   def logout
      unless session[:user]
         flash[:notice] = "Please login to continue."
         redirect_to :action => "login" and return
      end
      user_action("Users","Logged Out from #{request.remote_ip}!")
      reset_session
      if params[:message]
         flash[:message] = params[:message]
      elsif params[:notice]
         flash[:notice] = params[:notice]
      elsif params[:warning]
         flash[:warning] = params[:warning]
      else
         flash[:notice] = "Please login to continue."
      end
      redirect_to :action => "login"
   end

   # Change Password
   def change_password
      @help_page = ["New Password","change_password"]
      @current_tab = "New Password"
      unless session[:user]
         redirect_to :action => 'login'
         return
      end
      @user = current_user
      @page_title = "Change Password For: " + @user.login

      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to root_path
            return
         end
         if User.authenticate(@user.login, params[:user][:old_password])
            if params[:user][:old_password] == params[:user][:password]
               @user.errors.add(:password, "cannot be the same as your old password!")
            elsif params[:user][:password] == "pa$$word"
               @user.errors.add(:password, "cannot be pa$$word!")
            else
               @user.update_attributes(:password=>params[:user][:password], :password_confirmation => params[:user][:password_confirmation])
               if @user.save
                  session[:user_expires] = @user.expires
                  redirect_to root_path
                  user_action("Users","Changed Their Password.")
               end
            end
         else
            flash[:notice] = "Invalid Current Password"
         end
      end
   end

   # User Listing
   def index
      require_permission Perm[:view_user]
      @help_page = ["Users","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:user_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:user_filter] = nil
            redirect_to users_path and return
         end
      else
         @query = session[:user_filter] || HashWithIndifferentAccess.new
      end
      unless @users = paged('user', :order => 'users.login', :include => [:groups, {:contact => :agency}])
         @query = HashWithIndifferentAccess.new
         session[:user_filter] = nil
         redirect_to users_path and return
      end
      @current_tab = "Users"
      @links = [['New', edit_user_path],['Photos', url_for(:action => 'photo_album')]]
   end

   # Show User
   def show
      require_permission Perm[:view_user]
      @help_page = ["Users","show"]
      @current_tab = "Users"
      params[:id] or raise_missing "User ID"
      @user = User.find_by_id(params[:id]) or raise_not_found "User ID #{params[:id]}"
      unless @contact = @user.contact
         flash[:warning] = 'User has no profile! Please edit user to fix it!'
      end
      @links = []
      if has_permission(Perm[:reset_user])
         @links.push(['Reset Pswd', reset_user_path(:id => @user.id), {:method => 'post', :confirm => 'Are you sure you want to reset this users Password?'}])
      end
      if has_permission(Perm[:update_user])
         @links.push(['Expire Pswd', url_for(:action => 'expire', :id => @user.id), {:method => 'post', :confirm => 'Are you sure you want to expire this users Password?'}])
         @links.push([(@user.locked ? "Unlock" : "Lock"), toggle_user_path(:id => @user.id), {:method => 'post'}])
         @links.push(["New", edit_user_path])
         @links.push(["Edit", edit_user_path(:id => @user.id)])
      end
      if has_permission Perm[:destroy_user]
         @links.push(['Delete', @user, {:method => 'delete', :confirm => 'Are you sure you want to delete this User?'}])
      end
      if has_permission(Perm[:update_user])
         @links.push(["Photo", photo_user_path(:id => @user.id)])
      end
      @page_links = [@user.contact, [@user.creator,'creator'],[@user.updater,'updater']]
   end

   # This action resets a user's password to "pa$$word" and expires it so that
   # they are forced to change it to something else the next time they log in.
   def reset
      require_permission Perm[:reset_user]
      require_method :post
      params[:id] or raise_missing "User ID"
      @user = User.find_by_id(params[:id]) or raise_not_found "User ID #{params[:id]}"
      @user.update_attributes(:password=>"pa$$word", :password_confirmation =>"pa$$word")
      @user.save or raise_db(:save, "User")
      flash[:notice]="Password Reset. Temporary password is 'pa$$word'"
      user_action("Users","Reset User Password for #{@user.login}.")
      redirect_to users_path
   end
   
   # this action expires a users password causing the to have to change it the
   # next time they log in
   def expire
      require_permission Perm[:update_user]
      require_method :post
      params[:id] or raise_missing "User ID"
      @user = User.find_by_id(params[:id]) or raise_not_found "User ID #{params[:id]}"
      @user.update_attribute(:expires, Date.yesterday)
      flash[:notice] = "Password Expired. User will be required to change it on next login."
      user_action("Users","Expired User Password for #{@user.login}.")
      redirect_to users_path
   end
   
   # Add or Edit User
   def edit
      require_permission Perm[:update_user]
      @help_page = ["Users","edit"]
      @current_tab = "Users"
      if params[:id]
         @user = User.find_by_id(params[:id]) or raise_not_found "User ID #{params[:id]}"
         @page_title = "Edit User ID: #{@user.id}"
         @user.updated_by = session[:user]
         unless @contact = @user.contact
            @contact = Contact.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "User Profile")
         end
         @contact.updated_by = session[:user]
         unless request.post?
            if @user.groups.empty?
               @user.groups.build
            end
         end
      else
         @user = User.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "User")
         @contact = Contact.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "User Profile")
         @page_title = "New User"
         @user.groups.build
      end

      if request.post?
         if params[:commit] == 'Cancel'
            if @user.new_record?
               redirect_to users_path
            else
               redirect_to @user
            end
            return
         end
         params[:user][:assigned_groups] ||= {}
         params[:contact][:existing_phone_attributes] ||= {}
         @user.attributes = params[:user]
         @contact.attributes = params[:contact]
         if @user.valid? && @contact.valid?
            @contact.save
            @user.contact_id = @contact.id
            @user.save
            redirect_to @user
            if params[:id]
               user_action("Users","Edited User #{@user.login}.")
            else
               user_action("Users", "Added User #{@user.login}.")
            end
         end
      end
   end
   
   # User Photo
   def photo
      unless (SiteConfig.allow_user_photos && session[:user] == params[:id].to_i) || has_permission(Perm[:update_user])
         raise_not_allowed "Edit User"
      end
      @help_page = ["Users","photo"]
      if SiteConfig.allow_user_photos
         @current_tab = "User Photo"
      else
         @current_tab = "Users"
      end
      params[:id] or raise_missing "User ID"
      @user = User.find_by_id(params[:id]) or raise_not_found "User ID #{params[:id]}"
      @page_title = "User Photo"
      @user.updated_by = session[:user]
      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to root_path
            return
         end
         @user.attributes = params[:user]
         if params[:commit] == 'Delete'
            @user.user_photo = nil
         end
         if @user.save
            redirect_to root_path
            if params[:commit] == 'Delete'
               user_action("Users","Removed User Photo for User #{@user.login}.")
            else
               user_action("Users","Updated User Photo for User #{@user.login}.")
            end
         end
      end
   end
   
   # shows user photos on one page
   def photo_album
      require_permission Perm[:view_user]
      @query = session[:user_filter] ||= HashWithIndifferentAccess.new
      cond = "user_photo_file_name != ''"
      unless @query.nil? || @query.empty?
         cond << " AND #{User.condition_string(@query)}"
      end
      @users = User.find(:all, :order => 'login', :conditions => cond)
      if @users.empty?
         flash[:warning] = "No photos available!"
         redirect_to users_path and return
      end
      @page_title = "Photo Album: Users"
   end
   
   # Destroys the specified user. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_user]
      require_method :delete
      params[:id] or raise_missing "User ID"
      @user = User.find_by_id(params[:id]) or raise_not_found "User ID #{params[:id]}"
      @user.destroy or raise_db(:destroy, "User ID #{params[:id]}")
      user_action("Users","Destroyed User #{@user.login}.")
      flash[:notice] = 'Record Deleted!'
      redirect_to users_path
   end

   # This displays the user preferences allowing the owner only to update
   # them.
   def edit_settings
      @help_page = ["Preferences","edit_settings"]
      @user = current_user
      @current_tab = "Preferences"
      @page_title = "Edit Preferences For: " + session[:user_login]
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to root_path and return
         end
         @user.attributes = params[:user]
         if @user.save
            session[:items_per_page] = @user.items_per_page
            session[:show_tips] = @user.show_tips
            redirect_to root_path
            user_action("Users","Edited Their User Preferences.")
         end
      end
   end
   
   # This action toggles the lock on user accounts.  Accounts that are locked
   # cannot be logged in to.
   def toggle
      require_permission Perm[:update_user]
      require_method :post
      params[:id] or raise_missing "User ID"
      @user = User.find_by_id(params[:id]) or raise_not_found "User ID #{params[:id]}"
      @user.toggle!(:locked)
      if @user.locked?
         user_action("Users","Locked User #{@user.login}.")
      else
         user_action("Users","Unlocked User #{@user.login}.")
      end
      redirect_to @user
   end
end