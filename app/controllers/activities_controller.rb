# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Activities are actions performed by system users.  All actions are logged
# in the Activity model and retained for 90 days. The only action pertaining
# to activities is the index action.
class ActivitiesController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Activities Listing
   def index
      require_permission Perm[:admin]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:activity_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:activity_filter] = nil
            redirect_to activities_path and return
         end
      else
         @query = session[:activity_filter] || HashWithIndifferentAccess.new
      end
      unless @activities = paged('activity', :order => 'activities.id DESC', :include => :user)
         @query = HashWithIndifferentAccess.new
         session[:activity_filter] = nil
         redirect_to activities_path and return
      end
   end

   protected

   # ensures that all actions will have the "Activity Log" menu item properly
   # highlighted as current.
   def set_tab_name
      @current_tab = "Activity Log"
   end
end
