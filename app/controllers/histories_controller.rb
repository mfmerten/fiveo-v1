# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# History records are derived from legacy data exported from old computer
# systems once used by the agency.  These are static records added through
# migrations (no add/edit capability) and are included to improve the
# upon the information available for people.
class HistoriesController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   before_filter :check_enabled

   # History Index
   def index
      require_permission Perm[:view_history]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:history_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:history_filter] = nil
            redirect_to histories_path and return
         end
      else
         @query = session[:history_filter] || HashWithIndifferentAccess.new
      end
      @help_page = ["History","index"]
      unless @histories = paged('history',:order => 'name', :include => :type)
         @query = HashWithIndifferentAccess.new
         session[:history_filter] = nil
         redirect_to histories_path and return
      end
   end

   # Show History
   def show
      require_permission Perm[:view_history]
      @help_page = ["History","show"]
      params[:id] or raise_missing "History ID"
      @history = History.find_by_id(params[:id]) or raise_not_found "History ID #{params[:id]}"
      @links = []
      if has_permission Perm[:destroy_history]
         @links.push(['Delete', @history, {:method => 'delete', :confirm => 'Are you sure you want to destroy this History?'}])
      end
   end
   
   # Add/Edit History
   # There is no action for adding or editing History records.  They are
   # imported by CSV file and exist as-is until they are destroyed.

   # Destroys the specified history. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_history]
      require_method :delete
      params[:id] or raise_missing "History ID"
      @history = History.find_by_id(params[:id]) or raise_not_found "History ID #{params[:id]}"
      @history.destroy or raise_db(:destroy, "History ID #{params[:id]}")
      user_action("History","Destroyed History ID: #{params[:id]}.")
      flash[:notice] = 'Record Deleted!'
      redirect_to histories_path
   end

   protected

   # checks to see whether the history section is enabled in Site Config
   def check_enabled
      unless SiteConfig.show_history
         raise PermissionDeniedError.new("History Section Is Disabled By Site Administrator")
      end
   end

   # ensures that the "History" menu item is highlighted for all actions in this
   # controller.
   def set_tab_name
      @current_tab = "History"
   end
end
