# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Events are displayed in the home page calendars. Events with details are
# displayed as a link to the event show view, those without details are
# displayed without a link.
class EventsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Event Listing
   def index
      require_permission Perm[:view_event]
      @help_page = ["Calendar Events","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:event_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:event_filter] = nil
            redirect_to events_path and return
         end
      else
         @query = session[:event_filter] || HashWithIndifferentAccess.new
      end
      unless @events = paged('event',:order => 'start_date',:include => [{:creator => :contact}])
         @query = HashWithIndifferentAccess.new
         session[:event_filter] = nil
         redirect_to events_path and return
      end
      @links = [["New", edit_event_path]]
      if has_permission Perm[:destroy_event]
         @links.push(["Purge", purge_events_path, {:method => 'delete', :confirm => 'Are you sure you want to purge (destroy) Events?'}])
      end
   end

   # Show Event
   def show
      # show does not require special permission to access
      # (other than being logged in) because this is required
      # to view event information when clicking through links in
      # the calendar
      @help_page = ["Calendar Events","show"]
      params[:id] or raise_missing "Event ID"
      @event = Event.find_by_id(params[:id]) or raise_not_found "Event ID #{params[:id]}"
      enforce_privacy(@event)
      @links = []
      if has_permission Perm[:update_event]
         @links.push(["New", edit_event_path])
         @links.push(["Edit", edit_event_path(:id => @event.id)])
      end
      if has_permission Perm[:destroy_event]
         @links.push(["Delete", @event, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Event?'}])
      end
      @page_links = [[@event.investigation,'invest'],[@event.owner,'Owner'],[@event.creator,'creator'],[@event.updater,'updater']]
   end

   # Add or Edit Event
   def edit
      require_permission Perm[:update_event]
      @help_page = ["Calendar Events","edit"]
      if params[:id]
         @event = Event.find_by_id(params[:id]) or raise_not_found "Event ID #{params[:id]}"
         enforce_privacy(@event)
         @page_title = "Edit Event ID: #{@event.id}"
         @event.updated_by = session[:user]
      else
         if params[:investigation_id]
            inv = Investigation.find_by_id(params[:investigation_id]) or raise_not_found "Investigation ID #{params[:investigation]}"
            return_path = inv
            id = inv.id
            priv = inv.private?
            owned_by = inv.owned_by
         else
            return_path = events_path
            id = nil
            priv = nil
            owned_by = nil
         end
         @event = Event.new(:investigation_id => id, :private => priv, :owned_by => owned_by, :created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Event")
         @page_title = "New Event"
      end

      if request.post?
         if params[:commit] == "Cancel"
            if @event.new_record?
               redirect_to return_path
            else
               redirect_to @event
            end
            return
         end
         @event.attributes = params[:event]
         if @event.save
            redirect_to @event
            if params[:id]
               user_action("Calendar Event","Edited Calendar Event ID: #{@event.id}.")
            else
               user_action("Calendar Event", "Added Calendar Event ID: #{@event.id}.")
            end
         end
      end
   end

   # Destroys the specified event. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_event]
      require_method :delete
      params[:id] or raise_missing "Event ID"
      @event = Event.find_by_id(params[:id]) or raise_not_found "Event ID #{params[:id]}"
      enforce_privacy(@event)
      if @event.investigation.nil?
         return_path = events_path
      else
         return_path = @event.investigation
      end
      @event.destroy or raise_db(:destroy, "Event ID #{params[:id]}")
      user_action("Calendar Event","Destroyed Calendar Event ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to return_path
   end

   # Destroys all expired events over 2 months old (those that will no longer
   def purge
      require_permission Perm[:destroy_event]
      require_method :delete
      user_action("Calendar Event", "Purged old calendar events!")
      Event.purge_expired
      redirect_to events_path
   end

   protected

   # Ensures that the "Events" menu item is highlighted for all
   # actions in this controller
   def set_tab_name
      @current_tab = "Events"
   end
end
