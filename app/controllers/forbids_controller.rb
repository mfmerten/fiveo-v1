# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Forbids are "Forbidden to Enter Or Remain on Premises" forms that are 
# filled out by a person to prevent another to enter onto the specified
# property. These are entered either from a call sheet or from an offense
# report.
class ForbidsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Forbid Listing
   def index
      require_permission Perm[:view_forbid]
      @help_page = ["Forbids","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:forbid_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:forbid_filter] = nil
            redirect_to forbids_path and return
         end
      else
         @query = session[:forbid_filter] || HashWithIndifferentAccess.new
      end
      unless @forbids = paged('forbid',:order => 'forbid_last, forbid_first, forbid_middle1, forbid_middle2, forbid_middle3',:include => :person, :alpha_name => 'forbids.forbid_last')
         @query = HashWithIndifferentAccess.new
         session[:forbid_filter] = nil
         redirect_to forbids_path and return
      end
      @links = []
      if has_permission Perm[:update_forbid]
         @links.push(['New', edit_forbid_path ])
      end
      if current_user.review_forbid?
         @links.push(['Review', url_for(:action => 'review')])
      end
   end

   # Show Forbid
   def show
      require_permission Perm[:view_forbid]
      @help_page = ["Forbids","show"]
      params[:id] or raise_missing "Forbid ID"
      @forbid = Forbid.find_by_id(params[:id]) or raise_not_found "Forbid ID #{params[:id]}"
      @page_title = "Forbid ID: #{@forbid.id}#{@forbid.legacy? ? ' -- [Legacy]' : ''}"
      @links = []
      if has_permission Perm[:update_forbid]
         @links.push(['Edit', edit_forbid_path(:id => @forbid.id)])
      end
      if has_permission Perm[:destroy_forbid]
         @links.push(['Delete', @forbid, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Forbid?'}])
      end
      @links.push(['PDF', url_for(:action => 'forbid_form', :forbid_id => @forbid.id)])
      @page_links = [@forbid.person,@forbid.call]
      @page_links.concat(@forbid.offenses)
      @page_links.push([@forbid.receiving_officer_id,'receiver'],[@forbid.serving_officer_id,'server'],[@forbid.creator,'creator'],[@forbid.updater,'updater'])
   end

   # Add or Edit Forbid
   def edit
      require_permission Perm[:update_forbid]
      @help_page = ["Forbids","edit"]
      if params[:id]
         @forbid = Forbid.find_by_id(params[:id]) or raise_not_found "Forbid ID #{params[:id]}"
         @page_title = "Edit Forbid ID: #{@forbid.id}"
         @forbid.updated_by = session[:user]
         @forbid.legacy = false
      else
         @forbid = Forbid.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Forbid")
         @forbid.request_date = Date.today
         if !current_user.contact.nil?
            @forbid.receiving_officer_id = current_user.contact.id
         else
            @forbid.receiving_officer = session[:user_name]
            @forbid.receiving_officer_unit = session[:user_unit]
            @forbid.receiving_officer_badge = session[:user_badge]
         end
         if params[:offense_id]
            if offense = Offense.find_by_id(params[:offense_id])
               @forbid.offenses << offense
               @forbid.call_id = offense.call_id
               if @forbid.call.nil?
                  @forbid.receiving_officer_id = offense.officer_id
                  @forbid.receiving_officer = offense.officer
                  @forbid.receiving_officer_unit = offense.officer_unit
                  @forbid.receiving_officer_badge = offense.officer_badge
                  @forbid.request_date = offense.offense_date
               else
                  unless @forbid.call.call_subjects.nil?
                     @forbid.call.call_subjects.each do |s|
                        if !s.subject_type.nil? && s.subject_type.long_name == 'Complainant'
                           @forbid.complainant = s.name
                           @forbid.comp_street = s.address1
                           @forbid.comp_city_state_zip = s.address2
                           @forbid.comp_phone = s.home_phone
                        end
                        if !s.subject_type.nil? && s.subject_type.long_name == 'Subject'
                           @forbid.name = s.name
                           parts = @forbid.parse_name(s.name)
                           @forbid.forbid_first = parts[:first]
                           if parts[:middle].blank?
                              @forbid.forbid_middle1, @forbid.forbid_middle2, @forbid.forbid_middle3 = []
                           else
                              @forbid.forbid_middle1, @forbid.forbid_middle2, @forbid.forbid_middle3 = parts[:middle].split
                           end
                           @forbid.forbid_last = parts[:last]
                           @forbid.forbid_suffix = parts[:suffix]
                           @forbid.forbid_street = s.address1
                           @forbid.forbid_city_state_zip = s.address2
                           @forbid.forbid_phone = s.home_phone
                        end
                     end
                  end
                  @forbid.receiving_officer_id = @forbid.call.dispatcher_id
                  @forbid.receiving_officer = @forbid.call.dispatcher
                  @forbid.receiving_officer_unit = @forbid.call.dispatcher_unit
                  @forbid.receiving_officer_badge = @forbid.call.dispatcher_badge
                  @forbid.request_date = @forbid.call.call_date
               end
               @forbid.case_no = offense.case_no
            end
         elsif params[:call_id]
            @forbid.call_id = params[:call_id]
            if @forbid.call.nil?
               if !current_user.contact.nil?
                  @forbid.receiving_officer_id = current_user.contact.id
               else
                  @forbid.receiving_officer = session[:user_name]
                  @forbid.receiving_officer_unit = session[:user_unit]
                  @forbid.receiving_officer_badge = session[:user_badge]
               end
               @forbid.request_date = Date.today
            else
               unless @forbid.call.call_subjects.nil?
                  @forbid.call.call_subjects.each do |s|
                     if !s.subject_type.nil? && s.subject_type.long_name == 'Complainant'
                        @forbid.complainant = s.name
                        @forbid.comp_street = s.address1
                        @forbid.comp_city_state_zip = s.address2
                        @forbid.comp_phone = s.home_phone
                     end
                     if !s.subject_type.nil? && s.subject_type.long_name == 'Subject'
                        @forbid.name = s.name
                        parts = @forbid.parse_name(s.name)
                        @forbid.forbid_first = parts[:first]
                        if parts[:middle].blank?
                           @forbid.forbid_middle1, @forbid.forbid_middle2, @forbid.forbid_middle3 = []
                        else
                           @forbid.forbid_middle1, @forbid.forbid_middle2, @forbid.forbid_middle3 = parts[:middle].split
                        end
                        @forbid.forbid_last = parts[:last]
                        @forbid.forbid_suffix = parts[:suffix]
                        @forbid.forbid_street = s.address1
                        @forbid.forbid_city_state_zip = s.address2
                        @forbid.forbid_phone = s.home_phone
                     end
                  end
               end
               @forbid.receiving_officer_id = @forbid.call.dispatcher_id
               @forbid.receiving_officer = @forbid.call.dispatcher
               @forbid.receiving_officer_unit = @forbid.call.dispatcher_unit
               @forbid.receiving_officer_badge = @forbid.call.dispatcher_badge
               @forbid.case_no = @forbid.call.case_no
               @forbid.request_date = @forbid.call.call_date
            end
         end
         if params[:person_id]
            @forbid.forbidden_id = params[:person_id]
            unless @forbid.person.nil?
               @forbid.name = @forbid.person.full_name
               @forbid.forbid_street = @forbid.person.physical_line1
               @forbid.forbid_city_state_zip = @forbid.person.physical_line2
               @forbid.forbid_phone = @forbid.person.home_phone
            end
         end
         @page_title = "New Forbid"
      end

      if request.post?
         if params[:commit] == "Cancel"
            if @forbid.new_record?
               redirect_to forbids_path
            else
               redirect_to @forbid
            end
            return
         end
         @forbid.attributes = params[:forbid]
         if @forbid.save
            redirect_to(@forbid)
            if params[:id]
               user_action("Forbid","Edited Forbid ID: #{@forbid.id}.")
            else
               user_action("Forbid", "Added Forbid ID: #{@forbid.id}.")
            end
         end
      end
   end

   # Destroys the specified forbid. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_forbid]
      require_method :delete
      params[:id] or raise_missing "Forbid ID"
      @forbid = Forbid.find_by_id(params[:id]) or raise_not_found "Forbid ID #{params[:id]}"
      @forbid.destroy or raise_db(:destroy, "Forbid ID #{params[:id]}")
      user_action("Forbid","Destroyed Forbid ID: #{params[:id]}.")
      flash[:notice] = 'Record Deleted!'
      redirect_to forbids_path
   end
   
   # Generates a PDF forbid form for the specified forbid
   def forbid_form
      require_permission Perm[:view_forbid]
      params[:forbid_id] or raise_missing "Forbid ID"
      @forbid = Forbid.find_by_id(params[:forbid_id]) or raise_not_found "Forbid ID #{params[:forbid_id]}"
      user_action("Forbid","Printed Forbid ID: #{@forbid.id}.")
      @page_title = "Entry After Being Forbidden Notice"
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "forbid-#{@forbid.id}.pdf"
      render :template => 'forbids/forbid_form.pdf.prawn', :layout => false
   end
   
   # accepts a person  id from the edit forbid form and validates a person,
   # updating all person identification fields from the person record.
   def update_person
      if params[:c] && has_permission(Perm[:view_person])
         if person = Person.find_by_id(params[:c])
            render :update do |page|
               page['forbid_name'].value = "#{person.full_name}"
               page['forbid_forbid_street'].value = "#{person.physical_line1}"
               page['forbid_forbid_city_state_zip'].value = "#{person.physical_line2}"
               page['forbid_forbid_phone'].value = "#{person.home_phone}"
               page['forbid_name'].select()
            end
         else
            render :update do |page|
               page['forbid_name'].value = "Invalid Person ID"
               page['forbid_forbidden_id'].select()
            end
         end
      else
         render :text => ""
      end
   end
   
   # Review latest forbids
   def review
      require_permission Perm[:view_forbid]
      @page_title = "Forbids Review"
      @forbids = review_paged('forbid', :include => :person)
      @links = [["View Next Page", url_for(:action => 'review')]]
      @links.push(["Skip All",url_for(:action => 'skip_all')])
   end

   # skip all forbid reviews
   def skip_all
      require_permission Perm[:view_forbid]
      review_skip_all('forbid')
      flash[:notice] = "All forbid reviews skipped!"
      redirect_to root_path
      return
   end
   
   protected
   
   # ensures that the "Forbids" menu item is highlighted for all actions in 
   # this controller
   def set_tab_name
      @current_tab = "Forbids"
   end
end
