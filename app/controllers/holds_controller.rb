# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Holds are reasons to keep custody of a prisoner. Holds are applied by
# arrests, or manually for various purposes.  When all holds have been
# released, the prisoner should be free to leave.
class HoldsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Hold Listing
   def index
      require_permission Perm[:view_booking]
      @help_page = ["Holds","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:hold_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:hold_filter] = nil
            redirect_to holds_path and return
         end
      else
         @query = session[:hold_filter] || HashWithIndifferentAccess.new
      end
      unless @holds = paged('hold', :order => 'hold_date DESC, hold_time DESC', :include => [:hold_type, :booking, :arrest, :blockers, :sentence, :doc_record, :temp_release_reason], :active => 'holds.cleared_date IS NULL', :skip_query => ['cleared_date_from','cleared_date_to'])
         @query = HashWithIndifferentAccess.new
         session[:hold_filter] = nil
         redirect_to holds_path and return
      end
      @links = []
      if session[:show_all_holds]
         @links.push(['Show Active', holds_path(:show_all => '0')])
      else
         @links.push(['Show All', holds_path(:show_all => '1')])
      end
   end

   # Show Hold
   def show
      require_permission Perm[:view_booking]
      @help_page = ["Holds","show"]
      params[:id] or raise_missing "Hold ID"
      @hold = Hold.find_by_id(params[:id]) or raise_not_found "Hold ID #{params[:id]}"
      @page_title = "Hold ID: #{@hold.id}#{@hold.legacy? ? ' -- [Legacy]' : ''}"
      ['served','goodtime','total','time_served'].each do |t|
         if @hold.send("hours_#{t}").blank? || @hold.send("hours_#{t}") < 0
            @hold.send("hours_#{t}=",0)
         end
      end
      @links = []
      if has_permission(Perm[:update_booking]) && (@hold.hold_type.nil? || @hold.hold_type.long_name != 'Temporary Release')
         @links.push(['Edit', edit_hold_path(:id => @hold.id)])
      end
      if has_permission(Perm[:destroy_booking]) && (@hold.hold_type.nil? || @hold.hold_type.long_name != 'Temporary Release' || (@hold.hold_type.long_name == 'Temporary Release' && @hold.cleared_date?))
         @links.push(['Delete', @hold, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Hold?'}])
      end
      unless @hold.booking.release_date?
         if has_permission(Perm[:update_booking]) && (@hold.hold_type.nil? || @hold.hold_type.long_name != 'Temporary Release') && @hold.cleared_date? && !@hold.booking.nil? && !@hold.booking.release_date?
            @links.push(['Reactivate', url_for(:action => 'clear_release', :id => @hold.id), {:method => 'post'}])
         end
         if has_permission Perm[:update_booking]
            unless @hold.cleared_date? || @hold.hold_type.nil?
               if @hold.hold_type.long_name == '72 Hour'
                  @links.push(['Clear', seventytwo_path(:id => @hold.arrest_id, :booking_id => @hold.booking_id)])
               elsif @hold.hold_type.long_name == 'Bondable'
                  @links.push(['Clear', edit_bond_path(:arrest_id => @hold.arrest_id, :hold_id => @hold.id)])
                  @links.push(['Clear WO Bond', clear_hold_path(:id => @hold.id, :cause => 'Manual Release')])
               elsif @hold.hold_type.long_name == 'Temporary Transfer' || @hold.hold_type.long_name == 'Temporary Housing'
                  if @hold.hold_type.long_name == 'Temporary Transfer' && has_permission(Perm[:update_billing])
                     @links.push([(@hold.other_billable? ? 'Not Billable' : 'Billable'), url_for(:action => 'toggle_bill', :id => @hold.id)])
                  end
                  @links.push(['Clear', url_for(:controller => 'transfers', :action => 'transfer', :id => @hold.booking_id)])
               elsif @hold.hold_type.long_name == 'Hold for City' || @hold.hold_type.long_name == 'Hold for Other Agency'
                  @links.push(['Clear', clear_hold_path(:id => @hold.id, :cause => 'Manual Release')])
                  @links.push(['Add Bond', edit_bond_path(:arrest_id => @hold.arrest_id, :hold_id => @hold.id)])
               elsif @hold.hold_type.long_name == 'Temporary Release'
                  @links.push(['Return', url_for(:controller => 'bookings', :action => 'temp_release_in', :hold_id => @hold.id)])
               else
                  @links.push(['Clear', clear_hold_path(:id => @hold.id)])
               end
            end
            if !@hold.hold_type.nil? && (@hold.hold_type.long_name == 'Serving Sentence' || @hold.hold_type.long_name == 'Fines/Costs')
               @links.push(['Adjustment', url_for(:action => 'adjustment', :id => @hold.id)])
            end
         end
      end
      @page_links = []
      unless @hold.booking.nil?
         @page_links.push(@hold.booking.person)
      end
      @page_links.push(@hold.arrest, @hold.booking, @hold.transfer, @hold.doc_record, @hold.sentence, @hold.revocation, @hold.probation,[@hold.creator,'creator'],[@hold.updater,'updater'])
   end
   
   # Clear Hold
   def clear
      require_permission Perm[:update_booking]
      @help_page = ["Holds","clear"]
      params[:id] or raise_missing "Hold ID"
      @hold = Hold.find_by_id(params[:id]) or raise_not_found "Hold ID #{params[:id]}"
      if @hold.booking.nil?
         flash[:warning] = "Hold has invalid booking."
         redirect_to @hold and return
      end
      @booking = @hold.booking
      @page_title = "Clear Hold ID: #{@hold.id}"
      @hold.updated_by = session[:user]
      @hold.legacy = false
      if params[:cause]
         @hold.cleared_because_id = Option.lookup("Release Type",params[:cause])
      end
      @hold.cleared_date, @hold.cleared_time = get_date_and_time
      if !current_user.contact.nil?
         @hold.cleared_by_id = current_user.contact.id
      else
         @hold.cleared_by_badge = session[:user_badge]
         @hold.cleared_by_unit = session[:user_unit]
         @hold.cleared_by = session[:user_name]
      end
      @page_links = [@hold.booking.person, @hold.arrest, @hold.booking, @hold.transfer, @hold.doc_record, @hold.sentence, @hold.revocation, @hold.probation]
      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to @booking and return
         end
         @hold.attributes = params[:hold]
         if @hold.save
            redirect_to @booking
            user_action("Holds","Cleared Hold ID: #{@hold.id}.")
         end
      end
   end

   # Add or Edit Hold
   def edit
      require_permission Perm[:update_booking]
      @help_page = ["Holds","edit"]
      if params[:id]
         @hold = Hold.find_by_id(params[:id]) or raise_not_found "Hold ID #{params[:id]}"
         if @hold.booking.nil?
            flash[:warning] = "Hold has invalid booking."
            redirect_to @hold and return
         end
         @booking = @hold.booking
         @page_title = "Edit Hold ID: #{@hold.id}"
         @hold.updated_by = session[:user]
         @hold.legacy = false
      else
         params[:booking_id] or raise_missing "Booking ID"
         @booking = Booking.find_by_id(params[:booking_id]) or raise_not_found "Booking ID #{params[:booking_id]}"
         @hold = Hold.new(:created_by => session[:user],:updated_by => session[:user],:booking_id => @booking.id) or raise_db(:new, "Hold")
         if @arrest = Arrest.find_by_id(params[:arrest_id])
            @hold.arrest_id = @arrest.id
            @hold.hold_date = @arrest.arrest_date
            @hold.hold_time = @arrest.arrest_time
            @hold.hold_by_badge = @arrest.officer1_badge
            @hold.hold_by_unit = @arrest.officer1_unit
            @hold.hold_by = @arrest.officer1
            @hold.hold_by_id = @arrest.officer1_id
         else
            @hold.hold_date, @hold.hold_time = get_date_and_time
            if !current_user.contact.nil?
               @hold.hold_by_id = current_user.contact.id
            else
               @hold.hold_by_badge = session[:user_badge]
               @hold.hold_by_unit = session[:user_unit]
               @hold.hold_by = session[:user_name]
            end
         end
         @page_title = "New Hold"
      end
      @page_links = []
      unless @hold.booking.nil?
         @page_links.push(@hold.booking.person)
      end
      @page_links.concat([@hold.arrest, @hold.booking, @hold.transfer, @hold.doc_record, @hold.sentence, @hold.revocation, @hold.probation])
      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to @booking and return
         end
         @hold.attributes = params[:hold]
         if @hold.save
            redirect_to @booking
            if params[:id]
               user_action("Holds","Edited Hold ID: #{@hold.id}.")
            else
               user_action("Holds", "Added Hold ID: #{@hold.id}.")
            end
         end
      end
   end

   # Destroys the specified hold. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_booking]
      require_method :delete
      params[:id] or raise_missing "Hold ID"
      @hold = Hold.find_by_id(params[:id]) or raise_not_found "Hold ID #{params[:id]}"
      if !@hold.hold_type.nil? && @hold.hold_type.long_name == 'Temporary Release' && !@hold.cleared_date?
         raise_not_allowed("destroy this hold: Temporary Release has not been cleared.")
      end
      booking = @hold.booking
      @hold.destroy or raise_db(:destroy, "Hold ID #{params[:id]}")
      user_action("Holds","Destroyed Hold ID: #{params[:id]}.")
      flash[:notice] = 'Record Deleted!'
      if booking.nil?
         redirect_to holds_path
      else
         redirect_to booking
      end
   end
   
   def toggle_bill
      require_permission Perm[:update_billing]
      require_method :post
      params[:id] or raise_missing "Hold ID"
      @hold = Hold.find_by_id(params[:id]) or raise_not_found "Hold ID #{params[:id]}"
      @hold.toggle!(:other_billable)
      if @hold.other_billable?
         user_action("Holds","Flagged Hold ID: #{@hold.id} as billable!")
      else
         user_action("Holds","Flagged Hold ID: #{@hold.id} as Not Billable!")
      end
      if @hold.booking.nil?
         redirect_to @hold
      else
         redirect_to @hold.booking
      end
   end
   
   def clear_release
      require_permission Perm[:update_booking]
      require_method :post
      params[:id] or raise_missing "Hold ID"
      @hold = Hold.find_by_id(params[:id]) or raise_not_found "Hold ID #{params[:id]}" 
      if @hold.booking.nil?
         flash[:warning] = "Hold has an invalid booking"
         redirect_to @hold and return
      end
      unless @hold.cleared_date?
         flash[:warning] = 'This hold already active!'
         redirect_to @hold and return
      end
      if @hold.booking.release_date?
         flash[:warning] = 'Booking must be open before reactivating a hold!'
         redirect_to @hold and return
      end
      if @hold.clear_release(session[:user])
         flash[:notice] = 'Hold Reactivated!'
         redirect_to @hold.booking and return
      else
         flash[:warning] = 'Hold reactivation failed!'
         redirect_to @hold and return
      end
   end

   # adjustment
   def adjustment
      require_permission Perm[:update_booking]
      @help_page = ["Holds","adjustment"]
      params[:id] or raise_missing "Hold ID"
      @hold = Hold.find_by_id(params[:id]) or raise_not_found "Hold ID #{params[:id]}"
      if @hold.hold_type.nil?
         flash[:warning] = "Hold has invalid Hold Type"
         redirect_to @hold and return
      end
      if @hold.hold_type.long_name != 'Serving Sentence' && @hold.hold_type.long_name != 'Fines/Costs'
         flash[:warning] = "Only Serving Sentence or Fines/Costs Holds may be adjusted!"
         redirect_to @hold and return
      end
      @hold_adjustment = HoldAdjustment.new
      @page_title = "Adjusting Time Counters for Hold ID #{@hold.id}"
      @page_links = []
      unless @hold.booking.nil?
         @page_links.push(@hold.booking.person)
      end
      @page_links.concat([@hold, @hold.arrest, @hold.booking, @hold.transfer, @hold.doc_record, @hold.sentence, @hold.revocation])
   
      # force nil or negative hours to 0
      ['served','goodtime','total','time_served'].each do |t|
         if @hold.send("hours_#{t}").blank? || @hold.send("hours_#{t}") < 0
            @hold.send("hours_#{t}=",0)
         end
      end
   
      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to @hold.booking and return
         end
         @hold_adjustment.update_attributes(params[:hold_adjustment])
         if @hold_adjustment.valid?
            @hold.hours_total += @hold_adjustment.hours_total_adjustment
            @hold.hours_served += @hold_adjustment.hours_served_adjustment
            @hold.hours_goodtime += @hold_adjustment.hours_goodtime_adjustment
            @hold.hours_time_served += @hold_adjustment.hours_time_served_adjustment
            if params[:commit] == 'Preview'
               render :action => 'adjustment'
            else
               if @hold.save
                  redirect_to @hold
                  user_action("Holds","Adjusted time balances for Sentence Hold ID: #{@hold.id}.")
               end
            end
         end
      end
   end
   
   protected
   
   # ensures that the "Holds" menu item is highlighted for all actions in this
   # controller.
   def set_tab_name
      @current_tab = "Holds"
   end
end
