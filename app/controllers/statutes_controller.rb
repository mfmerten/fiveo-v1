# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Statutes are a collection of State and Local statutes (laws) used as
# charges on arrests, warrants and dockets.
class StatutesController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Statute Listing
   def index
      require_permission Perm[:view_statute]
      @help_page = ["Statutes","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:statute_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:statute_filter] = nil
            redirect_to statutes_path and return
         end
      else
         @query = session[:statute_filter] || HashWithIndifferentAccess.new
      end
      unless @statutes = paged('statute', :order => 'name')
         @query = HashWithIndifferentAccess.new
         session[:statute_filter] = nil
         redirect_to statutes_path and return
      end
      if has_permission(Perm[:update_statute])
         @links = [['New', edit_statute_path]]
      end
   end

   # Show Statute
   def show
      require_permission Perm[:view_statute]
      @help_page = ["Statutes","show"]
      params[:id] or raise_missing "Statute ID"
      @statute = Statute.find_by_id(params[:id]) or raise_not_found "Statute ID #{params[:id]}"
      @links = []
      if has_permission(Perm[:update_statute])
         @links.push(['New', edit_statute_path])
         @links.push(['Edit', edit_statute_path(:id => @statute.id)])
      end
      if has_permission Perm[:destroy_statute]
         @links.push(['Delete', @statute, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Statute?'}])
      end
      @page_links = [[@statute.creator,'creator'],[@statute.updater,'updater']]
   end

   # Add or Edit Statute
   def edit
      require_permission Perm[:update_statute]
      @help_page = ["Statutes","edit"]
      if params[:id]
         @statute = Statute.find_by_id(params[:id]) or raise_not_found "Statute ID #{params[:id]}"
         @page_title = "Edit Statute ID: #{@statute.id}"
         @statute.updated_by = session[:user]
      else
         @statute = Statute.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Statute")
         @page_title = "New Statute"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @statute.new_record?
               redirect_to statutes_path
            else
               redirect_to @statute
            end
            return
         end
         @statute.attributes = params[:statute]
         if @statute.save
            redirect_to @statute
            if params[:id]
               user_action("Statute","Edited Statute ID: #{@statute.id}.")
            else
               user_action("Statute", "Added Statute ID: #{@statute.id}.")
            end
         end
      end
   end

   # Destroys the specified statute. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_statute]
      require_method :delete
      params[:id] or raise_missing "Statute ID"
      @statute = Statute.find_by_id(params[:id]) or raise_not_found "Statute ID #{params[:id]}"
      @statute.destroy or raise_db(:destroy, "Statute ID #{params[:id]}")
      user_action("Statute","Destroyed Statute ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to(statutes_url)
   end
   
   # Accepts a text partial via AJAX call and updates the specified field
   # with the full statute name if a match is found.
   def validate_charge
      if (field = params[:update]) && !params[:u].strip.empty?
         if statute = Statute.find_first_by_name_partial(params[:u].upcase)
            render :update do |page|
               page[field].value = statute.name
               page[field].select()
            end
         else
            render :text => ""
         end
      else
         render :text => ""
      end
   end

   protected

   # ensures "Statutes" is highlighted in the menu for all actions in this
   # controller.
   def set_tab_name
      @current_tab = "Statutes"
   end

end