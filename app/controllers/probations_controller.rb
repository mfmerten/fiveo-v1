# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# This controller deals with probation records.
class ProbationsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Probationer Listing
   def index
      require_permission Perm[:view_probation]
      @help_page = ["Probation","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:probation_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:probation_filter] = nil
            redirect_to probations_path and return
         end
      else
         @query = session[:probation_filter] || HashWithIndifferentAccess.new
      end
      if session[:show_all_probations].nil?
         session[:show_all_probations] = false
      end
      if params[:show_all]
         session[:show_all_probations] = (params[:show_all] == "1")
      end
      active_id = Option.lookup("Probation Status","Active",nil,nil,true)
      pending_id = Option.lookup("Probation Status","Pending",nil,nil,true)
      if session[:show_all_probations] == false
         cond = "(probations.status_id = #{active_id} OR probations.status_id = #{pending_id})"
      else
         cond = ''
      end
      if @query.nil? || @query.empty?
         @page_title = "Probationers List"
      else
         if session[:show_all_probations] == false
            @query.delete('status_id')
         end
         # if @query, but conditions are empty, return nil to cause index to clear filter
         unless cond_string = Probation.condition_string(@query)
            @query = HashWithIndifferentAccess.new
            session[:probation_filter] = nil
            redirect_to probations_path and return
         end
         unless cond.blank?
            cond << " AND "
         end
         cond << cond_string
         @page_title = "Probationers List (FILTERED)"
      end
      if session[:show_all_probations] == false
         @page_title << " (Active)"
      else
         @page_title << " (All)"
      end
      probations = Probation.find(:all, :include => [:person, :status, :probation_payments], :conditions => cond)
      @people = paged_array(probations.collect{|pr| pr.person}.compact.uniq.sort{|a,b| a.full_name <=> b.full_name})
      @links = []
      if session[:show_all_probations]
         @links.push(['Show Active', probations_path(:show_all => '0')])
      else
         @links.push(['Show All', probations_path(:show_all => '1')])
      end
      if has_permission(Perm[:update_probation])
         @links.push(['New', edit_probation_path])
      end
   end
   
   # Show Probation
   def show
      require_permission Perm[:view_probation]
      @help_page = ["Probation","show"]
      params[:id] or raise_missing "Probation ID"
      @probation = Probation.find_by_id(params[:id]) or raise_not_found "Probation ID #{params[:id]}"
      @pending = false
      if @probation.docket.nil?
         if Docket.find(:first, :conditions => {:docket_no => @probation.docket_no, :person_id => @probation.person_id})
            @pending = true
         end
      end
      @links = [["Probationer", url_for(:action => 'show_probationer', :person_id => @probation.person_id)]]
      if has_permission(Perm[:update_probation])
         @links.push(["New", edit_probation_path])
         @links.push(["Edit", edit_probation_path(:id => @probation.id)])
      end
      if has_permission Perm[:destroy_probation]
         @links.push(['Delete', @probation, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Probation?'}])
      end
      if has_permission Perm[:update_probation]
         @links.push(['Transaction', edit_probation_payment_path(:probation_id => @probation.id)])
      end
      @page_links = [@probation.person, @probation.docket, @probation.court, @probation.sentence, @probation.hold,[@probation.probation_officer_id,'Officer'],[@probation.creator,'creator'],[@probation.updater, 'updater']]
   end
   
   # Show Probationer
   def show_probationer
      require_permission Perm[:view_probation]
      @help_page = ["Probation","show_probationer"]
      params[:person_id] or raise_missing "Probationer Person ID"
      @person = Person.find_by_id(params[:person_id]) or raise_not_found "Probationer Person ID #{params[:person_id]}"
      @page_title = "Show Probationer: [#{@person.id}] #{@person.full_name}"
      @payments = ProbationPayment.find(:all, :include => [{:probation => :person}], :order => 'probation_payments.transaction_date DESC, probation_payments.id DESC', :conditions => ['people.id = ?',@person.id])
      @links = [["Back",probations_path]]
      @page_links = [@person]
   end
   
   # show payment
   def show_payment
      require_permission Perm[:view_probation]
      @help_page = ["Probation","show_payment"]
      params[:id] or raise_missing "Probation Transaction ID"
      @probation_payment = ProbationPayment.find_by_id(params[:id]) or raise_not_found "Probation Transaction ID #{params[:id]}"
      @page_title = "Probation Transaction ID #{@probation_payment.id}"
      @links = []
      if has_permission(Perm[:update_probation]) && !@probation_payment.report_datetime?
         @links.push(["Edit", edit_probation_payment_path(:id => @probation_payment.id)])
         @links.push(["Delete", @probation_payment, {:method => 'delete', :confirm => 'Are you sure you want to destroy this transaction?'}])
      end
      @links.push(["Receipt", url_for(:action => 'receipt', :id => @probation_payment.id)])
      @page_links = [@probation_payment.probation, @probation_payment.probation.person]
   end

   # Add or Edit Probation
   def edit
      require_permission Perm[:update_probation]
      @help_page = ["Probation","edit"]
      if params[:id]
         @probation = Probation.find_by_id(params[:id]) or raise_not_found "Probation ID #{params[:id]}"
         @page_title = "Edit Probation ID: #{@probation.id}"
         @probation.updated_by = session[:user]
         @charge = @probation.da_charge
      else
         @page_title = "New Probation"
         @probation = Probation.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Probation")
         if !current_user.contact.nil?
            @probation.probation_officer_id = current_user.contact.id
         else
            @probation.probation_officer = session[:user_name]
            @probation.probation_officer_unit = session[:user_unit]
            @probation.probation_officer_badge = session[:user_badge]
         end
         @probation.status_id = Option.lookup("Probation Status","Active")
         @probation.start_date = Date.today
         @charge = nil
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @probation.new_record?
               redirect_to probations_path
            else
               redirect_to @probation
            end
            return
         end
         @probation.attributes = params[:probation]
         if @probation.save
            redirect_to @probation
            if params[:id]
               user_action("Probation","Edited Probation ID: #{@probation.id}.")
            else
               user_action("Probation", "Added Probation ID: #{@probation.id}.")
            end
         end
      end
   end

   # Destroys the specified probation record. Only responds to DELETE
   # methods.
   def destroy
      require_permission Perm[:destroy_probation]
      require_method :delete
      params[:id] or raise_missing "Probation ID"
      @probation = Probation.find_by_id(params[:id]) or raise_not_found "Probation ID #{params[:id]}"
      @probation.destroy or raise_db(:destroy, "Probation ID #{params[:id]}")
      user_action("Probation", "Destroyed Probation ID: #{params[:id]}")
      flash[:notice] = "Record Deleted!"
      redirect_to probations_path
   end
   
   # Destroys the specified probation payment record. Only responds to DELETE
   # methods.
   def destroy_payment
      require_permission Perm[:update_probation]
      require_method :delete
      params[:id] or raise_missing "Probation Transaction ID"
      @probation_payment = ProbationPayment.find_by_id(params[:id]) or raise_not_found "Probation Transaction ID #{params[:id]}"
      return_path = url_for(@probation_payment.probation)
      !@probation_payment.report_datetime? or raise_not_allowed("delete posted transactions")
      @probation_payment.destroy or raise_db(:destroy, "Probation Transaction ID #{params[:id]}")
      user_action("Probation", "Destroyed Probation Transaction ID: #{params[:id]}")
      flash[:notice] = "Record Deleted!"
      redirect_to return_path
   end
   
   # Add new probation payment.
   def edit_payment
      require_permission Perm[:update_probation]
      @help_page = ["Probation","edit_payment"]
      if params[:id]
         @probation_payment = ProbationPayment.find_by_id(params[:id]) or raise_not_found "Probation Transaction ID #{params[:id]}"
         if @probation_payment.probation.nil?
            flash[:warning] = "Probation Link Is Invalid!  Cannot Edit This Transaction!"
            redirect_to @probation_payment and return
         else
            @probation = @probation_payment.probation
         end
         @probation_payment.updated_by = session[:user]
         @page_title = "Edit Transaction ID: #{params[:id]}"
      else
         params[:probation_id] or raise_missing "Probation ID"
         @probation = Probation.find_by_id(params[:probation_id]) or raise_not_found "Probation ID #{params[:probation_id]}"
         @probation_payment = ProbationPayment.new(
            :created_by => session[:user],
            :updated_by => session[:user],
            :transaction_date => Date.today,
            :probation_id => @probation.id
         ) or raise_db(:new, "Probation Transaction")
         if current_user.contact.nil?
            @probation_payment.officer = session[:user_name]
            @probation_payment.officer_unit = session[:user_unit]
            @probation_payment.officer_badge = session[:user_badge]
         else
            @probation_payment.officer_id = current_user.contact_id
         end
         # set names from probation
         (1..9).each do |x|
            @probation_payment.send("fund#{x}_name=",@probation.send("fund#{x}_name"))
         end
         @page_title = "New Transaction"
      end
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to @probation and return
         end
         @probation_payment.attributes = params[:probation_payment]
         if @probation_payment.save
            redirect_to @probation
            user_action("Probation", "Added Transaction ID: #{@probation_payment.id}.")
         end
      end
   end

   # This generates a PDF balance report
   def probation_balance
      require_permission Perm[:view_probation]
      @probations = Probation.find(:all, :include => :person, :order => 'people.lastname, people.firstname, probations.id').reject{|p| p.total_balance == 0}
      @report_date = DateTime.now
      @timestamp = format_date(@report_date, :timestamp => true)
      if @probations.empty?
         flash[:warning] = 'Nothing to report!'
         redirect_to probations_path and return
      end
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Probation_Balance_Due-#{@timestamp}.pdf"
      render :template => 'probations/probation_balance.pdf.prawn', :layout => false
      user_action("Probation", "Printed Balance Due Report.")
   end

   # this generates an unposted transaction report
   def posting_report
      require_permission Perm[:view_probation]
      @status = params[:status]
      if @status == 'post'
         require_method :post
         require_permission Perm[:update_probation]
      end
      if @status == 'reprint'
         params[:post_datetime] or raise_missing "Posting Date"
         report_datetime = valid_datetime(params[:post_datetime]) or raise_invalid "Posting Date", "'#{params[:post_datetime]} is not a valid date."
         @payments = ProbationPayment.all_unposted(report_datetime)
         @report_time = report_datetime
      else
         @payments = ProbationPayment.all_unposted
         @report_time = DateTime.now
      end
      if @status == 'preview'
         @page_title = "Probation Posting Report (PREVIEW)"
      else
         @page_title = "Probation Posting Report"
      end
      @timestamp = format_date(@report_time, :timestamp => true)
      if @payments.empty?
         flash[:warning] = "Nothing To Report"
         redirect_to probations_path and return
      end
      @funds = @payments.collect{|p| [p.fund1_name, p.fund2_name, p.fund3_name, p.fund4_name, p.fund5_name, p.fund6_name, p.fund7_name, p.fund8_name, p.fund9_name]}.flatten.compact.uniq.reject{|f| f.blank?}
      if @status == 'post'
         # post the transactions
         @payments.each{|p| p.update_attribute(:report_datetime, @report_time)}
      end
      if @status == 'post' || @status == 'reprint'
         prawnto :prawn => {
            :page_size => 'LETTER',
            :page_layout => :landscape,
            :top_margin => 10,
            :bottom_margin => 10,
            :left_margin => 20,
            :right_margin => 20},
            :filename => "Unposted_Transactions-#{@timestamp}.pdf"
         user_action("Probation", "Printed Unposted Transactions Report (#{@status == 'post' ? 'Posted Transactions' : 'Reprint for ' + format_date(@report_time)}).")
      else
         prawnto :prawn => {
            :page_size => 'LETTER',
            :page_layout => :landscape,
            :top_margin => 10,
            :bottom_margin => 10,
            :left_margin => 20,
            :right_margin => 20},
            :filename => "Unposted_Transactions_Preview-#{@timestamp}.pdf"
         user_action("Probation", "Printed Unposted Transactions Preview Report.")
      end
      render :template => 'probations/posting_report.pdf.prawn', :layout => false
   end
   
   def transaction_history
      require_permission Perm[:view_probation]
      @start_date = valid_date(params[:start_date])
      @stop_date = valid_date(params[:end_date])
      if !@start_date.nil? && !@stop_date.nil? && @start_date > @stop_date
         # wrong order, swap
         @start_date, @stop_date = @stop_date, @start_date
      end
      if params[:probation_id] && !params[:probation_id].blank?
         @probation = Probation.find_by_id(params[:probation_id]) or raise_not_found "Probation ID #{params[:probation_id]}"
         @person = nil
         @probation_payments = ProbationPayment.locate_transactions_by_probation(@probation.id, @start_date, @stop_date)
      elsif params[:person_id] && !params[:person_id].blank?
         @person = Person.find_by_id(params[:person_id]) or raise_not_found "Person ID #{params[:person_id]}"
         @probation = nil
         @probation_payments = ProbationPayment.locate_transactions_by_person(@person.id, @start_date, @stop_date)
      else
         @person = nil
         @probation = nil
         @probation_payments = ProbationPayment.locate_transactions_by_date(@start_date, @stop_date)
      end
      @report_datetime = DateTime.now
      @timestamp = format_date(@report_datetime, :timestamp => true)
      if @probation_payments.empty?
         flash[:warning] = 'Nothing to report!'
         redirect_to probations_path and return
      end
      if !@probation.nil?
         # specified by probation
         @page_title = "Probation Transaction History (Probation ID #{@probation.id})"
         prawnto :prawn => {
               :page_size => 'LETTER',
               :top_margin => 10,
               :bottom_margin => 10,
               :left_margin => 20,
               :right_margin => 20
            },
            :filename => "Transaction_History_Probation_ID_#{@probation.id}-#{@timestamp}.pdf"
         user_action("Probation", "Printed Transaction History for Probation ID: #{@probation.id}")
      elsif !@person.nil?
         # specified by person
         @page_title = "Probation Transaction History (Person ID #{@person.id})"
         prawnto :prawn => {
               :page_size => 'LETTER',
               :top_margin => 10,
               :bottom_margin => 10,
               :left_margin => 20,
               :right_margin => 20
            },
            :filename => "Transaction_History_Person_ID_#{@person.id}-#{@timestamp}.pdf"
         user_action("Probation", "Printed Transaction History for Person ID: #{@person.id}")
      else
         # neither specified
         @page_title = "Probation Transaction History"
         prawnto :prawn => {
               :page_size => 'LETTER',
               :top_margin => 10,
               :bottom_margin => 10,
               :left_margin => 20,
               :right_margin => 20
            },
            :filename => "Transaction_History-#{@timestamp}.pdf"
         user_action("Probation", "Printed Transaction History")
      end
      render :template => 'probations/transaction_history.pdf.prawn', :layout => false
   end
   
   # This generates a PDF Active Probation Listing
   def active_list
      require_permission Perm[:view_probation]
      @probations = Probation.all_active
      @report_datetime = DateTime.now
      @timestamp = format_date(@report_datetime, :timestamp => true)
      if @probations.empty?
         flash[:warning] = 'Nothing to report!'
         redirect_to probations_path and return
      end
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Active_Probation_List-#{@timestamp}.pdf"
      render :template => 'probations/active_list.pdf.prawn', :layout => false
      user_action("Probation", "Printed Active Probation Listing.")
   end
   
   def disable_sentence_info
      if params[:c]
         if params[:c].blank?
            render :update do |page|
               page['probation_trial_result'].disabled = false
               page['probation_conviction_count'].disabled = false
               page['probation_conviction'].disabled = false
               page['probation_doc_false'].disabled = false
               page['probation_doc_true'].disabled = false
               page['probation_fines'].disabled = false
               page['probation_costs'].disabled = false
               page['probation_pay_by_date'].disabled = false
               page['probation_default_hours'].disabled = false
               page['probation_default_days'].disabled = false
               page['probation_default_months'].disabled = false
               page['probation_default_years'].disabled = false
               page['probation_sentence_hours'].disabled = false
               page['probation_sentence_days'].disabled = false
               page['probation_sentence_months'].disabled = false
               page['probation_sentence_years'].disabled = false
               page['probation_parish_jail'].disabled = false
               page['probation_sentence_suspended'].disabled = false
               page['probation_suspended_except_hours'].disabled = false
               page['probation_suspended_except_days'].disabled = false
               page['probation_suspended_except_months'].disabled = false
               page['probation_suspended_except_years'].disabled = false
               page['probation_probation_hours'].disabled = false
               page['probation_probation_days'].disabled = false
               page['probation_probation_months'].disabled = false
               page['probation_probation_years'].disabled = false
               page['probation_reverts_to_unsup'].disabled = false
               page['probation_reverts_conditions'].disabled = false
               page['probation_credit_served'].disabled = false
               page['probation_service_days'].disabled = false
               page['probation_service_hours'].disabled = false
               page['probation_service_served'].disabled = false
               page['probation_sap'].disabled = false
               page['probation_sap_complete'].disabled = false
               page['probation_driver'].disabled = false
               page['probation_driver_complete'].disabled = false
               page['probation_probation_fees'].disabled = false
               page['probation_anger'].disabled = false
               page['probation_anger_complete'].disabled = false
               page['probation_sat'].disabled = false
               page['probation_sat_complete'].disabled = false
               page['probation_random_screens'].disabled = false
               page['probation_no_victim_contact'].disabled = false
               page['probation_restitution_amount'].disabled = false
               page['probation_restitution_date'].disabled = false
               page['probation_dare_amount'].disabled = false
               page['probation_idf_amount'].disabled = false
               page['probation_art893'].disabled = false
               page['probation_art894'].disabled = false
               page['probation_art895'].disabled = false
               page['probation_sentence_notes'].disabled = false
               page['probation_notes'].disabled = false
               page['probation_remarks'].disabled = false
               page['probation_hold_if_arrested'].focus
            end
         else
            render :update do |page|
               page['probation_trial_result'].disabled = true
               page['probation_conviction_count'].disabled = true
               page['probation_conviction'].disabled = true
               page['probation_doc_false'].disabled = true
               page['probation_doc_true'].disabled = true
               page['probation_fines'].disabled = true
               page['probation_costs'].disabled = true
               page['probation_pay_by_date'].disabled = true
               page['probation_default_hours'].disabled = true
               page['probation_default_days'].disabled = true
               page['probation_default_months'].disabled = true
               page['probation_default_years'].disabled = true
               page['probation_sentence_hours'].disabled = true
               page['probation_sentence_days'].disabled = true
               page['probation_sentence_months'].disabled = true
               page['probation_sentence_years'].disabled = true
               page['probation_parish_jail'].disabled = true
               page['probation_sentence_suspended'].disabled = true
               page['probation_suspended_except_hours'].disabled = true
               page['probation_suspended_except_days'].disabled = true
               page['probation_suspended_except_months'].disabled = true
               page['probation_suspended_except_years'].disabled = true
               page['probation_probation_hours'].disabled = true
               page['probation_probation_days'].disabled = true
               page['probation_probation_months'].disabled = true
               page['probation_probation_years'].disabled = true
               page['probation_reverts_to_unsup'].disabled = true
               page['probation_reverts_conditions'].disabled = true
               page['probation_credit_served'].disabled = true
               page['probation_service_days'].disabled = true
               page['probation_service_hours'].disabled = true
               page['probation_service_served'].disabled = true
               page['probation_sap'].disabled = true
               page['probation_sap_complete'].disabled = true
               page['probation_driver'].disabled = true
               page['probation_driver_complete'].disabled = true
               page['probation_probation_fees'].disabled = true
               page['probation_anger'].disabled = true
               page['probation_anger_complete'].disabled = true
               page['probation_sat'].disabled = true
               page['probation_sat_complete'].disabled = true
               page['probation_random_screens'].disabled = true
               page['probation_no_victim_contact'].disabled = true
               page['probation_restitution_amount'].disabled = true
               page['probation_restitution_date'].disabled = true
               page['probation_dare_amount'].disabled = true
               page['probation_idf_amount'].disabled = true
               page['probation_art893'].disabled = true
               page['probation_art894'].disabled = true
               page['probation_art895'].disabled = true
               page['probation_sentence_notes'].disabled = true
               page['probation_notes'].disabled = true
               page['probation_remarks'].disabled = true
               page['probation_da_charge_id'].focus
            end
         end
      else
         render :text => ''
      end
   end
      
   protected
   
   # Ensures that the 'Probation' menu item is highlighted as current for
   # all actions in this controller.
   def set_tab_name
      @current_tab = "Probation"
   end
end
