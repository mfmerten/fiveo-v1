# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Evidences are records detailing the storage and disposition of evidence
# collected by officers. A photo may be taken of the evidence and uploaded
# into the system. Permanent EvidenceLocation records are added to document
# the evidence as it is moved from one place to another.
class EvidencesController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Evidence Listing
   def index
      require_permission Perm[:view_evidence]
      @help_page = ["Evidence","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:evidence_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:evidence_filter] = nil
            redirect_to evidences_path and return
         end
      else
         @query = session[:evidence_filter] || HashWithIndifferentAccess.new
      end
      unless @evidences = paged('evidence', :order => 'id DESC', :include => [:offense, :evidence_locations])
         @query = HashWithIndifferentAccess.new
         session[:evidence_filter] = nil
         redirect_to evidences_path and return
      end
   end

   # Show Evidence
   def show
      require_permission Perm[:view_evidence]
      @help_page = ["Evidence","show"]
      params[:id] or raise_missing "Evidence ID Not Specified"
      @evidence = Evidence.find_by_id(params[:id]) or raise_not_found "Evidence ID #{params[:id]}"
      enforce_privacy(@evidence)
      @page_title = "Evidence ID: #{@evidence.id}#{@evidence.legacy? ? ' -- [Legacy]' : ''}"
      @links = []
      if has_permission(Perm[:update_evidence]) && check_author(@evidence)
         @links.push(['Edit', edit_evidence_path(:id => @evidence.id)])
      end
      if has_permission(Perm[:destroy_evidence]) && check_author(@evidence)
         @links.push(['Delete', @evidence, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Evidence?'}])
      end
      if has_permission Perm[:update_evidence]
         @links.push(['Move', evidence_location_path(:evidence_id => @evidence.id)])
         if check_author(@evidence)
            @links.push(["Photo", photo_evidence_path(:id => @evidence.id)])
         end
      end
      @page_links = [@evidence.offense, @evidence.investigation, @evidence.crime, [@evidence.evidence_owner,'Owner'],[@evidence.creator,'creator'],[@evidence.updater,'updater']]
   end

   # Add or Edit Evidence
   def edit
      require_permission Perm[:update_evidence]
      @help_page = ["Evidence","edit"]
      if params[:id]
         @evidence = Evidence.find_by_id(params[:id]) or raise_not_found "Evidence ID #{params[:id]}"
         enforce_privacy(@evidence)
         enforce_author(@evidence)
         @page_title = "Edit Evidence ID: #{@evidence.id}"
         @evidence.updated_by = session[:user]
         @evidence.legacy = false
         @show_initial_form = false
      else
         if params[:offense_id]
            off = Offense.find_by_id(params[:offense_id]) or raise_not_found "Offense ID #{params[:offense_id]}"
            offense = off.id
            return_path = off
            priv = nil
            inv_id = nil
            inv_crime_id = nil
            owner = nil
            case_no = off.case_no
         elsif params[:invest_crime_id]
            ivc = InvestCrime.find_by_id(params[:invest_crime_id]) or raise_not_found "Crime ID #{params[:invest_crime_id]}"
            offense = nil
            return_path = ivc
            priv = ivc.private?
            inv_id = ivc.investigation_id
            inv_crime_id = ivc.id
            owner = ivc.owned_by
            case_no = nil
         elsif params[:investigation_id]
            inv = Investigation.find_by_id(params[:investigation_id]) or raise_not_found "Investigation ID #{params[:investigation_id]}"
            offense = nil
            return_path = inv
            priv = inv.private?
            inv_id = inv.id
            inv_crime_id = nil
            owner = inv.owned_by
            case_no = nil
         else
            raise_missing "Offense ID or Crime ID or Investigation ID"
         end
         @evidence = Evidence.new(
            :created_by => session[:user],
            :updated_by => session[:user],
            :offense_id => offense,
            :investigation_id => inv_id,
            :invest_crime_id => inv_crime_id,
            :private => priv,
            :owned_by => owner,
            :case_no => case_no
         ) or raise_db(:new, "Evidence")
         @page_title = "New Evidence"
         @show_initial_form = true
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @evidence.new_record?
               redirect_to return_path
            else
               redirect_to @evidence
            end
            return
         end
         @evidence.attributes = params[:evidence]
         if @evidence.save
            redirect_to @evidence
            if params[:id]
               user_action("Evidence","Edited Evidence ID: #{@evidence.id}.")
            else
               user_action("Evidence", "Added Evidence ID: #{@evidence.id}.")
            end
         end
      end
   end
   
   # Evidence Photo
   def photo
      require_permission Perm[:update_evidence]
      @help_page = ["Evidence","photo"]
      params[:id] or raise_missing "Evidence ID"
      @evidence = Evidence.find_by_id(params[:id]) or raise_not_found "Evidence ID #{params[:id]}"
      enforce_privacy(@evidence)
      enforce_author(@evidence)
      @page_title = "Evidence Photo"
      @evidence.updated_by = session[:user]
      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to @evidence
            return
         end
         @evidence.attributes = params[:evidence]
         if params[:commit] == 'Delete'
            @evidence.evidence_photo = nil
         end
         if @evidence.save
            redirect_to @evidence
            if params[:commit] == 'Delete'
               user_action("Evidence","Removed Evidence Photo for Evidence ID: #{@evidence.id}.")
            else
               user_action("Evidence","Updated Evidence Photo for Evidence ID: #{@evidence.id}.")
            end
         end
      end
   end

   # Destroys the specified evidence. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_evidence]
      require_method :delete
      params[:id] or raise_missing "Evidence ID"
      @evidence = Evidence.find_by_id(params[:id]) or raise_not_found "Evidence ID #{params[:id]}"
      enforce_privacy(@evidence)
      enforce_author(@evidence)
      if !@evidence.crime.nil?
         return_path = @evidence.crime
      elsif !@evidence.investigation.nil?
         return_path = @evidence.investigation
      elsif !@evidence.offense.nil?
         return_path = @evidence.offense
      else
         return_path = evidences_path
      end
      @evidence.destroy or raise_db(:destroy, "Evidence ID #{params[:id]}")
      user_action("Evidence","Destroyed Evidence ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to return_path
   end
   
   # This is called from the show evidence view and is used to add a new
   # evidence location record (move or disposition evidence).
   def location
      require_permisison Perm[:update_evidence]
      @help_page = ["Evidence","location"]
      params[:evidence_id] or raise_missing "Evidence ID"
      @evidence = Evidence.find_by_id(params[:evidence_id]) or raise_not_found "Evidence ID #{params[:evidence_id]}"
      enforce_privacy(@evidence)
      @evidence_location = EvidenceLocation.new(:evidence_id => params[:evidence_id], :created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Evidence Location")
      @evidence_location.storage_date, @evidence_location.storage_time = get_date_and_time
      if !current_user.contact.nil?
         @evidence_location.officer_id = current_user.contact.id
      else
         @evidence_location.officer = session[:user_name]
         @evidence_location.officer_unit = session[:user_unit]
         @evidence_location.officer_badge = session[:user_badge]
      end
      @evidence.update_attribute(:updated_by, session[:user])
      @page_title = "Move Evidence"
      if @evidence.evidence_locations.empty?
         @evidence_location.location_from = "Initial Collection"
      else
         @evidence_location.location_from = @evidence.evidence_locations.last.location_to
      end
      
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to @evidence and return
         end
         @evidence_location.attributes = params[:evidence_location]
         if @evidence_location.save
            redirect_to @evidence
            user_action("Evidence","Moved Evidence ID: #{@evidence.id} From #{@evidence_location.location_from} To #{@evidence_location.location_to}.")
         end
      end
   end
   
   protected
   
   # Ensures that the "Evidence" menu item is highlighted for all actions
   # in this controller.
   def set_tab_name
      @current_tab = 'Evidence'
   end
end
