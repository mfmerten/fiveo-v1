# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Pawns are pawn shop tickets.
class PawnsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Pawn Listing
   def index
      require_permission Perm[:view_pawn]
      @help_page = ["Pawns","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:pawn_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:pawn_filter] = nil
            redirect_to pawns_path and return
         end
      else
         @query = session[:pawn_filter] || HashWithIndifferentAccess.new
      end
      unless @pawns = paged('pawn',:order => 'ticket_date DESC', :include => [:pawn_items,:pawn_co,:ticket_type])
         @query = HashWithIndifferentAccess.new
         session[:pawn_filter] = nil
         redirect_to pawns_path and return
      end
      @links = []
      if has_permission(Perm[:update_pawn])
         @links.push(['New', edit_pawn_path])
      end
      if current_user.review_pawn?
         @links.push(['Review', url_for(:action => 'review')])
      end
   end

   # Show Pawn
   def show
      require_permission Perm[:view_pawn]
      @help_page = ["Pawns","show"]
      params[:id] or raise_missing "Pawn ID"
      @pawn = Pawn.find_by_id(params[:id]) or raise_not_found "Pawn ID #{params[:id]}"
      @page_title = "Pawn ID: #{@pawn.id}#{@pawn.legacy? ? ' -- [Legacy]' : ''}"
      @links = []
      if has_permission(Perm[:update_pawn])
         @links.push(['New', edit_pawn_path])
         @links.push(['Edit', edit_pawn_path(:id => @pawn.id)])
      end
      if has_permission Perm[:destroy_pawn]
         @links.push(['Delete', @pawn, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Pawn Ticket?'}])
      end
      @page_links = [[@pawn.creator,'creator'],[@pawn.updater,'updater']]
   end

   # Add or Edit Pawn Ticket
   def edit
      require_permission Perm[:update_pawn]
      @help_page = ["Pawns","edit"]
      if params[:id]
         @pawn = Pawn.find_by_id(params[:id]) or raise_not_found "Pawn ID #{params[:id]}"
         @page_title = "Edit Pawn ID: #{@pawn.id}"
         @pawn.updated_by = session[:user]
         @pawn.legacy = false
         if @pawn.pawn_items.empty?
            unless request.post?
               @pawn.pawn_items.build(:created_by => session[:user], :updated_by => session[:user])
            end
         end
      else
         @pawn = Pawn.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Pawn")
         @page_title = "New Pawn Ticket"
         unless request.post?
            @pawn.pawn_items.build(:created_by => session[:user], :updated_by => session[:user])
         end
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @pawn.new_record?
               redirect_to pawns_path
            else
               redirect_to @pawn
            end
            return
         end
         params[:pawn][:existing_pawn_item_attributes] ||= {}
         @pawn.attributes = params[:pawn]
         if @pawn.save
            redirect_to @pawn
            if params[:id]
               user_action("Pawns","Edited Pawn Ticket ID: #{@pawn.id}.")
            else
               user_action("Pawns", "Added Pawn Ticket ID: #{@pawn.id}.")
            end
         end
      end
   end

   # Destroys the specified pawn ticket. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_pawn]
      require_method :delete
      params[:id] or raise_missing "Pawn ID"
      @pawn = Pawn.find_by_id(params[:id]) or raise_not_found "Pawn ID #{params[:id]}"
      @pawn.destroy or raise_db(:destroy, "Pawn ID #{params[:id]}")
      user_action("Pawns", "Destroyed Pawn Ticket ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to pawns_path
   end
   
   # Review latest pawn tickets
   def review
      require_permission Perm[:view_pawn]
      @page_title = "Pawns Review"
      @pawns = review_paged('pawn',:include => [:pawn_items,:pawn_co,:ticket_type])
      @links = [["View Next Page", url_for(:action => 'review')]]
      @links.push(["Skip All",url_for(:action => 'skip_all')])
   end

   # skip all reviews
   def skip_all
      require_permission Perm[:view_pawn]
      review_skip_all('pawn')
      flash[:notice] = "All pawn ticket reviews skipped!"
      redirect_to root_path
      return
   end

   protected

   # ensures "Pawns" menu item is highlighted for all actions in this
   # controller
   def set_tab_name
      @current_tab = "Pawns"
   end
end
