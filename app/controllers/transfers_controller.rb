# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Transfers Controller
# displays and controls access to the transfer log
class TransfersController < ApplicationController

   before_filter :set_tab_name
   before_filter :update_session_active
   
   # show transfer log
   def index
      require_permission Perm[:view_booking]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:transfer_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:transfer_filter] = nil
            redirect_to transfers_path and return
         end
      else
         @query = session[:transfer_filter] || HashWithIndifferentAccess.new
      end
      unless @transfers = paged('transfer',:order => 'transfers.id DESC', :include => [:person, :booking, :transfer_type], :page_name => 'Transfer Log')
         @query = HashWithIndifferentAccess.new
         session[:transfer_filter] = nil
         redirect_to transfers_path and return
      end
   end
   
   # view transfer information
   def show
      require_permission Perm[:view_booking]
      params[:id] or raise_missing "Transfer ID"
      @transfer = Transfer.find_by_id(params[:id]) or raise_not_found "Transfer ID #{params[:id]}"
      @links = []
      if has_permission(Perm[:destroy_booking])
         @links.push(["Delete", @transfer, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Transfer?'}])
      end
      @page_links = [@transfer.person, @transfer.booking, [@transfer.from_agency_id,'From'],[@transfer.to_agency_id,'to'],[@transfer.from_officer_id,'officer'],[@transfer.to_officer_id,'officer'],[@transfer.creator,'creator'],[@transfer.updater,'updater']]
   end
   
   # this updates the transfer form via ajax
   def update_transfer
      unless has_permission(Perm[:update_booking]) && params[:transfer_type_id] && params[:booking_id] && (@booking = Booking.find_by_id(params[:booking_id]))
         render :text => ''
         return
      end
      transfer_type = Option.find_by_id(params[:transfer_type_id])
      if transfer_type.nil?
         render :text => ''
         return
      end
      
      # initial settings
      transfer_date, transfer_time = get_date_and_time
      from_agency = nil
      from_officer = nil
      from_officer_unit = nil
      from_officer_badge = nil
      to_agency = nil
      to_officer = nil
      to_officer_unit = nil
      to_officer_badge = nil
      remarks = nil
      
      case transfer_type.long_name
      when "DOC Transfer (IN)", "Temporary Housing (IN)", "Temporary Transfer Return (IN)"
         to_agency = SiteConfig.agency_short
         to_officer = session[:user_name]
         to_officer_unit = session[:user_unit]
         to_officer_badge = session[:user_badge]
         if transfer_type.long_name == "Temporary Transfer Return (IN)"
            if hold = @booking.holds.reject{|h| h.cleared_date? || h.hold_type.nil? || h.hold_type.long_name != 'Temporary Transfer'}.first
               from_agency = hold.agency
            end
         end
      when "Permanent Transfer (OUT)", "Temporary Transfer (OUT)", "Temporary Housing Return (OUT)"
         from_agency = SiteConfig.agency_short
         from_officer = session[:user_name]
         from_officer_unit = session[:user_unit]
         from_officer_badge = session[:user_badge]
         if transfer_type.long_name == "Temporary Housing Return (OUT)"
            if hold = @booking.holds.reject{|h| h.cleared_date? || h.hold_type.nil? || h.hold_type.long_name != 'Temporary Housing'}.first
               to_agency = hold.agency
            end
         end
      end

      render :update do |page|
         page.replace_html 'type-name', transfer_type.long_name
         page['transfer_transfer_type_id'].value = transfer_type.id.to_s
         page['transfer_transfer_date'].value = transfer_date.to_s(:us)
         page['transfer_transfer_time'].value = transfer_time
         page['transfer_from_agency'].value = from_agency
         page['transfer_from_officer'].value = from_officer
         page['transfer_from_officer_unit'].value = from_officer_unit
         page['transfer_from_officer_badge'].value = from_officer_badge
         page['transfer_to_agency'].value = to_agency
         page['transfer_to_officer'].value = to_officer
         page['transfer_to_officer_unit'].value = to_officer_unit
         page['transfer_to_officer_badge'].value = to_officer_badge
         page['transfer_remarks'].value = remarks
         page.hide 'select-type'
         page.show 'transfer-form'
         page['transfer_transfer_date'].focus
      end
   end
   
   # called to transfer a prisoner to another agency
   def transfer
      require_permission Perm[:update_booking]
      params[:id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:id]) or raise_not_found "Booking ID #{params[:id]}"
      # determine all valid transfer types for the current booking status
      # and set @transfer_types accordingly
      @page_links = [@booking.person, @booking]
      if @booking.is_transferred
         # currently transferred out, can only transfer back
         tname = 'Temporary Transfer Return (IN)'
         @types = [[tname,Option.lookup("Transfer Type",tname,nil,nil,true)]]
      elsif @booking.is_housing
         # currently temporary housing, can return (out) or temp transfer (out)
         @types = []
         ['Temporary Housing Return (OUT)','Temporary Transfer (OUT)'].each do |t|
            @types.push([t,Option.lookup("Transfer Type",t,nil,nil,true)])
         end
      else
         # anything goes except the returns
         @types = []
         ['DOC Transfer (IN)', 'Temporary Housing (IN)', 'Temporary Transfer (OUT)', 'Permanent Transfer (OUT)'].each do |t|
            @types.push([t,Option.lookup("Transfer Type",t,nil,nil,true)])
         end
      end
      @types.unshift(['---',nil])
      @transfer = Transfer.new(
         :booking_id => @booking.id,
         :person_id => @booking.person_id,
         :created_by => session[:user],
         :updated_by => session[:user]
      ) or raise_db(:new, "Transfer")
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to @booking and return
         end
         @transfer.attributes = params[:transfer]
         if @transfer.save
            user_action("Booking","Transferred Booking ID: #{@booking.id} to #{@transfer.to_agency}.")
            redirect_to @booking
         end
      end
   end
   
   def destroy
      require_permission Perm[:destroy_booking]
      require_method :delete
      params[:id] or raise_missing "Transfer ID"
      @transfer = Transfer.find_by_id(params[:id]) or raise_not_found "Transfer ID #{params[:id]}"
      @booking = @transfer.booking
      @transfer.destroy or raise_db(:destroy, "Transfer ID #{params[:id]}")
      user_action("Booking","Destroyed Transfer ID #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to @booking
   end

   protected
   
   # ensures that all actions will have the correct menu item highlighted.
   def set_tab_name
      @current_tab = "Transfer Log"
   end
end