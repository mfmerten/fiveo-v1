# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# This is a special form of contact specific to professional bondsmen. It is
# handled separately from contacts because the bondsmen name may be the name
# of a company, in which case the name parsing done by contacts will fail.
# Bondsman records populate the bondsman select control when entering bonds.
class BondsmenController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Bondsmen Listing
   def index
      require_permission Perm[:view_option]
      @help_page = ["Bondsmen","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:bondsman_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:bondsman_filter] = nil
            redirect_to bondsmen_path and return
         end
      else
         @query = session[:bondsman_filter] || HashWithIndifferentAccess.new
      end
      unless @bondsmen = paged('bondsman',:order => 'bondsmen.name',:include => :bondsman_phones)
         @query = HashWithIndifferentAccess.new
         session[:bondsman_filter] = nil
         redirect_to bondsmen_path and return
      end
      @links = []
      if has_permission Perm[:update_option]
         @links.push(['New', edit_bondsman_path])
      end
   end

   # Show Bondsman
   def show
      require_permission Perm[:view_option]
      @help_page = ["Bondsmen","show"]
      params[:id] or raise_missing "Bondsman ID"
      @bondsman = Bondsman.find_by_id(params[:id]) or raise_not_found "Bondsman ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_option]
         @links.push(["New", edit_bondsman_path])
         @links.push(["Edit", edit_bondsman_path(:id => @bondsman.id)])
      end
      if has_permission Perm[:destroy_option]
         @links.push(['Delete', @bondsman, {:method => 'delete', :confirm => "Are you sure you want to destroy this Bondsman?"}])
      end
      @page_links = [[@bondsman.creator,'creator'],[@bondsman.updater,'updater']]
   end

   # Add or Edit Bondsman
   def edit
      require_permission Perm[:update_option]
      @help_page = ["Bondsmen","edit"]
      if params[:id]
         @bondsman = Bondsman.find_by_id(params[:id]) or raise_not_found "Bondsman ID #{params[:id]}"
         @page_title = "Edit Bondsman ID: #{@bondsman.id}"
         @bondsman.updated_by = session[:user]
         if @bondsman.bondsman_phones.empty?
            unless request.post?
               @bondsman.bondsman_phones.build(:created_by => session[:user], :updated_by => session[:user])
            end
         end
      else
         @page_title = "New Bondsman"
         @bondsman = Bondsman.new(:created_by => session[:user], :updated_by => session[:user], :state_id => Option.lookup("State","Louisiana")) or raise_db(:new, "Bondsman")
         unless request.post?
            @bondsman.bondsman_phones.build(:created_by => session[:user], :updated_by => session[:user])
         end
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @bondsman.new_record?
               redirect_to bondsmen_path
            else
               redirect_to @bondsman
            end
            return
         end
         params[:bondsman][:existing_phone_attributes] ||= {}
         @bondsman.attributes = params[:bondsman]
         if @bondsman.save
            redirect_to @bondsman
            if params[:id]
               user_action("Bondsmen","Edited Bondsman ID: #{@bondsman.id}.")
            else
               user_action("Bondsmen", "Added Bondsman ID: #{@bondsman.id}.")
            end
         end
      end
   end

   # Destroys the specified bondsman record. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_option]
      require_method :delete
      params[:id] or raise_missing "Bondsman ID"
      @bondsman = Bondsman.find_by_id(params[:id]) or raise_not_found "Bondsman ID #{params[:id]}"
      @bondsman.destroy or raise_db(:destroy, "Bondsman ID #{params[:id]}")
      user_action("Bondsmen","Destroyed Bondsman ID: #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to bondsmen_path
   end

   protected

   # ensures that the "Bondsmen" menu item is highlighted for all actions in
   # this controller
   def set_tab_name
      @current_tab = "Bondsmen"
   end
end
