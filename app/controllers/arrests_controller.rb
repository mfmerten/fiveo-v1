# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Arrests are used to place new charges on a person.
class ArrestsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Arrests Listing
   def index
      require_permission Perm[:view_arrest]
      @help_page = ["Arrests","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:arrest_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:arrest_filter] = nil
            redirect_to arrests_path and return
         end
      else
         @query = session[:arrest_filter] || HashWithIndifferentAccess.new
      end
      unless @arrests = paged('arrest',:order => 'arrests.id DESC',:include => [:person, {:district_charges => :arrest_warrant}, {:city_charges => :arrest_warrant}, {:other_charges => :arrest_warrant}])
         @query = HashWithIndifferentAccess.new
         session[:arrest_filter] = nil
         redirect_to arrests_path and return
      end
      @links = []
      if current_user.review_arrest?
         @links.push(["Review", url_for(:action => 'review')])
      end
   end

   # Show Arrest
   def show
      require_permission Perm[:view_arrest]
      @help_page = ["Arrests","show"]
      params[:id] or raise_missing "Arrest ID"
      @arrest = Arrest.find_by_id(params[:id]) or raise_not_found "Arrest ID #{params[:id]}"
      @page_title = "Arrest ID: #{@arrest.id}#{@arrest.legacy? ? ' -- [Legacy]' : ''}"
      @links = []
      if has_permission Perm[:update_arrest]
         @links.push(["Edit", edit_arrest_path(:id => @arrest.id)])
      end
      if has_permission Perm[:destroy_arrest]
         @links.push(['Delete', @arrest, {:method => 'delete', :confirm => "Are you sure you want to destroy this Arrest?"} ])
      end
      @links.push(['ID Card PDF', id_card_path(:id => @arrest.id)])
      if @arrest.find_48
         @links.push(['48Hr PDF', url_for(:action => 'print_48', :id => @arrest.id)])
      end
      @links.push(['72Hr PDF', url_for(:action => 'print_72', :id => @arrest.id)])
      if !@arrest.person.bookings.empty? && !@arrest.person.bookings.last.release_date? && !@arrest.holds.empty? && @arrest.holds.last.booking != @arrest.person.bookings.last
         @links.push(['Rebook', url_for(:action => 'rebook', :id => @arrest.id, :booking_id => @arrest.person.bookings.last.id), {:method => 'post'}])
      end
      #
      @page_links = [@arrest.person, @arrest.bonds, @arrest.offense]
      @page_links.concat(@arrest.holds.collect{|h| h.booking}.uniq)
      @page_links.push([@arrest.officer1_id,'Officer1'],[@arrest.officer2_id,'officer2'],[@arrest.officer3_id,'officer3'],[@arrest.agency_id,'agency'],[@arrest.dwi_test_officer_id,'dwi tester'],[@arrest.creator,'Creator'], [@arrest.updater, 'Updater'])
   end
   
   def rebook
      require_permission Perm[:update_arrest]
      require_method :post
      params[:id] or raise_missing "Arrest ID"
      @arrest = Arrest.find_by_id(params[:id]) or raise_not_found "Arrest ID #{params[:id]}"
      params[:booking_id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:booking_id]) or raise_not_found "Booking ID #{params[:booking_id]}"
      if @booking.release_date?
         flash[:warning] = "Arrested Persons Last Booking (ID #{params[:booking_id]}) Is Not Open"
         redirect_to @arrest and return
      end
      @arrest.holds.reverse.each do |h|
         next if h.hold_type.nil? || !["72 Hour","Bondable","No Bond","Hold for City","Hold for Other Agency"].include?(h.hold_type.long_name)
         if h.cleared_date?
            h.clear_release(session[:user])
            h.update_attribute(:booking_id, @booking.id)
            flash[:notice] = "Successfully placed #{h.hold_type.long_name} hold on new booking."
            redirect_to @booking
            return
         end
      end
      flash[:warning] = "Could not find an appropriate hold to rebook!"
      redirect_to @arrest
   end
   
   # Add or Edit Arrest
   def edit
      require_permission Perm[:update_arrest]
      @help_page = ["Arrests","edit"]
      if params[:id]
         @arrest = Arrest.find_by_id(params[:id]) or raise_not_found "Arrest ID #{params[:id]}"
         if @arrest.person.bookings.empty?
            flash[:warning] = "Arrested Person Not Booked. Cannot edit this arrest."
            redirect_to @arrest and return
         end
         @booking = @arrest.person.bookings.last
         if @booking.release_date?
            # booking has closed
            flash.now[:warning] = "Warning... the booking record is closed. Editing this arrest may have unintended consequences."
         elsif !@arrest.holds.empty? && @arrest.holds.first.booking != @booking
            # booking has changed
            flash.now[:warning] = "Warning... Arrested person has a new booking. Editing this arrest may have unintended consequences."
         end
         @page_title = "Edit Arrest ID: #{@arrest.id}"
         @arrest.updated_by = session[:user]
         @arrest.legacy = false
         if @arrest.district_charges.empty? && @arrest.city_charges.empty? && @arrest.other_charges.empty?
            @type_locked = false
         else
            @type_locked = true
         end
         unless request.post?
            if @arrest.district_charges.empty?
               @arrest.district_charges.build(:created_by => session[:user], :updated_by => session[:user])
            end
            if @arrest.district_warrants.empty?
               @arrest.district_warrants.build(:created_by => session[:user], :updated_by => session[:user])
            end
            if @arrest.city_charges.empty?
               @arrest.city_charges.build(:created_by => session[:user], :updated_by => session[:user])
            end
            if @arrest.city_warrants.empty?
               @arrest.city_warrants.build(:created_by => session[:user], :updated_by => session[:user])
            end
            if @arrest.other_warrants.empty?
               @arrest.other_warrants.build(:created_by => session[:user], :updated_by => session[:user])
            end
         end
      else
         params[:booking_id] or raise_missing "Booking ID"
         @booking = Booking.find_by_id(params[:booking_id]) or raise_not_found "Booking ID #{params[:booking_id]}"
         @page_title = "New Arrest"
         @arrest = Arrest.new(:arrest_type => 0, :person_id => @booking.person_id, :created_by => session[:user], :updated_by => session[:user], :bond_amt => 0) or raise_db(:new, "Arrest")
         @arrest.arrest_date, @arrest.arrest_time = get_date_and_time
         unless request.post?
            @arrest.district_charges.build(:created_by => session[:user], :updated_by => session[:user])
            @arrest.city_charges.build(:created_by => session[:user], :updated_by => session[:user])
            @arrest.district_warrants.build(:created_by => session[:user], :updated_by => session[:user])
            @arrest.city_warrants.build(:created_by => session[:user], :updated_by => session[:user])
            @arrest.other_warrants.build(:created_by => session[:user], :updated_by => session[:user])
         end
      end
      @page_links = [@booking]
      if request.post?
         if params[:commit] == "Cancel"
            if @arrest.new_record?
               redirect_to arrests_path
            else
               redirect_to @arrest
            end
            return
         end
         params[:arrest][:existing_district_charges] ||= {}
         params[:arrest][:existing_city_charges] ||= {}
         params[:arrest][:assigned_district_warrants] ||= {}
         params[:arrest][:assigned_city_warrants] ||= {}
         params[:arrest][:assigned_other_warrants] ||= {}
         @arrest.attributes = params[:arrest]
         # added to catch warrants served on the wrong person
         warrant_errors = false
         unless params[:arrest][:assigned_other_warrants].empty?
            params[:arrest][:assigned_other_warrants].each do |x|
               if war = Warrant.find_by_id(x)
                  unless war.person.nil?
                     if war.person.id != @arrest.person_id
                        @arrest.errors.add(:base, "Other Warrant No. #{war.warrant_no} is not for this person!")
                        warrant_errors = true
                     end
                  end
               end
            end
         end
         unless params[:arrest][:assigned_city_warrants].empty?
            params[:arrest][:assigned_city_warrants].each do |x|
               if war = Warrant.find_by_id(x)
                  unless war.person.nil?
                     if war.person.id != @arrest.person_id
                        @arrest.errors.add(:base, "City Warrant No. #{war.warrant_no} is not for this person!")
                        warrant_errors = true
                     end
                  end
               end
            end
         end
         unless params[:arrest][:assigned_district_warrants].empty?
            params[:arrest][:assigned_district_warrants].each do |x|
               if war = Warrant.find_by_id(x)
                  unless war.person.nil?
                     if war.person.id != @arrest.person_id
                        @arrest.errors.add(:base, "District Warrant No. #{war.warrant_no} is not for this person!")
                        warrant_errors = true
                     end
                  end
               end
            end
         end
         unless warrant_errors
            if @arrest.save
               redirect_to @arrest
               if params[:id]
                  user_action("Arrest","Edited Arrest ID: #{@arrest.id} (#{show_person @arrest.person}).")
               else
                  user_action("Arrest", "Added Arrest ID: #{@arrest.id} (#{show_person @arrest.person}).")
               end
            end
         end
      end
      if params[:seventytwo]
         render :template => 'arrests/72_hour'
      end
   end

   # 72 hour information for Arrest
   def seventytwo
      require_permission Perm[:update_arrest]
      @help_page = ["Arrests","seventytwo"]
      params[:id] or raise_missing "Arrest ID"
      @arrest = Arrest.find_by_id(params[:id]) or raise_not_found "Arrest ID #{params[:id]}"
      if @arrest.arrest_type != 0
         flash[:warning] = "Not a District Arrest, 72 Hour Not Applicable"
         redirect_to @arrest and return
      end
      unless hold = @arrest.find_72(true)
         flash[:warning] = "No 72 Hour hold found for this Arrest.  72 Hour Not Applicable"
         redirect_to @arrest and return
      end
      if hold.cleared_date?
         flash[:warning] = "72 Hour hold has already been cleared.  72 Hour not Applicable"
         redirect_to hold and return
      end
      if hold.booking.nil?
         flash[:warning] = "72 Hour Hold has no valid booking.  72 Hour Not Applicable"
         redirect_to hold and return
      end
      @booking = hold.booking
      if @booking.release_date?
         # booking has closed
         flash.now[:warning] = "Booking record is closed. 72 Hour Not Applicable"
         redirect_to @booking and return
      end
      @page_title = "72 Hour Information for Arrest ID: #{@arrest.id}"
      @arrest.updated_by = session[:user]
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to @booking and return
         end
         @arrest.attributes = params[:arrest]
         if @arrest.save
            redirect_to @booking
            user_action("Arrest","Updated 72 Hour information for Arrest ID: #{@arrest.id} (#{show_person @arrest.person}).")
         end
      end
   end

   # Generates a PDF booking report, or "ID Card" as it is called for the
   # current record as specified with the id parameter
   def id_card
      require_permission Perm[:view_arrest]
      params[:id] or raise_missing "Arrest ID"
      @arrest = Arrest.find_by_id(params[:id]) or raise_not_found "Arrest ID #{params[:id]}"
      @page_title = "IDENTIFICATION CARD"
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "id-card-#{@arrest.id}.pdf"
      render :template => 'arrests/id_card.pdf.prawn', :layout => false
      user_action("Arrest","Printed Jail ID Card for Arrest ID: #{@arrest.id}.")
   end

   # Destroys the specified arrest record. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_arrest]
      require_method :delete
      params[:id] or raise_missing "Arrest ID"
      @arrest = Arrest.find_by_id(params[:id]) or raise_missing "Arrest ID #{params[:id]}"
      @arrest.destroy or raise_db(:destroy, "Arrest ID #{params[:id]}")
      user_action("Arrest","Destroyed Arrest ID: #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to arrests_path
   end
   
   # Review latest arrests
   def review
      require_permission Perm[:view_arrest]
      @page_title = "Arrests Review"
      @arrests = review_paged('arrest',:include => :person_review)
      @links = [["View Next Page", url_for(:action => 'review')]]
      @links.push(["Skip All",url_for(:action => 'skip_all')])
   end

   def skip_all
      review_skip_all('arrest')
      flash[:notice] = "All arrest reviews skipped!"
      redirect_to root_path
      return
   end
   
   # prints the PDF version of the 72 Hour form
   def print_72
      require_permission Perm[:view_arrest]
      params[:id] or raise_missing "Arrest ID"
      @arrest = Arrest.find_by_id(params[:id]) or raise_not_found "Arrest ID #{params[:id]}"
      user_action("Arrest","Printed 72 Hour Form for Arrest ID: #{@arrest.id}.")
      @page_title = "Notice Of Custody Of Arrested Person"
      prawnto :prawn => {
         :page_size => 'LEGAL',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "72Hr-#{@arrest.id}.pdf"
      render :template => 'arrests/72_hour_form.pdf.prawn', :layout => false
   end
   
   # prints the PDF version of the 48 Hour form
   def print_48
      require_permission Perm[:view_arrest]
      params[:id] or raise_missing "Arrest ID"
      @arrest = Arrest.find_by_id(params[:id]) or raise_not_found "Arrest ID #{params[:id]}"
      user_action("Arrest","Printed 48 Hour Form for Arrest ID: #{@arrest.id}.")
      @page_title = "Affidavit In Support Of Arrest Without A Warrant"
      prawnto :prawn => {
         :page_size => 'LEGAL',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "48Hr-#{@arrest.id}.pdf"
      render :template => 'arrests/48_hour_form.pdf.prawn', :layout => false
   end
   
   # AJAX: updates arrest form when arrest type changes
   def update_arrest_type
      arrest_type = params[:t]
      case arrest_type
      when '0'
         render :update do |page|
            page.show 'district'
            page.hide 'city'
            page.hide 'other'
         end
      when '1'
         render :update do |page|
            page.hide 'district'
            page.show 'city'
            page.hide 'other'
         end
      when '2'
         render :update do |page|
            page.hide 'district'
            page.hide 'city'
            page.show 'other'
         end
      else
         render :text => ''
      end
   end
   
   # a PDF report that summarizes officer arrests
   def arrest_summary
      require_permission(Perm[:view_arrest])
      # Note: the following assumes arbitrary start and end dates
      #       consider changing it to report on single months only and having
      #       the parameters select a month and a year similar to booking.month_billing
      params[:start_date] or raise_missing "Start Date"
      params[:end_date] or raise_missing "End Date"
      @start_date = valid_date(params[:start_date])
      @stop_date = valid_date(params[:end_date])
      if !@start_date.nil? && !@stop_date.nil? && @start_date > @stop_date
         # wrong order, swap
         @start_date, @stop_date = @stop_date, @start_date
      end
      @agency = Contact.find_by_id(params[:agency_id])
      if @agency.nil?
         @arrests = Arrest.arrest_summary(@start_date, @stop_date)
      else
         if Contact.find(:all, :conditions => {:agency_id => @agency.id}).empty?
           flash[:warning] = "No officers for that Agency in Contacts!"
           redirect_to @arrests and return
         end
         @arrests = Arrest.arrest_summary(@start_date, @stop_date, @agency.id)
      end
      @page_title = "Arrest Summary"
      @report_datetime = DateTime.now
      @timestamp = @report_datetime.to_s(:timestamp)
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "arrest-summary-#{@timestamp}.pdf"
      render :template => 'arrests/arrest_summary.pdf.prawn', :layout => false
      user_action("Arrest","Printed Arrest Summary Report.")
   end
   
   protected
   
   # ensures that the Arrests menu item is highlighted for all actions in this
   # controller
   def set_tab_name
      @current_tab = "Arrests"
   end
end
