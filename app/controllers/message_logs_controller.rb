# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class MessageLogsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   def index
      require_permission Perm[:admin]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:message_log_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:message_log_filter] = nil
            redirect_to message_logs_path and return
         end
      else
         @query = session[:message_log_filter] || HashWithIndifferentAccess.new
      end
      unless @message_logs = paged('message_log', :order => 'message_logs.id DESC', :include => [:sender, :receiver])
         @query = HashWithIndifferentAccess.new
         session[:message_log_filter] = nil
         redirect_to message_logs_path and return
      end
   end
   
   def show
      require_permission Perm[:admin]
      params[:id] or raise_missing "Message Log ID"
      @message_log = MessageLog.find_by_id(params[:id]) or raise_not_found "Message Log ID #{params[:id]}"
      @page_title = "Message Log ID: #{@message_log.id}"
      @page_links = []
      unless @message_log.receiver.nil?
         @page_links.push([@message_log.receiver.contact,'receiver'])
      end
      unless @message_log.sender.nil?
         @page_links.push([@message_log.sender.contact,'sender'])
      end
   end
   
   protected

   # ensures that all actions will have the "Message Log" menu item properly
   # highlighted as current.
   def set_tab_name
      @current_tab = "Message Log"
   end
end
