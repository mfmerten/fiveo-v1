# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# ZIP code database used to validate address entries and to perform city/
# state lookups by zip code.  The database includes longitude and latitude
# per zip code, but we don't use them at this time. This data is city/state/
# county level only, and does not go into street level detail and is therefore
# of limited use.
class ZipsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # ZIP code Listing
   def index
      require_permission Perm[:view_option]
      @help_page = ["Zip Codes","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:zip_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:zip_filter] = nil
            redirect_to zips_path and return
         end
      else
         @query = session[:zip_filter] || HashWithIndifferentAccess.new
      end
      unless @zips = paged('zip', :order => 'zip_code')
         @query = HashWithIndifferentAccess.new
         session[:zip_filter] = nil
         redirect_to zips_path and return
      end
      if has_permission(Perm[:update_option])
         @links = [['New', edit_zip_path]]
      end
   end

   # Show ZIP Code
   def show
      require_permission Perm[:view_option]
      @help_page = ["Zip Codes","show"]
      params[:id] or raise_missing "ZIP Code ID"
      @zip = Zip.find_by_id(params[:id]) or raise_not_found "ZIP Code ID #{params[:id]}"
      @links = []
      if has_permission(Perm[:update_option])
         @links.push(['New', edit_zip_path])
         @links.push(['Edit', edit_zip_path(:id => @zip.id)])
      end
      if has_permission Perm[:destroy_option]
         @links.push(['Delete', @zip, {:method => 'delete', :confirm => 'Are you sure you want to destroy this ZIP Code?'}])
      end
      @page_links = [[@zip.creator,'creator'],[@zip.updater,'updater']]
   end

   # Add or Edit ZIP Code
   def edit
      require_permission Perm[:update_option]
      @help_page = ["Zip Codes","edit"]
      if params[:id]
         @zip = Zip.find_by_id(params[:id]) or raise_not_found "ZIP Code ID #{params[:id]}"
         @page_title = "Edit ZIP Code"
         @zip.updated_by = session[:user]
      else
         @zip = Zip.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "ZIP Code")
         @page_title = "New ZIP Code"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @zip.new_record?
               redirect_to zips_path
            else
               redirect_to @zip
            end
            return
         end
         @zip.attributes = params[:zip]
         if @zip.save
            redirect_to @zip
            if params[:id]
               user_action("Zip Codes","Edited ZIP Code #{@zip.zip_code}.")
            else
               user_action("Zip Codes", "Added User #{@user.login}.")
            end
         end
      end
   end

   # Destroys the specified ZIP code. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_option]
      require_method :delete
      params[:id] or raise_missing "ZIP Code ID"
      @zip = Zip.find_by_id(params[:id]) or raise_not_found "ZIP Code ID #{params[:id]}"
      @zip.destroy or raise_db(:destroy, "ZIP Code ID #{params[:id]}")
      user_action("Zip Codes","Destroyed ZIP Code ID #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to zips_path
   end

   protected

   # highlights "Zip Codes" menu item for all actions in this controller
   def set_tab_name
      @current_tab = "Zip Codes"
   end
end
