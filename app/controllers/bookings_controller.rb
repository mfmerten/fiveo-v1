# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Booking records are essentially inmate inventory records for the jail. A
# booking record begins when the prisoner is taken into custody at the
# detention facility, and ends when the prisoner is released from custody.
# There must not be more than one active booking record per person.
#
class BookingsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Booking index
   def index
      require_permission Perm[:view_booking]
      @help_page = ["Booking","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:booking_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:booking_filter] = nil
            redirect_to bookings_path and return
         end
      else
         @query = session[:booking_filter] || HashWithIndifferentAccess.new
      end
      unless @bookings = paged('booking',:order => 'people.lastname, people.firstname', :include => [:person, :facility, :cell, {:holds => [:doc_record, :hold_type, :blockers, :blocks, :temp_release_reason]}], :active => 'bookings.release_date IS NULL', :skip_query => ['release_date_from','release_date_to'])
         @query = HashWithIndifferentAccess.new
         session[:booking_filter] = nil
         redirect_to bookings_path and return
      end
      @needs_attention = Booking.needs_attention
      @links = []
      if session[:show_all_bookings]
         @links.push(['Show Active', bookings_path(:show_all => '0')])
      else
         @links.push(['Show All', bookings_path(:show_all => '1')])
      end
      @links.push(['PDF', url_for(:action => 'list_form')])
      @links.push(['Visitors', url_for(:action => 'visitors')])
      @links.push(['Photos', url_for(:action => 'photo_album')])
   end

   # Show Booking
   def show
      require_permission Perm[:view_booking]
      @help_page = ["Booking","show"]
      params[:id] or raise_missing "Booking ID"
      @booking = Booking.find(:first, :include => [{:holds => [{:arrest => [:district_charges, :city_charges, :other_charges]}, :hold_type, :booking, :arrest, :sentence, :doc_record, :revocation]}], :conditions => ['bookings.id = ?',params[:id]]) or raise_not_found "Booking ID #{params[:id]}"
      if session[:show_all_holds].nil?
         session[:show_all_holds] = false
      end
      @releasable = true
      @booking.holds.each do |h|
         unless h.cleared_date?
            @releasable = false
         end
      end
      @page_title = "Booking ID: #{@booking.id}#{@booking.legacy? ? ' -- [Legacy]' : ''}"
      @links = []
      if has_permission Perm[:update_booking]
         @links.push(["Edit", edit_booking_path(:id => @booking.id)])
      end
      if has_permission Perm[:destroy_booking]
         @links.push(['Delete', @booking, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Booking?'}])
      end
      if @booking.release_date?
         if @booking == @booking.person.bookings.last && has_permission(Perm[:reopen_booking])
            @links.push(['Reopen', booking_reopen_path(:id => @booking.id), {:method => 'post'}])
         end
      else
         if has_permission Perm[:update_booking]
            @links.push(['Medications', medications_path(:person_id => @booking.person_id)])
            if @releasable
               @links.push(['Release', booking_release_path(:id => @booking.id)])
            end
            @links.push(['Transfer', url_for(:controller => 'transfers', :action => 'transfer', :id => @booking.id)])
            unless @booking.temporary_release?
               @links.push(['Temp Release', url_for(:action => 'temp_release_out', :id => @booking.id)])
            end
         end
         if has_permission Perm[:update_arrest]
            @links.push(['New Arrest', edit_arrest_path(:booking_id => @booking.id)])
         end
         if has_permission Perm[:update_booking]
            @links.push(['New Hold', edit_hold_path(:booking_id => @booking.id)])
            @links.push(["New Visitor", visitor_booking_path(:booking_id => @booking.id)])
         end
      end
      @page_links = [@booking.person, [@booking.booking_officer_id,'booker'],[@booking.release_officer_id,'releaser'],[@booking.creator,'creator'],[@booking.updater,'updater']]
   end
   
   # this toggles the show all holds session variable and reloads the show
   # view
   def toggle_holds
      require_permission Perm[:view_booking]
      params[:id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:id]) or raise_not_found "Booking ID #{params[:id]}"
      session[:show_all_holds] = !session[:show_all_holds]
      redirect_to @booking
   end
   
   # this toggles the good-time flag on the booking record.
   def toggle_goodtime
      require_permission Perm[:update_booking]
      require_method :post
      params[:booking_id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:booking_id]) or raise_not_found "Booking ID #{params[:booking_id]}"
      @booking.update_attribute(:updated_by, session[:user])
      @booking.toggle(:good_time)
      @booking.save(false)
      if @booking.good_time?
         user_action("Booking","Turned on Good Time for Booking ID: #{@booking.id} (#{show_person @booking.person}).")
      else
         user_action("Booking","Turned off Good Time for Booking ID: #{@booking.id} (#{show_person @booking.person}).")
      end   
      redirect_to @booking
   end
   
   # Add or Edit Booking
   def edit
      require_permission Perm[:update_booking]
      @help_page = ["Booking","edit"]
      if params[:id]
         @booking = Booking.find_by_id(params[:id]) or raise_not_found "Booking ID #{params[:id]}"
         @person = @booking.person
         @page_title = "Edit Booking ID: #{@booking.id}"
         @booking.updated_by = session[:user]
         @booking.legacy = false
         if @booking.properties.empty?
            unless request.post?
               @booking.properties.build(:created_by => session[:user], :updated_by => session[:user])
            end
         end
      else
         params[:person_id] or raise_missing "Person ID"
         @person = Person.find_by_id(params[:person_id]) or raise_not_found "Person ID #{params[:person_id]}"
         unless @person.bookings.empty? || @person.bookings.last.release_date?
            flash[:warning] = "Person Already Booked"
            redirect_to @person.bookings.last and return
         end
         @page_title = "New Booking"
         @booking = Booking.new(:created_by => session[:user], :updated_by => session[:user], :person_id => @person.id, :cash_at_booking => BigDecimal.new('0',2)) or raise_db(:new, "Booking")
         unless request.post?
            @booking.properties.build(:created_by => session[:user], :updated_by => session[:user])
         end
         @booking.booking_date, @booking.booking_time = get_date_and_time
         @booking.good_time = SiteConfig.goodtime_by_default
         if !current_user.contact.nil?
            @booking.booking_officer_id = current_user.contact.id
         else
            @booking.booking_officer = session[:user_name]
            @booking.booking_officer_unit = session[:user_unit]
            @booking.booking_officer_badge = session[:user_badge]
         end
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @booking.new_record?
               redirect_to @person
            else
               redirect_to @booking
            end
            return
         end
         params[:booking][:existing_property_attributes] ||= {}
         @booking.attributes = params[:booking]
         if @booking.save
            redirect_to @booking
            if params[:id]
               user_action("Booking","Edited Booking ID: #{@booking.id} (#{show_person @booking.person}).")
            else
               user_action("Booking", "Added Booking ID: #{@booking.id} (#{show_person @booking.person}).")
            end
         end
      end
   end

   # Release Inmate
   def release
      require_permission Perm[:update_booking]
      @help_page = ["Booking","release"]
      params[:id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:id]) or raise_not_found "Booking ID #{params[:id]}"
      @person = @booking.person
      @page_title = "Release Booking ID: #{@booking.id}"
      @booking.updated_by = session[:user]
      @booking.legacy = false
      @booking.release_date, @booking.release_time = get_date_and_time
      if !current_user.contact.nil?
         @booking.release_officer_id = current_user.contact.id
      else
         @booking.release_officer = session[:user_name]
         @booking.release_officer_unit = session[:user_unit]
         @booking.release_officer_badge = session[:user_badge]
      end
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to @booking and return
         end
         @booking.attributes = params[:booking]
         if @booking.save
            redirect_to @booking
            user_action("Booking","Released Booking ID: #{@booking.id} (#{show_person @booking.person}).")
            @booking.send_release_messages
         end
      end
   end

   # Destroys the specified booking record. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_booking]
      require_method :delete
      params[:id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:id]) or raise_not_found "Booking ID #{params[:id]}" 
      @booking.destroy or raise_db(:destroy, "Booking ID #{params[:id]}") 
      user_action("Booking","Destroyed Booking ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to bookings_path
   end
   
   # Called from a button on the booking show view, this updates the current
   # record as specified with the id parameter to remove any release information
   # from the booking record (un-release a prisoner)
   def clear_release
      require_permission Perm[:reopen_booking]
      require_method :post
      params[:id] or raise_missing "Booking ID" 
      @booking = Booking.find_by_id(params[:id]) or raise_not_found "Booking ID #{params[:id]}" 
      unless @booking = @booking.person.bookings.last
         flash[:warning] = "Cannot Reopen Booking, Newer Booking Exists"
         redirect_to @booking and return
      end
      @booking.release_officer = nil
      @booking.release_officer_unit = nil
      @booking.release_officer_badge = nil
      @booking.release_date = nil
      @booking.release_time = nil
      @booking.updated_by = session[:user]
      @booking.released_because_id = nil
      @booking.save(false)
      redirect_to @booking
      user_action("Booking","Updated Booking ID: #{@booking.id} (#{show_person @booking.person}) to Clear Release.")
   end
   
   # called to place inmate on temporary release
   def temp_release_out
      require_permission Perm[:update_booking]
      params[:id] or raise_missing "Booking ID" 
      @booking = Booking.find_by_id(params[:id]) or raise_not_found "Booking ID #{params[:id]}" 
      if @booking.release_date?
         flash[:warning] = "Temporary Release Not Possible, Booking is Closed"
         redirect_to @booking and return
      end
      if @booking.temporary_release?
         flash[:warning] = "Temporary Release Not Possible, Already On Temporary Release"
         redirect_to @booking and return
      end
      @page_links = [@booking, [@booking.person,'inmate']]
      if request.post?
         redirect_to @booking
         return if params[:commit] == "Cancel"
         unless params[:reason]
            flash[:warning] = 'No Reason Specified!'
            return
         end
         unless ot = OptionType.find_by_name("Temporary Release Reason")
            flash[:warning] = "Missing Option Type (Temporary Release Reason)"
            return
         end
         unless o = Option.find_by_id(params[:reason])
            flash[:warning] = 'Invalid Reason Specified!'
            return
         end
         unless o.option_type_id == ot.id
            flash[:warning] = 'Invalid Reason Type Specified!'
            return
         end
         unless trelease = Option.lookup("Hold Type", 'Temporary Release',nil,nil,true)
            flash[:warning] = 'Required Options Missing!'
            return
         end
         @hold = Hold.new(
            :booking_id => @booking.id,
            :hold_type_id => trelease,
            :hold_by => session[:user_name],
            :hold_by_unit => session[:user_unit],
            :hold_by_badge => session[:user_badge],
            :temp_release_reason_id => o.id,
            :created_by => session[:user],
            :updated_by => session[:user]
         ) or raise_db(:new, "Hold")
         @hold.hold_date, @hold.hold_time = get_date_and_time
         unless current_user.contact.nil?
            @hold.hold_by_id = current_user.contact.id
         end
         if @hold.save
            @booking.stop_time(session[:user])
         else
            flash[:warning] = "Failed to save hold! #{@hold.error_messages}"
            return
         end
      end
   end
   
   # called to return an inmate from temporary release
   def temp_release_in
      require_permission Perm[:update_booking]
      require_method :post
      params[:hold_id] or raise_missing "Hold ID" 
      @hold = Hold.find_by_id(params[:hold_id]) or raise_not_found "Hold ID #{params[:hold_id]}" 
      if @hold.booking.nil?
         flash[:warning] = "Hold has an invalid Booking"
         redirect_to @hold and return
      end
      if @hold.cleared_date?
         flash[:warning] = "Hold is not active!"
         redirect_to @hold.booking and return
      end
      @booking = @hold.booking
      # update the hold
      unless current_user.contact.nil?
         @hold.cleared_by_id = current_user.contact.id
      else
         @hold.cleared_by = session[:user_name]
         @hold.cleared_by_unit = session[:user_unit]
         @hold.cleared_by_badge = session[:user_badge]
      end
      @hold.cleared_because_id = Option.lookup("Release Type","Manual Release",nil,nil,true)
      @hold.cleared_date, @hold.cleared_time = get_date_and_time
      @hold.explanation = "Returned from Temporary Release"
      unless @hold.save
         flash[:warning] = "Failed to Save Hold! #{@hold.errors.full_messages}"
         redirect_to @booking and return
      end
      @booking.start_time(session[:user])
      redirect_to @booking
   end
   
   # generates PDF for Jail Card
   def jail_card
      require_permission Perm[:view_booking]
      params[:booking_id] or raise_missing "Booking ID" 
      @booking = Booking.find_by_id(params[:booking_id]) or raise_not_found "Booking ID #{params[:booking_id]}"
      if @booking.person.nil?
         flash[:warning] = "Booking has Invalid Person!"
         redirect_to @booking and return
      end
      @person = @booking.person
      @arrests = @booking.holds.reject{|h| h.cleared_date? || h.arrest.nil?}
      fines_costs = Option.lookup("Hold Type","Fines/Costs",nil,nil,true)
      serving_time = Option.lookup("Hold Type","Serving Sentence",nil,nil,true)
      @sentences = @booking.holds.reject{|h| h.cleared_date? || (h.hold_type_id != serving_time && h.hold_type_id != fines_costs)}
      temp_trans = Option.lookup("Hold Type", "Temporary Transfer",nil,nil,true)
      temp_rel = Option.lookup("Hold Type","Temporary Release",nil,nil,true)
      @other = @booking.holds.reject{|h| h.cleared_date? || !h.arrest.nil? || h.hold_type_id == serving_time || h.hold_type_id == fines_costs || h.hold_type_id == temp_trans || h.hold_type_id == temp_rel}
      prawnto :prawn => {
         :page_size => 'LEGAL',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Jail_Card-#{@booking.id}.pdf"
      render :template => 'bookings/jail_card.pdf.prawn', :layout => false
      user_action("Booking","Printed Jail Card for Booking ID: #{@booking.id} (#{show_person @booking.person}).")
   end
   
   # Add a new visitor record.
   def visitor_in
      require_permission Perm[:update_booking]
      @help_page = ["Booking","visitor_in"]
      params[:booking_id] or raise_missing "Booking ID" 
      @booking = Booking.find_by_id(params[:booking_id]) or raise_not_found "Booking ID #{params[:booking_id]}" 
      @visitor = Visitor.new(:created_by => session[:user], :updated_by => session[:user], :booking_id => @booking.id, :visit_date => Date.today, :sign_in_time => current_time) or raise_db(:new, "Visitor")
      @page_title = "New Visitor"
      @page_links = [@booking, @booking.person]
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to @booking and return
         end
         @visitor.attributes = params[:visitor]
         if @visitor.save
            # check to see if person has a matching associate record
            # and create one otherwise. (compare name and relationships only)
            assoc_match = false
            @booking.person.associates.each do |a|
               if a.name == @visitor.name && a.relationship_id == @visitor.relationship_id
                  assoc_match = true
               end
            end
            unless assoc_match
               unless Associate.create(:person_id => @booking.person_id, :created_by => session[:user], :updated_by => session[:user], :relationship_id => @visitor.relationship_id, :name => @visitor.name, :remarks => "Jail Visitor")
                  flash[:warning] = "Warning: Could not associate visitor to person: Associate Creation Failed!"
               end
            end
            redirect_to @booking
            user_action("Visitor", "Signed In Visitor (#{@visitor.name}) for Booking ID: #{@booking.id} (#{show_person @booking.person}).")
         end
      end
   end
   
   # Called from a link on the booking view (visitor table), this is used
   # to sign a visitor out using the current time and user information.
   def visitor_out
      require_permission Perm[:update_booking]
      require_method :post
      params[:booking_id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:booking_id]) or raise_not_found "Booking ID #{params[:booking_id]}" 
      params[:id] or raise_missing "Visitor ID"
      @visitor = Visitor.find_by_id(params[:id]) or raise_not_found "Visitor ID #{params[:id]}"
      if @visitor.booking.nil?
         flash[:warning] = "Visitor has Invalid Booking"
         redirect_to @booking and return
      end
      unless @visitor.booking_id == @booking.id
         flash[:warning] = "Visitor is not for Specified Booking"
         redirect_to @visitor.booking and return
      end
      if @visitor.sign_out_time?
         flash[:warning] = "Visitor already signed out!"
         redirect_to @booking and return
      end
      @visitor.sign_out_time = current_time
      @visitor.updated_by = session[:user]
      @visitor.save(false)
      redirect_to @booking
      user_action("Visitor", "Signed Out Visitor (#{@visitor.name}) for Booking ID: #{@booking.id} (#{show_person @booking.person}).")
   end
   
   # Called from a link on the booking view (visitor table), this is used
   # to undo the case where a visitor was mistakenly signed out.
   def visit_reset
      require_permission Perm[:update_booking]
      require_method :post
      params[:id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:id]) or raise_not_found "Booking ID #{params[:id]}"
      params[:visitor] or raise_missing "Visitor ID" 
      @visitor = Visitor.find_by_id(params[:visitor]) or raise_not_found "Visitor ID #{params[:visitor]}"
      if @visitor.booking.nil?
         flash[:warning] = "Visitor has Invalid Booking"
         redirect_to @booking and return
      end
      unless @visitor.booking_id == @booking.id
         flash[:warning] = "Visitor is not for Specified Booking"
         redirect_to @visitor.booking and return
      end
      unless @visitor.sign_out_time?
         flash[:warning] = "Visitor not signed out!"
         redirect_to @booking and return
      end
      @visitor.update_attribute(:sign_out_time, nil)
      @visitor.update_attribute(:updated_by, session[:user])
      redirect_to @booking
      user_action("Visitor", "Reset Visitor (#{@visitor.name}) for Booking ID: #{@booking.id} (#{show_person @booking.person}).")
   end
   
   # Visitor listing. Called from a button on the booking index view, this
   # produces a listing of all visitors currently in any of the facilities.
   def visitors
      require_permission Perm[:view_booking]
      @help_page = ["Booking","visitors"]
      @visitors = Visitor.find(:all, :order => 'visit_date, sign_in_time', :conditions => 'sign_out_time is NULL')
      @page_title = "Current Visitors"
      @links = [["Return", bookings_path]]
   end
   
   # New medical screening.  This creates a new medical form allowing the
   # jailor to perform a medical screening for the booking record identified
   # by the booking_id parameter.
   def medical
      require_permission Perm[:update_booking]
      @help_page = ["Booking","medical"]
      params[:booking_id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:booking_id]) or raise_not_found "Booking ID #{params[:booking_id]}"
      @medical = Medical.new(:created_by => session[:user], :updated_by => session[:user], :officer => session[:user_name], :officer_unit => session[:user_unit], :booking_id => @booking.id) or raise_db(:new, "Medical Screening")
      @medical.screen_date, @medical.screen_time = get_date_and_time
      @page_title = "New Medical Screening"
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to @booking and return
         end
         @medical.attributes = params[:medical]
         if @medical.save
            @booking.update_attribute(:updated_by, session[:user])
            redirect_to @booking
            user_action("Medical", "Added Medical Screening for Booking ID: #{@booking.id} (#{show_person @booking.person}).")
         end
      end
   end
   
   # This prints a PDF medical screening report from the last screening
   # done for the booking record identified by the booking_id parameter.
   def medical_form
      require_permission Perm[:view_booking]
      params[:booking_id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:booking_id]) or raise_not_found "Booking ID #{params[:booking_id]}"
      if @booking.medicals.empty?
         flash[:warning] = "No Medical Screening Found For This Booking"
         redirect_to @booking and return
      end
      @medical = @booking.medicals.last
      @page_title = "Medical Screening Report"
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Screening-#{@medical.id}.pdf"
      render :template => 'bookings/medical_form.pdf.prawn', :layout => false
      user_action("Medical", "Printed Medical Screening for Booking ID: #{@booking.id} (#{show_person @booking.person}).")
   end
   
   # show photos of selected (filtered) inmates
   def photo_album
      require_permission Perm[:view_booking]
      if session[:show_all_bookings].nil?
         session[:show_all_bookings] = false
      end
      @query = session[:booking_filter] ||= HashWithIndifferentAccess.new
      cond = nil
      unless @query.nil? || @query.empty? || Booking.condition_string(@query).blank?
         if session[:show_all_bookings] == false
            @query.delete('release_date_from')
            @query.delete('release_date_to')
         end
         cond = Booking.condition_string(@query)
         if session[:show_all_bookings] == false
            cond << " AND release_date IS NULL"
         end
      else
         if session[:show_all_bookings] == false
            cond = "release_date IS NULL"
         end
      end
      if cond.nil?
         cond = ""
      else
         cond << " AND "
      end
      cond << "people.front_mugshot_file_name != ''"
      @bookings = Booking.find(:all, :include => [:person, :facility, :cell, {:holds => [:doc_record, :hold_type, :blockers, :blocks, :temp_release_reason]}], :order => 'people.lastname, people.firstname, people.middlename, people.suffix', :conditions => cond)
      if @bookings.empty?
         flash[:warning] = "No Inmate Photos Available!"
         redirect_to bookings_path and return
      end
      @page_title = "Photo Album: Inmates"
   end
   
   # This prints a booking list (PDF) using the same query parameters that
   # are in effect for the booking list view.  If no query parameters are
   # saved in the session, the full booking list will be printed.
   def list_form
      require_permission Perm[:view_booking]
      if session[:show_all_bookings].nil?
         session[:show_all_bookings] = false
      end
      @query = session[:booking_filter] ||= HashWithIndifferentAccess.new
      cond = nil
      unless @query.nil? || @query.empty? || Booking.condition_string(@query).blank?
         if session[:show_all_bookings] == false
            @query.delete('release_date_from')
            @query.delete('release_date_to')
         end
         cond = Booking.condition_string(@query)
         if session[:show_all_bookings] == false
            cond << " AND release_date IS NULL"
         end
      else
         if session[:show_all_bookings] == false
            cond = "release_date IS NULL"
         end
      end
      @bookings = Booking.find(:all, :include => [:person, :facility, :cell, {:holds => [:doc_record, :hold_type, :blockers, :blocks, :temp_release_reason]}], :order => 'people.lastname, people.firstname, people.middlename, people.suffix', :conditions => cond)
      if @bookings.empty?
         flash[:warning] = "No Bookings selected for report!"
         redirect_to bookings_path and return
      end
      @page_title = "Booking List"
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Booking-List.pdf"
      render :template => 'bookings/list_form.pdf.prawn', :layout => false
      user_action("Booking","Printed Booking List.")
   end
   
   # This prints a booking list (PDF) for the selected facility.  Referred
   # to as the monthly in-jail report (for the DA), this prints on a landscape
   # legal sheet. This report excludes inmates serving sentence and temporary
   # transfers.
   def in_jail
      require_permission Perm[:view_booking]
      if params[:facility_id]
         @facility = Option.find_by_id(params[:facility_id])
      else
         # allow nil to print for all facilities
         @facility = nil
      end
      if @facility.nil?
         @bookings = Booking.find(:all, :include => :person, :order => 'people.lastname, people.firstname, people.middlename, bookings.id', :conditions => 'release_date is null')
      else
         @bookings = Booking.find(:all, :include => :person, :order => 'people.lastname, people.firstname, people.middlename, bookings.id', :conditions => ['release_date is null and facility_id = ?', @facility.id])
      end
      @report_date = Date.today
      prawnto :prawn => {
         :page_size => 'LEGAL',
         :page_layout => :landscape,
         :left_margin => 20,
         :right_margin => 20,
         :top_margin => 10,
         :bottom_margin => 10 },
         :filename => "In-Jail_Report-#{format_date(@report_date, :timestamp => true)}.pdf"
      render :template => 'bookings/in_jail.pdf.prawn', :layout => false
      user_action("Booking","Printed In-Jail Report.")
   end
   
   # generates Social Security Administration monthly report
   def ssa_report
      require_permission Perm[:view_booking]
      params[:ssa_date] or raise_missing "Report Date"
      report_date = valid_date(params[:ssa_date]) or raise_invalid "Report Date", "'#{params[:ssa_date]}' Is Not A Valid Date"
      report_date <= Date.today or raise_invalid "Report Date", "Cannot Be In The Future"
      @start_date = (report_date - 1.month).beginning_of_month
      @stop_date = (report_date - 1.month).end_of_month
      @bookings = Booking.find(:all, :include => :person, :order => 'people.lastname, people.firstname, people.middlename, bookings.booking_date', :conditions => ['booking_date >= ? AND booking_date <= ? AND (release_date IS NULL OR release_date > ?)',@start_date,@stop_date,@stop_date])
      @timestamp = DateTime.now.to_s(:timestamp)
      prawnto :prawn => {
         :page_size => 'LETTER',
         :page_layout => :portrait,
         :left_margin => 20,
         :right_margin => 20,
         :top_margin => 10,
         :bottom_margin => 10 },
         :filename => "SSA_Report-#{@timestamp}.pdf"
      render :template => 'bookings/ssa_report.pdf.prawn', :layout => false
      user_action("Booking","Printed SSA Report.")
   end
   
   # generates Fraud and Child Support monthly report
   def fcs_report
      require_permission Perm[:view_booking]
      params[:fcs_date] or raise_missing "Report Date"
      report_date = valid_date(params[:fcs_date]) or raise_invalid "Report Date", "'#{params[:fcs_date]}' Is Not A Valid Date"
      report_date <= Date.today or raise_invalid "Report Date", "Cannot Be In The Future"
      @start_date = (report_date - 1.month).beginning_of_month
      @stop_date = (report_date - 1.month).end_of_month
      @bookings = Booking.find(:all, :include => :person, :order => 'people.lastname, people.firstname, people.middlename, bookings.id', :conditions => ['booking_date >= ? AND booking_date <= ?',@start_date,@stop_date])
      @timestamp = DateTime.now.to_s(:timestamp)
      prawnto :prawn => {
         :page_size => 'LETTER',
         :page_layout => :landscape,
         :left_margin => 20,
         :right_margin => 20,
         :top_margin => 10,
         :bottom_margin => 10 },
         :filename => "FCS_Report-#{@timestamp}.pdf"
      render :template => 'bookings/fcs_report.pdf.prawn', :layout => false
      user_action("Booking","Printed FCS Report.")
   end
   
   # This prints an arraignment notice
   def arraignment_notice
      require_permission Perm[:view_booking]
      params[:booking_id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:booking_id]) or raise_not_found "Booking ID #{params[:booking_id]}"
      if @booking.person.nil?
         flash[:warning] = "Booking Has Invalid Person"
         redirect_to @booking and return
      end
      holds = @booking.holds.reject{|h| h.hold_type.nil? || h.hold_type.long_name != 'Bondable'}
      # TODO: Figure out what to do if @holds is more than one...
      # Do we need multiple arraignment notices?
      # Do we only print arraignment notices for bondable holds with active bonds?
      # What about bondable holds cleared w/o bond?
      # What about bondable holds still open?
      # If one notice, do we clump charges?  What if unrelated?  How do I tell if unrelated?
      #
      # Until I find the answers to these questions, I'm going to group together all bondable
      # holds into one arraignment notice
      @arrests = holds.collect{|h| h.arrest}.compact.uniq
      @charges = @arrests.collect{|a| a.district_charge_summary == 'No Charges' ? nil : a.district_charge_summary}.compact.join(', ')
      prawnto :prawn => {
         :page_size => 'LEGAL',
         :left_margin => 20,
         :right_margin => 20,
         :top_margin => 10,
         :bottom_margin => 10 },
         :filename => "Arraignment_Notice-#{@booking.person.id}.pdf"
      render :template => 'bookings/arraignment_notice.pdf.prawn', :layout => false
      user_action("Booking","Printed Arraignment notice for #{show_person @booking.person}.")
   end
   
   # Nightly Billing Headcount Report
   # This prints the nightly billing headcount report for the specified facility.
   def billing_headcount
      require_permission Perm[:view_booking]
      if params[:facility_id]
         @facility = Option.find_by_id(params[:facility_id])
      else
         @facility = nil
      end
      params[:report_date] or raise_missing "Report Date"
      @report_date = valid_date(params[:report_date]) or raise_invalid "Report Date", "'#{params[:report_date]}' Is Not A Valid Date"
      @report_date < Date.today or raise_invalid "Report Date", "Must Be In The Past"
      btypes = ["Temporary Release","DOC","DOC Billable","City","Parish","Other","Not Billable"]
      @data = {}
      btypes.each do |t|
         @data[t] = []
      end
      @data['Temporary Release'], @data['DOC'], @data['DOC Billable'], @data['City'], @data['Parish'], @data['Other'], @data['Not Billable'] = Booking.headcount(@report_date, "ALL",@facility)
      @report_datetime = get_datetime(@report_date, "23:59")
      @data['Transfers'] = Transfer.find(:all, :include => :booking, :conditions => ['transfer_date = ? AND bookings.facility_id = ?', @report_date, @facility])
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Nightly_Headcount#{@facility.nil? ? '' : '-' + @facility.short_name}-#{format_date(@report_date, :timestamp => true)}.pdf"
      render :template => 'bookings/billing_headcount.pdf.prawn', :layout => false
      user_action("Booking","Printed Nightly Headcount Report (#{@facility.nil? ? 'All Facilities' : @facility.short_name}) for #{format_date(@report_date)}.")
   end
   
   # Monthly Billing Headcount Report
   # This prints the monthly billing headcount report for the specified facility.
   def month_billing
      require_permission Perm[:view_booking]
      if params[:facility_id]
         @facility = Option.find_by_id(params[:facility_id])
      else
         @facility = nil
      end
      params[:month] or raise_missing "Report Month"
      params[:year] or raise_missing "Report Year"
      @start_date = valid_date("#{params[:month]}/01/#{params[:year]}") or raise_invalid "Report Month/Year", "'#{params[:month]}/#{params[:year]}' Is Not A Valid Month/Year"
      @start_date < Date.today or raise_invalid "Report Month/Year", "Cannot Be In The Future"
      if @start_date.end_of_month > Date.yesterday
         @stop_date = Date.yesterday
      else
         @stop_date = @start_date.end_of_month
      end
      @data = JailBilling::MonthlyReport::TableData.new(@facility,@start_date,@stop_date)
      raise DatabaseError.new("Unable To Get Monthly Billing Report Data") if @data.nil?
      prawnto :prawn => {
         :page_size => 'LETTER',
         :page_layout => :landscape,
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20,
         :skip_page_creation => true},
         :filename => "Month_Billing#{@facility.nil? ? '' : '-' + @facility.short_name}-#{format_date(@stop_date, :timestamp => true)}.pdf"
      render :template => 'bookings/month_billing.pdf.prawn', :layout => false
      user_action("Booking","Printed Month Billing Headcount Report (#{@facility.nil? ? 'All Facilities' : @facility.short_name}) for #{format_date(@report_datetime)}.")
   end
   
   # prints booking time log
   def time_log
      require_permission Perm[:view_booking]
      params[:id] or raise_missing "Booking ID"
      @booking = Booking.find_by_id(params[:id]) or raise_not_found "Booking ID #{params[:id]}"
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Time_Log-#{@booking.id}.pdf"
      render :template => 'bookings/time_log.pdf.prawn', :layout => false
      user_action("Booking","Printed Time Log for Booking ID: #{@booking.id}.")
   end
   
   # prints booking record problem list
   def problem_list
      require_permission Perm[:view_booking]
      @needs_attention = Booking.needs_attention
      if @needs_attention.empty?
         flash[:warning] = 'No Problems To Report'
         redirect_to bookings_path and return
      end
      @report_time = DateTime.now
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Problem_list-#{format_date(@report_time, :timestamp => true)}.pdf"
      render :template => 'bookings/problem_list.pdf.prawn', :layout => false
      user_action("Booking","Printed Problem List for Booking.")
   end
   
   
   # This is called by various views using a field observer and AJAX call to
   # validate that a booking id entered on a form is valid. It inserts the
   # validated name of the booked person (linked to the booking record if the
   # current user has View Booking permission) into the form field identified
   # by the update parameter.
   def validate_id
      if (field = params[:update]) && params[:i]
         if booking = Booking.find_by_id(params[:i])
            if has_permission Perm[:view_booking]
               name = booking.person.full_name
               p = %(<a href='#{booking_path(booking)}'>#{name}</a>)
            else
               p = booking.person.full_name
            end
         else
            p = "INVALID"
         end
         render :update do |page|
            page.replace_html field, p
         end
         return
      end
      render :text => "INVALID"
   end
   
   # This is called by various views (by means of a "lookup" link) to allow
   # searching of booking records by name. A popup window populated with the
   # search results is generated and clicking the correct record will insert
   # the booking id and prisoner name into the appropriate fields using
   # JavaScript.
   def lookup
      # note: at this time, this only works for current (open) booking records
      if (name = params[:name]) && has_permission(Perm[:view_booking])
         @bookings = Booking.locate_by_name(params[:name])
         @page_title = "Results for '#{name}'"
         @caller_id = params[:caller_id]
         @caller_name = params[:caller_name]
         render :layout => 'popup'
      else
         render :text => ''
      end
   end
   
   # doc inmate index
   def index_doc
      require_permission Perm[:view_booking]
      @help_page = ["Booking","index_doc"]
      @current_tab = 'DOC Inmates'
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:doc_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:doc_filter] = nil
            redirect_to docs_path and return
         end
      else
         @query = session[:doc_filter] || HashWithIndifferentAccess.new
      end
      unless @docs = paged('doc', :include => [:person, :hold], :order => 'people.lastname, people.firstname, people.middlename, docs.id', :active => 'holds.cleared_date IS NULL')
         @query = HashWithIndifferentAccess.new
         session[:doc_filter] = nil
         redirect_to docs_path and return
      end
      @links = []
      if session[:show_all_docs]
         @links.push(['Show Active', docs_path(:show_all => '0')])
      else
         @links.push(['Show All', docs_path(:show_all => '1')])
      end
   end
   
   # Show Doc
   def show_doc
      require_permission Perm[:view_booking]
      @help_page = ["Booking","show_doc"]
      @current_tab = 'DOC Inmates'
      params[:id] or raise_missing "DOC ID"
      @doc = Doc.find_by_id(params[:id]) or raise_not_found "DOC ID #{params[:id]}"
      if session[:show_all_docs].nil?
         session[:show_all_docs] = false
      end
      @page_title = "DOC ID: #{@doc.id}"
      @links = []
      unless @doc.transfer.nil? || @doc.transfer.booking.nil?
         @links.push(["Booking", booking_path(:id => @doc.transfer.booking_id)])
      end
      if has_permission Perm[:update_booking]
         @links.push(["Edit", edit_doc_path(:id => @doc.id)])
      end
      if has_permission Perm[:destroy_booking]
         @links.push(['Delete', @doc, {:method => 'delete', :confirm => 'Are you sure you want to destroy this DOC?'}])
      end
      @page_links = [@doc.person, @doc.booking, @doc.transfer, @doc.sentence, @doc.revocation, @doc.hold,[@doc.creator,'creator'],[@doc.updater,'updater']]
   end
   
   # Add or Edit DOC
   def edit_doc
      require_permission Perm[:update_booking]
      @help_page = ["Booking","edit_doc"]
      @current_tab = 'DOC Inmates'
      if params[:id]
         @doc = Doc.find_by_id(params[:id]) or raise_not_found "DOC ID #{params[:id]}"
         @page_title = "Edit DOC ID: #{@doc.id}"
         @doc.updated_by = session[:user]
      else
         # the only thing that creates new DOC records using this action is
         # a transfer_in (DOC)
         params[:transfer_id] or raise_missing "Transfer ID"
         @transfer = Transfer.find_by_id(params[:transfer_id]) or raise_not_found "Transfer ID #{params[:transfer_id]}"
         @page_title = "New DOC"
         @doc = Doc.new(:created_by => session[:user], :updated_by => session[:user], :transfer_id => @transfer.id) or raise_db(:new, "DOC")
         @doc.transfer_date = @transfer.transfer_date
         @doc.transfer_time = @transfer.transfer_time
         @doc.booking_id = @transfer.booking_id
         @doc.person_id = @transfer.person_id
         unless @doc.person.nil? || !@doc.person.doc_number?
            @doc.doc_number = @person.doc_number
         end
      end
      @page_links = [@doc.person, @doc.booking, @doc.transfer, @doc.sentence, @doc.revocation, @doc.hold]
      if request.post?
         if params[:commit] == "Cancel"
            if @doc.new_record?
               redirect_to @transfer
            else
               redirect_to @doc
            end
            return
         end
         @doc.attributes = params[:doc]
         if @doc.save
            redirect_to @doc
            if params[:id]
               user_action("Booking","Edited DOC ID: #{@doc.id} (#{show_person @doc.person}).")
            else
               user_action("Booking", "Added DOC ID: #{@doc.id} (#{show_person @doc.person}).")
            end
         end
      end
   end

   # Destroys the specified doc record. Only responds to DELETE methods.
   def destroy_doc
      require_permission Perm[:destroy_booking]
      require_method :delete
      params[:id] or raise_missing "DOC ID"
      @doc = Doc.find_by_id(params[:id]) or raise_not_found "DOC ID #{params[:id]}"
      @doc.destroy or raise_db(:destroy, "DOC ID #{params[:id]}")
      user_action("Booking","Destroyed DOC ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to docs_path
   end
   
   protected
   
   # Assures that the "Booking" menu item is highlighted for all actions in
   # this controller.
   def set_tab_name
      @current_tab = "Booking"
   end
end
