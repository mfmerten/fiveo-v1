# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Bugs - otherwise known as trouble tickets. These are entered when a user
# has problems with the system.  The bug reporter, the bug assignee, the
# bug admin and all bug watchers are notified of changes to the bug using
# the user messages system (See MessagesController).
class BugsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Bug Listing
   def index
      require_permission Perm[:view_bug]
      @help_page = ["Bug Reports","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:bug_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:bug_filter] = nil
            redirect_to bugs_path and return
         end
      else
         @query = session[:bug_filter] || HashWithIndifferentAccess.new
      end
      resolved = Option.lookup("Bug Status","Resolved",nil,nil,true)
      unless @bugs = paged('bug', :order => 'bugs.id DESC', :include => [:assigned, :priority, :status, :resolution], :active => "bugs.status_id != #{resolved}", :query_skip => 'status_id')
         @query = HashWithIndifferentAccess.new
         session[:bug_filter] = nil
         redirect_to bugs_path and return
      end
      @links = []
      if session[:show_all_bugs]
         @links.push(['Show Active', bugs_path(:show_all => '0')])
      else
         @links.push(['Show All', bugs_path(:show_all => '1')])
      end
      if has_permission Perm[:update_bug]
         @links.push(['New', edit_bug_path])
      end
      if current_user.review_bug?
         @links.push(["Review", url_for(:action => 'review')])
      end
   end

   # Show Bug
   def show
      require_permission Perm[:view_bug]
      @help_page = ["Bug Reports","show"]
      params[:id] or raise_missing "Bug ID"
      @bug = Bug.find_by_id(params[:id]) or raise_not_found "Bug ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_bug]
         @links.push(["New", edit_bug_path])
         @links.push(["Edit", edit_bug_path(:id => @bug.id)])
      end
      if has_permission Perm[:destroy_bug]
         @links.push(['Delete', @bug, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Bug Report?'}])
      end
      if has_permission Perm[:update_bug]
         @links.push(["Comment", comment_bug_path(:bug_id => @bug.id)])
      end
      @links.push([(@bug.watchers.include?(current_user) ? "Unwatch" : "Watch"), watch_bug_path(:id => @bug.id)])
      @linksx = []
      if has_permission Perm[:update_bug]
         if @bug.status.long_name == "Resolved" && (@bug.reporter_id == session[:user] || @bug.admin_id == session[:user])
            @linksx.push(["Reopen", url_for(:action => 'reopen', :id => @bug.id), {:method => 'post'}])
         end
         unless @bug.resolution_date?
            if @bug.reporter_id == session[:user] || @bug.admin_id == session[:user] || @bug.assigned_id == session[:user]
               unless @bug.status.long_name == "Hold"
                  @linksx.push(["Hold", url_for(:action => 'status', :id => @bug.id, :bug_status => 'hold'), {:method => 'post'}])
               end
               if @bug.assigned_id == session[:user]
                  unless @bug.status.long_name == "Working On"
                     @linksx.push(["Working On", url_for(:action => 'status', :id => @bug.id, :bug_status => 'working on'), {:method => 'post'}])
                  end
               end
            end
            if @bug.assigned_id == session[:user]
               @linksx.push(["Fixed", url_for(:action => 'resolve', :id => @bug.id, :bug_resolution => 'fixed'), {:method => 'post'}])
               @linksx.push(["Answered", url_for(:action => 'resolve', :id => @bug.id, :bug_resolution => 'answered'), {:method => 'post'}])
               @linksx.push(["Duplicate", url_for(:action => 'resolve', :id => @bug.id, :bug_resolution => 'duplicate bug'), {:method => 'post'}])
            end
         end
      end
      @page_links = [[@bug.reporter, 'reporter'],[@bug.admin, 'admin'],[@bug.assigned, 'assigned'],[@bug.creator, 'creator'],[@bug.updater, 'updater']]
   end

   # Add or Edit Bug
   def edit
      require_permission Perm[:update_bug]
      @help_page = ["Bug Reports","edit"]
      if params[:id]
         @bug = Bug.find_by_id(params[:id]) or raise_not_found "Bug ID #{params[:id]}"
         @page_title = "Edit Bug Report: " + @bug.id.to_s
         @bug.updated_by = session[:user]
         unless request.post?
            if @bug.watchers.empty?
               @bug.watchers.build
            end
         end
      else
         @page_title = "New Bug Report"
         admin = SiteConfig.bug_admin_id
         unless assignto = User.find_by_login("mfmerten")
            assignto = User.find_by_login("admin")
         end
         stat = Option.lookup("Bug Status","New")
         pri = Option.lookup("Bug Priority","3 - Normal")
         @bug = Bug.new(:created_by => session[:user], :updated_by => session[:user], :reporter_id => session[:user], :admin_id => admin, :assigned_id => assignto.id, :status_id => stat, :priority_id => pri) or raise_db(:new, "Bug")
         @bug.summary = params[:summary]
         @bug.details = "#{request.user_agent}\n#{'-' * 50}\n"
         @bug.reported_date, @bug.reported_time = get_date_and_time
         unless request.post?
            @bug.watchers.build
         end
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @bug.new_record?
               redirect_to bugs_path
            else
               redirect_to @bug
            end
            return
         end
         params[:bug][:assigned_watchers] ||= {}
         @bug.attributes = params[:bug]
         if @bug.save
            redirect_to @bug
            if params[:id]
               user_action("Bug Report","Edited Bug Report ID: #{@bug.id}.")
            else
               user_action("Bug Report", "Added Bug Report ID: #{@bug.id}.")
            end
         end
      end
   end
   
   # Add Bug Comment
   def comment
      require_permission Perm[:update_bug]
      @help_page = ["Bug Reports","comment"]
      params[:bug_id] or raise_missing "Bug ID"
      @bug = Bug.find_by_id(params[:bug_id]) or raise_not_found "Bug ID #{params[:bug_id]}"
      @page_title = "New Bug Comment"
      @bug_comment = BugComment.new(:created_by => session[:user], :updated_by => session[:user], :bug_id => @bug.id, :user_id => session[:user]) or raise_db(:new, "Bug Comment")
      @bug_comment.comments = params[:leadin]
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to @bug
            return
         end
         @bug_comment.attributes = params[:bug_comment]
         if params[:commit] == "Preview"
            @preview = @bug_comment.comments
            render :action => "comment"
         else
            if @bug_comment.save
               @bug.updated_by = session[:user]
               @bug.without_messages = true
               @bug.save(false)
               redirect_to @bug
               user_action("Bug Report","Added Comment to Bug Report ID: #{@bug.id}.")
            end
         end
      end
   end
   
   # Watch/Unwatch a Bug
   def watch
      require_permission Perm[:view_bug]
      params[:id] or raise_missing "Bug ID" 
      @bug = Bug.find_by_id(params[:id]) or raise_not_found "Bug ID #{params[:id]}"
      if @bug.watchers.include?(current_user)
         @bug.watchers.delete(current_user)
      else
         @bug.watchers << current_user
      end
      redirect_to @bug
   end

   # Destroys the specified bug report. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_bug]
      require_method :delete
      params[:id] or raise_missing "Bug ID"
      @bug = Bug.find_by_id(params[:id]) or raise_not_found "Bug ID #{params[:id]}"
      @bug.destroy or raise_db(:destroy, "Bug ID #{params[:id]}")
      user_action("Bug Report","Destroyed Bug Report ID: #{params[:id]}!")
      flash[:notice] = 'Record Deleted!'
      redirect_to bugs_path
   end
   
   # Reopens the bug specified by id. This changes status to reopened,
   # changes resolution to not satisfied and clears the resolution_date and
   # resolution_time.  Only the bug reporter and bug supervisor will see this
   # button, and then only for resolved bugs. When clicked and the status is
   # changed, the user is redirected to the new bug comment form for entry.
   def reopen
      require_permission Perm[:update_bug]
      require_method :post
      params[:id] or raise_missing "Bug ID"
      @bug = Bug.find_by_id(params[:id]) or raise_not_found "Bug ID #{params[:id]}"
      unless @bug.resolution_date?
         flash[:warning] = "Bug ID #{params[:id]} Is Not Closed"
         redirect_to @bug and return
      end
      @bug.reporter_id == session[:user] || @bug.admin_id == session[:user] or raise_not_allowed "Reopen This Bug", "Bug Reporter", "Bug Administrator"
      @bug.status_id = Option.lookup("Bug Status", 'Reopened')
      @bug.resolution_id = nil
      @bug.resolution_date = nil
      @bug.resolution_time = nil
      @bug.updated_by = session[:user]
      @bug.without_messages = true
      @bug.save(false)
      redirect_to url_for(:action => 'comment', :bug_id => @bug.id, :leadin => "*I'm reopening this bug because:* ")
      user_action("Bug Report","Reopened Bug Report ID: #{@bug.id}.")
   end
   
   # Closes the bug specified by id. This changes status to Resolved,
   # changes resolution to the value set by the bug_resolution parameter
   # and sets the resolution_date and resolution_time.  Only the bug assignee
   # will see this button, and then only for open bugs. When clicked and the
   # status is changed, the user is redirected to the new bug comment form for
   # entry.
   def resolve
      require_permission Perm[:update_bug]
      require_method :post
      params[:id] or raise_missing "Bug ID"
      @bug = Bug.find_by_id(params[:id]) or raise_not_found "Bug ID #{params[:id]}"
      @bug.assigned_id == session[:user] or raise_not_allowed "Resolve This Bug", "Bug Assignees"
      params[:bug_resolution] or raise_missing "Bug Resolution"
      @bug.resolution_id = Option.lookup("Bug Resolution", params[:bug_resolution].titleize,nil,nil,true) or raise_not_found "Bug Resolution '#{params[:bug_resolution].titleize}'"
      if @bug.resolution_date?
         flash[:warning] = "Bug ID #{params[:id]} Is Already Closed"
         redirect_to @bug and return
      end
      @bug.status_id = Option.lookup("Bug Status","Resolved",nil,nil,true)
      @bug.updated_by = session[:user]
      @bug.without_messages = true
      @bug.resolution_date, @bug.resolution_time = get_date_and_time
      @bug.save(false)
      redirect_to url_for(:action => 'comment', :bug_id => @bug.id, :leadin => "*I'm changing this bug to Resolved: #{params[:bug_resolution].titleize} because:* ")
      user_action("Bug Report","Marked Bug Report ID: #{@bug.id} as Resolved: #{params[:bug_resolution].titleize}.")
   end
   
   # Changes the bug status to something other than "Resolved" or "Reopened",
   # makes sure resolution and resolution date/time are clear. Only the bug
   # assignee, the bug reporter or the bug supervisor can perform this action,
   # and only on open bugs.
   def status
      require_permission Perm[:update_bug]
      require_method :post
      params[:id] or raise_missing "Bug ID"
      @bug = Bug.find_by_id(params[:id]) or raise_not_found "Bug ID #{params[:id]}"
      @bug.assigned_id == session[:user] || @bug.reporter_id == session[:user] || @bug.admin_id == session[:user] or raise_not_allowed "Set Bug Status", "Bug Assignee", "Bug Reporter", "Bug Supervisor"
      params[:bug_status] or raise_missing "Bug Status"
      @bug.status_id = Option.lookup("Bug status", params[:bug_status].titleize,nil,nil,true) or raise_not_found "Bug Status '#{params[:bug_status]}'"
      if @bug.resolution_date?
         flash[:warning] = "Bug ID #{params[:id]} Is Already Closed"
         redirect_to @bug and return
      end
      @bug.resolution_id = nil
      @bug.resolution_date = nil
      @bug.resolution_time = nil
      @bug.updated_by = session[:user]
      @bug.without_messages = true
      @bug.save(false)
      redirect_to url_for(:action => 'comment', :bug_id => @bug.id, :leadin => "*I'm changing this bug status to #{params[:bug_status].titleize} because:* ")
      user_action("Bug Report","Marked Bug Report ID: #{@bug.id} as Status: #{params[:bug_status].titleize}.")
   end
   
   # Review latest bugs
   def review
      require_permission Perm[:view_bug]
      @page_title = "Bug Reports Review"
      @bugs = review_paged('bug',:include => [:priority, :assigned, :reporter, :admin, :status, :resolution])
      @links = [["View Next Page", url_for(:action => 'review')]]
      @links.push(["Skip All",url_for(:action => 'skip_all')])
   end

   # Skip all bug reviews
   def skip_all
      require_permission Perm[:view_bug]
      review_skip_all('bug')
      flash[:notice] = "All bug reviews skipped!"
      redirect_to root_path
      return
   end
   
   protected

   # ensures that the "Bug Reports" menu item is highlighted for all actions in
   # this controller
   def set_tab_name
      @current_tab = "Bug Reports"
   end
end
