# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Filters and methods that apply globally to all controllers.
# This also defines which view helper methods are exported for access in
# controllers.
require 'socket'

class ApplicationController < ActionController::Base
   include ExceptionNotification::Notifiable

   before_filter :ssl_required
   before_filter :login_required, :except => ['login_required','login']
   before_filter :load_config

   filter_parameter_logging :password, :user, :ssn
   
   helper_method :user_action, :logged_in, :popup_options, :current_user, :has_permission, :show_person, :local_ip, :check_privacy, :big_zero
   helper :all # include all helpers, all the time
   
   # this is uncommented whenever I wish to look at the actual error pages
   # produced on production machines.
   # alias_method :rescue_action_locally, :rescue_action_in_public
   
   # this picks out 404 error codes and displays a special template
   def render_optional_error_file(status_code)
      if status_code == :not_found
         render_404
      else
         super
      end
   end
   
   def render_404
      load_config
      respond_to do |type|
         type.html { render :template => "error_codes/error_404", :layout => 'application', :status => 404 } 
         type.all  { render :nothing => true, :status => 404 }
      end
      true
   end
   
   protected
   
   ####################
   # Custom Error Section
   ####################
   
   class InvalidParameterError < StandardError; end
   class MissingParameterError < StandardError; end
   class DoesNotExistError < StandardError; end
   class DatabaseError < StandardError; end
   class PermissionDeniedError < StandardError; end
   class InvalidMethodError < StandardError; end
   
   rescue_from ActionController::InvalidAuthenticityToken, InvalidParameterError, MissingParameterError, DoesNotExistError, DatabaseError, PermissionDeniedError, InvalidMethodError, :with => :custom_application_error
   
   # this is called for any of the above specified exceptions
   def custom_application_error(e)
      load_config
      @message = e.message
      @type = e.class.name.demodulize
      logger.info("\nApplication Error: #{@type}: #{@message} (UID #{session[:user]})\n\n")
      render :template => 'error_codes/custom_application_error.html.erb', :layout => 'application'
   end
   
   # shortcut for DoesNotExistError
   def raise_not_found(txt=nil)
      if txt.blank? || !txt.is_a?(String)
         txt = "Record"
      end
      raise DoesNotExistError.new("#{txt} Not Found")
   end
   
   # shortcut for MissingParameterError
   def raise_missing(txt=nil)
      if txt.blank? || !txt.is_a?(String)
         txt = "Parameter"
      end
      raise MissingParameterError.new("#{txt} Not Specified")
   end
   
   # shortcut for InvalidParameterError
   def raise_invalid(txt=nil,valid_values=nil)
      if txt.blank? || !txt.is_a?(String)
         txt = "Parameter"
      end
      valid_values = nil unless (valid_values.is_a?(Array) && !valid_values.empty?) || valid_values.is_a?(String)
      valid_string = ""
      unless valid_values.nil?
         if valid_values.is_a?(String)
            valid_string = ": #{valid_values}"
         else
            valid_string = ": must be one of #{valid_values.join(', ').sub(/,\s([^,]*)$/,' or \1')}"
         end
      end
      raise InvalidParameterError.new("Invalid #{txt} Specified#{valid_string}")
   end
   
   # shortcut for PermissionDeniedError
   def raise_not_allowed(txt=nil,*allowed_array)
      if txt.blank? || !txt.is_a?(String)
         txt = "Perform This Action"
      end
      if allowed_array.is_a?(Array) && allowed_array[0].is_a?(Array)
         # remove enclosing array
         allowed_array = allowed_array[0]
      end
      allowed_array = nil unless allowed_array.is_a?(Array) && !allowed_array.empty?
      allowed_string = ""
      unless allowed_array.nil?
         allowed_string = ": must be #{allowed_array.join(', ').sub(/,\s([^,]*)$/,' or \1')}"
      end
      raise PermissionDeniedError.new("You are not allowed to #{txt}#{allowed_string}")
   end
   
   # shortcut for DatabaseError
   def raise_db(op_type,txt=nil)
      valid_values = [:new, :destroy, :create, :build, :save]
      valid_string = valid_values.collect{|v| v.to_s}.join(', ').sub(/,\s([^,]*)$/,' or \1')
      if op_type.blank? || (!op_type.is_a?(String) && !op_type.is_a?(Symbol)) || !valid_values.include?(op_type)
         raise InvalidParameterError.new("assert_db_error: #{op_type.inspect} is not a valid database operation type. Must be one of #{valid_string}")
      end
      if txt.blank? || !txt.is_a?(String)
         txt = "Record"
      end
      raise DatabaseError.new("Database Operation #{op_type.to_s.capitalize} #{txt} Failed")
   end
   
   ####################
   
   prawnto :prawn => {:left_margin => 48, :right_margin => 48, :top_margin => 0, :bottom_margin => 80}, :inline => false
   
   def local_ip
     orig, Socket.do_not_reverse_lookup = Socket.do_not_reverse_lookup, true  # turn off reverse DNS resolution temporarily
     UDPSocket.open do |s|
       s.connect '64.233.187.99', 1
       s.addr.last
     end
   ensure
     Socket.do_not_reverse_lookup = orig
   end
   
   # displays person information in a standardized way.  Handles nil gracefully.
   def show_person(obj, *opts)
      # check for options
      options = HashWithIndifferentAccess.new(:include_id => true, :invalid => 'Unknown')
      options.update(opts.pop) if opts.last.is_a?(Hash)
      return options[:invalid] if obj.nil?
      obj.is_a?(Person) or raise InvalidParameterError.new("show_person: expected Person, got #{obj.class.name}")
      if options[:include_id] == true
         return "[#{obj.id}] #{obj.full_name}"
      else
         return obj.full_name
      end
   end
   
   # For production environments only, this redirects all http urls to https
   # to enforce ssl for all connections:
   def ssl_required
      if ENV['RAILS_ENV'] == 'production'
         redirect_to :protocol => "https://" unless (request.ssl? or local_request?)
      end
   end
   
   # See ActionController::RequestForgeryProtection for details
   # Uncomment the :secret if you're not using the cookie session store
   protect_from_forgery
   
   # this writes a user action to the action log (and environment log)
   def user_action(section="",description="")
      section.is_a?(String) && !section.blank? or raise InvalidParameterError.new("user_action: section name (arg1) is required and must be a string")
      description.is_a?(String) && !description.blank? or raise InvalidParameterError.new("user_action: description (arg2) is required and must be a string")
      Activity.create(:section => section, :description => description, :user_id => session[:user])
      logger.info("User Activity: #{DateTime.now.to_s(:us)} #{section}: #{session[:user]} => #{description}")
   end
   
   # defines options for various popup windows
   def popup_options(name="", width=640, height=480)
      name = "" unless name.is_a?(String)
      width = 640 unless width.is_a?(Integer)
      height = 480 unless height.is_a?(Integer)
      ["#{name}", "status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=1,scrollbars=1,height=#{height},width=#{width}"]
   end
   
   # Accepts one or more permissions and returns true if the current user
   # has any of them, either directly or through groups and roles. Permissions
   # are specified by name as stored in the Perm model. Admins will automatically
   # test true for any permission, unless one of the perms supplied is 'noadmin'
   # in which case the Admin will need to have one of the other specified perms
   # directly assigned to their account.
   # TODO: rewrite this so that DB access not required
   def has_permission(*perms)
      permissions = [perms].flatten
      if permissions.include?("noadmin")
         permissions.delete("noadmin")
      else
         return true if session[:perms].include?(Perm[:admin])
      end
      permissions.each do |p|
         return true if session[:perms].include?(p)
      end
      return false
   end
   
   # Wrapper around has_permission() method
   # Will raise permission denied error exception if check fails
   def require_permission(*permissions)
      return true if has_permission(permissions)
      raise PermissionDeniedError.new("You do not have permission to perform this action. Requires: #{permissions.collect{|p| Perm[p]}.join(', ').sub(/,\s([^,]*)$/,' or \1')}")
   end
   
   # returns true if object is not capable of privacy, or if the record is
   # not private, or if the record is private and the current_user "owns" the
   # record.  Otherwise it returns false.
   def check_privacy(object)
      return true unless object.is_a?(ActiveRecord::Base) && object.respond_to?('private') && (object.respond_to?('owned_by') || object.respond_to?('created_by'))
      return true unless object.private?
      if object.respond_to?('owned_by')
         return true if object.owned_by == session[:user] || (object.owned_by.nil? && object.created_by == session[:user])
      end
      if object.respond_to?('created_by')
         return true if object.created_by == session[:user]
      end
      return false
   end
   
   # uses check_privacy method and if returns false, raises permission denied error
   def enforce_privacy(object)
      return true if check_privacy(object)
      raise PermissionDeniedError.new("You do not have permission to access this private information.")
   end
   
   # returns true if object is owned by current_user or current user has
   # author override permissions.
   def check_author(object)
      return true if has_permission Perm[:author_all]
      return true unless object.is_a?(ActiveRecord::Base) && (object.respond_to?('owned_by') || object.respond_to?('created_by'))
      if object.respond_to?('owned_by')
         return true if object.owned_by == session[:user] || (object.owned_by.nil? && object.created_by == session[:user])
      end
      if object.respond_to?('created_by')
         return true if object.created_by == session[:user]
      end
      return false
   end
   
   # uses check_author method and raises exception if false
   def enforce_author(object)
      return true if check_author(object)
      raise PermissionDeniedError.new("Only the owner of this record can perform that action.")
   end
   
   # accepts method (text or symbol) and raises exception if current request
   # method does not match
   def require_method(req_method)
      unless req_method.is_a?(String) || req_method.is_a?(Symbol)
         raise InvalidParameterError.new("require_method: required argument not string or symbol: #{req_method.inspect}")
      end
      req_method = req_method.to_s
      unless ['get','put','post','delete'].include?(req_method)
         raise InvalidParameterError.new("require_method: invalid method specified: #{req_method}")
      end
      if request.nil?
         raise InvalidParameterError.new("require_method: request is Nil")
      end
      raise InvalidMethodError.new("Invalid Request Method: Requires #{req_method}") unless request.send("#{req_method}?")
      return true
   end
   
   # this performs a paged retrieval of data from a model and initializes a
   # standard set of instance variables from model data - access params hash directly
   # modified 2/16/2010 to recognize alpha parameter
   def paged(model, *opts)
      model.is_a?(String) || model.is_a?(Symbol) or raise InvalidParameterError.new("paged: model name (arg1) is required and must be a string or symbol")
      model = model.to_s
      config = {:page_name => nil, :alpha_name => nil, :order => nil, :include => nil, :select => nil, :active => nil, :query_skip => nil}
      config.update(opts.pop) if opts.last.is_a?(Hash)
      if params[:page]
         @page = params[:page].to_i
      else
         @page = 1
      end
      if !config[:active].nil?
         if session["show_all_#{model.pluralize}".to_sym].nil?
            session["show_all_#{model.pluralize}".to_sym] = false
         end
      else
         session["show_all_#{model.pluralize}".to_sym] = true
      end
      if params[:show_all]
         session["show_all_#{model.pluralize}".to_sym] = (params[:show_all] == "1")
      end
      class_name = model.camelize
      options = {}
      max_items = session[:items_per_page]
      options[:limit] = max_items
      unless config[:include].nil?
         options[:include] = config[:include]
      end
      unless config[:order].nil?
         options[:order] = config[:order]
      end
      unless config[:select].nil?
         options[:select] = "*,#{config[:select]}"
      end
      if !config[:active].nil? && session["show_all_#{model.pluralize}".to_sym] == false
         options[:conditions] = config[:active]
      end
      alpha_page = 0
      if class_name.constantize.respond_to?('column_names') && class_name.constantize.column_names.include?('private')
         if class_name.constantize.column_names.include?('owned_by')
            perms = "(#{model.pluralize}.private != 1 || (#{model.pluralize}.private = 1 AND ( #{model.pluralize}.owned_by = '#{session[:user]}' || (#{model.pluralize}.owned_by IS NULL AND #{model.pluralize}.created_by = '#{session[:user]}'))))"
         elsif class_name.constantize.respond_to?('created_by')
            perms = "(#{model.pluralize}.private != 1 || (#{model.pluralize}.private = 1 AND #{model.pluralize}.created_by = '#{session[:user]}'))"
         end
         if options[:conditions].nil?
            options[:conditions] = perms
         else
            options[:conditions] << " AND #{perms}"
         end
         # otherwise, can't do anything with it, so we ignore private
      end
      if @query.nil? || @query.empty?
         if params[:alpha] && !config[:alpha_name].nil?
            @page = (class_name.constantize.count(:include => options[:include], :conditions => ["#{options[:conditions].nil? ? '' : options[:conditions] + ' AND '}#{config[:alpha_name]} < ?",params[:alpha]]).to_f / max_items.to_f).ceil
         end
         @pages = (class_name.constantize.count(:include => options[:include], :conditions => options[:conditions]).to_f / max_items.to_f).ceil
         if config[:page_name].nil?
            @page_title = "#{class_name.pluralize.titleize} List"
         else
            @page_title = "#{config[:page_name]}"
         end
      else
         if !config[:active].nil? && session["show_all_#{model.pluralize}".to_sym] == false
            if config[:query_skip].is_a?(String)
               @query.delete(config[:query_skip])
            elsif config[:query_skip].is_a?(Array)
               config[:query_skip].each{|s| @query.delete(s)}
            end
         end
         # if @query, but conditions are empty, return nil to cause index to clear filter
         return nil unless cond_string = class_name.constantize.condition_string(@query)
         if options[:conditions].nil?
            options[:conditions] = cond_string
         else
            options[:conditions] << " AND #{cond_string}"
         end
         if params[:alpha] && !config[:alpha_name].nil?
            @page = (class_name.constantize.count(:include => options[:include], :conditions => ["#{options[:conditions]} and #{config[:alpha_name]} < ?",params[:alpha]]).to_f / max_items.to_f).ceil
         end
         @pages = (class_name.constantize.count(:include => options[:include], :conditions => "#{options[:conditions]}").to_f / max_items.to_f).ceil
         if config[:page_name].nil?
            @page_title = "#{class_name.pluralize} List (FILTERED)"
         else
            @page_title = "#{config[:page_name]} (FILTERED)"
         end
      end
      if !config[:active].nil?
         if session["show_all_#{model.pluralize}".to_sym] == false
            @page_title << " (Active)"
         else
            @page_title << " (All)"
         end
      end
      @page = @pages if @page > @pages
      @page = 1 if @page < 1
      options[:offset] = ((@page - 1) * max_items)
      return class_name.constantize.find(:all, options.to_options)
   end
   
   # this accepts an array (usually of records) and simply pages it
   def paged_array(array)
      array.is_a?(Array) or raise InvalidParameterError.new("paged_array: expected Array, got #{array.inspect}")
      if params[:page]
         @page = params[:page].to_i
      else
         @page = 1
      end
      limit = session[:items_per_page]
      @pages = (array.size.to_f / limit.to_f).ceil
      @page = @pages if @page > @pages
      @page = 1 if @page < 1
      offset = ((@page - 1) * limit)
      return array.slice(offset,limit)
   end
      
   # this performs a paged retrieval of review data from a model and initializes a
   # standard set of instance variables from model data - access params hash directly
   # it updates the last_viewed fields in the user to the highest id number for the
   # page returned.
   def review_paged(model, *opts)
      model.is_a?(String) || model.is_a?(Symbol) or raise InvalidParameterError.new("review_pages: model name (arg1) is required and must be a string or symbol")
      model = model.to_s
      config = HashWithIndifferentAccess.new(:include => nil, :limit => nil)
      config.update(opts.pop) if opts.last.is_a?(Hash)
      # honor user preferences
      return [] if current_user.send("review_#{model}") == false
      # start the retrieval of review records
      class_name = model.camelize
      options = HashWithIndifferentAccess.new
      max_items = session[:items_per_page]
      if config[:limit].nil?
         options[:limit] = max_items
      else
         options[:limit] = config[:limit]
      end
      unless config[:include].nil?
         options[:include] = config[:include]
      end
      last_viewed = current_user.send("last_#{model}")
      if last_viewed.nil?
         last_viewed = 0
      end
      options[:conditions] = "#{model.pluralize}.id > #{last_viewed}"
      # for reviews, we skip any record that is marked private
      if class_name.constantize.respond_to?('column_names') && class_name.constantize.column_names.include?('private')
         options[:conditions] << " AND #{model.pluralize}.private != 1"
      end
      if options[:include].nil?
         @pages = (class_name.constantize.count(:conditions => "#{options[:conditions]}")).ceil
      else
         @pages = (class_name.constantize.count(:include => options[:include], :conditions => "#{options[:conditions]}")).ceil
      end
      records = class_name.constantize.find(:all, options.to_options)
      unless records.empty?
         current_user.update_attribute("last_#{model}".to_sym, records.last.id)
      end
      return records
   end
   
   def review_skip_all(model)
      model.is_a?(String) || model.is_a?(Symbol) or raise InvalidParameterError.new("review_skip_all: model name (arg1) is required and must be a string or symbol")
      model = model.to_s
      class_name = model.camelize
      lastid = class_name.constantize.find(:last).id
      current_user.update_attribute("last_#{model}".to_sym, lastid)
      return true
   end
   
   # This loads site colors from the Colorscheme model
   def load_config
      @colors = Colorscheme.load_config(SiteConfig.colorscheme)
   end

   # Returns true if the user has logged in (session[:user] valid) or false
   # if not.
   def logged_in
      session[:user] ? true : false
   end

   # Returns the current user record with associated contact preloaded.
   def current_user
      User.find(session[:user], :include => :contact)
   end
   
   # called before each action to update the session active time
   def update_session_active
      if session[:user]
         session[:active_at] = Time.now
      end
   end

   # called to test whether session has expired according to site config
   # settings
   def session_expired?
      return true if session[:started_at].nil? || session[:active_at].nil?
      if SiteConfig.session_max > 0 && SiteConfig.session_max.minutes.ago > session[:started_at]
         # beyond maximum life - expire it
         return true
      end
      unless SiteConfig.session_life > 0 && SiteConfig.session_life.minutes.ago < session[:started_at]
         # no grace period or it has expired
         if SiteConfig.session_active > 0 && SiteConfig.session_active.minutes.ago > session[:active_at]
            # inactive - expire it
            return true
         end
      end
      return false
   end

   # called as a filter, checks to see if the user has logged in and redirects
   # to the login screen if not.
   def login_required
      if session[:user]
         if session_expired?
            # session expired
            redirect_to :controller => "users", :action => "logout", :message => "Your session has expired. Please login again."
         elsif session[:user_expires] <= Date.today
            # password expired
            flash[:warning] = "Your password has expired."
            redirect_to :controller => "users", :action => "change_password"
         end
         return
      end
      flash[:notice] = "Please login to continue."
      redirect_to :controller => "users", :action => "login"
      return false 
   end
   
   # shorthand for big decimal value of 0 when used in calculations
   def big_zero
      BigDecimal.new('0.0',2)
   end
   
   # returns array of dispatcher shift numbers that are configured
   # in SiteConfig
   def valid_shifts
      shifts = []
      unless SiteConfig.dispatch_shift1_start.nil? || SiteConfig.dispatch_shift1_stop.nil?
         shifts.push(1)
      end
      unless SiteConfig.dispatch_shift2_start.nil? || SiteConfig.dispatch_shift2_stop.nil?
         shifts.push(2)
      end
      unless SiteConfig.dispatch_shift3_start.nil? || SiteConfig.dispatch_shift3_stop.nil?
         shifts.push(3)
      end
      return shifts
   end
end