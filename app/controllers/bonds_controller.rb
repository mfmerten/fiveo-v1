# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Bonds are placed against bondable holds until the bond amount is satisfied,
# upon which the hold will be marked as cleared.
class BondsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Bonds Listing
   def index
      require_permission Perm[:view_bond]
      @help_page = ["Bonds","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:bond_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:bond_filter] = nil
            redirect_to bonds_path and return
         end
      else
         @query = session[:bond_filter] || HashWithIndifferentAccess.new
      end
      unless @bonds = paged('bond',:order => 'bonds.issued_date DESC, bonds.issued_date DESC',:include => [:person, :arrest, :bond_type, :bondsman, :surety, :status])
         @query = HashWithIndifferentAccess.new
         session[:bond_filter] = nil
         redirect_to bonds_path and return
      end
      @links = []
      if has_permission Perm[:view_booking]
         @links.push(['Booking', bookings_path])
      end
      @links.push(['PDF', url_for(:action => 'list_form')])
   end

   # Show Bond
   def show
      require_permission Perm[:view_bond]
      @help_page = ["Bonds","show"]
      params[:id] or raise_missing "Bond ID"
      @bond = Bond.find_by_id(params[:id]) or raise_not_found "Bond ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_bond]
         @links.push(["Edit", edit_bond_path(:id => @bond.id)])
      end
      if has_permission Perm[:destroy_bond]
         @links.push(['Delete', @bond, {:method => 'delete', :confirm => "Are you sure you want to destroy this Bond?"}])
      end
      if has_permission Perm[:view_bond]
         @links.push(["PDF", url_for(:action => 'bond_form', :id => @bond.id)])
         if has_permission(Perm[:update_court]) && !@bond.status.nil? && @bond.status.long_name == 'Active'
            if session[:letters].nil? || session[:letters].empty? || !session[:letters].include?(@bond.id)
               @links.push(["Queue Letter", url_for(:action => 'add_to_queue', :id => @bond.id)])
            end
            if !session[:letters].nil? && session[:letters].include?(@bond.id)
               @links.push(["Unqueue Letter", url_for(:action => 'remove_from_queue', :id => @bond.id)])
            end
         end
      end
      @linksx = []
      active = ( has_permission(Perm[:update_bond]) && !@bond.status.nil? && @bond.status.long_name == 'Active' )
      booked = ( !@bond.person.bookings.empty? && !@bond.person.bookings.last.release_date? )
      revoked = ( has_permission(Perm[:update_bond]) && !@bond.status.nil? && @bond.status.long_name == 'Revoked' )
      forfeited = ( has_permission(Perm[:update_bond]) && !@bond.status.nil? && @bond.status.long_name == 'Forfeited' )
      revokable = (active || (revoked && booked))
      forfeitable = (active || (forfeited && booked))
      @linksx.push(["Surrender", url_for(:action => 'recall_bond', :id => @bond.id, :status => 'surrender'), {:method => :post, :confirm => 'Really Surrender This Bond?', :disabled => !(booked && active)}])
      @linksx.push(["Revoke", url_for(:action => 'recall_bond', :id => @bond.id, :status => 'revoke'), {:method => :post, :confirm => 'Really Revoke This Bond?', :disabled => !revokable}])
      @linksx.push(["Forfeit", url_for(:action => 'recall_bond', :id => @bond.id, :status => 'forfeit'), {:method => :post, :confirm => 'Really Forfeit This Bond?', :disabled => !forfeitable}])
      if has_permission Perm[:update_court]
         @linksx.push(["Final", url_for(:action => 'recall_bond', :id => @bond.id, :status => 'final'), {:method => :post, :confirm => 'Really Finalize This Bond?', :disabled => !active}])
      end
      @page_links = [@bond.person,@bond.bondsman, [@bond.surety,'Surety'], @bond.arrest, @bond.hold]
      unless @bond.hold.nil?
         @page_links.push(@bond.hold.booking)
      end
      @page_links.push([@bond.creator,'creator'],[@bond.updater, 'updater'])
   end

   # Add or Edit Bond
   def edit
      require_permission Perm[:update_bond]
      @help_page = ["Bonds","edit"]
      if params[:id]
         @bond = Bond.find_by_id(params[:id]) or raise_not_found "Bond ID #{params[:id]}"
         @page_title = "Edit Bond ID: #{@bond.id}"
         @bond.updated_by = session[:user]
         unless @bond.hold.nil?
            @hold = @bond.hold
         end
      else
         params[:hold_id] or raise_missing "Hold ID"
         @hold = Hold.find_by_id(params[:hold_id]) or raise_not_found "Hold ID #{params[:hold_id]}"
         if @hold.cleared_date?
            flash[:warning] = "Cannot add a bond to a cleared hold!"
            redirect_to @hold and return
         end
         @page_title = "New Bond"
         @bond = Bond.new(:created_by => session[:user], :updated_by => session[:user], :hold_id => @hold.id) or raise_db(:new, "Bond")
         @bond.issued_date, @bond.issued_time = get_date_and_time
         @bond.status_date, @bond.status_time = get_date_and_time
         @bond.arrest_id = @hold.arrest_id
         if @hold.fines?
           @bond.bond_amt = @hold.fines
         end
         if @hold.arrest.nil?
            @bond.person_id = @hold.booking.person_id
         else
            @bond.person_id = @hold.arrest.person_id
         end
         unless @hold.hold_type.nil?
            if @hold.hold_type.long_name == 'Hold for City' || @hold.hold_type.long_name == 'Hold for Other Agency'
               @bond.status_id = Option.lookup("Bond Status","For Other Agency",nil,nil,true)
            else
               @bond.status_id = Option.lookup("Bond Status","Active",nil,nil,true)
             end
         end
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @bond.new_record?
               redirect_to bonds_path
            else
               redirect_to bond_path(:id => @bond.id, :hold_id => @hold.id)
            end
            return
         end
         @bond.attributes = params[:bond]
         if @bond.save
            redirect_to bond_path(:id => @bond.id, :hold_id => @hold.id)
            if params[:id]
               user_action("Bond","Edited Bond ID: #{@bond.id}.")
            else
               user_action("Bond", "Added Bond ID: #{@bond.id}.")
            end
         end
      end
   end

   # Destroys the specified bond record. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_bond]
      require_method :delete
      params[:id] or raise_missing "Bond ID"
      @bond = Bond.find_by_id(params[:id]) or raise_not_found "Bond ID #{params[:id]}"
      @bond.destroy or raise_db(:destroy, "Bond ID #{params[:id]}")
      user_action("Bond","Destroyed Bond ID: #{params[:id]}.")
      flash[:notice] = "Bond Deleted!"
      redirect_to bonds_path
   end
   
   # processes bond surrender, forfeiture and revocation
   def recall_bond
      require_permission Perm[:update_bond]
      require_method :post
      params[:id] or raise_missing "Bond ID"
      @bond = Bond.find_by_id(params[:id]) or raise_not_found "Bond ID #{params[:id]}"
      status = params[:status] or raise_missing "Status Not Specified"
      valid_status_array = ['revoke','surrender','forfeit','final']
      valid_status_array.include?(status) or raise_invalid("Status", valid_status_array)
      if @bond.person.nil?
         flash[:warning] = "Bonded Person is Not Valid!"
         redirect_to @bond and return
      end
      if @bond.person.bookings.last.nil? && status == 'surrender'
         flash[:warning] = "Cannot Surrender Bond unless Bonded Person is Booked!"
         redirect_to @bond and return
      end
      @bond.update_attribute(:updated_by, session[:user])
      if result = @bond.recall(status)
         flash[:warning] = "Bond #{status} Failed: #{result}!"
         redirect_to @bond and return
      end
      user_action("Bond","Recalled Bond ID: #{@bond.id} (#{status})")
      redirect_to @bond
   end
   
   # Generates a PDF bond specified with the booking_id parameter
   def bond_form
      require_permission Perm[:view_bond]
      params[:id] or raise_missing "Bond ID"
      @bond = Bond.find_by_id(params[:id]) or raise_not_found "Bond ID #{params[:id]}"
      @page_title = "BOND"
      prawnto :prawn => {
         :page_size => 'LEGAL',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "bond-#{@bond.id}.pdf"
      render :template => 'bonds/bond_form.pdf.prawn', :layout => false
      user_action("Bond","Printed Bond for Bond ID: #{@bond.id}.")
   end
   
   # allows individual bonds to be queued for bondsman letters
   def add_to_queue
      require_permission Perm[:view_bond]
      params[:id] or raise_missing "Bond ID"
      @bond = Bond.find_by_id(params[:id]) or raise_not_found "Bond ID #{params[:id]}"
      if @bond.status.nil? || @bond.status.long_name != 'Active'
         flash[:warning] = 'Bond not active, Cannot Queue!'
         redirect_to @bond and return
      end
      if !session[:letters].nil? && session[:letters].include?(@bond.id)
         flash[:warning] = 'Bond already in letter queue!'
         redirect_to @bond and return
      end
      if session[:letters].nil?
         session[:letters] = [@bond.id]
      else
         session[:letters].push(@bond.id)
      end
      flash[:notice] = "Bond added to letter queue."
      redirect_to @bond
   end
   
   # allows bonds to be removed from the bondsman letter queue
   def remove_from_queue
      require_permission Perm[:view_bond]
      params[:id] or raise_missing "Bond ID"
      @bond = Bond.find_by_id(params[:id]) or raise_not_found "Bond ID #{params[:id]}"
      if session[:letters].nil? || !session[:letters].include?(@bond.id)
         flash[:warning] = 'Bond is not queued for printing!'
         redirect_to @bond and return
      end
      session[:letters].delete(@bond.id)
      if session[:letters].empty?
         session[:letters] = nil
      end
      flash[:notice] = "Bond removed from letter queue."
      redirect_to @bond
   end
   
   # This prints a bond list (PDF) using the same query parameters that
   # are in effect for the bond list view.  If no query parameters are
   # saved in the session, the full bond list will be printed.
   def list_form
      require_permission Perm[:view_bond]
      @query = session[:bond_filter] ||= HashWithIndifferentAccess.new
      cond = nil
      unless @query.nil? || @query.empty?
         cond = Bond.condition_string(@query)
      end
      @bonds = Bond.find(:all, :order => 'bonds.issued_date DESC, bonds.issued_date DESC', :include => [:person, :arrest, :bond_type, :bondsman, :surety, :status], :conditions => cond)
      if @bonds.empty?
         flash[:warning] = "No Bonds selected for report!"
         redirect_to bonds_path and return
      end
      @report_date = Date.today
      @page_title = "Bond List"
      prawnto :prawn => {
         :page_size => 'LETTER',
         :page_layout => :landscape,
         :left_margin => 20,
         :right_margin => 20,
         :top_margin => 10,
         :bottom_margin => 10 },
         :filename => "Bond-List.pdf"
      render :template => 'bonds/list_form.pdf.prawn', :layout => false
      user_action("Bonds","Printed Bond List.")
   end
   
   protected
   
   # This ensures that the "Bonds" menu item is highlighted for all actions
   # in this controller.
   def set_tab_name
      @current_tab = "Bonds"
   end
end
