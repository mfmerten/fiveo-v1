# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Dockets -- These records detail all court related operations.
class DocketsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Docket Listing
   def index
      require_permission Perm[:view_court]
      @help_page = ["Dockets","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:docket_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:docket_filter] = nil
            redirect_to dockets_path and return
         end
      else
         @query = session[:docket_filter] || HashWithIndifferentAccess.new
      end
      unless @dockets = paged('docket', :order => 'docket_no', :include => [:person, :courts])
         @query = HashWithIndifferentAccess.new
         session[:docket_filter] = nil
         redirect_to dockets_path and return
      end
      @unprocessed = Sentence.find(:all, :conditions => 'processed != 1')
      @unprocessed.concat(Revocation.find(:all, :conditions => 'processed != 1'))
      @links = []
      if has_permission Perm[:update_court]
         @links.push(['New', edit_docket_path])
      end
   end
   
   # lists unprocessed sentences
   def list_unprocessed
      require_permission Perm[:view_court]
      @unprocessed = Sentence.find(:all, :conditions => 'processed != 1')
      @unprocessed << Revocation.find(:all, :conditions => 'processed != 1')
      @unprocessed = @unprocessed.flatten.compact
   end
   
   # Show Docket
   def show
      require_permission Perm[:view_court]
      @help_page = ["Dockets","show"]
      params[:id] or raise_missing "Docket ID"
      @docket = Docket.find(:first, :include => [{:sentences => :result}, {:da_charges => [:arrest, {:arrest_charge => [:district_arrest, :citation]}, :sentence]}], :conditions => {:id => params[:id]}) or raise_not_found "Docket ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_court]
         @links.push(['New', edit_docket_path])
         @links.push(['Edit', edit_docket_path(:id => @docket.id)])
      end
      if has_permission Perm[:destroy_court]
         @links.push(['Delete', @docket, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Docket?'}])
      end
      if has_permission Perm[:update_court]
         @links.push(['Link Charges', url_for(:action => 'link_charges', :docket_id => @docket.id)])
         @links.push(['New Court', edit_court_path(:docket_id => @docket.id)])
      end
      @page_links = [@docket.person]
      @page_links.concat(@docket.probations)
      @page_links.push([@docket.creator,'creator'],[@docket.updater,'updater'])
   end

   # Add or Edit Docket
   def edit
      require_permission Perm[:update_court]
      @help_page = ["Dockets","edit"]
      if params[:id]
         @docket = Docket.find_by_id(params[:id]) or raise_not_found "Docket ID #{params[:id]}"
         @page_title = "Edit Docket ID: #{@docket.id}"
         @docket.updated_by = session[:user]
         if @docket.da_charges.empty?
            unless request.post?
               @docket.da_charges.build(:created_by => session[:user], :updated_by => session[:user])
            end
         end
      else
         @docket = Docket.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Docket")
         @page_title = "New Docket"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @docket.new_record?
               redirect_to dockets_path
            else
               redirect_to @docket
            end
            return
         end
         params[:docket][:existing_charge_attributes] ||= {}
         @docket.attributes = params[:docket]
         if @docket.save
            redirect_to @docket
            if params[:id]
               user_action("Dockets","Edited Docket ID: #{@docket.id}.")
            else
               user_action("Dockets", "Added Docket ID: #{@docket.id}.")
            end
         end
      end
   end
   
   # Destroys the specified docket record. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_court]
      require_method :delete
      params[:id] or raise_missing "Docket ID"
      @docket = Docket.find_by_id(params[:id]) or raise_not_found "Docket ID #{params[:id]}"
      @docket.destroy or raise_db(:destroy, "Docket")
      user_action("Dockets", "Destroyed Docket ID #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to dockets_path
   end

   # Show Court
   def show_court
      require_permission Perm[:view_court]
      @help_page = ["Dockets","show_court"]
      params[:id] or raise_missing "Court ID"
      @court = Court.find_by_id(params[:id]) or raise_not_found "Court ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_court]
         @links.push(['New', edit_court_path(:docket_id => @court.docket_id)])
         @links.push(['Edit', edit_court_path(:id => @court.id)])
      end
      if has_permission Perm[:destroy_court]
         @links.push(['Delete', @court, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Court?'}])
      end
      unless @court.docket.misc?
         if has_permission Perm[:update_court]
            @links.push(['New Revocation', edit_revocation_path(:docket_id => @court.docket_id)])
         end
         if has_permission Perm[:view_court]
            @links.push(['Sent. Sheets', url_for(:action => 'worksheets', :id => @court.id, :type => 'sentence')])
            @links.push(['Revoc. Sheet', url_for(:action => 'worksheets', :id => @court.id, :type => 'revocation')])
         end
      end
      @page_links = []
      unless @court.docket.nil?
         @page_links.push(@court.docket.person,@court.docket)
      end
      @page_links.push([@court.creator,'creator'],[@court.updater,'updater'])
   end

   # Add or Edit Court
   def edit_court
      require_permission Perm[:update_court]
      @help_page = ["Dockets","edit_court"]
      if params[:id]
         @court = Court.find_by_id(params[:id]) or raise_not_found "Court ID #{params[:id]}"
         @page_title = "Edit Court ID: " + @court.id.to_s
         @court.updated_by = session[:user]
      else
         params[:docket_id] or raise_missing "Docket ID"
         @docket = Docket.find_by_id(params[:docket_id]) or raise_not_found "Docket ID #{params[:docket_id]}"
         @court = Court.new(:created_by => session[:user], :updated_by => session[:user], :docket_id => @docket.id) or raise_db(:new, "Court")
         @page_title = "New Court"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @court.new_record?
               redirect_to @court.docket
            else
               redirect_to @court
            end
            return
         end
         @court.attributes = params[:court]
         if @court.save
            redirect_to @court
            if params[:id]
               user_action("Dockets","Edited Court ID: #{@court.id}.")
            else
               user_action("Dockets", "Added Court ID: #{@court.id}.")
            end
         end
      end
   end
   
   # Destroys the specified court record. Only responds to DELETE methods.
   def destroy_court
      require_permission Perm[:destroy_court]
      require_method :delete
      params[:id] or raise_missing "Court ID"
      @court = Court.find_by_id(params[:id]) or raise_not_found "Court ID #{params[:id]}"
      docket = @court.docket
      @court.destroy or raise_db(:destroy, "Court ID #{params[:id]}")
      user_action("Dockets", "Destroyed Court ID #{params[:id]}!")
      flash[:notice] = "Record Deleted"
      redirect_to docket
   end
   
   # Show Revocation
   def show_revocation
      require_permission Perm[:view_court]
      @help_page = ["Dockets","show_revocation"]
      params[:id] or raise_missing "Revocation ID"
      @revocation = Revocation.find_by_id(params[:id]) or raise_not_found "Revocation ID #{params[:id]}"
      @page_title = "Revocation ID: #{@revocation.id}"
      @links = []
      if has_permission Perm[:update_court]
         @links.push(['New', edit_revocation_path])
         @links.push(['Edit', edit_revocation_path(:id => @revocation.id)])
      end
      if has_permission Perm[:destroy_court]
         @links.push(['Delete', @revocation, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Revocation?'}])
      end
      @page_links = []
      unless @revocation.docket.nil?
         @page_links.push(@revocation.docket.person,@revocation.docket)
         @page_links.concat(@revocation.docket.probations)
      end
      @page_links.push(@revocation.doc_record,@revocation.hold,@revocation.arrest,[@revocation.creator,'creator'],[@revocation.updater,'updater'])
   end

   # Add or Edit Revocation
   def edit_revocation
      require_permission Perm[:update_court]
      @help_page = ["Dockets","edit_revocation"]
      if params[:id]
         @revocation = Revocation.find_by_id(params[:id]) or raise_not_found "Revocation ID #{params[:id]}"
         @page_title = "Edit Revocation ID: #{@revocation.id}"
         @revocation.updated_by = session[:user]
      else
         params[:docket_id] or raise_missing "Docket ID"
         @docket = Docket.find_by_id(params[:docket_id]) or raise_not_found "Docket ID #{params[:docket_id]}"
         @revocation = Revocation.new(:created_by => session[:user], :updated_by => session[:user], :docket_id => @docket.id) or raise_db(:new, 'Revocation')
         @revocation.total_hours, @revocation.total_days, @revocation.total_months, @revocation.total_years = @revocation.total_of_sentences
         @page_title = "New Revocation"
      end
      ['total','parish'].each do |t|
         ['hours','days','months','years'].each do |s|
            if @revocation.send("#{t}_#{s}").blank?
               @revocation.send("#{t}_#{s}=", 0)
            end
         end
      end
      
      if request.post?
         if params[:commit] == 'Cancel'
            if @revocation.new_record?
               redirect_to @revocation.docket
            else
               redirect_to @revocation
            end
            return
         end
         @revocation.attributes = params[:revocation]
         if @revocation.save
            redirect_to @revocation
            if params[:id]
               user_action("Dockets","Edited Revocation ID: #{@revocation.id}.")
            else
               user_action("Dockets", "Added Revocation ID: #{@revocation.id}.")
            end
         end
      end
   end
   
   # Destroys the specified revocation record. Only responds to DELETE methods.
   def destroy_revocation
      require_permission Perm[:destroy_court]
      require_method :delete
      params[:id] or raise_missing "Revocation ID"
      @revocation = Revocation.find_by_id(params[:id]) or raise_not_found "Revocation ID #{params[:id]}"
      docket = @revocation.docket
      @revocation.destroy or raise_db(:destroy, "Revocation ID #{params[:id]}")
      user_action("Dockets", "Destroyed Revocation ID #{params[:id]}!")
      flash[:notice] = "Record Deleted"
      redirect_to docket
   end
   
   # Display a sentence
   def show_sentence
      require_permission Perm[:view_court]
      params[:id] or raise_missing "Sentence ID"
      @sentence = Sentence.find_by_id(params[:id]) or raise_not_found "Sentence ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_court]
         @links.push(['Edit', edit_sentence_path(:id => @sentence.id)])
      end
      if has_permission Perm[:destroy_court]
         @links.push(['Delete', @sentence, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Sentence?'}])
      end
      @page_links = []
      unless @sentence.docket.nil?
         @page_links.push(@sentence.docket.person)
      end
      @page_links.push(@sentence.docket, @sentence.court,@sentence.doc_record,@sentence.hold,@sentence.probation,[@sentence.creator,'creator'],[@sentence.updater,'updater'])
   end
   
   # Edit a sentence
   def edit_sentence
      require_permission Perm[:update_court]
      @help_page = ["Dockets","edit_sentence"]
      if params[:id]
         @sentence = Sentence.find_by_id(params[:id]) or raise_not_found "Sentence ID #{params[:id]}"
         @page_title = "Edit Sentence ID: #{@sentence.id}"
         @sentence.updated_by = session[:user]
      else
         params[:da_charge_id] or raise_missing "DA Charge ID"
         @da_charge = DaCharge.find_by_id(params[:da_charge_id]) or raise_not_found "DA Charge ID #{params[:da_charge_id]}"
         params[:court_id] or raise_missing "Court ID"
         @court = Court.find_by_id(params[:court_id]) or raise_not_found "Court ID #{params[:court_id]}"
         @court.docket_id == @da_charge.docket_id or raise_invalid "Docket", "Court ID and DA Charge ID are for different Dockets."
         @sentence = Sentence.new(
            :created_by => session[:user],
            :updated_by => session[:user],
            :docket_id => @da_charge.docket.id,
            :court_id => @court.id,
            :da_charge_id => @da_charge.id,
            :conviction => @da_charge.charge,
            :conviction_count => @da_charge.count
         ) or raise_db(:new, "Sentence")
         @page_title = "New Sentence"
      end
      ['probation','default','sentence','suspended_except'].each do |t|
         ['hours','days','months','years'].each do |s|
            if @sentence.send("#{t}_#{s}").blank?
               @sentence.send("#{t}_#{s}=", 0)
            end
         end
      end
      
      if request.post?
         if params[:commit] == 'Cancel'
            if @sentence.new_record?
               redirect_to @court
            else
               redirect_to @sentence
            end
            return
         end
         @sentence.attributes = params[:sentence]
         if @sentence.save
            redirect_to @sentence
            if params[:id]
               user_action("Dockets","Edited Sentence ID: #{@sentence.id}.")
            else
               user_action("Dockets", "Added Sentence ID: #{@sentence.id}.")
            end
         end
      end
   end
   
   # delete a sentence
   def destroy_sentence
      require_permission Perm[:destroy_court]
      require_method :delete
      params[:id] or raise_missing "Sentence ID"
      @sentence = Sentence.find_by_id(params[:id]) or raise_not_found "Sentence ID #{params[:id]}"
      court = @sentence.court
      @sentence.destroy or raise_db(:destroy, "Sentence ID #{params[:id]}")
      user_action("Dockets", "Destroyed Sentence ID #{params[:id]}!")
      flash[:notice] = "Record Deleted"
      redirect_to court
   end
   
   # force a sentence or revocation to process with a closed booking
   # this is used when a person is sentenced to a few hours or days
   # and they serve their time and are released before the sentence or
   # revocation is entered into the system.
   def force_processing
      require_permission Perm[:update_court]
      require_method :post
      params[:type] or raise_missing "Type"
      rtype = params[:type]
      valid_types = ["sentence","revocation"]
      valid_types.include?(rtype) or raise_invalid "Type '#{rtype}'", valid_types
      params[:id] or raise_missing "#{rtype.capitalize} ID"
      if rtype == "sentence"
         @record = Sentence.find_by_id(params[:id])
      else
         @record = Revocation.find_by_id(params[:id])
      end
      !@record.nil? or raise_not_found "#{rtype.capitalize} ID #{params[:id]}"
      @record.force = true
      @record.save(false)
      user_action("Dockets","Forced processing for #{rtype.capitalize} ID: #{@record.id}.")
      redirect_to @record
   end

   # Prints worksheets for a docket.
   def worksheets
      require_permission Perm[:view_court]
      params[:id] or raise_missing "Court ID"
      @court = Court.find_by_id(params[:id]) or raise_not_found "Court ID #{params[:id]}"
      params[:type] or raise_missing "Type"
      valid_types = ["sentence","revocation"]
      valid_types.include?(params[:type]) or raise_invalid "Type '#{params[:type]}'", valid_types
      if params[:type] == 'sentence'
         prawnto :prawn => {
            :page_size => 'LEGAL',
            :left_margin => 20,
            :right_margin => 20,
            :top_margin => 10,
            :bottom_margin => 10 },
            :filename => "sentence_sheet-#{@court.id}.pdf"
         user_action("Dockets","Printed Sentence Worksheets for Court ID: #{@court.id}")
         render :template => "dockets/sentence_worksheet.pdf.prawn", :layout => false
      else
         prawnto :prawn => {
            :page_size => 'LEGAL',
            :left_margin => 20,
            :right_margin => 20,
            :top_margin => 10,
            :bottom_margin => 10 },
            :filename => "revocation_sheet-#{@court.id}.pdf"
         user_action("Dockets","Printed Revocation Worksheet for Docket ID: #{@court.id}")
         render :template => "dockets/revocation_worksheet.pdf.prawn", :layout => false
      end
   end
   
   # link charges to the docket
   def link_charges
      require_permission Perm[:update_court]
      params[:docket_id] or raise_missing "Docket ID"
      @docket = Docket.find_by_id(params[:docket_id]) or raise_not_found "Docket ID #{params[:docket_id]}"
      if @docket.person.nil?
         flash[:warning] = "Docket Has Invalid Person"
         redirect_to @docket and return
      end
      @charges = []
      @docket.person.arrests.each do |a|
         a.district_charges.each do |c|
            next unless c.da_charges.reject{|d| d.info_only?}.empty?
            if c.da_charges.empty?
               @charges.push([a.arrest_date, 'Arrest', c.id, c.charge, nil])
            else
               @charges.push([a.arrest_date, 'Arrest', c.id, c.charge, '[INFO]'])
            end
         end
      end
      @docket.person.citations.each do |a|
         a.charges.each do |c|
            next unless c.da_charges.reject{|d| d.info_only?}.empty?
            if c.da_charges.empty?
               @charges.push([a.citation_date, 'Citation', c.id, c.charge, nil])
            else
               @charges.push([a.citation_date, 'Citation', c.id, c.charge, '[INFO]'])
            end
         end
      end
      if @charges.empty?
         flash[:warning] = "No available charges!"
         redirect_to @docket and return
      end
      @charges = @charges.sort{|a,b| "#{a[0]} #{a[1]}" <=> "#{b[0]} #{b[1]}"}
      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to @docket and return
         end
         params[:docket] ||= {:charges => {}}
         @docket.linked_charges = params[:docket][:charges]
         redirect_to @docket
         user_action("Dockets","Updated Linked Charges for Docket ID: #{@docket.id}.")
      end
   end
   
   # link charges to the docket
   def link_arr_or_charge
      require_permission Perm[:update_court]
      params[:da_charge_id] or raise_missing "DA Charge ID"
      @da_charge = DaCharge.find_by_id(params[:da_charge_id]) or raise_not_found "DA Charge ID #{params[:da_charge_id]}"
      if @da_charge.docket.nil?
         flash[:warning] = "DA Charge Has Invalid Docket"
         redirect_to dockets_path and return
      end
      if @da_charge.docket.person.nil?
         flash[:warning] = "Docket for DA Charge has Invalid Person"
         redirect_to @da_charge.docket and return
      end
      @records = @da_charge.docket.person.arrests.collect{|a| [a.arrest_date,a]} + @da_charge.docket.person.citations.collect{|c| [c.citation_date,c]}
      if @records.empty?
         flash[:warning] = "No available arrests or citations!"
         redirect_to @da_charge.docket and return
      end
      @records = @records.sort{|a,b| b[0] <=> a[0]}
      @links = [["Cancel", @da_charge.docket]]
   end
   
   def do_link_arr_or_charge
      require_permission Perm[:update_court]
      require_method :post
      params[:da_charge_id] or raise_missing "DA Charge ID"
      @da_charge = DaCharge.find_by_id(params[:da_charge_id]) or raise_not_found "DA Charge ID #{params[:da_charge_id]}"
      if params[:arrest_id]
         @arrest = Arrest.find_by_id(params[:arrest_id]) or raise_not_found "Arrest ID #{params[:arrest_id]}"
         @da_charge.update_attribute(:arrest_id, @arrest.id)
      elsif params[:charge_id]
         @charge = Charge.find_by_id(params[:charge_id]) or raise_not_found "Charge ID #{params[:charge_id]}"
         @da_charge.update_attribute(:charge_id, @charge.id)
      else
         raise_missing "Arrest ID or Charge ID"
      end
      redirect_to @da_charge.docket
      user_action("Dockets","Updated Charge Link for Docket ID: #{@da_charge.docket.id}.")
   end
   
   # AJAX: validates a docket ID and returns person name
   def validate
      if docket = Docket.find_by_id(params[:c])
         render :text => docket.person.full_name
      else
         render :text => "Invalid Docket!"
      end
   end

   # AJAX: validates a court ID and returns court information
   def validate_court
      if court = Court.find_by_id(params[:c])
         render :text => "#{format_date(court.court_date)} #{court.judge.long_name} (#{court.court_type.long_name})"
      else
         render :text => "Invalid Court!"
      end
   end
   
   # AJAX: validates a da_charge ID and returns charge info
   def validate_da_charge
      if da_charge = DaCharge.find_by_id(params[:c])
         render :text => "#{da_charge.count} count #{da_charge.charge}"
      else
         render :text => "Invalid Charge!"
      end
   end
   
   # Prints bondsmen letters
   def letters
      require_permission Perm[:view_court]
      if params[:court_id]
         one_court = Court.find_by_id(params[:court_id]) or raise_not_found "Court ID #{params[:court_id]}"
         @court_date = one_court.court_date
         if one_court.docket_type.nil?
            flash[:warning] = "Court has Invalid Docket Type"
            redirect_to one_court and return
         end
         @dt = one_court.docket_type
         if one_court.judge.nil?
            flash[:warning] = "Court has Invalid Judge"
            redirect_to one_court and return
         end
         j = one_court.judge
      else
         params[:court_date] or raise_missing "Court Date"
         @court_date = valid_date(params[:court_date]) or raise_invalid "Court Date", "'#{params[:court_date]}' Is Not A Valid Date"
         params[:docket_type] or raise_missing "Docket Type"
         @dt = Option.find_by_id(params[:docket_type]) or raise_not_found "Docket Type ID #{params[:docket_type]}"
         params[:judge] or raise_missing "Judge"
         j = Option.find_by_id(params[:judge]) or raise_not_found "Judge ID #{params[:judge]}"
      end
      params[:time] or raise_missing "Time"
      @time = valid_time(params[:time]) or raise_invalid "Time", "'#{params[:time]}' Is Not A Valid Time"
      # convert time to 12 hour A/P
      hour = @time.slice(0,2).to_i
      if hour > 12
         hour = hour - 12
         shour = hour.to_s
         if shour.size == 1
            shour = '0' + shour
         end
         @time = @time.sub(/^\d{2}/,shour)
         @time << ' PM'
      else
         @time << ' AM'
      end
      active = Option.lookup("Bond Status","Active",nil,nil,true)
      if !session[:letters].nil? && !session[:letters].empty?
         @bonds = session[:letters].collect{|bid| [Bond.find_by_id(bid),nil]}.compact.reject{|b| b[0].status.nil? || b[0].status.long_name != 'Active'}
      else
         if one_court.nil?
            courts = Court.find(:all, :conditions => ['court_date = ? and judge_id = ? and docket_type_id = ?',@court_date,j.id,@dt.id])
         else
            courts = [one_court]
         end
         @bonds = []
         courts.each do |c|
            if c.court_type.nil?
               flash[:warning] = "Court ID: #{c.id} is missing a valid Court Type!"
               redirect_to c and return
            end
            c.docket.bonds.compact.uniq.each do |b|
               unless b.status_id != active
                  @bonds.push([b,c])
               end
            end
         end
      end
      if @bonds.empty?
         flash[:warning] = "No Bonds Found!"
         redirect_to dockets_path and return
      end
      prawnto :prawn => {
         :page_size => 'LETTER',
         :left_margin => 20,
         :right_margin => 20,
         :top_margin => 10,
         :bottom_margin => 10 },
         :filename => "letters-#{@dt.long_name}.pdf"
      user_action("Dockets","Printed Bond Letters for Docket Type: #{@dt.long_name}")
      render :template => "dockets/letters.pdf.prawn", :layout => false
   end
   
   def clear_letter_queue
      session[:letters] = nil
      redirect_to dockets_path
   end
   
   # Prints dockets
   def dockets
      require_permission Perm[:view_court]
      params[:court_date] or raise_missing "Court Date"
      @court_date = valid_date(params[:court_date]) or raise_invalid "Court Date", "'#{params[:court_date]}' Is Not A Valid Date"
      params[:docket_type] or raise_missing "Docket Type"
      @dt = Option.find_by_id(params[:docket_type]) or raise_not_found "Docket Type ID #{params[:docket_type]}"
      params[:judge] or raise_missing "Judge"
      @j = Option.find_by_id(params[:judge]) or raise_not_found "Judge ID #{params[:judge]}"
      @courts = Court.find(:all, :conditions => ['court_date = ? and judge_id = ? and docket_type_id = ?',@court_date,@j.id,@dt.id])
      @courts = @courts.sort{|a,b| "#{a.docket.person.full_name} #{a.docket.docket_no}" <=> "#{b.docket.person.full_name} #{b.docket.docket_no}"}
      if @courts.empty?
         flash[:warning] = "No Items for That Docket!"
         redirect_to dockets_path and return
      end
      prawnto :prawn => {
         :page_size => 'LEGAL',
         :page_layout => :landscape,
         :left_margin => 20,
         :right_margin => 20,
         :top_margin => 10,
         :bottom_margin => 10 },
         :filename => "#{@dt.long_name}_Docket-#{@court_date.to_s(:db)}.pdf"
      user_action("Dockets","Printed #{@dt.long_name} Docket")
      render :template => "dockets/dockets.pdf.prawn", :layout => false
   end
   
   def date_stamp
      d = DateTime.now.strftime("%Y%m%d%H%M%S")
      render :update do |page|
         page['docket_docket_no'].value = d
      end
   end
   
   protected
   
   # ensures that all actions will have the "Dockets" menu item properly
   # highlighted as current.
   def set_tab_name
      @current_tab = "Dockets"
   end
end
