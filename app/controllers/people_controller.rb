# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# People are "persons of interest", i.e. everybody the officers come into
# contact with, including prisoners, victims, witnesses, etc.
class PeopleController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # People Listing
   def index
      require_permission Perm[:view_person]
      @help_page = ["People","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:person_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:person_filter] = nil
            redirect_to people_path and return
         end
      else
         @query = session[:person_filter] || HashWithIndifferentAccess.new
      end
      unless @people = paged('person',:order => 'people.lastname, people.firstname', :include => [:warrants, :race, :oln_state, :forbids, :bookings], :alpha_name => 'people.lastname')
         @query = HashWithIndifferentAccess.new
         session[:person_filter] = nil
         redirect_to people_path and return
      end
      @links = [['New', edit_person_path],['Photos', url_for(:action => 'photo_album')]]
   end

   # Show Person
   def show
      require_permission Perm[:view_person]
      @help_page = ["People","show"]
      params[:id] or raise_missing "Person ID"
      @person = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
      if @person.alias_for.nil?
         @alias = @person
      else
         @alias = @person.alias_for
      end
      @page_title = "Person ID: #{@person.id}#{@person.legacy? ? ' -- [Legacy]' : ''}"
      @links = []
      @page_links = []
      if !@person.bookings.empty? && !@person.bookings.last.release_date?
         @page_links.push(@person.bookings.last)
      end
      unless @person.alias_for.nil?
         @page_links.push([@person.alias_for,'real i d'])
      end
      @person.aliases.each do |p|
         @page_links.push([p,'alias'])
      end
      unless @person.merge_locked?
         if has_permission(Perm[:update_person])
            @links.push(["New", edit_person_path])
            @links.push(["Edit", edit_person_path(:id => @person.id)])
         end
         if has_permission Perm[:destroy_person]
            @links.push(["Delete", @person, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Person?', :disabled => @person.cannot_be_deleted}])
         end
         @links.push(["Medications", url_for(:action => 'index_meds', :person_id => @person.id)])
         @links.push(["Rap Sheet", url_for(:action => 'rap_sheet', :id => @person.id)])
         @links.push(["View Links", url_for(:action => 'links', :id => @person.id)])
         if has_permission(Perm[:view_offense])
            @links.push(["View Offenses", offenses_path(:person_id => @person.id)])
         end
         if has_permission(Perm[:merge_person]) && !@person.alias_for.nil?
            @links.push(["Merge", url_for(:action => 'merge', :id => @person.id)])
         end
         @linksx = []
         if has_permission(Perm[:update_citation]) && @person.alias_for.nil?
            @linksx.push(["New Citation", edit_citation_path(:person_id => @person.id)])
         end
         if has_permission(Perm[:update_commissary]) && @person.alias_for.nil?
            @linksx.push(["Commissary", edit_commissary_path(:person_id => @person.id)])
         end
         if has_permission(Perm[:update_forbid]) && @person.alias_for.nil?
            @linksx.push(["New Forbid", edit_forbid_path(:person_id => @person.id)])
         end
         if has_permission(Perm[:update_warrant]) && @person.alias_for.nil?
            @linksx.push(["New Warrant", edit_warrant_path(:person_id => @person.id)])
         end
         if has_permission(Perm[:update_transport]) && @person.alias_for.nil?
            @linksx.push(["New Voucher", edit_transport_path(:person_id => @person.id)])
         end
         if @person.bookings.empty? || @person.bookings.last.release_date?
            if has_permission(Perm[:update_booking]) && @person.alias_for.nil?
               @linksx.push(["New Booking", edit_booking_path(:person_id => @person.id)])
            end
         end
         # pull together data for current and past records
         # commissary for past 30 days
         @commissary = []
         if has_permission(Perm[:view_commissary])
            @commissary = @person.past_days(30).reverse
         end
      
         # verified warrants
         @vwarrants = @person.warrants.reject(&:dispo_date?)
            
         # possible warrants
         @pwarrants = @person.wanted(false)
            
         # verified forbids
         @vforbids = @person.forbids.reject(&:recall_date?)
            
         # possible forbids
         @pforbids = @person.forbidden(false)
            
         # active bonds
         @bonds = @person.bonds.reject{|b| b.status.nil? || b.status.long_name != "Active"}
            
         # active probation records
         state = Option.lookup("Probation Status", "State",nil,nil,true)
         unsup = Option.lookup("Probation Status", "Unsupervised",nil,nil,true)
         activ = Option.lookup("Probation Status", "Active",nil,nil,true)
         @probations = Probation.find(:all, :conditions => ['(status_id = ? OR status_id = ? OR status_id = ?) AND person_id = ?',state,unsup,activ,@person.id])
            
         # history records
         @history = @person.arrests.reject{|a| a.charge_summary == 'No Charges'}.collect{|a| [a.arrest_date, a]}
         @history.concat(@person.citations.collect{|c| [c.citation_date, c]})
         @history.concat(@person.transfers.reject{|t| t.transfer_type.nil? || t.transfer_type.long_name =~ /^Temporary/}.collect{|t| [t.transfer_date, t]})
         @history.concat(@person.probations.collect{|p| p.start_date? ? [p.start_date, p] : [p.created_at.to_date, p]})
         @history = @history.reject{|h| h.blank? }.sort{|a,b| a[0] <=> b[0]}
            
         # warrant exemptions
         @wexempt = @person.warrant_exemptions.reject(&:dispo_date?)
         
         # forbid exemtpions
         @fexempt = @person.forbid_exemptions.reject(&:recall_date?)
      end
      @page_links.push([@person.creator, 'creator'],[@person.updater, 'updater'])
   end

   # Add or Edit Person
   def edit
      require_permission Perm[:update_person]
      @help_page = ["People","edit"]
      if params[:id]
         @person = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
         @person.merge_locked? and raise_not_allowed "Edit a Merged Person"
         @person.name = @person.full_name
         @page_title = "Edit Person: #{@person.id}"
         @person.updated_by = session[:user]
         @person.legacy = false
         unless request.post?
            if @person.marks.empty?
               @person.marks.build(:created_by => session[:user], :updated_by => session[:user])
            end
            if @person.associates.empty?
               @person.associates.build(:created_by => session[:user], :updated_by => session[:user])
            end
         end
      else
         @person = Person.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Person")
         unless request.post?
            @person.marks.build(:created_by => session[:user], :updated_by => session[:user])
            @person.associates.build(:created_by => session[:user], :updated_by => session[:user])
         end
         @person.name = ""
         @page_title = "New Person"
      end
      
      if request.post?
         if params[:commit] == "Cancel"
            if @person.new_record?
               redirect_to people_path
            else
               redirect_to @person
            end
            return
         end
         success = true
         params[:person][:existing_mark_attributes] ||= {}
         params[:person][:existing_associate_attributes] ||= {}
         new_id = params[:person][:id]
         if @person.new_record? && has_permission(Perm[:set_person_id])
            unless new_id.blank?
               if Person.find_by_id(new_id)
                  @person.errors.add(:id, 'is already taken!')
                  success = false
               end
            end
         end
         params[:person].delete('id')
         @person.attributes = params[:person]
         if success == true && @person.new_record? && !new_id.blank?
            @person.id = new_id
         end
         unless success == false
            if @person.save
               redirect_to @person
               if params[:id]
                  user_action("People","Edited Person: #{show_person @person}.")
               else
                  user_action("People", "Added Person: #{show_person @person}.")
               end
            end
         end
      end
   end
   
   # Front Mugshot
   def front_mugshot
      require_permission Perm[:update_person]
      @help_page = ["People","front_mugshot"]
      params[:id] or raise_missing "Person ID"
      @person = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
      @page_title = "Front Mugshot"
      @person.merge_locked? and raise_not_allowed "Edit a Merged Person"
      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to @person
            return
         end
         @person.updated_by = session[:user]
         @person.attributes = params[:person]
         if params[:commit] == 'Delete'
            @person.front_mugshot = nil
         end
         if @person.save
            redirect_to @person
            if params[:commit] == 'Delete'
               user_action("People","Removed Front Mugshot for Person: #{show_person @person}.")
            else
               user_action("People","Updated Front Mugshot for Person: #{show_person @person}.")
            end
         end
      end
   end
   
   # Side Mugshot
   def side_mugshot
      require_permission Perm[:update_person]
      @help_page = ["People","side_mugshot"]
      params[:id] or raise_missing "Person ID"
      @person = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
      @page_title = "Side Mugshot"
      @person.merge_locked? and raise_not_allowed "Edit a Merged Person"
      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to @person
            return
         end
         @person.updated_by = session[:user]
         @person.attributes = params[:person]
         if params[:commit] == 'Delete'
            @person.side_mugshot = nil
         end
         if @person.save
            redirect_to @person
            if params[:commit] == 'Delete'
               user_action("People","Removed Side Mugshot for Person: #{show_person @person}.")
            else
               user_action("People","Updated Side Mugshot for Person: #{show_person @person}.")
            end
         end
      end
   end
   
   # shows all available pictures on one page
   def photo_album
      require_permission Perm[:view_person]
      @query = session[:person_filter] ||= HashWithIndifferentAccess.new
      cond = "front_mugshot_file_name != ''"
      unless @query.nil? || @query.empty?
         cond << " AND #{Person.condition_string(@query)}"
      end
      @people = Person.find(:all, :order => 'lastname, firstname, middlename, suffix', :conditions => cond)
      if @people.empty?
         flash[:warning] = "No photos available!"
         redirect_to people_path and return
      end
      @page_title = 'Photo Album: People'
   end

   # Destroys the specified person. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_person]
      require_method :delete
      params[:id] or raise_missing "Person ID"
      @person = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
      @person.merge_locked? and raise_not_allowed "Destroy a Merged Person"
      raise_not_allowed("Destroy this Person: links exist") if @person.cannot_be_deleted
      @person.destroy or raise_db(:destroy, "Person ID #{params[:id]}")
      user_action("People","Destroyed Person ID #{params[:id]}.")
      flash[:notice] = 'Record Deleted!'
      redirect_to people_path
   end
   
   # merge person
   def merge
      require_permission Perm[:merge_person]
      @help_page = ['People','merge']
      params[:id] or raise_missing "Person ID"
      @old_person = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
      @old_person.merge_locked? and raise_not_allowed "Merge This Person: Already Merged"
      @old_person.name = @old_person.full_name
      # make sure alias is set properly
      unless @person = Person.find_by_id(@old_person.is_alias_for)
         flash[:warning] = "Cannot Merge This Person: Real ID Not Set"
         redirect_to @old_person and return
      end
      @person.name = @person.full_name
      # make sure alias is not a loop
      if @old_person == @person
         flash[:warning] = "Cannot Merge This Person: Real ID Is Same As ID"
         redirect_to @old_person and return
      end
      # this next block is required because one person cannot have two
      # open booking records, and if they have 1, it must be the latest one.
      # must check that after the merger, these rules will be true.
      #
      # make sure both persons do not have open bookings
      unless @old_person.bookings.empty? || @person.bookings.empty?
         # both have attached booking records
         #
         # check to see if both are open, which is forbidden!
         unless @old_person.bookings.last.release_date? || @person.bookings.last.release_date?
            # both have open bookings
            flash[:warning] = "Both of these people have open bookings.  Cannot merge these people."
            redirect_to @old_person and return
         end
         #
         # if one booking is open, it must have the highest booking_id
         unless @old_person.bookings.last.release_date? && @person.bookings.last.release_date?
            # both last bookings are not closed
            #
            # check sequencing of booking records
            if (@person.bookings.last.release_date? && @person.bookings.last.id > @old_person.bookings.last.id) || (@old_person.bookings.last.release_date? && @old_person.bookings.last.id > @person.bookings.last.id)
               # the open booking is prior to the closed one which means we cannot merge
               flash[:warning] = "One of these people has an open booking that is earlier than the other persons closed booking.  Cannot merge these people."
               redirect_to @old_person and return
            end
         end
         # both bookings are closed, or one is open and is later than the closed one which is ok - can merge
      end
      @person.legacy = false
      unless @person.commissary_facility_id?
         @person.commissary_facility_id = @old_person.commissary_facility_id
      end
      
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to @old_person
            return
         end
         
         params[:person][:existing_mark_attributes] ||= {}
         params[:person][:existing_associate_attributes] ||= {}
         @person.attributes = params[:person]
         if @person.is_alias_for? && @person.is_alias_for = @person.id
            @person.is_alias_for = nil
         end
         if @person.valid?
            # call global swap_person method
            recs, errs = @old_person.merge_person(@person.id, session[:user])
            if recs == -1
               flash[:warning] = "Merge Failed: Person model returned parameter error!"
               redirect_to @old_person and return
            elsif !errs.empty?
               flash[:warning] = "Merge Failed: parameter errors returned by these models: #{errs.join(', ')}."
               redirect_to @old_person and return
            else
               flash[:notice] = "Merge Succeeded: #{recs} records updated, old person locked!"
               user_action("People", "Merged Person: #{show_person @old_person} into Person: #{show_person @person}!")
               @person.save(false)
               redirect_to @person
               # update old person fields and mark as alias only - read only
               @old_person.update_attribute(:merge_locked, true)
            end
         end
      end
   end
   
   # called from various views using AJAX, validates a person by id and
   # returns the persons name
   def validate
      caller_id = params[:caller_id]
      caller_name = params[:caller_name]
      status = params[:caller_status]
      person_id = params[:id]
      if vperson = Person.find_by_id(params[:id])
         person_name = vperson.full_name
         status_text = "&nbsp;"
         while (!vperson.alias_for.nil?)
            vperson = vperson.alias_for
            status_text = "ID: #{person_id} (#{person_name}) was an alias! Updated to real person."
         end
         render :update do |page|
            page[caller_id].value = vperson.id
            page[caller_name].value = vperson.full_name
            page.replace_html status, status_text
         end
      else
         render :update do |page|
            page[caller_id].value = nil
            page[caller_name].value = nil
            page.replace_html status, "<span class='error'>Invalid ID!</span>"
            page[caller_id].focus
         end
      end
   end
   
   # called by the person_row helper method using an observer, this looks
   # up people records matching the supplied name.
   def lookup
      if has_permission Perm[:view_person]
         @people = Person.locate_by_name(params[:name])
         caller_id = params[:caller_id]
         caller_name = params[:caller_name]
         name = params[:name]
         status = params[:caller_status]
         if @people.empty?
            render :update do |page|
               page[caller_id].value = nil
               page[caller_name].value = nil
               page.replace_html status, "<span class='error'>#{name}: Not Found!</span>"
               page[caller_id].focus
            end
         elsif @people.size == 1
            vperson = @people.first
            person_id = vperson.id
            person_name = vperson.full_name
            status_text = "&nbsp;"
            while (!vperson.alias_for.nil?)
               vperson = vperson.alias_for
               status_text = "ID: #{person_id} (#{person_name}) was an alias! Updated to real person."
            end
            render :update do |page|
               page[caller_id].value = vperson.id
               page[caller_name].value = vperson.full_name
               page.replace_html status, status_text
               page[caller_name].focus
            end
         else
            if name.blank?
               render :text => ""
            else
               path = url_for(:controller => 'people', :action => 'lookup_list', :name => name, :caller_status => params[:caller_status], :caller_id => params[:caller_id], :caller_name => params[:caller_name])
               render :update do |page|
                  page.call 'window.open', path, 'name', 'height=500,width=800'
               end
            end
         end
      else
         render :text => ""
      end
   end
   
   # called from the lookup action, this finds all person records matching a
   # supplied name and displays them in a popup window. Clicking on a person
   # record in the new window will (via javascript) insert the selected id
   # and name into appropriate fields on the original form.
   def lookup_list
      if has_permission Perm[:view_person]
         @people = paged_array(Person.locate_by_name(params[:name]))
         @page_title = "Results for '#{params[:name]}'"
         @caller_id = params[:caller_id]
         @caller_name = params[:caller_name]
         @caller_status = params[:caller_status]
         @status = "&nbsp;"
         render :layout => 'popup'
      else
         render :text => ""
      end
   end
   
   # this is called to hard link a possible warrant to the person
   def verify_warrant
      require_permission Perm[:update_person]
      require_method :post
      params[:id] or raise_missing "Person ID"
      vperson = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
      params[:warrant_id] or raise_missing "Warrant ID"
      warrant = Warrant.find_by_id(params[:warrant_id]) or raise_not_found "Warrant ID #{params[:warrant_id]}"
      warrant.update_attribute(:person_id, vperson.id)
      redirect_to vperson
      user_action("People", "Verified Warrant ID: #{warrant.id} is for Person: #{show_person vperson}!")
   end
   
   # when called with a valid warrant_id, adds the warrant to the person's
   # warrant_exemptions table which stops the warrant showing up on his
   # record unless it is explicitely linked to his person.
   def warrant_exemption
      require_permission Perm[:update_person]
      require_method :post
      params[:id] or raise_missing "Person ID"
      vperson = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
      params[:warrant_id] or raise_missing "Warrant ID"
      warrant = Warrant.find_by_id(params[:warrant_id]) or raise_not_found "Warrant ID #{params[:warrant_id]}"
      if warrant.person_id == vperson.id
         warrant.update_attribute(:person_id, nil)
      end
      if params[:restore] == '1'
         vperson.warrant_exemptions.delete(warrant)
         user_action("People", "Removed Exemption for Warrant ID: #{warrant.id} from Person: #{show_person vperson}!")
      else
         vperson.warrant_exemptions << warrant unless vperson.warrant_exemptions.include?(warrant)
         user_action("People", "Added Exemption for Warrant ID: #{warrant.id} to Person: #{show_person vperson}!")
      end
      redirect_to vperson
   end
   
   # this is called to hard link a possible forbid to the person
   def verify_forbid
      require_permission Perm[:update_person]
      require_method :post
      params[:id] or raise_missing "Person ID"
      vperson = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
      params[:forbid_id] or raise_missing "Forbid ID"
      forbid = Forbid.find_by_id(params[:forbid_id]) or raise_not_found "Forbid ID #{params[:forbid_id]}"
      forbid.update_attribute(:forbidden_id, vperson.id)
      redirect_to vperson
      user_action("People", "Verified Forbid ID: #{forbid.id} is for Person: #{show_person vperson}!")
   end
   
   # when called with a valid forbid_id, adds the forbid to the person's
   # forbid_exemptions table which stops the forbid showing up on his
   # record unless it is explicitely linked to his person.
   def forbid_exemption
      require_permission Perm[:update_person]
      require_method :post
      params[:id] or raise_missing "Person ID"
      vperson = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
      params[:forbid_id] or raise_missing "Forbid ID"
      forbid = Forbid.find_by_id(params[:forbid_id]) or raise_not_found "Forbid ID #{params[:forbid_id]}"
      if forbid.forbidden_id == vperson.id
         forbid.update_attribute(:forbidden_id, nil)
      end
      if params[:restore] == '1'
         vperson.forbid_exemptions.delete(forbid)
         user_action("People", "Removed Exemption for Forbid ID: #{forbid.id} from Person: #{show_person vperson}!")
      else
         vperson.forbid_exemptions << forbid unless vperson.forbid_exemptions.include?(forbid)
         user_action("People", "Added Exemption for Forbid ID: #{forbid.id} to Person: #{show_person vperson}!")
      end
      redirect_to vperson
   end
   
   # displays a page showing all the links to a person
   #
   ## /people/links?id=5
   def links
      require_permission Perm[:view_person]
      params[:id] or raise_missing "Person ID"
      @person = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
      @links = [['Return', @person]]
   end
   
   # Generates a PDF rap sheet for the current record
   def rap_sheet
      require_permission Perm[:view_person]
      params[:id] or raise_missing "Person ID"
      @person = Person.find_by_id(params[:id]) or raise_not_found "Person ID #{params[:id]}"
      @page_title = "RAP SHEET"
      @datetime = DateTime.now.to_s(:us)
      unless @person.merge_locked?
         # pull together data for current and past records
         # verified warrants
         @vwarrants = @person.warrants.reject(&:dispo_date?)
         # possible warrants
         @pwarrants = @person.wanted(false)
         # verified forbids
         @vforbids = @person.forbids.reject(&:recall_date?)
         # possible forbids
         @pforbids = @person.forbidden(false)
         # active bonds
         @bonds = @person.bonds.reject{|b| b.status.nil? || b.status.long_name != "Active"}
         # active probation records
         state = Option.lookup("Probation Status", "State",nil,nil,true)
         unsup = Option.lookup("Probation Status", "Unsupervised",nil,nil,true)
         activ = Option.lookup("Probation Status", "Active",nil,nil,true)
         @probations = Probation.find(:all, :conditions => ['(status_id = ? OR status_id = ? OR status_id = ?) AND person_id = ?',state,unsup,activ,@person.id])
         # history records
         @history = @person.arrests.reject{|a| a.charge_summary == 'No Charges'}.collect{|a| [a.arrest_date, a]}
         @history.concat(@person.citations.collect{|c| [c.citation_date, c]})
         @history.concat(@person.transfers.reject{|t| t.transfer_type.nil? || t.transfer_type.long_name =~ /^Temporary/}.collect{|t| [t.transfer_date, t]})
         @history.concat(@person.probations.collect{|p| p.start_date? ? [p.start_date, p] : [p.created_at.to_date, p]})
         @history = @history.reject{|h| h.blank? }.sort{|a,b| a[0] <=> b[0]}
         # warrant exemptions
         @wexempt = @person.warrant_exemptions.reject(&:dispo_date?)
         # forbid exemtpions
         @fexempt = @person.forbid_exemptions.reject(&:recall_date?)
      end
      prawnto :prawn => {
         :page_size => 'LEGAL',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "rap-sheet-#{@person.id}.pdf"
      render :template => 'people/rap_sheet.pdf.prawn', :layout => false
      user_action("People","Printed Rap Sheet for Person: #{show_person @person}.")
   end
   
   def index_meds
      require_permission Perm[:view_person]
      @help_page = ['People','index_meds']
      params[:person_id] or raise_missing "Person ID"
      @person = Person.find_by_id(params[:person_id]) or raise_not_found "Person ID #{params[:person_id]}"
      @page_title = "Medications for: #{show_person @person}"
      @medications = @person.medications
      @links = [['Person', @person]]
      unless @person.bookings.empty?
         if has_permission(Perm[:view_booking])
            @links.push(['Booking',@person.bookings.last])
         end
      end
      if has_permission(Perm[:update_person])
         @links.push(['New', edit_medication_path(:person_id => @person.id)])
      end
   end
   
   def show_med
      require_permission Perm[:view_person]
      params[:id] or raise_missing "Medication ID"
      @medication = Medication.find_by_id(params[:id]) or raise_not_found "Medication ID #{params[:id]}"
      @links = []
      @page_title = "Medication ID: #{@medication} For #{show_person @medication.person}"
      if has_permission(Perm[:update_person])
         @links.push(['New', edit_medication_path(:person_id => @medication.person_id)])
         @links.push(['Edit', edit_medication_path(:id => @medication.id)])
         @links.push(['Delete', @medication, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Medication?'}])
      end
      @page_links = [@medication.person,[@medication.physician_id,'physician'],[@medication.pharmacy_id,'pharmacy'],[@medication.creator,'creator'],[@medication.updater,'updater']]
   end
   
   def edit_med
      require_permission Perm[:update_person]
      if params[:id]
         @medication = Medication.find_by_id(params[:id]) or raise_not_found "Medication ID #{params[:id]}"
         @medication.updated_by = session[:user]
         @page_title = "Edit Medication ID: #{@medication.id} for #{show_person @medication.person}"
         unless request.post?
            if @medication.med_schedules.empty?
               @medication.med_schedules.build
            end
         end
      else
         params[:person_id] or raise_missing "Person ID"
         @person = Person.find_by_id(params[:person_id]) or raise_not_found "Person ID #{params[:id]}"
         @medication = Medication.new(:person_id => @person.id, :created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Medication")
         if @medication.med_schedules.empty? && !request.post?
            @medication.med_schedules.build
         end
         @page_title = "New Medication"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @medication.new_record?
               redirect_to medications_path(:person_id => @person.id)
            else
               redirect_to @medication
            end
            return
         end
         params[:medication][:existing_schedule_attributes] ||= {}
         @medication.attributes = params[:medication]
         if @medication.save
            redirect_to @medication
            if params[:id]
               user_action("People","Edited Medication ID: #{@medication.id}.")
            else
               user_action("People","Added Medication ID: #{@medication.id}.")
            end
         end
      end
   end
      
   def destroy_med
      require_permission Perm[:update_person]
      require_method :delete
      params[:id] or raise_missing "Medication ID"
      @medication = Medication.find_by_id(params[:id]) or raise_not_found "Medication ID #{params[:id]}"
      return_path = medications_path(:person_id => @medication.person_id)
      @medication.destroy or raise_db(:destroy, "Medication ID #{params[:id]}")
      user_action("People","Destroyed Medication ID: #{params[:id]}")
      flash[:notice] = 'Record Deleted!'
      redirect_to return_path
   end
   
   protected

   # ensures "People" menu item is highlighted for all actions in this
   # controller
   def set_tab_name
      @current_tab = "People"
   end
end
