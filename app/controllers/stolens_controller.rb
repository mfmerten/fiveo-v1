# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Stolen Controller
# Provides access to the stolen items database
class StolensController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Stolen Item Listing
   def index
      require_permission Perm[:view_stolen]
      @help_page = ["Stolen","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:stolen_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:stolen_filter] = nil
            redirect_to stolens_path and return
         end
      else
         @query = session[:stolen_filter] || HashWithIndifferentAccess.new
      end
      unless @stolens = paged('stolen', :order => 'stolen_date DESC', :include => :person)
         @query = HashWithIndifferentAccess.new
         session[:stolen_filter] = nil
         redirect_to stolens_path and return
      end
      if has_permission(Perm[:update_stolen])
         @links = [['New', edit_stolen_path]]
      end
   end

   # Show Stolen Item
   def show
      require_permission Perm[:view_stolen]
      @help_page = ["Stolen","show"]
      params[:id] or raise_missing "Stolen ID"
      @stolen = Stolen.find_by_id(params[:id]) or raise_not_found "Stolen ID #{params[:id]}"
      enforce_privacy(@stolen)
      @links = []
      if has_permission(Perm[:update_stolen])
         @links.push(['New', edit_stolen_path])
         @links.push(['Edit', edit_stolen_path(:id => @stolen.id)])
      end
      if has_permission Perm[:destroy_stolen]
         @links.push(['Delete', @stolen, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Stolen Item?'}])
      end
      @page_links = [[@stolen.person,'owned by'],[@stolen.investigation,'invest'],[@stolen.crime,'crime'],[@stolen.criminal,'criminal'],[@stolen.creator,'creator'],[@stolen.updater,'updater']]
   end

   # Add or Edit Stolen Item
   def edit
      require_permission Perm[:update_stolen]
      @help_page = ["Stolen","edit"]
      if params[:id]
         @stolen = Stolen.find_by_id(params[:id]) or raise_not_found "Stolen ID #{params[:id]}"
         enforce_privacy(@stolen)
         @page_title = "Edit Stolen Item ID: #{@stolen.id.to_s}"
         @stolen.updated_by = session[:user]
      else
         if params[:invest_criminal_id]
            inv_criminal = InvestCriminal.find_by_id(params[:invest_criminal_id]) or raise_not_found "Criminal ID #{params[:invest_criminal_id]}"
            return_path = url_for(inv_criminal)
            owner = inv_criminal.owned_by
            id = inv_criminal.investigation_id
            crime_id = inv_criminal.invest_crime_id
            criminal_id = inv_criminal.id
            priv = inv_criminal.private?
         elsif params[:invest_crime_id]
            inv_crime = InvestCrime.find_by_id(params[:invest_crime_id]) or raise_not_found "Crime ID #{params[:invest_crime_id]}"
            return_path = url_for(inv_crime)
            owner = inv_crime.owned_by
            id = inv_crime.investigation_id
            crime_id = inv_crime.id
            criminal_id = nil
            priv = inv_crime.private?
         elsif params[:investigation_id]
            inv = Investigation.find_by_id(params[:investigation_id]) or raise_not_found "Investigation ID #{params[:investigation_id]}"
            return_path = url_for(inv)
            owner = inv.owned_by
            id = inv.id
            crime_id = nil
            criminal_id = nil
            priv = inv.private?
         else
            return_path = stolens_path
            owner = nil
            id = nil
            crime_id = nil
            criminal_id = nil
            priv = false
         end
         @stolen = Stolen.new(
            :investigation_id => id,
            :invest_crime_id => crime_id,
            :invest_criminal_id => criminal_id,
            :private => priv,
            :owned_by => owner,
            :created_by => session[:user],
            :updated_by => session[:user]
         ) or raise_db(:new, "Stolen")
         @page_title = "New Stolen Item"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @stolen.new_record?
               redirect_to return_path
            else
               redirect_to @stolen
            end
            return
         end
         @stolen.attributes = params[:stolen]
         if @stolen.save
            redirect_to @stolen
            if params[:id]
               user_action("Stolen","Edited Stolen Item ID: #{@stolen.id}.")
            else
               user_action("Stolen", "Added Stolen Item ID: #{@stolen.id}.")
            end
         end
      end
   end

   # Destroys the specified stolen item. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_stolen]
      require_method :delete
      params[:id] or raise_missing "Stolen ID"
      @stolen = Stolen.find_by_id(params[:id]) or raise_not_found "Stolen ID #{params[:id]}"
      enforce_privacy(@stolen)
      if !@stolen.criminal.nil?
         return_path = url_for(@stolen.criminal)
      elsif !@stolen.crime.nil?
         return_path = url_for(@stolen.crime)
      elsif !@stolen.investigation.nil?
         return_path = url_for(@stolen.investigation)
      else
         return_path = stolens_path
      end
      @stolen.destroy or raise_db(:destroy, "Stolen ID #{params[:id]}")
      user_action("Stolen","Destroyed Stolen Item ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to return_path
   end
   
   protected

   # ensures "Stolen" is highlighted in the menu for all actions in this
   # controller.
   def set_tab_name
      @current_tab = "Stolen"
   end
end
