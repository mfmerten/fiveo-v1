# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Booking records are essentially inmate inventory records for the jail. A
# booking record begins when the prisoner is taken into custody at the
# detention facility, and ends when the prisoner is released from custody.
# There must not be more than one active booking record per person.
class Jail3sController < ApplicationController
   before_filter :check_enabled
   before_filter :set_jail_name
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Booking index
   def index
   end
   
   protected
   
   # raises an exception unless Jail3 is enabled in SiteConfig
   def check_enabled
      SiteConfig.jail3_enabled or raise_not_allowed "Access Jail 3: it is disabled in Site Configuration."
   end
   
   # sets this jail's name from SiteConfig
   def set_jail_name
      if SiteConfig.jail3_short_name.blank?
         @jail_name = "Jail 3"
      else
         @jail_name = SiteConfig.jail3_short_name
      end
   end
   
   # Assures that the proper menu item is highlighted for all actions in
   # this controller.
   def set_tab_name
      @current_tab = "#{@jail_name}"
   end
end
