# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Paper Service
class PapersController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Papers Listing
   def index
      require_permission Perm[:view_civil]
      @help_page = ["Papers","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:paper_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:paper_filter] = nil
            redirect_to papers_path and return
         end
      else
         @query = session[:paper_filter] || HashWithIndifferentAccess.new
      end
      unless @papers = paged('paper', :include => :paper_type, :active => "((billable is true and paid_date is null) or (billable is false and served_date is null and returned_date is null))")
         @query = HashWithIndifferentAccess.new
         session[:paper_filter] = nil
         redirect_to papers_path and return
      end
      @links = []
      if session[:show_all_papers]
         @links.push(['Show Active', papers_path(:show_all => '0')])
      else
         @links.push(['Show All', papers_path(:show_all => '1')])
      end
      if has_permission Perm[:update_civil]
         @links.push(['New', edit_paper_path])
      end
      @links.push(['Customers', paper_customers_path])
      @links.push(['Paper Types', paper_types_path])
      # unposted transactions
      @payments = PaperPayment.unposted
   end

   # Show Paper
   def show
      require_permission Perm[:view_civil]
      @help_page = ["Papers","show"]
      params[:id] or raise_missing "Paper ID"
      @paper = Paper.find_by_id(params[:id]) or raise_not_found "Paper ID #{params[:id]}"
      @query = session[:paper_filter] || HashWithIndifferentAccess.new
      @links = []
      if has_permission Perm[:update_civil]
         @links.push(['New', edit_paper_path])
         # don't allow editing a paper if it has been invoiced
         unless @paper.invoice_datetime?
            @links.push(['Edit', edit_paper_path(:id => @paper.id)])
         end
      end
      # don't allow destroy of paper if it has been invoiced.
      if has_permission(Perm[:destroy_civil]) && !@paper.invoice_datetime?
         @links.push(['Delete', @paper, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Paper?'}])
      end
      @links.push(['Info Sheet', url_for(:action => 'info_sheet', :paper_id => @paper.id)])
      if has_permission Perm[:update_civil]
         if @paper.invoice_datetime?
            @links.push(['Invoice', url_for(:action => 'invoice', :paper_id => @paper.id)])
            if @paper.paid_date?
              @links.push(['Receipt', url_for(:action => 'invoice', :paper_id => @paper.id, :receipt => 1)])
            else
              @links.push(['Payment', url_for(:action => 'payment', :customer_id => @paper.paper_customer_id, :invoice_id => @paper.id)])
            end
         else
            unless @paper.service_type.nil?
               if (@paper.service_type.long_name == 'No Service' && @paper.returned_date? and !@paper.noservice_type.nil?) || (@paper.served_date? && @paper.served_to?)
                  @links.push(['Invoice', url_for(:action => 'invoice', :paper_id => @paper.id), {:method => 'post'}])
               end
            end
         end
      end
      @page_links = [[@paper.customer,'customer'],[@paper.officer_id,'Officer'],[@paper.creator,'creator'],[@paper.updater,'updater']]
   end

   # Add or Edit Paper
   def edit
      require_permission Perm[:update_civil]
      @help_page = ["Papers","edit"]
      if params[:id]
         @paper = Paper.find_by_id(params[:id]) or raise_not_found "Paper ID #{params[:id]}"
         if @paper.invoice_datetime?
            flash[:warning] = "Already Invoiced!  Cannot Edit.  Delete Invoice Transaction First!"
            redirect_to @paper and return
         end
         @page_title = "Edit Paper ID: #{@paper.id}"
         @paper.updated_by = session[:user]
      else
         @paper = Paper.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Paper")
         @page_title = "New Paper"
         @paper.state_id = Option.lookup("State","Louisiana",nil,nil,true)
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @paper.new_record?
               redirect_to papers_path
            else
               redirect_to @paper
            end
            return
         end
         @paper.attributes = params[:paper]
         if @paper.save
            redirect_to @paper
            if params[:id]
               user_action("Papers","Edited Paper ID: #{@paper.id}.")
            else
               user_action("Papers", "Added Paper ID: #{@paper.id}.")
            end
         end
      end
   end

   # Destroy Paper
   def destroy
      require_permission Perm[:destroy_civil]
      require_method :delete
      params[:id] or raise_missing "Paper ID"
      @paper = Paper.find_by_id(params[:id]) or raise_not_found "Paper ID #{params[:id]}"
      if @paper.invoice_datetime?
         flash[:warning] = "Cannot Delete - Paper Invoiced!"
         redirect_to @paper and return
      end
      @paper.destroy or raise_db(:destroy, "Paper ID #{params[:id]}")
      user_action("Papers","Destroyed Paper ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to papers_path
   end
   
   # generate a new invoice for the specified customer or reprint an old one
   def invoice
      if request.post?
         # creating new invoices
         require_permission Perm[:update_civil]
         if params[:customer_id]
            # billing all billable papers for this customer
            @customer = PaperCustomer.find_by_id(params[:customer_id]) or raise_not_found "Customer ID #{params[:customer_id]}"
            @customer.valid? or raise_invalid "Customer", "Customer #{@customer.id} (#{@customer.name}) has errors. Fix it first."
            @papers = Paper.all_billable(@customer.id)
         elsif params[:paper_id]
            paper = Paper.find_by_id(params[:paper_id]) or raise_not_found "Paper ID #{params[:paper_id]}"
            if paper.invoice_datetime?
               flash[:warning] = 'Paper already invoiced!'
               redirect_to paper and return
            end
            @customer = paper.customer
            @customer.nil? and raise_invalid "Paper", "Paper ID #{@paper.id} has no valid Customer!"
            @customer.valid? or raise_invalid "Customer", "Customer #{@customer.id} (#{@customer.name}) has errors. Fix it first."
            @papers = [paper]
         else
            raise_missing "Customer ID or Paper ID"
         end
         if @papers.nil? || @papers.empty?
            flash[:warning] = 'Nothing to report!'
            redirect_to papers_path and return
         end
         invoice_datetime = DateTime.now
         # update necessary invoice information for each of the papers
         @papers.each do |p|
            p.invoice_datetime = invoice_datetime
            if p.mileage_only?
               p.invoice_total = BigDecimal.new("0.0",2)
            else
               if p.served_date?
                  p.invoice_total = p.service_fee
               else
                  p.invoice_total = p.noservice_fee
                  p.service_type_id = Option.lookup("Service Type","No Service",nil,nil,true)
                  p.noservice_type_id = Option.lookup("Noservice Type","Unable to Locate",nil,nil,true)
               end
            end
            p.invoice_total += p.mileage * p.mileage_fee
            p.save(false)
            trans = PaperPayment.new(
               :paper_customer_id => p.paper_customer_id,
               :paper_id => p.id,
               :transaction_date => invoice_datetime.to_date,
               :officer => session[:user_name],
               :officer_unit => session[:user_unit],
               :officer_badge => session[:user_badge],
               :charge => p.invoice_total,
               :memo => "Invoicing Paper ID: #{p.id}",
               :created_by => session[:user],
               :updated_by => session[:user]
            ) or raise_db(:new, "Paper Payment")
            unless trans.save(false)
               # something bad happened with the transactaion, clear
               # invoice information so it can be fixed
               p.invoice_total = 0
               p.invoice_datetime = nil
               unless p.remarks?
                  p.remarks = ""
               end
               p.remarks << "WARNING: Invoice transaction failed!"
               p.save(false)
               # remove paper from papers list
               @papers.delete(p)
            end
         end
      else
         # reprinting a single invoice or a receipt
         require_permission Perm[:view_civil]
         params[:paper_id] or raise_missing "Paper ID"
         paper = Paper.find_by_id(params[:paper_id]) or raise_not_found "Paper ID #{params[:paper_id]}"
         unless paper.invoice_datetime?
            flash[:warning] = 'Paper Not Invoiced - cannot reprint!'
            redirect_to paper and return
         end
         # since this is a single paper, need to set @customer nil
         @customer = nil
         @papers = [paper]
         if params[:receipt] && paper.paid_date?
           @receipt = true
         end
      end
      
      # now generate invoices for all of the @papers
      if !@customer.nil?
         user_action("Papers","Printed Invoices for Customer ID: #{@customer.id}.")
      else
         user_action("Papers","Printed Invoice for Paper ID: #{@papers.first.id}.")
      end
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Invoices-#{DateTime.now.to_s(:timestamp)}.pdf"
      render :template => 'papers/invoice.pdf.prawn', :layout => false
   end
   
   # Customer Listing
   def index_customers
      require_permission Perm[:view_civil]
      @help_page = ["Papers","index_customers"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:paper_customer_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:paper_customer_filter] = nil
            redirect_to paper_customers_path and return
         end
      else
         @query = session[:paper_customer_filter] || HashWithIndifferentAccess.new
      end
      unless @customers = paged('paper_customer', :order => 'name')
         @query = HashWithIndifferentAccess.new
         session[:paper_customer_filter] = nil
         redirect_to paper_customers_path and return
      end
      if has_permission Perm[:update_civil]
         @links = [['New', edit_paper_customer_path]]
      end
   end

   # Show Customer
   def show_customer
      require_permission Perm[:view_civil]
      @help_page = ["Papers","show_customer"]
      params[:id] or raise_missing "Customer ID"
      @customer = PaperCustomer.find_by_id(params[:id]) or raise_not_found "Customer ID #{params[:id]}"
      @links = []
      if has_permission(Perm[:update_civil])
         @links.push(['New', edit_paper_customer_path])
         @links.push(['Edit', edit_paper_customer_path(:id => @customer.id)])
      end
      if has_permission(Perm[:destroy_civil])
         @links.push(['Delete', @customer, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Customer?', :disabled => !@customer.can_be_deleted}])
      end
      if has_permission(Perm[:update_civil])
         @links.push([(@customer.disabled? ? "Enable" : "Disable"), url_for(:action => 'toggle_customer', :id => @customer.id), {:method => 'post'}])
         unless @customer.papers.reject{|p| p.invoice_datetime? }.empty?
            @links.push(['Invoice', url_for(:action => 'invoice', :customer_id => @customer.id), {:method => 'post'}])
         end
         unless @customer.papers_for_select.empty?
            @links.push(['Payment', url_for(:action => 'payment', :customer_id => @customer.id)])
         end
      end
      @payments = @customer.past_days(1000).reverse
   end

   # Add or Edit Customer
   def edit_customer
      require_permission Perm[:update_civil]
      @help_page = ["Papers","edit_customer"]
      if params[:id]
         @customer = PaperCustomer.find_by_id(params[:id]) or raise_not_found "Customer ID #{params[:id]}"
         @page_title = "Edit Customer ID: #{@customer.id.to_s}"
         @customer.updated_by = session[:user]
      else
         @customer = PaperCustomer.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Paper Customer")
         @page_title = "New Customer"
         @customer.state_id = Option.lookup("State","Louisiana",nil,nil,true)
         @customer.noservice_fee = SiteConfig.noservice_fee
         @customer.mileage_fee = SiteConfig.mileage_fee
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @customer.new_record?
               redirect_to paper_customers_path
            else
               redirect_to @customer
            end
            return
         end
         @customer.attributes = params[:customer]
         if @customer.save
            redirect_to @customer
            if params[:id]
               user_action("Papers","Edited Paper Customer ID: #{@customer.id}.")
            else
               user_action("Papers", "Added Paper Customer ID: #{@customer.id}.")
            end
         end
      end
   end

   # Destroy Customer.
   def destroy_customer
      require_permission Perm[:destroy_civil]
      require_method :delete
      params[:id] or raise_missing "Customer ID"
      @customer = PaperCustomer.find_by_id(params[:id]) or raise_not_found "Customer ID #{params[:id]}"
      @customer.can_be_deleted or raise_not_allowed "Destroy Customer: in use."
      @customer.destroy or raise_db(:destroy, "Customer ID #{params[:id]}")
      user_action("Papers","Destroyed Paper Customer ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to paper_customers_path
   end
   
   # this is called update customer fee schedules when site configuration 
   # defaults change
   def update_fees
      require_permission Perm[:update_civil]
      require_method :post
      params[:status] or raise_missing "Status"
      status = params[:status]
      valid_status = ['noservice','mileage','all']
      valid_status.include?(status) or raise_invalid "Status '#{params[:status]}'", valid_status
      if params[:id]
         @customer = PaperCustomer.find_by_id(params[:id]) or raise_not_found "Customer ID #{params[:id]}"
         if status == 'noservice' || status == 'all'
            @customer.update_noservice_fee(SiteConfig.noservice_fee)
         end
         if status == 'mileage' || status == 'all'
            @customer.update_mileage_fee(SiteConfig.mileage_fee)
         end
         user_action("Papers","Updated Customer ID: #{@customer.id} Fees (#{status.titleize}).")
         redirect_to @customer
      else
         if status == 'noservice' || status == 'all'
            PaperCustomer.update_noservice_fee(SiteConfig.noservice_fee)
         end
         if status == 'mileage' || status == 'all'
            PaperCustomer.update_mileage_fee(SiteConfig.mileage_fee)
         end
         user_action("Papers","Updated All Customer Fees (#{status.titleize}).")
         redirect_to paper_customers_path
      end
   end
   
   # show paper payment
   def show_payment
      require_permission Perm[:view_civil]
      @help_page = ["Papers","show_payment"]
      params[:id] or raise_missing("Paper Payment ID")
      @payment = PaperPayment.find_by_id(params[:id]) or raise_not_found "Paper Payment ID #{params[:id]}"
      if @payment.payment? && @payment.payment > 0
         @page_title = "Paper Payment Transaction ID #{@payment.id}"
      else
         @page_title = "Paper Invoice Transaction ID #{@payment.id}"
      end
      @links = []
      unless @payment.report_datetime? || @payment.trans_not_deletable?
         @links.push(["Delete", @payment, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Transaction?'}])
      end
      @page_links = [[@payment.customer,'customer'],[@payment.paper,'Paper'],[@payment.creator,'creator'],[@payment.updater,'updater']]
   end
   
   # destroy paper payment
   def destroy_payment
      require_permission(Perm[:update_civil])
      require_method(:delete)
      params[:id] or raise_missing("Paper Payment ID")
      @payment = PaperPayment.find_by_id(params[:id]) or raise_not_found("Paper Payment ID #{params[:id]}")
      paper = @payment.paper
      cust = @payment.customer
      @payment.trans_not_deletable? and raise_not_allowed("Destroy This Transaction")
      # check for posted transaction
      @payment.report_datetime? and raise_not_allowed("Destroy Posted Transactions")
      # check if trying to delete an invoice transaction after the invoice has
      # been paid (must delete the payment transaction first)
      !@payment.paper.nil? && @payment.paper.paid_date? && !(@payment.payment? && @payment.payment > 0) and raise_not_allowed("Destroy an Invoice that has been paid: remove the payment first!")
      @payment.destroy or raise_db(:destroy, "Paper Payment ID #{params[:id]}")
      flash[:notice] = "Record Deleted!"
      if paper.nil?
         redirect_to cust
      else
         redirect_to paper
      end
   end
   
   # this enables/disables the customer
   def toggle_customer
      require_permission Perm[:update_civil]
      require_method :post
      params[:id] or raise_missing "Customer ID"
      @customer = PaperCustomer.find_by_id(params[:id]) or raise_not_found "Customer ID #{params[:id]}"
      @customer.toggle!(:disabled)
      redirect_to @customer
   end
   
   # paper type index
   def index_paper_type
      require_permission Perm[:view_civil]
      @help_page = ["Papers","index_paper_type"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:paper_type_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:paper_type_filter] = nil
            redirect_to paper_types_path and return
         end
      else
         @query = session[:paper_type_filter] || HashWithIndifferentAccess.new
      end
      unless @paper_types = paged('paper_type', :order => 'name', :active => 'disabled is false')
         @query = HashWithIndifferentAccess.new
         session[:paper_type_filter] = nil
         redirect_to paper_types_path and return
      end
      @links = []
      if session[:show_all_paper_types].nil?
         session[:show_all_paper_types] = false
      end
      if session[:show_all_paper_types]
         @links.push(['Show Active', paper_types_path(:show_all => '0')])
      else
         @links.push(['Show All', paper_types_path(:show_all => '1')])
      end
      if has_permission(Perm[:update_civil])
         @links.push(['New', edit_paper_type_path])
      end
   end
   
   # show paper type
   def show_paper_type
      require_permission Perm[:view_civil]
      @help_page = ["Papers","show_paper_type"]
      params[:id] or raise_missing "Paper Type ID"
      @paper_type = PaperType.find_by_id(params[:id]) or raise_not_found "Paper Type ID #{params[:id]}"
      @links = []
      if has_permission(Perm[:update_civil])
         @links.push(['New', edit_paper_type_path])
         @links.push(['Edit', edit_paper_type_path(:id => @paper_type.id)])
         if @paper_type.papers.empty?
            @links.push(['Delete', @paper_type, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Paper Type?'}])
         end
         @links.push([(@paper_type.disabled? ? "Enable" : "Disable"), url_for(:action => 'toggle_paper_type', :id => @paper_type.id), {:method => 'post'}])
      end
   end

   # add or edit paper type
   def edit_paper_type
      require_permission Perm[:update_civil]
      @help_page = ["Papers","edit_paper_type"]
      if params[:id]
         @paper_type = PaperType.find_by_id(params[:id]) or raise_not_found "Paper Type ID #{params[:id]}"
         @page_title = "Edit Paper Type ID: #{@paper_type.id.to_s}"
         @paper_type.updated_by = session[:user]
      else
         @paper_type = PaperType.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Paper Type")
         @page_title = "New Paper Type"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @paper_type.new_record?
               redirect_to paper_types_path
            else
               redirect_to @paper_type
            end
            return
         end
         @paper_type.attributes = params[:paper_type]
         if @paper_type.save
            redirect_to @paper_type
            if params[:id]
               user_action("Papers","Edited Paper Type ID: #{@paper_type.id}.")
            else
               user_action("Papers", "Added Paper Type ID: #{@paper_type.id}.")
            end
         end
      end
   end

   # destroy paper type
   # only requires Update permission so that paper service personnel can maintain
   # the paper type table
   def destroy_paper_type
      require_permission Perm[:update_civil]
      require_method :delete
      params[:id] or raise_missing "Paper Type ID"
      @paper_type = PaperType.find_by_id(params[:id]) or raise_not_found "Paper Type ID #{params[:id]}"
      unless @paper_type.papers.empty?
         flash[:warning] = 'Paper type in use: cannot delete! (try disabling it instead)'
         redirect_to @paper_type and return
      end
      @paper_type.destroy or raise_db(:destroy, "Paper Type ID #{params[:id]}")
      user_action("Papers","Destroyed Paper Type ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to paper_types_path
   end
   
   # allows enable/disable paper type
   def toggle_paper_type
      require_permission Perm[:update_civil]
      require_method :post
      params[:id] or raise_missing "Paper Type ID"
      @paper_type = PaperType.find_by_id(params[:id]) or raise_not_found "Paper Type ID #{params[:id]}"
      @paper_type.toggle!(:disabled)
      redirect_to @paper_type
   end
   
   # called from the paper edit form, this looks up the supplied serve_to name
   # and if previously used, will autofill address fields from the previous
   # record.
   def autofill_serve_to
      paper_street = ''
      paper_city = ''
      paper_state_id = ''
      paper_zip = ''
      paper_phone = ''
      paper_mileage = '0.0'
      paper_defendant = "#{params[:paper][:defendant]}"
      paper_prior = ''
      if params[:name]
         if (paper = Paper.find_by_name(params[:name])) && has_permission(Perm[:view_civil])
            paper_street = "#{paper.street}"
            paper_city = "#{paper.city}"
            paper_state_id = "#{paper.state_id}"
            paper_zip = "#{paper.zip}"
            paper_phone = "#{paper.phone}"
            paper_mileage = "#{paper.mileage}"
            if paper_mileage.blank?
               paper_mileage = "0.0"
            end
            unless paper.noservice_type.nil?
               paper_prior = "<span class='error'>Prior Attempt: #{paper.noservice_type.long_name} #{paper.noservice_extra}</span>"
            end
         end
         if paper_defendant.blank?
            paper_defendant = "#{params[:name]}"
         end
         render :update do |page|
            page['paper_street'].value = paper_street
            page['paper_city'].value = paper_city
            page['paper_state_id'].value = paper_state_id
            page['paper_zip'].value = paper_zip
            page['paper_phone'].value = paper_phone
            page['paper_mileage'].value = paper_mileage
            page['paper_defendant'].value = paper_defendant
            page.replace_html 'prior-status', paper_prior
         end
      else
         render :text => ''
      end
   end
   
   # call from paper edit form to clear "prior-status" span when address is
   # modified
   def clear_prior
      render :update do |page|
         page.replace_html 'prior-status', "&nbsp;"
      end
   end
   
   # called from paper edit form, this looks up the supplied invoice_no and
   # if previously used, will autofill suit information.
   def autofill_suit_info
      if (paper = Paper.find_by_suit(params[:suit])) && has_permission(Perm[:view_civil])
         render :update do |page|
            page['paper_plaintiff'].value = paper.plaintiff
            page['paper_defendant'].value = paper.defendant
            page['paper_paper_customer_id'].value = paper.paper_customer_id
         end
      else
         render :text => ''
      end
   end
   
   # updates the paper form after the customer is selected
   def autofill_customer_info
      if customer = PaperCustomer.find_by_id(params[:customer_id])
         render :update do |page|
            page['paper_noservice_fee'].value = number_to_currency(customer.noservice_fee, :format => "%n")
            page['paper_mileage_fee'].value = number_to_currency(customer.mileage_fee, :format => "%n")
         end
      else
         render :text => ''
      end
   end
   
   # updates the paper form after the paper type is selected
   def autofill_paper_info
      if ptype = PaperType.find_by_id(params[:paper_type_id])
         render :update do |page|
            page['paper_service_fee'].value = number_to_currency(ptype.cost, :format => "%n")
         end
      else
         render :text => ''
      end
   end

   # if personal service, copy serve_to name to served_to field
   def autofill_served_to
      stype = Option.find_by_id(params[:service_type_id])
      if !stype.nil? && stype.long_name == 'Personal' && params[:paper][:serve_to]
         render :update do |page|
            page['paper_served_to'].value = "#{params[:paper][:serve_to]}"
         end
      else
         render :text => ''
      end
   end

   # Generates a PDF information sheet for the specified paper
   def info_sheet
      require_permission Perm[:view_civil]
      params[:paper_id] or raise_missing "Paper ID"
      @paper = Paper.find_by_id(params[:paper_id]) or raise_not_found "Paper ID #{params[:paper_id]}"
      user_action("Papers","Printed Information Sheet for Paper ID: #{@paper.id}.")
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Paper_Info-#{@paper.id}.pdf"
      render :template => 'papers/info_sheet.pdf.prawn', :layout => false
   end
   
   # Generates a PDF service status report
   def service_status
      require_permission Perm[:view_civil]
      params[:court_date] or raise_missing "Court Date"
      @court_date = valid_date(params[:court_date]) or raise_invalid "Court Date", "'#{params[:court_date]}' is not a valid date."
      @papers = Paper.find(:all, :conditions => {:court_date => @court_date})
      if @papers.empty?
         flash[:warning] = 'Nothing to report!'
         redirect_to papers_path and return
      end
      user_action("Papers","Printed Service Status Report for #{format_date(@court_date)}.")
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Service_Status-#{format_date(@court_date, :timestamp => true)}.pdf"
      render :template => 'papers/service_status.pdf.prawn', :layout => false
   end
   
   def history
      require_permission Perm[:view_civil]
      params[:customer_id] or raise_missing "Customer ID"
      @customer = PaperCustomer.find_by_id(params[:customer_id]) or raise_not_found "Customer ID #{params[:customer_id]}"
      @from_date = valid_date(params[:from_date])
      @to_date = valid_date(params[:to_date])
      conditions = "paper_customer_id = #{@customer.id} AND payment IS NOT NULL AND payment > 0"
      unless @from_date.nil?
         conditions << " AND transaction_date >= '#{@from_date.to_s(:db)}'"
      end
      unless @to_date.nil?
         conditions << " AND transaction_date <= '#{@to_date.to_s(:db)}'"
      end
      @payments = PaperPayment.find(:all, :conditions => conditions)
      if @payments.empty?
         flash[:warning] = 'Nothing to report!'
         redirect_to papers_path and return
      end
      user_action("Papers","Printed Payment History for #{@customer.name}.")
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Payment_History-#{@customer.id}.pdf"
      render :template => 'papers/history.pdf.prawn', :layout => false
   end
   
   # Generates a PDF report showing all customers that could be billed
   def billing_status
      require_permission Perm[:view_civil]
      params[:status] or raise_missing "Status"
      @status = params[:status]
      valid_status = ['billable','payable']
      valid_status.include?(@status) or raise_invalid "Status '#{params[:status]}'", valid_status
      @customer = PaperCustomer.find_by_id(params[:customer_id])
      if @status == 'billable'
         if @customer.nil?
            @papers = Paper.all_billable.sort{|a,b| a.paper_customer_id.to_s.rjust(6,'0') + a.id.to_s.rjust(6,'0') <=> b.paper_customer_id.to_s.rjust(6,'0') + b.id.to_s.rjust(6,'0')}
         else
            @papers = Paper.all_billable(@customer.id).sort{|a,b| a.id.to_s.rjust(6,'0') <=> b.id.to_s.rjust(6,'0')}
         end
      else
         if @customer.nil?
            @papers = Paper.all_payable.sort{|a,b| a.paper_customer_id.to_s.rjust(6,'0') + a.id.to_s.rjust(6,'0') <=> b.paper_customer_id.to_s.rjust(6,'0') + b.id.to_s.rjust(6,'0')}
         else
            @papers = Paper.all_payable(@customer.id).sort{|a,b| a.id.to_s.rjust(6,'0') <=> b.id.to_s.rjust(6,'0')}
         end
      end
      if @papers.empty?
         flash[:warning] = "Nothing to report!"
         redirect_to papers_path and return
      end
      @report_datetime = DateTime.now
      user_action("Papers","Printed #{@status.titleize} Papers Report.")
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "#{@status.titleize}_Papers-#{format_date(@report_datetime, :timestamp => true)}.pdf"
      render :template => 'papers/billing_status.pdf.prawn', :layout => false
   end

   # make a payment transaction
   def payment
      require_permission Perm[:update_civil]
      @help_page = ["Papers","payment"]
      params[:customer_id] or raise_missing "Customer ID"
      @customer = PaperCustomer.find_by_id(params[:customer_id]) or raise_not_found "Customer ID #{params[:customer_id]}"
      if @customer.papers_for_select.empty?
         flash[:warning] = "No unpaid papers!"
         redirect_to @customer and return
      end
      @paper_payment = PaperPayment.new(:created_by => session[:user], :updated_by => session[:user], :paper_customer_id => @customer.id) or raise_db(:new, "Paper Payment")
      if params[:invoice_id]
         if (paper = Paper.find_by_id(params[:invoice_id])) && paper.paper_customer_id == @customer.id
            if paper.paid_date?
               flash[:warning] = "Paper ID #{paper.id} Has Already Been Paid!"
               redirect_to paper and return
            end
            @paper_payment.paper_id = paper.id
            @paper_payment.payment = paper.invoice_total
         end
      end
      @paper_payment.transaction_date = Date.today
      if !current_user.contact.nil?
         @paper_payment.officer_id = current_user.contact.id
      else
         @paper_payment.officer = session[:user_name]
         @paper_payment.officer_unit = session[:user_unit]
         @paper_payment.officer_badge = session[:user_badge]
      end
      @page_title = "New Payment"
      if request.post?
         if params[:commit] == "Cancel"
            if @paper_payment.paper.nil?
               redirect_to @customer and return
            else
               redirect_to @paper_payment.paper and return
            end
         end
         @paper_payment.attributes = params[:paper_payment]
         if @paper_payment.save
            redirect_to @paper_payment
            user_action("Papers", "Added Paper Payment transaction (ID: #{@paper_payment.id}) for Customer ID: #{@customer.id} (#{@customer.name}).")
         end
      end
   end
   
   # make an adjustment transaction
   def adjustment
      require_permission Perm[:update_civil]
      @help_page = ["Papers","adjustment"]
      params[:customer_id] or raise_missing "Customer ID"
      @customer = PaperCustomer.find_by_id(params[:customer_id]) or raise_not_found "Customer ID #{params[:customer_id]}"
      @paper_payment = PaperPayment.new(:created_by => session[:user], :updated_by => session[:user], :paper_customer_id => @customer.id, :payment_method => "Adjustment") or raise_db(:new, "Paper Payment")
      @paper_payment.transaction_date = Date.today
      if !current_user.contact.nil?
         @paper_payment.officer_id = current_user.contact.id
      else
         @paper_payment.officer = session[:user_name]
         @paper_payment.officer_unit = session[:user_unit]
         @paper_payment.officer_badge = session[:user_badge]
      end
      @page_title = "New Adjustment"
      if request.post?
         if params[:commit] == "Cancel"
            redirect_to @customer and return
         end
         @paper_payment.attributes = params[:paper_payment]
         @paper_payment.adjust = true
         if @paper_payment.save
            redirect_to @paper_payment
            user_action("Papers", "Added Paper Payment transaction (ID: #{@paper_payment.id}) for Customer ID: #{@customer.id} (#{@customer.name}).")
         end
      end
   end
   
   # updates payment when paper is selected
   def update_payment
      if p = Paper.find_by_id(params[:i])
         render :update do |page|
            page.replace_html 'invoice_total', "#{number_to_currency(p.invoice_total)}"
            page['paper_payment_payment'].value = p.invoice_total
         end
      else
         render :update do |page|
            page.replace_html 'invoice_total', "#{number_to_currency(0)}"
            page['paper_payment_payment'].value = 0
         end
      end
   end
   
   # updates payment adjustment when payment amt is entered
   def update_adjustment
      if (p = Paper.find_by_id(params[:paper_payment][:paper_id])) && (payment = params[:p])
         adj = (BigDecimal.new(payment,2) - p.invoice_total)
         render :update do |page|
            page['paper_payment_charge'].value = adj
            page.replace_html 'charge-div', "#{number_to_currency(adj)}"
         end
      else
         render :text => ''
      end
   end
   
   # this generates an unposted transaction report
   def posting_report
      require_permission Perm[:view_civil]
      params[:status] or raise_missing "Status"
      @status = params[:status]
      valid_status = ['post','preview','reprint']
      valid_status.include?(@status) or raise_invalid "Status '#{params[:status]}'", valid_status
      if @status == 'post'
         require_method :post
         require_permission Perm[:update_civil]
      end
      if @status == 'reprint'
         params[:post_datetime] or raise_missing "Posting Date"
         report_datetime = valid_datetime(params[:post_datetime]) or raise_invalid "Posting Date","'#{params[:post_datetime]}' is not a valid date."
         @payments = PaperPayment.posted(report_datetime)
         @report_time = report_datetime
      else
         @payments = PaperPayment.unposted
         @report_time = DateTime.now
      end
      @timestamp = DateTime.now.to_s(:timestamp)
      if @payments.empty?
         flash[:warning] = 'Nothing to report!'
         redirect_to papers_path and return
      end
      if @status == 'post' || @status == 'reprint'
         prawnto :prawn => {
            :page_size => 'LETTER',
            :top_margin => 10,
            :bottom_margin => 10,
            :left_margin => 20,
            :right_margin => 20},
            :filename => "Paper_Posting_Report#{@status == 'reprint' ? '_Reprint' : ''}-#{@timestamp}.pdf"
         user_action("Papers", "#{@status == 'reprint' ? 'Reprinted' : 'Printed'} Paper Posting Report for #{format_date(@report_time)}.")
      else
         prawnto :prawn => {
            :page_size => 'LETTER',
            :top_margin => 10,
            :bottom_margin => 10,
            :left_margin => 20,
            :right_margin => 20},
            :filename => "Paper_Posting_Preview-#{@timestamp}.pdf"
         user_action("Papers", "Printed Paper Posting Preview Report.")
      end
      render :template => 'papers/posting_report.pdf.prawn', :layout => false
   end
   
   protected

   # ensures "Papers" is highlighted in the menu for all actions in this
   # controller.
   def set_tab_name
      @current_tab = "Papers"
   end
end
