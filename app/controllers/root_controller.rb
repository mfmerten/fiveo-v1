# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Root Controller contains the web server root ('/') namespace. This includes
# a dynamically generated site.css file stored locally as a site.css.erb
# view.
class RootController < ApplicationController
   skip_before_filter :login_required, :only => ['site']
   before_filter :update_session_active, :except => ['site']

   # This displays the application home page.
   def index
      @help_page = ["Home","index"]
      @current_tab = "Home"
      @page_title = "Welcome " + session[:user_name]
      @announcements = Announcement.find(:all, :order => 'updated_at DESC').reject{|a| a.ignorers.include?(current_user)}
      @calendars = []
      (-2..5).each do |x|
         date = Date.today + x.months
         @calendars.push([date, Date::MONTHNAMES[(date - 1.month).month], Date::MONTHNAMES[(date.month)], Date::MONTHNAMES[(date + 1.month).month]])
      end
      @arrests = @bugs = @calls = @forbids = @pawns = @warrants = nil
      if has_permission(Perm[:view_arrest]) && current_user.review_arrest?
         @arrests = review_paged('arrest',:include => :person_review, :limit => 3)
         @arrest_count = Arrest.review_count(session[:user])
      end
      if has_permission(Perm[:view_bug]) && current_user.review_bug?
         @bugs = review_paged('bug',:include => [:priority, :assigned, :reporter, :admin, :status, :resolution], :limit => 3)
         @bug_count = Bug.review_count(session[:user])
      end
      if has_permission(Perm[:view_call]) && current_user.review_call?
         @calls = review_paged('call',:include => [:received_via,:signal_code,:call_units,:call_subjects], :limit => 3)
         @call_count = Call.review_count(session[:user])
      end
      if has_permission(Perm[:view_forbid]) && current_user.review_forbid?
         @forbids = review_paged('forbid', :include => :person, :limit => 3)
         @forbid_count = Forbid.review_count(session[:user])
      end
      if has_permission(Perm[:view_pawn]) && current_user.review_pawn?
         @pawns = review_paged('pawn',:include => [:pawn_items,:pawn_co,:ticket_type], :limit => 3)
         @pawn_count = Pawn.review_count(session[:user])
      end
      if has_permission(Perm[:view_warrant]) && current_user.review_warrant?
         @warrants = review_paged('warrant', :include => :person, :limit => 3)
         @warrant_count = Warrant.review_count(session[:user])
      end
      @links = [["Show All", url_for(:controller => 'announcements', :action => 'announcements')]]
   end

   # This is a show case action that queries each model in the system to
   # get a list of all records that reference the specified case number
   # (case parameter).
   def show_case
      require_permission Perm[:view_case]
      params[:case] or raise_missing "Case Number"
      @current_tab = "Home"
      @case = params[:case]
      @case_notes = CaseNote.find_all_by_case_no(@case)
      @links = [["New", edit_case_note_path(:case => @case)]]

      models = []
      Dir.chdir(File.join(RAILS_ROOT, "app/models")) do 
         models = Dir["*.rb"]
      end
      @items = []
      models.each do |m|
         # handle case notes separately
         next if m == 'case_note.rb'
         next if m == 'invest_case.rb'
         model_name = m.sub(/\.rb$/,'')
         class_name = model_name.camelize
         if (class_name.constantize.respond_to?('column_names') && class_name.constantize.column_names.include?('case_no')) || model_name == 'investigation' || model_name == 'docket'
            @items.push({'name' => class_name.pluralize, 'view' => model_name.pluralize, 'partial' => model_name, 'records' => class_name.constantize.find_all_by_case_no(@case).reject{|r| r.respond_to?('private') && r.private? && !((r.respond_to?('owned_by') && !r.owned_by.nil?) ? r.owned_by == session[:user] : (r.respond_to?('created_by') ? r.created_by == session[:user] : true))}})
         end
      end
      @related = Investigation.related_cases(@case)
      drefs = Docket.find_all_by_case_no(@case)
      @docket_references = []
      drefs.collect{|d| d.id}.each do |did|
         d = Docket.find(did)
         unless d.case_numbers.reject{|c| c == @case}.empty?
            @docket_references.push(d)
         end
      end
   end
   
   # called to update the hours to hours/days/months/years calculator on
   # the application layout
   def h_to_hdmy
      # preset date/time values
      start_date = valid_date(params[:conv_start_date], :allow_nil => false)
      start_datetime = get_datetime(start_date,"00:00")
      stop_date = current_user.valid_date(params[:conv_stop_date], :allow_nil => false)
      stop_datetime = get_datetime(stop_date,"00:00")
      #
      if params[:conv_total_hours]
         # first section changed
         total_hours = params[:conv_total_hours].to_i
         if total_hours >= 0
            hours, days, months, years = hours_to_sentence(total_hours)
            stop_date = (start_datetime + total_hours.hours).to_date
            render :update do |page|
               page['conv_total_hours'].value = total_hours
               page['conv_hours'].value = hours
               page['conv_days'].value = days
               page['conv_months'].value = months
               page['conv_years'].value = years
               page['conv_start_date'].value = format_date(start_date)
               page['conv_stop_date'].value = format_date(stop_date)
            end
         else
            render :text => ''
         end
         return
      elsif params[:conv_hours]
         # middle section changed
         hours = params[:conv_hours]
         days = params[:conv_days]
         months = params[:conv_months]
         years = params[:conv_years]
         total_hours = sentence_to_hours(hours,days,months,years)
         stop_date = (start_datetime + total_hours.hours).to_date
         render :update do |page|
            page['conv_total_hours'].value = total_hours
            page['conv_hours'].value = hours.to_i
            page['conv_days'].value = days.to_i
            page['conv_months'].value = months.to_i
            page['conv_years'].value = years.to_i
            page['conv_start_date'].value = format_date(start_date)
            page['conv_stop_date'].value = format_date(stop_date)
         end
         return
      else
         # assume bottom section changed
         diff = hours_diff(start_datetime, stop_datetime)
         hours, days, months, years = hours_to_sentence(diff)
         render :update do |page|
            page['conv_total_hours'].value = diff
            page['conv_hours'].value = hours
            page['conv_days'].value = days
            page['conv_months'].value = months
            page['conv_years'].value = years
            page['conv_start_date'].value = format_date(start_date)
            page['conv_stop_date'].value = format_date(stop_date)
         end
         return
      end
   end

   # Site wide dynamic CSS file.
   def site
      respond_to do |format|
         format.css
      end
   end

   # Global Search (Sphinx)
   def global_search
      classes = []
      classes.push(Person) if has_permission(Perm[:view_person])
      classes.push(Contact) if has_permission(Perm[:view_contact])
      classes.push(Call) if has_permission(Perm[:view_call])
      classes.push(Arrest) if has_permission(Perm[:view_arrest])
      classes.push(CaseNote) if has_permission(Perm[:view_case])
      classes.push(Citation) if has_permission(Perm[:view_citation])
      classes.push(Evidence) if has_permission(Perm[:view_evidence])
      classes.push(Forbid) if has_permission(Perm[:view_forbid])
      classes.push(History) if has_permission(Perm[:view_history]) && SiteConfig.show_history
      classes.push(Offense) if has_permission(Perm[:view_offense])
      classes.push(Pawn) if has_permission(Perm[:view_pawn])
      classes.push(Stolen) if has_permission(Perm[:view_stolen])
      classes.push(Warrant) if has_permission(Perm[:view_warrant])
      classes.push(Booking) if has_permission(Perm[:view_booking])
      classes.push(Investigation) if has_permission(Perm[:view_investigation])
      classes.push(InvestCase) if has_permission(Perm[:view_investigation])
      classes.push(InvestCrime) if has_permission(Perm[:view_investigation])
      classes.push(InvestCriminal) if has_permission(Perm[:view_investigation])
      classes.push(InvestPhoto) if has_permission(Perm[:view_investigation])
      classes.push(InvestReport) if has_permission(Perm[:view_investigation])
      classes.push(InvestVehicle) if has_permission(Perm[:view_investigation])
      classes.push(Sentence) if has_permission(Perm[:view_court])
      classes.push(Probation) if has_permission(Perm[:view_probation])
      classes.push(Doc) if has_permission(Perm[:view_booking])
      if params[:search]
         session[:global_search] = params[:search]
      end
      @search = session[:global_search]
      if classes.empty?
         redirect_to root_path and return
      end
      @results = ThinkingSphinx.search @search, :classes => classes, :order => "updated_at DESC", :page => params[:page], :per_page => session[:items_per_page]
      @page = @results.current_page
      @pages = @results.total_pages
   end
end
