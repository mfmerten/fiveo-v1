# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Wants and warrants.
class WarrantsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Warrant Listing
   def index
      require_permission Perm[:view_warrant]
      @help_page = ["Warrants","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:warrant_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:warrant_filter] = nil
            redirect_to warrants_path and return
         end
      else
         @query = session[:warrant_filter] || HashWithIndifferentAccess.new
      end
      unless @warrants = paged('warrant', :order => 'warrants.lastname, warrants.firstname, warrants.middle1, warrants.middle2, warrants.middle3', :include => [:person, :jurisdiction, :disposition], :active => 'warrants.dispo_date IS NULL', :skip_query => ['disposition','disposition_from','disposition_to'], :alpha_name => 'warrants.lastname')
         @query = HashWithIndifferentAccess.new
         session[:warrant_filter] = nil
         redirect_to warrants_path and return
      end
      @links = []
      if session[:show_all_warrants]
         @links.push(['Show Active', warrants_path(:show_all => '0')])
      else
         @links.push(['Show All', warrants_path(:show_all => '1')])
      end
      if has_permission(Perm[:update_warrant])
         @links.push(['New', edit_warrant_path])
      end
      @links.push(['PDF', url_for(:action => 'list_form')])
      if current_user.review_warrant?
         @links.push(['Review', url_for(:action => 'review')])
      end
   end

   # Show Warrant
   def show
      require_permission Perm[:view_warrant]
      @help_page = ["Warrants","show"]
      params[:id] or raise_missing "Warrant ID"
      @warrant = Warrant.find_by_id(params[:id]) or raise_not_found "Warrant ID #{params[:id]}"
      enforce_privacy(@warrant)
      @page_title = "Warrant ID: #{@warrant.id}#{@warrant.legacy? ? ' -- [Legacy]' : ''}"
      @links = []
      if has_permission(Perm[:update_warrant])
         @links.push(['New', edit_warrant_path])
         @links.push(['Edit', edit_warrant_path(:id => @warrant.id)])
      end
      if has_permission Perm[:destroy_warrant]
         @links.push(['Delete', @warrant, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Warrant?'}])
      end
      unless @warrant.person.nil? || @warrant.person.bookings.empty?
         unless @warrant.person.bookings.last.release_date?
            if has_permission(Perm[:update_arrest])
               @links.push(['Serve', edit_arrest_path(:person_id => @warrant.person_id, :warrant_id => @warrant.id, :booking_id => @warrant.person.bookings.last.id)])
            end
         end
      end
      @page_links = [@warrant.person, [@warrant.jurisdiction_id,'agency']]
      @page_links.concat(@warrant.district_arrests + @warrant.city_arrests + @warrant.other_arrests)
      @page_links.push([@warrant.investigation,'invest'],[@warrant.crime,'crime'],[@warrant.criminal,'criminal'],[@warrant.creator,'creator'],[@warrant.updater,'updater'])
   end

   # Add or Edit Warrant
   def edit
      require_permission Perm[:update_warrant]
      @help_page = ["Warrants","edit"]
      if params[:id]
         @warrant = Warrant.find_by_id(params[:id]) or raise_not_found "Warrant ID #{params[:id]}"
         enforce_privacy(@warrant)
         @page_title = "Edit Warrant ID: #{@warrant.id}"
         @warrant.updated_by = session[:user]
         @warrant.legacy = false
         @warrant.name = @warrant.full_name
         unless request.post?
            if @warrant.charges.empty?
               @warrant.charges.build(:created_by => session[:user], :updated_by => session[:user])
            end
         end
      else
         if params[:invest_criminal_id]
            inv_criminal = InvestCriminal.find_by_id(params[:invest_criminal_id]) or raise_not_found "Criminal ID #{params[:invest_criminal_id]}"
            return_path = url_for(inv_criminal)
            owner = inv_criminal.owned_by
            id = inv_criminal.investigation_id
            crime_id = inv_criminal.invest_crime_id
            criminal_id = inv_criminal.id
            priv = inv_criminal.private?
         elsif params[:invest_crime_id]
            inv_crime = InvestCrime.find_by_id(params[:invest_crime_id]) or raise_not_found "Crime ID #{params[:invest_crime_id]}"
            return_path = url_for(inv_crime)
            owner = inv_crime.owned_by
            id = inv_crime.investigation_id
            crime_id = inv_crime.id
            criminal_id = nil
            priv = inv_crime.private?
         elsif params[:investigation_id]
            inv = Investigation.find_by_id(params[:investigation_id]) or raise_not_found "Investigation ID #{params[:investigation_id]}"
            return_path = url_for(inv)
            owner = inv.owned_by
            id = inv.id
            crime_id = nil
            criminal_id = nil
            priv = inv.private?
         else
            return_path = warrants_path
            owner = nil
            id = nil
            crime_id = nil
            criminal_id = nil
            priv = false
         end
         @warrant = Warrant.new(
            :created_by => session[:user],
            :updated_by => session[:user],
            :owned_by => owner,
            :investigation_id => id,
            :invest_crime_id => crime_id,
            :invest_criminal_id => criminal_id,
            :private => priv,
            :received_date => Date.today,
            :issued_date => Date.today
         ) or raise_db(:new, "Warrant")
         @warrant.name = ""
         if params[:person_id]
            @warrant.person_id = params[:person_id]
            unless @warrant.person.nil?
               return_path = @warrant.person
               @warrant.firstname = @warrant.person.firstname
               if @warrant.person.middlename.blank?
                  @warrant.middle1, @warrant.middle2, @warrant.middle3 = []
               else
                  @warrant.middle1, @warrant.middle2, @warrant.middle3 = @warrant.person.middlename.split
               end
               @warrant.lastname = @warrant.person.lastname
               @warrant.suffix = @warrant.person.suffix
               @warrant.street = @warrant.person.physical_line1
               @warrant.city = @warrant.person.phys_city
               @warrant.state_id = @warrant.person.phys_state_id
               @warrant.zip = @warrant.person.phys_zip
               @warrant.dob = @warrant.person.date_of_birth
               @warrant.ssn = @warrant.person.ssn
               @warrant.oln = @warrant.person.oln
               @warrant.oln_state_id = @warrant.person.oln_state_id
               @warrant.name = @warrant.person.full_name
               @warrant.sex = @warrant.person.sex
               @warrant.race_id = @warrant.person.race_id
            end
         end
         @page_title = "New Warrant"
         unless request.post?
            @warrant.charges.build(:created_by => session[:user], :updated_by => session[:user])
         end
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @warrant.new_record?
               redirect_to return_path
            else
               redirect_to @warrant
            end
            return
         end
         params[:warrant][:existing_charge_attributes] ||= {}
         @warrant.attributes = params[:warrant]
         if @warrant.save
            redirect_to @warrant
            if params[:id]
               user_action("Warrants","Edited Warrant ID: #{@warrant.id}.")
            else
               user_action("Warrants", "Added Warrant ID: #{@warrant.id}.")
            end
         end
      end
   end

   # Quick Add Warrant - From arrest
   def add
      require_permission Perm[:update_arrest]
      require_method :put
      @warrant = Warrant.new(
         :created_by => session[:user],
         :updated_by => session[:user],
         :received_date => Date.today,
         :issued_date => Date.today
      ) or raise_db(:new, "Warrant")
      unless params[:commit] == "Cancel"
         params[:warrant][:existing_charge_attributes] ||= {}
         @warrant.attributes = params[:warrant]
         if @warrant.save
            user_action("Warrants", "Added Warrant ID: #{@warrant.id}.")
         end
      end
      render :update do |page|
         page.hide 'add-form'
         page.show 'list'
         page.call 'location.reload'
      end
   end

   # Destroys the specified warrant. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_warrant]
      require_method :delete
      params[:id] or raise_missing "Warrant ID"
      @warrant = Warrant.find_by_id(params[:id]) or raise_not_found "Warrant ID #{params[:id]}"
      if !@warrant.investigation.nil?
         return_path = @warrant.investigation
      elsif !@warrant.crime.nil?
         return_path = @warrant.crime
      elsif !@warrant.criminal.nil?
         return_path = @warrant.criminal
      elsif !@warrant.person.nil?
         return_path = @warrant.person
      else
         return_path = warrants_path
      end
      @warrant.destroy or raise_db(:destroy, "Warrant ID #{params[:id]}")
      user_action("Warrants","Destroyed Warrant ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to return_path
   end
   
   # This prints a warrant list (PDF) using the same query parameters that
   # are in effect for the warrant list view.  If no query parameters are
   # saved in the session, the full booking list will be printed.
   def list_form
      require_permission Perm[:view_warrant]
      if session[:show_all_warrants].nil?
         session[:show_all_warrants] = false
      end
      @query = session[:warrant_filter] || HashWithIndifferentAccess.new
      options = HashWithIndifferentAccess.new
      options[:include] = :person
      options[:order] = 'lastname, firstname, middle1, middle2, middle3'
      # this is for privacy checks
      options[:conditions] = "(warrants.private != 1 OR (warrants.private = 1 AND ( warrants.owned_by = '#{session[:user]}' OR (warrants.owned_by IS NULL AND warrants.created_by = '#{session[:user]}'))))"
      # ----
      unless @query.nil? || @query.empty?
         if session[:show_all_warrants] == false
            @query.delete("disposition")
            @query.delete("disposition_from")
            @query.delete("disposition_to")
         end
         if cond_string = Warrant.condition_string(@query)
            options[:conditions] << " AND #{cond_string}"
         end
      end
      if session[:show_all_warrants] == false
         options[:conditions] << " AND dispo_date IS NULL"
      end
      @warrants = Warrant.find(:all, options.to_options)
      if @warrants.empty?
         flash[:warning] = "No Warrants selected for report!"
         redirect_to warrants_path and return
      end
      @page_title = "Warrant List"
      prawnto :prawn => {
         :page_size => 'LEGAL',
         :page_layout => :landscape,
         :left_margin => 20,
         :right_margin => 20,
         :top_margin => 10,
         :bottom_margin => 10 },
         :filename => "Warrant-List.pdf"
      render :template => 'warrants/list_form.pdf.prawn', :layout => false
      user_action("Warrants","Printed Warrant List.")
   end
   
   # called from the warrant_row helper, this finds all warrant records matching a
   # supplied warrant_no and displays them in a popup window. Clicking on a warrant
   # record in the new window will (via javascript) insert the selected id
   # and other information into appropriate fields on the original form. Clicking
   # "Not on File" button will allow adding a warrant via "add" action then
   # refreshes warrant lookup window.
   def lookup_list
      require_permission Perm[:update_arrest]
      @warrants = paged_array(Warrant.find_all_by_warrant_no(params[:warrant_no], :conditions => 'private != 1'))
      @person = Person.find_by_id(params[:person_id])
      @page_title = "Warrants On File with Number: '#{params[:warrant_no]}'"
      @warrant_id = params[:warrant_id]
      @warrant_status = params[:warrant_status]
      @warrant_charges = params[:warrant_charges]
      if @person.middlename.blank?
         middle1, middle2, middle3 = []
      else
         middle1, middle2, middle3 = @person.middlename.split
      end
      @warrant = Warrant.new(
         :person_id => @person.id,
         :name => @person.full_name,
         :firstname => @person.firstname,
         :lastname => @person.lastname,
         :middle1 => middle1,
         :middle2 => middle2,
         :middle3 => middle3,
         :suffix => @person.suffix,
         :special_type => 0,
         :warrant_no => params[:warrant_no],
         :received_date => Date.today,
         :created_by => session[:user],
         :updated_by => session[:user]
      )
      @warrant.charges.build(:created_by => 1, :updated_by => 1)
      render :layout => 'popup'
   end
   
   # accepts a person  id from the edit warrant form and validates a person,
   # updating all person identification fields from the person record.
   def update_person
      if params[:c] && has_permission(Perm[:view_person])
         if person = Person.find_by_id(params[:c])
            render :update do |page|
               page['warrant_name'].value = person.full_name
               page['warrant_ssn'].value = person.ssn
               page['warrant_dob'].value = format_date(person.date_of_birth)
               page['warrant_oln'].value = person.oln
               page['warrant_oln_state_id'].value = person.oln_state_id
               page['warrant_street'].value = person.physical_line1
               page['warrant_city'].value = person.phys_city
               page['warrant_state_id'].value = person.phys_state_id
               page['warrant_zip'].value = person.phys_zip
               page['warrant_name'].select()
            end
         else
            render :update do |page|
               page['warrant_full_name'].value = "Invalid Person ID"
               page['warrant_person_id'].select()
            end
         end
      else
         render :text => ""
      end
   end
   
   # Review latest warrants
   def review
      require_permission Perm[:view_warrant]
      @page_title = "Warrants Review"
      @warrants = review_paged('warrant', :include => :person)
      @links = [["View Next Page", url_for(:action => 'review')]]
      @links.push(["Skip All",url_for(:action => 'skip_all')])
   end

   def skip_all
      require_permission Perm[:view_warrant]
      review_skip_all('warrant')
      flash[:notice] = "All warrant reviews skipped!"
      redirect_to root_path
      return
   end
      
   protected
   
   # highlights the "Warrants" menu item for all actions in this controller
   def set_tab_name
      @current_tab = "Warrants"
   end
end
