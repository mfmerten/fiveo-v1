# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Fax cover sheets are customizable to a certain extent, but all use the
# same basic format and the agency letterhead.
class FaxCoversController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Fax cover Listing
   def index
      @help_page = ["FAX Covers","index"]
      @query = nil
      @fax_covers = paged('fax_cover','name')
      @links = [["New", edit_fax_cover_path]]
   end

   # Show Fax Cover
   def show
      @help_page = ["FAX Covers", "show"]
      params[:id] or raise_missing "Cover ID"
      @fax_cover = FaxCover.find_by_id(params[:id]) or raise_not_found "FAX Cover ID #{params[:id]}"
      @links = [
         ["New", edit_fax_cover_path],
         ["Edit", edit_fax_cover_path(:id => @fax_cover.id)],
         ["Delete", @fax_cover, 'delete', true]]
      @links.push(["PDF", url_for(:action => 'fax_cover', :fax_cover_id => @fax_cover.id)])
      @page_links = [[@fax_cover.creator,'creator'],[@fax_cover.updater,'updater']]
   end

   # Add or Edit FAX Cover
   def edit
      @help_page = ["FAX Covers","edit"]
      if params[:id]
         @fax_cover = FaxCover.find_by_id(params[:id]) or raise_not_found "FAX Cover ID #{params[:id]}"
         @page_title = "Edit Fax Cover ID: #{@fax_cover.id}"
         @fax_cover.updated_by = session[:user]
      else
         @fax_cover = FaxCover.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "FAX Cover")
         @page_title = "New Fax Cover"
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @fax_cover.new_record?
               redirect_to fax_covers_path
            else
               redirect_to @fax_cover
            end
            return
         end
         @fax_cover.attributes = params[:fax_cover]
         if @fax_cover.save
            redirect_to @fax_cover
            if params[:id]
               user_action("FAX Covers","Edited Fax Cover ID: #{@fax_cover.id}.")
            else
               user_action("FAX Covers", "Added Fax Cover ID: #{@fax_cover.id}.")
            end
         end
      end
   end

   # Destroys the specified fax_cover. Only responds to DELETE methods.
   def destroy
      require_method :delete
      params[:id] or raise_missing "FAX Cover ID"
      @fax_cover = FaxCover.find_by_id(params[:id]) or raise_not_found "FAX Cover ID #{params[:id]}"
      enforce_author(@fax_cover)
      @fax_cover.destroy or raise_db(:destroy, "FAX Cover ID #{params[:id]}")
      user_action("FAX Covers","Destroyed Fax Cover ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to fax_covers_path
   end
   
   # prints the selected FAX cover as a PDF document.
   def fax_cover
      params[:fax_cover_id] or raise_missing "FAX Cover ID"
      @fax_cover = FaxCover.find_by_id(params[:fax_cover_id]) or raise_not_found "FAX Cover ID #{params[:fax_cover_id]}"
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 10,
         :bottom_margin => 10,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "FAX_Cover-#{@fax_cover.id}.pdf"
      render :template => 'fax_covers/fax_cover.pdf.prawn', :layout => false
      user_action("FAX Covers","Printed fax cover ID: #{@fax_cover.id}.")
   end

   protected

   # Ensures that the "FAX Covers" menu item is highlighted for all
   # actions in this controller
   def set_tab_name
      @current_tab = "FAX Covers"
   end
end
