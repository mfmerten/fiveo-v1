# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Absents records detail when inmates are temporarily absent from the detention
# center (always with an officer escort). There should be no more than one
# active absent record per booking, and the active absent record affects
# the reported location of the prisoner on booking records.
class AbsentsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Absent Index
   def index
      require_permission Perm[:view_booking]
      @help_page = ["Absents","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:absent_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:absent_filter] = nil
            redirect_to absents_path and return
         end
      else
         @query = session[:absent_filter] || HashWithIndifferentAccess.new
      end
      unless @absents = paged("absent", :order => "absents.id DESC", :include => [:prisoners, :facility, :leave_type])
         @query = HashWithIndifferentAccess.new
         session[:absent_filter] = nil
         redirect_to absents_path and return
      end
      if has_permission Perm[:view_booking]
         @links = [['New', edit_absent_path]]
      end
   end

   # Show Absent
   def show
      require_permission Perm[:view_booking]
      @help_page = ["Absents","show"]
      params[:id] or raise_missing "Absent ID"
      @absent = Absent.find_by_id(params[:id]) or raise_not_found "Absent ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_booking]
         @links.push(['New', edit_absent_path])
         @links.push(['Copy', copy_absent_path(:id => @absent.id), {:method => :post}])
         @links.push(['Edit', edit_absent_path(:id => @absent.id)])
      end
      if has_permission Perm[:destroy_booking]
         @links.push(['Delete', @absent, {:method => :delete, :confirm => "Are you sure you want to destroy this Absent?"}])
      end
      if has_permission Perm[:update_booking]
         unless @absent.leave_date?
            @links.push(['Leave', absent_leave_path(:id => @absent.id), {:method => :post}])
         end
         if @absent.leave_date? && !@absent.return_date?
            @links.push(['Return', absent_return_path(:id => @absent.id), {:method => :post}])
         end
      end
      @page_links = [[@absent.escort_officer_id,'officer'],[@absent.creator,'creator'],[@absent.updater,'updater']]
   end

   # Add or Edit Absent
   def edit
      require_permission Perm[:view_booking]
      @help_page = ["Absents","edit"]
      if params[:id]
         @absent = Absent.find_by_id(params[:id]) or raise_not_found "Absent ID #{params[:id]}"
         @page_title = "Edit Absent ID: #{@absent.id}"
         @absent.updated_by = session[:user]
         if @absent.prisoners.empty?
            @absent.prisoners.build
         end
      else
         @absent = Absent.new(:created_by => session[:user],:updated_by => session[:user]) or raise_db(:new, "Absent")
         @page_title = "New Absent"
         if !current_user.contact.nil?
            @absent.escort_officer_id = current_user.contact.id
         else
            @absent.escort_officer = session[:user_name]
            @absent.escort_officer_unit = session[:user_unit]
            @absent.escort_officer_badge = session[:user_badge]
         end
         unless request.post?
            @absent.prisoners.build
         end
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @absent.new_record?
               redirect_to absents_path and return
            else
               redirect_to @absent and return
            end
         end
         params[:absent][:assigned_prisoners] ||= {}
         @absent.attributes = params[:absent]
         if @absent.save
            redirect_to @absent
            if params[:id]
               user_action("Absents","Edited Absent ID: #{@absent.id}.")
            else
               user_action("Absents", "Added Absent ID: #{@absent.id}.")
            end
         end
      end
   end
   
   # This is called to make a copy of an absent as a new one - saves
   # time when taking out the same work crew over and over.
   def copy
      require_permission Perm[:update_booking]
      require_method :post
      params[:id] or raise_missing "Absent ID"
      old = Absent.find_by_id(params[:id]) or raise_not_found "Absent ID #{params[:id]}"
      absent = Absent.new(:created_by => session[:user], :updated_by => session[:user], :leave_type_id => old.leave_type_id, :facility_id => old.facility_id) or raise_db(:new, "Absent")
      if !current_user.contact.nil?
         absent.escort_officer_id = current_user.contact.id
      else
         absent.escort_officer = session[:user_name]
         absent.escort_officer_unit = session[:user_unit]
         absent.escort_officer_badge = session[:user_badge]
      end
      absent.save(false)
      absent.prisoners << old.prisoners
      redirect_to edit_absent_path(:id => absent.id) and return
   end
   
   # Typically called from a button on the show view, this updates an existing
   # absent record showing that the inmate(s) have left the facility.
   def leave
      require_permission Perm[:update_booking]
      require_method :post
      params[:id] or raise_missing "Absent ID"
      @absent = Absent.find_by_id(params[:id]) or raise_not_found "Absent ID #{params[:id]}"
      @absent.leave_date, @absent.leave_time = get_date_and_time
      @absent.updated_by = session[:user]
      @absent.save(false)
      redirect_to @absent
      user_action("Absents", "Updated Absent ID #{@absent.id} as Left.")
   end
   
   # Typically called from a button on the show view, this updates an existing
   # absent record showing that the inmates have returned.
   def come_back
      require_permission Perm[:update_booking]
      require_method :post
      params[:id] or raise_missing "Absent ID"
      @absent = Absent.find_by_id(params[:id]) or raise_not_found "Absent ID #{params[:id]}"
      @absent.return_date, @absent.return_time = get_date_and_time
      @absent.updated_by = session[:user]
      @absent.save(false)
      redirect_to @absent
      user_action("Absents", "Updated Absent ID #{@absent.id} as Returned.")
   end

   # Destroys the specified absent record. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_booking]
      require_method :delete
      params[:id] or raise_missing "Absent ID"
      @absent = Absent.find_by_id(params[:id]) or raise_not_found "Absent ID #{params[:id]}"
      @absent.destroy or raise_db(:destroy, "Absent ID #{params[:id]}")
      flash[:notice] = "Record Deleted!"
      user_action("Absent", "Destroyed Absent ID #{params[:id]}!")
      redirect_to absents_path
   end

   protected
   
   # ensures that all actions will have the "Absents" menu item properly
   # highlighted as current.
   def set_tab_name
      @current_tab = "Absents"
   end
end
