# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Commissary Controller
# This controller provides access to commissary transactions, reports and
# sales items.
class CommissaryController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # The main commissary page. Displays unposted transactions and gives
   # access to reporting, posting and sales items.
   def index
      require_permission Perm[:view_commissary]
      @help_page = ["Commissary","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:commissary_item_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:commissary_item_filter] = nil
            redirect_to commissaries_path and return
         end
      else
         @query = session[:commissary_item_filter] || HashWithIndifferentAccess.new
      end
      unless @commissary_items = paged('commissary_item', :order => 'description', :include => :facilities)
         @query = HashWithIndifferentAccess.new
         session[:commissary_item_filter] = nil
         redirect_to commissaries_path and return
      end
      @commissaries = Commissary.all_unposted
      if has_permission Perm[:update_commissary]
         @links = [["New", edit_commissary_item_path]]
      end
   end
   
   # This generates a PDF fund balance report listing all people that have
   # a non-zero balance.
   def fund_balance
      require_permission Perm[:view_commissary]
      @facility = params[:facility_id]
      unless @facility.blank?
         o = Option.find_by_id(@facility) or raise_not_found "Facility ID #{params[:facility_id]}"
      end
      @facility = nil if @facility.blank?
      @trans = Commissary.find(:all, :select => 'person_id, sum(deposit - withdraw) as balance', :group => 'person_id', :having => 'balance != 0')
      if o.nil?
         prawnto :prawn => {
            :page_size => 'LETTER',
            :top_margin => 10,
            :bottom_margin => 10,
            :left_margin => 20,
            :right_margin => 20},
            :filename => "Commissary_Total.pdf"
         user_action("Commissary", "Printed Commissary Fund Balance Report.")
      else
         prawnto :prawn => {
            :page_size => 'LETTER',
            :top_margin => 10,
            :bottom_margin => 10,
            :left_margin => 20,
            :right_margin => 20},
            :filename => "Commissary_Total-#{o.short_name}.pdf"
         user_action("Commissary", "Printed #{o.short_name} Commissary Fund Balance Report.")
      end
      render :template => 'commissary/fund_balance.pdf.prawn', :layout => false
   end
   
   # this generates an unposted transaction report
   def posting_report
      require_permission Perm[:view_commissary]
      params[:status] or raise_missing "Status"
      @status = params[:status]
      valid_status = ['post','reprint','preview']
      valid_status.include?(@status) or raise_invalid "Status '#{params[:status]}'", valid_status
      if @status == 'post'
         require_permission Perm[:update_commissary]
         require_method :post
      end
      @facility = params[:facility_id]
      @facility = nil if @facility.blank?
      unless @facility.nil?
         o = Option.find_by_id(@facility) or raise_not_found "Facility ID '#{params[:facility_id]}'"
      end
      if @status == 'reprint'
         params[:post_datetime] or raise_missing "Posting Date"
         report_datetime = valid_datetime(params[:post_datetime]) or raise_invalid "Posting Date", "'#{params[:post_datetime]}' Is Not A Valid Date"
         @deposits = Commissary.deposit_posted(@facility, report_datetime)
         @withdrawals = Commissary.withdraw_posted(@facility, report_datetime)
         @report_time = report_datetime
      else
         @deposits = Commissary.deposit_unposted(@facility)
         @withdrawals = Commissary.withdraw_unposted(@facility)
         @report_time = DateTime.now
      end
      if @deposits.empty? && @withdrawals.empty?
         flash[:warning] = 'Nothing to report!'
         redirect_to commissaries_path and return
      end
      if @status == 'post' || @status == 'reprint'
         if @facility.nil?
            prawnto :prawn => {
               :page_size => 'LETTER',
               :top_margin => 10,
               :bottom_margin => 10,
               :left_margin => 20,
               :right_margin => 20},
               :filename => "Unposted_Transactions-#{format_date(@report_time, :timestamp => true)}.pdf"
            user_action("Commissary", "Printed Unposted Transactions Report (#{@status == 'post' ? 'Posted Transactions' : 'Reprint for ' + format_date(@report_time)}).")
         else
            prawnto :prawn => {
               :page_size => 'LETTER',
               :top_margin => 10,
               :bottom_margin => 10,
               :left_margin => 20,
               :right_margin => 20},
               :filename => "Unposted_Transactions-#{o.short_name}-#{format_date(@report_time, :timestamp => true)}.pdf"
            user_action("Commissary", "Printed #{o.short_name} Unposted Transactions Report (#{@status == 'post' ? 'Posted Transactions' : 'Reprint for ' + format_date(@report_time)}).")
         end
      else
         if @facility.nil?
            prawnto :prawn => {
               :page_size => 'LETTER',
               :top_margin => 10,
               :bottom_margin => 10,
               :left_margin => 20,
               :right_margin => 20},
               :filename => "Unposted_Transactions_Preview-#{format_date(@report_time, :timestamp => true)}.pdf"
            user_action("Commissary", "Printed Unposted Transactions Preview Report.")
         else
            prawnto :prawn => {
               :page_size => 'LETTER',
               :top_margin => 10,
               :bottom_margin => 10,
               :left_margin => 20,
               :right_margin => 20},
               :filename => "Unposted_Transactions_Preview-#{o.short_name}-#{format_date(@report_time, :timestamp => true)}.pdf"
            user_action("Commissary", "Printed #{o.short_name} Unposted Transactions Preview Report.")
         end
      end
      render :template => 'commissary/posting_report.pdf.prawn', :layout => false
   end
   
   # Show Commissary Item
   def show
      require_permission Perm[:view_commissary]
      params[:id] or raise_missing "Commissary Item ID"
      @commissary_item = CommissaryItem.find_by_id(params[:id]) or raise_not_found "Commissary Item ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_commissary]
         @links.push(["New", edit_commissary_item_path])
         @links.push(["Edit", edit_commissary_item_path(:id => @commissary_item.id)])
      end
      if has_permission Perm[:destroy_commissary]
         @links.push(["Delete", @commissary_item, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Commissary Item?'}])
      end
   end
   
   # Add or Edit Commissary Item
   def edit
      require_permission Perm[:update_commissary]
      @help_page = ["Commissary","edit"]
      if params[:id]
         @commissary_item = CommissaryItem.find_by_id(params[:id]) or raise_not_found "Commissary Item ID #{params[:id]}"
         @commissary_item.updated_by = session[:user]
         @page_title = "Edit Commissary Item ID: #{@commissary_item.id}"
         if @commissary_item.facilities.empty?
            @commissary_item.facilities.build
         end
      else
         @commissary_item = CommissaryItem.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Commissary Item")
         @page_title = "New Commissary Item"
         @commissary_item.facilities.build
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @commissary_item.new_record?
               redirect_to commissaries_path
            else
               redirect_to @commissary_item
            end
            return
         end
         params[:commissary_item][:assigned_facilities] ||= {}
         @commissary_item.attributes = params[:commissary_item]
         if @commissary_item.save
            redirect_to(@commissary_item)
            if params[:id]
               user_action("Commissary","Edited Commissary Item ID: #{@commissary_item.id}.")
            else
               user_action("Commissary", "Added Commissary Item ID: #{@commissary_item.id}.")
            end
         end
      end
   end
   
   # Destroys the specified commissary item. Only responds to DELETE methods.
   # Note: this is one of the few destroy actions allowed to users witout Destroy permissions
   def destroy
      require_permission Perm[:update_commissary]
      require_method :delete
      params[:id] or raise_missing "Commissary Item ID"
      @commissary_item = CommissaryItem.find_by_id(params[:id]) or raise_not_found "Commissary Item ID #{params[:id]}"
      @commissary_item.destroy or raise_db(:destroy, "Commissary Item ID #{params[:id]}")
      user_action("Commissary","Destroyed Commissary Item ID: #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to commissaries_path
   end
   
   # This generates the PDF sales sheets for the facility specified
   def sales_sheet
      require_permission Perm[:view_commissary]
      if !params[:person_id].blank?
         cperson = Person.lookup(params[:person_id]) or raise_not_found "Person ID #{params[:person_id]}"
         @facility = cperson.commissary_facility
         if @facility.nil? && !cperson.bookings.empty?
            @facility = cperson.bookings.last.facility
         end
      elsif !params[:facility_id].blank?
         @facility = Option.find_by_id(params[:facility_id]) or raise_not_found "Facility ID #{params[:facility_id]}"
         cperson = nil
      else
         raise_missing "Person ID or Facility ID"
      end
      if cperson.nil?
         unless @people = Person.find(:all, :include => :bookings, :order => 'people.lastname, people.firstname', :conditions => ['bookings.release_date is null and bookings.facility_id = ?',@facility.id])
            flash[:warning] = 'Nothing To Report'
            redirect_to commissaries_path and return
         end
      else
         @people = [cperson]
      end
      @commissary_items = CommissaryItem.find(:all, :order => 'description').reject{|c| !c.facilities.include?(@facility)}
      if @commissary_items.empty?
         flash[:warning] = 'No Commissary Items found for Facility!'   
         redirect_to commissaries_path and return
      end
      if cperson.nil?
         prawnto :prawn => {
            :page_size => 'LEGAL',
            :top_margin => 10,
            :bottom_margin => 10,
            :left_margin => 20,
            :right_margin => 20},
            :filename => "Commissary_Sheets-#{@facility.short_name}.pdf"
         user_action("Commissary", "Printed #{@facility.short_name} Sales Sheets.")
      else
         prawnto :prawn => {
            :page_size => 'LEGAL',
            :top_margin => 10,
            :bottom_margin => 10,
            :left_margin => 20,
            :right_margin => 20},
            :filename => "Commissary_Sheet-#{cperson.full_name}.pdf"
         user_action("Commissary", "Printed #{cperson.full_name} Sales Sheet.")
      end
      render :template => 'commissary/sales_sheet.pdf.prawn', :layout => false
   end
   
   # show commissary transaction
   def show_commissary
      require_permission Perm[:view_commissary]
      @help_page = ["Commissary","show_commissary"]
      params[:id] or raise_missing "Commissary Transaction ID"
      @commissary = Commissary.find_by_id(params[:id]) or raise_not_found "Commissary Transaction ID #{params[:id]}"
      @page_title = "Commissary Transaction ID #{@commissary.id}"
      @links = []
      if has_permission Perm[:update_commissary]
         @links.push(["New", edit_commissary_path(:person_id => @commissary.person_id)])
         unless @commissary.report_datetime?
            @links.push(["Edit", edit_commissary_path(:id => @commissary.id)])
            @links.push(["Delete", @commissary, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Commissary Transaction?'}])
         end
      end
      @page_links = [@commissary.person]
      if !@commissary.person.nil? && !@commissary.person.bookings.empty? && !@commissary.person.bookings.last.release_date?
         @page_links.push(@commissary.person.bookings.last)
      end
   end
   
   # Add new commissary transaction record.
   def commissary
      require_permission Perm[:update_commissary]
      @help_page = ["Commissary","commissary"]
      if params[:id]
         @commissary = Commissary.find_by_id(params[:id]) or raise_not_found "Commissary Transaction ID #{params[:id]}"
         !@commissary.report_datetime? or raise_not_allowed("Edit Posted Transactions")
         @person = @commissary.person
         @page_title = "Edit Commissary Transaction ID #{params[:id]}"
      else
         params[:person_id] or raise_missing "Person ID"
         @person = Person.lookup(params[:person_id]) or raise_not_found "Person ID #{params[:person_id]}"
         @commissary = Commissary.new(:created_by => session[:user], :updated_by => session[:user], :transaction_date => Date.today, :person_id => @person.id) or raise_db(:new, "Commissary Transaction")
         if !current_user.contact.nil?
            @commissary.officer_id = current_user.contact.id
         else
            @commissary.officer = session[:user_name]
            @commissary.officer_unit = session[:user_unit]
            @commissary.officer_badge = session[:user_badge]
         end
         @page_title = "New Commissary Transaction"
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @commissary.id.nil?
               redirect_to commissaries_path and return
            else
               redirect_to @commissary and return
            end
         end
         @commissary.attributes = params[:commissary]
         if @commissary.save
            redirect_to @commissary
            if params[:id]
               user_action("Commissary", "Edited Commissary Transaction (ID: #{params[:id]}) for Person: #{show_person @person}.")
            else
               user_action("Commissary", "Added Commissary Transaction (ID: #{@commissary.id}) for Person: #{show_person @person}.")
            end
         end
      end
   end
   
   # Destroys the specified commissary transaction
   def destroy_commissary
      require_permission Perm[:update_commissary]
      require_method :delete
      params[:id] or raise_missing "Commissary Transaction ID"
      @commissary = Commissary.find_by_id(params[:id]) or raise_not_found "Commissary Transaction ID #{params[:id]}"
      !@commissary.report_datetime? or raise_not_allowed("Destroy Posted Transactions")
      @commissary.destroy or raise_db(:destroy, "Commissary Transaction ID #{params[:id]}")
      user_action("Commissary","Destroyed Commissary Transaction ID: #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to commissaries_path
   end
   
   # prints the detailed or summary transaction report
   def transaction_report
      require_permission Perm[:view_commissary]
      params[:status] or raise_missing "Report Type"
      @status = params[:status]
      ['summary','detailed'].include?(@status) or raise_invalid "Report Type", ['summary','detailed']
      params[:start_date] or raise_missing "Start Date"
      params[:end_date] or raise_missing "End Date"
      @start_date = valid_date(params[:start_date]) or raise_invalid "Start Date","'#{params[:start_date]}' is not a valid date."
      @stop_date = valid_date(params[:end_date]) or raise_invalid "End Date","'#{params[:end_date]}' is not a valid date."
      if @start_date > @stop_date
         # wrong order, swap
         @start_date, @stop_date = @stop_date, @start_date
      end
      if params[:person_id] && !params[:person_id].blank?
         @person = Person.find_by_id(params[:person_id]) or raise_not_found "Person ID #{params[:person_id]}"
         @people = [@person]
         @facility = nil
      else
         @person = nil
         @facility = Option.find_by_id(params[:facility_id])
         if @facility.nil?
            @people = Commissary.all_people(@start_date,@stop_date)
         else
            ot = OptionType.find_by_name("Facility") or raise_not_found "Option Type 'Facility'"
            @facility.option_type_id == ot.id or raise_invalid "Facility ID #{params[:facility_id]} is not a Facility! It is a #{ot.name}"
            @people = Commissary.all_people(@start_date,@stop_date,@facility.id)
         end
      end
      comm = []
      @fund_begin_balance = BigDecimal.new('0.0',2)
      @commissaries = @people.collect{|p| p.commissary_transactions(@start_date, @stop_date)}.flatten.compact.sort{|a,b| a.id <=> b.id}
      @page_title = "#{@start_date.to_s(:us)} -- #{@stop_date.to_s(:us)}"
      if @status == 'summary'
         prawnto :prawn => {
            :page_size => 'LETTER',
            :top_margin => 10,
            :bottom_margin => 10,
            :left_margin => 20,
            :right_margin => 20},
            :filename => "Commissary_Transactions_Summary.pdf"
         render :template => 'commissary/summary_balance.pdf.prawn', :layout => false
         user_action("Commissary", "Printed Summary Commissary Report")
      else
         prawnto :prawn => {
            :page_size => 'LETTER',
            :top_margin => 10,
            :bottom_margin => 10,
            :left_margin => 20,
            :right_margin => 20},
            :filename => "Commissary_Transactions_Detail.pdf"
         render :template => 'commissary/detailed_balance.pdf.prawn', :layout => false
         user_action("Commissary", "Printed Detailed Commissary Report")
      end
   end

   private
   
   # ensures that the Commissary menu item is highlighted for all actions
   # in this controller
   def set_tab_name
      @current_tab = 'Commissary'
   end
end