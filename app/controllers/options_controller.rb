# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Options are items used to build select controls. They are grouped by
# option type (see OptionTypesController).
class OptionsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Option Listing
   def index
      require_permission Perm[:view_option]
      @help_page = ["Options","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:option_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:option_filter] = nil
            redirect_to options_path and return
         end
      else
         @query = session[:option_filter] || HashWithIndifferentAccess.new
      end
      unless @options = paged('option',:order => 'option_types.name, long_name', :include => :option_type)
         @query = HashWithIndifferentAccess.new
         session[:option_filter] = nil
         redirect_to options_path and return
      end
      if has_permission Perm[:update_option]
         @links = [['New',edit_option_path]]
      end
   end

   # Show Option
   def show
      require_permission Perm[:view_option]
      @help_page = ["Options","show"]
      params[:id] or raise_missing "Option ID"
      @option = Option.find_by_id(params[:id]) or raise_not_found "Option ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_option]
         @links.push([(@option.disabled ? "Enable" : "Disable"), toggle_option_path(:id => @option.id), {:method => 'post'}])
         @links.push(['New', edit_option_path])
      end
      unless @option.permanent?
         if has_permission Perm[:edit_option]
            @links.push(['Edit', edit_option_path(:id => @option.id)])
         end
         if has_permission Perm[:destroy_option]
            @links.push(['Delete', @option, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Option?'}])
         end
      end
      @page_links = [[@option.creator,'creator'],[@option.updater,'updater']]
   end

   # Add or Edit Option
   def edit
      @help_page = ["Options","edit"]
      if params[:id]
         if request.post?
            require_permission Perm[:update_option], Perm[:edit_option]
         else
            require_permission Perm[:edit_option]
         end
         @option = Option.find_by_id(params[:id]) or raise_not_found "Option ID #{params[:id]}"
         @option.permanent? and raise_not_allowed "Edit Permanent Options"
         @page_title = "Edit Option ID: #{@option.id}"
         @option.updated_by = session[:user]
      else
         require_permission Perm[:update_option]
         @option = Option.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Option")
         @page_title = "New Option"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @option.new_record?
               redirect_to options_path
            else
               redirect_to @option
            end
            return
         end
         @option.attributes = params[:option]
         if @option.save
            redirect_to @option
            if params[:id]
               user_action("Option","Edited Option ID: #{@option.id} (#{@option.long_name}).")
            else
               user_action("Option", "Added Option ID: #{@option.id} (#{@option.long_name}).")
            end
         end
      end
   end

   # Destroys the specified option. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_option]
      require_method :delete
      params[:id] or raise_missing "Option ID"
      @option = Option.find_by_id(params[:id]) or raise_not_found "Option ID #{params[:id]}"
      @option.permanent? and raise_not_allowed "Destroy Permanent Options"
      @option.in_use? and raise_not_allowed "Destroy Options In Use"
      @option.destroy or raise_db(:destroy, "Option ID #{params[:id]}")
      user_action("Option", "Destroyed Option ID: #{params[:id]}")
      flash[:notice] = "Record Deleted!"
      redirect_to options_path
   end

   # this is called to toggle the :disabled attribute on an option. (Disabled
   # options don't show up in select controls on forms and cannot be selected
   # for new records but are still available for reference on historical 
   # records.).  Note that records marked as 'permanent' cannot be disabled
   # using this method.
   def toggle
      require_permission Perm[:update_option]
      require_method :post
      params[:id] or raise_missing "Option ID"
      @option = Option.find_by_id(params[:id]) or raise_not_found "Option ID #{params[:id]}"
      @option.toggle!(:disabled)
      if @option.disabled?
         flash[:notice] = "Option Disabled!"
         user_action("Option", "Disabled Option ID: #{@option.id} (#{@option.long_name}).")
      else
         flash[:notice] = "Option Enabled!"
         user_action("Option", "Enabled Option ID: #{@option.id} (#{@option.long_name}).")
      end
      redirect_to @option
   end

   protected

   # ensures that the "Options" menu item is highlighted for all actions in
   # this controller.
   def set_tab_name
      @current_tab = "Options"
   end
end
