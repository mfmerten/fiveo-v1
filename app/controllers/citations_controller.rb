# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Misdemeanor Citations
class CitationsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   
   # Citation Listing
   def index
      require_permission Perm[:view_citation]
      @help_page = ["Citations","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:citation_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:citation_filter] = nil
            redirect_to citations_path and return
         end
      else
         @query = session[:citation_filter] || HashWithIndifferentAccess.new
      end
      unless @citations = paged('citation', :order => 'citation_date DESC', :include => [:person, :charges])
         @query = HashWithIndifferentAccess.new
         session[:citation_filter] = nil
         redirect_to citations_path and return
      end
      if has_permission Perm[:update_citation]
         @links = [['New', edit_citation_path]]
      end
   end

   # Show Citation
   def show
      require_permission Perm[:view_citation]
      @help_page = ["Citations","show"]
      params[:id] or raise_missing "Citation ID"
      @citation = Citation.find_by_id(params[:id]) or raise_not_found "Citation ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_citation]
         @links.push(['New', edit_citation_path])
         @links.push(['Edit', edit_citation_path(:id => @citation.id)])
      end
      if has_permission Perm[:destroy_citation]
         @links.push(['Delete', @citation, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Citation?'}])
      end
      @page_links = [@citation.person,[@citation.officer_id,'Officer'],[@citation.creator,'creator'],[@citation.updater,'updater']]
   end

   # Add or Edit Citation
   def edit
      require_permission Perm[:update_citation]
      @help_page = ["Citations","edit"]
      if params[:id]
         @citation = Citation.find_by_id(params[:id]) or raise_not_found "Citation ID #{params[:id]}"
         @page_title = "Edit Citation ID: " + @citation.id.to_s
         @citation.updated_by = session[:user]
         if @citation.charges.empty?
            unless request.post?
               @citation.charges.build(:created_by => session[:user], :updated_by => session[:user])
            end
         end
      else
         @citation = Citation.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Citation")
         @page_title = "New Citation"
         if params[:person_id]
            @citation.person_id = params[:person_id]
         end
         if params[:offense_id]
            @citation.offense_id = params[:offense_id]
            unless @citation.offense.nil?
               @citation.citation_date = @citation.offense.offense_date
               @citation.citation_time = @citation.offense.offense_time
               @citation.officer_id = @citation.offense.officer_id
               @citation.officer = @citation.offense.officer
               @citation.officer_unit = @citation.offense.officer_unit
               @citation.officer_badge = @citation.offense.officer_badge
            end
         end
         unless request.post?
            @citation.charges.build(:created_by => session[:user], :updated_by => session[:user])
         end
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @citation.new_record?
               redirect_to citations_path
            else
               redirect_to @citation
            end
            return
         end
         params[:citation][:existing_charge_attributes] ||= {}
         @citation.attributes = params[:citation]
         if @citation.save
            redirect_to @citation
            if params[:id]
               user_action("Citations","Edited Citation ID: #{@citation.id}.")
            else
               user_action("Citations", "Added Citation ID: #{@citation.id}.")
            end
         end
      end
   end
   
   # Destroys the specified citation record. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_citation]
      require_method :delete
      params[:id] or raise_missing "Citation ID"
      @citation = Citation.find_by_id(params[:id]) or raise_not_found "Citation ID #{params[:id]}"
      @citation.destroy or raise_db(:destroy, "Citation ID #{params[:id]}")
      user_action("Citations", "Destroyed Citation ID #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to citations_path
   end

   protected
   
   # ensures that all actions will have the "Citations" menu item properly
   # highlighted as current.
   def set_tab_name
      @current_tab = "Citations"
   end
end
