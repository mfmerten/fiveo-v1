# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Messages are the internal equivalent of e-mail between FiveO users. The
# messages system is implemented through the RESTfulEasyMessages plugin.
# This controller was modified somewhat from the original as installed by
# the plugin.
#
# Referenced Permissions:
# * None
class MessagesController < ApplicationController

   skip_before_filter :login_required
   before_filter :update_session_active

   include RestfulEasyMessagesControllerSystem

   # Restful_authentication Filter
   before_filter :rezm_login_required

   # GET /messages
   def index
      redirect_to inbox_user_messages_url
   end

   # GET /messages/1
   def show
      params[:id] or raise_missing "Message ID"
      @message = Message.find_by_id(params[:id]) or raise_not_found "Message ID #{params[:id]}"
      @tips = "To print this message, click the <b>Display</b> button to display the message without the page headers and menu bar, then click the <b>Print</b> button."
      @links = []
      unless (!@message.sender.nil? && @message.sender.disabled_text?) || @message.body =~ /DO NOT REPLY/
         # can't reply to disabled users or messages containing "DO NOT REPLY" text
         @links.push(['Reply', reply_user_message_path(current_user, @message)])
      end
      @links.push(['Delete', user_message_path(current_user, @message), {:method => 'delete', :confirm => 'Are you sure you want to destroy this message?'}])
      @links.push(['Display', print_message_path(:user_id => session[:user], :id => @message.id)])
      
      can_view(@message) or raise_not_allowed "View This Message"
      @message.mark_message_read(current_user)
      @page_links = [[@message.sender,'sender'],[@message.receiver,'receiver']]
   end

   # GET /messages/new
   def new
      @help_page = ['Messages','new']
      @current_tab = "New Message"
      @page_title = "New Message"
      @message = Message.new or raise_db(:new, "Message")
   end

   # POST /messages
   def create
      @current_tab = "New Message"
      @message = Message.new((params[:message] || {}).merge(:sender_id => session[:user])) or raise_db(:new, "Message")
      @page_title = "New Message"
      if params[:commit] == "Cancel"
         redirect_to inbox_user_messages_path
         return
      end
      if params[:commit] == "Preview"
         @preview = @message.body
         render :action => "new"
      else
         if @message.save
            flash[:notice] = 'Message was sent successfully.'
            user_action("Messages","Sent message to #{@message.receiver.name}.")
            redirect_to outbox_user_messages_path and return
         else
            if @message.errors.count == 0
               # not a validation problem, check if a bug report reply
               bugid = @message.subject.clone.sub(/^.*Bug#(\d+).*$/, '\1').to_i
               unless b = Bug.find_by_id(bugid)
                  flash[:warning] = "Cannot Process: Bug##{bugid} does not exist so you cannot reply to it."
                  redirect_to inbox_user_messages_path and return
               end
               txt = @message.body.clone.sub(/\r\n__________.*<p>__________/m, '')
               if txt.strip.blank?
                  flash[:warning] = "Cannot Process: You forgot to include your comment in the reply to Bug#{b.id}."
                  redirect_to inbox_user_messages_path and return
               end
               BugComment.create(
                  :bug_id => b.id,
                  :user_id => session[:user],
                  :comments => txt,
                  :created_by => session[:user],
                  :updated_by => session[:user]
               ) or raise_db(:create, "Bug Comment")
               redirect_to b and return
            else
               render :action => "new"
            end
         end
      end
   end

   # DELETE /messages/1
   def destroy
      require_method :delete
      params[:id] or raise_missing "Message ID"
      @message = Message.find_by_id(params[:id]) or raise_not_found "Message ID #{params[:id]}"
      can_view(@message) or raise_not_allowed "Destroy This Message"
      mark_message_for_destruction(@message)
      redirect_to current_mailbox
   end

   ### Non-CRUD Actions

   # GET /messages/inbox
   # GET /messages/inbox.xml
   # GET /messages/inbox.atom
   # Displays all new and read and undeleted messages in the User's inbox
   def inbox
      session[:mail_box] = "inbox"
      @messages = current_user.inbox_messages
      @page_title = "Inbox For: " + session[:user_name]
      @current_tab = "Inbox"
      @links = []
      @links.push(['New Message', new_user_message_path])
      @links.push(['Outbox', outbox_user_messages_path])
      @links.push(['Trashbin', trashbin_user_messages_path])

      render :action => "index"
   end

   # GET /messages/outbox
   # Displays all messages sent by the user
   def outbox
      session[:mail_box] = "outbox"
      @messages = current_user.outbox_messages
      @page_title = "Outbox For: " + session[:user_name]
      @current_tab = "Outbox"
      @links = []
      @links.push(['New Message', new_user_message_path])
      @links.push(['Inbox', inbox_user_messages_path])
      @links.push(['Trashbin', trashbin_user_messages_path])

      render :action => "index"
   end

   # GET /messages/trashbin
   # Displays all messages deleted from the user's inbox
   def trashbin
      session[:mail_box] = "trashbin"
      @messages = current_user.trashbin_messages
      @page_title = "Trashbin For: " + session[:user_name]
      @current_tab = "Trashbin"
      @links = []
      @links.push(['New Message', new_user_message_path])
      @links.push(['Inbox', inbox_user_messages_path])
      @links.push(['Outbox', outbox_user_messages_path])

      render :action => "index"
   end

   # GET /messages/1/reply
   def reply
      params[:id] or raise_missing "Message ID"
      @message = Message.find_by_id(params[:id]) or raise_not_found "Message ID #{params[:id]}"
      @message.body =~ /DO NOT REPLY/ and raise_not_allowed "Reply to This Message"
      @current_tab = "New Message"
      @page_title = "Reply To Message"

      can_view(@message) or raise_not_allowed "View This Message"
      @message.receiver_id = @message.sender_id
      @message.subject = "Re: " + @message.subject 
      @message.body = "\n\n___________________________\nOn #{format_date(@message.created_at)}, #{@message.sender.name} wrote:\n\n" + @message.body
      render :action => "new"
   end
   
   # if print button is clicked, will print a digest, otherwise will
   # destroy the selected messages.
   def select_messages
      params[:selected] or raise_missing "Message Selection"
      ['Digest', 'Delete'].include?(params[:commit]) or raise_invalid "Commit", "Must click either Delete or Digest buttons!"
      if params[:commit] == 'Digest'
         @messages = params[:selected].map { |m| Message.find_by_id(m) }
         @links = [["Print","javascript:window.print()"],["Return",current_mailbox]]
         render :template => 'messages/print_digest.html.erb', :layout => 'print'
      else
         require_method :post
         messages = params[:selected].map { |m| Message.find_by_id(m) } 
         messages.each do |message| 
            mark_message_for_destruction(message)
         end
         redirect_to current_mailbox
      end
      return
   end
   
   def print_message
      params[:id] or raise_missing "Message ID"
      @message = Message.find_by_id(params[:id]) or raise_not_found "Message ID #{params[:id]}"
      can_view(@message) or raise_not_allowed "View This Message"
      @tips = nil
      @links = [['Print', 'javascript:window.print()'],['Return', user_message_path(current_user, @message)]]
      render :template => 'messages/show', :layout => 'print'
   end
   
   def warning
      render :layout => 'popup'
   end
   
   protected

   # Security check to make sure the requesting user is either the 
   # sender (for outbox display) or the receiver (for inbox or trash_bin display)
   def can_view(message)
      true if !message.nil? and (session[:user] == message.sender_id or session[:user] == message.receiver_id)
   end

   def current_mailbox
      case session[:mail_box]
      when "inbox"
         inbox_user_messages_path
      when "outbox"
         outbox_user_messages_path
      when "trashbin"
         trashbin_user_messages_path
      else
         inbox_user_messages_path
      end
   end

   # Performs a "soft" delete of a message then check if it can do a destroy on the message
   # * Marks Inbox messages as "receiver deleted" essentially moving the message to the Trash Bin
   # * Marks Outbox messages as "sender_deleted" and "purged" removing the message from [:inbox_messages, :outbox_messages, :trashbin_messages]
   # * Marks Trash Bin messages as "receiver purged"
   # * Checks to see if both the sender and reciever have purged the message.  If so, the message record is destroyed
   #
   # Returns to the updated view of the current "mailbox"
   def mark_message_for_destruction(message)
      if can_view(message)

         # "inbox"
         if session[:user] == message.receiver_id and !message.receiver_deleted
            message.receiver_deleted = true             

            # "outbox"
         elsif session[:user] == message.sender_id and !message.sender_deleted
            message.sender_deleted = true
            message.sender_purged = true

            # "trash_bin"
         elsif session[:user] == message.receiver_id and message.receiver_deleted
            message.receiver_purged = true
         end

         message.save(false) 
         message.purge
      end
   end
   
end
