# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Offense photos are crime scene photos uploaded into the system for offense
# reports. An offense report can have any number of offense photos.
class OffensePhotosController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active

   # Offense Photos Listing
   def index
      require_permission Perm[:view_offense]
      @help_page = ["Offense Photos","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:offense_photo_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:offense_photo_filter] = nil
            redirect_to offense_photos_path and return
         end
      else
         @query = session[:offense_photo_filter] || HashWithIndifferentAccess.new
      end
      unless @offense_photos = paged('offense_photo',:order => 'offense_id DESC, date_taken DESC', :include => :offense)
         @query = HashWithIndifferentAccess.new
         session[:offense_photo_filter] = nil
         redirect_to offense_photos_path and return
      end
      @links = []
      if has_permission Perm[:update_offense]
         @links.push(['New', edit_offense_photo_path])
      end
   end

   # Show Offense Photo
   def show
      require_permission Perm[:view_offense]
      @help_page = ["Offense Photos","show"]
      params[:id] or raise_missing "Offense Photo ID"
      @offense_photo = OffensePhoto.find_by_id(params[:id]) or raise_not_found "Offense Photo ID #{params[:id]}"
      @links = []
      if has_permission Perm[:update_offense]
         @links.push(["New", edit_offense_photo_path])
         @links.push(["Edit", edit_offense_photo_path(:id => @offense_photo.id)])
      end
      if has_permission Perm[:destroy_offense]
         @links.push(['Delete', @offense_photo, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Offense Photo?'}])
      end
      if has_permission Perm[:update_offense]
         @links.push(['Photo', crime_scene_photo_path(:id => @offense_photo.id)])
      end
      @page_links = [@offense_photo.offense,[@offense_photo.taken_by_id,'taken by'],[@offense_photo.creator, 'creator'],[@offense_photo.updater, 'updater']]
   end

   # Add or Edit Offense Photo
   def edit
      require_permission Perm[:update_offense]
      @help_page = ["Offense Photos","edit"]
      if params[:id]
         @offense_photo = OffensePhoto.find_by_id(params[:id]) or raise_not_found "Offense Photo ID #{params[:id]}"
         @page_title = "Edit Photo ID: #{@offense_photo.id}"
         @offense_photo.updated_by = session[:user]
      else
         @page_title = "New Crime Scene Photo"
         @offense_photo = OffensePhoto.new(:created_by => session[:user], :updated_by => session[:user]) or raise_db(:new, "Offense Photo")
         if params[:offense_id]
            @offense_photo.offense_id = params[:offense_id]
            unless @offense_photo.offense.nil?
               @offense_photo.case_no = @offense_photo.offense.case_no
            end
         end
      end
      if request.post?
         if params[:commit] == "Cancel"
            if @offense_photo.new_record?
               redirect_to offense_photo_path
            else
               redirect_to @offense_photo
            end
            return
         end
         @offense_photo.attributes = params[:offense_photo]
         if @offense_photo.save
            redirect_to @offense_photo
            if params[:id]
               user_action("Offense Photo","Edited Photo ID: #{@offense_photo.id}.")
            else
               user_action("Offense Photo", "Added Photo ID: #{@offense_photo.id}.")
            end
         end
      end
   end
   
   # Crime Scene Photo
   def crime_scene_photo
      require_permission Perm[:update_offense]
      @help_page = ["Offense Photos","crime_scene_photo"]
      params[:id] or raise_missing "Offense Photo ID"
      @offense_photo = OffensePhoto.find_by_id(params[:id]) or raise_not_found "Offense Photo ID #{params[:id]}"
      @page_title = "Crime Scene Photo"
      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to @offense_photo
            return
         end
         @offense_photo.updated_by = session[:user]
         @offense_photo.attributes = params[:offense_photo]
         if params[:commit] == 'Delete'
            @offense_photo.crime_scene_photo = nil
         end
         if @offense_photo.save
            redirect_to @offense_photo
            if params[:commit] == 'Delete'
               user_action("Offense Photo","Removed Photo for Offense Photo ID: #{@offense_photo.id}.")
            else
               user_action("Offense Photo","Updated Photo for Offense Photo ID: #{@offense_photo.id}.")
            end
         end
      end
   end

   # Destroys the specified offense photo. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_offense]
      require_method :delete
      params[:id] or raise_missing "Offense Photo ID"
      @offense_photo = OffensePhoto.find_by_id(params[:id]) or raise_not_found "Offense Photo ID #{params[:id]}"
      @offense_photo.destroy or raise_db(:destroy, "Offense Photo ID #{params[:id]}")
      user_action("Offense Photo","Destroyed Offense Photo ID: #{params[:id]}.")
      flash[:notice] = "Record Deleted!"
      redirect_to offense_photos_path
   end

   protected

   # ensures that "Offense Photos" menu item is highlighted for all actions
   # in this controller.
   def set_tab_name
      @current_tab = "Offense Photos"
   end
end
