# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Investigations
class InvestigationsController < ApplicationController
   before_filter :set_tab_name
   before_filter :update_session_active
   before_filter :verify_detective
   
   # Investigation Listing
   def index
      require_permission Perm[:view_investigation]
      @help_page = ["Investigations","index"]
      if @query = params[:search]
         if params[:commit] == "Set Filter"
            session[:invest_filter] = @query
         else
            @query = HashWithIndifferentAccess.new
            session[:invest_filter] = nil
            redirect_to investigations_path and return
         end
      else
         @query = session[:invest_filter] || HashWithIndifferentAccess.new
      end
      solved = Option.lookup("Investigation Status","Solved",nil,nil,true)
      unless @investigations = paged('investigation', :include => [:cases, :reports], :order => 'investigations.id DESC', :active => "investigations.status_id != #{solved}")
         @query = HashWithIndifferentAccess.new
         session[:invest_filter] = nil
         redirect_to investigations_path and return
      end
      @links = []
      if session[:show_all_investigations]
         @links.push(['Show Active', investigations_path(:show_all => '0')])
      else
         @links.push(['Show All', investigations_path(:show_all => '1')])
      end
      if has_permission Perm[:update_investigation]
         @links.push(['New', edit_investigation_path])
      end
   end

   # Show Investigation
   def show
      require_permission Perm[:view_investigation]
      @help_page = ["Investigations","show"]
      params[:id] or raise_missing "Investigation ID"
      @investigation = Investigation.find_by_id(params[:id]) or raise_not_found "Investigation ID #{params[:id]}"
      enforce_privacy(@investigation)
      @links = []
      if has_permission Perm[:update_investigation]
         @links.push(['New', edit_investigation_path])
         if check_author(@investigation)
            @links.push(['Edit', edit_investigation_path(:id => @investigation.id)])
         end
      end
      if has_permission(Perm[:destroy_investigation]) && check_author(@investigation)
         @links.push(['Delete', @investigation, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Investigation?'}])
      end
      if has_permission(Perm[:update_investigation]) && check_author(@investigation)
         @links.push(['New Case', edit_invest_case_path(:investigation_id => @investigation.id)])
         @links.push(['New Crime', edit_invest_crime_path(:investigation_id => @investigation.id)])
         @links.push(['New Criminal', edit_invest_criminal_path(:investigation_id => @investigation.id)])
         @links.push(['New Event', edit_event_path(:investigation_id => @investigation.id)])
         @links.push(['New Evidence', edit_evidence_path(:investigation_id => @investigation.id)])
         @links.push(['New Photo', edit_invest_photo_path(:investigation_id => @investigation.id)])
         @links.push(['New Report', edit_invest_report_path(:investigation_id => @investigation.id)])
         @links.push(['New Stolen', edit_stolen_path(:investigation_id => @investigation.id)])
         @links.push(['New Vehicle', edit_invest_vehicle_path(:investigation_id => @investigation.id)])
         @links.push(['New Warrant', edit_warrant_path(:investigation_id => @investigation.id)])
      end
      @page_links = [[@investigation.detective_id,'detective'],[@investigation.creator, 'creator'],[@investigation.updater, 'updater']]
   end

   # Add or Edit Investigation
   def edit
      require_permission Perm[:update_investigation]
      @help_page = ["Investigations","edit"]
      if params[:id]
         @investigation = Investigation.find_by_id(params[:id]) or raise_not_found "Investigation ID #{params[:id]}"
         enforce_privacy(@investigation)
         enforce_author(@investigation)
         @page_title = "Edit Investigation ID: #{@investigation.id}"
         @investigation.updated_by = session[:user]
         unless request.post?
            if @investigation.witnesses.empty?
               @investigation.witnesses.build
            end
            if @investigation.victims.empty?
               @investigation.victims.build
            end
            if @investigation.sources.empty?
               @investigation.sources.build
            end
            if @investigation.contacts.empty?
               @investigation.contacts.build
            end
         end
      else
         @investigation = Investigation.new(:created_by => session[:user], :updated_by => session[:user], :owned_by => session[:user]) or raise_db(:new, "Investigation")
         @page_title = "New Investigation"
         @investigation.investigation_date, @investigation.investigation_time = get_date_and_time
         if !current_user.contact.nil?
            @investigation.detective_id = current_user.contact.id
         else
            @investigation.detective = current_user.name
            @investigation.detective_unit = current_user.unit
            @investigation.detective_badge = current_user.badge
         end
         @investigation.status_id = Option.lookup("Investigation Status","Active",nil,nil,true)
         @investigation.private = true
         unless request.post?
            @investigation.witnesses.build
            @investigation.victims.build
            @investigation.sources.build
            @investigation.contacts.build
         end
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @investigation.new_record?
               redirect_to investigations_path
            else
               redirect_to @investigation
            end
            return
         end
         params[:investigation][:assigned_victims] ||= {}
         params[:investigation][:assigned_sources] ||= {}
         params[:investigation][:assigned_witnesses] ||= {}
         params[:investigation][:assigned_contacts] ||= {}
         @investigation.attributes = params[:investigation]
         if @investigation.save
            redirect_to @investigation
            if params[:id]
               user_action("Investigations","Edited Investigation ID: #{@investigation.id}.")
            else
               user_action("Investigations","Added Investigation ID: #{@investigation.id}.")
            end
         end
      end
   end
   
   # Destroys the specified Investigation record. Only responds to DELETE methods.
   def destroy
      require_permission Perm[:destroy_investigation]
      require_method :delete
      params[:id] or raise_missing "Investigation ID"
      @investigation = Investigation.find_by_id(params[:id]) or raise_not_found "Investigation ID #{params[:id]}"
      enforce_privacy(@investigation)
      enforce_author(@investigation)
      @investigation.destroy or raise_db(:destroy, "Investigation ID #{params[:id]}")
      user_action("Investigations", "Destroyed Investigation ID #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to investigations_path
   end

   # Show Investigation Case
   def show_case
      require_permission Perm[:view_investigation]
      @help_page = ["Investigations","show_case"]
      params[:id] or raise_missing "Case ID"
      @invest_case = InvestCase.find_by_id(params[:id]) or raise_not_found "Case ID #{params[:id]}"
      enforce_privacy(@invest_case)
      @links = []
      if has_permission(Perm[:update_investigation]) && check_author(@invest_case)
         @links.push(['Edit', edit_invest_case_path(:id => @invest_case.id)])
         @links.push(['Delete', @invest_case, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Investigation Case?'}])
      end
      @page_links = [[@invest_case.investigation, 'invest'], [@invest_case.crime,'crime'],[@invest_case.creator,'creater'],[@invest_case.updater,'updater']]
   end

   # Add or Edit Investigation Case
   def edit_case
      require_permission Perm[:update_investigation]
      @help_page = ["Investigations","edit_case"]
      if params[:id]
         @invest_case = InvestCase.find_by_id(params[:id]) or raise_not_found "Case ID #{params[:id]}"
         enforce_privacy(@invest_case)
         enforce_author(@invest_case)
         @page_title = "Edit Case ID: #{@invest_case.id}"
         @invest_case.updated_by = session[:user]
      else
         if params[:invest_crime_id]
            inv_crime = InvestCrime.find_by_id(params[:invest_crime_id]) or raise_not_found "Crime ID #{params[:invest_crime_id]}"
            return_path = url_for(inv_crime)
            owner = inv_crime.owned_by
            crime_id = inv_crime.id
            id = inv_crime.investigation_id
            priv = inv_crime.private?
         elsif params[:investigation_id]
            inv = Investigation.find_by_id(params[:investigation_id]) or raise_not_found "Investigation ID #{params[:investigation_id]}"
            return_path = url_for(inv)
            owner = inv.owned_by
            id = inv.id
            crime_id = nil
            priv = inv.private?
         else
            raise_missing "Investigation ID or Crime ID"
         end
         @invest_case = InvestCase.new(
            :investigation_id => id,
            :invest_crime_id => crime_id,
            :private => priv,
            :created_by => session[:user],
            :updated_by => session[:user],
            :owned_by => owner
         ) or raise_db(:new, "Investigation Case")
         @page_title = "New Case"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @invest_case.new_record?
               redirect_to return_path
            else
               redirect_to @invest_case
            end
            return
         end
         @invest_case.attributes = params[:invest_case]
         if @invest_case.save
            redirect_to @invest_case
            if params[:id]
               user_action("Investigations","Edited Case ID: #{@invest_case.id}.")
            else
               user_action("Investigations","Added Case ID: #{@invest_case.id}.")
            end
         end
      end
   end
   
   # Destroys the specified Investigation case record. Only responds to DELETE methods.
   def destroy_case
      require_permission Perm[:update_investigation]
      require_method :delete
      params[:id] or raise_missing "Case ID"
      @invest_case = InvestCase.find_by_id(params[:id]) or raise_not_found "Case ID #{params[:id]}"
      enforce_privacy(@invest_case)
      enforce_author(@invest_case)
      return_path = investigations_path
      if !@invest_case.crime.nil?
         return_path = url_for(@invest_case.crime)
      elsif !@invest_case.investigation.nil?
         return_path = url_for(@invest_case.investigation)
      end
      @invest_case.destroy or raise_db(:destroy, "Case ID #{params[:id]}")
      user_action("Investigations", "Destroyed Case ID #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to return_path
   end

   # Show Investigation Photo
   def show_photo
      require_permission Perm[:view_investigation]
      @help_page = ["Investigations","show_photo"]
      params[:id] or raise_missing "Photo ID"
      @invest_photo = InvestPhoto.find_by_id(params[:id]) or raise_not_found "Photo ID #{params[:id]}"
      enforce_privacy(@invest_photo)
      @links = []
      if has_permission(Perm[:update_investigation]) && check_author(@invest_photo)
         @links.push(['Edit', edit_invest_photo_path(:id => @invest_photo.id)])
         @links.push(['Delete', @invest_photo, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Investigation Photo?'}])
         @links.push(['Photo', investigation_photo_path(:id => @invest_photo.id)])
      end
      @page_links = [[@invest_photo.investigation,'invest'],[@invest_photo.crime,'crime'],[@invest_photo.criminal, 'criminal'],[@invest_photo.report,'report'],[@invest_photo.vehicle,'vehicle'],[@invest_photo.taken_by_id,'taken by'],[@invest_photo.creator,'creator'],[@invest_photo.updater,'updater']]
   end

   # Add or Edit Investigation Photo
   def edit_photo
      require_permission Perm[:update_investigation]
      @help_page = ["Investigations","edit_photo"]
      if params[:id]
         @invest_photo = InvestPhoto.find_by_id(params[:id]) or raise_not_found "Photo ID #{params[:id]}"
         enforce_privacy(@invest_photo)
         enforce_author(@invest_photo)
         @page_title = "Edit Photo ID: #{@invest_photo.id}"
         @invest_photo.updated_by = session[:user]
      else
         if params[:invest_vehicle_id]
            inv_vehicle = InvestVehicle.find_by_id(params[:invest_vehicle_id]) or raise_not_found "Vehicle ID #{params[:invest_vehicle_id]}"
            return_path = url_for(inv_vehicle)
            owner = inv_vehicle.owned_by
            id = inv_vehicle.investigation_id
            crime_id = inv_vehicle.invest_crime_id
            report_id = inv_vehicle.invest_report_id
            criminal_id = inv_vehicle.invest_criminal_id
            vehicle_id = inv_vehicle.id
            priv = inv_vehicle.private?
         elsif params[:invest_criminal_id]
            inv_criminal = InvestCriminal.find_by_id(params[:invest_criminal_id]) or raise_not_found "Criminal ID #{params[:invest_criminal_id]}"
            return_path = url_for(inv_criminal)
            owner = inv_criminal.owned_by
            id = inv_criminal.investigation_id
            crime_id = inv_criminal.invest_crime_id
            report_id = inv_criminal.invest_report_id
            criminal_id = inv_criminal.id
            vehicle_id = nil
            priv = inv_criminal.private?
         elsif params[:invest_report_id]
            inv_report = InvestReport.find_by_id(params[:invest_report_id]) or raise_not_found "Report ID #{params[:invest_report_id]}"
            return_path = url_for(inv_report)
            owner = inv_report.owned_by
            id = inv_report.investigation_id
            crime_id = inv_report.invest_crime_id
            report_id = inv_report.id
            criminal_id = nil
            vehicle_id = nil
            priv = inv_report.private?
         elsif params[:invest_crime_id]
            inv_crime = InvestCrime.find_by_id(params[:invest_crime_id]) or raise_not_found "Crime ID #{params[:invest_crime_id]}"
            return_path = url_for(inv_crime)
            owner = inv_crime.owned_by
            id = inv_crime.investigation_id
            crime_id = inv_crime.id
            report_id = nil
            criminal_id = nil
            vehicle_id = nil
            priv = inv_crime.private?
         elsif params[:investigation_id]
            inv = Investigation.find_by_id(params[:investigation_id]) or raise_not_found "Investigation ID #{params[:investigation_id]}"
            return_path = url_for(inv)
            owner = inv.owned_by
            id = inv.id
            crime_id = nil
            report_id = nil
            criminal_id = nil
            vehicle_id = nil
            priv = inv.private?
         else
            raise_missing "Investigation ID, Crime ID, Report ID, Criminal ID or Vehicle ID"
         end
         @invest_photo = InvestPhoto.new(
            :investigation_id => id,
            :invest_crime_id => crime_id,
            :invest_report_id => report_id,
            :invest_criminal_id => criminal_id,
            :invest_vehicle_id => vehicle_id,
            :private => priv,
            :created_by => session[:user],
            :updated_by => session[:user],
            :owned_by => owner
         ) or raise_db(:new, "Investigation Photo")
         @page_title = "New Photo"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @invest_photo.new_record?
               redirect_to return_path
            else
               redirect_to @invest_photo
            end
            return
         end
         @invest_photo.attributes = params[:invest_photo]
         if @invest_photo.save
            redirect_to @invest_photo
            if params[:id]
               user_action("Investigations","Edited Photo ID: #{@invest_photo.id}.")
            else
               user_action("Investigations","Added Photo ID: #{@invest_photo.id}.")
            end
         end
      end
   end
   
   # Destroys the specified Investigation photo record. Only responds to DELETE methods.
   def destroy_photo
      require_permission Perm[:update_investigation]
      require_method :delete
      params[:id] or raise_missing "Photo ID"
      @invest_photo = InvestPhoto.find_by_id(params[:id]) or raise_not_found "Photo ID #{params[:id]}"
      require_privacy(@invest_photo)
      require_author(@invest_photo)
      return_path = investigations_path
      if !@invest_photo.vehicle.nil?
         return_path = url_for(@invest_photo.vehicle)
      elsif !@invest_photo.criminal.nil?
         return_path = url_for(@invest_photo.criminal)
      elsif !@invest_photo.report.nil?
         return_path = url_for(@invest_photo.report)
      elsif !@invest_photo.crime.nil?
         return_path = url_for(@invest_photo.crime)
      elsif !@invest_photo.investigation.nil?
         return_path = url_for(@invest_photo.investigation)
      end
      @invest_photo.destroy or raise_db(:destroy, "Photo ID #{params[:id]}")
      user_action("Investigations", "Destroyed Photo ID #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to return_path
   end

   # Investigation Photo
   def investigation_photo
      require_permission Perm[:update_investigation]
      @help_page = ["Investigations","investigation_photo"]
      params[:id] or raise_missing "Photo ID"
      @invest_photo = InvestPhoto.find_by_id(params[:id]) or raise_not_found "Photo ID #{params[:id]}"
      enforce_privacy(@invest_photo)
      enforce_author(@invest_photo)
      @page_title = "Photo"
      @page_links = [[@invest_photo.investigation,'invest'],[@invest_photo.crime,'crime'],[@invest_photo.criminal,'criminal'],[@invest_photo.report,'report'],[@invest_photo.vehicle,'vehicle']]
      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to @invest_photo
            return
         end
         @invest_photo.updated_by = session[:user]
         @invest_photo.attributes = params[:invest_photo]
         if params[:commit] == 'Delete'
            @invest_photo.investigation_photo = nil
         end
         if @invest_photo.save
            redirect_to @invest_photo
            if params[:commit] == 'Delete'
               user_action("Investigations","Removed Photo for Photo ID: #{@invest_photo.id}.")
            else
               user_action("Investigations","Updated Photo for Photo ID: #{@invest_photo.id}.")
            end
         end
      end
   end

   # Show Investigation Vehicle
   def show_vehicle
      require_permission Perm[:view_investigation]
      @help_page = ["Investigations","show_vehicle"]
      params[:id] or raise_missing "Vehicle ID"
      @invest_vehicle = InvestVehicle.find_by_id(params[:id]) or raise_not_found "Vehicle ID #{params[:id]}"
      enforce_privacy(@invest_vehicle)
      @links = []
      if has_permission(Perm[:update_investigation]) && check_author(@invest_vehicle)
         @links.push(['Edit', edit_invest_vehicle_path(:id => @invest_vehicle.id)])
         @links.push(['Delete', @invest_vehicle, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Investigation Vehicle?'}])
         @links.push(['New Photo', edit_invest_photo_path(:invest_vehicle_id => @invest_vehicle.id)])
      end
      @page_links = [[@invest_vehicle.investigation,'invest'],[@invest_vehicle.crime,'crime'],[@invest_vehicle.criminal,'criminal'],[@invest_vehicle.report,'report'],[@invest_vehicle.registered_owner,'owned by'],[@invest_vehicle.creator,'creator'],[@invest_vehicle.updater,'updater']]
   end

   # Add or Edit Investigation Vehicle
   def edit_vehicle
      require_permission Perm[:update_investigation]
      @help_page = ["Investigations","edit_vehicle"]
      if params[:id]
         @invest_vehicle = InvestVehicle.find_by_id(params[:id]) or raise_not_found "Vehicle ID #{params[:id]}"
         enforce_privacy(@invest_vehicle)
         enforce_author(@invest_vehicle)
         @page_title = "Edit Vehicle ID: #{@invest_vehicle.id}"
         @invest_vehicle.updated_by = session[:user]
      else
         if params[:invest_criminal_id]
            inv_criminal = InvestCriminal.find_by_id(params[:invest_criminal_id]) or raise_not_found "Criminal ID #{params[:invest_criminal_id]}"
            return_path = url_for(inv_criminal)
            owner = inv_criminal.owned_by
            id = inv_criminal.investigation_id
            crime_id = inv_criminal.invest_crime_id
            report_id = inv_criminal.invest_report_id
            criminal_id = inv_criminal.id
            priv = inv_criminal.private?
         elsif params[:invest_report_id]
            inv_report = InvestReport.find_by_id(params[:invest_report_id]) or raise_not_found "Report ID #{params[:invest_report_id]}"
            return_path = url_for(inv_report)
            owner = inv_report.owned_by
            id = inv_report.investigation_id
            crime_id = inv_report.invest_crime_id
            report_id = inv_report.id
            criminal_id = nil
            priv = inv_report.private?
         elsif params[:invest_crime_id]
            inv_crime = InvestCrime.find_by_id(params[:invest_crime_id]) or raise_not_found "Crime ID #{params[:invest_crime_id]}"
            return_path = url_for(inv_crime)
            owner = inv_crime.owned_by
            id = inv_crime.investigation_id
            crime_id = inv_crime.id
            report_id = nil
            criminal_id = nil
            priv = inv_crime.private?
         elsif params[:investigation_id]
            inv = Investigation.find_by_id(params[:investigation_id]) or raise_not_found "Investigation ID #{params[:investigation_id]}"
            return_path = url_for(inv)
            owner = inv.owned_by
            id = inv.id
            crime_id = nil
            report_id = nil
            criminal_id = nil
            priv = inv.private?
         else
            raise_missing "Investigation ID, Crime ID, Report ID or Criminal ID"
         end
         @invest_vehicle = InvestVehicle.new(
            :investigation_id => id,
            :invest_crime_id => crime_id,
            :invest_report_id => report_id,
            :invest_criminal_id => criminal_id,
            :private => priv,
            :created_by => session[:user],
            :updated_by => session[:user],
            :owned_by => owner
         ) or raise_db(:new, "Investigation Vehicle")
         @page_title = "New Vehicle"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @invest_vehicle.new_record?
               redirect_to return_path
            else
               redirect_to @invest_vehicle
            end
            return
         end
         @invest_vehicle.attributes = params[:invest_vehicle]
         if @invest_vehicle.save
            redirect_to @invest_vehicle
            if params[:id]
               user_action("Investigations","Edited Vehicle ID: #{@invest_vehicle.id}.")
            else
               user_action("Investigations","Added Vehicle ID: #{@invest_vehicle.id}.")
            end
         end
      end
   end
   
   # Destroys the specified Investigation vehicle record. Only responds to DELETE methods.
   def destroy_vehicle
      require_permission Perm[:update_investigation]
      require_method :delete
      params[:id] or raise_missing "Vehicle ID"
      @invest_vehicle = InvestVehicle.find_by_id(params[:id]) or raise_not_found "Vehicle ID #{params[:id]}"
      enforce_privacy(@invest_vehicle)
      enforce_author(@invest_vehicle)
      return_path = investigations_path
      if !@invest_vehicle.criminal.nil?
         return_path = url_for(@invest_vehicle.criminal)
      elsif !@invest_vehicle.report.nil?
         return_path = url_for(@invest_vehicle.report)
      elsif !@invest_vehicle.crime.nil?
         return_path = url_for(@invest_vehicle.crime)
      elsif !@invest_vehicle.investigation.nil?
         return_path = url_for(@invest_vehicle.investigation)
      end
      @invest_vehicle.destroy or raise_db(:destroy, "Vehicle ID #{params[:id]}")
      user_action("Investigations", "Destroyed Vehicle ID #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to return_path
   end

   # Show Investigation Criminal
   def show_criminal
      require_permission Perm[:view_investigation]
      @help_page = ["Investigations","show_criminal"]
      params[:id] or raise_missing "Criminal ID"
      @invest_criminal = InvestCriminal.find_by_id(params[:id]) or raise_not_found "Criminal ID #{params[:id]}"
      enforce_privacy(@invest_criminal)
      @links = []
      if has_permission(Perm[:update_investigation]) && check_author(@invest_criminal)
         @links.push(['Edit', edit_invest_criminal_path(:id => @invest_criminal.id)])
         @links.push(['Delete', @invest_criminal, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Investigation Criminal?'}])
         @links.push(['New Photo', edit_invest_photo_path(:invest_criminal_id => @invest_criminal.id)])
         @links.push(['New Stolen', edit_stolen_path(:invest_criminal_id => @invest_criminal.id)])
         @links.push(['New Vehicle', edit_invest_vehicle_path(:invest_criminal_id => @invest_criminal.id)])
         @links.push(['New Warrant', edit_warrant_path(:invest_criminal_id => @invest_criminal.id)])
      end
      @page_links = [@invest_criminal.person,[@invest_criminal.investigation,'invest'],[@invest_criminal.crime,'crime'],[@invest_criminal.report,'report'],[@invest_criminal.creator, 'creator'],[@invest_criminal.updater,'updater']]
   end

   # Add or Edit Investigation Criminal
   def edit_criminal
      require_permission Perm[:update_investigation]
      @help_page = ["Investigations","edit_criminal"]
      if params[:id]
         @invest_criminal = InvestCriminal.find_by_id(params[:id]) or raise_not_found "Criminal ID #{params[:id]}"
         enforce_privacy(@invest_criminal)
         enforce_author(@invest_criminal)
         @page_title = "Edit Criminal ID: #{@invest_criminal.id}"
         @invest_criminal.updated_by = session[:user]
      else
         if params[:invest_report_id]
            inv_report = InvestReport.find_by_id(params[:invest_report_id]) or raise_not_found "Report ID #{params[:invest_report_id]}"
            return_path = url_for(inv_report)
            owner = inv_report.owned_by
            id = inv_report.investigation_id
            crime_id = inv_report.invest_crime_id
            report_id = inv_report.id
            priv = inv_report.private?
         elsif params[:invest_crime_id]
            inv_crime = InvestCrime.find_by_id(params[:invest_crime_id]) or raise_not_found "Crime ID #{params[:invest_crime_id]}"
            return_path = url_for(inv_crime)
            owner = inv_crime.owned_by
            id = inv_crime.investigation_id
            crime_id = inv_crime.id
            report_id = nil
            priv = inv_crime.private?
         elsif params[:investigation_id]
            inv = Investigation.find_by_id(params[:investigation_id]) or raise_not_found "Investigation ID #{params[:investigation_id]}"
            return_path = url_for(inv)
            owner = inv.owned_by
            id = inv.id
            crime_id = nil
            report_id = nil
            priv = inv.private?
         else
            raise_missing "Investigation ID, Report ID or Crime ID"
         end
         @invest_criminal = InvestCriminal.new(
            :investigation_id => id,
            :invest_crime_id => crime_id,
            :invest_report_id => report_id,
            :private => priv,
            :created_by => session[:user],
            :updated_by => session[:user],
            :owned_by => owner
         ) or raise_db(:new, "Investigation Criminal")
         @page_title = "New Criminal"
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @invest_criminal.new_record?
               redirect_to return_path
            else
               redirect_to @invest_criminal
            end
            return
         end
         @invest_criminal.attributes = params[:invest_criminal]
         if @invest_criminal.save
            redirect_to @invest_criminal
            if params[:id]
               user_action("Investigations","Edited Criminal ID: #{@invest_criminal.id}.")
            else
               user_action("Investigations","Added Criminal ID: #{@invest_criminal.id}.")
            end
         end
      end
   end
   
   # Destroys the specified Investigation criminal record. Only responds to DELETE methods.
   def destroy_criminal
      require_permission Perm[:update_investigation]
      require_method :delete
      params[:id] or raise_missing "Criminal ID"
      @invest_criminal = InvestCriminal.find_by_id(params[:id]) or raise_not_found "Criminal ID #{params[:id]}"
      enforce_privacy(@invest_criminal)
      enforce_author(@invest_criminal)
      return_path = investigations_path
      if !@invest_criminal.report.nil?
         return_path = url_for(@invest_criminal.report)
      elsif !@invest_criminal.crime.nil?
         return_path = url_for(@invest_criminal.crime)
      elsif !@invest_criminal.investigation.nil?
         return_path = url_for(@invest_criminal.investigation)
      end
      @invest_criminal.destroy or raise_db(:destroy, "Criminal ID #{params[:id]}")
      user_action("Investigations", "Destroyed Criminal ID #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to return_path
   end
   
   # Show Investigation Report
   def show_report
      require_permission Perm[:view_investigation]
      @help_page = ["Investigations","show_report"]
      params[:id] or raise_missing "Report ID"
      @invest_report = InvestReport.find_by_id(params[:id]) or raise_not_found "Report ID #{params[:id]}"
      enforce_privacy(@invest_report)
      @links = []
      if has_permission(Perm[:update_investigation]) && check_author(@invest_report)
         @links.push(['Edit', edit_invest_report_path(:id => @invest_report.id)])
         @links.push(['Delete', @invest_report, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Investigation Report?'}])
         @links.push(['PDF', url_for(:action => 'print_report', :id => @invest_report.id)])
         @links.push(['New Photo', edit_invest_photo_path(:invest_report_id => @invest_report.id)])
         @links.push(['New Vehicle', edit_invest_vehicle_path(:invest_report_id => @invest_report.id)])
         @links.push(['New Criminal', edit_invest_criminal_path(:invest_report_id => @invest_report.id)])
      end
      @page_links = [[@invest_report.investigation,'invest'],[@invest_report.crime,'crime'],[@invest_report.creator,'creator'],[@invest_report.updater,'updater']]
   end

   # Add or Edit Investigation Report
   def edit_report
      require_permission Perm[:update_investigation]
      @help_page = ["Investigations","edit_reports"]
      if params[:id]
         @invest_report = InvestReport.find_by_id(params[:id]) or raise_not_found "Report ID #{params[:id]}"
         enforce_privacy(@invest_report)
         enforce_author(@invest_report)
         @page_title = "Edit Report ID: #{@invest_report.id}"
         @invest_report.updated_by = session[:user]
         unless request.post?
            if @invest_report.victims.empty?
               @invest_report.victims.build
            end
            if @invest_report.witnesses.empty?
               @invest_report.witnesses.build
            end
            if @invest_report.sources.empty?
               @invest_report.sources.build
            end
         end
      else
         if params[:invest_crime_id]
            inv_crime = InvestCrime.find_by_id(params[:invest_crime_id]) or raise_not_found "Crime ID #{params[:invest_crime_id]}"
            return_path = url_for(inv_crime)
            owner = inv_crime.owned_by
            id = inv_crime.investigation_id
            crime_id = inv_crime.id
            priv = inv_crime.private?
         elsif params[:investigation_id]
            inv = Investigation.find_by_id(params[:investigation_id]) or raise_not_found "Investigation ID #{params[:investigation_id]}"
            return_path = url_for(inv)
            owner = inv.owned_by
            id = inv.id
            crime_id = nil
            priv = inv.private?
         else
            raise_missing "Investigation ID or Crime ID"
         end
         @invest_report = InvestReport.new(
            :investigation_id => id,
            :invest_crime_id => crime_id,
            :private => priv,
            :created_by => session[:user],
            :updated_by => session[:user],
            :owned_by => owner
         ) or raise_db(:new, "Investigation Report")
         @page_title = "New Report"
         @invest_report.report_date, @invest_report.report_time = get_date_and_time
         unless request.post?
            @invest_report.victims.build
            @invest_report.witnesses.build
            @invest_report.sources.build
         end
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @invest_report.new_record?
               redirect_to return_path
            else
               redirect_to @invest_report
            end
            return
         end
         params[:invest_report][:assigned_victims] ||= {}
         params[:invest_report][:assigned_sources] ||= {}
         params[:invest_report][:assigned_witnesses] ||= {}
         @invest_report.attributes = params[:invest_report]
         if @invest_report.save
            redirect_to @invest_report
            if params[:id]
               user_action("Investigations","Edited Report ID: #{@invest_report.id}.")
            else
               user_action("Investigations","Added Report ID: #{@invest_report.id}.")
            end
         end
      end
   end
   
   # Destroys the specified Investigation report record. Only responds to DELETE methods.
   def destroy_report
      require_permission Perm[:update_investigation]
      require_method :delete
      params[:id] or raise_missing "Report ID"
      @invest_report = InvestReport.find_by_id(params[:id]) or raise_not_found "Report ID #{params[:id]}"
      enforce_privacy(@invest_report)
      enforce_author(@invest_report)
      return_path = investigations_path
      if !@invest_report.crime.nil?
         return_path = url_for(@invest_report.crime)
      elsif !@invest_report.investigation.nil?
         return_path = url_for(@invest_report.investigation)
      end
      @invest_report.destroy or raise_db(:destroy, "Report ID #{params[:id]}")
      user_action("Investigations", "Destroyed Report ID #{@invest_report.id}!")
      flash[:notice] = "Record Deleted!"
      redirect_to return_path
   end
   
   def print_report
      require_permission Perm[:view_investigation]
      params[:id] or raise_missing "Report ID"
      @invest_report = InvestReport.find_by_id(params[:id]) or raise_not_found "Report ID #{params[:id]}"
      enforce_privacy(@invest_report)
      prawnto :prawn => {
         :page_size => 'LETTER',
         :top_margin => 20,
         :bottom_margin => 20,
         :left_margin => 20,
         :right_margin => 20},
         :filename => "Investigative_Report-#{@invest_report.id}.pdf"
      render :template => 'investigations/print_report.pdf.prawn', :layout => false
      user_action("Investigations","Printed Report ID: #{@invest_report.id}.")
   end
   
   # Show Investigation Crime
   def show_crime
      require_permission Perm[:view_investigation]
      @help_page = ["Investigations","show_crime"]
      params[:id] or raise_missing "Crime ID"
      @invest_crime = InvestCrime.find_by_id(params[:id]) or raise_not_found "Crime ID #{params[:id]}"
      enforce_privacy(@invest_crime)
      @links = []
      if has_permission(Perm[:update_investigation]) && check_author(@invest_crime)
         @links.push(['Edit', edit_invest_crime_path(:id => @invest_crime.id)])
         @links.push(['Delete', @invest_crime, {:method => 'delete', :confirm => 'Are you sure you want to destroy this Investigation Crime?'}])
         @links.push(['New Case', edit_invest_case_path(:invest_crime_id => @invest_crime.id)])
         @links.push(['New Criminal', edit_invest_criminal_path(:invest_crime_id => @invest_crime.id)])
         @links.push(['New Evidence', edit_evidence_path(:invest_crime_id => @invest_crime.id)])
         @links.push(['New Photo', edit_invest_photo_path(:invest_crime_id => @invest_crime.id)])
         @links.push(['New Report', edit_invest_report_path(:invest_crime_id => @invest_crime.id)])
         @links.push(['New Stolen', edit_stolen_path(:invest_crime_id => @invest_crime.id)])
         @links.push(['New Vehicle', edit_invest_vehicle_path(:invest_crime_id => @invest_crime.id)])
         @links.push(['New Warrant', edit_warrant_path(:invest_crime_id => @invest_crime.id)])
      end
      @page_links = [[@invest_crime.investigation,'invest'],[@invest_crime.creator,'creator'],[@invest_crime.updater,'updater']]
   end

   # Add or Edit Investigation Crime
   def edit_crime
      require_permission Perm[:update_investigation]
      @help_page = ["Investigations","edit_crimes"]
      if params[:id]
         @invest_crime = InvestCrime.find_by_id(params[:id]) or raise_not_found "Crime ID #{params[:id]}"
         enforce_privacy(@invest_crime)
         enforce_author(@invest_crime)
         @page_title = "Edit Crime ID: #{@invest_crime.id}"
         @invest_crime.updated_by = session[:user]
         unless request.post?
            if @invest_crime.victims.empty?
               @invest_crime.victims.build
            end
            if @invest_crime.witnesses.empty?
               @invest_crime.witnesses.build
            end
            if @invest_crime.sources.empty?
               @invest_crime.sources.build
            end
         end
      else
         params[:investigation_id] or raise_missing "Investigation ID"
         inv = Investigation.find_by_id(params[:investigation_id]) or raise_not_found "Investigation ID #{params[:investigation_id]}"
         @invest_crime = InvestCrime.new(
            :investigation_id => inv.id,
            :private => inv.private,
            :created_by => session[:user],
            :updated_by => session[:user],
            :owned_by => inv.owned_by
         ) or raise_db(:new, "Investigation Crime")
         @page_title = "New Crime"
         unless request.post?
            @invest_crime.victims.build
            @invest_crime.witnesses.build
            @invest_crime.sources.build
         end
      end
      if request.post?
         if params[:commit] == 'Cancel'
            if @invest_crime.new_record?
               redirect_to (@invest_crime.investigation.nil? ? investigations_path : @invest_crime.investigation)
            else
               redirect_to @invest_crime
            end
            return
         end
         params[:invest_crime][:assigned_victims] ||= {}
         params[:invest_crime][:assigned_sources] ||= {}
         params[:invest_crime][:assigned_witnesses] ||= {}
         @invest_crime.attributes = params[:invest_crime]
         if @invest_crime.save
            redirect_to @invest_crime
            if params[:id]
               user_action("Investigations","Edited Crime ID: #{@invest_crime.id}.")
            else
               user_action("Investigations","Added Crime ID: #{@invest_crime.id}.")
            end
         end
      end
   end
   
   # Destroys the specified Investigation crime record. Only responds to DELETE methods.
   def destroy_crime
      require_permission Perm[:update_investigation]
      require_method :delete
      params[:id] or raise_missing "Crime ID"
      @invest_crime = InvestCrime.find_by_id(params[:id]) or raise_not_found "Crime ID #{params[:id]}"
      enforce_privacy(@invest_crime)
      enforce_author(@invest_crime)
      return_path = (@invest_crime.investigation.nil? ? investigations_path : @invest_crime.investigation)
      @invest_crime.destroy or raise_db(:destroy, "Crime ID #{params[:id]}")
      user_action("Investigations", "Destroyed Crime ID #{params[:id]}!")
      flash[:notice] = "Record Deleted!"
      redirect_to return_path
   end
   
   # accepts a person  id from the edit invest_criminal form and validates a person,
   # updating the full_name field from the person record.
   def update_person
      if params[:c] && has_permission(Perm[:view_person])
         if person = Person.find_by_id(params[:c])
            render :update do |page|
               page['invest_criminal_full_name'].value = person.full_name
               page['invest_criminal_full_name'].select()
            end
         else
            render :update do |page|
               page['invest_criminal_full_name'].value = "Invalid Person ID"
               page['invest_criminal_person_id'].select()
            end
         end
      else
         render :text => ""
      end
   end
   
   protected

   # ensures that all actions will have the "Investigations" menu item properly
   # highlighted as current.
   def set_tab_name
      @current_tab = "Investigation"
   end
   
   # ensures that only active detectives can access this controller
   def verify_detective
      raise PermissionDeniedError.new("You are not an active Detective and cannot access Investigations.") unless !current_user.contact.nil? && current_user.contact.active? && current_user.contact.officer? && current_user.contact.detective?
   end
end
