# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# This is a special controller for database admins showing table stats and
# giving access to maintenance tasks.
class DatabaseController < ApplicationController
   before_filter :update_session_active

   # Queries all models for description and record count and displays the
   # result in tabular form. Only Model classes that respond to the desc
   # method are included in the list.
   def index
      require_permission Perm[:view_database]
      @help_page = ["Database","index"]
      @current_tab = "Database"
      @page_title = "Database Administration"
      # build the model list
      models = []
      Dir.chdir(File.join(RAILS_ROOT, "app/models")) do 
         models = Dir["*.rb"]
      end
      @tables = []
      models.sort.each do |m|         
         class_name = m.sub(/\.rb$/,'').camelize
         # only want the active record models
         if class_name.constantize.superclass == ActiveRecord::Base
            if class_name.constantize.respond_to?('desc')
               desc = class_name.constantize.desc
               next if desc.nil?
            else
               desc = "desc method missing for model #{class_name}"
            end
            @tables.push({:model_name => "#{class_name}", :records => class_name.constantize.count, :description => desc})
         end
      end
   end
   
   # displays site configuration
   def show_config
      require_permission Perm[:admin]
      @current_tab = 'Site'
      @links = [['Edit', url_for(:action => 'edit_config')]]
   end
   
   # edit site configuration
   def edit_config
      require_permission Perm[:admin]
      @help_page = ["Site","edit_config"]
      @current_tab = "Site"
      @site_configuration = SiteConfiguration.new or raise_db(:new, "Site Configuration")
      # defaults loaded, now get site values
      @site_configuration.load_site_file or raise_not_found("Site Configuration File")
      if request.post?
         if params[:commit] == 'Cancel'
            redirect_to :action => 'show_config' and return
         end
         @site_configuration.attributes = params[:site_configuration]
         if @site_configuration.valid?
            # write the new data to file
            if @site_configuration.write_site_file
               flash[:notice] = "Site configuration file updated!"
               # TODO: update current SiteConfig object with new data
            else
               flash[:warning] = 'WARNING! Could not write configuration data to site configuration file!'
            end
            redirect_to :action => 'show_config'
         end
      end
   end

   # This will clear the sessions table of ALL records, including the one
   # currently in use by the dba. Login will be required after executing
   # this method.
   def clear_sessions
      require_permission Perm[:update_database]
      require_method :post
      user_action("Database","Cleared the session table!")
      Session.reset_table
      redirect_to databases_path
   end

   # This resets the case number sequence table. As a result, the next case
   # number generated after this method is used will consist of the current
   # 4 digit year followed by "00001". If this is done mid-year during a
   # production run, duplicate case numbers will be issued!
   def clear_cases
      require_permission Perm[:update_database]
      require_method :post
      user_action("Database", "Reset Case Numbers!")
      sql = Case.connection
      sql.execute "truncate table cases"
      redirect_to databases_path
   end
   
   # This cycles through the entire ZIP code table, performing the following
   # tasks:
   # * Converts 2 digit state values to upper case.
   # * Converts city values to proper case (first letter of each word
   #   capitalized.
   # * Converts county values to proper case.
   def fix_zip
      require_permission Perm[:update_database]
      require_method :post
      user_action("Database", "Fixed the ZIP Code Table!")
      @zips = Zip.find(:all)
      @zips.each do |z|
         # fix state
         z.update_attribute(:state, z.state.upcase)
         # fix city and county
         z.update_attribute(:city, z.city.titleize)
         z.update_attribute(:county, z.county.titleize)
         z.update_attribute(:updated_by, session[:user])
      end
      redirect_to databases_path
   end
   
   # this is intended to be run after maintenance deployments to update the
   # generated documentation for the application.
   def update_api_docs
      require_permission Perm[:update_database]
      require_method :post
      user_action("Database","Generated the API Documentation!")
      `rake doc:reapp`
      redirect_to databases_path
   end
   
   # upload images for site configuration
   def upload_image
      require_permission Perm[:admin]
      require_method :post
      params[:fname] or raise_missing("File Name")
      valid_names = ['logo','ceo_img', 'header_left_img','header_right_img']
      valid_names.include?(params[:fname]) or raise_invalid("File Name '#{params[:fname]}'", valid_names)
      post = UploadedFile.save_image(params[:fname], params[:datafile])
      post == false and raise_missing("Image Filename")
      SiteConfig.send("#{params[:fname]}=", "/system/#{params[:fname]}.png")
      # write the new data to file
      if conf = File.open("#{RAILS_ROOT}/public/system/site_configuration.yml","w")
         conf.puts(SiteConfig.marshal_dump.to_yaml)
         conf.close
      else
         flash[:warning] = 'WARNING! Could not save configuration data!'
      end
      redirect_to :action => 'show_config'
   end
   
   # This is posted to after a file upload and is responsible for saving
   # the file and redirecting to the appropriate file handler to process it.
   # it is used for uploading CSV files for legacy data import
   # TODO: remove this after fines/bonds and papers import
   def upload_file
      require_permission Perm[:update_database]
      require_method :post
      params[:type] or raise_missing "Type"
      valid_types = ['history']
      @type = params[:type]
      valid_types.include?(@type) or raise_invalid "Type '#{@type}'", valid_types
      if @type == 'history'
         post = UploadedFile.save(params[:histories])
         post == false and raise_invalid("Import Filename")
         if result = History.process_upload
            flash[:notice] = "Import Succeeded! Added #{result[0]} new records (skipped #{result[1]})."
            redirect_to histories_path
            return
         else
            flash[:warning] = "Import Failed!"
            redirect_to databases_path
            return
         end
      end
      flash[:warning] = "Unknown Option: #{@type}"
      redirect_to databases_path
   end
   
   # purges database tables related to history prior to import of new CSV file
   # TODO: remove this after fines/bonds import
   def purge_histories
      session[:user_login] == 'mfmerten' or raise_not_allowed "Purge History", ["FiveO Software Support"]
      require_method :post
      # this clears everything associated with histories and resets all of the tables
      # so that the IDs start with 1
      if RAILS_ENV == 'development'
         `mysql fiveo_development < #{RAILS_ROOT}/doc/other/clear_histories.sql`
      else
         `mysql fiveo < #{RAILS_ROOT}/doc/other/clear_histories.sql`
      end
      # done
      flash[:notice] = "System Purged for History Import"
      redirect_to databases_path and return
   end
end
