# == Schema Information
#
# Table name: call_subjects
#
#  id              :integer(4)      not null, primary key
#  call_id         :integer(4)
#  subject_type_id :integer(4)
#  name            :string(255)
#  address1        :string(255)
#  address2        :string(255)
#  home_phone      :string(255)
#  cell_phone      :string(255)
#  created_by      :integer(4)      default(1)
#  updated_by      :integer(4)      default(1)
#  created_at      :datetime
#  updated_at      :datetime
#  work_phone      :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class CallSubject < ActiveRecord::Base
   belongs_to :call
   belongs_to :subject_type, :class_name => 'Option', :foreign_key => 'subject_type_id'
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   
   before_validation :fix_name
   
   validates_presence_of :name
   validates_option :subject_type_id, :option_type => 'Subject Type'
   validates_phone :home_phone, :cell_phone, :work_phone, :allow_blank => true
   
   def self.desc
      'Subjects for Call Sheets'
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_call]
   end
   
   # returns true if the specified option id is used in any record for any
   # option type field.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(subject_type_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   private
   
   # normalizes the names
   def fix_name
      if name?
         self.name = unparse_name(parse_name(name), true)
      end
      return true
   end
end
