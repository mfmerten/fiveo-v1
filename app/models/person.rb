# == Schema Information
#
# Table name: people
#
#  id                         :integer(4)      not null, primary key
#  lastname                   :string(255)
#  firstname                  :string(255)
#  middlename                 :string(255)
#  suffix                     :string(255)
#  aka                        :string(255)
#  is_alias_for               :integer(4)
#  phys_street                :string(255)
#  phys_city                  :string(255)
#  phys_state_id              :integer(4)
#  phys_zip                   :string(255)
#  mail_street                :string(255)
#  mail_city                  :string(255)
#  mail_state_id              :integer(4)
#  mail_zip                   :string(255)
#  date_of_birth              :date
#  place_of_birth             :string(255)
#  race_id                    :integer(4)
#  sex                        :integer(4)
#  fbi                        :string(255)
#  sid                        :string(255)
#  ssn                        :string(255)
#  oln                        :string(255)
#  oln_state_id               :integer(4)
#  home_phone                 :string(255)
#  cell_phone                 :string(255)
#  email                      :string(255)
#  build_id                   :integer(4)
#  height_ft                  :integer(4)
#  height_in                  :integer(4)
#  weight                     :integer(4)
#  hair_color_id              :integer(4)
#  hair_type_id               :integer(4)
#  eye_color_id               :integer(4)
#  glasses                    :boolean(1)
#  shoe_size                  :string(255)
#  complexion_id              :integer(4)
#  facial_hair_id             :integer(4)
#  occupation                 :string(255)
#  employer                   :string(255)
#  emergency_contact          :string(255)
#  em_relationship_id         :integer(4)
#  emergency_address          :string(255)
#  emergency_phone            :string(255)
#  med_allergies              :string(255)
#  hepatitis                  :boolean(1)
#  hiv                        :boolean(1)
#  tb                         :boolean(1)
#  deceased                   :boolean(1)
#  dna_swab_date              :date
#  gang_affiliation           :string(255)
#  remarks                    :text
#  created_at                 :datetime
#  updated_at                 :datetime
#  doc_number                 :string(255)
#  created_by                 :integer(4)      default(1)
#  updated_by                 :integer(4)      default(1)
#  front_mugshot_file_name    :string(255)
#  side_mugshot_file_name     :string(255)
#  front_mugshot_content_type :string(255)
#  side_mugshot_content_type  :string(255)
#  front_mugshot_file_size    :integer(4)
#  side_mugshot_file_size     :integer(4)
#  front_mugshot_updated_at   :datetime
#  side_mugshot_updated_at    :datetime
#  merge_locked               :boolean(1)      default(FALSE)
#  legacy                     :boolean(1)      default(FALSE)
#  commissary_facility_id     :integer(4)
#  dna_blood_date             :date
#  dna_profile                :string(255)
#  fingerprint_class          :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Person Model
# The 'person of interest' records.
class Person < ActiveRecord::Base
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   has_many :marks, :dependent => :destroy
   has_many :associates, :dependent => :destroy
   belongs_to :phys_state, :class_name => "Option", :foreign_key => 'phys_state_id'
   belongs_to :mail_state, :class_name => "Option", :foreign_key => 'mail_state_id'
   belongs_to :race, :class_name => "Option", :foreign_key => 'race_id'
   belongs_to :oln_state, :class_name => "Option", :foreign_key => 'oln_state_id'
   belongs_to :build, :class_name => "Option", :foreign_key => 'build_id'
   belongs_to :hair_color, :class_name => "Option", :foreign_key => 'hair_color_id'
   belongs_to :hair_type, :class_name => "Option", :foreign_key => 'hair_type_id'
   belongs_to :eye_color, :class_name => "Option", :foreign_key => 'eye_color_id'
   belongs_to :complexion, :class_name => "Option", :foreign_key => 'complexion_id'
   belongs_to :facial_hair, :class_name => "Option", :foreign_key => 'facial_hair_id'
   belongs_to :em_relationship, :class_name => "Option", :foreign_key => 'em_relationship_id'
   has_many :arrests
   has_and_belongs_to_many :offenses_as_victim, :class_name => "Offense", :join_table => 'victims'
   has_and_belongs_to_many :offenses_as_suspect, :class_name => "Offense", :join_table => 'suspects'
   has_and_belongs_to_many :offenses_as_witness, :class_name => "Offense", :join_table => 'witnesses'
   has_many :forbids, :class_name => "Forbid", :foreign_key => "forbidden_id"
   has_many :warrants, :conditions => 'warrants.private != 1'
   has_many :bonds
   has_many :bonds_as_surety, :class_name => 'Bond', :foreign_key => 'surety_id'
   has_many :bookings, :include => :facility
   has_many :bookings_review, :class_name => 'Booking', :foreign_key => 'person_id', :include => :holds_review
   has_many :transports
   has_many :evidences, :foreign_key => 'owner_id', :conditions => 'evidences.private != 1'
   has_many :probations
   has_many :stolens, :conditions => 'stolens.private != 1' 
   has_many :dockets
   has_many :citations
   belongs_to :alias_for, :class_name => 'Person', :foreign_key => 'is_alias_for'
   has_many :aliases, :class_name => 'Person', :foreign_key => 'is_alias_for'
   has_and_belongs_to_many :warrant_exemptions, :class_name => "Warrant", :join_table => 'warrant_exemptions'
   has_and_belongs_to_many :forbid_exemptions, :class_name => 'Forbid', :join_table => 'forbid_exemptions'
   has_many :vehicles, :class_name => 'InvestVehicle', :foreign_key => 'owner_id', :conditions => 'invest_vehicles.private != 1'
   has_many :transfers
   has_many :docs
   has_many :medications, :dependent => :destroy
   has_many :commissaries
   belongs_to :commissary_facility, :class_name => 'Option', :foreign_key => 'commissary_facility_id'
   # these access possibly protected records - do not use these associations
   # they are included only for person merge purposes
   has_many :ivehicles, :class_name => 'InvestVehicle', :foreign_key => 'owner_id'
   has_many :icriminals, :class_name => 'InvestCriminal', :foreign_key => 'person_id'
   has_and_belongs_to_many :icrimes_as_witness, :class_name => 'InvestCrime', :join_table => 'crime_witnesses'
   has_and_belongs_to_many :icrimes_as_victim, :class_name => 'InvestCrime', :join_table => 'crime_victims'
   has_and_belongs_to_many :icrimes_as_source, :class_name => 'InvestCrime', :join_table => 'invest_crime_sources'
   has_and_belongs_to_many :iinvests_as_witness, :class_name => 'Investigation', :join_table => 'invest_witnesses'
   has_and_belongs_to_many :iinvests_as_victim, :class_name => 'Investigation', :join_table => 'invest_victims'
   has_and_belongs_to_many :iinvests_as_source, :class_name => 'Investigation', :join_table => 'invest_sources'
   has_and_belongs_to_many :iinvest_reports_as_source, :class_name => 'InvestReport', :join_table => 'invest_report_sources'
   # -----
   
   # used to allow entering full name on form but saving it as individual name
   # parts in the record.
   attr_accessor :name

   # this sets up sphinx indices for this model
   define_index do
      indexes [lastname, firstname, middlename, suffix], :as => :name
      indexes lastname
      indexes firstname
      indexes middlename
      indexes aka
      indexes fbi
      indexes sid
      indexes ssn
      indexes oln
      indexes email
      indexes occupation
      indexes employer
      indexes gang_affiliation
      indexes remarks
      indexes doc_number
      indexes phys_street
      indexes phys_city
      indexes mail_street
      indexes mail_city
      indexes dna_profile
      indexes fingerprint_class
      indexes marks.description, :as => :markdescs
      indexes associates.name, :as => :anames
      indexes associates.remarks, :as => :aremarks
      
      has updated_at
   end
   
   # this is the front view mugshot.
   has_attached_file :front_mugshot,
      :url => "/system/:attachment/:id/:style/:basename.:extension",
      :path => ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
      :default_url => "/images/missing_:style.jpg",
      :styles => {
         :thumb => "80x80",
         :original => "600x600",
         :medium => "300x300",
         :small => "150x150" }
   
   # this is the side view mugshot.
   has_attached_file :side_mugshot,
      :url => "/system/:attachment/:id/:style/:basename.:extension",
      :path => ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
      :default_url => "/images/missing_:style.jpg",
      :styles => {
         :thumb => "80x80",
         :original => "600x600",
         :medium => "300x300",
         :small => "150x150" }

   before_validation :split_name, :city_state_by_zip, :check_addresses, :check_alias
   after_update :save_marks, :save_associates
   after_save :check_creator
   before_destroy :check_and_destroy_dependencies
   
   validates_uniqueness_of :id, :allow_nil => true
   validates_presence_of :firstname, :lastname
   validates_zip :phys_zip, :mail_zip, :allow_blank => true, :allow_nil => true
   validates_phone :home_phone, :cell_phone, :allow_nil => true, :allow_blank => true
   validates_email :email, :allow_nil => true, :allow_blank => true
   validates_ssn :ssn, :allow_nil => true, :allow_blank => true
   validates_date_in_past :date_of_birth, :dna_swab_date, :dna_blood_date, :allow_nil => true
   validates_numericality_of :sex, :only_integer => true, :greater_than => 0, :less_than => 3, :allow_nil => true
   validates_presence_of :oln_state_id, :if => :oln?
   validates_presence_of :oln, :if => :oln_state_id?
   validates_numericality_of :id, :height_ft, :height_in, :weight, :only_integer => true, :allow_nil => true
   validates_person :is_alias_for, :allow_nil => true
   validates_option :phys_state_id, :mail_state_id, :oln_state_id, :option_type => "State", :allow_nil => true
   validates_option :race_id, :option_type => "Race", :allow_nil => true
   validates_option :build_id, :option_type => "Build", :allow_nil => true
   validates_option :hair_color_id, :option_type => "Hair Color", :allow_nil => true
   validates_option :hair_type_id, :option_type => "Hair Type", :allow_nil => true
   validates_option :eye_color_id, :option_type => "Eye Color", :allow_nil => true
   validates_option :complexion_id, :option_type => "Complexion", :allow_nil => true
   validates_option :facial_hair_id, :option_type => 'Facial Hair', :allow_nil => true
   validates_option :em_relationship_id, :option_type => 'Associate Type', :allow_nil => true

   # a short description used by the DatabaseController index view
   def self.desc
      "People of Interest"
   end
   
   # this attempts to locate a person record by full name. It will return
   # all records that match the supplied name (missing parts are skipped).
   def self.locate_by_name(name)
      names = parse_name(name)
      cond = ''
      unless names[:first].blank?
         cond << "firstname like '#{names[:first]}'"
      end
      unless names[:middle].blank?
         unless cond.empty?
            cond << " and "
         end
         cond << "middlename like '#{names[:middle]}'"
      end
      unless names[:suffix].blank?
         unless cond.empty?
            cond << " and "
         end
         cond << "suffix like '#{names[:suffix]}%'"
      end
      unless names[:last].blank?
         unless cond.empty?
            cond << " and "
         end
         cond << "lastname like '#{names[:last]}'"
      end
      if cond.empty?
         find(:all, :order => 'lastname, firstname, middlename, suffix')
      else
         find(:all, :order => 'lastname, firstname, middlename, suffix', :conditions => sanitize_sql_for_conditions(cond))
      end
   end
   
   # accepts a full name and returns the first record that matches exactly
   # all parts of the name
   def self.locate_first_by_name(name)
      names = parse_name(name)
      cond = ""
      if names[:first].blank?
         cond << "firstname is null"
      else
         cond << "firstname like '#{names[:first]}'"
      end
      cond << " and "
      if names[:middle].blank?
         cond << 'middlename is null'
      else
         cond << "middlename like '#{names[:middle]}'"
      end
      cond << " and "
      if names[:last].blank?
         cond << 'lastname is null'
      else
         cond << "lastname like '#{names[:last]}'"
      end
      cond << " and "
      if names[:suffix].blank?
         cond << 'suffix is null'
      else
         cond << "suffix like '#{names[:suffix]}%'"
      end
      find(:first, :order => 'lastname, firstname', :conditions => sanitize_sql_for_conditions(cond))
   end
   
   # returns all people with the specified commissary_facility_id
   def self.find_by_facility(facility_id)
      find(:all, :order => 'lastname, firstname, middlename, suffix', :conditions => ['commissary_facility_id = ?',facility_id])
   end
   
   # retrieves person record by id.  If person is an alias, returns true identity
   # instead of specified identity. Will follow alias chain until it reaches the
   # master identity record. Returns nil if record not found or person_id invalid.
   # accepts person id as integer or string. This is intended to replace the
   # standard find_by_id method when wanting to take aliases into account.
   def self.lookup(person_id)
      return nil unless person_id.is_a?(Integer) || person_id.is_a?(String)
      pid = person_id.to_i
      return nil unless pid > 0
      person_record = find_by_id(pid)
      unless person_record.nil?
         while !person_record.alias_for.nil?
            person_record = person_record.alias_for
         end
      end
      return person_record
   end

   # returns true if the specified option id is in use in any of the option
   # type fields for any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(phys_state_id = '#{oid}' OR mail_state_id = '#{oid}' OR race_id = '#{oid}' OR oln_state_id = '#{oid}' OR build_id = '#{oid}' OR hair_color_id = '#{oid}' OR hair_type_id = '#{oid}' OR eye_color_id = '#{oid}' OR complexion_id = '#{oid}' OR facial_hair_id = '#{oid}' OR em_relationship_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns base perms required to view people
   def self.base_perms
      Perm[:view_person]
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      # this is a special case for people.  The only field affected is the :is_alias_for
      # during the update we have to make sure that :is_alias_for is never set to the
      # same as the record ID or it breaks things.
      rec_count = update_all("is_alias_for = #{good_id.to_i}, updated_by = #{user_id.to_i}", "is_alias_for = #{bad_id.to_i}")
      # second update fixes the above-mentioned circular references
      update_all("is_alias_for = NULL","id = is_alias_for")
      return rec_count
   end
   
   # returns array element(s) of array containing true/false (person is in use) and text description of field
   def self.uses_person(person_id)
      # special case that looks for person records that are aliases for the
      # specified person and checks whether any of them have linked records
      # that would disallow deleting them
      # this is done because destroying a person also destroys their aliases
      person_id ||= 0
      person_id = person_id.to_i
      if this_person = find_by_id(person_id)
         this_person.aliases.each do |p|
            return true if p.cannot_be_deleted
         end
      end
      false
   end
   
   def medication_summary
      medications.collect{|m| "#{m.drug_name}#{m.dosage? ? '(' + m.dosage + ')' : ''}"}.join(', ')
   end
   
   # This calls the swap_person method in all applicable models with the
   # the two person ID numbers to be merged.  Each model is responsible
   # for swapping the person ID numbers in any of its records that uses it.
   def merge_person(master_id, user_id)
      return [-1,["Person"]] unless master_id.to_i > 0 && user_id.to_i > 0
      models = []
      Dir.chdir(File.join(RAILS_ROOT, "app/models")) do 
         models = Dir["*.rb"]
      end
      swapped_count = 0
      model_errors = []
      models.each do |m|
         class_name = m.sub(/\.rb$/,'').camelize
         if class_name.constantize.respond_to?('swap_person')
            recs = class_name.constantize.swap_person(id, master_id.to_i,user_id.to_i)
            if recs == -1
               model_errors.push(class_name)
            else
               swapped_count += recs
            end
         end
      end
      return [swapped_count, model_errors]
   end
   
   # returns true if this person has a pending or active probation. Ignores
   # completed and revoked probations.
   def on_probation?
      return false if probations.empty?
      probations.each do |p|
         return true if !p.status.nil? && p.status.long_name == 'Active'
         return true if !p.status.nil? && p.status.long_name == 'Pending'
      end
      return false
   end
   
   # accepts an array of new distinguishing mark attributes and builds mark
   # records for them using the marks association.
   def new_mark_attributes=(mark_attributes)
      mark_attributes.each do |attributes|
         unless attributes[:description].blank?
            marks.build(attributes)
         end
      end
   end

   # compares an array of distinguishing mark attributes with the current
   # marks association.  If a mark does not appear in the array, it is
   # destroyed, otherwise it is updated from the array.
   def existing_mark_attributes=(mark_attributes)
      marks.reject(&:new_record?).each do |mark|
         attributes = mark_attributes[mark.id.to_s]
         if attributes
            mark.attributes = attributes
         else
            marks.delete(mark)
         end
      end
   end
   
   # accepts an array of new known associates attributes and builds associate
   # records for them using the associates association.
   def new_associate_attributes=(associate_attributes)
      associate_attributes.each do |attributes|
         unless attributes[:name].blank?
            associates.build(attributes)
         end
      end
   end

   # compares an array of known associates attributes with the current
   # associates association.  If an associate does not appear in the array, 
   # it is destroyed, otherwise it is updated from the array.
   def existing_associate_attributes=(associate_attributes)
      associates.reject(&:new_record?).each do |assoc|
         attributes = associate_attributes[assoc.id.to_s]
         if attributes
            assoc.attributes = attributes
         else
            associates.delete(assoc)
         end
      end
   end
   
   # returns array of warrants the person is associated with.
   def wanted(include_linked = true)
      wars = []
      if include_linked
         wars.concat(warrants.reject(&:dispo_date?))
      end
      wars.concat(Warrant.warrants_for(id, false).reject{|w| warrant_exemptions.include?(w)})
      return wars.uniq
   end

   # returns array of forbids the person is associated with.
   def forbidden(include_linked = true)
      fors = []
      if include_linked
         fors.concat(forbids.reject(&:recall_date?))
      end
      fors.concat(Forbid.forbids_for(id, false).reject{|f| forbid_exemptions.include?(f)})
      return fors.uniq
   end

   # returns the person full name, optionally in last-name-first order.
   def full_name(reverse = true)
      nameparts = HashWithIndifferentAccess.new
      nameparts[:first] = firstname
      nameparts[:middle] = middlename
      nameparts[:last] = lastname
      nameparts[:suffix] = suffix
      unparse_name(nameparts, reverse)
   end

   # returns persons sex (stored as an integer) as a sex name string.
   def sex_name(short=false)
      if short
         return "M" if sex == 1
         return "F" if sex == 2
      else
         return "Male" if sex == 1
         return "Female" if sex == 2
      end
      return nil
   end

   # returns a text version of the persons description by parsing all of the
   # description fields and converting them to a string that can be naturally
   # spoken.
   def person_description
      d = ""
      unless race.nil?
         d << race.long_name
      end
      if sex?
         unless d.empty?
            d << " "
         end
         d << sex_name
      end
      unless deceased?
         # not going to calculate the age if known to be dead.
         unless self.age.nil?
            unless d.empty?
               d << ", "
            end
            d << "#{age} years old"
         end
      end
      if height_ft?
         unless d.empty?
            d << ", "
         end
         d << "#{height_ft} feet"
      end
      if height_in?
         unless d.empty?
            if height_ft?
               d << " "
            else
               d << ","
            end
         end
         d << "#{height_in} inches tall"
      else
         if height_ft?
            d << " tall"
         end
      end
      if weight?
         unless d.empty?
            d << ", "
         end
         d << "#{weight} pounds"
      end
      unless build.nil?
         unless d.empty?
            d << ", "
         end
         d << "#{build.long_name} build"
      end
      unless complexion.nil?
         unless d.empty?
            d << ", "
         end
         d << "#{complexion.long_name} complexion"
      end
      unless hair_type.nil?
         unless d.empty?
            d << ", "
         end
         d << "has #{hair_type.long_name}"
      end
      unless hair_color.nil?
         if d.empty?
            d << "Has "
         else
            unless hair_type.nil?
               d << " "
            else
               d << ", has "
            end
         end
         d << "#{hair_color.long_name} hair"
      else
         unless hair_type.nil?
            d << " hair"
         end
      end
      unless facial_hair.nil?
         if d.empty?
            d << "Has "
         else
            unless hair_type.nil? && hair_color.nil?
               d << " with "
            else
               d << ", has "
            end
         end
         d << facial_hair.long_name
      end
      unless eye_color.nil?
         if d.empty?
            d << "Has "
         else
            d << ", "
         end
         d << "#{eye_color.long_name} eyes"
      end
      if glasses?
         if d.empty?
            d << "Wears glasses."
         else
            d << ", wears glasses."
         end
      else
         unless d.empty?
            d << "."
         end
      end
      if shoe_size?
         unless d.empty?
            d << " "
         end
         d << "Wears size #{shoe_size} shoes."
      end
      return d
   end

   # returns the distinguishing marks as a string that can be naturally
   # spoken
   def marks_description
      d = ""
      marks.each do |m|
         unless d.empty?
            d << ", "
         end
         d << "#{m.description}#{m.location.nil? ? '' : ' on ' + m.location.long_name}"
      end
      return d
   end

   # calculates a persons age from the date of birth.  Will return nil if the
   # person is deceased, no date of birth given, or if the result is over
   # 100.
   def age
      return nil if deceased
      return nil unless date_of_birth?
      now = Date.today
      if (now.month < date_of_birth.month || now.month == date_of_birth.month) && (now.day < date_of_birth.day)
         theage = now.year - date_of_birth.year - 1
      else
         theage = now.year - date_of_birth.year
      end
      return (theage < 100) ? theage : nil
   end

   # returns the known associates as a string that can be naturally spoken.
   def associates_description
      d = ""
      associates.each do |a|
         unless d.empty?
            d << ", "
         end
         d << "#{a.name}#{a.relationship.nil? ? '' : ' (' + a.relationship.long_name + ')'}"
         if a.remarks?
            d << " [#{a.remarks}]"
         end
      end
      return d
   end

   # returns line one of the physical address (street)
   def physical_line1
      name = ""
      if phys_street?
         name << phys_street
      end
      return name
   end

   # returns line two of the physical address (city, ST ZIP)
   def physical_line2
      name = ""
      if phys_city
         name << phys_city
      end
      if !phys_state.nil?
         unless name.empty?
            name << ", "
         end
         name << phys_state.short_name
      end
      if phys_zip?
         unless name.empty?
            name << " "
         end
         name << phys_zip
      end
      return name
   end

   # returns line one of the mailing address (street)
   def mailing_line1
      name = ""
      if mail_street?
         name << mail_street
      else
         name << physical_line1
      end
      return name
   end

   # returns line two of the mailing address (city, ST ZIP)
   def mailing_line2
      name = ""
      if mail_city
         name << mail_city
      end
      if !mail_state.nil?
         unless name.empty?
            name << ", "
         end
         name << mail_state.short_name
      end
      if mail_zip?
         unless name.empty?
            name << " "
         end
         name << mail_zip
      end
      if name.empty?
         name = physical_line2
      end
      return name
   end
   
   # returns a string listing a persons phone numbers.
   def phones
      txt = ''
      if home_phone?
         txt << "Home: <b>#{home_phone}</b>"
      end
      if cell_phone?
         unless txt.empty?
            txt << "&nbsp;&nbsp;"
         end
         txt << "Cell: <b>#{cell_phone}</b>"
      end
      return txt
   end
   
   # returns mother name if mother associate exists.
   def mother
      return "" if associates.empty?
      associates.each do |a|
         if !a.relationship.nil? && a.relationship.long_name == "Mother"
            return a.name
         end
      end
      return ""
   end

   # returns father name if father associate exists.
   def father
      return "" if associates.empty?
      associates.each do |a|
         if !a.relationship.nil? && a.relationship.long_name == "Father"
            return a.name
         end
      end
      return ""
   end
   
   # checks to see if this person can be deleted, returns true/false
   def cannot_be_deleted
      models = []
      Dir.chdir(File.join(RAILS_ROOT, "app/models")) do 
         models = Dir["*.rb"]
      end
      models.each do |m|
         class_name = m.sub(/\.rb$/,'').camelize
         if class_name.constantize.respond_to?('uses_person')
            return true if class_name.constantize.uses_person(id)
         end
      end
      return false
   end
   
   # returns all commissary transactions for the selected period
   # defaults to month-to-date
   def commissary_transactions(start_date=Date.today.beginning_of_month,stop_date=Date.today)
      return [] unless start_date.is_a?(Date) && stop_date.is_a?(Date)
      if start_date > stop_date
         start_date,stop_date = stop_date,start_date
      end
      Commissary.find(:all, :order => 'commissaries.id', :conditions => ['person_id = ? and transaction_date >= ? and transaction_date <= ?', id, start_date, stop_date])
   end

   # returns all payment transactions for the past _days_ days.
   def past_days(days = 30)
      return [] unless days.is_a?(Integer) && days >= 0
      date = Date.today - days.days
      Commissary.find(:all, :conditions => ['person_id = ? and transaction_date >= ?',id,date])
   end
   
   # returns commissary balance at end of date specified
   def commissary_balance(edate = Date.today)
      return nil unless edate.is_a?(Date)
      if edate == Date.today
         Commissary.sum('deposit - withdraw', :conditions => ['person_id = ?', id]).to_d
      else
         Commissary.sum('deposit - withdraw', :conditions => ['person_id = ? and transaction_date <= ?', id, edate]).to_d
      end
   end
   
   # returns total probation fees due
   def probation_balance
      total = BigDecimal.new("0.0",2)
      probations.each do |p|
         total += p.total_balance
      end
      total
   end
   
   # this recursively checks for provided id number as an alias
   def is_alias_loop(person_id)
     return false if person_id.nil?
     person_id = person_id.to_i
     if is_alias_for?
       return true if is_alias_for == person_id
       unless alias_for.nil?
         return alias_for.is_alias_loop(person_id)
       end
     end
     false
   end

   private
   
   # prevents a person from being aliased to itself (circular alias)
   def check_alias
      if is_alias_for?
         if is_alias_for == id
            self.errors.add(:is_alias_for, 'cannot be the same as this Person ID number!')
            return false
         end
         # need to verify that target master does not resolve as an eventual
         # alias of this person creating a loop
         unless alias_for.nil?
           if alias_for.is_alias_loop(id)
             self.errors.add(:is_alias_for, 'cannot reference a Person that is an alias of this Person!')
             return false
           end
         end
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # ensures that when a person record is destroyed, all
   # offense associations as victim, suspect or witness are cleared as well
   # as forbid and warrant exemptions.
   # also, any person record that is flagged as an alias of this record
   # will be destroyed first.  Any failure in this will cause destroy of this
   # record to abort
   # TODO: add code to destroy aliases
   def check_and_destroy_dependencies
      return false if cannot_be_deleted
      self.forbid_exemptions.clear
      self.warrant_exemptions.clear
      return true
   end
   
   # ensures that associated distinguishing marks are saved when the record
   # is updated.
   def save_marks
      marks.each do |mark|
         mark.save(false)
      end
   end
   
   # ensures that associated known associates are saved when the record is
   # updated.
   def save_associates
      associates.each do |associate|
         associate.save(false)
      end
   end

   # looks up city and state by zip code and updates the record if needed.
   def city_state_by_zip
      if phys_zip?
         # if either city or state is blank, replace both by lookup
         unless phys_city? && phys_state_id?
            citystate = Zip.find_city_state_by_zip(phys_zip.slice(0,5))
            self.phys_city = citystate[0]
            self.phys_state_id = Option.lookup("State", citystate[1], 'short_name')
         end
      end
      if mail_zip?
         # if either city or state is blank, replace both by lookup
         unless mail_city? && mail_state_id?
            citystate = Zip.find_city_state_by_zip(mail_zip.slice(0,5))
            self.mail_city = citystate[0]
            self.mail_state_id = Option.lookup("State", citystate[1],'short_name')
         end
      end
   end

   # checks that physical and mailing address city and state match the zip
   # codes.
   def check_addresses
      retval = true
      if phys_city? && phys_state_id? && phys_zip?
         mycity = phys_city.split.each{|w| w.capitalize}.join(" ")
         mystate = phys_state.short_name
         myzip = phys_zip.slice(0,5)
         unless Zip.verify_zip(mycity,mystate,myzip)
            zips = Zip.find_all_by_city_and_state(mycity,mystate)
            self.errors.add(:base, "Error: Physical Address ZIP (#{myzip}) does not match #{mycity}, #{mystate}... valid ZIPs are #{zips}!")
            retval = false
         end
      end
      if mail_city? && mail_state_id? && mail_zip?
         mycity = mail_city.split.each{|w| w.capitalize}.join(" ")
         mystate = mail_state.short_name
         myzip = mail_zip.slice(0,5)
         unless Zip.verify_zip(mycity,mystate,myzip)
            zips = Zip.find_all_by_city_and_state(mycity,mystate)
            self.errors.add(:base, "Error: Physical Address ZIP (#{myzip}) does not match #{mycity}, #{mystate}... valid ZIPs are #{zips}!")
            retval = false
         end
      end
      return retval
   end    

   # converts the name attribute to the underlying firstname, middlename,
   # lastname and suffix parts.
   def split_name
      unless name.nil? || name.blank?
         nameparts = parse_name(name)
         self.firstname = nameparts[:first]
         self.middlename = nameparts[:middle]
         self.lastname = nameparts[:last]
         self.suffix = nameparts[:suffix]
      end
      # while we're at it, normalize the emergency contact, if present
      if emergency_contact?
         self.emergency_contact = unparse_name(parse_name(emergency_contact), true)
      end
      return true
   end

   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      unless query[:height_from_ft].nil?
         query[:height_from] = (query[:height_from_ft].to_i * 12).to_s
      end
      unless query[:height_from_in].nil?
         query[:height_from] = (query[:height_from].to_i + query[:height_from_in].to_i).to_s
      end
      unless query[:height_to_ft].nil?
         query[:height_to] = (query[:height_to_ft].to_i * 12).to_s
      end
      unless query[:height_to_in].nil?
         query[:height_to] = (query[:height_to].to_i + query[:height_to_in].to_i).to_s
      end
      numtypes = ["oln_state_id","race_id","sex","build_id","complexion_id", "hair_type_id","hair_color_id","facial_hair_id","eye_color_id", "glasses"]

      query.each do |key, value|
         if key == "id" || (key == 'simple' && value.to_i > 0)
            unless condition.empty?
               condition << " and "
            end
            condition << "people.id = '#{value}'"
         elsif key == 'name' || (key == 'simple' && value.to_i == 0)
            nameparts = parse_name(value)
            temp = ""
            unless nameparts[:first].blank?
               unless temp.blank?
                  temp << " and "
               end
               temp << "(people.firstname like '#{nameparts[:first]}' OR people.middlename like '#{nameparts[:first]}')"
            end
            unless nameparts[:middle].blank?
               unless temp.blank?
                  temp << " and "
               end
               temp << "(people.middlename like '#{nameparts[:middle]}' OR people.firstname like '#{nameparts[:middle]}')"
            end
            unless nameparts[:last].blank?
               unless temp.blank?
                  temp << " and "
               end
               temp << "people.lastname like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless temp.blank?
                  temp << " and "
               end
               temp << "people.suffix like '#{nameparts[:suffix]}'"
            end
            if value =~ /^[A-Za-z]+$/
               # one word name search, allow matching beginning of last name
               unless condition.blank?
                  condition << " and "
               end
               condition << "(people.lastname like '#{value}%' OR (#{temp}))"
            else
               condition << temp
            end
         elsif key == "street"
            unless condition.empty?
               condition << " and "
            end
            condition << "(people.phys_street like '%#{value}%' or people.mail_street like '%#{value}%')"
         elsif key == "city"
            unless condition.empty?
               condition << " and "
            end
            condition << "(people.phys_city like '%#{value}%' or people.mail_city  like '%#{value}%')"
         elsif key == "state"
            unless condition.empty?
               condition << " and "
            end
            condition << "(people.phys_state_id = '#{value}' or people.mail_state_id = '#{value}')"
         elsif key == "zip"
            unless condition.empty?
               condition << " and "
            end
            condition << "(people.phys_zip like '%#{value}%' or people.mail_zip like '%#{value}%')"
         elsif key == "dob_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "people.date_of_birth >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "dob_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "people.date_of_birth <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "height_from_ft" || key == "height_from_in" || key == "height_to_ft" || key == "height_to_in"
            # no-op - effectively skips these keys completely
            # they're used indirectly in the following two conditions
         elsif key == "height_from"
            unless condition.empty?
               condition << " and "
            end
            condition << "(people.height_ft * 12) + people.height_in >= '#{value}'"
         elsif key == "height_to"
            unless condition.empty?
               condition << " and "
            end
            condition << "(people.height_ft * 12) + people.height_in <= '#{value}'"
         elsif key == "weight_from"
            unless condition.empty?
               condition << " and "
            end
            condition << "people.weight >= '#{value}'"
         elsif key == "weight_to"
            unless condition.empty?
               condition << " and "
            end
            condition << "people.weight <= '#{value}'"
         else
            unless condition.empty?
               condition << " and "
            end
            if numtypes.include?(key)
               condition << "people.#{key} = '#{value}'"
            else
               condition << "people.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
