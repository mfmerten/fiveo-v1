# == Schema Information
#
# Table name: activities
#
#  id          :integer(4)      not null, primary key
#  user_id     :integer(4)
#  section     :string(255)
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Activity Model
# This is the user activity log. Records are retained for 90 days then
# deleted. Each act of writing a log entry will also purge any entries
# > 90 days old.
class Activity < ActiveRecord::Base
   belongs_to :user
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   
   # a short description for the DatabaseController index action.
   def self.desc
      "User Activity Log"
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:admin]
   end
   
   # removes all records > SiteConfig.activity_retention days old
   def self.purge_old_records
      unless SiteConfig.activity_retention.blank? || SiteConfig.activity_retention == 0
         extent = SiteConfig.activity_retention.days.ago
         delete_all(["created_at <= ?", extent])
      end
   end

   private

   # accepts a query hash and converts it to an SQL partial suitable in a where
   # clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ["user_id"]
      # build an sql WHERE string
      condition = ""
      query.each do |key, value|
         if key == "created_from"
            # convert date to y-m-d for mysql use or discard it if invalid
            if myval = valid_datetime(value, '00:00')
               # must be ok now
               unless condition.empty?
                  condition << " and "
               end
               condition << "activities.created_at >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "created_to"
            # convert date to y-m-d for mysql use or discard it if invalid
            if myval = valid_datetime(value, "23:59")
               # must be ok now
               unless condition.empty?
                  condition << " and "
               end
               condition << "activities.created_at <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else
            unless condition.empty?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "activities.#{key} = '#{value}'"
            else
               condition << "activities.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
