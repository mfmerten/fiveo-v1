# == Schema Information
#
# Table name: case_notes
#
#  id            :integer(4)      not null, primary key
#  case_no       :string(255)
#  note_datetime :datetime
#  note          :text
#  created_at    :datetime
#  updated_at    :datetime
#  created_by    :integer(4)      default(1)
#  updated_by    :integer(4)      default(1)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Case Note Model
# These are notes entered for particular cases by officers
class CaseNote < ActiveRecord::Base
    belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
    belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
    
    # this sets up sphinx indices for this model
    define_index do
       indexes case_no
       indexes note_datetime
       indexes note
       indexes created_by
       indexes updated_by

       has updated_at
    end
    
    before_validation :adjust_case_no
    after_save :check_creator
    
    validates_presence_of :note, :note_datetime, :case_no
    
    # a short description of the model for use by the DatabaseController index
    # view.
    def self.desc
       "Case Notes"
    end
    
    # returns base permissions needed to View records
    def self.base_perms
       Perm[:view_case]
    end
    
    private
    
    # strips all characters from case_no except for numbers, letters, and -
    def adjust_case_no
       self.case_no = fix_case(case_no)
       return true
    end
    
    # checks that if created_by or updated_by are nil, 0 or non-integer after
    # the record is saved, they get set to the default value of 1 (admin).
    def check_creator
       if !created_by? || creator.nil?
          self.update_attribute(:created_by, 1)
       end
       if !updated_by? || updater.nil?
          self.update_attribute(:updated_by, 1)
       end
       return true
    end
    
    # accepts a query hash and turns it into a partial SQL query string
    def self.condition_string(fullquery)
       return nil unless query = quoted(fullquery)
       intfields = ["state_id"]
       condition = ""
       query.each do |key, value|
          if key == "note_datetime_from"
             if myval = valid_date(value)
                unless condition.empty?
                   condition << " and "
                end
                condition << "case_notes.note_datetime >= '#{myval} 00:00'"
             else
                fullquery.delete(key)
             end
          elsif key == "note_datetime_to"
             if myval = valid_date(value)
                unless condition.empty?
                   condition << " and "
                end
                condition << "case_notes.note_datetime <= '#{myval} 23:59'"
             else
                fullquery.delete(key)
             end
          else
             unless condition.empty?
                condition << " and "
             end
             if intfields.include?(key)
                condition << "case_notes.#{key} = '#{value}'"
             else
                condition << "case_notes.#{key} like '%#{value}%'"
             end
          end
       end
       return sanitize_sql_for_conditions(condition)
    end
end
