# == Schema Information
#
# Table name: helps
#
#  id         :integer(4)      not null, primary key
#  section    :string(255)
#  page       :string(255)
#  title      :string(255)
#  body       :text
#  created_at :datetime
#  updated_at :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Help Model
# Main help screens for FiveO application.
class Help < ActiveRecord::Base
   validates_presence_of :title, :body, :section, :page
   
   # a short description used by the DatabaseController index action
   def self.desc
      "Application Help Screens"
   end
   
   private

   # accepts a query hash and turns it into a partial SQL query string
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      query.each do |key, value|
         if key == 'text'
            unless condition.empty?
               condition << " and "
            end
            condition << "(helps.title like '%#{value}%' or helps.body like '%#{value}%')"
         else
            unless condition.empty?
               condition << " and "
            end
            condition << "helps.#{key} like '%#{value}%'"
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
