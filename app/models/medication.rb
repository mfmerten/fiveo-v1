# == Schema Information
#
# Table name: medications
#
#  id              :integer(4)      not null, primary key
#  person_id       :integer(4)
#  prescription_no :integer(4)
#  physician_id    :integer(4)
#  pharmacy_id     :integer(4)
#  drug_name       :string(255)
#  dosage          :string(255)
#  warnings        :string(255)
#  created_by      :integer(4)      default(1)
#  updated_by      :integer(4)      default(1)
#  created_at      :datetime
#  updated_at      :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class Medication < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :physician, :class_name => 'Contact', :foreign_key => 'physician_id'
   belongs_to :pharmacy, :class_name => 'Contact', :foreign_key => 'pharmacy_id'
   belongs_to :person
   has_many :med_schedules, :dependent => :destroy
   
   after_save :save_meds, :check_creator
   
   validates_presence_of :drug_name, :dosage
   validates_person :person_id
   validates_contact :physician_id, :pharmacy_id, :allow_nil => true
   
   def self.desc
      'Inmate Medications'
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_person]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["physician_id", "pharmacy_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      # never return true because these are dependents of Person
      false
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def dose_schedule
      med_schedules.collect{|m| "#{m.time}: #{m.dose}"}.join(", ")
   end
   
   def new_schedule_attributes=(schedule_attributes)
      schedule_attributes.each do |attributes|
         unless attributes[:dose].blank?
            med_schedules.build(attributes)
         end
      end
   end

   def existing_schedule_attributes=(schedule_attributes)
      med_schedules.reject(&:new_record?).each do |med|
         attributes = schedule_attributes[med.id.to_s]
         if attributes
            med.attributes = attributes
         else
            med_schedules.delete(med)
         end
      end
   end
   
   private
   
   def save_meds
      med_schedules.each{|m| m.save(false)}
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
end
