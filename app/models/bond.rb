# == Schema Information
#
# Table name: bonds
#
#  id           :integer(4)      not null, primary key
#  bondsman_id  :integer(4)
#  surety_id    :integer(4)
#  person_id    :integer(4)
#  arrest_id    :integer(4)
#  bond_amt     :decimal(12, 2)  default(0.0)
#  issued_date  :date
#  issued_time  :string(255)
#  bond_type_id :integer(4)
#  remarks      :text
#  power        :string(255)
#  status_id    :integer(4)
#  status_date  :date
#  status_time  :string(255)
#  bond_no      :string(255)
#  created_by   :integer(4)
#  updated_by   :integer(4)
#  created_at   :datetime
#  updated_at   :datetime
#  hold_id      :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Bond Model
# Bonds are used to release (satisfy) bondable booking holds to allow a 
# prisoner to leave the jail facility without an escort.
class Bond < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :bondsman
   belongs_to :surety, :class_name => 'Person', :foreign_key => 'surety_id'
   belongs_to :arrest
   belongs_to :person
   belongs_to :bond_type, :class_name => 'Option', :foreign_key => 'bond_type_id'
   belongs_to :status, :class_name => 'Option', :foreign_key => 'status_id'
   belongs_to :hold
   
   before_validation :fix_times, :check_surety
   before_update :fix_hold
   after_save :check_bond_no, :check_bondsman, :process_hold, :check_creator
   before_destroy :fix_hold
   
   validates_numericality_of :bond_amt
   validates_date :issued_date
   validates_time :issued_time
   validates_date :status_date, :allow_nil => true
   validates_time :status_time, :allow_nil => true
   validates_presence_of :status_time, :if => :status_date?
   validates_presence_of :status_date, :if => :status_time?
   validates_option :status_id, :option_type => 'Bond Status'
   validates_option :bond_type_id, :option_type => 'Bond Type'
   validates_person :person_id
   validates_person :surety_id, :allow_nil => true
   validates_arrest :arrest_id, :allow_nil => true

   # a short description of the model used by the DatabaseController index
   # view.
   def self.desc
      'Bond Database'
   end
   
   # returns true if any bond record uses the specified option id in any of
   # the option type fields.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(bond_type_id = '#{oid}' OR status_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}' OR surety_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   # returns base permissions needed to View these objects
   def self.base_perms
      Perm[:view_bond]
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id","surety_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def surety_name
      if !bondsman.nil?
         "#{bondsman.company} (#{bondsman.name})"
      elsif !surety.nil?
         "#{surety.full_name}#{bond_type.nil? ? '' : ' (' + bond_type.long_name + ')'}"
      else
         "Unknown"
      end
   end
   
   # handles bond revocation, surrender and forfeiture
   def recall(status='revoke')
      retval = nil
      return "Invalid Person" if person.nil?
      
      # cannot surrender a bond without the person being booked first
      booking = person.bookings.last
      return "No Open Booking For Person" if (booking.nil? || booking.release_date?) && status == 'surrender'
      
      # look up status options
      revoke = Option.lookup("Bond Status","Revoked",nil,nil,true)
      surrender = Option.lookup("Bond Status","Surrendered",nil,nil,true)
      forfeit = Option.lookup("Bond Status","Forfeited",nil,nil,true)
      final = Option.lookup("Bond Status","Final",nil,nil,true)
      
      # check the argument
      return "Invalid Recall Reason: #{status}" unless ['revoke','surrender','forfeit','final'].include?(status)
      
      # if revoke/forfeit and booking closed, or always for final, skip booking processing
      unless ((booking.nil? || booking.release_date?) && ['revoke','forfeit'].include?(status)) || status == 'final'
         # revoke/forfeit/surrender and booking is open... process booking
         
         # check that this bond has a valid hold linked to it
         return "Invalid Or Unknown Type Hold" if hold.nil? || hold.hold_type.nil?

         # we only handle bonds for district charges (bondable holds)
         return "Not For District Charges" unless hold.hold_type.long_name == 'Bondable'

         # all bonds are for arrest holds... arrest must be present
         return "No Valid Arrest Linked To Bond Or Hold" if arrest.nil? && hold.arrest.nil?
         if arrest.nil?
            arr = hold.arrest
         else
            arr = arrest
         end
         
         # has the booking changed?
         if booking == hold.booking
            # same booking - reprocess existing bondable hold
            if hold.cleared_date?
               self.hold.clear_release(updated_by)
            end
         else
            # leave the original bondable hold alone and add a new 72
            Hold.add(booking.id, arr.id, '72 Hour')
            # clear the 72 hour information
            arr.hour_72_judge_id = nil
            arr.hour_72_date = nil
            arr.hour_72_time = nil
            arr.attorney = nil
            arr.condition_of_bond = nil
            arr.bond_amt = 0
            arr.bondable = false
            arr.save(false)
            # unlink all bonds still pointing to the original hold
            # they will be picked up on the new bondable hold when 72 is entered
            self.hold.bonds.clear
         end
      end
      # process status for this bond
      case status
      when 'revoke'
         self.status_id = revoke
      when 'surrender'
         self.status_id = surrender
      when 'forfeit'
         self.status_id = forfeit
      when 'final'
         self.status_id = final
      end
      self.status_date, self.status_time = get_date_and_time
      self.save(false)
      return nil
   end
	   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # checks that there is either a valid surety or a valid bondsman
   def check_surety
      retval = true
      if bondsman_id? && bondsman.nil?
         self.errors.add(:bondsman_id, "must be a current valid Bondsman ID or blank!")
         retval = false
      else
         if surety_id? && bondsman_id?
            self.errors.add_to_base("Only one of the Bondsman or the Surety must be specifed, not both!")
            retval = false
         elsif !surety_id? && !bondsman_id?
            self.errors.add_to_base("Either the Bondsman or the Surety must be specified!")
            retval = false
         end
      end
      return retval
   end

   # this updates the bond_no field with the record number unless one is
   # already assigned.
   def check_bond_no
      unless bond_no?
         self.update_attribute(:bond_no, id.to_s.rjust(6,'0'))
      end
   end
   
   # checks to see if a bondsman is linked as surety... if so, bond type of
   # Professional is forced.
   def check_bondsman
      unless bondsman.nil?
         if o = get_option('Bond Type', 'Professional')
            unless bond_type_id == o
               self.update_attribute(:bond_type_id, o)
            end
         end
      end
   end
   
   # if the "bondable" hold has cleared, this will un_clear it
   # prior to updating the bond fields if the bond status is Active.
   # upon update, the hold will be cleared if the resulting sum of
   # all bonds attached to it >= the hold.costs field.
   def fix_hold
      return true unless status.long_name == "Active"
      unless hold.nil?
         # cant do it if the booking itself is already cleared
         unless hold.booking.release_date?
            # see if hold released by bonding
            if hold.cleared_date? && hold.cleared_because.long_name == "Bonded"
               # clear the hold release
               self.hold.clear_release(updated_by)
            end
         end
      end
      return true
   end

   # if all of the bonds for the associated bondable hold total enough
   # to release the hold, this updates the hold to released status.
   # only done if the bond is active.
   def process_hold
      return true unless status(true).long_name == "Active"
      unless hold(true).nil?
         # make sure its not already released
         unless hold.cleared_date?
            # cant do it if the booking itself is already cleared
            unless (!hold.booking.nil? && hold.booking.release_date?)
               if hold.hold_type.long_name =~ /^Probation/
                 # bonding on a probation hold, just clear the hold
                 self.hold.cleared_because_id = get_option("Release Type","Bonded")
                 self.hold.cleared_date = issued_date
                 self.hold.cleared_time = issued_time
                 self.hold.cleared_by = updater.name
                 self.hold.cleared_by_unit = updater.unit
                 self.hold.cleared_by_badge = updater.badge
                 self.hold.updated_by = updated_by
                 self.hold.explanation = "Automatic Clear by Judge Ordered Bond"
                 self.hold.save(false)
               else
                 # bonding on a Bondable hold, make sure total is satisfied before
                 # clearing the hold.
                 bonded = hold.fines
                 active_status = get_option("Bond Status","Active")
                 hold.bonds(true).reject{|b| b.status_id != active_status}.each do |b|
                   bonded = bonded - b.bond_amt
                 end
                 if bonded <= BigDecimal.new("0",2)
                   # bonds entered for this hold are sufficient
                   self.hold.cleared_because_id = get_option("Release Type","Bonded")
                   self.hold.cleared_date = issued_date
                   self.hold.cleared_time = issued_time
                   self.hold.cleared_by = updater.name
                   self.hold.cleared_by_unit = updater.unit
                   self.hold.cleared_by_badge = updater.badge
                   self.hold.updated_by = updated_by
                   self.hold.explanation = "Automatic Clear by Bond Amount Satisfied"
                   self.hold.save(false)
                 end
               end
            end
         end
      end
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      numtypes = ["person_id","bondsman_id","surety_id","arrest_id","bond_type_id", "status_id"]
      query.each do |key, value|
         if key == "issued_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bonds.issued_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "issued_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bonds.issued_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else
            unless condition.empty?
               condition << " and "
            end
            if numtypes.include?(key)
               condition << "bonds.#{key} = '#{value}'"
            else
               condition << "bonds.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
   
   # this is a helper that calls the valid_time() method with each time
   # field for a bond to insert any missing colons (:) into the time values
   def fix_times
      self.issued_time = valid_time(issued_time)
      self.status_time = valid_time(status_time)
   end
end
