# == Schema Information
#
# Table name: wreckers
#
#  id         :integer(4)      not null, primary key
#  offense_id :integer(4)
#  name       :string(255)
#  by_request :boolean(1)      default(FALSE)
#  location   :string(255)
#  vin        :string(255)
#  tag        :string(255)
#  created_by :integer(4)      default(1)
#  updated_by :integer(4)      default(1)
#  created_at :datetime
#  updated_at :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class Wrecker < ActiveRecord::Base
   belongs_to :offense
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   
   validates_presence_of :name
   
   def self.desc
      'Wreckers for Offenses'
   end
   
   private
end
