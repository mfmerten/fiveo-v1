# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# provides a mechanism for updating default and site configuration files
class SiteConfiguration < ActiveRecord::Base
   include Tableless

   column :agency, :string, "FiveO"  # full agency name
   column :agency_short, :string, "FiveO"   # abbreviated agency name or acronym
   column :motto, :string, "Law Enforcement Software"   # agency "motto"
   column :logo, :string, "/images/fiveo_original.png"   # server root path to logo image
   column :ceo, :string, "Michael Merten"   # chief executive officer name
   column :ceo_title, :string, "Midlake Technical Services"   # chief executive officer title
   column :ceo_img, :string, "/images/ceo_original.png"   # server root path to ceo photo file
   column :district, :integer, 0   # district number (0 disabled, otherwise highest district number allowed
   column :zone, :integer, 0   # zone number (0 disabled, otherwise highest zone number allowed
   column :ward, :integer, 0   # ward number (0 disabled, otherwise highest ward number allowed
   column :local_zips, :string, nil   # comma separated list of zip codes considered local for this agency
   column :local_agencies, :string, nil   # comma separated list of Contact IDs for agencies using this agency as their jail
   column :show_history, :boolean, false   # Whether to show the History (legacy data) section
   column :bug_admin_id, :integer, 1   # User ID for the person with Site Admin permissions that will admin the bug report system
   column :site_email, :string, "monitor@midlaketech.com"   # sender email for bug report (offsite) messages
   column :message_retention, :integer, 90   # number of days to retain copies of user messages (for review)
   column :activity_retention, :integer, 90   # number of days to retain entries in activity log
   column :header_title, :string, "FiveO"   # agency name for title line on letterhead
   column :header_subtitle1, :string, "Midlake Technical Services"   # second line on letterhead
   column :header_subtitle2, :string, "Michael Merten, Owner"   # third line on letterhead
   column :header_subtitle3, :string, "http://midlaketech.com - mike@midlaketech.com"  # fourth line on letterhead
   column :header_left_img, :string, "/images/header_original.png"   # server root path for left letterhead image
   column :header_right_img, :string, "/images/header_original.png"   # server root path for right letterhead image
   column :footer_text, :string, "Law Enforcement Software"   # letterhead footer text (center)
   column :pdf_header_bg, :string, 'ddddff'   # background color for PDF header objects
   column :pdf_header_txt, :string, '000099'   # text color for PDF header objects
   column :pdf_section_bg, :string, 'ddddff'   # background color for PDF section objects
   column :pdf_section_txt, :string, '000099'   # text color for PDF section objects
   column :pdf_em_bg, :string, 'ffdddd'   # background color for PDF emergency objects
   column :pdf_em_txt, :string, '990000'   # text color for PDF emergency objects
   column :pdf_row_odd, :string, 'ffffff'   # PDF table odd row background color
   column :pdf_row_even, :string, 'eeeeee'   # PDF table even row background color
   column :session_life, :integer, 720   # standard session life in minutes
   column :session_active, :integer, 15   # timeout for inactive sessions in minutes
   column :session_max, :integer, 960   # maximum allowed session life in minutes
   column :allow_user_photos, :boolean, true   # whether users may submit their own user photo
   column :user_update_contact, :boolean, true   # whether users are allowed to update their own contact (profile)
   column :dispatch_shift1_start, :string, '05:30'   # start time for dispatcher shift 1
   column :dispatch_shift1_stop, :string, '17:30'   # stop time for dispatcher shift 1
   column :dispatch_shift2_start, :string, '17:30'   # start time for dispatcher shift 2
   column :dispatch_shift2_stop, :string, '05:30'   # stop time for dispatcher shift 2
   column :dispatch_shift3_start, :string, nil   # start time for dispatcher shift 3
   column :dispatch_shift3_stop, :string, nil   # stop time for dispatcher shift 3
   column :goodtime_by_default, :boolean, true
   column :goodtime_hours_per_day_local, :integer, 24
   column :jail1_enabled, :boolean, false   # enable jail 1
   column :jail1_name, :string, nil   # full name of jail facility
   column :jail1_short_name, :string, nil   # short name (label) of jail facility
   column :jail1_male, :boolean, false   # allow males in jail1
   column :jail1_female, :boolean, false   # allow females in jail1
   column :jail1_gt_by_default, :boolean, false  # allow goodtime by default for jail1
   column :jail1_gt_hours_per_day, :integer, 0  # number of hours goodtime awarded per day served
   column :jail2_enabled, :boolean, false   # enable jail 2
   column :jail2_name, :string, nil   # full name of jail facility
   column :jail2_short_name, :string, nil   # short name (label) of jail facility
   column :jail2_male, :boolean, false   # allow males in jail2
   column :jail2_female, :boolean, false   # allow females in jail2
   column :jail2_gt_by_default, :boolean, false  # allow goodtime by default for jail2
   column :jail2_gt_hours_per_day, :integer, 0  # number of hours goodtime awarded per day served
   column :jail3_enabled, :boolean, false   # enable jail 3
   column :jail3_name, :string, nil   # full name of jail facility
   column :jail3_short_name, :string, nil   # short name (label) of jail facility
   column :jail3_male, :boolean, false   # allow males in jail3
   column :jail3_female, :boolean, false   # allow females in jail3
   column :jail3_gt_by_default, :boolean, false  # allow goodtime by default for jail3
   column :jail3_gt_hours_per_day, :integer, 0  # number of hours goodtime awarded per day served
   column :probation_fund1, :string, "Fines"
   column :probation_fund2, :string, "Costs"
   column :probation_fund3, :string, "Probation Fees"
   column :probation_fund4, :string, "IDF"
   column :probation_fund5, :string, "DARE"
   column :probation_fund6, :string, "Restitution"
   column :probation_fund7, :string, "DA Fees"
   column :probation_fund8, :string, nil
   column :probation_fund9, :string, nil
   # Paper Service Configuration
   column :mileage_fee, :float, 0.88   # default mileage fee charged for paper service
   column :noservice_fee, :float, 5.00   # default no-service (unable to locate) fee for paper service
   column :paper_pay_type1, :string, "Cash"
   column :paper_pay_type2, :string, "Check"
   column :paper_pay_type3, :string, "Money Order"
   column :paper_pay_type4, :string, "Credit Card"
   column :paper_pay_type5, :string, nil
   # Added for colorscheme select
   column :colorscheme, :integer, 1  # default colorscheme
   
   ######################################################################
   # callbacks
   ######################################################################
   before_validation :fix_colors, :fix_times, :missing_to_default

   ######################################################################
   # validations
   ######################################################################
   validates_presence_of :agency, :agency_short, :motto, :logo, :ceo, :ceo_title,
      :header_title, :header_subtitle1, :footer_text, :site_email
   validates_numericality_of :district, :ward, :zone, :session_life, :session_active, :session_max,
      :bug_admin_id, :message_retention, :activity_retention, :goodtime_hours_per_day_local, :colorscheme,
      :only_integer => true, :greater_than_or_equal_to => 0
   validates_user :bug_admin_id
   validates_format_of :pdf_header_bg, :pdf_header_txt, :pdf_section_bg, :pdf_section_txt,
      :pdf_em_bg, :pdf_em_txt, :pdf_row_odd, :pdf_row_even,
      :with => /\A[a-fA-F0-9]{6}\Z/
   validates_numericality_of :mileage_fee, :noservice_fee, :greater_than_or_equal_to => 0
   validates_time :dispatch_shift1_start, :dispatch_shift1_stop, :dispatch_shift2_start,
      :dispatch_shift2_stop, :dispatch_shift3_start, :dispatch_shift3_stop,
      :allow_nil => true

   ######################################################################
   # methods
   ######################################################################

   # called to load the site configuration into the instance
   # only program defaults exist prior to this
   def load_site_file
      conf_file = "#{RAILS_ROOT}/public/system/site_configuration.yml"
      if File.exists?(conf_file)
         conf = YAML.load_file(conf_file)
         # remove any attributes that no longer exist
         ckeys = conf.keys
         if ckeys[0].is_a?(Symbol)
           obsolete = ckeys - self.attribute_names.collect{|a| a.to_sym}
         else
           obsolete = ckeys - self.attribute_names
         end
         obsolete.each do |key|
            conf.delete(key)
         end
         self.attributes = conf
         return true
      end
      false
   end
   
   # called to write the instance data to the site file
   def write_site_file
      if conf = File.new("#{RAILS_ROOT}/public/system/site_configuration.yml","w")
         conf.puts(self.attributes.to_yaml)
         conf.close
         
         # also need to update SiteConfig
         self.attributes.keys.each do |key|
            # must do it one field at a time because updating with a hash
            # only updates OpenStruct variables, not its methods which is
            # a glaring deficiency in my opinion
            SiteConfig.send("#{key}=", self.send(key))
         end
         
         return true
      end
      false
   end
   
   # called to reset the object back to defaults (requires defaults file exists)
   def load_default_file
      conf_file = "#{RAILS_ROOT}/config/defaults.yml"
      if File.exists?(conf_file)
         conf = YAML.load_file(conf_file)
         # remove any attributes that no longer exist
         ckeys = conf.keys
         if ckeys[0].is_a?(Symbol)
           obsolete = ckeys - self.attribute_names.collect{|a| a.to_sym}
         else
           obsolete = ckeys - self.attribute_names
         end
         obsolete.each do |key|
            conf.delete(key)
         end
         self.attributes = conf
         return true
      end
      false
   end
   
   # called to write the data to the **PROGRAM DEFAULT FILE**
   # WARNING: NEVER CALL THIS FROM ANYWHERE in the program.  This is included
   # as a utility so I do not have to maintain defaults both here AND in defaults
   # file, which is used during initialization to load SiteConfDefault object.
   # It is intended only for use from the console during development updates
   # to the program defaults
   def write_default_file
      if outfile = File.new("#{RAILS_ROOT}/config/defaults.yml", 'w')
         outfile.puts(self.attributes.to_yaml)
         outfile.close
         return true
      end
      false
   end
   
   private
   
   # removes any characters that are not hex digits
   def fix_colors
      ['pdf_header_bg', 'pdf_header_txt', 'pdf_section_bg', 'pdf_section_txt','pdf_em_bg','pdf_em_txt','pdf_row_odd','pdf_row_even'].each do |field|
         self.send("#{field}=", self.send(field).gsub(/[^a-fA-F0-9]/,'')) unless self.send(field).nil?
      end
      true
   end
   
   # some fields automatically get default values when nil
   def missing_to_default
      # pdf colors
      ['pdf_header_bg', 'pdf_header_txt', 'pdf_section_bg', 'pdf_section_txt','pdf_em_bg','pdf_em_txt','pdf_row_odd','pdf_row_even'].each do |field|
         self.send("#{field}=", SiteConfigDefault.send(field)) if self.send(field).nil?
      end
      true
   end
   
   # uses valid_time method to fix time values entered without :
   def fix_times
      ['dispatch_shift1_start','dispatch_shift1_stop','dispatch_shift2_start','dispatch_shift2_stop','dispatch_shift3_start','dispatch_shift3_stop'].each do |field|
         self.send("#{field}=", valid_time(self.send(field)))
      end
      true
   end
end
