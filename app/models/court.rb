# == Schema Information
#
# Table name: courts
#
#  id                     :integer(4)      not null, primary key
#  docket_id              :integer(4)
#  judge_id               :integer(4)
#  court_date             :date
#  court_type_id          :integer(4)
#  next_court_date        :date
#  next_court_type_id     :integer(4)
#  next_judge_id          :integer(4)
#  bench_warrant_issued   :boolean(1)      default(FALSE)
#  bond_forfeiture_issued :boolean(1)      default(FALSE)
#  is_upset_wo_refix      :boolean(1)      default(FALSE)
#  is_refixed             :boolean(1)      default(FALSE)
#  refix_reason           :string(255)
#  plea_type_id           :integer(4)
#  is_dismissed           :boolean(1)      default(FALSE)
#  is_overturned          :boolean(1)      default(FALSE)
#  overturned_date        :date
#  created_by             :integer(4)      default(1)
#  updated_by             :integer(4)      default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  psi                    :boolean(1)      default(FALSE)
#  needs_state_report     :boolean(1)      default(FALSE)
#  processed              :boolean(1)      default(FALSE)
#  docket_type_id         :integer(4)
#  next_docket_type_id    :integer(4)
#  fines_paid             :boolean(1)      default(FALSE)
#  notes                  :text
#  remarks                :text
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Court Model
# this tracks actual court appearances for dockets
class Court < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :docket, :include => :person
   belongs_to :judge, :class_name => 'Option', :foreign_key => 'judge_id'
   belongs_to :court_type, :class_name => 'Option', :foreign_key => 'court_type_id'
   belongs_to :next_court_type, :class_name => 'Option', :foreign_key => 'next_court_type_id'
   belongs_to :next_judge, :class_name => 'Option', :foreign_key => 'next_judge_id'
   belongs_to :plea, :class_name => 'Option', :foreign_key => 'plea_type_id'
   has_many :sentences, :dependent => :destroy
   belongs_to :docket_type, :class_name => 'Option', :foreign_key => 'docket_type_id'
   belongs_to :next_docket_type, :class_name => 'Option', :foreign_key => 'next_docket_type_id'
   
   after_save :check_creator, :generate_next_court
   
   validates_date :court_date
   validates_date :next_court_date, :overturned_date, :allow_nil => true
   validates_option :court_type_id, :option_type => 'Court Type'
   validates_option :judge_id, :option_type => 'Judge'
   validates_option :next_court_type_id, :option_type => 'Court Type', :allow_nil => true
   validates_option :next_judge_id, :option_type => 'Judge', :allow_nil => true
   validates_option :docket_type_id, :option_type => 'Docket Type'
   validates_option :next_docket_type_id, :option_type => 'Docket Type', :allow_nil => true
   validates_docket :docket_id
   
   # returns a short description for the DatabaseController index view
   def self.desc
      'Court Appearance Table'
   end
   
   # returns true if the specified option id is in use in any of the option
   # type fields in any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(judge_id = '#{oid}' or court_type_id = '#{oid}' or next_judge_id = '#{oid}' or next_court_type_id = '#{oid}' or plea_type_id = '#{oid}' or docket_type_id = '#{oid}' or next_docket_type_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns base permissions required to View records
   def self.base_perms
      Perm[:view_court]
   end
   
   # returns a status string for this court
   def status
      txt = ""
      if is_refixed?
         txt << "[Refixed] "
      elsif is_upset_wo_refix?
         txt << "[Upset W/O Refix] "
      elsif is_dismissed?
         txt << "[Dismissed] "
      elsif !plea.nil?
         txt << "[Pled #{plea.long_name}] "
      elsif is_overturned?
         txt << "[Overturned] "
      end
      if bench_warrant_issued?
         txt << "[BW] "
      end
      if bond_forfeiture_issued?
         txt << "[Bond Forfeiture]"
      end
      if psi?
         txt << "[Pre-Sent. Inv.]"
      end
      txt
   end
   
   # processes all sentences for this court then checks to see if all
   # docket charges are satisfied and updates itself to processed.
   def process_court
      return true if processed?
      
      # process any pending sentences
      sentences.reject(&:processed?).each{|s| s.save(false)}
      
      # check if anything is remaining
      if sentences.reject(&:processed?).empty? && (docket.nil? || docket.da_charges.reject{|c| !c.sentence.nil? || c.dropped_by_da? || c.info_only?}.empty?)
         self.update_attribute(:processed, true)
      end
   end
   
   private
   
   # checks that any docket id entered is valid
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # generates the next court record if enough information is provided and if
   # it does not already exist.
   def generate_next_court
      unless next_court_date.nil? || next_judge_id.to_i == 0 || next_court_type_id.to_i == 0 || next_docket_type_id.to_i == 0
         unless Court.find(:first, :conditions => {:docket_id => docket_id, :court_date => next_court_date, :judge_id => next_judge_id, :court_type_id => next_court_type_id, :docket_type_id => next_docket_type_id})
            c = Court.new(:docket_id => docket_id, :court_date => next_court_date, :judge_id => next_judge_id, :court_type_id => next_court_type_id, :docket_type_id => next_docket_type_id, :created_by => updated_by, :updated_by => updated_by)
            c.save(false)
         end
      end
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      intvals = ['needs_state_report','docket_id','court_type_id', 'judge_id', 'next_judge_id', 'next_court_type_id', 'bench_warrant_issued', 'bond_forfeiture_ordered', 'is_upset_wo_refix', 'is_refixed','plea_type_id', 'is_dismissed', 'is_overturned']
      query.each do |key, value|
         if key == "court_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "courts.court_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "court_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "courts.court_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "next_court_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "courts.next_court_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "next_court_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "courts.next_court_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "overturned_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "courts.overturned_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "overturned_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "courts.overturned_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else
            unless condition.empty?
               condition << " and "
            end
            if intvals.include?(key)
               condition << "courts.#{key} = '#{value}'"
            else
               condition << "courts.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
