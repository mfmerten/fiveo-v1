# == Schema Information
#
# Table name: paper_types
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  cost       :decimal(12, 2)  default(0.0)
#  disabled   :boolean(1)      default(FALSE)
#  created_by :integer(4)      default(1)
#  updated_by :integer(4)      default(1)
#  created_at :datetime
#  updated_at :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class PaperType < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   has_many :papers
   
   before_destroy :check_used
   
   validates_presence_of :name
   validates_uniqueness_of :name
   validates_numericality_of :cost
   
   def self.desc
      "Paper Types"
   end
   
   # returns all types formatted for a select control
   def self.names_for_select(allow_disabled=false)
      if allow_disabled == true
         find(:all, :order => 'name').collect{|c| [c.name,c.id]}
      else
         find(:all, :order => 'name', :conditions => "disabled is false").collect{|c| [c.name, c.id]}
      end
   end
   
   private
   
   # disallow deletes if record is used
   def check_used
      papers.empty?
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      query.each do |key, value|
         if key == 'simple'
            unless condition.empty?
               condition << " and "
            end
            if value.to_i > 0
               condition << "(paper_types.id = '#{value}' OR paper_types.name like '%#{value}%')"
            else
               condition << "paper_types.name like '%#{value}%'"
            end
         else
            unless condition.empty?
               condition << " and "
            end
            condition << "paper_types.#{key} like '%#{value}%'"
         end
         return sanitize_sql_for_conditions(condition)
      end
   end
   
end
