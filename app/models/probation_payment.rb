# == Schema Information
#
# Table name: probation_payments
#
#  id               :integer(4)      not null, primary key
#  probation_id     :integer(4)
#  transaction_date :date
#  officer          :string(255)
#  officer_unit     :string(255)
#  officer_badge    :string(255)
#  officer_id       :integer(4)
#  created_by       :integer(4)      default(1)
#  updated_by       :integer(4)      default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  receipt_no       :string(255)
#  report_datetime  :datetime
#  memo             :string(255)
#  fund1_name       :string(255)
#  fund1_charge     :decimal(12, 2)  default(0.0)
#  fund1_payment    :decimal(12, 2)  default(0.0)
#  fund2_name       :string(255)
#  fund2_charge     :decimal(12, 2)  default(0.0)
#  fund2_payment    :decimal(12, 2)  default(0.0)
#  fund3_name       :string(255)
#  fund3_charge     :decimal(12, 2)  default(0.0)
#  fund3_payment    :decimal(12, 2)  default(0.0)
#  fund4_name       :string(255)
#  fund4_charge     :decimal(12, 2)  default(0.0)
#  fund4_payment    :decimal(12, 2)  default(0.0)
#  fund5_name       :string(255)
#  fund5_charge     :decimal(12, 2)  default(0.0)
#  fund5_payment    :decimal(12, 2)  default(0.0)
#  fund6_name       :string(255)
#  fund6_charge     :decimal(12, 2)  default(0.0)
#  fund6_payment    :decimal(12, 2)  default(0.0)
#  fund7_name       :string(255)
#  fund7_charge     :decimal(12, 2)  default(0.0)
#  fund7_payment    :decimal(12, 2)  default(0.0)
#  fund8_name       :string(255)
#  fund8_charge     :decimal(12, 2)  default(0.0)
#  fund8_payment    :decimal(12, 2)  default(0.0)
#  fund9_name       :string(255)
#  fund9_charge     :decimal(12, 2)  default(0.0)
#  fund9_payment    :decimal(12, 2)  default(0.0)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# ProbationPayment Model
# records probation payment transactions
class ProbationPayment < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :probation
   
   before_save :process_transaction
   before_validation :verify_unposted, :lookup_officers
   after_save :check_creator
   before_destroy :check_and_unprocess
   
   validates_presence_of :officer
   validates_date :transaction_date
   validates_numericality_of :fund1_payment, :fund1_charge, :fund2_payment, :fund2_charge, :fund3_payment,
      :fund3_charge, :fund4_payment, :fund4_charge, :fund5_payment, :fund5_charge, :fund6_payment,
      :fund6_charge, :fund7_payment, :fund7_charge, :fund8_payment, :fund8_charge, :fund9_payment, :fund9_charge
   
   # a short description of the model for the DatabaseController index view.
   def self.desc
      'Probation Payment Records'
   end
   
   # returns the last 10 posting datetimes as an array of arrays for use in
   # a select control
   def self.posting_dates
      postings = find(:all, :select => 'DISTINCT report_datetime', :order => 'report_datetime DESC').collect{|c| c.report_datetime.blank? ? nil : [format_date(c.report_datetime),c.report_datetime]}.compact
      if postings.size > 10
         return postings.slice(-10,10)
      else
         return postings
      end
   end
   
   # returns all unposted transactions
   def self.all_unposted(time = nil)
      if time.blank?
         find(:all, :conditions => 'report_datetime is null')
      else
         find(:all, :conditions => ['report_datetime = ?', time])
      end
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # locate transactions by probation_id and date range
   def self.locate_transactions_by_probation(probation_id, start_date=nil, end_date=nil)
      prob = Probation.find_by_id(probation_id) or return []
      sdate = valid_date(start_date)
      edate = valid_date(end_date)
      cond = "probations.id = '#{prob.id}'"
      unless sdate.nil?
         cond << " AND transaction_date >= '#{sdate.to_s(:db)}'"
      end
      unless edate.nil?
         cond << " AND transaction_date <= '#{edate.to_s(:db)}'"
      end
      find(:all, :include => [{:probation => :person}], :order => 'people.lastname, people.firstname, people.middlename, people.suffix, probations.id, probation_payments.id', :conditions => cond)
   end
   
   # locate transactions by person_id and date range
   def self.locate_transactions_by_person(person_id, start_date=nil, end_date=nil)
      pers = Person.find_by_id(person_id) or return []
      sdate = valid_date(start_date)
      edate = valid_date(end_date)
      cond = "probations.person_id = '#{pers.id}'"
      unless sdate.nil?
         cond << " AND transaction_date >= '#{sdate.to_s(:db)}'"
      end
      unless edate.nil?
         cond << " AND transaction_date <= '#{edate.to_s(:db)}'"
      end
      find(:all, :include => [{:probation => :person}], :order => 'people.lastname, people.firstname, people.middlename, people.suffix, probations.id, probation_payments.id', :conditions => cond)
   end
   
   # locate transactions by date range
   def self.locate_transactions_by_date(start_date=nil, end_date=nil)
      sdate = valid_date(start_date)
      edate = valid_date(end_date)
      cond = ""
      unless sdate.nil?
         unless cond.blank?
            cond << " AND "
         end
         cond << "transaction_date >= '#{sdate.to_s(:db)}'"
      end
      unless edate.nil?
         unless cond.blank?
            cond << " AND "
         end
         cond << "transaction_date <= '#{edate.to_s(:db)}'"
      end
      find(:all, :include => [{:probation => :person}], :order => 'people.lastname, people.firstname, people.middlename, people.suffix, probations.id, probation_payments.id', :conditions => cond)
   end
   
   # returns the sum of all charges on a payment transaction
   def total_charges
      total = BigDecimal.new('0.0',2)
      (1..9).each do |x|
         unless send("fund#{x}_charge").nil?
            total += send("fund#{x}_charge")
         end
      end
      total
   end
   
   # returns the sum of all payments on a payment transaction
   def total_payments
      total = BigDecimal.new('0.0',2)
      (1..9).each do |x|
         unless send("fund#{x}_payment").nil?
            total += send("fund#{x}_payment")
         end
      end
      total
   end
   
   private
   
   # returns a validation error if the transaction has been posted - prevents
   # saving changes to posted transactions
   def verify_unposted
      if report_datetime?
         self.errors.add(:base, "You Cannot Change Posted Transactions!")
         return false
      end
      return true
   end
   
   # checks that transaction has not been posted, then unprocesses the transaction
   # to update probation balances before allowing destroy
   def check_and_unprocess
      if report_datetime?
         # posted, cannot allow delete
         return false
      end
      # perform a reverse processing on the transaction
      unless probation.nil?
         Probation.transaction do
            prob = Probation.find(probation_id, :lock => true)
            (1..9).each do |x|
               unless send("fund#{x}_charge").nil?
                  prob.send("fund#{x}_charged=", prob.send("fund#{x}_charged") - send("fund#{x}_charge"))
               end
               unless send("fund#{x}_payment").nil?
                  prob.send("fund#{x}_paid=", prob.send("fund#{x}_paid") - send("fund#{x}_payment"))
               end
            end
            prob.save!
         end
      end
      # if we get this far, return true to allow deletion
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if officer_id?
         if ofc = Contact.find_by_id(officer_id)
            self.officer = ofc.fullname
            self.officer_badge = ofc.badge_no
            self.officer_unit = ofc.unit
         end
      end
   end

   # This updates the probation balances for the transaction unless transaction
   # is posted.
   def process_transaction
      unless report_datetime?
         unless probation.nil?
            Probation.transaction do
               prob = Probation.find(probation_id, :lock => true)
               (1..9).each do |x|
                  # charge
                  newval = send("fund#{x}_charge")
                  if newval.nil?
                     newval = BigDecimal.new('0.00',2)
                  end
                  oldval = BigDecimal.new('0.00',2)
                  if send("fund#{x}_charge_changed?")
                     unless send("fund#{x}_charge_was").nil?
                        oldval = send("fund#{x}_charge_was")
                     end
                  end
                  prob.send("fund#{x}_charged=", prob.send("fund#{x}_charged") + newval - oldval)
                  # payment
                  newval = send("fund#{x}_payment")
                  if newval.nil?
                     newval = BigDecimal.new('0.00',2)
                  end
                  oldval = BigDecimal.new('0.00',2)
                  if send("fund#{x}_payment_changed?")
                     unless send("fund#{x}_payment_was").nil?
                        oldval = send("fund#{x}_payment_was")
                     end
                  end
                  prob.send("fund#{x}_paid=", prob.send("fund#{x}_paid") + newval - oldval)
                  # new fund?
                  if prob.send("fund#{x}_name") != send("fund#{x}_name")
                     prob.send("fund#{x}_name=", send("fund#{x}_name"))
                  end
               end
               prob.save!
            end
         end
      end
      return true
   end
end
