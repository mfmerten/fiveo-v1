# == Schema Information
#
# Table name: papers
#
#  id                :integer(4)      not null, primary key
#  suit_no           :string(255)
#  plaintiff         :string(255)
#  defendant         :string(255)
#  paper_customer_id :integer(4)
#  serve_to          :string(255)
#  street            :string(255)
#  city              :string(255)
#  state_id          :integer(4)
#  zip               :string(255)
#  phone             :string(255)
#  received_date     :date
#  served_date       :date
#  served_time       :string(255)
#  returned_date     :date
#  service_type_id   :integer(4)
#  noservice_type_id :integer(4)
#  noservice_extra   :string(255)
#  served_to         :string(255)
#  officer_id        :integer(4)
#  officer           :string(255)
#  officer_badge     :string(255)
#  officer_unit      :string(255)
#  comments          :text
#  billable          :boolean(1)      default(TRUE)
#  invoice_datetime  :datetime
#  paid_date         :date
#  invoice_total     :decimal(12, 2)  default(0.0)
#  noservice_fee     :decimal(12, 2)  default(0.0)
#  mileage_fee       :decimal(12, 2)  default(0.0)
#  service_fee       :decimal(12, 2)  default(0.0)
#  memo              :string(255)
#  remarks           :text
#  created_by        :integer(4)      default(1)
#  updated_by        :integer(4)      default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  court_date        :date
#  mileage           :decimal(5, 1)   default(0.0)
#  report_datetime   :datetime
#  paper_type_id     :integer(4)
#  mileage_only      :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Paper Model
# these are civil suit papers to be served
class Paper < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :state, :class_name => 'Option', :foreign_key => 'state_id'
   belongs_to :paper_type
   belongs_to :service_type, :class_name => 'Option', :foreign_key => 'service_type_id'
   belongs_to :noservice_type, :class_name => 'Option', :foreign_key => 'noservice_type_id'
   belongs_to :customer, :class_name => 'PaperCustomer', :foreign_key => 'paper_customer_id'
   has_many :payments, :class_name => 'PaperPayment'
   
   before_validation :fix_times, :lookup_officers, :check_noservice, :check_customer, :check_type
   after_save :check_creator
   before_save :fix_mileage, :fix_fees
   before_destroy :check_and_update_dependencies
   
   validates_presence_of :suit_no, :plaintiff, :defendant, :serve_to
   validates_phone :phone, :allow_blank => true
   validates_zip :zip, :allow_blank => true
   validates_date :received_date
   validates_date :served_date, :returned_date, :paid_date, :court_date, :allow_nil => true
   validates_presence_of :officer, :served_to, :service_type_id, :if => :served_date?
   validates_presence_of :officer, :service_type_id, :if => :returned_date?
   validates_time :served_time, :allow_nil => true
   validates_presence_of :served_date, :if => :served_time?
   validates_numericality_of :mileage, :service_fee, :noservice_fee, :mileage_fee, :allow_nil => true
   validates_option :state_id, :option_type => 'State', :allow_nil => true
   validates_option :service_type_id, :option_type => 'Service Type', :allow_nil => true
   validates_option :noservice_type_id, :option_type => 'Noservice Type', :allow_nil => true
   
   # a short description for the DatabaseController index view
   def self.desc
      "Paper Service"
   end
   
   # returns true if the specified option id is used by any of the option
   # type fields in any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(state_id = '#{oid}' OR service_type_id = '#{oid}' OR noservice_type_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns the last record where serve_to matches the specified name or nil
   # if no matches
   def self.find_by_name(serve_to)
      return nil unless serve_to.is_a?(String) && !serve_to.blank?
      find(:last, :conditions => {:serve_to => serve_to})
   end
   
   def self.find_by_suit(suit_no)
      return nil unless suit_no.is_a?(String) && !suit_no.blank?
      find(:last, :conditions => {:suit_no => suit_no})
   end
   
   # returns a list of paper records that are billable but have not yet been
   # invoiced.
   def self.all_billable(customer_id = nil)
      if customer_id.to_i > 0
         customer_id = customer_id.to_i
      else
         customer_id = nil
      end
      if customer_id.nil?
         find(:all, :conditions => "papers.billable is true and papers.invoice_datetime is null and (papers.served_date is not null or papers.returned_date is not null)")
      else
         find(:all, :conditions => ["papers.billable is true and papers.invoice_datetime is null and (papers.served_date is not null or papers.returned_date is not null) and papers.paper_customer_id = ?",customer_id])
      end
   end
   
   # returns a list of paper records that are payable (invoiced but not paid)
   def self.all_payable(customer_id = nil)
      if customer_id.to_i > 0
         customer_id = customer_id.to_i
      else
         customer_id = nil
      end
      if customer_id.nil?
         find(:all, :conditions => "papers.invoice_datetime is not null and papers.paid_date is null")
      else
         find(:all, :conditions => ["papers.invoice_datetime is not null and papers.paid_date is null and papers.paper_customer_id = ?",customer_id])
      end
   end
   
   # returns base permissions required to view papers
   def self.base_perms
      Perm[:view_civil]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def self.status_for_select
     [['Servable',1],['Served',2],['Returned',3],['No-Bill',4],['Billable',5],['Invoiced',6],['Payable',7],['Paid',8]]
   end
   
   # returns line one of the address (street)
   def address_line1
      street
   end

   # returns line two of the address (city, ST ZIP)
   def address_line2
      name = ""
      if city
         name << city
      end
      if state_id?
         unless name.empty?
            name << ", "
         end
         name << (state.nil? ? '' : state.short_name)
      end
      if zip?
         unless name.empty?
            name << " "
         end
         name << zip
      end
      return name
   end
   
   # returns an address string suitable for mapping with Yahoo Maps or some such
   def map_address
      "#{street}, #{city}, #{state.nil? ? '' : state.short_name}"
   end
   
   # returns a status string for the paper
   def status
      txt = ""
      if paid_date?
         txt << "Paid"
      elsif invoice_datetime?
         txt << "Payable"
      elsif served_date? || returned_date?
         if billable?
            return "Billable"
         else
            return 'No-Bill'
         end
      else
         return "Servable"
      end
   end
   
   private
   
   def check_and_update_dependencies
      if invoice_datetime?
         # cannot destroy invoiced papers
         return false
      end
      # remove paper_id from all associated transactions
      payments.each{|p| p.update_attribute(:paper_id, nil)}
      return true
   end
   
   def fix_mileage
      if mileage.nil?
         self.mileage = 0
      end
      return true
   end
   
   def fix_fees
      if service_fee.nil? || service_fee == 0
         if paper_type.nil?
            self.service_fee = BigDecimal.new("0.0",2)
         else
            self.service_fee = paper_type.cost
         end
      end
      if noservice_fee.nil? || noservice_fee == 0
         if customer.nil?
            self.noservice_fee = BigDecimal.new("0.0",2)
         elsif customer.noservice_fee?
            self.noservice_fee = customer.noservice_fee
         else
            self.noservice_fee = SiteConfig.noservice_fee
         end
      end
      if mileage_fee.nil? || mileage_fee == 0
         if customer.nil?
            self.mileage_fee = BigDecimal.new("0.0",2)
         elsif customer.mileage_fee?
            self.mileage_fee = customer.mileage_fee
         else
            self.mileage_fee = SiteConfig.mileage_fee
         end
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # checks for a service type of no service and enforces entry of the 
   # noservice type.  Checks noservice type and if Moved To:, enforces entry
   # of noservice_extra (for new location)
   def check_noservice
      unless service_type.nil?
         if service_type.long_name == "No Service"
            if noservice_type.nil?
               self.errors.add(:noservice_type_id, "must be set if Service type is 'No Service'!")
               return false
            end
         end
         unless noservice_type.nil?
            if noservice_type.long_name == "Moved To:"
               unless noservice_extra?
                  self.errors.add(:noservice_extra, "cannot be blank if No Service Type is 'Moved To:'.  Enter new location here.")
                  return false
               end
            end
         end
      end
      return true
   end
   
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if officer_id?
         if ofc = Contact.find_by_id(officer_id)
            self.officer = ofc.fullname
            self.officer_badge = ofc.badge_no
            self.officer_unit = ofc.unit
         end
      end
   end
   
   # validates customer
   def check_customer
      if paper_customer_id?
         if customer.nil?
            self.errors.add(:paper_customer_id, "is not a valid Customer ID!")
            return false
         end
      else
         if billable?
            self.errors.add(:paper_customer_id, "is required if this paper is billable!")
            return false
         end
      end
      return true
   end
   
   # validates paper type
   def check_type
      if paper_type_id?
         if paper_type.nil?
            self.errors.add(:paper_type_id, "is not a valid Paper Type!")
            return false
         end
      else
         self.errors.add(:paper_type_id, "is required!")
         return false
      end
      return true
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      intvals = ['paper_customer_id','paper_type_id']
      query.each do |key, value|
         if key == 'court_date'
            unless condition.blank?
               condition << " and "
            end
            condition << "papers.court_date = '#{value.to_s(:db)}'"
         elsif key == 'suit_no'
            unless condition.blank?
               condition << " and "
            end
            condition << "papers.suit_no like '#{value}'"
         elsif key == 'id'
            unless condition.blank?
               condition << " and "
            end
            condition << "papers.id = '#{value.to_i}'"
         elsif key == 'simple'
            txt = ''
            if value =~ /^\d{2}\/\d{2}\/\d{2,4}$/ && (cd = valid_date(value))
               txt << "papers.court_date = '#{cd.to_s(:db)}'"
            else
               txt << "papers.suit_no like '#{value}%'"
               if value.to_i > 0
                  unless txt.blank?
                     txt << " or "
                  end
                  txt << "papers.id = '#{value.to_i}'"
               end
            end
            unless txt.blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "(#{txt})"
            end
          elsif key == 'name'
            unless condition.blank?
              condition << " AND "
            end
            condition << "(papers.plaintiff LIKE '%#{value}%' OR papers.defendant LIKE '%#{value}%' OR papers.serve_to LIKE '%#{value}%' OR papers.served_to LIKE '%#{value}%')"
         elsif key == 'paper_status'
           unless condition.blank?
             condition << " AND "
           end
           if value == '1'
             # servable
             condition << "papers.served_date IS NULL AND papers.returned_date IS NULL"
           elsif value == '2'
             condition << "papers.served_date IS NOT NULL"
           elsif value == '3'
             condition << "papers.returned_date IS NOT NULL"
           elsif value == '4'
             condition << "papers.billable IS FALSE"
           elsif value == '5'
             condition << "(papers.served_date IS NOT NULL OR papers.returned_date IS NOT NULL) AND papers.billable IS TRUE AND papers.invoice_datetime IS NULL"
           elsif value == '6'
             condition << "papers.invoice_datetime IS NOT NULL"
           elsif value == '7'
             condition << "papers.invoice_datetime IS NOT NULL AND papers.paid_date IS NULL"
           elsif value == '8'
             condition << "papers.paid_date IS NOT NULL"
           else
             query.delete(key)
           end
         else
            unless condition.empty?
               condition << " and "
            end
            if intvals.include?(key)
               condition << "papers.#{key} = '#{value}'"
            else
               condition << "papers.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
   
   # calls the valid_time() method for each time type field (valid_time
   # inserts the : between hours and minutes if its omitted).
   def fix_times
      self.served_time = valid_time(served_time)
   end
end
