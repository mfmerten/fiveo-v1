# == Schema Information
#
# Table name: medicals
#
#  id              :integer(4)      not null, primary key
#  booking_id      :integer(4)
#  screen_date     :date
#  screen_time     :string(255)
#  arrest_injuries :string(255)
#  officer         :string(255)
#  officer_unit    :string(255)
#  disposition     :string(255)
#  rec_med_tmnt    :string(255)
#  rec_dental_tmnt :string(255)
#  rec_mental_tmnt :string(255)
#  current_meds    :string(255)
#  allergies       :string(255)
#  injuries        :string(255)
#  infestation     :string(255)
#  alcohol         :string(255)
#  drug            :string(255)
#  withdrawal      :string(255)
#  attempt_suicide :string(255)
#  suicide_risk    :string(255)
#  danger          :string(255)
#  pregnant        :string(255)
#  deformities     :string(255)
#  heart_disease   :boolean(1)
#  blood_pressure  :boolean(1)
#  diabetes        :boolean(1)
#  epilepsy        :boolean(1)
#  hepatitis       :boolean(1)
#  hiv             :boolean(1)
#  tb              :boolean(1)
#  ulcers          :boolean(1)
#  venereal        :boolean(1)
#  created_by      :integer(4)
#  updated_by      :integer(4)
#  created_at      :datetime
#  updated_at      :datetime
#  officer_id      :integer(4)
#  officer_badge   :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Medical Model
# Medical screening reports for booking.
class Medical < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :booking
   
   before_validation :fix_time
   after_save :check_creator
   
   validates_presence_of :officer, :disposition
   validates_date :screen_date
   validates_time :screen_time
   validates_booking :booking_id
   
   # a short description for the DatabaseController index view.
   def self.desc
      'Medical Screening Records'
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_booking]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # calls valid_time() for each time type field which checks the value and
   # inserts a : if it was omitted.
   def fix_time
      self.screen_time = valid_time(screen_time)
   end
end
