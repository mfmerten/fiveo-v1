# == Schema Information
#
# Table name: sentences
#
#  id                          :integer(4)      not null, primary key
#  docket_id                   :integer(4)
#  court_id                    :integer(4)
#  probation_id                :integer(4)
#  da_charge_id                :integer(4)
#  result_id                   :integer(4)
#  doc                         :boolean(1)      default(FALSE)
#  fines                       :decimal(12, 2)  default(0.0)
#  court_cost_type_id          :integer(4)
#  costs                       :decimal(12, 2)  default(0.0)
#  default_days                :integer(4)      default(0)
#  default_months              :integer(4)      default(0)
#  default_years               :integer(4)      default(0)
#  pay_by_date                 :date
#  sentence_days               :integer(4)      default(0)
#  sentence_months             :integer(4)      default(0)
#  sentence_years              :integer(4)      default(0)
#  suspended                   :boolean(1)      default(FALSE)
#  suspended_except_days       :integer(4)      default(0)
#  suspended_except_months     :integer(4)      default(0)
#  suspended_except_years      :integer(4)      default(0)
#  probation_days              :integer(4)      default(0)
#  probation_months            :integer(4)      default(0)
#  probation_years             :integer(4)      default(0)
#  credit_time_served          :boolean(1)      default(FALSE)
#  community_service_days      :integer(4)
#  substance_abuse_program     :boolean(1)      default(FALSE)
#  driver_improvement          :boolean(1)      default(FALSE)
#  probation_fees              :decimal(12, 2)  default(0.0)
#  idf_amount                  :decimal(12, 2)  default(0.0)
#  dare_amount                 :decimal(12, 2)  default(0.0)
#  restitution_amount          :decimal(12, 2)  default(0.0)
#  anger_management            :boolean(1)      default(FALSE)
#  substance_abuse_treatment   :boolean(1)      default(FALSE)
#  random_drug_screens         :boolean(1)      default(FALSE)
#  no_victim_contact           :boolean(1)      default(FALSE)
#  art_893                     :boolean(1)      default(FALSE)
#  art_894                     :boolean(1)      default(FALSE)
#  art_895                     :boolean(1)      default(FALSE)
#  probation_reverts           :boolean(1)      default(FALSE)
#  probation_revert_conditions :string(255)
#  wo_benefit                  :boolean(1)      default(FALSE)
#  sentence_consecutive        :boolean(1)      default(FALSE)
#  sentence_notes              :string(255)
#  fines_consecutive           :boolean(1)      default(FALSE)
#  fines_notes                 :string(255)
#  costs_consecutive           :boolean(1)      default(FALSE)
#  costs_notes                 :string(255)
#  notes                       :text
#  remarks                     :text
#  processed                   :boolean(1)      default(FALSE)
#  created_by                  :integer(4)      default(1)
#  updated_by                  :integer(4)      default(1)
#  created_at                  :datetime
#  updated_at                  :datetime
#  parish_jail                 :boolean(1)      default(FALSE)
#  consecutive_type_id         :integer(4)
#  conviction                  :string(255)
#  conviction_count            :integer(4)
#  probation_type_id           :integer(4)
#  restitution_date            :date
#  legacy                      :boolean(1)      default(FALSE)
#  process_status              :string(255)
#  default_hours               :integer(4)      default(0)
#  sentence_hours              :integer(4)      default(0)
#  suspended_except_hours      :integer(4)      default(0)
#  probation_hours             :integer(4)      default(0)
#  delay_execution             :boolean(1)      default(FALSE)
#  community_service_hours     :integer(4)      default(0)
#  deny_goodtime               :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Sentence Model
# This holds sentences for docket charges.
class Sentence < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :docket
   belongs_to :court
   belongs_to :probation, :dependent => :destroy
   belongs_to :da_charge
   belongs_to :result, :class_name => 'Option', :foreign_key => 'result_id'
   belongs_to :court_cost_type, :class_name => 'Option', :foreign_key => 'court_cost_type_id'
   belongs_to :consecutive_type, :class_name => 'Option', :foreign_key => 'consecutive_type_id'
   belongs_to :probation_type, :class_name => 'Option', :foreign_key => 'probation_type_id'
   has_and_belongs_to_many :consecutive_dockets, :class_name => 'Docket', :join_table => 'dockets_sentences'
   has_one :hold, :dependent => :destroy
   has_one :doc_record, :class_name => 'Doc', :dependent => :destroy
   
   attr_accessor :force
   
   # this sets up sphinx indices for this model
   define_index do
      indexes [docket.person.lastname, docket.person.firstname, docket.person.middlename], :as => :name
      indexes notes
      indexes remarks
      indexes conviction
      indexes docket.docket_no, :as => :dno
      
      has updated_at
   end
   
   before_validation :lookup_court_costs, :check_probation_type
   after_create :check_docket
   before_save :fix_numbers, :process
   after_save :check_creator
   after_update :save_children
   before_destroy :check_and_destroy_dependencies
   
   validates_option :result_id, :option_type => 'Trial Result'
   validates_option :court_cost_type_id, :option_type => 'Court Costs', :allow_nil => true
   validates_option :consecutive_type_id, :option_type => 'Consecutive Type', :allow_nil => true
   validates_option :probation_type_id, :option_type => 'Probation Type', :allow_nil => true
   validates_date :restitution_date, :pay_by_date, :allow_nil => true
   validates_numericality_of :fines, :costs, :probation_fees, :idf_amount, :dare_amount, :restitution_amount, :greater_than_or_equal_to => 0
   validates_numericality_of :default_hours, :default_days, :default_months, :default_years, :sentence_hours, :sentence_days, :sentence_months, :sentence_years, :suspended_except_hours, :suspended_except_days, :suspended_except_months, :suspended_except_years, :probation_hours, :probation_days, :probation_months, :probation_years, :community_service_days, :only_integer => true, :greater_than_or_equal_to => 0, :allow_nil => true
   
   def self.desc
      'Sentences for Dockets'
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_court]
   end
   
   # returns true if the specified option id is in use in any of the option
   # type fields in any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(result_id = '#{oid}' or court_cost_type_id = '#{oid}' or probation_type_id = '#{oid}' or consecutive_type_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   def self.hours_served(*sentence_ids)
      sentences = []
      sentence_ids.flatten.uniq.each do |s|
         if sent = Sentence.find_by_id(s)
            sentences.push(sent)
         end
      end
      return 0 if sentences.empty?
      ranges = []
      sentences.each do |s|
         if s.hold.nil?
            unless s.doc_record.nil? || s.doc_record.hold.nil?
               hold_datetime = get_datetime(s.doc_record.hold.hold_date, s.doc_record.hold.hold_time)
               datetime = hold_datetime
               unless s.doc_record.hold.booking.nil?
                  booking_datetime = get_datetime(s.doc_record.hold.booking.booking_date, s.doc_record.hold.booking.booking_time)
                  datetime = [hold_datetime, booking_datetime].sort.pop
               end
               ranges.push([datetime, get_datetime(s.doc_record.hold.cleared_date, s.doc_record.hold.cleared_time)])
            end
         else
            hold_datetime = get_datetime(s.hold.hold_date, s.hold.hold_time)
            datetime = hold_datetime
            unless s.hold.booking.nil?
               booking_datetime = get_datetime(s.hold.booking.booking_date, s.hold.booking.booking_time)
               datetime = [hold_datetime, booking_datetime].sort.pop
            end
            ranges.push([datetime, get_datetime(s.hold.cleared_date, s.hold.cleared_time)])
         end
      end
      ranges = ranges.compact
      calculate_time_served(ranges)
   end
   
   def sentence_date
      return nil if court.nil?
      court.court_date
   end
   
   # returns a string combining the sentence fields
   def sentence_string
      sentence_to_string(sentence_hours, sentence_days, sentence_months, sentence_years)
   end
   
   def total_hours
      if suspended?
         sentence_to_hours(suspended_except_hours, suspended_except_days, suspended_except_months, suspended_except_years)
      else
         sentence_to_hours(sentence_hours, sentence_days, sentence_months, sentence_years)
      end
   end
   
   def sentence_summary(with_probation=true)
      txt = ''
      unless result.nil?
         case result.long_name
         when "Dismissed"
            return "Charge Dismissed"
         when "Not Guilty"
            return "Found Not Guilty"
         when "Pled Guilty"
            trial_result = "Pled Guilty to"
         when "Found Guilty"
            trial_result = "Found Guilty of"
         when "No Contest"
            trial_result = "Pled No Contest to"
         else
            trial_result = result.long_name
         end
      end
      txt << "#{trial_result} #{conviction_count} #{conviction_count == 1 ? 'count' : 'counts'} #{conviction != da_charge.charge ? 'of amended charge' : ''} #{conviction}"
      unless txt.slice(-1,1) == '.'
         txt << '.'
      end
      unless sentence_string.blank?
         txt << " To serve #{sentence_string}"
         if doc? && !parish_jail?
            txt << " at hard labor for Dept. of Corrections"
         elsif doc? && parish_jail?
            txt << " in Parish jail for Dept. of Corrections"
         else
            txt << " in Parish jail"
         end
         if wo_benefit?
            txt << " without benefit of parole"
         end
         if credit_time_served?
            txt << ", with credit for time served"
         end
         unless with_probation == false
            if suspended?
               txt << ", suspended"
               unless suspended_except_string.blank?
                  txt << " except #{suspended_except_string}"
               end
               unless probation_string.blank?
                  txt << ", and placed on #{probation_type.nil? ? '' : probation_type.long_name} Probation for #{probation_string}"
                  if probation_reverts?
                     txt << " reverting to Unsupervised"
                  end
                  if probation_revert_conditions?
                     txt << " after #{probation_revert_conditions}"
                  end
               end
            end
         end
         unless txt.slice(-1,1) == '.'
            txt << "."
         end
      end
      return txt
   end
   
   # returns a string combining the suspended_except fields
   def suspended_except_string
      sentence_to_string(suspended_except_hours, suspended_except_days, suspended_except_months, suspended_except_years)
   end

   # returns a string combining the default fields
   def default_string
      sentence_to_string(default_hours, default_days, default_months, default_years)
   end

   # returns a string combining the probation fields
   def probation_string
      sentence_to_string(probation_hours, probation_days, probation_months, probation_years)
   end
   
   def consecutive_dockets=(selected_dockets)
      self.consecutive_dockets.clear
      selected_dockets.each do |d|
         self.consecutive_dockets << Docket.find_by_id(d) unless d.empty?
      end
   end
   
   # returns true if this sentence should result in a probation record for
   # parish probation
   def parish_probation?
      return false if probation_string.blank? # no probation time entered
      return false if probation_type.nil? # can't determine probation type
      return (probation_type.long_name == 'Parish')
   end
   
   # return true if this sentence should result in a serving sentence hold
   def jail_time?
      return false if sentence_string.blank?  # no sentence entered, no time to serve
      return false if suspended? && suspended_except_string.blank?  # sentence suspended except for a specified amount
      return true
   end
   
   def consecutive_string
      cons_any = Option.lookup("Consecutive Type","Any Current",nil,nil,true)
      cons_this = Option.lookup("Consecutive Type","This Docket",nil,nil,true)
      cons_other = Option.lookup("Consecutive Type", "Other Dockets",nil,nil,true)
      cons_txt = ""
      unless consecutive_type.nil?
         if consecutive_type.long_name == 'Any Current'
            cons_txt << "with any current sentences."
         elsif consecutive_type.long_name == 'This Docket'
            cons_txt << "with other sentences on this docket."
         elsif consecutive_type.long_name == 'Other Dockets'
            unless consecutive_dockets.empty?
               cons = consecutive_dockets.collect{|d| d.docket_no}.join(", ")
               cons_txt << "with dockets #{cons}."
            end
         end
      end
      fines_txt = "Fines to run #{fines_consecutive? ? 'consecutive' : 'concurrent'}#{cons_txt.blank? ? '' : ' ' + cons_txt}#{fines_notes.blank? ? '' : ' (Note: ' + fines_notes + ')'}."
      costs_txt = "Costs to run #{costs_consecutive? ? 'consecutive' : 'concurrent'}#{cons_txt.blank? ? '' : ' ' + cons_txt}#{costs_notes.blank? ? '' : ' (Note: ' + costs_notes + ')'}."
      sent_txt = "Sentence to run #{sentence_consecutive? ? 'consecutive' : 'concurrent'}#{cons_txt.blank? ? '' : ' ' + cons_txt}#{sentence_notes.blank? ? '' : ' (Note: ' + sentence_notes + ')'}."
      return "#{sent_txt} #{fines_txt} #{costs_txt}"
   end
   
   # return a list of the consecutive sentencing holds as determined by the consecutive_type option
   def consecutive_holds
      return [] if consecutive_type.nil? || !sentence_consecutive?
      if consecutive_type.long_name == "Any Current"
         hold_list = []
         unless docket.nil? || docket.person.nil? || docket.person.bookings.empty? || docket.person.bookings.last.release_date? || docket.person.bookings.last.holds.empty?
            docket.person.bookings.last.holds.reject{|h| h.cleared_date? || h.hold_type.nil? || h.hold_type.long_name != 'Serving Sentence'}.each do |h|
               hold_list.push(hold)
            end
         end
         return hold_list
      elsif consecutive_type.long_name == "Other Dockets" || consecutive_type.long_name == "This Docket"
         hold_list = []
         if consecutive_type.long_name == "Other Dockets"
            docket_list = consecutive_dockets
         else
            docket_list = [docket]
         end
         docket_list.each do |d|
            d.da_charges.each do |c|
               # if the charge has not been processed, we can't calculate consecutives yet
               return nil unless c.processed?
               # if the charge does not have a sentence, skip it
               next if c.sentence.nil?
               # if the sentence is this one, skip it
               next if c.sentence.id == self.id
               # if the sentence does not have a hold or doc record, its not for serving time
               next if c.sentence.hold.nil? && c.sentence.doc_record.nil?
               if c.sentence.doc_record.nil?
                  # return sentence hold
                  hold_list.push(c.sentence.hold)
               else
                  return nil if c.sentence.doc_record.hold.nil?
                  hold_list.push(c.sentence.doc_record.hold)
               end
            end
         end
         return hold_list
      else
         # unknown consecutive_type - ignore it
         return []
      end
   end
      
   private
   
   def check_probation_type
      if probation_type_id?
         unless (probation_hours? && probation_hours > 0) || (probation_days? && probation_days > 0) || (probation_years? && probation_years > 0) || (probation_months? && probation_months > 0)
            self.errors.add(:probation_type_id, "must be blank (---) if there is no probation time specified.")
            self.errors.add(:probation_hours, "must be specified when a probation type is selected.")
            self.errors.add(:probation_days, "must be specified when a probation type is selected.")
            self.errors.add(:probation_months, "must be specified when a probation type is selected.")
            self.errors.add(:probation_years, "must be specified when a probation type is selected.")
            return false
         end
      else
         if (probation_hours? && probation_hours > 0) || (probation_days? && probation_days > 0) || (probation_years? && probation_years > 0) || (probation_months? && probation_months > 0)
            self.errors.add(:probation_type_id, "cannot be blank (---) if there is probation time specified.")
            if probation_hours? && probation_hours > 0
               self.errors.add(:probation_hours, "cannot be specified when the probation type is blank (---).")
            end
            if probation_days? && probation_days > 0
               self.errors.add(:probation_days, "cannot be specified when the probation type is blank (---).")
            end
            if probation_months? && probation_months > 0
               self.errors.add(:probation_months, "cannot be specified when the probation type is blank (---).")
            end
            if probation_years? && probation_years > 0
               self.errors.add(:probation_years, "cannot be specified when the probation type is blank (---).")
            end
            return false
         end
      end
      true
   end
   
   # makes sure all sentence type numbers are not nil
   # for some reason, the database defaults are not working reliably
   def fix_numbers
      ['sentence','suspended_except','default','probation'].each do |t|
         ['hours','days','months','years'].each do |s|
            if send("#{t}_#{s}").blank?
               self.send("#{t}_#{s}=",0)
            end
         end
      end
      return true
   end
   
   def check_and_destroy_dependencies
      self.consecutive_dockets.clear
      return true
   end

   # executes after validation but before saving to generate or update any
   # needed probation or serving sentence hold.
   def process
      # since this record has possibly been altered, clear the processed
      # flag.
      self.processed = false
      
      # at this point, need to make sure any attached docket an/or court
      # record get their processed flag unset as well
      unless docket.nil? || !docket.processed?
         self.docket.update_attribute(:processed, false)
      end
      unless court.nil? || !court.processed?
         self.court.update_attribute(:processed, false)
      end
      unless da_charge.nil? || !da_charge.processed?
         self.da_charge.update_attribute(:processed, false)
      end
      
      # default to everything ok
      success = true
      
      # clear old status from process status field
      self.process_status = ""
      
      # check for prerequisits
      if docket.nil?
         success = false
         self.process_status << "[No Linked Docket]"
      elsif docket.person.nil?
         success = false
         self.process_status << "[Docket has Invalid Person]"
      elsif da_charge.nil?
         success = false
         self.process_status << "[No Linked Docket Charge]"
      elsif docket_id != da_charge.docket_id
         success = false
         self.process_status << "[Charge for Different Docket]"
      end

      # update probation if necessary
      if success && !probation_string.blank?
         p = probation
         if p.nil?
            unless da_charge.nil?
               p = da_charge.probation
            end
            if p.nil?
               p = Probation.new
            end
         end
            
         # get appropriate probation status
         case
         when probation_type.nil? || probation_type.long_name == 'Parish'
            stat = Option.lookup("Probation Status","Active",nil,nil,true)
            hif = true
         when probation_type.long_name == 'State'
            stat = Option.lookup("Probation Status","State",nil,nil,true)
            hif = true
         when probation_type.long_name == 'Unsupervised'
            stat = Option.lookup("Probation Status","Unsupervised",nil,nil,true)
            hif = false
         end
         
         # calculate probation hours
         total_hours = sentence_to_hours(probation_hours, probation_days, probation_months, probation_years)
         
         p.person_id = docket.person_id
         # if probation is already completed, don't mess with status
         if !p.status.nil? && p.status.long_name == "Completed"
           # just turn off hold_if_arrested flag
           p.hold_if_arrested = false
         else
           # new probation or status is not completed... process normally
           p.status_id = stat
           p.hold_if_arrested = hif
         end
         p.start_date = sentence_date
         p.end_date = sentence_date + total_hours.hours
         p.conviction = conviction
         p.conviction_count = conviction_count
         p.probation_hours = probation_hours
         p.probation_days = probation_days
         p.probation_months = probation_months
         p.probation_years = probation_years
         p.sentence_notes = consecutive_string + (notes.blank? ? '' : ' -- Additional Sentence Notes: ' + notes)
         unless p.created_by?
            p.created_by = updated_by
         end
         p.updated_by = updated_by
         p.probation_fees = probation_fees
         p.docket_id = docket_id
         p.docket_no = docket.docket_no
         p.court_id = court_id
         p.da_charge_id = da_charge_id
         p.doc = doc
         p.parish_jail = parish_jail
         p.trial_result = (result.nil? ? 'Unknown' : result.long_name)
         p.costs = costs
         p.fines = fines
         p.default_hours = default_hours
         p.default_days = default_days
         p.default_months = default_months
         p.default_years = default_years
         p.pay_by_date = pay_by_date
         p.sentence_hours = sentence_hours
         p.sentence_days = sentence_days
         p.sentence_months = sentence_months
         p.sentence_years = sentence_years
         p.sentence_suspended = suspended
         p.suspended_except_hours = suspended_except_hours
         p.suspended_except_days = suspended_except_days
         p.suspended_except_months = suspended_except_months
         p.suspended_except_years = suspended_except_years
         p.reverts_to_unsup = probation_reverts
         p.reverts_conditions = probation_revert_conditions
         p.credit_served = credit_time_served
         p.service_days = community_service_days
         p.service_hours = community_service_hours
         p.restitution_amount = restitution_amount
         p.restitution_date = restitution_date
         p.idf_amount = idf_amount
         p.dare_amount = dare_amount
         p.sap = substance_abuse_program
         p.driver = driver_improvement
         p.anger = anger_management
         p.sat = substance_abuse_treatment
         p.random_screens = random_drug_screens
         p.no_victim_contact = no_victim_contact
         p.art893 = art_893
         p.art894 = art_894
         p.art895 = art_895
         p.remarks = 'Automatically added by sentence processing.'
         if p.save
            self.probation_id = p.id
         else
            success = false
            self.process_status << "[Probation Could Not Be Saved]"
            self.process_status << "(#{p.errors.full_messages.join(', ')})"
         end
      end
      
      # Check for jail
      if success && jail_time?
         # validate booking
         if docket.person.bookings.empty? || (hold.nil? && docket.person.bookings.last.release_date?)
            # if delay_execution set, need to create booking record
            if delay_execution? || (force == true && (docket.person.bookings.empty? || (docket.person.bookings.last.release_date < court.court_date)))
               # creating a booking because:
               # 1. delayed execution
               # 2. forcing processing and no bookings exist
               # 3. forcing processing and last booking was released before the court date
               book = Booking.new(
                  :person_id => docket.person_id,
                  :disable_timers => true,
                  :created_by => updated_by,
                  :updated_by => updated_by,
                  :remarks => 'Created Automatically By Sentence Processing'
               )
               book.booking_date, book.booking_time = get_date_and_time
               if force == true
                  book.release_date = book.booking_date
                  book.release_time = book.booking_time
                  book.release_officer = book.booking_officer
                  book.release_officer_unit = book.booking_officer_unit
                  book.release_officer_badge = book.booking_officer_badge
                  book.released_because_id = Option.lookup("Release Reason","Automatic Release",nil,nil,true)
                  book.remarks = 'Created and Released Automatically By Sentence Processing.'
               end
               book.save(false)
               unless force == true
                  # booking sucessful, add temp release hold
                  newhold = Hold.new(
                     :booking_id => book.id,
                     :hold_type_id => Option.lookup("Hold Type","Temporary Release",nil,nil,true),
                     :temp_release_reason_id => Option.lookup("Temporary Release Reason","Awaiting Execution Of Sentence",nil,nil,true),
                     :hold_by => updater.name,
                     :hold_by_unit => updater.unit,
                     :hold_by_badge => updater.badge,
                     :created_by => updated_by,
                     :updated_by => updated_by,
                     :remarks => 'Created Automatically By Sentence Processing'
                  )
                  newhold.hold_date, newhold.hold_time = get_date_and_time
                  if newhold.valid?
                     newhold.save
                     # add pending sentence hold (this sentence may not process successfully)
                     newhold = Hold.new(
                        :booking_id => book.id,
                        :hold_type_id => Option.lookup("Hold Type","Pending Sentence",nil,nil,true),
                        :hold_by => updater.name,
                        :hold_by_unit => updater.unit,
                        :hold_by_badge => updater.badge,
                        :created_by => updated_by,
                        :updated_by => updated_by,
                        :remarks => 'Created Automatically By Sentence Processing'
                     )
                     newhold.hold_date, newhold.hold_time = get_date_and_time
                     if newhold.valid?
                        newhold.save
                     else
                        self.process_status << '[Unable to Create Pending Sentence]'
                        self.process_status << "(#{newhold.errors.full_messages.join(', ')})"
                        success = false
                     end
                  else
                     self.process_status << '[Unable to Create Temporary Release]'
                     self.process_status << "(#{newhold.errors.full_messages.join(', ')})"
                     success = false
                  end
               end
            elsif force == true && docket.person.bookings.last.release_date?
               book = docket.person.bookings.last
            else
               success = false
               self.process_status << '[Invalid Booking]'
            end
         else
            # using last booking because its still open, or we are forcing processing
            book = docket.person.bookings.last
         end
         
         if success
            # should be ready now to post the jail hold
            
            if doc? && (!suspended? || (suspended? && !parish_jail?))
               # Hard Labor Dept of Corrections
               
               # check to make sure there is not a parish hold already
               if !hold.nil? && !hold.doc?
                  # oops, get rid of it
                  hold.destroy
               end
               
               # now place a doc record which should re-add the hold
               if suspended?
                  total_hours = sentence_to_hours(suspended_except_hours, suspended_except_days, suspended_except_months, suspended_except_years)
               else
                  total_hours = sentence_to_hours(sentence_hours, sentence_days, sentence_months, sentence_years)
               end
               if total_hours <= 0
                  total_hours = 0
               end
               if credit_time_served?
                  # subtract time served on this charge from total hours to be served
                  time_credit = da_charge.time_served_in_hours
                  if time_credit > 0
                     total_hours = total_hours - time_credit
                     rem = remarks + "\nTo Serve time reflects #{time_credit} Hours credit for time served."
                  end
               end
               
               d = doc_record
               if d.nil?
                  d = self.build_doc_record
               end
               d.sentence_hours = sentence_hours
               d.sentence_days = sentence_days
               d.sentence_months = sentence_months
               d.sentence_years = sentence_years
               d.conviction = conviction_count.to_s + " count " + conviction
               d.remarks = (rem.nil? ? remarks : remarks + rem)
               unless d.created_by?
                  d.created_by = updated_by
               end
               d.updated_by = updated_by
               d.booking_id = book.id
               d.person_id = docket.person_id
               d.to_serve_hours, d.to_serve_days, d.to_serve_months, d.to_serve_years = hours_to_sentence(total_hours)
               if force == true
                  # if we are forcing, tell doc to use a closed booking
                  d.force = true
               end
               unless d.valid?
                  self.process_status << '[Unable to Create DOC Record]'
                  self.process_status << "(#{d.errors.full_messages.join(', ')})"
                  success = false
               end
            else
               # Parish Jail
               
               # make sure there is not a doc_record already
               unless doc_record.nil?
                  # oops, get rid of it
                  doc_record.destroy
               end
               
               # calculate total hours
               if suspended?
                  total_hours = sentence_to_hours(suspended_except_hours, suspended_except_days, suspended_except_months, suspended_except_years)
               else
                  total_hours = sentence_to_hours(sentence_hours, sentence_days, sentence_months, sentence_years)
               end
               if total_hours <= 0
                  total_hours = 0
               end
               
               # get consecutives
               consecs = consecutive_holds
               if consecs.nil?
                  # not ready for processing
                  self.process_status << "[Sentence consecutive to unprocessed sentences]"
                  success = false
               else
                  served = book.hours_served_since(get_datetime(court.court_date, "12:00"))
                  h = hold(true)
                  if h.nil?
                     h = self.build_hold
                  end
                  h.booking_id = book.id
                  h.hold_date = court.court_date
                  h.hold_time = "12:00"
                  h.hold_type_id = Option.lookup("Hold Type","Serving Sentence",nil,nil,true)
                  h.hold_by = updater.name
                  h.hold_by_unit = updater.unit
                  h.hold_by_badge = updater.badge
                  h.remarks = "Automatically Added by Sentence Processing."
                  unless h.created_by?
                     h.created_by = updated_by
                  end
                  h.updated_by = updated_by
                  unless h.hours_served? && h.hours_served > 0
                     h.hours_served = served
                  end
                  h.hours_total = total_hours
                  if credit_time_served?
                     unless h.hours_time_served? && h.hours_time_served > 0
                        ts = da_charge.time_served_in_hours
                        if ts > 0
                           h.hours_time_served = ts
                        else
                           h.hours_time_served = 0
                        end
                     end
                  else
                     h.hours_time_served = 0
                  end
                  unless h.datetime_counted?
                     h.datetime_counted = DateTime.now
                  end
                  if force == true
                     h.cleared_date = h.hold_date
                     h.cleared_time = h.hold_time
                     h.cleared_because_id = Option.lookup("Release Type","Automatic Release",nil,nil,true)
                     h.cleared_by = h.hold_by
                     h.cleared_by_unit = h.hold_by_unit
                     h.cleared_by_badge = h.hold_by_badge
                     h.explanation = 'Inmate Released Prior To Sentence Processing because of time served or transfer.'
                     h.remarks = 'Automatically Added And Cleared by Sentence Processing.'
                  end
                  h.deny_goodtime = deny_goodtime
                  if h.valid?
                     # need to close all pending sentence holds, if any
                     pendings = Hold.find(:all, :conditions => {:booking_id => book.id, :cleared_date => nil, :hold_type_id => Option.lookup("Hold Type","Pending Sentence",nil,nil,true)})
                     unless pendings.empty?
                        auto_release = Option.lookup('Release Type','Automatic Release',nil,nil,true)
                        pendings.each do |p|
                           p.cleared_date, p.cleared_time = get_date_and_time
                           p.cleared_because_id = auto_release
                           p.cleared_by = updater.name
                           p.cleared_by_unit = updater.unit
                           p.cleared_by_badge = updater.badge
                           p.explanation = "Automatically cleared by Sentence Processing."
                           p.updated_by = updater
                           p.save(false)
                        end
                     end
                  else
                     self.process_status << '[Unable to Create Sentence Hold]'
                     self.process_status << "(#{h.errors.full_messages.join(', ')})"
                     success = false
                  end
               end
            end
         end
      end
      if success
         self.processed = true
         self.process_status = nil
         self.da_charge.update_attribute(:processed, true)
      end
      self.force = false
      return true
   end
   
   def save_children
      if doc? && (!suspended? || (suspended? && !parish_jail?))
         unless doc_record.nil?
            self.doc_record.save(false)
         end
      end
      unless hold.nil?
         self.hold.save(false)
         self.hold.blockers.clear
         c = consecutive_holds
         unless c.empty? || c.nil?
            self.hold.blockers << c
         end
      end
   end
   
   def check_docket
      unless processed?
         if docket.processed?
            self.docket.update_attribute(:processed, false)
         end
         if court.processed?
            self.court.update_attribute(:processed, false)
         end
      end
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   def lookup_court_costs
      unless court_cost_type.nil?
         self.costs = BigDecimal.new(court_cost_type.short_name, 2)
      end
   end
end
