# == Schema Information
#
# Table name: dockets
#
#  id                :integer(4)      not null, primary key
#  docket_no         :string(255)
#  person_id         :integer(4)
#  state_report_date :date
#  created_by        :integer(4)      default(1)
#  updated_by        :integer(4)      default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  processed         :boolean(1)      default(FALSE)
#  notes             :text
#  remarks           :text
#  misc              :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Docket Model
# This is the main part of the court system and tracks court cases by
# docket number.
class Docket < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   has_many :da_charges, :dependent => :destroy
   belongs_to :person
   has_many :probations
   has_many :courts, :dependent => :destroy
   has_many :sentences
   has_many :revocations, :dependent => :destroy
   
   before_validation :fix_docket_no, :ensure_docket_unique
   after_save :check_creator, :check_misc
   after_update :save_charges
   
   validates_presence_of :docket_no
   validates_date :state_report_date, :allow_nil => true
   validates_person :person_id
   
   # returns a short description for the DatabaseController index view
   def self.desc
      'Court Docket Table'
   end
   
   # returns base permissions needed to View records
   def self.base_perms
      Perm[:view_court]
   end

   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end

   # Finds all unprocessed dockets processes each court
   def self.process_dockets
      find(:all, :conditions => 'processed != 1').each{|d| d.process_docket}
   end
   
   # Returns a list of dockets for a person for a select control
   def self.dockets_for_select(person_id)
      return [] unless person_id.is_a?(Integer)
      find(:all, :conditions => ['person_id = ?',person_id]).collect{|d| [d.docket_no, d.id]}.unshift(["---",nil])
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # returns a list of all dockets reporting relationship to specified case number
   def self.find_all_by_case_no(caseno)
      find(:all, :include => {:da_charges => [{:arrest => :offense}, {:arrest_charge => {:district_arrest => :offense}}]}, :conditions => ['arrests.case_no = ? OR district_arrests_charges.case_no = ? OR offenses.case_no = ? OR offenses_arrests.case_no = ?', caseno, caseno, caseno, caseno])
   end
   
   # returns the next court(s) in chronological order (earliest first)
   def next_courts
      return [] if courts.empty?
      cs = Court.find(:all, :order => 'court_date', :conditions => ['docket_id = ? AND court_date >= ?', id, Date.today])
      return [] if cs.empty?
      nextcs = []
      firstc = nil
      cs.each do |c|
         if firstc.nil?
            firstc = c
         end
         if c.court_date == firstc.court_date
            nextcs.push(c)
         end
      end
      return nextcs
   end
   
   # accepts a list of charge ids and uses them to build a set of da_charges
   def linked_charges=(selected_charges)
      selected_charges.each do |c|
         if chg = Charge.find_by_id(c)
            self.da_charges.create(
               :charge_id => chg.id,
               :docket_id => id,
               :count => chg.count,
               :charge => chg.charge,
               :created_by => updated_by,
               :updated_by => updated_by,
               :processed => false,
               :dropped_by_da => false
            )
         end
      end
   end
   
   # examine da_charges and find all arrests linked through arrest_charge
   def arrests
      arr = []
      da_charges.each do |c|
         unless c.arrest_charge.nil?
            unless c.arrest_charge.district_arrest.nil?
               arr.push(c.arrest_charge.district_arrest)
            end
         end
         unless c.arrest.nil?
            arr.push(c.arrest)
         end
      end
      arr.uniq
   end
   
   def bonds
      arrests.collect{|a| a.bonds}.flatten.compact.uniq
   end
   
   # examine da_charges and find all citations linked through arrest_charge
   def citations
      cit = []
      da_charges.each do |c|
         unless c.arrest_charge.nil?
            unless c.arrest_charge.citation.nil?
               cit.push(c.arrest_charge.citation)
            end
         end
      end
      cit.uniq
   end
   
   # accepts an array of charge attributes from the DocketsController
	# edit view and creates charge records linked to this docket.
   def new_charge_attributes=(charge_attributes)
      charge_attributes.each do |attributes|
         unless attributes[:charge].blank?
            da_charges.build(attributes)
         end
      end
   end

   # accepts an array of charge attributes from the DocketsController
   # edit view. It cycles through all existing charges for the
   # docket. If the charge is included in the charge attributes, the
   # charge is updated with the attributes, otherwise the charge
   # record is destroyed.
   def existing_charge_attributes=(charge_attributes)
      da_charges.reject(&:new_record?).each do |charge|
         attributes = charge_attributes[charge.id.to_s]
         if attributes
            attributes[:updated_by] = updated_by
            charge.attributes = attributes
         else
            da_charges.delete(charge)
         end
      end
   end
   
   def sentence_summary
      return nil if sentences.empty?
      txt = []
      skip = []
      skip.push(Option.lookup("Trial Result", 'Not Guilty',nil,nil,true))
      skip.push(Option.lookup("Trial Result", 'Dismissed',nil,nil,true))
      sentences.each do |s|
         unless skip.include?(s.result_id)
            txt.push("#{s.conviction}(#{s.conviction_count})")
         end
      end
      return txt.join(', ')
   end
   
   # This returns a single text string listing all charges and their counts
   # produced by cycling through all associated charge records.
   def charge_summary(skip_dropped = false)
      if skip_dropped == true
         text = da_charges.reject{|c| c.dropped_by_da?}.collect{|c| "#{c.charge}(#{c.count})"}.join(" -- ")
      else
         text = da_charges.collect{|c| "#{c.charge}(#{c.count})"}.join(" -- ")
      end
      if text.blank?
         return "No Charges"
      end
      return text
   end
   
   def case_numbers
      list = []
      arrests.each do |a|
         list.push(a.case_no)
         unless a.offense.nil?
            list.push(a.offense.case_no)
            unless a.offense.call.nil?
               list.push(a.offense.call.case_no)
            end
         end
      end
      list.compact.uniq
   end
   
   # Calculate the "Time Served" to-date for a docket by examining all arrests
   def time_served_in_hours
      return 0 if arrests.empty?
      Arrest.hours_served(arrests.collect{|a| a.id})
   end
   
   # accepts a docket id and determines total sentence length for that docket
   # does not take into consideration any other dockets that may be running
   # consecutively.
   def total_sentence_length
      total_hours = 0
      return 0 if sentences.empty?
      consecutives = []
      concurrents = []
      sentences.each do |s|
         if s.consecutive_type.nil? || s.consecutive_type.long_name == ''
            concurrents.push(s.total_hours)
         else
            consecutives.push(s.total_hours)
         end
      end
      total_hours += concurrents.sort{|a,b| b <=> a}.first
      consecutives.each{|c| total_hours += c}
      return total_hours
   end
   
   def original_sentence_date
      # return earliest date from all sentences
      return nil if sentences.empty?
      sent_date = Date.today
      sentences.each do |s|
         if !s.sentence_date.nil? && s.sentence_date < sent_date
            sent_date = s.sentence_date
         end
      end
      return sent_date
   end
   
   # processes courts that that have not yet been processed for whatever reason
   def process_docket
      return true if processed?
      # if the docket is miscellaneous, none of the charges will ever be
      # processed, they are merely for informational purposes.  Same goes
      # for all of the courts.
      if misc?
         da_charges.each do |c|
            c.update_attribute(:processed, true)
         end
         courts.each do |c|
            c.update_attribute(:processed, true)
         end
         self.update_attribute(:processed, true)
      end
      
      # check for unprocessed revocations
      revocations.reject(&:processed?).each{|r| r.save(false)}
      
      # check for unprocessed courts
      courts.reject(&:processed?).each{|c| c.process_court}
      
      # doublecheck
      if courts.reject(&:processed?).empty? && revocations.reject(&:processed?).empty?
         self.update_attribute(:processed, :true)
      end
   end
   
   private
   
   def fix_docket_no
      unless docket_no.nil?
         self.docket_no = docket_no.clone.strip.sub(/^0*/,'')
      end
      true
   end
   
   def check_misc
      if misc?
         da_charges.each do |c|
            unless c.info_only?
               c.update_attribute(:info_only, true)
               c.update_attribute(:processed, true)
            end
         end
         unless processed?
            self.update_attribute(:processed, true)
         end
      end
   end
   
   # ensures docket is unique for person
   def ensure_docket_unique
      # only do this for new dockets
      if new_record?
         if person_id? && docket_no?
            if Docket.find(:all, :conditions => "docket_no like '#{docket_no}' AND person_id = #{person_id}").reject{|d| d.new_record?}.size > 0
               self.errors.add(:docket_no, "already exists for Person #{person_id}!")
               return false
            end
         end
      end
      return true
   end
   
   # this ensures that all associated charges get saved when a 
   # docket is updated.
	def save_charges
      da_charges.each do |c|
         c.save(false)
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      intvals = ['person_id']
      query.each do |key, value|
         if key == 'name'
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.firstname like '#{nameparts[:first]}'"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.middlename like '#{nameparts[:middle]}'"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.lastname like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.suffix like '#{nameparts[:suffix]}'"
            end
         elsif key == 'court_date'
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "courts.court_date = '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == 'judge_id'
            unless condition.empty?
               condition << " and "
            end
            condition << "courts.judge_id = '#{value}'"
         elsif key == 'docket_type_id'
            unless condition.empty?
               condition << " and "
            end
            condition << "courts.docket_type_id = '#{value}'"
         else
            unless condition.empty?
               condition << " and "
            end
            if intvals.include?(key)
               condition << "dockets.#{key} = '#{value}'"
            else
               condition << "dockets.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
