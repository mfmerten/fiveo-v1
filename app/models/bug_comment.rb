# == Schema Information
#
# Table name: bug_comments
#
#  id         :integer(4)      not null, primary key
#  bug_id     :integer(4)
#  user_id    :integer(4)
#  comments   :text
#  created_by :integer(4)      default(1)
#  updated_by :integer(4)      default(1)
#  created_at :datetime
#  updated_at :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# BugComment Model
# These are comments added to bug reports.
class BugComment < ActiveRecord::Base
   belongs_to :bug
   belongs_to :user
   belongs_to :creator, :class_name => 'User', :foreign_key => "created_by"
   belongs_to :updater, :class_name => 'User', :foreign_key => "updated_by"
   
   before_validation :check_bug_id
   after_save :message_users, :check_creator
   
   validates_presence_of :comments
   validates_user :user_id
   
   # a short description of the model used by the DatabaseController index
   # view.
   def self.desc
      "Comments for bug reports"
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_bug]
   end
   
   private
   
   # refuses to save if bug_id is not valid
   def check_bug_id
      if bug_id?
         if bug.nil?
            self.errors.add_to_base("Cannot Save! Invalid Bug Report ID. Contact An Administrator!")
            return false
         end
      else
         self.errors.add_to_base("Cannot Save! Cannot Determine Bug Report ID. Contact An Administrator!")
         return false
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # This generates the "Bug#2 has a new comment" user messages sent out
   # whenever anyone adds a new comment.
   def message_users
      receivers = [bug.updated_by, bug.reporter_id, bug.admin_id, bug.assigned_id, bug.created_by]
      unless bug.watchers.empty?
         receivers << bug.watchers.collect{|w| w.id}
      end
      receivers.uniq.each do |r|
         unless updater.no_self_bug_messages? && r == updated_by
            if r == bug.updated_by
               reason = "the one who updated (or created) the bug"
            elsif r == bug.reporter_id
               reason = "the bug reporter"
            elsif r == bug.admin_id
               reason = "the bug administrator"
            elsif r == bug.assigned_id
               reason = "assigned to the bug"
            else
               reason = "watching this bug"
            end
            txt = "You are receiving this message because you are #{reason}.</p>" +
            "<p>Bug##{bug.id} (#{bug.summary}) has a new comment added by #{user.name} on #{format_date(created_at)}:</p>" +
            "<blockquote><p>#{comments}</p></blockquote><p>__________"
            Message.create(:receiver_id => r, :sender_id => updated_by, :subject => "Bug##{bug.id} Has A New Comment", :body => txt)
         end
      end
   end
end
