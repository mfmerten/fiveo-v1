# == Schema Information
#
# Table name: contacts
#
#  id         :integer(4)      not null, primary key
#  title      :string(255)
#  unit       :string(255)
#  street     :string(255)
#  city       :string(255)
#  state_id   :integer(4)
#  zip        :string(255)
#  pri_email  :string(255)
#  alt_email  :string(255)
#  web_site   :string(255)
#  notes      :string(255)
#  created_at :datetime
#  updated_at :datetime
#  officer    :boolean(1)
#  street2    :string(255)
#  created_by :integer(4)      default(1)
#  updated_by :integer(4)      default(1)
#  badge_no   :string(255)
#  active     :boolean(1)      default(FALSE)
#  detective  :boolean(1)      default(FALSE)
#  physician  :boolean(1)      default(FALSE)
#  pharmacy   :boolean(1)      default(FALSE)
#  legacy     :boolean(1)      default(FALSE)
#  business   :boolean(1)      default(FALSE)
#  fullname   :string(255)
#  agency_id  :integer(4)
#  printable  :boolean(1)      default(TRUE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Contact Model
# These are the business contacts (rolodex) for the application.  Includes
# employee records.
class Contact < ActiveRecord::Base
   has_many :phones, :dependent => :destroy
   belongs_to :state, :class_name => 'Option', :foreign_key => 'state_id'
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   has_one :user
   has_many :warrants, :class_name => "Warrant", :foreign_key => 'jurisdiction_id'
   belongs_to :agency, :class_name => "Contact", :foreign_key => 'agency_id'
   has_many :employees, :class_name => "Contact", :foreign_key => 'agency_id'

   # this sets up sphinx indices for this model
   define_index do
      indexes :fullname
      indexes :title
      indexes :unit
      indexes agency.fullname, :as => :aname
      indexes pri_email
      indexes alt_email
      indexes notes
      indexes street
      indexes street2
      indexes city
      
      has updated_at
   end

   before_validation :city_state_by_zip, :check_addresses, :fix_name
   after_save :check_creator
   after_update :save_phones
   before_destroy :check_for_user

   validates_presence_of :fullname
   validates_zip :zip, :allow_blank => true
   validates_email :pri_email, :alt_email, :allow_blank => true
   validates_url :web_site, :allow_blank => true
   validates_option :state_id, :option_type => 'State', :allow_nil => true
   validates_contact :agency_id, :allow_nil => true

   # a short description used by the DatabaseController index view.
   def self.desc
      "Contacts (rolodex)"
   end
   
   # returns true if the specified option id is in use in any of the option
   # type fields in any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(state_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns all active officer names and id numbers in an array of arrays
   # suitable for use in a select control.
   def self.officers_for_select(allow_inactive=false)
      if allow_inactive == true
         find(:all, :order => 'fullname, unit', :conditions => {:officer => true, :business => false}).collect{|o| ["#{o.fullname} #{o.unit}",o.id]}
      else
         find(:all, :order => 'fullname, unit', :conditions => {:officer => true, :active => true, :business => false}).collect{|o| ["#{o.fullname} #{o.unit}",o.id]}
      end
   end
   
   def self.physicians_for_select(allow_inactive=false)
      if allow_inactive == true
         find(:all, :order => 'fullname', :conditions => {:physician => true, :business => false}).collect{|o| [o.fullname, o.id]}
      else
         find(:all, :order => 'fullname', :conditions => {:physician => true, :active => true, :business => false}).collect{|o| [o.fullname, o.id]}
      end
   end
   
   # returns all active detectives (officer) names and id numbers in an array of arrays
   # suitable for use in a select control.
   def self.detectives_for_select(allow_inactive=false)
      if allow_inactive == true
         find(:all, :order => 'fullname, unit', :conditions => {:detective => true, :officer => true, :business => false}).collect{|o| ["#{o.fullname} #{o.unit}",o.id]}
      else
         find(:all, :order => 'fullname, unit', :conditions => {:detective => true, :officer => true, :active => true, :business => false}).collect{|o| ["#{o.fullname} #{o.unit}",o.id]}
      end
   end
   
   # returns all active detectives that have linked user accounts in Fiveo - this
   # is a special for the Investigation Edit view.
   def self.detective_users_for_select
      find(:all, :order => 'fullname, unit', :conditions => {:detective => true, :officer => true, :active => true, :business => false}).reject{|d| d.user.nil? }.collect{|o| ["#{o.fullname} #{o.unit}",o.user.id]}
   end
   
   # returns all active (individual) names and id numbers in an array of arrays
   # suitable for use in a select control.
   def self.contacts_for_select(allow_inactive=false)
      if allow_inactive == true
         find(:all, :order => 'fullname, unit', :conditions => {:business => false}).collect{|o| ["#{o.fullname} #{o.unit}", o.id]}
      else
         find(:all, :order => 'fullname, unit', :conditions => {:active => true, :business => false}).collect{|o| ["#{o.fullname} #{o.unit}", o.id]}
      end
   end
   
   # returns all active contact names and id numbers in an array of arrays
   # suitable for use in a select control.
   def self.contacts_and_companies_for_select(allow_inactive=false)
      if allow_inactive == true
         find(:all, :order => "fullname, unit").collect{|o| ["#{o.fullname} #{o.unit}", o.id]}
      else
         find(:all, :order => "fullname, unit", :conditions => {:active => true}).collect{|o| ["#{o.fullname} #{o.unit}", o.id]}
      end
   end
   
   # returns all active law enforcement agency names and id numbers in an array
   # of arrays suitable for use in a select control.
   def self.agencies_for_select(allow_inactive=false)
      if allow_inactive == true
         find(:all, :order => 'fullname', :conditions => {:officer => true, :business => true}).collect{|a| [a.fullname, a.id]}
      else
         find(:all, :order => 'fullname', :conditions => {:officer => true, :active => true, :business => true}).collect{|a| [a.fullname, a.id]}
      end
   end
   
   def self.pharmacies_for_select(allow_inactive=false)
      if allow_inactive == true
         find(:all, :order => 'fullname', :conditions => {:pharmacy => true, :business => true}).collect{|a| [a.fullname, a.id]}
      else
         find(:all, :order => 'fullname', :conditions => {:pharmacy => true, :active => true, :business => true}).collect{|a| [a.fullname, a.id]}
      end
   end
   
   # returns all active agency names and id numbers in an array of arrays
   # suitable for use in a select control.
   def self.companies_for_select(allow_inactive=false,limit_printable=false)
      if allow_inactive == true
         if limit_printable == true
            find(:all, :order => 'fullname', :conditions => {:business => true, :printable => true}).collect{|a| [a.fullname, a.id]}
         else
            find(:all, :order => 'fullname', :conditions => {:business => true}).collect{|a| [a.fullname, a.id]}
         end
      else
         if limit_printable == true
            find(:all, :order => 'fullname', :conditions => {:active => true, :business => true, :printable => true}).collect{|a| [a.fullname, a.id]}
         else
            find(:all, :order => 'fullname', :conditions => {:active => true, :business => true}).collect{|a| [a.fullname, a.id]}
         end
      end
   end
   
   def self.company_names_for_select(allow_inactive=false)
      if allow_inactive == true
         find(:all, :order => 'fullname', :conditions => {:business => true}).collect{|a| [a.fullname, a.fullname]}
      else
         find(:all, :order => 'fullname', :conditions => {:active => true, :business => true}).collect{|a| [a.fullname, a.fullname]}
      end
   end
   
   # returns base permissions required to View records here
   def self.base_perms
      Perm[:view_contact]
   end

   # accepts an array of new contact phone attributes and builds records for
   # them using the phones association.
   def new_phone_attributes=(phone_attributes)
      phone_attributes.each do |attributes|
         unless attributes[:value].blank?
            phones.build(attributes)
         end
      end
   end

   # accepts an array of contact phone attributes and compares them to the
   # existing phones association.  If phone does not exist in the attribute
   # list, it will be destroyed, otherwise the existing record is updated.
   def existing_phone_attributes=(phone_attributes)
      phones.reject(&:new_record?).each do |phone|
         attributes = phone_attributes[phone.id.to_s]
         if attributes
            phone.attributes = attributes
         else
            phones.delete(phone)
         end
      end
   end
   
   # returns the first line of the contact address (street)
   def address_line1
      street? ? street : ""
   end
   
   # returns the second line of the contact address (other)
   def address_line2
      street2? ? street2 : ""
   end

   # returns the third line of the contact address (city, ST ZIP)
   def address_line3
      name = ""
      # don't return anything if only state exists
      if city? || zip?
         if city?
            name << city
         end
         if state_id?
            unless name.empty?
               name << ", "
            end
            name << state.short_name
         end
         if zip?
            unless name.empty?
               name << " "
            end
            name << zip
         end
      end
      name
   end
   
   # This calls the swap_contact method in all applicable models with the
   # the two contact ID numbers to be merged.  Each model is responsible
   # for swapping the contact ID numbers in any of its records that uses it.
   def merge_contact(master_id, user_id)
      return [-1,["Contact"]] unless master_id.to_i > 0 && user_id.to_i > 0
      models = []
      Dir.chdir(File.join(RAILS_ROOT, "app/models")) do 
         models = Dir["*.rb"]
      end
      swapped_count = 0
      model_errors = []
      models.each do |m|
         class_name = m.sub(/\.rb$/,'').camelize
         if class_name.constantize.respond_to?('swap_contact')
            recs = class_name.constantize.swap_contact(id, master_id.to_i,user_id.to_i)
            if recs == -1
               model_errors.push(class_name)
            else
               swapped_count += recs
            end
         end
      end
      return [swapped_count, model_errors]
   end
   
   private
   
   # uses the name parser to put names in last, first middle order unless
   # business is checked
   def fix_name
      unless business?
         parts = parse_name(fullname)
         self.fullname = unparse_name(parts,true)
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # ensures that contact phones are saved when the contact is updated.
   def save_phones
      phones.each do |phone|
         phone.save(false)
      end
   end
   
   # if zip is supplied, and either city or state is blank, look up both by
   # zip code.
   def city_state_by_zip
      if zip?
         # if either city or state is blank, replace both by lookup
         unless city? && state_id?
            citystate = Zip.find_city_state_by_zip(zip.slice(0,5))
            self.city = citystate[0]
            self.state_id = Option.lookup("State", citystate[1], 'short_name')
         end
      end
   end

   # verifies that the entered city and state match the entered zip code.
   def check_addresses
      retval = true
      if city? && state_id?
         mycity = city.split.each{|w| w.capitalize}.join(" ")
         mystate = state.short_name
         if zip?
            myzip = zip.slice(0,5)
         else
            myzip = ""
         end
         unless Zip.verify_zip(mycity,mystate,myzip)
            zips = Zip.find_all_by_city_and_state(mycity,mystate)
            self.errors.add(:base, "Error: ZIP code (#{myzip}) does not match #{mycity}, #{mystate}... valid ZIPs are #{zips}!")
            retval = false
         end
      end
      return retval
   end

   # disallow destroying a user profile contact record
   def check_for_user
      return user.nil?
   end

   # accepts a search query hash and converts it into an SQL partial
   # suitable for use in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ['state_id', 'officer', 'active', 'business', 'detective', 'physician', 'pharmacy']
      condition = ""
      query.each do |key, value|
         if key == 'fullname' || (key == 'simple' && value.to_i == 0)
            unless condition.blank?
               condition << " and "
            end
            condition << "contacts.fullname like '%#{value}%'"
         elsif key == 'simple' && value.to_i > 0
            unless condition.blank?
               condition << " and "
            end
            condition << "contacts.id = '#{value.to_i}'"
         elsif key == "email"
            unless condition.blank?
               condition << " and "
            end
            condition << "(contacts.pri_email like '%#{query[:email]}%' or contacts.alt_email like '%#{query[:email]}%')"
         else
            unless condition.blank?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "contacts.#{key} = '#{value}'"
            else
               condition << "contacts.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
