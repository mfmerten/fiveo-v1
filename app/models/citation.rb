# == Schema Information
#
# Table name: citations
#
#  id            :integer(4)      not null, primary key
#  citation_date :date
#  person_id     :integer(4)
#  officer_id    :integer(4)
#  officer       :string(255)
#  officer_unit  :string(255)
#  officer_badge :string(255)
#  ticket_no     :string(255)
#  paid_date     :date
#  recalled_date :date
#  created_by    :integer(4)      default(1)
#  updated_by    :integer(4)      default(1)
#  created_at    :datetime
#  updated_at    :datetime
#  offense_id    :integer(4)
#  citation_time :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Citation Model
# this tracks parish tickets (citations)
class Citation < ActiveRecord::Base
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   belongs_to :person
   has_many :charges, :dependent => :destroy
   belongs_to :offense
   
   # this sets up sphinx indices for this model
   define_index do
      indexes citation_date
      indexes officer
      indexes officer_unit
      indexes [person.lastname, person.firstname, person.middlename, person.suffix], :as => :name
      indexes charges.charge, :as => :chgs
      indexes ticket_no
      
      has updated_at
   end
   
   before_validation :fix_times, :lookup_officer
   after_update :save_charges
   after_save :check_creator
   
   validates_presence_of :ticket_no, :officer
   validates_date :citation_date
   validates_time :citation_time
   validates_date :paid_date, :recalled_date, :allow_nil => true
   validates_person :person_id
   validates_contact :officer_id, :allow_nil => true
   validates_offense :offense_id, :allow_nil => true
   
   # returns a short description for the DatabaseController index view
   def self.desc
      'Misdemeanor Citations Table'
   end
   
   # returns base perms required to view these records
   def self.base_perms
      Perm[:view_citation]
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # accepts an array of charge attributes from the CitationsController
	# citation edit view and creates charge records linked to this citation.
   def new_charge_attributes=(charge_attributes)
      charge_attributes.each do |attributes|
         unless attributes[:charge].blank?
            charges.build(attributes)
         end
      end
   end

   # accepts an array of charge attributes from the CitationsController
   # citation edit view. It cycles through all existing charges for the
   # citation. If the charge is included in the charge attributes, the
   # charge is updated with the attributes, otherwise the charge
   # record is destroyed.
   def existing_charge_attributes=(charge_attributes)
      charges.reject(&:new_record?).each do |charge|
         attributes = charge_attributes[charge.id.to_s]
         if attributes
            attributes[:updated_by] = updated_by
            charge.attributes = attributes
         else
            charges.delete(charge)
         end
      end
   end
   
   # This returns a single text string listing all charges and their counts
   # produced by cycling through all associated charge records.
   def charge_summary
      text = ''
      charges.each do |c|
         unless text.blank?
            text << ", "
         end
         text << "#{c.charge}(#{c.count})"
      end
      if text.blank?
         return "No Charges"
      end
      return text
   end
   
   private
   
   # this ensures that all associated charges get saved when a 
   # citation is updated.
	def save_charges
      charges.each do |c|
         c.save(false)
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
      
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officer
      if officer_id?
         if ofc = Contact.find_by_id(officer_id)
            self.officer = ofc.fullname
            self.officer_badge = ofc.badge_no
            self.officer_unit = ofc.unit
         end
      end
   end
   
   # accepts a query hash and converts it to an SQL partial suitable in a where
   # clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      query.each do |key, value|
         if key == 'name'
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.firstname like '#{nameparts[:first]}'"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.middlename like '#{nameparts[:middle]}'"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.lastname like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.suffix like '#{nameparts[:suffix]}'"
            end
         elsif key == "citation_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "citations.citation_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "citation_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "citations.citation_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "paid_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "citations.paid_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "paid_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "citations.paid_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "recalled_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "citations.recalled_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "recalled_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "citations.recalled_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "person_id"
            unless condition.blank?
               condition << " and "
            end
            condition << "citations.person_id = '#{value}'"
         elsif key == 'offense_id'
            unless condition.blank?
               condition << ' and '
            end
            condition << "citations.offense_id = '#{value}'"
         elsif key == "charge"
            unless condition.blank?
               condition << " and "
            end
            condition << "charges.charge like '%#{value}%'"
         else
            unless condition.empty?
               condition << " and "
            end
            condition << "citations.#{key} like '%#{value}%'"
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
   
   # calls the valid_time() method for each time type field (valid_time
   # inserts the : between hours and minutes if its omitted).
   def fix_times
      self.citation_time = valid_time(citation_time)
   end
end
