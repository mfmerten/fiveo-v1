# == Schema Information
#
# Table name: invest_vehicles
#
#  id                 :integer(4)      not null, primary key
#  investigation_id   :integer(4)
#  invest_criminal_id :integer(4)
#  invest_crime_id    :integer(4)
#  invest_report_id   :integer(4)
#  private            :boolean(1)      default(TRUE)
#  owner_id           :integer(4)
#  make               :string(255)
#  model              :string(255)
#  vin                :string(255)
#  license            :string(255)
#  license_state_id   :integer(4)
#  color              :string(255)
#  tires              :string(255)
#  model_year         :string(255)
#  vehicle_type_id    :integer(4)
#  description        :text
#  remarks            :text
#  created_by         :integer(4)      default(1)
#  updated_by         :integer(4)      default(1)
#  created_at         :datetime
#  updated_at         :datetime
#  owned_by           :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Investigation Vehicles
# these are records of known vehicles for a Detective Investigation
#
# Note the following:
#       owner_id is the Person ID for the registered owner of the vehicle and is
#            accessed through the :registered_owner association
#       owned_by is the User ID for the Detective that currently owns the record
#            and is accessed through the :owner association. (this is done the same
#            way for all investigation records and is the mechanism used to enforce
#            privacy)
# Hopefully this will not cause too much confusion.
#
class InvestVehicle < ActiveRecord::Base
   has_many :photos, :class_name => 'InvestPhoto'
   belongs_to :investigation
   belongs_to :crime, :class_name => 'InvestCrime', :foreign_key => 'invest_crime_id'
   belongs_to :criminal, :class_name => 'InvestCriminal', :foreign_key => 'invest_criminal_id'
   belongs_to :report, :class_name => 'InvestReport', :foreign_key => 'invest_report_id'
   belongs_to :registered_owner, :class_name => 'Person', :foreign_key => 'owner_id'
   belongs_to :license_state, :class_name => 'Option', :foreign_key => 'license_state_id'
   belongs_to :vehicle_type, :class_name => 'Option', :foreign_key => 'vehicle_type_id'
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :owner, :class_name => 'User', :foreign_key => 'owned_by'
   
   # this sets up sphinx indices for this model
   define_index do
      indexes [registered_owner.lastname, registered_owner.firstname, registered_owner.middlename, registered_owner.suffix], :as => :person
      indexes make
      indexes model
      indexes vin
      indexes license
      indexes color
      indexes tires
      indexes vehicle_type.long_name, :as => :vtype
      indexes description
      indexes remarks

      where 'private != 1'
      
      has updated_at
   end
   
   after_save :check_creator
   
   validates_person :owner_id, :allow_nil => true
   validates_option :license_state_id, :option_type => 'State', :allow_nil => true
   validates_option :vehicle_type_id, :option_type => 'Vehicle Type', :allow_nil => true
   
   def self.desc
      'Vehicles for Detective Investigation'
   end
   
   # returns base perms required to view investigations
   def self.base_perms
      Perm[:view_investigation]
   end
   
   # returns true if the specified option id is in use for any option type
   # fields in any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(vehicle_type_id = '#{oid}' or license_state_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(owner_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["owner_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the value of creator for investigation,
   # or the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:created_by, 1)
         else
            self.update_attribute(:created_by, investigation.created_by)
         end
      end
      if !updated_by? || updater.nil?
         if investigation.nil? || investigation.updater.nil?
            self.update_attribute(:updated_by, 1)
         else
            self.update_attribute(:updated_by, investigation.updated_by)
         end
      end
      if !owned_by? || owner.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:owned_by, 1)
         else
            self.update_attribute(:owned_by, investigation.created_by)
         end
      end
      return true
   end
end
