# == Schema Information
#
# Table name: invest_crimes
#
#  id                     :integer(4)      not null, primary key
#  investigation_id       :integer(4)
#  private                :boolean(1)      default(TRUE)
#  motive                 :string(255)
#  location               :string(255)
#  occupied               :boolean(1)
#  entry_gained           :string(255)
#  weapons_used           :string(255)
#  victim_relationship_id :integer(4)
#  facts_prior            :text
#  facts_after            :text
#  targets                :text
#  security               :string(255)
#  impersonated           :string(255)
#  actions_taken          :text
#  threats_made           :text
#  details                :text
#  remarks                :text
#  created_by             :integer(4)      default(1)
#  updated_by             :integer(4)      default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  owned_by               :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Investigation Crimes
# these record the MO for individual crimes that are involved in a Detective
# investigation
class InvestCrime < ActiveRecord::Base
   has_many :reports, :class_name => 'InvestReport', :dependent => :destroy
   has_many :photos, :class_name => 'InvestPhoto', :dependent => :destroy
   has_many :vehicles, :class_name => 'InvestVehicle', :dependent => :destroy
   belongs_to :investigation
   belongs_to :victim_relationship, :class_name => 'Option', :foreign_key => 'victim_relationship_id'
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :owner, :class_name => 'User', :foreign_key => 'owned_by'
   has_many :criminals, :class_name => 'InvestCriminal', :dependent => :destroy
   has_and_belongs_to_many :sources, :class_name => 'Person', :join_table => 'invest_crime_sources'
   has_many :cases, :class_name => 'InvestCase', :dependent => :destroy
   has_many :evidences, :dependent => :destroy
   has_many :warrants, :dependent => :destroy
   has_and_belongs_to_many :victims, :class_name => 'Person', :join_table => 'crime_victims'
   has_and_belongs_to_many :witnesses, :class_name => 'Person', :join_table => 'crime_witnesses'
   has_many :stolens, :dependent => :destroy
   
   # this sets up sphinx indices for this model
   define_index do
      indexes motive
      indexes location
      indexes entry_gained
      indexes weapons_used
      indexes facts_prior
      indexes facts_after
      indexes targets
      indexes security
      indexes impersonated
      indexes actions_taken
      indexes threats_made
      indexes details
      indexes remarks
      indexes victim_relationship.long_name, :as => :relationship
      
      where 'private != 1'
      
      has updated_at
   end
   
   after_save :check_creator
   before_destroy :handle_dependencies
   
   validates_presence_of :location, :details
   validates_option :victim_relationship_id, :option_type => 'Associate Type', :allow_nil => true

   def self.desc
      "Crimes related to Detective Investigation"
   end
   
   # returns base perms required to view investigations
   def self.base_perms
      Perm[:view_investigation]
   end
   
   # returns true if the specified option id is in use for any option type
   # fields in any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(victim_relationship_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      count(:include => [:sources, :victims, :witnesses], :conditions => "people.id = '#{p_id}'") > 0
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      # catch the HABTM tables for people
      # TODO: find a better way to do this
      rec_count += count(:all, :include => :victims, :conditions => "crime_victims.person_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE crime_victims SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      rec_count += count(:all, :include => :sources, :conditions => "invest_crime_sources.person_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE invest_crime_sources SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      rec_count += count(:all, :include => :witnesses, :conditions => "crime_witnesses.person_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE crime_witnesses SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      return rec_count
   end
   
   def charge_summary
      "N/A"
   end
   
   def assigned_victims=(selected_victims)
      self.victims.clear
      selected_victims.each do |v|
         self.victims << Person.find_by_id(v) unless v.empty?
      end
   end
   
   def assigned_witnesses=(selected_witnesses)
      self.witnesses.clear
      selected_witnesses.each do |w|
         self.witnesses << Person.find_by_id(w) unless w.empty?
      end
   end
   
   def assigned_sources=(selected_sources)
      self.sources.clear
      selected_sources.each do |s|
         self.sources << Person.find_by_id(s) unless s.empty?
      end
   end
   
   private
   
   # removes dependencies before destroy
   def handle_dependencies
      self.sources.clear
      self.officers.clear
      self.victims.clear
      self.witnesses.clear
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the value of creator for investigation,
   # or the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:created_by, 1)
         else
            self.update_attribute(:created_by, investigation.created_by)
         end
      end
      if !updated_by? || updater.nil?
         if investigation.nil? || investigation.updater.nil?
            self.update_attribute(:updated_by, 1)
         else
            self.update_attribute(:updated_by, investigation.updated_by)
         end
      end
      if !owned_by? || owner.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:owned_by, 1)
         else
            self.update_attribute(:owned_by, investigation.created_by)
         end
      end
      return true
   end
end
