# == Schema Information
#
# Table name: paper_customers
#
#  id            :integer(4)      not null, primary key
#  name          :string(255)
#  street        :string(255)
#  city          :string(255)
#  state_id      :integer(4)
#  zip           :string(255)
#  phone         :string(255)
#  attention     :string(255)
#  remarks       :text
#  disabled      :boolean(1)      default(FALSE)
#  created_by    :integer(4)      default(1)
#  updated_by    :integer(4)      default(1)
#  created_at    :datetime
#  updated_at    :datetime
#  balance_due   :decimal(12, 2)  default(0.0)
#  noservice_fee :decimal(12, 2)  default(0.0)
#  mileage_fee   :decimal(12, 2)  default(0.0)
#  fax           :string(255)
#  street2       :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Paper Customer Model
#
# These are "bill to" customers for paper service
class PaperCustomer < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :state, :class_name => 'Option', :foreign_key => 'state_id'
   has_many :papers
   has_many :paper_payments
   
   before_validation :fix_fees
   after_save :check_creator
   before_destroy :check_dependencies
   
   validates_presence_of :name, :street, :city
   validates_numericality_of :noservice_fee, :mileage_fee
   validates_zip :zip
   validates_phone :phone, :fax, :allow_blank => true
   validates_option :state_id, :option_type => 'State'

   # a short description of the model for use by the DatabaseController index
   # view.
   def self.desc
      "Paper Service Customers"
   end
   
   # returns true if any record uses the specified option id in any of
   # the option type fields.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(state_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns customer names and id numbers for select controls - only customers
   # with billable papers will be included.
   def self.billable_names_for_select
      find(:all, :include => :papers, :order => 'paper_customers.name', :conditions => "papers.billable is true and papers.invoice_datetime is null and (papers.served_date is not null or papers.returned_date is not null)").collect{|c| [c.name, c.id]}
   end
   
   def self.payable_names_for_select
      find(:all, :include => :papers, :order => 'paper_customers.name', :conditions => "papers.invoice_datetime is not null and papers.paid_date is null").collect{|c| [c.name, c.id]}
   end
   
   # returns customer names and id number for select controls
   def self.names_for_select(allow_disabled=false)
      if allow_disabled == true
         find(:all, :order => 'name').collect{|c| [c.name,c.id]}
      else
         find(:all, :order => 'name', :conditions => "disabled != 1").collect{|c| [c.name, c.id]}
      end
   end
   
   # returns 5 most commonly used customers formatted for a select control
   def self.common_names_for_select(allow_disabled=false)
      if allow_disabled == true
         Paper.find_by_sql('select paper_customer_id, count(paper_customer_id) as used, paper_customers.name as name from papers left outer join paper_customers on paper_customers.id = papers.paper_customer_id group by paper_customer_id order by used desc limit 5').collect{|p| [p.name,p.paper_customer_id]}.sort{|a,b| a[0] <=> b[0]}
      else
         Paper.find_by_sql('select paper_customer_id, count(paper_customer_id) as used, paper_customers.name as name from papers left outer join paper_customers on paper_customers.id = papers.paper_customer_id where paper_customers.disabled is false group by paper_customer_id order by used desc limit 5').collect{|p| [p.name,p.paper_customer_id]}.sort{|a,b| a[0] <=> b[0]}
      end
   end
   
   # returns base permissions required to View these records
   def self.base_perms
      Perm[:view_civil]
   end
   
   def self.update_noservice_fee(price)
      update_all("noservice_fee = #{BigDecimal.new("#{price}",2)}")
   end
   
   def self.update_mileage_fee(price)
      update_all("mileage_fee = #{BigDecimal.new("#{price}",2)}")
   end
   
   def update_noservice_fee(price)
      self.update_attribute(:noservice_fee, BigDecimal.new("#{price}",2))
   end
   
   def update_mileage_fee(price)
      self.update_attribute(:mileage_fee, BigDecimal.new("#{price}",2))
   end
   
   # returns line one of the address (street)
   def address_line1
      street
   end
   
   def address_line2
      street2
   end

   # returns line two of the address (city, ST ZIP)
   def address_line3
      txt = ""
      if city?
         txt << city
      end
      if state_id?
         unless txt.empty?
            txt << ", "
         end
         txt << "#{state.short_name}"
      end
      if zip?
         unless txt.empty?
            txt << " "
         end
         txt << zip
      end
      return txt
   end
   
   # returns array of payable papers suitable for a select control
   def papers_for_select
      papers.reject{|p| p.paid_date? || !p.invoice_datetime? || (p.invoice_total? && p.invoice_total == 0)}.collect{|p| [p.id.to_s, p.id]}
   end
   
   def past_days(days = 30)
      return [] unless days.is_a?(Integer) && days >= 0
      date = Date.today - days.days
      PaperPayment.find(:all, :conditions => ['paper_customer_id = ? and transaction_date >= ?',id,date])
   end
   
   # returns commissary balance at beginning of date specified
   def balance_due_on(edate = Date.today)
      return nil unless edate.is_a?(Date)
      if edate == Date.today
         return balance_due
      else
         PaperPayment.sum('charge - payment', :conditions => ['paper_customer_id = ? and transaction_date <= ?', id, edate]).to_d
      end
   end
   
   def can_be_deleted
      papers.empty? && paper_payments.empty?
   end
   
   private
   
   def fix_fees
      if noservice_fee.blank?
         self.noservice_fee = SiteConfig.noservice_fee
      end
      if mileage_fee.blank?
         self.mileage_fee = SiteConfig.mileage_fee
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # disallows destroy (returns false) if papers or payments exist
   def check_dependencies
      papers.empty? && paper_payments.empty?
   end
      
   
   # accepts a search query hash and converts it into an SQL partial
   # suitable for use in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ['state_id', 'disabled']
      condition = ""
      query.each do |key, value|
         if key == 'simple'
            unless condition.blank?
               condition << " and "
            end
            if value.to_i > 0
               condition << "(paper_customers.id = '#{value}' OR paper_customers.name like '%#{value}%')"
            else
               condition << "paper_customers.name like '%#{value}%'"
            end
         else
            unless condition.blank?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "paper_customers.#{key} = '#{value}'"
            else
               condition << "paper_customers.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
