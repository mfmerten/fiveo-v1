# == Schema Information
#
# Table name: options
#
#  id             :integer(4)      not null, primary key
#  option_type_id :integer(4)
#  long_name      :string(255)
#  short_name     :string(255)
#  disabled       :boolean(1)      default(FALSE)
#  created_at     :datetime
#  updated_at     :datetime
#  created_by     :integer(4)      default(1)
#  updated_by     :integer(4)      default(1)
#  permanent      :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Option Model
# These are the select control options for the application.
class Option < ActiveRecord::Base
   belongs_to :option_type
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'

   before_validation :validate_option_type
   after_save :check_creator

   validates_presence_of :long_name

   # a short description for the DatabaseController index view.
   def self.desc
      "Form Options"
   end

   # returns the first option value that matches the specified conditions:
   #
   #  type: this specifies the Option Type id
   #  value: this is the value to search for
   #  method: this specifies whether value is long_name or short_name
   #  field: this specifies which field the return value is obtained from
   #  allow_disabled: if true, allows lookup to match on disabled options.
   def self.lookup(type, value, method = 'long_name', field = 'id', allow_disabled = false)
      return nil if type.nil? || value.nil? || type.blank? || value.blank?
      if field.nil?
         field = 'id'
      end
      if method.nil?
         method = 'long_name'
      end
      return nil unless ['long_name','short_name'].include?(method)
      return nil unless ['long_name','short_name','id'].include?(field)
      if option_type = OptionType.find_by_name(type)
         if allow_disabled
            if option = find(:first, :conditions => ["#{method} = ? and option_type_id = ?", value, option_type.id])
               return option.send("#{field}")
            end
         else
            if option = find(:first, :conditions => ["#{method} = ? and option_type_id = ? and disabled != 1", value, option_type.id])
               return option.send("#{field}")
            end
         end
      end
      return nil
   end

   # base permissions required to View options
   def self.base_perms
      Perm[:view_option]
   end

   # This queries the uses_option() method in all application models to check
   # whether the option id in question is in use anywhere.  (very expensive
   # databae call!)  Used to decide whether to allow option deletion.
   def in_use?
      models = []
      Dir.chdir(File.join(RAILS_ROOT, "app/models")) do 
         models = Dir["*.rb"]
      end
      models.each do |m|
         class_name = m.sub(/\.rb$/,'').camelize
         if class_name.constantize.respond_to?('uses_option')
            return true if class_name.constantize.uses_option(id)
         end
      end
      return false
   end
   
   # returns the option type name
   def type_name
      option_type.name
   end

   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
      
   # makes sure option type is present and valid
   def validate_option_type
      if option_type_id?
         if option_type.nil?
            self.errors.add(:option_type_id, "is not a valid Option Type ID!")
            return false
         end
      else
         self.errors.add(:option_type_id, "is required!")
         return false
      end
      return true
   end

   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      query.each do |key, value|
         unless condition.empty?
            condition << " and "
         end
         if key == "option_type_id"
            condition << "options.#{key} = '#{value}'"
         else
            condition << "options.#{key} like '%#{value}%'"
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
