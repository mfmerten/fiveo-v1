# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# this is used to populate a form for adjusting time on a sentence hold
# it does not persist (no database table, regardless of the schema shown above)
# and is used to simplify initialization and validation of form data
class HoldAdjustment < ActiveRecord::Base
   include Tableless
   
   column :plus_hours_total, :integer, 0
   column :plus_days_total, :integer, 0
   column :plus_months_total, :integer, 0
   column :plus_years_total, :integer, 0
   column :minus_hours_total, :integer, 0
   column :minus_days_total, :integer, 0
   column :minus_months_total, :integer, 0
   column :minus_years_total, :integer, 0
   column :plus_hours_served, :integer, 0
   column :plus_days_served, :integer, 0
   column :plus_months_served, :integer, 0
   column :plus_years_served, :integer, 0
   column :minus_hours_served, :integer, 0
   column :minus_days_served, :integer, 0
   column :minus_months_served, :integer, 0
   column :minus_years_served, :integer, 0
   column :plus_hours_goodtime, :integer, 0
   column :plus_days_goodtime, :integer, 0
   column :plus_months_goodtime, :integer, 0
   column :plus_years_goodtime, :integer, 0
   column :minus_hours_goodtime, :integer, 0
   column :minus_days_goodtime, :integer, 0
   column :minus_months_goodtime, :integer, 0
   column :minus_years_goodtime, :integer, 0
   column :plus_hours_time_served, :integer, 0
   column :plus_days_time_served, :integer, 0
   column :plus_months_time_served, :integer, 0
   column :plus_years_time_served, :integer, 0
   column :minus_hours_time_served, :integer, 0
   column :minus_days_time_served, :integer, 0
   column :minus_months_time_served, :integer, 0
   column :minus_years_time_served, :integer, 0

   validates_numericality_of :plus_hours_total, :plus_days_total, :plus_months_total,
      :plus_years_total, :minus_hours_total, :minus_days_total, :minus_months_total,
      :minus_years_total, :plus_hours_served, :plus_days_served, :plus_months_served,
      :plus_years_served, :minus_hours_served, :minus_days_served, :minus_months_served,
      :minus_years_served, :plus_hours_goodtime, :plus_days_goodtime, :plus_months_goodtime,
      :plus_years_goodtime, :minus_hours_goodtime, :minus_days_goodtime, :minus_months_goodtime,
      :minus_years_goodtime, :plus_hours_time_served, :plus_days_time_served, :plus_months_time_served,
      :plus_years_time_served, :minus_hours_time_served, :minus_days_time_served, :minus_months_time_served,
      :minus_years_time_served,
      :integer_only => true, :greater_than_or_equal_to => 0
   
   def hours_total_adjustment
      (plus_hours_total - minus_hours_total) +
      ((plus_days_total - minus_days_total) * 24) +
      ((plus_months_total - minus_months_total) * 30 * 24) +
      ((plus_years_total - minus_years_total) * 365 * 30 * 24)
   end
   
   def hours_served_adjustment
      (plus_hours_served - minus_hours_served) +
      ((plus_days_served - minus_days_served) * 24) +
      ((plus_months_served - minus_months_served) * 30 * 24) +
      ((plus_years_served - minus_years_served) * 365 * 30 * 24)
   end
   
   def hours_goodtime_adjustment
      (plus_hours_goodtime - minus_hours_goodtime) +
      ((plus_days_goodtime - minus_days_goodtime) * 24) +
      ((plus_months_goodtime - minus_months_goodtime) * 30 * 24) +
      ((plus_years_goodtime - minus_years_goodtime) * 365 * 30 * 24)
   end
   
   def hours_time_served_adjustment
      (plus_hours_time_served - minus_hours_time_served) +
      ((plus_days_time_served - minus_days_time_served) * 24) +
      ((plus_months_time_served - minus_months_time_served) * 30 * 24) +
      ((plus_years_time_served - minus_years_time_served) * 365 * 30 * 24)
   end
end
   
