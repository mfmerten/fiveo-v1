# == Schema Information
#
# Table name: absents
#
#  id                   :integer(4)      not null, primary key
#  leave_date           :date
#  leave_time           :string(255)
#  leave_type_id        :integer(4)
#  return_date          :date
#  return_time          :string(255)
#  escort_officer       :string(255)
#  escort_officer_unit  :string(255)
#  created_by           :integer(4)
#  updated_by           :integer(4)
#  created_at           :datetime
#  updated_at           :datetime
#  facility_id          :integer(4)
#  escort_officer_id    :integer(4)
#  escort_officer_badge :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Absent Model
# Absent records document periods of time where inmates leave the confines of
# their detention facility without leaving custody.
class Absent < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :leave_type, :class_name => 'Option', :foreign_key => 'leave_type_id'
   belongs_to :facility, :class_name => 'Option', :foreign_key => 'facility_id'
   has_and_belongs_to_many :prisoners, :class_name => "Booking", :join_table => 'absents_bookings', :foreign_key => 'absent_id', :association_foreign_key => 'booking_id'
   
   before_validation :fix_times, :lookup_officers
   after_save :check_creator
   
   validates_presence_of :escort_officer
   validates_date :leave_date, :return_date, :allow_nil => true
   validates_time :leave_time, :return_time, :allow_nil => true
   validates_presence_of :leave_time, :if => :leave_date?
   validates_presence_of :return_time, :if => :return_date?
   validates_presence_of :leave_date, :if => :leave_time?
   validates_presence_of :return_date, :if => :return_time?
   validates_contact :escort_officer_id, :allow_nil => true
   validates_option :leave_type_id, :option_type => 'Absent Type'
   validates_option :facility_id, :option_type => 'Facility'
   
   # Provides a short description for the database controller
   def self.desc
      "Prisoner Absentee Records"
   end
   
   # Returns true if specified option id is used by any option in any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(leave_type_id = "#{oid}" OR facility_id = "#{oid}")
      count(:conditions => cond) > 0
   end
   
   # Returns the base permissions required to View objects in this model
   def self.base_perms
      Perm[:view_booking]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["escort_officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # Updates the prisoner association with all records selected on the edit
   # view.
   def assigned_prisoners=(selected_prisoners)
      self.prisoners.clear
      selected_prisoners.each do |p|
         unless p.empty?
            if pb = Booking.find_by_id(p)
               self.prisoners << pb
            end
         end
      end
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if escort_officer_id?
         if ofc = Contact.find_by_id(escort_officer_id)
            self.escort_officer = ofc.fullname
            self.escort_officer_badge = ofc.badge_no
            self.escort_officer_unit = ofc.unit
         end
      end
   end
   
   # accepts a query hash and converts it to an SQL partial suitable in a where
   # clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ["facility_id"]
      # build an sql WHERE string
      condition = ""
      query.each do |key, value|
         if key == "person_id"
            unless condition.empty?
               condition << " and "
            end
            condition << "bookings.person_id = '#{value}'"
         elsif key == "booking_id"
            unless condition.empty?
               condition << " and "
            end
            condition << "bookings.id = '#{value}'"
         elsif key == "leave_from"
            # discard date if invalid
            if myval = valid_date(value)
               # must be ok now
               unless condition.empty?
                  condition << " and "
               end
               condition << "absents.leave_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "leave_to"
            # convert date to y-m-d for mysql use or discard it if invalid
            if myval = valid_date(value)
               # must be ok now
               unless condition.empty?
                  condition << " and "
               end
               condition << "absents.leave_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else
            unless condition.empty?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "absents.#{key} = '#{value}'"
            else
               condition << "absents.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
   
   # uses valid_time() convert time values entered as 4 digits only to
   # 5 characters (including : separator) prior to validation.
   def fix_times
      self.leave_time = valid_time(leave_time)
      self.return_time = valid_time(return_time)
   end
end
