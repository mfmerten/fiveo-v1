# == Schema Information
#
# Table name: commissary_items
#
#  id          :integer(4)      not null, primary key
#  description :string(255)
#  price       :decimal(12, 2)  default(0.0)
#  active      :boolean(1)      default(TRUE)
#  created_by  :integer(4)      default(1)
#  updated_by  :integer(4)      default(1)
#  created_at  :datetime
#  updated_at  :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Commissary Item Model
# These are sales items for the jail commissary
class CommissaryItem < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   has_and_belongs_to_many :facilities, :class_name => "Option", :join_table => 'commissary_facilities'
   
   before_validation :fix_description
   after_save :check_creator
   
   validates_presence_of :description
   validates_numericality_of :price
   
   # a short description for the DatabaseController index view
   def self.desc
      'Commissary Sales Items'
   end
   
   # returns base permissions necessary to View these records
   def self.base_perms
      Perm[:view_commissary]
   end
   
   # return all facility names (optionally short names)
   def facility_names(short=false)
      return "" if facilities.empty?
      if short
         facilities.collect{|f| f.short_name }.join(', ')
      else
         facilities.collect{|f| f.long_name }.join(', ')
      end
   end
   
   # Accepts a list of facilities from the CommissaryItemsController edit view
   # and associates them with the commissary item after clearing any previous
   # associations.
   def assigned_facilities=(selected_facilities)
      self.facilities.clear
      selected_facilities.each do |f|
         self.facilities << Option.find_by_id(f) unless f.empty?
      end
   end
   
   private
   
   # fixes the case on description text
   def fix_description
      if description?
         self.description = description.titleize
      end
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # accepts a search query hash and converts it into an SQL partial
   # suitable for use in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      query.each do |key, value|
         if key == 'facility_id'
            unless condition.blank?
               condition << " and "
            end
            condition << "options.id = '#{value}'"
         else
            unless condition.blank?
               condition << " and "
            end
            condition << "commissary_items.#{key} like '%#{value}%'"
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
