# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Message Mailer Model
# This is used to send user messages to the associated email address
# in the user contact when present.
class MessageMailer < ActionMailer::Base
   def get_email
      email = nil
      if !SiteConfig.site_email.blank?
         email = SiteConfig.site_email
      elsif File::exists?("site_mail_name.txt")
         if efile = File.new("site_mail_name.txt")
            email = efile.gets.chomp
            efile.close
         end
      end
      return 'monitor@midlaketech.com' if email.nil?
      return email
   end

   def message_email(site, message)
      recipients  message.receiver.contact.pri_email
      from        "#{site} <#{get_email}>"
      headers     "Reply-to" => "#{get_email}"
      subject     "You have a new message."
      body        :message => message
   end
   
   def bug_email(sub, message)
      recipients "bugs@midlaketech.com"
      from       "#{get_email}"
      headers    "Reply-to" => "#{get_email}"
      content_type  'text/plain'
      subject    sub
      body       :message => message
   end
end
