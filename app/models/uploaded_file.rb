# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# UploadedFile model
# this was written to support uploading CSV files for processing
# legacy agency data.
class UploadedFile
   # writes the file to the public/incoming directory after
   # sanitizing the file name
   def self.save(upload)
      return false if upload['datafile'].nil?
      name = sanitize_filename(upload['datafile'].original_filename)
      directory = "public/incoming"
      path = File.join(directory, name)
      File.open(path, "wb") {|f| f.write(upload['datafile'].read)}
   end
   
   def self.save_image(fname, datafile)
      return false if datafile.nil?
      original_name = sanitize_filename(datafile.original_filename)
      name = fname + '.png'
      original_directory = "public/incoming"
      directory = "public/system"
      original_path = File.join(original_directory, original_name)
      path = File.join(directory, name)
      File.open(original_path, 'wb'){|f| f.write(datafile.read)}
      `convert #{original_path} #{path}`
      `rm #{original_path}`
   end

   private
   
   # sanitizes the filename by stripping any path and converting
   # problematic characters to underscores
   def self.sanitize_filename(file_name)
      just_filename = File.basename(file_name)
      just_filename.sub(/[^\w\.\-]/, '_')
   end

end