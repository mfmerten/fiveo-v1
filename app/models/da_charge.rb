# == Schema Information
#
# Table name: da_charges
#
#  id            :integer(4)      not null, primary key
#  docket_id     :integer(4)
#  charge_id     :integer(4)
#  count         :integer(4)      default(1)
#  charge        :string(255)
#  dropped_by_da :boolean(1)      default(FALSE)
#  processed     :boolean(1)      default(FALSE)
#  created_by    :integer(4)      default(1)
#  updated_by    :integer(4)      default(1)
#  created_at    :datetime
#  updated_at    :datetime
#  arrest_id     :integer(4)
#  info_only     :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class DaCharge < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :docket
   belongs_to :arrest_charge, :class_name => 'Charge', :foreign_key => 'charge_id'
   has_one :sentence, :dependent => :destroy
   belongs_to :arrest
   has_one :probation
   
   after_save :check_dropped, :check_info_only
   
   validates_presence_of :count, :charge
   
   def self.desc
      'Charges for Dockets'
   end
   
   # Calculate the "Time Served" to-date for a da_charge by examining attached arrest
   def time_served_in_hours
      return Arrest.hours_served(arrest_id) unless arrest.nil?
      return Arrest.hours_served(arrest_charge.d_arrest_id) unless arrest_charge.nil? || arrest_charge.district_arrest.nil?
      # everything else (including citations) have 0 time served
      return 0
   end
   
   private
   
   def check_info_only
      if info_only?
         unless processed?
            self.update_attribute(:processed, true)
         end
      end
   end
   
   def check_dropped
      if dropped_by_da?
         unless processed?
            self.update_attribute(:processed, true)
         end
      end
   end
end
