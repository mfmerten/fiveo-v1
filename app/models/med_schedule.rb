# == Schema Information
#
# Table name: med_schedules
#
#  id            :integer(4)      not null, primary key
#  medication_id :integer(4)
#  dose          :string(255)
#  time          :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class MedSchedule < ActiveRecord::Base
   belongs_to :medication
   
   before_validation :fix_time
   
   validates_presence_of :dose
   validates_time :time
   
   def self.desc
      'Medicine Schedules'
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_person]
   end
   
   private
   
   def fix_time
      self.time = valid_time(time)
   end
end
