# == Schema Information
#
# Table name: docs
#
#  id              :integer(4)      not null, primary key
#  transfer_date   :date
#  transfer_time   :string(255)
#  transfer_id     :integer(4)
#  sentence_days   :integer(4)      default(0)
#  sentence_months :integer(4)      default(0)
#  sentence_years  :integer(4)      default(0)
#  to_serve_days   :integer(4)      default(0)
#  to_serve_months :integer(4)      default(0)
#  to_serve_years  :integer(4)      default(0)
#  conviction      :string(255)
#  billable        :boolean(1)      default(FALSE)
#  bill_from_date  :date
#  remarks         :text
#  created_by      :integer(4)      default(1)
#  updated_by      :integer(4)      default(1)
#  created_at      :datetime
#  updated_at      :datetime
#  hold_id         :integer(4)
#  sentence_id     :integer(4)
#  doc_number      :string(255)
#  booking_id      :integer(4)
#  person_id       :integer(4)
#  revocation_id   :integer(4)
#  sentence_hours  :integer(4)      default(0)
#  to_serve_hours  :integer(4)      default(0)
#  problems        :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class Doc < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :transfer
   belongs_to :hold, :dependent => :destroy
   belongs_to :booking
   belongs_to :person
   belongs_to :sentence
   belongs_to :revocation
   
   attr_accessor :force
   
   # this sets up sphinx indices for this model
   define_index do
      indexes [person.lastname, person.firstname, person.middlename], :as => :name
      indexes conviction
      indexes remarks
      indexes doc_number
      
      has updated_at
   end
   before_validation :fix_times, :validate_booking
   before_save :fix_numbers, :process
   after_update :check_creator, :update_person
   
   validates_numericality_of :to_serve_hours, :to_serve_days, :to_serve_months, :to_serve_years, :sentence_hours, :sentence_days, :sentence_months, :sentence_years, :only_integer => true, :allow_nil => true
   
   def self.desc
      'DOC inmate records'
   end
   
   # returns base permissions needed to View records
   def self.base_perms
      Perm[:view_booking]
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   # Processes all unprocessed records.
   def self.process_docs
      find(:all, :conditions => {:processed => false}).each{|d| d.process_doc}
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def sentence_string
      sentence_to_string(sentence_hours, sentence_days, sentence_months, sentence_years)
   end
   
   def to_serve_string
      sentence_to_string(to_serve_hours, to_serve_days, to_serve_months, to_serve_years)
   end
   
   private
   
   # makes sure all sentence type numbers are not nil
   # for some reason, the database defaults are not working reliably
   def fix_numbers
      ['to_serve','sentence'].each do |t|
         ['hours','days','months','years'].each do |s|
            if send("#{t}_#{s}").blank?
               self.send("#{t}_#{s}=",0)
            end
         end
      end
      return true
   end
   
   # processes doc record by adding or updating a Serving Sentence hold
   def process
      # new processing attempt, clear out any previous processing problems
      self.problems = ""
      
      # must have a valid booking
      if booking.nil?
         self.problems << "[Invalid Booking]"
      else
         # calculate hours to serve
         to_serve = 0
         if to_serve_hours? || to_serve_days? || to_serve_months? || to_serve_years?
            to_serve = sentence_to_hours(to_serve_hours, to_serve_days, to_serve_months, to_serve_years)
         else
            self.problems << "[No To-Serve Time]"
         end
         
         # if this generated from a sentence or revocation, check for consecutives - cannot
         # process this record if consecutives specified but not themselves processed
         consecs = []
         if !sentence.nil?
            c = sentence.consecutive_holds
            if c.nil?
               self.problems << "[Sentence Consecutive to Unprocessed Sentences]"
            else
               consecs.concat(c)
            end
         elsif !revocation.nil?
            c = revocation.consecutive_holds
            if c.nil?
               self.problems << "[Revocation Consecutive to Unprocessed Sentences]"
            else
               consecs.concat(c)
            end
         end

         # get appropriate information from source of doc record
         if !transfer.nil?
            hdate = transfer.transfer_date
            htime = transfer.transfer_time
            hby_id = transfer.creator.contact_id
            hby = transfer.creator.name
            hby_unit = transfer.creator.unit
            hby_badge = transfer.creator.badge
         elsif !sentence.nil?
            hdate = sentence.court.court_date
            htime = "12:00"
            hby_id = sentence.creator.contact_id
            hby = sentence.creator.name
            hby_unit = sentence.creator.unit
            hby_badge = sentence.creator.badge
         elsif !revocation.nil?
            hdate = revocation.revocation_date
            htime = "12:00"
            hby_id = revocation.creator.contact_id
            hby = revocation.creator.name
            hby_unit = revocation.creator.unit
            hby_badge = revocation.creator.badge
         else
            hdate = booking.booking_date
            htime = booking.booking_time
            hby_id = booking.booking_officer_id
            hby = booking.booking_officer
            hby_unit = booking.booking_officer_unit
            hby_badge = booking.booking_officer_badge
         end

         # add or update a sentence hold
         if hold.nil? && booking.release_date? && force != true
            self.problems << "[Booking Released: Cannot add hold]"
         else
            h = hold
            if h.nil?
               # no existing hold
               h = Hold.new
            end
            h.booking_id = booking_id
            h.hold_date = hdate
            h.hold_time = htime
            h.hold_type_id = Option.lookup("Hold Type","Serving Sentence",nil,nil,true)
            h.hold_by = hby
            h.hold_by_id = hby_id
            h.hold_by_unit = hby_unit
            h.hold_by_badge = hby_badge
            h.remarks = "Added Automatically By DOC Processing."
            h.created_by = updated_by unless h.created_by?
            h.updated_by = updated_by
            h.doc = true
            unless h.hours_served? && h.hours_served > 0
               h.hours_served = booking.hours_served_since(get_datetime(hdate, htime))
            end
            h.hours_total = to_serve
            h.hours_goodtime = 0
            h.hours_time_served = 0
            unless h.datetime_counted?
               h.datetime_counted = DateTime.now
            end
            h.doc_billable = billable
            h.bill_from_date = bill_from_date
            h.transfer_id = transfer_id
            h.sentence_id = sentence_id
            h.revocation_id = revocation_id
            if force == true && booking.release_date?
               h.cleared_date = h.hold_date
               h.cleared_time = h.hold_time
               h.cleared_by_id = h.hold_by_id
               h.cleared_by = h.hold_by
               h.cleared_by_unit = h.hold_by_unit
               h.cleared_by_badge = h.hold_by_badge
               h.cleared_because_id = Option.lookup("Release Type","Automatic Release",nil,nil,true)
               h.explanation = "Inmate Released Prior To Sentence Processing because of Time Served or Transfer."
               h.remarks = "Added and Cleared Automatically by DOC Processing."
            end
            if h.valid?
               h.save
               h.blockers << consecs
               # add hold id to doc record
               self.hold_id = h.id
            else
               self.problems << '[Unable to Create Sentence Hold]'
               self.problems << "(#{h.errors.full_messages.join(', ')})"
               success = false
            end
            
            # check for doc_number
            if doc_number?
               if person.doc_number != doc_number
                  person.update_attribute(:doc_number, doc_number)
               end
            else
               if person.doc_number?
                  doc_number = person.doc_number
               end
            end
         end
      end
      self.force = false
      return true
   end
   
   # doc records cannot be saved unless the attached booking is valid
   # for valid booking:
   # 1. if doc does not have attached hold, booking must exist and must be open
   # 2. if doc has attached hold, booking must exist, but may be released
   def validate_booking
      if booking_id?
         if booking.nil?
            self.errors.add(:booking_id, 'must be a valid booking id!')
            return false
         end
         # check that person is same
         if person_id? && booking.person_id? && person_id != booking.person_id
            self.errors.add(:booking_id, 'is not for this person!')
            return false
         end
         # if no hold is attached, verify that booking is open
         if hold.nil? && booking.release_date? && force != true
            self.errors.add(:booking_id, 'must be a currently open booking for this person!')
            return false
         end
      else
         self.errors.add(:booking_id, 'is required!')
         return false
      end
      return true
   end
   
   # updates doc_number for people
   def update_person
      if doc_number?
         unless person.nil?
            unless doc_number == person.doc_number
               self.person.update_attribute(:doc_number, doc_number)
            end
         end
      else
         unless person.nil?
            if person.doc_number?
               self.update_attribute(:doc_number, person.doc_number)
            end
         end
      end
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # calls the valid_time() method for each time type field (valid_time
   # inserts the : between hours and minutes if its omitted).
   def fix_times
      self.transfer_time = valid_time(transfer_time)
   end

   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      intvals = ['id', 'booking_id', 'person_id','billable']
      query.each do |key, value|
         if key == 'person_name'
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.firstname like '#{nameparts[:first]}'"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.middlename like '#{nameparts[:middle]}'"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.lastname like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.suffix like '#{nameparts[:suffix]}'"
            end
         elsif key == "transfer_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "docs.transfer_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "transfer_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "docs.transfer_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else
            unless condition.empty?
               condition << " and "
            end
            if intvals.include?(key)
               condition << "docs.#{key} = '#{value}'"
            else
               condition << "docs.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
