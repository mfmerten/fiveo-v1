# == Schema Information
#
# Table name: bugs
#
#  id              :integer(4)      not null, primary key
#  reporter_id     :integer(4)
#  reported_date   :date
#  reported_time   :string(255)
#  assigned_id     :integer(4)
#  admin_id        :integer(4)
#  status_id       :integer(4)
#  resolution_id   :integer(4)
#  resolution_date :date
#  resolution_time :string(255)
#  priority_id     :integer(4)
#  summary         :string(255)
#  details         :text
#  created_by      :integer(4)      default(1)
#  updated_by      :integer(4)      default(1)
#  created_at      :datetime
#  updated_at      :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Bug Model
# This is the bug tracker database.
class Bug < ActiveRecord::Base
   belongs_to :reporter, :class_name => 'User', :foreign_key => 'reporter_id'
   belongs_to :assigned, :class_name => 'User', :foreign_key => 'assigned_id'
   belongs_to :admin, :class_name => 'User', :foreign_key => 'admin_id'
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   has_and_belongs_to_many :watchers, :class_name => 'User', :join_table => 'watchers'
   has_many :bug_comments, :order => 'created_at DESC', :dependent => :destroy
   belongs_to :status, :class_name => 'Option', :foreign_key => 'status_id'
   belongs_to :resolution, :class_name => 'Option', :foreign_key => 'resolution_id'
   belongs_to :priority, :class_name => 'Option', :foreign_key => 'priority_id'
   
   attr_accessor :without_messages
   
   before_validation :fix_times, :validate_options
   after_save :message_users, :unless => :without_messages
   after_save :check_creator
#   after_create :send_to_midlaketech
   
   validates_presence_of :summary, :details
   validates_date :reported_date
   validates_time :reported_time
   validates_date :resolution_date, :allow_nil => true
   validates_time :resolution_time, :allow_nil => true
   validates_presence_of :resolution_time, :if => :resolution_date?
   validates_presence_of :resolution_date, :if => :resolution_time?
   validates_presence_of :resolution_date, :if => :resolution_id?
   validates_option :status_id, :option_type => 'Bug Status'
   validates_option :priority_id, :option_type => 'Bug Priority'
   validates_option :resolution_id, :option_type => 'Bug Resolution', :allow_nil => true
   validates_presence_of :resolution_id, :if => :resolution_date?
   validates_user :reporter_id, :allow_locked => true
   validates_user :assigned_id, :admin_id
   
   # a short description of the model used by the DatabaseController index 
   # view.
   def self.desc
      "Bug Report Database"
   end
   
   # returns true if any bug record uses the specified option id in any of its
   # option type fields.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(status_id = '#{oid}' OR resolution_id = '#{oid}' OR priority_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns the base perms required to View these objects
   def self.base_perms
      Perm[:view_bug]
   end
   
   # returns the number of items to be reviewed by supplied user_id
   def self.review_count(user_id)
      return 0 unless user = User.find_by_id(user_id)
      if user.last_bug.nil? || !user.last_bug.is_a?(Integer)
         user.update_attribute(:last_bug, 0)
      end
      count(:conditions => ['id > ?',user.last_bug])
   end

   # if bug has watchers, format a comma-separated list for display
   def watch_list
      watchers.collect{|w| w.fullname}.join(", ")
   end
   
   def assigned_watchers=(selected_watchers)
      self.watchers.clear
      selected_watchers.each do |u|
         if user = User.find_by_id(u)
            self.watchers << user
         end
      end
   end
   
   private
   
   # validates option fields
   def validate_options
      retval = true
      # check sanity of options
      unless status.nil?
         if status.long_name != 'Resolved' && (resolution_date? || resolution_time? || !(resolution.nil? || resolution.long_name == 'Not Satisfied'))
            self.errors.add(:status_id, "must be set to Resolved if a resolution and resolution date/time is entered!")
            retval = false
            unless resolution.nil? || resolution.long_name == 'Not Satisfied'
               self.errors.add(:resolution_id, "can only be blank or 'Not Satisfied' if the bug is not resolved!")
               retval = false
            end
            if resolution_date?
               self.errors.add(:resolution_date, "must not be entered if the bug is not resolved!")
               retval = false
            end
            if resolution_time?
               self.errors.add(:resolution_time, "must not be entered if the bug is not resolved!")
               retval = false
            end
         end
         if status.long_name == 'Resolved' && (!resolution_date? || !resolution_time? || resolution.nil? || resolution.long_name == 'Not Satisfied')
            self.errors.add(:status_id, "cannot be set to Resolved unless a resolution and a resolution date/time is entered!")
            retval = false
            unless resolution_id?
               self.errors.add(:resolution_id, "cannot be blank or 'Not Satisfied' if the bug is resolved!")
               retval = false
            end
            unless resolution_date?
               self.errors.add(:resolution_date, "must be entered if the bug is resolved!")
               retval = false
            end
            unless resolution_time?                  
               self.errors.add(:resolution_time, "must be entered if the bug is resolved!")
               retval = false
            end
         end
      end
      return retval
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # This generates the "Bug#2 has been updated" user message to all users
   # associated with the bug.
   def message_users
      receivers = [updated_by, reporter_id, admin_id, assigned_id, created_by]
      unless watchers.empty?
         receivers << watchers.collect{|w| w.id}
      end
      receivers.uniq.each do |r|
         unless updater.no_self_bug_messages? && r == updated_by
            if r == updated_by
               reason = "the one who updated (or created) the bug"
            elsif r == reporter_id
               reason = "the bug reporter"
            elsif r == admin_id
               reason = "the bug supervisor"
            elsif r == assigned_id
               reason = "assigned to the bug"
            else
               reason = "watching this bug"
            end
            txt = "You are receiving this message because you are #{reason}.</p>" +
            "<p>Bug##{id} has been changed. The updated version is:</p>" +
            "<table><tr><th style='width:150px;'>Reported by:</th><td>#{reporter.name}</td></tr>" +
            "<tr><th>Reported At:</th><td>#{format_date(reported_date, reported_time)}</td></tr>" +
            "<tr><th>Summary:</th><td>#{summary}</td></tr>" +
            "<tr><th>Bug Administrator:</th><td>#{admin.name}</td></tr>" +
            "<tr><th>Priority:</th><td>#{priority.long_name}</td></tr>" +
            "<tr><th>Assigned To:</th><td>#{assigned.name}</td></tr>" +
            "<tr><th>Details Of Problem:</th><td>#{details}</td></tr>" +
            "<tr><th>Status:</th><td>#{status.long_name}</td></tr>"
            unless resolution.nil?
               txt << "<tr><th>Resolution:</th><td>#{resolution.long_name}</td></tr>"
            end
            if resolution_date?
               txt << "<tr><th>Resolved At:</th><td>#{format_date(resolution_date, resolution_time)}</td></tr>"
            end
            txt << "</table><p>__________"
            Message.create(:receiver_id => r, :sender_id => updated_by, :subject => "Bug##{id} Has Changed", :body => txt)
         end
      end
      return true
   end
   
   # this sends an email to bugs@midlaketech.com for new bugs
   def send_to_midlaketech
      txt = "
Project: FiveO
Category: Support
Priority: Normal
Tracker: Bug

Reported By #{reporter.name}
Admin By #{admin.name}
#{SiteConfig.agency_short} Priority #{priority.long_name}
Server Version #{ApplicationHelper.version_no}

#{details} 
"
      MessageMailer.deliver_bug_email(summary, txt)
      return true
   end
   
   # accepts a query hash and converts it to an SQL partial suitable in a where
   # clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ["id", "reporter_id", "assigned_id", "admin_id", "status_id", "resolution_id", "priority_id"]
      # build an sql WHERE string
      condition = ""
      query.each do |key, value|
         if key == "id"
            unless condition.empty?
               condition << " and "
            end
            condition << "bugs.id = '#{value}'"
         elsif key == 'text'
            unless condition.empty?
               condition << " and "
            end
            condition << "(bugs.summary like '%#{value}%' or bugs.details like '%#{value}%')"
         elsif key == "reported_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bugs.reported_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "reported_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bugs.reported_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "resolution_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bugs.resolution_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "resolution_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bugs.resolution_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else
            unless condition.empty?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "bugs.#{key} = '#{value}'"
            else
               condition << "bugs.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
   
   # this is a helper that calls the valid_time() method with each time
   # field for a bug to insert any missing colons (:) into the time values
   def fix_times
      self.reported_time = valid_time(reported_time)
      self.resolution_time = valid_time(resolution_time)
   end
end
