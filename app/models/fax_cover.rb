# == Schema Information
#
# Table name: fax_covers
#
#  id             :integer(4)      not null, primary key
#  name           :string(255)
#  from_name      :string(255)
#  from_fax       :string(255)
#  from_phone     :string(255)
#  include_notice :boolean(1)      default(TRUE)
#  notice_title   :string(255)
#  notice_body    :text
#  created_by     :integer(4)      default(1)
#  updated_by     :integer(4)      default(1)
#  created_at     :datetime
#  updated_at     :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# FAX Cover Model
# this allows users to create customized fax cover sheets using the standard
# agency letterhead.
class FaxCover < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   
   after_save :check_creator
   
   validates_presence_of :name, :from_name
   validates_uniqueness_of :name
   validates_phone :from_fax, :from_phone
   
   # a short description of the model for the DatabaseController index view
   def self.desc
      "Custom Fax Cover Pages"
   end
   
   # Returns the base permissions required to View these records
   def self.base_perms
      nil
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
end
