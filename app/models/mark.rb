# == Schema Information
#
# Table name: marks
#
#  id          :integer(4)      not null, primary key
#  person_id   :integer(4)
#  location_id :integer(4)
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  created_by  :integer(4)      default(1)
#  updated_by  :integer(4)      default(1)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Mark Model
# These are the distinguishing marks for the People Model
class Mark < ActiveRecord::Base
    belongs_to :person
    belongs_to :location, :class_name => "Option", :foreign_key => 'location_id'
    belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
    belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
    
    after_save :check_creator
    
    validates_presence_of :description
    validates_option :location_id, :option_type => 'Anatomy'

    # a short description of the model for the DatabaseController index view.
    def self.desc
       "Distinguishing Marks for People"
    end
    
    # base permissions required to View these objects
    def self.base_perms
       Perm[:view_investigation]
    end
    
    # returns true if the specified option id is in use for any option type
    # fields in any record.
    def self.uses_option(oid)
       oid ||= 0
       oid = oid.to_i
       cond = %(location_id = '#{oid}')
       count(:conditions => cond) > 0
    end
    
    # returns true if person is in use (in any capacity)
    def self.uses_person(p_id)
       # never return true because these are dependents of Person
       false
    end
    
    def self.swap_person(bad_id, good_id, user_id)
       return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
       rec_count = 0
       ["person_id"].each do |field|
          rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
       end
       return rec_count
    end
    
    private

    # checks that if created_by or updated_by are nil, 0 or non-integer after
    # the record is saved, they get set to the default value of 1 (admin).
    def check_creator
       if !created_by? || creator.nil?
          self.update_attribute(:created_by, 1)
       end
       if !updated_by? || updater.nil?
          self.update_attribute(:updated_by, 1)
       end
       return true
    end
end
