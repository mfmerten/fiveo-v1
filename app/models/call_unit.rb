# == Schema Information
#
# Table name: call_units
#
#  id            :integer(4)      not null, primary key
#  call_id       :integer(4)
#  officer_unit  :string(255)
#  officer       :string(255)
#  created_by    :integer(4)
#  updated_by    :integer(4)
#  created_at    :datetime
#  updated_at    :datetime
#  officer_id    :integer(4)
#  officer_badge :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Call Unit Model
# these are records of dispatched units for dispatcher call sheets.
class CallUnit < ActiveRecord::Base
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   belongs_to :call
   
   before_validation :lookup_officers
   after_save :check_creator
   
   validates_presence_of :officer
   
   # a short description of the model used by the DatabaseController index
   # view.
   def self.desc
      "Dispatched Units for Call sheets."
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_call]
   end
      
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end

   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
      
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if officer_id?
         if ofc = Contact.find_by_id(officer_id)
            self.officer = ofc.fullname
            self.officer_badge = ofc.badge_no
            self.officer_unit = ofc.unit
         end
      end
   end
end
