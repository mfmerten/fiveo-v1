# == Schema Information
#
# Table name: bondsmen
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  street     :string(255)
#  city       :string(255)
#  state_id   :integer(4)
#  zip        :string(255)
#  company    :string(255)
#  disabled   :boolean(1)
#  created_by :integer(4)
#  updated_by :integer(4)
#  created_at :datetime
#  updated_at :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Bondsman Model
# Bondsmen have their own model separate from Contacts because the 
# bondsman name may be a business name which would confuse the name parsing
# routine that contacts uses. Bondsmen also don't need all of the fields
# that contacts has.
class Bondsman < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   has_many :bonds
   belongs_to :state, :class_name => 'Option', :foreign_key => 'state_id'
   has_many :bondsman_phones, :dependent => :destroy, :include => :label
   
   after_save :check_creator
   after_update :save_phones
   
   validates_presence_of :name, :street, :city
   validates_zip :zip
   validates_option :state_id, :option_type => "State"
   
   # a short description for use by the DatabaseController index view.
   def self.desc
      'Bondsmen Database'
   end
   
   # returns true if any record uses the indicated option id in any of its
   # option type fields.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(state_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns base perms necessary to View these objects
   def self.base_perms
      Perm[:view_option]
   end
   
   # accepts an array of bondsman_phone attributes from the BondsmenController
	# edit view and creates bondsman_phone records linked to this bondsman.
   def new_phone_attributes=(phone_attributes)
      phone_attributes.each do |attributes|
         unless attributes[:value].blank?
            bondsman_phones.build(attributes)
         end
      end
   end

   # accepts an array of bondsman_phone attributes from the BondsmenController
   # edit view. It cycles through all existing bondsman_phones for the
   # bondsman. If the bondsman_phone is included in the phone attributes, the
   # bondsman_phone is updated with the attributes, otherwise the
   # bondsman_phone record is destroyed.
   def existing_phone_attributes=(phone_attributes)
      bondsman_phones.reject(&:new_record?).each do |phone|
         attributes = phone_attributes[phone.id.to_s]
         if attributes
            phone.attributes = attributes
         else
            bondsman_phones.delete(phone)
         end
      end
   end
   
   # returns line 1 of the bondsman address (street)
   def address1
      street
   end
   
   # returns line 2 of the bondsman address (city, st ZIP)
   def address2
      ad = ""
      unless city.blank?
         ad << city
      end
      unless state.nil?
         unless ad.blank?
            ad << ", "
         end
         ad << state.short_name
      end
      unless zip.blank?
         unless ad.blank?
            ad << " "
         end
         ad << zip
      end
      return ad
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end

   # ensures that all new bondsman_phone records are saved after a record
   # update
   def save_phones
      bondsman_phones.each do |phone|
         phone.save(false)
      end
   end
   
   # accepts a query hash and turns it into a partial SQL query string
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ["state_id"]
      condition = ""
      query.each do |key, value|
         unless condition.empty?
            condition << " and "
         end
         if intfields.include?(key)
            condition << "bondsmen.#{key} = '#{value}'"
         else
            condition << "bondsmen.#{key} like '%#{value}%'"
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
