# == Schema Information
#
# Table name: evidences
#
#  id                          :integer(4)      not null, primary key
#  offense_id                  :integer(4)
#  evidence_number             :string(255)
#  case_no                     :string(255)
#  description                 :text
#  owner_id                    :integer(4)
#  remarks                     :text
#  created_at                  :datetime
#  updated_at                  :datetime
#  created_by                  :integer(4)      default(1)
#  updated_by                  :integer(4)      default(1)
#  evidence_photo_file_name    :string(255)
#  evidence_photo_content_type :string(255)
#  evidence_photo_file_size    :integer(4)
#  evidence_photo_updated_at   :datetime
#  investigation_id            :integer(4)
#  private                     :boolean(1)      default(FALSE)
#  invest_crime_id             :integer(4)
#  owned_by                    :integer(4)
#  legacy                      :boolean(1)      default(FALSE)
#  dna_profile                 :string(255)
#  fingerprint_class           :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Evidence Model
# These records document evidence collected at crime scenes.
class Evidence < ActiveRecord::Base
   belongs_to :evidence_owner, :class_name => "Person", :foreign_key => 'owner_id'
   has_many :evidence_locations, :dependent => :destroy
   belongs_to :offense
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   belongs_to :investigation
   belongs_to :crime, :class_name => 'InvestCrime', :foreign_key => 'invest_crime_id'
   belongs_to :owner, :class_name => 'User', :foreign_key => 'owned_by'
   
   # this sets up sphinx indices for this model
   define_index do
      indexes evidence_number
      indexes case_no
      indexes description
      indexes dna_profile
      indexes fingerprint_class
      indexes [evidence_owner.lastname, evidence_owner.firstname, evidence_owner.middlename, evidence_owner.suffix], :as => :name
      indexes remarks
      
      has updated_at
   end
   
   # A photo of the evidence
   has_attached_file :evidence_photo,
      :url => "/system/:attachment/:id/:style/:basename.:extension",
      :path => ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
      :default_url => "/images/no_image_:style.jpg",
      :styles => {
         :thumb => "80x80",
         :original => "600x600",
         :medium => "300x300",
         :small => "150x150" }
   
   before_validation :adjust_case_no
   after_save :check_evidence_no, :check_creator
   
   validates_presence_of :description
   validates_person :owner_id, :allow_nil => true
   validates_offense :offense_id, :allow_nil => true
   
   # a short description for the DatabaseController index view.
   def self.desc
      "Evidence Records"
   end
   
   # returns base permissions required to View these records
   def self.base_perms
      Perm[:view_evidence]
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(owner_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["owner_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # returns the last known location of the evidence.
   def location
      evidence_locations.empty? ? "" : evidence_locations.last.location_to
   end
   
   # sets up an initial storage location for the evidence.
   def initial=(loc)
      init_loc = self.evidence_locations.build(
         :location_from => "Initial Collection",
         :created_by => updated_by,
         :updated_by => updated_by,
         :officer => creator.name,
         :officer_unit => creator.unit,
         :officer_badge => creator.badge,
         :location_to => loc[:location_to],
         :remarks => loc[:remarks]
      )
      init_loc.storage_date, init_loc.storage_time = get_date_and_time
   end
   
   private
   
   # strips all characters from case_no except for numbers, letters, and -
   def adjust_case_no
      self.case_no = fix_case(case_no)
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
      
   # ensures that location records get saved when the evidence is updated.
   def save_location
      locations.each do |l|
         l.save(false)
      end
   end
   
   # checks the entered evidence number and adds one using the id as a string
   # if none exists
   def check_evidence_no
      unless evidence_number?
         self.update_attribute(:evidence_number, id.to_s.rjust(6,'0'))
      end
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      intvals = ["evidence_no","offense_id","case_no","owner_id"]
      query.each do |key, value|
         unless condition.empty?
             condition << " and "
         end
         if intvals.include?(key)
            condition << "evidences.#{key} = '#{value}'"
         else
            condition << "evidences.#{key} like '%#{value}%'"
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
