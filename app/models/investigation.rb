# == Schema Information
#
# Table name: investigations
#
#  id                 :integer(4)      not null, primary key
#  investigation_date :date
#  investigation_time :string(255)
#  detective          :string(255)
#  detective_id       :integer(4)
#  detective_badge    :string(255)
#  detective_unit     :string(255)
#  private            :boolean(1)      default(TRUE)
#  status_id          :integer(4)
#  details            :text
#  remarks            :text
#  created_by         :integer(4)      default(1)
#  updated_by         :integer(4)      default(1)
#  created_at         :datetime
#  updated_at         :datetime
#  owned_by           :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Investigation Model
# this is the master record for a Detective Investigation
class Investigation < ActiveRecord::Base
   belongs_to :status, :class_name => "Option", :foreign_key => 'status_id'
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   belongs_to :owner, :class_name => "User", :foreign_key => 'owned_by'
   has_many :reports, :class_name => 'InvestReport', :dependent => :destroy
   has_many :cases, :class_name => 'InvestCase', :dependent => :destroy
   has_many :evidences, :dependent => :destroy
   has_many :events, :dependent => :destroy
   has_many :warrants, :dependent => :destroy
   has_and_belongs_to_many :contacts
   has_and_belongs_to_many :victims, :class_name => 'Person', :join_table => 'invest_victims'
   has_and_belongs_to_many :witnesses, :class_name => 'Person', :join_table => 'invest_witnesses'
   has_and_belongs_to_many :sources, :class_name => 'Person', :join_table => 'invest_sources'
   has_many :photos, :class_name => 'InvestPhoto', :dependent => :destroy
   has_many :vehicles, :class_name => 'InvestVehicle', :dependent => :destroy
   has_many :crimes, :class_name => 'InvestCrime', :dependent => :destroy
   has_many :criminals, :class_name => 'InvestCriminal', :dependent => :destroy
   has_many :stolens, :dependent => :destroy

   # this sets up sphinx indices for this model
   define_index do
      indexes detective
      indexes detective_unit
      indexes status.long_name, :as => :stat
      indexes details
      indexes remarks

      where 'private != 1'
      
      has updated_at
   end

   after_save :check_creator, :check_owner, :update_private
   before_validation :fix_times, :lookup_officers
   before_destroy :handle_dependencies
   
   validates_presence_of :details, :detective
   validates_date_in_past :investigation_date
   validates_time :investigation_time
   validates_option :status_id, :option_type => 'Investigation Status'

   def self.desc
      'Detective Investigations'
   end
   
   # returns base perms required to view investigations
   def self.base_perms
      Perm[:view_investigation]
   end
   
   # returns true if the specified option id is in use for any option type
   # fields in any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(status_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      count(:include => [:witnesses, :victims, :sources], :conditions => "people.id = '#{p_id}'") > 0
   end
   
   # returns a list of investigations that include specified case number (via invest_case)
   def self.find_all_by_case_no(case_no)
      find(:all, :include => :cases, :conditions => ["invest_cases.case_no = ?",case_no])
   end
   
   # returns an array of case numbers that are related to the supplied case by
   # investigations
   def self.related_cases(case_number)
      related = []
      find(:all, :include => :cases, :conditions => ["invest_cases.case_no = ?",case_number]).each do |i|
         i.cases(true).reject{|c| c.private?}.each do |c|
            related.push([c.case_no, c.remarks, (c.owner.nil? ? "Unknown" : c.owner.name)])
         end
      end
      return related.uniq
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["detective_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      # catch the HABTM table for contacts
      # TODO: find a better way to do this
      rec_count += count(:all, :include => :contacts, :conditions => "contacts_investigations.contact_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE contacts_investigations SET contact_id = #{good_id.to_i} WHERE contact_id = #{bad_id.to_i}"
      return rec_count
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      # catch the HABTM tables for people
      # TODO: find a better way to do this
      rec_count += count(:all, :include => :sources, :conditions => "invest_sources.person_id = #{bad_id.to_i}")
      connection.execute "UPDATE invest_sources SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      rec_count += count(:all, :include => :victims, :conditions => "invest_victims.person_id = #{bad_id.to_i}")
      connection.execute "UPDATE invest_victims SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      rec_count += count(:all, :include => :witnesses, :conditions => "invest_witnesses.person_id = #{bad_id.to_i}")
      connection.execute "UPDATE invest_witnesses SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      return rec_count
   end
   
   def assigned_victims=(selected_victims)
      self.victims.clear
      selected_victims.each do |v|
         self.victims << Person.find_by_id(v) unless v.empty?
      end
   end
   
   def assigned_witnesses=(selected_witnesses)
      self.witnesses.clear
      selected_witnesses.each do |w|
         self.witnesses << Person.find_by_id(w) unless w.empty?
      end
   end
   
   def assigned_sources=(selected_sources)
      self.sources.clear
      selected_sources.each do |s|
         self.sources << Person.find_by_id(s) unless s.empty?
      end
   end
   
   def assigned_contacts=(selected_contacts)
      self.contacts.clear
      selected_contacts.each do |c|
         self.contacts << Contact.find_by_id(c) unless c.empty?
      end
   end
   
   private
   
   # removes or unlinks dependencies before destroy
   def handle_dependencies
      # is this necessary for HABTM joins?
      self.calls.clear
      self.offenses.clear
      self.citations.clear
      self.arrests.clear
      self.pawns.clear
      self.contacts.clear
      self.officers.clear
      self.victims.clear
      self.witnesses.clear
      self.sources.clear
      self.dockets.clear
      return true
   end
   
   # looks up any officer selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if detective_id?
         if ofc = Contact.find_by_id(detective_id)
            self.detective = ofc.fullname
            self.detective_badge = ofc.badge_no
            self.detective_unit = ofc.unit
         end
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      if !owned_by? || owner.nil?
         self.update_attribute(:owned_by, 1)
      end
      return true
   end
   
   # checks that all child records have the same owned_by as the investigation
   def check_owner
      # none of the following records have the ability to change the owner, so
      # they do not need one of these methods
      records = cases + crimes + criminals + photos + reports + vehicles + events + warrants + stolens + evidences
      records.each{|r| r.update_attribute(:owned_by, owned_by) unless r.owned_by == owned_by}
   end

   # checks that all child records have the same privacy as the investigation
   def update_private
      records = cases + crimes + criminals + photos + reports + vehicles + events + stolens + evidences
      records.each{|r| r.update_attribute(:private, self.private) if r.private != self.private}
      # warrants are done separately because can't make served warrants private
      served = Option.lookup("Warrant Disposition","Served",nil,nil,true)
      warrants.each{|w| w.update_attribute(:private, self.private) if w.private != self.private && w.disposition_id != served}
   end

   # calls the valid_time() method for each time type field (valid_time
   # inserts the : between hours and minutes if its omitted).
   def fix_times
      self.investigation_time = valid_time(investigation_time)
   end
   
   # accepts a query hash and creates a SQL partial suitable for use in a
   # WHERE clause.
   def self.condition_string(fullquery)
     return nil unless query = quoted(fullquery)
     condition = ""
     intvals = ['id']
     query.each do |key, value|
       if key == "investigation_date_from"
         if myval = valid_date(value)
           unless condition.empty?
             condition << " and "
           end
           condition << "investigations.investigation_date >= '#{myval}'"
         else
           fullquery.delete(key)
         end
       elsif key == "investigation_date_to"
         if myval = valid_date(value)
           unless condition.empty?
             condition << " and "
           end
           condition << "investigations.investigation_date <= '#{myval}'"
         else
           fullquery.delete(key)
         end
       elsif key == 'case_no'
         unless condition.empty?
           condition << " AND "
         end
         condition << "invest_cases.case_no = '#{value}'"
       elsif key == 'report_details'
         unless condition.empty?
           condition << " AND "
         end
         keywords = value.split
         subclause = []
         keywords.each do |k|
           subclause.push("invest_reports.details like '%#{k}%'")
         end
         condition << "#{subclause.join(' AND ')}"
       else
         unless condition.empty?
           condition << " and "
         end
         if intvals.include?(key)
           condition << "investigations.#{key} = '#{value}'"
         else
           condition << "investigations.#{key} like '%#{value}%'"
         end
       end
     end
     return sanitize_sql_for_conditions(condition)
   end
end
