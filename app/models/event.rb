# == Schema Information
#
# Table name: events
#
#  id               :integer(4)      not null, primary key
#  start_date       :date
#  end_date         :date
#  name             :string(255)
#  details          :text
#  created_at       :datetime
#  updated_at       :datetime
#  created_by       :integer(4)      default(1)
#  updated_by       :integer(4)      default(1)
#  private          :boolean(1)      default(FALSE)
#  investigation_id :integer(4)
#  owned_by         :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Event Model
# These are calendar events for the home page.
class Event < ActiveRecord::Base
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   belongs_to :owner, :class_name => 'User', :foreign_key => 'owned_by'
   belongs_to :investigation

   before_validation :fix_end_date
   after_save :check_creator

   validates_presence_of :name
   validates_date :start_date, :end_date

   # a short description of the model for the DatabaseController index view.
   def self.desc
      "Calendar events for the front page."
   end

   # Returns all events that are current during the calendar month containing
   # the specified date.
   def self.get_current_events(date,userid=nil)
      # returns events current during month for date
      events = []
      # build an array of dates covering the entire month
      first = Date.new(date.year, date.month, 1)
      month_range = []
      first.upto(first.end_of_month){|d| month_range.push(d)}
      # get all events
      if userid.nil?
         tmp = find(:all, :conditions => ['start_date <= ? AND end_date >= ? AND private != 1', month_range.last, month_range.first])
      else
         tmp = find(:all, :conditions => ['start_date <= ? AND end_date >= ? AND (private != 1 OR (private = 1 AND ((owned_by IS NOT NULL AND owned_by = ?) OR (owned_by IS NULL AND created_by = ?))))',month_range.last,month_range.first,userid,userid])
      end
      tmp.each do |e|   # build an array of dates covered by the event
         event_range = []
         e.start_date.upto(e.end_date){|d| event_range.push(d)}
         # get a list of dates that are in both arrays
         (event_range & month_range).each do |d|
            # add the event to each of the resulting dates
            events.push([d, e])
         end
      end
      return events
   end

   # this removes events that ended 2 months prior to the
   # first of this month (these are the ones that don't appear
   # in any of the front-page event calendars - they go back
   # 2 months)
   def self.purge_expired
      d = Date.today - 2.months
      limit = Date.new(d.year, d.month, 1)
      delete_all("end_date < '#{limit}' AND investigation_id IS NULL")
   end
   
   # returns base perms required to View items
   def self.base_perms
      Perm[:view_event]
   end

   # returns true if the current event has expired
   def expired?
      end_date < Date.today
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # this swaps start and end dates if end is before start.
   def fix_end_date
      unless end_date?
         self.end_date = start_date
      end
      if start_date > end_date
         tdate = start_date
         self.start_date = end_date
         self.end_date = tdate
      end
      return true
   end

   # accepts a query hash and turns it into a partial SQL query string
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ["created_by"]
      condition = ""
      query.each do |key, value|
         if key == 'start_from'
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "events.start_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "start_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "events.start_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == 'text'
            unless condition.empty?
               condition << " and "
            end
            condition << "(events.name like '%#{value}%' or events.details like '%#{value}%')"
         else
            unless condition.empty?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "events.#{key} = '#{value}'"
            else
               condition << "events.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
