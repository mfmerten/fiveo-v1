# == Schema Information
#
# Table name: paper_payments
#
#  id                  :integer(4)      not null, primary key
#  paper_id            :integer(4)
#  paper_customer_id   :integer(4)
#  transaction_date    :date
#  officer             :string(255)
#  officer_unit        :string(255)
#  officer_badge       :string(255)
#  officer_id          :integer(4)
#  receipt_no          :string(255)
#  payment             :decimal(12, 2)  default(0.0)
#  charge              :decimal(12, 2)  default(0.0)
#  report_datetime     :datetime
#  memo                :string(255)
#  created_by          :integer(4)      default(1)
#  updated_by          :integer(4)      default(1)
#  created_at          :datetime
#  updated_at          :datetime
#  payment_method      :string(255)
#  trans_not_deletable :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================

# Note: the 'trans_not_deletable' attribute was added when the transaction
# delete feature was implemented. This was done solely to prevent deleting any
# transactions existing prior to the update because allowing that could result
# in incorrect customer balance_due values.  This flag is always false for
# new transactions.

class PaperPayment < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :customer, :class_name => 'PaperCustomer', :foreign_key => 'paper_customer_id'
   belongs_to :paper
   
   # set to true for adjustment transactions
   attr_accessor_with_default :adjust, false
   
   after_create :process_transaction
   before_validation :check_links, :check_amounts, :lookup_officers
   after_save :check_creator
   before_destroy :check_and_unprocess
   
   validates_presence_of :officer
   validates_date :transaction_date
   validates_numericality_of :payment, :charge, :allow_nil => true
   
   def self.desc
      "Paper Service Payments"
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_civil]
   end
   
   def self.unposted
      find(:all, :include => :customer, :order => 'paper_payments.transaction_date, paper_payments.id', :conditions => 'report_datetime IS NULL')
   end
   
   def self.posted(report_datetime=nil)
      if report_datetime.is_a?(Time) || report_datetime.is_a?(DateTime)
         find(:all, :include => :customer, :order => 'paper_payments.transaction_date, paper_payments.id', :conditions => ["report_datetime = ?",report_datetime])
      else
         find(:all, :include => :customer, :order => 'paper_payments.transaction_date, paper_payments.id', :conditions => "report_datetime IS NOT NULL")
      end
   end
   
   def self.posting_dates
      postings = find(:all, :select => 'DISTINCT report_datetime', :order => 'report_datetime', :conditions => "report_datetime is not null").collect{|c| [format_date(c.report_datetime),c.report_datetime]}
      if postings.size > 10
         return postings.slice(-10,10).reverse
      else
         return postings.reverse
      end
   end
   
   def self.types_for_select
      type_names = []
      (1..5).each do |x|
         unless SiteConfig.send("paper_pay_type#{x}").blank?
            type_names.push([SiteConfig.send("paper_pay_type#{x}"),SiteConfig.send("paper_pay_type#{x}")])
         end
      end
      return type_names
   end
   
   def update_posted(report_time)
      if report_time.is_a?(Time) || report_time.is_a?(DateTime)
         self.update_attribute(:report_datetime, report_time)
      end
   end
   
   private
   
   def check_links
      retval = true
      if paper_customer_id?
         if customer.nil?
            self.errors.add(:paper_customer_id, 'is not a valid customer!')
            retval = false
         end
      else
         self.errors.add(:paper_customer_id, 'is required!')
         retval = false
      end
      if paper_id?
         if paper.nil?
            self.errors.add(:paper_id, 'is not a valid paper!')
            retval = false
         elsif paper.paper_customer_id != paper_customer_id
            self.errors.add(:paper_id, 'is not for this customer!')
            retval = false
         end
      else
         unless adjust == true
            self.errors.add(:paper_id, 'is required!')
            retval = false
         end
      end
      # check for memo (required on adjustments)
      if adjust == true
         unless memo?
            self.errors.add(:memo, 'is required for adjustments!')
            retval = false
         end
      end
      return retval
   end
   
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end

   def lookup_officers
      if officer_id?
         if ofc = Contact.find_by_id(officer_id)
            self.officer = ofc.fullname
            self.officer_badge = ofc.badge_no
            self.officer_unit = ofc.unit
         end
      end
   end

   def check_amounts
      unless (charge? && charge != 0) || (payment? && payment > 0)
         self.errors.add(:charge, "cannot be 0 if payment is 0!")
         self.errors.add(:payment, "cannot be 0 if charge is 0!")
         return false
      end
      if payment? && payment < 0
         self.errors.add(:payment, "cannot be negative!")
         return false
      end
      if paper.nil?
         # allow for adjustment transactions
         return true if adjust == true
         self.errors.add(:paper_id, "is not a valid Invoice.")
         return false
      end
      if payment? && payment > 0
         if (payment - (charge? && charge != 0 ? charge : 0)) != paper.invoice_total
            self.errors.add(:base, "Cannot accept partial payments! Payment - Charge must equal total due.")
            self.errors.add(:charge, "has been adjusted. Verify it and resubmit.")
            self.charge = payment - paper.invoice_total
            return false
         end
         if payment != paper.invoice_total && !memo?
            self.errors.add(:memo, "must explain difference if payment is not for invoice total.")
            return false
         end
      end
      return true
   end
   
   def process_transaction
      # special case to allow adjustment to customer balance
      if paper.nil?
         # must have a customer
         return false if customer.nil?
         cust = customer
         net_change = ((charge? && charge > 0 ? charge : BigDecimal.new('0.0',2)) - (payment? && payment > 0 ? payment : BigDecimal.new('0.0',2)))
         if net_change != 0
            cust.lock!
            cust.balance_due += net_change
            cust.save!
         end
         return true
      end
      # normal payments received must be the full amount due.
      # if payments does not equal invoice_total, add the difference to charge
      # as a balance adjustment
      #
      # cannot do this if customer or invoice is invalid, or if invoice is not for this customer
      if customer.nil? || customer.id != paper.paper_customer_id
         return false
      else
         PaperPayment.transaction do
            invoice = paper
            cust = customer
            if payment? && payment > 0
               unpaid_balance = invoice.invoice_total - payment + (charge? && charge != 0 ? charge : 0)
               if unpaid_balance != 0
                  self.charge -= unpaid_balance
                  unless memo?
                     self.memo = 'Includes Balance Adjustment'
                  end
                  self.save(false)
               end
               invoice.update_attribute(:paid_date, transaction_date)
               cust.lock!
               cust.balance_due -= invoice.invoice_total
               cust.save!
            elsif charge? && charge != 0
               cust.lock!
               cust.balance_due += charge
               cust.save!
            end
         end
      end
      return true
   end
   
   # checks that transaction has not been posted, then unprocesses the transaction
   # to update customer balance before allowing destroy
   def check_and_unprocess
      if report_datetime? || trans_not_deletable?
         # posted (or existed prior to delete feature), cannot allow delete
         return false
      end
      if !paper.nil? && paper.paid_date? && !(payment? && payment > 0)
         # attempting to delete an invoice that has been paid, cannot allow
         # (must delete the payment first)
         return false
      end
      # special case for balance adjustments (only affects customer balance)
      if paper.nil?
         unless customer.nil?
            cust = customer
            net_change = ((charge? && charge > 0 ? charge : BigDecimal.new('0.0',2)) - (payment? && payment > 0 ? payment : BigDecimal.new('0.0',2)))
            if net_change != 0
               cust.lock!
               cust.balance_due -= net_change
               cust.save!
            end
         end
         return true
      end
      # perform a reverse processing on the transaction
      unless customer.nil?
         PaperCustomer.transaction do
            invoice = paper
            cust = customer
            if payment? && payment > 0
               # customer payment
               # each payment transaction = invoice_total
               cust.lock!
               cust.balance_due += invoice.invoice_total
               cust.save!
               # mark the invoice un-paid
               invoice.update_attribute(:paid_date, nil)
            elsif charge? && charge > 0
               # new invoice
               cust.lock!
               cust.balance_due -= invoice.invoice_total
               cust.save!
               # now un-invoice the paper
               invoice.update_attribute(:invoice_total, BigDecimal.new('0.0',2))
               invoice.update_attribute(:invoice_datetime, nil)
            end
         end
      end
      return true
   end
end
