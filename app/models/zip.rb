# == Schema Information
#
# Table name: zips
#
#  id         :integer(4)      not null, primary key
#  zip_code   :string(255)
#  latitude   :string(255)
#  longitude  :string(255)
#  state      :string(255)
#  city       :string(255)
#  county     :string(255)
#  created_at :datetime
#  updated_at :datetime
#  created_by :integer(4)      default(1)
#  updated_by :integer(4)      default(1)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Zip Model
# ZIP codes
class Zip < ActiveRecord::Base
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   
   after_save :check_creator

   validates_presence_of :zip_code, :state, :city, :county
   validates_length_of :zip_code, :is => 5, :message => "must be 5-digit zip code only!"
   validates_length_of :state, :is => 2, :message => "must be 2-letter postal abbreviation only!"
   validates_numericality_of :zip_code, :integer_only => true

   # a short description for the DatabaseController index view.
   def self.desc
      "ZIP Codes"
   end
   
   # returns the first record matching the entered zip code
   def self.find_by_zip_code(zip)
      return nil if zip.nil? || zip.empty?
      find(:first, :conditions => ["zip_code = ?", zip.slice(0,5)])
   end

   # returns true if the specified city and state match the specified zip.
   def self.verify_zip(city,st,zip)
      return false if city.nil? || st.nil? || zip.nil?
      myzip = find_by_zip_code(zip)
      return false if myzip.nil?
      city = city.titleize
      if st.length > 2
         st = Option.lookup("State", st, 'long_name','short_name')
      end
      return true if myzip.city == city && myzip.state == st
      return false
   end

   # returns all zip codes matching the specified city and state.
   def self.find_all_by_city_and_state(city,st)
      return nil if city.nil? || st.nil?
      city = city.titleize
      if st.length > 2
         st = Option.lookup("State", st, 'long_name','short_name')
      end
      find(:all, :select => 'zip_code', :conditions => ["city = ? and state = ?", city, st]).collect{|z| z.zip_code}.join(" ,")
   end

   # returns the city and state that match the supplied zip code
   def self.find_city_state_by_zip(zip)
      return ["",""] if zip.nil?
      myzip = find_by_zip_code(zip)
      myzip.nil? ? ["",""] : [myzip.city, myzip.state]
   end
   
   # returns base perms required to view zip codes
   def self.base_perms
      Perm[:view_option]
   end

   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end

   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      query.each do |key, value|
         if key == "from"
            unless condition.empty?
               condition << " and "
            end
            condition << "zips.zip_code >= '#{value}'"
         elsif key == "to"
            unless condition.empty?
               condition << " and "
            end
            condition << "zips.zip_code <= '#{value}'"
         else
            unless condition.empty?
               condition << " and "
            end
            condition << "zips.#{key} like '%#{value}%'"
         end
      end
      return sanitize_sql_for_conditions(condition)
   end

end
