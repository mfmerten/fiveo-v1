# == Schema Information
#
# Table name: option_types
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Option Type Model
# Categories for Options
class OptionType < ActiveRecord::Base
    has_many :options
    
    before_destroy :refuse
    before_save :refuse
    
    validates_presence_of :name
    validates_uniqueness_of :name
    
    # a short description for the DatabaseController index view.
    def self.desc
       "Form Option Types"
    end
    
    # returns an array of arrays containing name and id in a form suitable
    # for use in a select control.
    def self.searchable
        result = []
        OptionType.find(:all, :select => 'name, id').each do |o|
            result << [o.name, o.id]
        end
        result
    end
    
    private
    
    # refuse to destroy option types (will break code)
    # future code maintainers should use migrations to
    # remove option_types and their options, and to
    # add new ones.
    def refuse
        return false
    end
end
