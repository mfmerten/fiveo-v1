# == Schema Information
#
# Table name: invest_photos
#
#  id                        :integer(4)      not null, primary key
#  investigation_id          :integer(4)
#  invest_crime_id           :integer(4)
#  invest_report_id          :integer(4)
#  invest_criminal_id        :integer(4)
#  invest_vehicle_id         :integer(4)
#  invest_photo_file_name    :string(255)
#  invest_photo_content_type :string(255)
#  invest_photo_file_size    :integer(4)
#  invest_photo_updated_at   :datetime
#  private                   :boolean(1)      default(TRUE)
#  description               :string(255)
#  date_taken                :date
#  location_taken            :string(255)
#  taken_by                  :string(255)
#  taken_by_id               :integer(4)
#  taken_by_unit             :string(255)
#  taken_by_badge            :string(255)
#  remarks                   :text
#  created_by                :integer(4)      default(1)
#  updated_by                :integer(4)      default(1)
#  created_at                :datetime
#  updated_at                :datetime
#  owned_by                  :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Investigation Photo Model
# these are photos (images) referenced by various parts of a Detective
# Investigation
class InvestPhoto < ActiveRecord::Base
   belongs_to :investigation
   belongs_to :crime, :class_name => 'InvestCrime', :foreign_key => 'invest_crime_id'
   belongs_to :report, :class_name => 'InvestReport', :foreign_key => 'invest_report_id'
   belongs_to :criminal, :class_name => 'InvestCriminal', :foreign_key => 'invest_criminal_id'
   belongs_to :vehicle, :class_name => 'InvestVehicle', :foreign_key => 'invest_vehicle_id'
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :owner, :class_name => 'User', :foreign_key => 'owned_by'
   
   # this sets up sphinx indices for this model
   define_index do
      indexes description
      indexes date_taken
      indexes location_taken
      indexes taken_by
      indexes taken_by_unit
      indexes remarks

      where 'private != 1'
      
      has updated_at
   end
   
   before_validation :lookup_officers
   after_save :check_creator
   
   validates_date_in_past :date_taken
   validates_presence_of :location_taken, :description, :taken_by
   
   # investigation photos
   has_attached_file :invest_photo,
      :url => "/system/:attachment/:id/:style/:basename.:extension",
      :path => ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
      :default_url => "/images/no_image_:style.jpg",
      :styles => {
         :thumb => "80x80",
         :original => "600x600",
         :medium => "300x300",
         :small => "150x150"
      }
   
   def self.desc
      "Photos for Detective Investigation"
   end
   
   # returns base perms required to view investigations
   def self.base_perms
      Perm[:view_investigation]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["taken_by_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   private
   
   # looks up any officer selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if taken_by_id?
         if ofc = Contact.find_by_id(taken_by_id)
            self.taken_by = ofc.fullname
            self.taken_by_badge = ofc.badge_no
            self.taken_by_unit = ofc.unit
         end
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the value of creator for investigation,
   # or the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:created_by, 1)
         else
            self.update_attribute(:created_by, investigation.created_by)
         end
      end
      if !updated_by? || updater.nil?
         if investigation.nil? || investigation.updater.nil?
            self.update_attribute(:updated_by, 1)
         else
            self.update_attribute(:updated_by, investigation.updated_by)
         end
      end
      if !owned_by? || owner.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:owned_by, 1)
         else
            self.update_attribute(:owned_by, investigation.created_by)
         end
      end
      return true
   end
end
