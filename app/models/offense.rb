# == Schema Information
#
# Table name: offenses
#
#  id                  :integer(4)      not null, primary key
#  call_id             :integer(4)
#  case_no             :string(255)
#  location            :string(255)
#  details             :text
#  pictures            :boolean(1)
#  restitution_form    :boolean(1)
#  victim_notification :boolean(1)
#  perm_to_search      :boolean(1)
#  misd_summons        :boolean(1)
#  fortyeight_hour     :boolean(1)
#  medical_release     :boolean(1)
#  other_documents     :text
#  officer             :string(255)
#  remarks             :text
#  created_at          :datetime
#  updated_at          :datetime
#  offense_date        :date
#  offense_time        :string(255)
#  created_by          :integer(4)      default(1)
#  updated_by          :integer(4)      default(1)
#  officer_unit        :string(255)
#  officer_id          :integer(4)
#  officer_badge       :string(255)
#  legacy              :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Offense Model
# Officer offense reports.
class Offense < ActiveRecord::Base
   belongs_to :call
   has_and_belongs_to_many :victims, :class_name => "Person", :join_table => 'victims'
   has_and_belongs_to_many :witnesses, :class_name => "Person", :join_table => 'witnesses'
   has_and_belongs_to_many :suspects, :class_name => "Person", :join_table => 'suspects'
   has_many :evidences, :conditions => 'evidences.private != 1'
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   has_and_belongs_to_many :forbids
	has_many :arrests
	has_many :offense_photos, :dependent => :destroy
	has_many :citations
	has_many :wreckers
   
   # this sets up sphinx indices for this model
   define_index do
      indexes case_no
      indexes location
      indexes details
      indexes wreckers.vin, :as => :wvins
      indexes wreckers.tag, :as => :wlics
      indexes wreckers.location, :as => :wlocations
      indexes wreckers.name, :as => :wwreckers
      indexes officer
      indexes officer_unit
      indexes remarks
      indexes offense_date
      indexes victims.lastname, :as => :vlnames
      indexes victims.firstname, :as => :vfnames
      indexes victims.middlename, :as => :vmnames
      indexes victims.suffix, :as => :vsuffs
      indexes witnesses.lastname, :as => :wlnames
      indexes witnesses.firstname, :as => :wfnames
      indexes witnesses.middlename, :as => :wmnames
      indexes witnesses.suffix, :as => :wsuffs
      indexes suspects.lastname, :as => :slnames
      indexes suspects.firstname, :as => :sfnames
      indexes suspects.middlename, :as => :smnames
      indexes suspects.suffix, :as => :ssuffs
      indexes offense_photos.description, :as => :opdescriptions
      indexes offense_photos.location_taken, :as => :oplocations
      indexes offense_photos.remarks, :as => :opremarks
      indexes offense_photos.taken_by, :as => :opofficers
      indexes offense_photos.taken_by_unit, :as => :opunits
      
      has updated_at
   end
   
   before_validation :adjust_case_no, :fix_time, :lookup_officers
   before_destroy :check_and_destroy_dependencies
   after_save :update_case, :check_creator
   after_update :save_wreckers
   
   validates_presence_of :officer, :location, :details
   validates_date :offense_date
   validates_time :offense_time
   validates_call :call_id
   
   # a short description for the DatabaseController index view.
   def self.desc
      "Offense Reports"
   end
   
   # returns base permissions required to view offense
   def self.base_perms
      Perm[:view_offense]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      count(:include => [:victims, :witnesses, :suspects], :conditions => "people.id = '#{p_id}'") > 0
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      # catch the HABTM tables for people
      # TODO: find a better way to do this
      rec_count += count(:all, :include => :suspects, :conditions => "suspects.person_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE suspects SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      rec_count += count(:all, :include => :witnesses, :conditions => "witnesses.person_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE witnesses SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      rec_count += count(:all, :include => :victims, :conditions => "victims.person_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE victims SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      return rec_count
   end
   
   # accepts a list of person attributes and associates them to this offense
   # as victims.
   def assigned_victims=(selected_victims)
      self.victims.clear
      selected_victims.each do |p|
         if person = Person.find_by_id(p)
            self.victims << person
         end
      end
   end
   
   # accepts a list of person attributes and associates them to this offense
   # as suspects. 
   def assigned_suspects=(selected_suspects)
      self.suspects.clear
      selected_suspects.each do |p|
         if person = Person.find_by_id(p)
            self.suspects << person
         end
      end
   end

   # accepts a list of person attributes and associates them to this offense
   # as witnesses.
   def assigned_witnesses=(selected_witnesses)
      self.witnesses.clear
      selected_witnesses.each do |p|
         if person = Person.find_by_id(p)
            self.witnesses << person
         end
      end
   end
   
   def new_wrecker_attributes=(wrecker_attributes)
      wrecker_attributes.each do |attributes|
         unless attributes[:name].blank?
            wreckers.build(attributes)
         end
      end
   end

   def existing_wrecker_attributes=(wrecker_attributes)
      wreckers.reject(&:new_record?).each do |wrecker|
         attributes = wrecker_attributes[wrecker.id.to_s]
         if attributes
            wrecker.attributes = attributes
         else
            wreckers.delete(wrecker)
         end
      end
   end
   
   private
   
   def save_wreckers
      wreckers.each{|w| w.save(false)}
   end
   
   # strips all characters from case_no except for numbers, letters, and -
   def adjust_case_no
      self.case_no = fix_case(case_no)
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if officer_id?
         if ofc = Contact.find_by_id(officer_id)
            self.officer = ofc.fullname
            self.officer_badge = ofc.badge_no
            self.officer_unit = ofc.unit
         end
      end
      return true
   end
   
   # ensures that when an offense report is deleted, the offense_id fields
   # in evidences, forbids and warrants is set to nil, and that all victim,
   # witness and suspect associations are cleared.
   def check_and_destroy_dependencies
      evidences.each do |e|
         e.update_attribute(:offense_id, nil)
      end
      self.victims.clear
      self.witnesses.clear
      self.suspects.clear
      self.forbids.clear
      return true
   end
   
   # Check to see if a case number was entered, and if not, get one
   # by looking up linked call sheet.
   def update_case
      unless case_no?
         unless call.nil?
            if call.case_no?
               self.update_attribute(:case_no, call.case_no)
            end
         end
      end
      return true
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = %w(call_id case_no pictures restitution_form victim_notification perm_to_search misd_summons fortyeight_hour medical_release)
      # convert to actual field names and build an sql WHERE string
      condition = ""
      query.each do |key, value|
         if key == "offense_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "offenses.offense_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "offense_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "offenses.offense_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
          elsif key == 'vehicle_location'
            unless condition.empty?
              condition << " and "
            end
            condition << "wreckers.location like '%#{value}%'"
          elsif key == 'vehicle_vin'
            unless condition.empty?
              condition << " and "
            end
            condition << "wreckers.vin like '%#{value}%'"
          elsif key == 'vehicle_lic'
            unless condition.empty?
              condition << " and "
            end
            condition << "wreckers.tag like '%#{value}%'"
         elsif key == "wrecker_used"
            unless condition.empty?
               condition << " and "
            end
            condition << "wreckers.name like '%#{value}%'"
         elsif key == "wrecker_by_request"
            unless condition.empty?
               condition << " and "
            end
            condition << "wreckers.by_request is #{value.to_i == 0 ? 'false' : 'true'}"
         else
            unless condition.empty?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "offenses.#{key} = '#{value}'"
            else
               condition << "offenses.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
   
   # checks the offense time field and inserts a colon if one was omitted.
   def fix_time
      self.offense_time = valid_time(offense_time)
   end
end
