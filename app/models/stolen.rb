# == Schema Information
#
# Table name: stolens
#
#  id                 :integer(4)      not null, primary key
#  case_no            :string(255)
#  person_id          :integer(4)
#  stolen_date        :date
#  model_no           :string(255)
#  serial_no          :string(255)
#  item_value         :decimal(12, 2)  default(0.0)
#  item               :string(255)
#  item_description   :text
#  recovered_date     :date
#  returned_date      :date
#  remarks            :text
#  created_by         :integer(4)      default(1)
#  updated_by         :integer(4)      default(1)
#  created_at         :datetime
#  updated_at         :datetime
#  private            :boolean(1)      default(FALSE)
#  investigation_id   :integer(4)
#  invest_crime_id    :integer(4)
#  invest_criminal_id :integer(4)
#  owned_by           :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Stolen Model
# records stolen items
class Stolen < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :owner, :class_name => 'User', :foreign_key => 'owned_by'
   belongs_to :person
   belongs_to :investigation
   belongs_to :crime, :class_name => 'InvestCrime', :foreign_key => 'invest_crime_id'
   belongs_to :criminal, :class_name => 'InvestCriminal', :foreign_key => 'invest_criminal_id'
   
   # this sets up sphinx indices for this model
   define_index do
      indexes [person.lastname, person.firstname, person.middlename, person.suffix], :as => :name
      indexes case_no
      indexes stolen_date
      indexes model_no
      indexes serial_no
      indexes item
      indexes item_description
      indexes remarks
      
      has updated_at
   end
   
   before_validation :adjust_case_no
   after_save :check_creator
   
   validates_numericality_of :item_value, :allow_nil => true
   validates_date :stolen_date, :recovered_date, :returned_date, :allow_nil => true
   validates_person :person_id
   
   # a short description for the DatabaseController index view.
   def self.desc
      "Stolen Items Database"
   end
   
   # returns base permission required to view stolen
   def self.base_perms
      Perm[:view_stolen]
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   private

   # strips all characters from case_no except for numbers, letters, and -
   def adjust_case_no
      self.case_no = fix_case(case_no)
      return true
   end

   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end

   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      intvals = ['person_id','id']
      query.each do |key, value|
         if key == 'person_name'
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.firstname like '#{nameparts[:first]}'"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.middlename like '#{nameparts[:middle]}'"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.lastname like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.suffix like '#{nameparts[:suffix]}'"
            end
         elsif key == "stolen_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "stolens.stolen_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "stolen_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "stolens.stolen_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "is_recovered"
            unless condition.empty?
               condition << " and "
            end
            if value == '1'
               condition << "stolens.recovered_date is not null"
            elsif value == '0'
               condition << "stolens.recovered_date is null"
            end
         elsif key == "recovered_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "stolens.recovered_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "recovered_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "stolens.recovered_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "is_returned"
            unless condition.empty?
               condition << " and "
            end
            if value == '1'
               condition << "stolens.returned_date is not null"
            elsif value == '0'
               condition << "stolens.returned_date is null"
            end
         elsif key == "returned_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "stolens.returned_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "returned_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "stolens.returned_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else
            unless condition.empty?
               condition << " and "
            end
            if intvals.include?(key)
               condition << "stolens.#{key} = '#{value}'"
            else
               condition << "stolens.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
   
end
