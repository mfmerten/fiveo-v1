# == Schema Information
#
# Table name: invest_cases
#
#  id               :integer(4)      not null, primary key
#  investigation_id :integer(4)
#  case_no          :string(255)
#  private          :boolean(1)      default(TRUE)
#  remarks          :text
#  created_by       :integer(4)      default(1)
#  updated_by       :integer(4)      default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  owned_by         :integer(4)
#  invest_crime_id  :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Investigation Case Model
# This links ordinary cases into an investigation and can (when made public)
# also tie cases together
class InvestCase < ActiveRecord::Base
   belongs_to :investigation
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :owner, :class_name => 'User', :foreign_key => 'owned_by'
   belongs_to :crime, :class_name => 'InvestCrime', :foreign_key => 'invest_crime_id'
   
   # this sets up sphinx indices for this model
   define_index do
      indexes case_no
      indexes remarks

      where 'private != 1'
      
      has updated_at
   end
   
   after_save :check_creator
   before_validation :validate_case
   
   validates_presence_of :case_no
   
   def self.desc
      "Case Numbers for Detective Investigation"
   end
   
   # returns base perms required to view investigations
   def self.base_perms
      Perm[:view_investigation]
   end
   
   private
   
   # validates that case numbers being added are valid pre-existing cases
   def validate_case
      self.case_no = fix_case(case_no)
      if case_no?
         unless Case.valid_case(case_no)
            self.errors.add(:case_no, "must be a vaild pre-existing case number!")
            return false
         end
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the value of creator for investigation,
   # or the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:created_by, 1)
         else
            self.update_attribute(:created_by, investigation.created_by)
         end
      end
      if !updated_by? || updater.nil?
         if investigation.nil? || investigation.updater.nil?
            self.update_attribute(:updated_by, 1)
         else
            self.update_attribute(:updated_by, investigation.updated_by)
         end
      end
      if !owned_by? || owner.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:owned_by, 1)
         else
            self.update_attribute(:owned_by, investigation.created_by)
         end
      end
      return true
   end
end
