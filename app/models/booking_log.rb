# == Schema Information
#
# Table name: booking_logs
#
#  id                  :integer(4)      not null, primary key
#  booking_id          :integer(4)
#  start_datetime      :datetime
#  stop_datetime       :datetime
#  start_officer       :string(255)
#  start_officer_badge :string(255)
#  start_officer_unit  :string(255)
#  stop_officer        :string(255)
#  stop_officer_badge  :string(255)
#  stop_officer_unit   :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class BookingLog < ActiveRecord::Base
   belongs_to :booking
   
   before_destroy :refuse
   
   def self.desc
      'Time Served Log for Booking'
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_booking]
   end
   
   def hours_served(start_time=nil,stop_time=nil)
      return 0 unless (start_time.nil? || start_time.is_a?(Time) || start_time.is_a?(DateTime)) && (stop_time.nil? || stop_time.is_a?(Time) || stop_time.is_a?(DateTime))
      return 0 unless start_datetime?
      # force all values to DateTime
      log_start = nil
      log_stop = nil
      log_start = get_datetime(start_datetime) if start_datetime?
      log_stop = get_datetime(stop_datetime) if stop_datetime?
      start_time = get_datetime(start_time) unless start_time.nil?
      stop_time = get_datetime(stop_time) unless stop_time.nil?
      mystart = nil
      mystop = nil
      if start_time.nil? || log_start >= start_time
         mystart = log_start
      else
         mystart = start_time
      end
      if stop_time.nil?
         if log_stop.nil?
            mystop = DateTime.now
         else
            mystop = log_stop
         end
      else
         if log_stop.nil? || log_stop > stop_time
            mystop = stop_time
         else
            mystop = log_stop
         end
      end
      return hours_diff(mystop, mystart)
   end
   
   def minutes_served(since_datetime=nil)
      return 0 unless since_datetime.nil? || since_datetime.is_a?(Time) || since_datetime.is_a?(DateTime)
      return 0 unless start_datetime?
      # force all values to DateTime
      log_stop = nil
      log_start = get_datetime(start_datetime)
      log_stop = get_datetime(stop_datetime) if stop_datetime?
      since_datetime = get_datetime(since_datetime) unless since_datetime.nil?
      
      if since_datetime.nil? || log_start >= since_datetime
         if log_stop.nil?
            return minutes_diff(DateTime.now, log_start)
         else
            return minutes_diff(log_stop, log_start)
         end
      elsif !log_stop.nil?
         if log_stop >= since_datetime
            return minutes_diff(log_stop, since_datetime)
         else
            # this entire record was before since datetime value
            return 0
         end
      else
         return minutes_diff(DateTime.now, since_datetime)
      end
   end
   
   private
   
   def refuse
      return false
   end
end
