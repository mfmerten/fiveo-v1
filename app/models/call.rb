# == Schema Information
#
# Table name: calls
#
#  id                :integer(4)      not null, primary key
#  case_no           :string(255)
#  received_via_id   :integer(4)
#  received_by       :string(255)
#  details           :text
#  disposition_id    :integer(4)
#  dispatcher        :string(255)
#  ward              :integer(4)
#  district          :integer(4)
#  remarks           :text
#  created_at        :datetime
#  updated_at        :datetime
#  call_date         :date
#  call_time         :string(255)
#  dispatch_date     :date
#  dispatch_time     :string(255)
#  arrival_date      :date
#  arrival_time      :string(255)
#  concluded_date    :date
#  concluded_time    :string(255)
#  created_by        :integer(4)      default(1)
#  updated_by        :integer(4)      default(1)
#  signal_code_id    :integer(4)
#  dispatcher_unit   :string(255)
#  received_by_unit  :string(255)
#  received_by_id    :integer(4)
#  received_by_badge :string(255)
#  dispatcher_id     :integer(4)
#  dispatcher_badge  :string(255)
#  detective         :string(255)
#  detective_id      :integer(4)
#  detective_unit    :string(255)
#  detective_badge   :string(255)
#  legacy            :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Call Model
# Dispatcher call sheets.
class Call < ActiveRecord::Base
   belongs_to :received_via, :class_name => "Option", :foreign_key => 'received_via_id'
   belongs_to :disposition, :class_name => "Option", :foreign_key => 'disposition_id'
   belongs_to :signal_code, :class_name => "Option", :foreign_key => 'signal_code_id'
   has_many :offenses
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   has_many :call_units, :dependent => :destroy
   has_many :forbids
   has_many :call_subjects, :dependent => :destroy
   
   # used on the edit form as a temporary input for the signal code
   attr_accessor :signal
   # used on the edit form to enter a case_number for followups
   attr_accessor :followup_no
   
   # this sets up sphinx indices for this model
   define_index do
      indexes call_subjects.name, :as => :subnames
      indexes call_subjects.address1, :as => :substreets
      indexes call_subjects.address2, :as => :subcities
      indexes case_no
      indexes received_by
      indexes received_by_unit
      indexes dispatcher
      indexes dispatcher_unit
      indexes detective
      indexes detective_unit
      indexes call_date
      indexes signal_code.short_name, :as => :scode
      indexes details
      indexes remarks
      
      has updated_at
   end
   
   before_destroy :check_and_destroy_dependencies
   before_validation :fix_times, :check_or_generate_case, :check_signal, :lookup_officers
   after_update :save_call_units, :save_call_subjects
   after_save :check_creator

   validates_presence_of :details, :dispatcher
   validates_time :call_time
   validates_date :call_date
   validates_date :dispatch_date, :arrival_date, :concluded_date, :allow_nil => true
   validates_time :dispatch_time, :arrival_time, :concluded_time, :allow_nil => true
   validates_presence_of :dispatch_time, :if => :dispatch_date?
   validates_presence_of :dispatch_date, :if => :dispatch_time?
   validates_presence_of :arrival_time, :if => :arrival_date?
   validates_presence_of :arrival_date, :if => :arrival_time?
   validates_presence_of :concluded_time, :if => :concluded_date?
   validates_presence_of :concluded_date, :if => :concluded_time?
   validates_numericality_of :ward, :district, :only_integer => true, :allow_nil => true
   validates_option :received_via_id, :option_type => 'Call Via'
   validates_option :disposition_id, :option_type => 'Call Disposition'
   validates_option :signal_code_id, :option_type => 'Signal Code'
      
   # a short description of the model used by the DatabaseController index
   # view.
   def self.desc
      "Call Sheets"
   end
   
   # returns the number of items to be reviewed by supplied user_id
   def self.review_count(user_id)
      return 0 unless user = User.find_by_id(user_id)
      if user.last_call.nil? || !user.last_call.is_a?(Integer)
         user.update_attribute(:last_call, 0)
      end
      count(:conditions => ['id > ?',user.last_call])
   end
   
   # returns true if the specified option id is used in any record for any
   # option type field.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(received_via_id = '#{oid}' OR disposition_id = '#{oid}' OR signal_code_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns base perms required to View these records
   def self.base_perms
      Perm[:view_call]
   end

   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["received_by_id", "dispatcher_id", "detective_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end

   # accepts a list of new dispatched unit attributes and builds records for
   # them using the call_units association.
   def new_unit_attributes=(unit_attributes)
      unit_attributes.each do |attributes|
         unless attributes[:officer].blank? && attributes[:officer_id].blank?
            call_units.build(attributes)
         end
      end
   end

   # accepts a list of dispatched unit attributes and compares them to the
   # existing dispatched units, if a match is found, the existing unit is 
   # updated, if not the existing unit is destroyed.
   def existing_unit_attributes=(unit_attributes)
      call_units.reject(&:new_record?).each do |unit|
         attributes = unit_attributes[unit.id.to_s]
         if attributes
            unit.attributes = attributes
         else
            call_units.delete(unit)
         end
      end
   end
   
   # accepts a list of new subject attributes and builds records for
   # them using the call_subjects association.
   def new_subject_attributes=(subject_attributes)
      subject_attributes.each do |attributes|
         unless attributes[:name].blank?
            call_subjects.build(attributes)
         end
      end
   end

   # accepts a list of subject attributes and compares them to the
   # existing subjects, if a match is found, the existing subject is 
   # updated, if not the existing subject is destroyed.
   def existing_subject_attributes=(subject_attributes)
      call_subjects.reject(&:new_record?).each do |subject|
         attributes = subject_attributes[subject.id.to_s]
         if attributes
            subject.attributes = attributes
         else
            call_subjects.delete(subject)
         end
      end
   end
   
   # returns a string containing a list of dispatched units.
   def units
      return "" if call_units.empty?
      call_units.collect{|u| (u.officer_unit + " " + u.officer).strip}.join(", ")
   end
   
   # returns a string listing subjects
   def subject_summary
      txt = ""
      call_subjects.each do |s|
         unless txt.empty?
            txt << " "
         end
         txt << "#{s.name? ? s.name.gsub(/ /,'&nbsp;') : ''}&nbsp;(#{s.subject_type.nil? ? '' : s.subject_type.long_name.gsub(/ /,'&nbsp;')})"
      end
      return txt
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if received_by_id?
         if ofc = Contact.find_by_id(received_by_id)
            self.received_by = ofc.fullname
            self.received_by_badge = ofc.badge_no
            self.received_by_unit = ofc.unit
         end
      end
      if dispatcher_id?
         if ofc = Contact.find_by_id(dispatcher_id)
            self.dispatcher = ofc.fullname
            self.dispatcher_badge = ofc.badge_no
            self.dispatcher_unit = ofc.unit
         end
      end
      if detective_id?
         if ofc = Contact.find_by_id(detective_id)
            self.detective = ofc.fullname
            self.detective_badge = ofc.badge_no
            self.detective_unit = ofc.unit
         end
      end
      return true
   end
   
   # ensures that the dispatched units are saved when the call sheet is 
   # updated
   def save_call_units
      call_units.each{|u| u.save(false)}
   end
   
   # ensures that the subjects are saved when the call sheet is updated
   def save_call_subjects
      call_subjects.each{|s| s.save(false)}
   end
   
   # looks up the signal from the entered signal code (option short name) and
   # sets signal_code_id from the matching record.
   def check_signal
      retval = true
      if signal.nil? || signal.blank?
         self.errors.add(:signal, "must not be blank!")
         retval = false
      else
         self.signal_code_id = Option.lookup("Signal Code",signal.upcase,"short_name")
         if signal_code.nil?
            self.errors.add(:signal, "not recognized!  Please check spelling.")
            retval = false
         end
      end
      return retval
   end

   # accepts a query hash and converts it to an SQL partial suitable for use
   # in a WHERE clause
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ["received_via_id", "disposition_id", "ward", "district", "signal_code_id"]
      condition = ""
      query.each do |key, value|
         if key == "name" || (key == "simple" && value.to_i == 0)
            parts = parse_name(value)
            unless parts[:first].blank?
               unless condition.empty?
                  condition << " and "
               end
               condition << "call_subjects.name like '%#{parts[:first]}%'"
            end
            unless parts[:last].blank?
               unless condition.empty?
                  condition << " and "
               end
               condition << "call_subjects.name like '%#{parts[:last]}%'"
            end
            unless parts[:middle].blank?
               unless condition.empty?
                  condition << " and "
               end
               condition << "call_subjects.name like '%#{parts[:middle]}%'"
            end
            unless parts[:suffix].blank?
               unless condition.empty?
                  condition << " and "
               end
               condition << "call_subjects.name like '%#{parts[:suffix]}%'"
            end
         elsif key == "case_no" || (key == "simple" && value.to_i > 0)
            unless condition.empty?
               condition << " and "
            end
            condition << "calls.case_no like '#{value}'"
         elsif key == "call_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "calls.call_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "call_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "calls.call_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == 'units'
            unit_parts = value.clone.split
            unless condition.empty?
               condition << " and "
            end
            condition << "("
            cond_parts = []
            unit_parts.each do |u|
               cond_parts.push("call_units.officer LIKE '%#{u}%' OR call_units.officer_unit LIKE '#{u}'")
            end
            condition << cond_parts.join(' OR ')
            condition << ")"
         else
            unless condition.empty?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "calls.#{key} = '#{value}'"
            else
               condition << "calls.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end

   # when call sheet is destroyed, ensures that the call_id on linked
   # offenses and forbids are set to nil
   def check_and_destroy_dependencies
      self.offenses.clear
      self.forbids.clear
      return true
   end

   # if followup_no is set, move it to case_no.  if the case number is left
   # blank, call the Case model to get the next one in sequence.
   def check_or_generate_case
      self.followup_no = fix_case(followup_no)
      unless followup_no.blank?
         unless Case.valid_case(followup_no)
            self.errors.add(:followup_no, "must be a valid existing case!")
            return false
         end
         self.case_no = followup_no
      end
      self.case_no = fix_case(case_no)
      unless case_no?
         self.case_no = Case.get_next_case_number
      end
      return true
   end
   
   # calls the valid_time() method for each time type field (valid_time
   # inserts the : between hours and minutes if its omitted).
   def fix_times
      self.call_time = valid_time(call_time)
      self.dispatch_time = valid_time(dispatch_time)
      self.arrival_time = valid_time(arrival_time)
      self.concluded_time = valid_time(concluded_time)
   end
end
