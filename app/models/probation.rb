# == Schema Information
#
# Table name: probations
#
#  id                      :integer(4)      not null, primary key
#  person_id               :integer(4)
#  status_id               :integer(4)
#  start_date              :date
#  end_date                :date
#  completed_date          :date
#  revoked_date            :date
#  revoked_for             :string(255)
#  hold_if_arrested        :boolean(1)      default(TRUE)
#  probation_days          :integer(4)      default(0)
#  probation_months        :integer(4)      default(0)
#  probation_years         :integer(4)      default(0)
#  costs                   :decimal(12, 2)  default(0.0)
#  notes                   :text
#  remarks                 :text
#  created_by              :integer(4)      default(1)
#  updated_by              :integer(4)      default(1)
#  created_at              :datetime
#  updated_at              :datetime
#  probation_fees          :decimal(12, 2)  default(0.0)
#  idf_amount              :decimal(12, 2)  default(0.0)
#  dare_amount             :decimal(12, 2)  default(0.0)
#  restitution_amount      :decimal(12, 2)  default(0.0)
#  probation_officer       :string(255)
#  probation_officer_unit  :string(255)
#  probation_officer_badge :string(255)
#  probation_officer_id    :integer(4)
#  fines                   :decimal(12, 2)  default(0.0)
#  docket_id               :integer(4)
#  court_id                :integer(4)
#  da_charge_id            :integer(4)
#  doc                     :boolean(1)      default(FALSE)
#  parish_jail             :boolean(1)      default(FALSE)
#  trial_result            :string(255)
#  default_days            :integer(4)      default(0)
#  default_months          :integer(4)      default(0)
#  default_years           :integer(4)      default(0)
#  pay_by_date             :date
#  sentence_days           :integer(4)      default(0)
#  sentence_months         :integer(4)      default(0)
#  sentence_years          :integer(4)      default(0)
#  sentence_suspended      :boolean(1)      default(FALSE)
#  suspended_except_days   :integer(4)      default(0)
#  suspended_except_months :integer(4)      default(0)
#  suspended_except_years  :integer(4)      default(0)
#  reverts_to_unsup        :boolean(1)      default(FALSE)
#  reverts_conditions      :string(255)
#  credit_served           :boolean(1)      default(FALSE)
#  service_days            :integer(4)      default(0)
#  service_served          :integer(4)      default(0)
#  sap                     :boolean(1)      default(FALSE)
#  sap_complete            :boolean(1)      default(FALSE)
#  driver                  :boolean(1)      default(FALSE)
#  driver_complete         :boolean(1)      default(FALSE)
#  anger                   :boolean(1)      default(FALSE)
#  anger_complete          :boolean(1)      default(FALSE)
#  sat                     :boolean(1)      default(FALSE)
#  sat_complete            :boolean(1)      default(FALSE)
#  random_screens          :boolean(1)      default(FALSE)
#  no_victim_contact       :boolean(1)      default(FALSE)
#  art893                  :boolean(1)      default(FALSE)
#  art894                  :boolean(1)      default(FALSE)
#  art895                  :boolean(1)      default(FALSE)
#  sentence_notes          :text
#  docket_no               :string(255)
#  conviction              :string(255)
#  conviction_count        :integer(4)
#  restitution_date        :date
#  probation_hours         :integer(4)      default(0)
#  default_hours           :integer(4)      default(0)
#  sentence_hours          :integer(4)      default(0)
#  suspended_except_hours  :integer(4)      default(0)
#  service_hours           :integer(4)      default(0)
#  fund1_name              :string(255)
#  fund2_name              :string(255)
#  fund3_name              :string(255)
#  fund4_name              :string(255)
#  fund5_name              :string(255)
#  fund6_name              :string(255)
#  fund7_name              :string(255)
#  fund8_name              :string(255)
#  fund9_name              :string(255)
#  fund1_charged           :decimal(12, 2)  default(0.0)
#  fund2_charged           :decimal(12, 2)  default(0.0)
#  fund3_charged           :decimal(12, 2)  default(0.0)
#  fund4_charged           :decimal(12, 2)  default(0.0)
#  fund5_charged           :decimal(12, 2)  default(0.0)
#  fund6_charged           :decimal(12, 2)  default(0.0)
#  fund7_charged           :decimal(12, 2)  default(0.0)
#  fund8_charged           :decimal(12, 2)  default(0.0)
#  fund9_charged           :decimal(12, 2)  default(0.0)
#  fund1_paid              :decimal(12, 2)  default(0.0)
#  fund2_paid              :decimal(12, 2)  default(0.0)
#  fund3_paid              :decimal(12, 2)  default(0.0)
#  fund4_paid              :decimal(12, 2)  default(0.0)
#  fund5_paid              :decimal(12, 2)  default(0.0)
#  fund6_paid              :decimal(12, 2)  default(0.0)
#  fund7_paid              :decimal(12, 2)  default(0.0)
#  fund8_paid              :decimal(12, 2)  default(0.0)
#  fund9_paid              :decimal(12, 2)  default(0.0)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Probation Model
# holds records about past, current and future probations
class Probation < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :status, :class_name => 'Option', :foreign_key => 'status_id'
   belongs_to :person
   has_one :hold, :dependent => :destroy
   has_many :probation_payments, :dependent => :destroy
   belongs_to :docket
   belongs_to :court
   belongs_to :da_charge
   has_one :sentence
   
   # this sets up sphinx indices for this model
   define_index do
      indexes [person.lastname, person.firstname, person.middlename], :as => :name
      indexes notes
      indexes remarks
      indexes sentence_notes
      indexes conviction
      indexes docket_no
      
      has updated_at
   end
   
   before_validation :lookup_officers, :check_person_and_docket, :update_end_date
   before_create :update_fund_names
   before_save :fix_numbers
   after_save :check_creator
   
   validates_date :start_date, :end_date, :completed_date, :revoked_date, :restitution_date, :allow_nil => true
   validates_presence_of :revoked_date, :if => :revoked_for?
   validates_presence_of :revoked_for, :if => :revoked_date?
   validates_numericality_of :probation_hours, :probation_days, :probation_months, :probation_years, :sentence_hours, :sentence_days, :sentence_months, :sentence_years, :default_hours, :default_days, :default_months, :default_years, :suspended_except_hours, :suspended_except_days, :suspended_except_months, :suspended_except_years, :only_integer => true, :allow_nil => true
   validates_numericality_of :fund1_charged, :fund1_paid, :fund2_charged, :fund2_paid, :fund3_charged, :fund3_paid, :fund4_charged, :fund4_paid, :fund5_charged, :fund5_paid,
      :fund6_charged, :fund6_paid, :fund7_charged, :fund7_paid, :fund8_charged, :fund8_paid, :fund9_charged, :fund9_paid, :costs, :fines, :probation_fees, :idf_amount,
      :dare_amount, :restitution_amount, :allow_nil => true
   validates_option :status_id, :option_type => 'Probation Status'
   
   # returns a short description for the DatabaseController index view.
   def self.desc
      "Probation Database"
   end
   
   # returns true if the specified option id is in use in any of the option
   # type fields for any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(status_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   # returns a list of all case numbers used in all records.
   def self.find_used_cases
      find(:all, :select => 'case_no').collect do |c|
         unless c.case_no.blank?
            c.case_no
         end
      end
   end

   # returns a list of all records using the specified case number.
   def self.find_by_case(case_no)
      return nil if case_no.nil? || case_no.empty?
      find_all_by_case_no(case_no)
   end
   
   # returns base permissions required to View Probation
   def self.base_perms
      Perm[:view_probation]
   end

   # Checks for any state or unsupervised  probation that has passed its end
   # date and attempts to update it to completed so it will no longer appear
   # on the person active records list.
   def self.update_probation_status
      state = Option.lookup("Probation Status","State",nil,nil,true)
      unsup = Option.lookup("Probation Status","Unsupervised",nil,nil,true)
      comp = Option.lookup("Probation Status", "Completed",nil,nil,true)
      find(:all, :conditions => ["(status_id = ? OR status_id = ?) AND end_date < ?",state,unsup,Date.today]).each do |p|
         p.status_id = comp
         p.completed_date = Date.today
         p.updated_by = 1
         p.save(false)
      end
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["probation_officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # returns all probations still active (status = 'Active')
   # this equates to a Parish Probation List
   def self.all_active
      active = Option.lookup("Probation Status", "Active")
      return [] if active.nil?
      find(:all, :include => :person, :order => 'people.lastname, people.firstname, people.middlename, people.suffix, probations.id', :conditions => ["probations.status_id = ?",active])
   end
   
   # returns all payment transactions for the past _days_ days.
   def past_days(days = 30)
      date = Date.today - days.days
      ProbationPayment.find(:all, :conditions => ['probation_id = ? and transaction_date >= ?',id,date])
   end
   
   # returns all payment transactions for the prior month
   def prior_month_payments
      # calculate a date falling into last month
      date = Date.today - 1.month
      # get the first and last dates for the month
      start_date = date.beginning_of_month
      end_date = date.end_of_month
      ProbationPayment.find(:all, :conditions => ['probation_id = ? and transaction_date >= ? and transaction_date <= ?', id, start_date, end_date])
   end
   
   # returns all payment transactions for the current month
   def current_month_payments
      # get today's date
      date = Date.today
      # get the first date for the month
      start_date = date.beginning_of_month
      ProbationPayment.find(:all, :conditions => ['probation_id = ? and transaction_date >= ?', id, start_date])
   end
   
   def fund1_balance
      (fund1_charged.nil? ? 0 : fund1_charged) - (fund1_paid.nil? ? 0 : fund1_paid)
   end
   
   def fund2_balance
      (fund2_charged.nil? ? 0 : fund2_charged) - (fund2_paid.nil? ? 0 : fund2_paid)
   end
   
   def fund3_balance
      (fund3_charged.nil? ? 0 : fund3_charged) - (fund3_paid.nil? ? 0 : fund3_paid)
   end
   
   def fund4_balance
      (fund4_charged.nil? ? 0 : fund4_charged) - (fund4_paid.nil? ? 0 : fund4_paid)
   end
   
   def fund5_balance
      (fund5_charged.nil? ? 0 : fund5_charged) - (fund5_paid.nil? ? 0 : fund5_paid)
   end
   
   def fund6_balance
      (fund6_charged.nil? ? 0 : fund6_charged) - (fund6_paid.nil? ? 0 : fund6_paid)
   end
   
   def fund7_balance
      (fund7_charged.nil? ? 0 : fund7_charged) - (fund7_paid.nil? ? 0 : fund7_paid)
   end
   
   def fund8_balance
      (fund8_charged.nil? ? 0 : fund8_charged) - (fund8_paid.nil? ? 0 : fund8_paid)
   end
   
   def fund9_balance
      (fund9_charged.nil? ? 0 : fund9_charged) - (fund9_paid.nil? ? 0 : fund9_paid)
   end
   
   def fund1_name=(new_name)
      unless !fund1_name.blank? || !new_name.is_a?(String) || new_name.blank?
         super(new_name)
      end
   end
   
   def fund2_name=(new_name)
      unless !fund2_name.blank? || !new_name.is_a?(String) || new_name.blank?
         super(new_name)
      end
   end
   
   def fund3_name=(new_name)
      unless !fund3_name.blank? || !new_name.is_a?(String) || new_name.blank?
         super(new_name)
      end
   end
   
   def fund4_name=(new_name)
      unless !fund4_name.blank? || !new_name.is_a?(String) || new_name.blank?
         super(new_name)
      end
   end
   
   def fund5_name=(new_name)
      unless !fund5_name.blank? || !new_name.is_a?(String) || new_name.blank?
         super(new_name)
      end
   end
   
   def fund6_name=(new_name)
      unless !fund6_name.blank? || !new_name.is_a?(String) || new_name.blank?
         super(new_name)
      end
   end
   
   def fund7_name=(new_name)
      unless !fund7_name.blank? || !new_name.is_a?(String) || new_name.blank?
         super(new_name)
      end
   end
   
   def fund8_name=(new_name)
      unless !fund8_name.blank? || !new_name.is_a?(String) || new_name.blank?
         super(new_name)
      end
   end
   
   def fund9_name=(new_name)
      unless !fund9_name.blank? || !new_name.is_a?(String) || new_name.blank?
         super(new_name)
      end
   end
   
   def total_charged
      total = BigDecimal.new("0.0",2)
      (1..9).each do |x|
         f = send("fund#{x}_charged")
         total += (f.nil? ? 0 : f)
      end
      total
   end
   
   def total_paid
      total = BigDecimal.new("0.0",2)
      (1..9).each do |x|
         f = send("fund#{x}_paid")
         total += (f.nil? ? 0 : f)
      end
      total
   end

   # returns the sum of all balances due
   def total_balance
      total_charged - total_paid
   end
   
   # returns the calculated balance at the beginning of start_date
   def balance_on(start_date)
      sdate = valid_date(start_date)
      if sdate.nil?
         return total_balance
      end
      # get balance right now
      bal = total_balance
      # collect transactions from start_date onward
      ProbationPayment.locate_transactions_by_probation(id, sdate).each do |p|
         bal = bal - p.total_charges + p.total_payments
      end
      bal
   end
   
   # returns number of months to charge probation fees
   def payment_months
      total = 0
      if probation_hours?
         total += probation_hours
      end
      if probation_days?
         total += (probation_days * 24)
      end
      if probation_months?
         total += (probation_months * 30 * 24)
      end
      if probation_years?
         total += (probation_years * 365 * 24)
      end
      mnths = ((total / 24.0) / 30.0).ceil
      return mnths
   end
   
   # returns a string combining the sentence fields
   def sentence_string
      sentence_to_string(sentence_hours, sentence_days, sentence_months, sentence_years)
   end
   
   # returns a string combining the suspended_except fields
   def suspended_except_string
      sentence_to_string(suspended_except_hours, suspended_except_days, suspended_except_months, suspended_except_years)
   end

   # returns a string combining the default fields
   def default_string
      sentence_to_string(default_hours, default_days, default_months, default_years)
   end

   # returns a string combining the probation fields
   def probation_string
      sentence_to_string(probation_hours, probation_days, probation_months, probation_years)
   end
   
   def community_service_complete?
      if service_days?
         if service_served?
            return service_days <= service_served
         else
            return false
         end
      elsif service_hours?
         if service_served?
            return service_hours <= service_served
         else
            return false
         end
      end
      return true
   end
   
   private
   
   # makes sure all sentence type numbers are not nil
   # for some reason, the database defaults are not working reliably
   def fix_numbers
      ['sentence','default','suspended_except','probation'].each do |t|
         ['hours','days','months','years'].each do |s|
            if send("#{t}_#{s}").blank?
               self.send("#{t}_#{s}=",0)
            end
         end
      end
      (1..9).each do |x|
         self.send("fund#{x}_charged=",0) if send("fund#{x}_charged").nil?
         self.send("fund#{x}_paid=",0) if send("fund#{x}_paid").nil?
      end
      return true
   end
   
   # adds fund names for newly created probations
   def update_fund_names
      (1..9).each do |x|
         self.send("fund#{x}_name=",SiteConfig.send("probation_fund#{x}"))
      end
      return true
   end
   
   # if start date is entered, and probation length is entered, calculates
   # and updates end_date accordingly
   def update_end_date
      unless end_date?
         if start_date? && (probation_hours? || probation_days? || probation_months? || probation_years?)
            hrs = sentence_to_hours(probation_hours, probation_days, probation_months, probation_years)
            self.end_date = start_date + hrs.hours
         end
      end
   end
   
   # checks that docket_no w/o docket_id does not match any docket for person
   # (manual entry of probation without docket record - for old probations)
   # unless da_charge_id is also specified (manual entry of probation prior to
   # sentence entry - done when probation officer needs to accept payment but
   # court clerk has not entered sentence yet),
   # and that if docket_id is entered (by sentence processing)
   # it is valid and the docket_no is copied from the docket
   def check_person_and_docket
      retval = true
      # first, check the person
      if person_id?
         if person.nil?
            self.errors.add(:person_id, 'is not a valid Person ID!')
            retval = false
         end
      else
         self.errors.add(:person_id, 'is required!')
         retval = false
      end
      # now check docket for that person
      if person_id? && !docket_id?
         if docket_no?
            # see if docket exists
            if d = Docket.find(:first, :conditions => {:docket_no => docket_no, :person_id => person_id})
               # no docket id entered, but we have a matching docket by docket_no
               if da_charge_id?
                  probs = Probation.find(:all, :conditions => {:person_id => person_id, :docket_no => docket_no, :da_charge_id => da_charge_id}).reject{|p| p.id = self.id}
                  if probs.size > 0
                     self.errors.add(:da_charge_id, 'already has a probation record associated with it.  You cannot add another probation for that charge!')
                     retval = false
                  end
                  if c = DaCharge.find_by_id(da_charge_id)
                     unless c.sentence.nil?
                        self.errors.add(:da_charge_id, 'has already been sentenced, you cannot add a manual probation for that charge!')
                        retval = false
                     end
                  end
                  unless d.da_charges.collect{|c| c.id}.include?(da_charge_id)
                     # pre-entry with da_charge_id that is bogus
                     self.errors.add(:da_charge_id, 'is not valid for this docket!')
                     retval = false
                  end
               else
                  # manual entry (without charge id) and docket already exists - disallow it
                  self.errors.add(:docket_no, 'exists, cannot do manual probation entries for existing dockets without specifying a Charge ID!')
                  self.errors.add(:da_charge_id, 'must be specified when creating probation entries for existing dockets!')
                  retval = false
               end
            end
         else
            self.errors.add(:docket_no, 'is required!')
            retval = false
         end
      end
      return retval
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
      
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if probation_officer_id?
         if ofc = Contact.find_by_id(probation_officer_id)
            self.probation_officer = ofc.fullname
            self.probation_officer_badge = ofc.badge_no
            self.probation_officer_unit = ofc.unit
         end
      end
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      numtypes = ["status_id", "hold_if_arrested", "person_id", "id"]
      query.each do |key, value|
         if key == 'id'
            unless condition.blank?
               condition << " and "
            end
            condition << "probations.id = '#{value}'"
         elsif key == "name"
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.firstname like '#{nameparts[:first]}'"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.middlename like '#{nameparts[:middle]}'"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.lastname like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.suffix like '#{nameparts[:suffix]}'"
            end
         elsif key == "start_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "probations.start_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "start_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "probations.start_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "end_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "probations.end_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "end_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "probations.end_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "completed_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "probations.completed_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "completed_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "probations.completed_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "revoked_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "probations.revoked_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "revoked_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "probations.revoked_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else
            unless condition.empty?
               condition << " and "
            end
            if numtypes.include?(key)
               condition << "probations.#{key} = '#{value}'"
            else
               condition << "probations.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
