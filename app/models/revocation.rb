# == Schema Information
#
# Table name: revocations
#
#  id                  :integer(4)      not null, primary key
#  revocation_date     :date
#  docket_id           :integer(4)
#  judge_id            :integer(4)
#  arrest_id           :integer(4)
#  revoked             :boolean(1)
#  time_served         :boolean(1)
#  consecutive_type_id :integer(4)
#  total_days          :integer(4)      default(0)
#  total_months        :integer(4)      default(0)
#  total_years         :integer(4)      default(0)
#  doc                 :boolean(1)
#  parish_days         :integer(4)      default(0)
#  parish_months       :integer(4)      default(0)
#  parish_years        :integer(4)      default(0)
#  processed           :boolean(1)
#  remarks             :text
#  created_by          :integer(4)      default(1)
#  updated_by          :integer(4)      default(1)
#  created_at          :datetime
#  updated_at          :datetime
#  process_status      :string(255)
#  total_hours         :integer(4)      default(0)
#  parish_hours        :integer(4)      default(0)
#  delay_execution     :boolean(1)      default(FALSE)
#  deny_goodtime       :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class Revocation < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :docket
   belongs_to :judge, :class_name => 'Option', :foreign_key => 'judge_id'
   belongs_to :arrest
   belongs_to :consecutive_type, :class_name => 'Option', :foreign_key => 'consecutive_type_id'
   has_one :hold, :dependent => :destroy
   has_one :doc_record, :class_name => 'Doc', :dependent => :destroy
   has_and_belongs_to_many :consecutive_dockets, :class_name => 'Docket', :join_table => 'dockets_revocations'

   attr_accessor :force
   
   before_validation :check_arrest
   after_create :check_docket
   before_save :fix_numbers, :process
   after_save :check_creator
   after_update :save_children
   before_destroy :check_and_destroy_dependencies
   
   validates_numericality_of :total_hours, :total_days, :total_months, :total_years, :parish_hours, :parish_days, :parish_months, :parish_years, :only_integer => true, :allow_nil => true
   validates_date :revocation_date
   validates_option :judge_id, :option_type => 'Judge'
   
   def self.desc
      'Revocations for Dockets'
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_court]
   end
   
   # returns true if the specified option id is in use in any of the option
   # type fields in any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(consecutive_type_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # takes a one or more revocation id numbers and returns the total hours
   # served for the revocation sentences - correctly considers overlaps
   def self.hours_served(*revocation_ids)
      revocations = [revocation_ids].flatten.compact.uniq.collect{|r| Revocation.find_by_id(r)}.compact
      return 0 if revocations.empty?
      ranges = []
      revocations.each do |r|
         if r.hold.nil?
            unless r.doc_record.nil? || r.doc_record.hold.nil?
               hold_datetime = get_datetime(r.doc_record.hold.hold_date, r.doc_record.hold.hold_time)
               datetime = hold_datetime
               unless r.doc_record.hold.booking.nil?
                  booking_datetime = get_datetime(r.doc_record.hold.booking.booking_date, r.doc_record.hold.booking.booking_time)
                  datetime = [hold_datetime, booking_datetime].sort.pop
               end
                ranges.push([datetime, get_datetime(r.doc_record.hold.cleared_date, r.doc_record.hold.cleared_time)])
            end
         else
            hold_datetime = get_datetime(r.hold.hold_date, r.hold.hold_time)
            datetime = hold_datetime
            unless r.hold.booking.nil?
               booking_datetime = get_datetime(r.hold.booking.booking_date, r.hold.booking.booking_time)
               datetime = [hold_datetime, booking_datetime].sort.pop
            end
            ranges.push([datetime, get_datetime(r.hold.cleared_date, r.hold.cleared_time)])
         end
      end
      return 0 if ranges.empty?
      ranges = ranges.compact
      calculate_time_served(ranges)
   end
   
   def total_string
      sentence_to_string(total_hours, total_days, total_months, total_years)
   end
   
   def parish_string
      sentence_to_string(parish_hours, parish_days, parish_months, parish_years)
   end
   
   # return true if this revocation will result in a serving sentence hold
   def jail_time?
      return false if !revoked? && parish_string.blank?
      return true
   end
   
   # look at all sentences for this docket and calculates hours, days, months, years total taking
   # concurrency (within this docket) into consideration
   def total_of_sentences
      hours_to_sentence(docket.total_sentence_length)
   end
   
   # look at all arrests for this docket and calculate total days time served
   def total_of_time_served
      hrs_served = 0
      hrs_served += Arrest.hours_served(docket.arrests.collect{|a| a.id})
      hrs_served += Sentence.hours_served(docket.sentences.collect{|s| s.id})
      hrs_served += Revocation.hours_served(docket.revocations.collect{|r| r.id})
      return hrs_served
   end
   
   # returns time served on probation revocation arrest
   def revocation_time_served
      return 0 if arrest.nil?
      return 0 if arrest.holds.empty?
      return 0 unless book = arrest.holds.first.booking
      release = (book.release_date? ? get_datetime(book.release_date, book.release_time) : DateTime.now)
      return hours_diff(release, get_datetime(arrest.arrest_date, arrest.arrest_time))
   end
   
   def status
      txt = []
      if doc?
         txt.push('[DOC]')
      end
      if processed?
         txt.push('[Processed]')
      else
         txt.push('[Pending]')
      end
      unless total_string.blank? && parish_string.blank?
         txt.push('[Sentenced]')
         if hold.nil?
            txt.push('[Missing Hold]')
         else
            if hold.cleared_date?
               txt.push('[Time Served]')
            else
               txt.push('[Serving Time]')
            end
         end
         if doc? && doc_record.nil?
            txt.push('[Missing DOC]')
         end
      else
         txt.push('[Not Sentenced]')
      end
      return txt.join(' ')
   end
   
   # return a list of the consecutive sentencing holds as determined by the consecutive_type option
   def consecutive_holds
      return [] if consecutive_type.nil?
      if consecutive_type.long_name == "Any Current"
         hold_list = []
         unless docket.nil? || docket.person.nil? || docket.person.bookings.empty? || docket.person.bookings.last.release_date? || docket.person.bookings.last.holds.empty?
            docket.person.bookings.last.holds.reject{|h| h.cleared_date? || h.hold_type.nil? || h.hold_type.long_name != 'Serving Sentence'}.each do |h|
               hold_list.push(hold)
            end
         end
         return hold_list
      elsif consecutive_type.long_name == "Other Dockets" || consecutive_type.long_name == "This Docket"
         hold_list = []
         if consecutive_type.long_name == "Other Dockets"
            docket_list = consecutive_dockets
         else
            docket_list = [docket]
         end
         docket_list.each do |d|
            d.da_charges.each do |c|
               # if the charge has not been processed, we can't calculate consecutives yet
               return nil unless c.processed?
               # if the charge does not have a sentence, skip it
               next if c.sentence.nil?
               # if the sentence is this one, skip it
               next if c.sentence.id == self.id
               # if the sentence does not have a hold or doc record, its not for serving time
               next if c.sentence.hold.nil? && c.sentence.doc_record.nil?
               if c.sentence.doc_record.nil?
                  # return sentence hold
                  hold_list.push(c.sentence.hold)
               else
                  return nil if c.sentence.doc_record.hold.nil?
                  hold_list.push(c.sentence.doc_record.hold)
               end
            end
         end
         return hold_list
      else
         # unknown consecutive_type - ignore it
         return []
      end
   end
   
   private
   
   # makes sure all sentence type numbers are not nil
   # for some reason, the database defaults are not working reliably
   def fix_numbers
      ['total','parish'].each do |t|
         ['hours','days','months','years'].each do |s|
            if send("#{t}_#{s}").blank?
               self.send("#{t}_#{s}=",0)
            end
         end
      end
      return true
   end
   
   def check_and_destroy_dependencies
      self.consecutive_dockets.clear
      return true
   end
   
   def process
      # only calling this if revocation is new or updated
      self.processed = false
      
      # make sure attached docket is also marked as unprocessed
      unless docket.nil? || !docket.processed?
         self.docket.update_attribute(:processed, false)
      end
      
      success = true
      self.process_status = ""
      
      if docket.nil?
         success = false
         self.process_status << "[No Linked Docket]"
      elsif docket.person.nil?
         success = false
         self.process_status << "[Docket has Invalid Person]"
      end
      
      total_to_serve = 0
      if revoked?
         unless total_string.blank?
            total_to_serve = sentence_to_hours(total_hours, total_days, total_months, total_years)
         end
      else
         unless parish_string.blank?
            total_to_serve = sentence_to_hours(parish_hours, parish_days, parish_months, parish_years)
         end
      end
      remaining_to_serve = total_to_serve
      if time_served?
         ts = total_of_time_served
         if ts > 0
            remaining_to_serve -= ts
            rem = "\nTo Serve time reflects #{ts} Hours Credit for Time Served."
         end
      end
      remaining_hours, remaining_days, remaining_months, remaining_years = hours_to_sentence(remaining_to_serve)   
      
      if success
         # check consecutive holds
         consecs = consecutive_holds
         if consecs.nil?
            # not ready for processing
            self.process_status << "[Revocation Consecutive to Unprocessed Sentences]"
            success = false
         end
      end
      
      if success && jail_time?
         # validate booking
         if docket.person.bookings.empty? || (hold.nil? && docket.person.bookings.last.release_date?)
            # create booking only if delay_execution set
            if delay_execution? || (force == true && (docket.person.bookings.empty? || (docket.person.bookings.last.release_date < revocation_date)))
               # creating a booking because:
               # 1. delayed execution
               # 2. forcing processing and no bookings exist
               # 3. forcing processing and last booking was released before the court date
               book = Booking.new(
                  :person_id => docket.person_id,
                  :disable_timers => true,
                  :created_by => updated_by,
                  :updated_by => updated_by,
                  :remarks => 'Created Automatically By Revocation Processing.'
               )
               book.booking_date, book.booking_time = get_date_and_time
               if force == true
                  book.release_date = book.booking_date
                  book.release_time = book.booking_time
                  book.release_officer = book.booking_officer
                  book.release_officer_unit = book.booking_officer_unit
                  book.release_officer_badge = book.booking_officer_badge
                  book.released_because_id = Option.lookup("Release Reason","Automatic Release",nil,nil,true)
                  book.remarks = 'Created and Released Automatically By Revocation Processing.'
               end
               book.save(false)
               unless force == true
                  # booking sucessful, add temp release hold
                  newhold = Hold.new(
                     :booking_id => book.id,
                     :hold_type_id => Option.lookup("Hold Type","Temporary Release",nil,nil,true),
                     :temp_release_reason_id => Option.lookup("Temporary Release Reason","Awaiting Execution Of Sentence",nil,nil,true),
                     :hold_by => updater.name,
                     :hold_by_unit => updater.unit,
                     :hold_by_badge => updater.badge,
                     :created_by => updated_by,
                     :updated_by => updated_by,
                     :remarks => 'Created Automatically By Revocation Processing'
                  )
                  newhold.hold_date, newhold.hold_time = get_date_and_time
                  if newhold.valid?
                     newhold.save
                     # add pending sentence hold (this sentence may not process successfully)
                     newhold = Hold.new(
                        :booking_id => book.id,
                        :hold_type_id => Option.lookup("Hold Type","Pending Sentence",nil,nil,true),
                        :hold_by => updater.name,
                        :hold_by_unit => updater.unit,
                        :hold_by_badge => updater.badge,
                        :created_by => updated_by,
                        :updated_by => updated_by,
                        :remarks => 'Created Automatically By Revocation Processing'
                     )
                     newhold.hold_date, newhold.hold_time = get_date_and_time
                     if newhold.valid?
                        newhold.save
                     else
                        self.process_status << '[Unable to Create Pending Sentence]'
                        self.process_status << "(#{newhold.errors.full_messages.join(', ')})"
                        success = false
                     end
                  else
                     self.process_status << '[Unable to Create Temporary Release]'
                     self.process_status << "(#{newhold.errors.full_messages.join(', ')})"
                     success = false
                  end
               end
            elsif force == true && docket.person.bookings.last.release_date?
               book = docket.person.bookings.last
            else
               success = false
               self.process_status << '[Invalid Booking]'
            end
         else
            book = docket.person.bookings.last
         end
         
         if success
            # should be ready to post revocation
            if doc?
               # Hard Labor Dept of Corrections
               
               # check is parish hold exists
               if !hold.nil? && !hold.doc?
                  # oops, has parish hold, get rid of it
                  hold.destroy
               end
               
               d = doc_record
               if d.nil?
                  d = self.build_doc_record
               end
               d.sentence_hours = total_hours
               d.sentence_days = total_days
               d.sentence_months = total_months
               d.sentence_years = total_years
               d.to_serve_hours = remaining_hours
               d.to_serve_days = remaining_days
               d.to_serve_months = remaining_months
               d.to_serve_years = remaining_years
               d.conviction = docket.sentence_summary
               d.remarks = (rem.nil? ? remarks : remarks + rem)
               unless d.created_by?
                  d.created_by = updated_by
               end
               d.updated_by = updated_by
               d.booking_id = docket.person.bookings.last.id
               d.person_id = docket.person_id
               if force == true
                  d.force = true
               end
               unless d.valid?
                  success = false
                  self.process_status << '[Unable to Create DOC Record]'
                  self.process_status << "(#{d.errors.full_messages.join(', ')})"
               end
            else
               # parish jail
               
               # make sure there is not a doc record already
               unless doc_record.nil?
                  # oops, get rid of it
                  doc_record.destroy
               end
               
               h = hold
               if h.nil?
                  h = self.build_hold
               end
               served = book.hours_served_since(get_datetime(revocation_date, '12:00'))
               h.booking_id = book.id
               h.hold_date = revocation_date
               h.hold_time = "12:00"
               h.hold_type_id = Option.lookup("Hold Type","Serving Sentence",nil,nil,true)
               h.hold_by_id = creator.contact_id
               h.hold_by = creator.name
               h.hold_by_unit = creator.unit
               h.hold_by_badge = creator.badge
               h.remarks = "Added Automatically by Revocation Processing"
               unless h.created_by?
                  h.created_by = updated_by
               end
               h.updated_by = updated_by
               unless h.hours_served? && h.hours_served > 0
                  h.hours_served = served
               end
               h.hours_total = total_to_serve
               if time_served?
                  unless h.hours_time_served? && h.hours_time_served > 0
                     h.hours_time_served = total_of_time_served
                  end
               else
                  h.hours_time_served = 0
               end
               unless h.datetime_counted?
                  h.datetime_counted = DateTime.now
               end
               if force == true && book.release_date?
                  h.cleared_date = h.hold_date
                  h.cleared_time = h.hold_time
                  h.cleared_because_id = Option.lookup("Release Type","Automatic Release",nil,nil,true)
                  h.cleared_by_id = h.hold_by_id
                  h.cleared_by = h.hold_by
                  h.cleared_by_unit = h.hold_by_unit
                  h.cleared_by_badge = h.hold_by_badge
                  h.explanation = 'Inmate Released Prior To Revocation Processing because of time served or transfer.'
                  h.remarks = 'Automatically Added And Cleared by Revocation Processing.'
               end
               h.deny_goodtime = deny_goodtime
               if h.valid?
                  # need to close all pending sentence holds, if any
                  pendings = Hold.find(:all, :conditions => {:booking_id => book.id, :cleared_date => nil, :hold_type_id => Option.lookup("Hold Type","Pending Sentence",nil,nil,true)})
                  unless pendings.empty?
                     auto_release = Option.lookup('Release Type','Automatic Release',nil,nil,true)
                     pendings.each do |p|
                        p.cleared_date, p.cleared_time = get_date_and_time
                        p.cleared_because_id = auto_release
                        p.cleared_by = updater.name
                        p.cleared_by_unit = updater.unit
                        p.cleared_by_badge = updater.badge
                        p.explanation = "Automatically cleared by Revocation Processing."
                        p.updated_by = updated_by
                        p.save(false)
                     end
                  end
               else
                  self.process_status << '[Unable to Create Sentence Hold]'
                  self.process_status << "(#{h.errors.full_messages.join(', ')})"
                  success = false
               end
            end
         end
      end
      if success
         if revoked?
            unless docket.probations.empty?
               revoked_status = Option.lookup("Probation Status","Revoked",nil,nil,true)
               docket.probations.each do |p|
                  p.status_id = revoked_status
                  p.revoked_date = revocation_date
                  p.revoked_for = "Revoked by Revocation Processing of Revocation ID: #{id}"
                  p.updated_by = updated_by
                  p.save(false)
               end
            end
         end
         self.processed = true
         self.process_status = nil
      end
      self.force = false
      return true
   end
   
   def save_children
      unless doc_record.nil?
         self.doc_record.save(false)
      end
      unless hold.nil?
         self.hold.save(false)
         self.hold.blockers.clear
         c = consecutive_holds
         unless c.nil? || c.empty?
            self.hold.blockers << c
         end
      end
   end
   
   def check_docket
      unless processed?
         if docket.processed?
            self.docket.update_attribute(:processed, false)
         end
      end
   end
   
   def check_arrest
      if arrest_id?
         if arrest.nil?
            self.errors.add(:arrest_id, "is not a valid arrest!")
            return false
         elsif arrest.person_id != docket.person_id
            self.errors.add(:arrest_id, "is not for this person!")
            return false
         end
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
end
