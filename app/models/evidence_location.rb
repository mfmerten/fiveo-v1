# == Schema Information
#
# Table name: evidence_locations
#
#  id            :integer(4)      not null, primary key
#  evidence_id   :integer(4)
#  location_from :string(255)
#  location_to   :string(255)
#  officer       :string(255)
#  remarks       :text
#  created_at    :datetime
#  updated_at    :datetime
#  storage_date  :date
#  storage_time  :string(255)
#  created_by    :integer(4)      default(1)
#  updated_by    :integer(4)      default(1)
#  officer_unit  :string(255)
#  officer_id    :integer(4)
#  officer_badge :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Evidence Location Model
# Tracks an evidence as it is moved from place to place
class EvidenceLocation < ActiveRecord::Base
   belongs_to :evidence
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   
   before_validation :fix_time
   after_save :check_creator
   
   validates_presence_of :location_from, :location_to, :officer, :remarks
   validates_date :storage_date
   validates_time :storage_time
   
   # a short description of the model for the DatabaseController index view.
   def self.desc
      "Evidence Chain records"
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
      
   # inserts a colon (:) into the time value if it is missing.
   def fix_time
      self.storage_time = valid_time(storage_time)
   end
end
