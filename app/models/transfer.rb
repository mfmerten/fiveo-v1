# == Schema Information
#
# Table name: transfers
#
#  id                 :integer(4)      not null, primary key
#  transfer_date      :date
#  transfer_time      :string(255)
#  booking_id         :integer(4)
#  person_id          :integer(4)
#  from_agency        :string(255)
#  from_agency_id     :integer(4)
#  from_officer       :string(255)
#  from_officer_id    :integer(4)
#  from_officer_unit  :string(255)
#  from_officer_badge :string(255)
#  to_agency          :string(255)
#  to_agency_id       :integer(4)
#  to_officer         :string(255)
#  to_officer_id      :integer(4)
#  to_officer_unit    :string(255)
#  to_officer_badge   :string(255)
#  remarks            :text
#  reported_datetime  :datetime
#  created_by         :integer(4)      default(1)
#  updated_by         :integer(4)      default(1)
#  created_at         :datetime
#  updated_at         :datetime
#  transfer_type_id   :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class Transfer < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :booking
   belongs_to :person
   has_one :doc
   has_one :hold
   belongs_to :transfer_type, :class_name => 'Option', :foreign_key => 'transfer_type_id'
   
   # this sets up sphinx indices for this model
   define_index do
      indexes [person.lastname, person.firstname, person.middlename, person.suffix], :as => :name
      indexes transfer_date
      indexes from_agency
      indexes from_officer
      indexes from_officer_unit
      indexes to_agency
      indexes to_officer
      indexes to_officer_unit
      indexes remarks
      
      has updated_at
   end

   before_validation :fix_times, :lookup_officers
   before_destroy :check_and_destroy_dependencies
   after_save :process_transfer

   validates_presence_of :to_agency, :from_agency, :to_officer, :from_officer, :remarks
   validates_date_in_past :transfer_date
   validates_time :transfer_time
   
   def self.desc
      'Prisoner Transfer Log'
   end
   
   # returns base perms required to view these records
   def self.base_perms
      Perm[:view_booking]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["from_officer_id", "to_officer_id", "from_agency_id", "to_agency_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   private
   
   # processes the transfer information depending on the selected transfer type.
   def process_transfer
      unless transfer_type.nil? || booking.nil? || booking.release_date?
         case transfer_type.long_name
         when "Temporary Transfer Return (IN)"
            # clear the temporary transfer hold
            if hold = Hold.find(:first, :conditions => {:booking_id => booking_id, :hold_type_id => Option.lookup("Hold Type","Temporary Transfer",nil,nil,true), :cleared_date => nil, :agency => from_agency})
               hold.cleared_date = transfer_date
               hold.cleared_time = transfer_time
               hold.cleared_by_id = updater.contact_id
               hold.cleared_by = updater.name
               hold.cleared_by_unit = updater.unit
               hold.cleared_by_badge = updater.badge
               hold.updated_by = updated_by
               hold.cleared_because_id = Option.lookup("Release Type","Returned from Transfer",nil,nil,true)
               hold.explanation = "Automatic Clear by Transfer"
               hold.save(false)
            end
         when "Temporary Transfer (OUT)"
            # add a new temporary transfer hold
            hold = Hold.new(
               :transfer_id => id,
               :booking_id => booking_id,
               :hold_date => transfer_date,
               :hold_time => transfer_time,
               :hold_type_id => Option.lookup("Hold Type","Temporary Transfer",nil,nil,true),
               :hold_by_id => from_officer_id,
               :hold_by => from_officer,
               :hold_by_unit => from_officer_unit,
               :hold_by_badge => from_officer_badge,
               :remarks => remarks,
               :created_by => updated_by,
               :updated_by => updated_by,
               :agency => to_agency,
               :transfer_officer_id => to_officer_id,
               :transfer_officer => to_officer,
               :transfer_officer_unit => to_officer_unit,
               :transfer_officer_badge => to_officer_badge
            )
            hold.save(false)
         when "Temporary Housing (IN)"
            # add a new temporary housing hold
            hold = Hold.new(
               :transfer_id => id,
               :booking_id => booking_id,
               :hold_date => transfer_date,
               :hold_time => transfer_time,
               :hold_type_id => Option.lookup("Hold Type","Temporary Housing",nil,nil,true),
               :hold_by_id => to_officer_id,
               :hold_by => to_officer,
               :hold_by_unit => to_officer_unit,
               :hold_by_badge => to_officer_badge,
               :remarks => remarks,
               :created_by => updated_by,
               :updated_by => updated_by,
               :agency => from_agency,
               :transfer_officer_id => from_officer_id,
               :transfer_officer => from_officer,
               :transfer_officer_unit => from_officer_unit,
               :transfer_officer_badge => from_officer_badge
            )
            hold.save(false)
         when "Permanent Transfer (OUT)","Temporary Housing Return (OUT)"
            # clear all holds and release booking.
            self.booking.holds.reject(&:cleared_date).each do |h|
               h.cleared_date = transfer_date
               h.cleared_time = transfer_time
               h.cleared_by_id = from_officer_id
               h.cleared_by = from_officer
               h.cleared_by_unit = from_officer_unit
               h.cleared_by_badge = from_officer_badge
               h.updated_by = updated_by
               h.cleared_because_id = Option.lookup("Release Type","Transferred",nil,nil,true)
               h.explanation = "Automatic Clear by Transfer to #{to_agency} by #{to_officer_unit} #{to_officer}."
               h.save(false)
            end
            self.booking.release_officer_id = updater.contact_id
            self.booking.release_officer = updater.name
            self.booking.release_officer_unit = updater.unit
            self.booking.release_officer_badge = updater.badge
            self.booking.release_date = transfer_date
            self.booking.release_time = transfer_time
            self.booking.released_because_id = Option.lookup("Release Reason","Transferred",nil,nil,true)
            self.booking.updated_by = updated_by
            self.booking.save(false)
            self.booking.send_release_messages
         when "DOC Transfer (IN)"
            # add a DOC record.
            doc = Doc.new(
               :transfer_date => transfer_date,
               :transfer_time => transfer_time,
               :transfer_id => id,
               :billable => false,
               :remarks => remarks,
               :created_by => updated_by,
               :updated_by => updated_by,
               :sentence_id => nil,
               :revocation_id => nil,
               :doc_number => (person.nil? ? nil : person.doc_number),
               :booking_id => booking_id,
               :person_id => person_id
            )
            doc.save(false)
         end
      end
   end

   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if to_officer_id?
         if ofc = Contact.find_by_id(to_officer_id)
            self.to_officer = ofc.fullname
            self.to_officer_badge = ofc.badge_no
            self.to_officer_unit = ofc.unit
         end
      end
      if from_officer_id?
         if ofc = Contact.find_by_id(from_officer_id)
            self.from_officer = ofc.fullname
            self.from_officer_badge = ofc.badge_no
            self.from_officer_unit = ofc.unit
         end
      end
      if to_agency_id?
         if ofc = Contact.find_by_id(to_agency_id)
            self.to_agency = ofc.fullname
         end
      end
      if from_agency_id?
         if ofc = Contact.find_by_id(from_agency_id)
            self.from_agency = ofc.fullname
         end
      end
   end
   
   # calls the valid_time() method for each time type field (valid_time
   # inserts the : between hours and minutes if its omitted).
   def fix_times
      self.transfer_time = valid_time(transfer_time)
   end
   
   # causes destruction of this transfer to also destroy the doc record associated with it.
   def check_and_destroy_dependencies
      unless doc(true).nil?
         self.doc.destroy
      end
      unless hold(true).nil?
         self.hold.destroy
      end
      return true
   end

   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      intvals = ['id', 'booking_id', 'person_id', 'transfer_type_id']
      query.each do |key, value|
         if key == 'person_name'
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.firstname like '#{nameparts[:first]}'"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.middlename like '#{nameparts[:middle]}'"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.lastname like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.suffix like '#{nameparts[:suffix]}'"
            end
         elsif key == "transfer_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "transfers.transfer_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "transfer_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "transfers.transfer_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == 'agency_name'
            unless condition.blank?
               condition << " and "
            end
            condition << "(transfers.from_agency LIKE '%#{value}%' OR transfers.to_agency LIKE '%#{value}%')"
         elsif key == 'officer'
            unless condition.blank?
               condition << " and "
            end
            condition << "(from_officer LIKE '%#{value}%' OR to_officer LIKE '%#{value}%')"
         elsif key == 'officer_unit'
            unless condition.blank?
               condition << " and "
            end
            condition << "(from_officer_unit LIKE '%#{value}%' OR to_officer_unit LIKE '%#{value}%')"
         elsif key == 'officer_badge'
            unless condition.blank?
               condition << " and "
            end
            condition << "(from_officer_badge LIKE '%#{value}%' OR to_officer_badge LIKE '%#{value}%')"
         else
            unless condition.empty?
               condition << " and "
            end
            if intvals.include?(key)
               condition << "transfers.#{key} = '#{value}'"
            else
               condition << "transfers.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
