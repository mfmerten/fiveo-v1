# == Schema Information
#
# Table name: sessions
#
#  id         :integer(4)      not null, primary key
#  session_id :string(255)     not null
#  data       :text
#  created_at :datetime
#  updated_at :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Session Model
# application sessions table
class Session < ActiveRecord::Base
   # returns a short description for the DatabaseController index view.
    def self.desc
       "User Sessions"
    end
    
    # method called by a DatabaseController action to clear the session table.
    def self.reset_table
        sql = Session.connection
        sql.execute "truncate table sessions"
    end
    
   # method called by a rake task to reap old sessions
   def self.reap_expired_sessions
      if SiteConfig.session_max > SiteConfig.session_life
         destroy_all("created_at < '#{(SiteConfig.session_max + 720).minutes.ago.to_s(:db)}'")
      else
         destroy_all("created_at < '#{(SiteConfig.session_life + 720).minutes.ago.to_s(:db)}'")
      end
   end
end
