# == Schema Information
#
# Table name: invest_criminals
#
#  id                :integer(4)      not null, primary key
#  investigation_id  :integer(4)
#  invest_crime_id   :integer(4)
#  invest_report_id  :integer(4)
#  person_id         :integer(4)
#  full_name         :string(255)
#  description       :string(255)
#  height            :string(255)
#  weight            :string(255)
#  shoes             :string(255)
#  fingerprint_class :string(255)
#  dna_profile       :string(255)
#  remarks           :text
#  created_by        :integer(4)
#  updated_by        :integer(4)
#  created_at        :datetime
#  updated_at        :datetime
#  private           :boolean(1)      default(TRUE)
#  owned_by          :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Investigation Criminal
# these are the suspects for the Detective Investigation
class InvestCriminal < ActiveRecord::Base
   has_many :photos, :class_name => 'InvestPhoto'
   has_many :vehicles, :class_name => 'InvestVehicle'
   belongs_to :investigation
   belongs_to :crime, :class_name => 'InvestCrime', :foreign_key => 'invest_crime_id'
   belongs_to :report, :class_name => 'InvestReport', :foreign_key => 'invest_report_id'
   belongs_to :person
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :owner, :class_name => 'User', :foreign_key => 'owned_by'
   has_many :stolens
   has_many :warrants
   
   # this sets up sphinx indices for this model
   define_index do
      indexes full_name
      indexes description
      indexes height
      indexes weight
      indexes shoes
      indexes fingerprint_class
      indexes dna_profile
      indexes remarks

      where 'private != 1'
      
      has updated_at
   end
      
   before_validation :normalize_name
   after_save :check_creator
   
   validates_presence_of :description
   validates_person :person_id, :allow_nil => true
   
   def self.desc
      "Suspects for Detective Investigations"
   end
   
   # returns base perms required to view investigations
   def self.base_perms
      Perm[:view_investigation]
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
      
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   private
   
   # this looks at person_id and if valid, pulls the full name from there.
   # otherwise, it leaves the name alone
   def normalize_name
      unless person.nil?
         self.full_name = person.full_name
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the value of creator for investigation,
   # or the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:created_by, 1)
         else
            self.update_attribute(:created_by, investigation.created_by)
         end
      end
      if !updated_by? || updater.nil?
         if investigation.nil? || investigation.updater.nil?
            self.update_attribute(:updated_by, 1)
         else
            self.update_attribute(:updated_by, investigation.updated_by)
         end
      end
      if !owned_by? || owner.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:owned_by, 1)
         else
            self.update_attribute(:owned_by, investigation.created_by)
         end
      end
      return true
   end
end
