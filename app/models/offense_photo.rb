# == Schema Information
#
# Table name: offense_photos
#
#  id                             :integer(4)      not null, primary key
#  offense_id                     :integer(4)
#  case_no                        :string(255)
#  crime_scene_photo_file_name    :string(255)
#  crime_scene_photo_content_type :string(255)
#  crime_scene_photo_file_size    :integer(4)
#  crime_scene_photo_updated_at   :datetime
#  description                    :string(255)
#  date_taken                     :date
#  location_taken                 :string(255)
#  taken_by                       :string(255)
#  taken_by_unit                  :string(255)
#  remarks                        :text
#  created_by                     :integer(4)
#  updated_by                     :integer(4)
#  created_at                     :datetime
#  updated_at                     :datetime
#  taken_by_id                    :integer(4)
#  taken_by_badge                 :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Offense Photo Model
# Crime scene photos for the offense report.
class OffensePhoto < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :offense
   
   before_validation :adjust_case_no, :lookup_officers
   after_save :check_creator
   
   validates_presence_of :description, :location_taken, :case_no
   validates_date :date_taken
   validates_offense :offense_id
   
   # crime scene photo
   has_attached_file :crime_scene_photo,
      :url => "/system/:attachment/:id/:style/:basename.:extension",
      :path => ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
      :default_url => "/images/no_image_:style.jpg",
      :styles => {
         :thumb => "80x80",
         :original => "600x600",
         :medium => "300x300",
         :small => "150x150" }
   
   # a short description for the DatabaseController index view.
   def self.desc
      "Offense Photo Database"
   end
   
   # base permissions required to view offense photos
   def self.base_perms
      Perm[:view_offense]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["taken_by_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   private
   
   # gets officer info from contacts if possible
   def lookup_officers
      if taken_by_id?
         if c = Contact.find_by_id(taken_by_id)
            self.taken_by = c.fullname
            self.taken_by_unit = c.unit
            self.taken_by_badge = c.badge_no
         end
      end
      true
   end
   
   # strips all characters from case_no except for numbers, letters, and -
   def adjust_case_no
      self.case_no = fix_case(case_no)
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end

   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = %w(offense_id)
      # convert to actual field names and build an sql WHERE string
      condition = ""
      query.each do |key, value|
         if key == "date_taken_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "offense_photos.date_taken >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "date_taken_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "offense_photos.date_taken <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else                
            unless condition.empty?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "offense_photos.#{key} = '#{value}'"
            else
               condition << "offense_photos.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
