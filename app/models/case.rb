# == Schema Information
#
# Table name: cases
#
#  id         :integer(4)      not null, primary key
#  assigned   :string(255)
#  created_at :datetime
#  updated_at :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Case Model
# This is the case number sequence table.
class Case < ActiveRecord::Base
   # a short description of the model used by the DatabaseController index
   # view.
    def self.desc
       "Case Number sequence table"
    end
    
    # Generates the next case number in sequence.
    # * If records exist, the current year is first compared to the year
    #   prefix of the first record.  If different, the table is cleared.
    # * If no records exist, the first record is created and the case number
    #   is generated using the current year and the record id (1).
    # * If records already exist, a record is appended and the case number is
    #   generated using the prefix from the first record and the id from the
    #   new record.
    def self.get_next_case_number
        if Case.count > 0
            mycentury = find(1).assigned.slice(0,4)
            if mycentury < Date.today.year.to_s
                sql = Case.connection
                sql.execute "truncate table cases"
                mycentury = Date.today.year.to_s
            end
        else
            mycentury = Date.today.year.to_s
        end
        mycase = create(:assigned => nil)
        caseno = "#{mycentury}#{mycase.id.to_s.rjust(5,'0')}"
        mycase.update_attribute(:assigned, caseno)
        return caseno
    end
    
    # returns true if specified case number is valid (used somewhere in
    # a record, not necessarily in the cases table)
    def self.valid_case(case_no)
       models = []
       Dir.chdir(File.join(RAILS_ROOT, "app/models")) do 
          models = Dir["*.rb"]
       end
       @items = []
       models.each do |m|
          model_name = m.sub(/\.rb$/,'')
          class_name = model_name.camelize
          next if class_name == 'InvestCase'
          if (class_name.constantize.respond_to?('column_names') && class_name.constantize.column_names.include?('case_no')) || model_name == 'investigation'
             class_name.constantize.find_all_by_case_no(case_no).each do |c|
                next if c.respond_to?('private') && c.private?
                return true
             end
          end
       end
       return false
    end

    private
end
