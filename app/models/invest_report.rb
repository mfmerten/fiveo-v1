# == Schema Information
#
# Table name: invest_reports
#
#  id               :integer(4)      not null, primary key
#  investigation_id :integer(4)
#  invest_crime_id  :integer(4)
#  report_date      :date
#  report_time      :string(255)
#  details          :text
#  remarks          :text
#  private          :boolean(1)      default(TRUE)
#  created_by       :integer(4)      default(1)
#  updated_by       :integer(4)      default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  owned_by         :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Investigation Report
# These are reports (or leads) for a Detective Investigation
class InvestReport < ActiveRecord::Base
   belongs_to :investigation
   belongs_to :crime, :class_name => 'InvestCrime', :foreign_key => 'invest_crime_id'
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :owner, :class_name => 'User', :foreign_key => 'updated_by'
   has_many :photos, :class_name => 'InvestPhoto'
   has_many :vehicles, :class_name => 'InvestVehicle'
   has_many :criminals, :class_name => 'InvestCriminal'
   has_and_belongs_to_many :sources, :class_name => 'Person', :join_table => 'invest_report_sources'
   has_and_belongs_to_many :victims, :class_name => 'Person', :join_table => 'report_victims'
   has_and_belongs_to_many :witnesses, :class_name => 'Person', :join_table => 'report_witnesses'
   
   
   # this sets up sphinx indices for this model
   define_index do
      indexes report_date
      indexes details
      indexes remarks

      where 'private != 1'
      
      has updated_at
   end
   
   after_save :check_creator
   before_validation :fix_times
   
   validates_presence_of :details
   validates_date_in_past :report_date
   validates_time :report_time
   
   def self.desc
      "Reports for Detective Investigation"
   end
   
   # returns base perms required to view investigations
   def self.base_perms
      Perm[:view_investigation]
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      count(:include => [:witnesses, :victims, :sources], :conditions => "people.id = '#{p_id}'") > 0
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      # catch the HABTM tables for people
      # TODO: find a better way to do this
      rec_count += count(:all, :include => :sources, :conditions => "invest_report_sources.person_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE invest_report_sources SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      rec_count += count(:all, :include => :victims, :conditions => "report_victims.person_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE report_victims SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      rec_count += count(:all, :include => :witnesses, :conditions => "report_witnesses.person_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE report_witnesses SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      return rec_count
   end
   
   def assigned_victims=(selected_victims)
      self.victims.clear
      selected_victims.each do |v|
         self.victims << Person.find_by_id(v) unless v.empty?
      end
   end
   
   def assigned_witnesses=(selected_witnesses)
      self.witnesses.clear
      selected_witnesses.each do |w|
         self.witnesses << Person.find_by_id(w) unless w.empty?
      end
   end
   
   def assigned_sources=(selected_sources)
      self.sources.clear
      selected_sources.each do |s|
         self.sources << Person.find_by_id(s) unless s.empty?
      end
   end
   
   private
   
   # calls the valid_time() method for each time type field (valid_time
   # inserts the : between hours and minutes if its omitted).
   def fix_times
      self.report_time = valid_time(report_time)
   end

   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the value of creator for investigation,
   # or the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:created_by, 1)
         else
            self.update_attribute(:created_by, investigation.created_by)
         end
      end
      if !updated_by? || updater.nil?
         if investigation.nil? || investigation.updater.nil?
            self.update_attribute(:updated_by, 1)
         else
            self.update_attribute(:updated_by, investigation.updated_by)
         end
      end
      if !owned_by? || owner.nil?
         if investigation.nil? || investigation.creator.nil?
            self.update_attribute(:owned_by, 1)
         else
            self.update_attribute(:owned_by, investigation.created_by)
         end
      end
      return true
   end
   
end
