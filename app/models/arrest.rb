# == Schema Information
#
# Table name: arrests
#
#  id                     :integer(4)      not null, primary key
#  person_id              :integer(4)
#  offense_id             :integer(4)
#  case_no                :string(255)
#  arrest_date            :date
#  arrest_time            :string(255)
#  officer1               :string(255)
#  officer1_unit          :string(255)
#  officer2               :string(255)
#  officer2_unit          :string(255)
#  officer3               :string(255)
#  officer3_unit          :string(255)
#  agency                 :string(255)
#  ward                   :integer(4)
#  district               :integer(4)
#  victim_notify          :boolean(1)
#  dwi_test_officer       :string(255)
#  dwi_test_officer_unit  :string(255)
#  dwi_test_results       :string(255)
#  hour_72_judge_id       :integer(4)
#  hour_72_date           :date
#  hour_72_time           :string(255)
#  attorney               :string(255)
#  condition_of_bond      :string(255)
#  remarks                :text
#  created_by             :integer(4)
#  updated_by             :integer(4)
#  created_at             :datetime
#  updated_at             :datetime
#  bond_amt               :decimal(12, 2)  default(0.0)
#  bondable               :boolean(1)      default(TRUE)
#  officer1_id            :integer(4)
#  officer1_badge         :string(255)
#  officer2_id            :integer(4)
#  officer2_badge         :string(255)
#  officer3_id            :integer(4)
#  officer3_badge         :string(255)
#  dwi_test_officer_id    :integer(4)
#  dwi_test_officer_badge :string(255)
#  agency_id              :integer(4)
#  arrest_type            :integer(4)      default(0)
#  legacy                 :boolean(1)      default(FALSE)
#  atn                    :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Arrest Model
# These are arrest records which contain a set of related charges, a warrant
# number, or some other reason to place a person in jail or keep them there.
class Arrest < ActiveRecord::Base
   belongs_to :person
   belongs_to :person_review, :class_name => "Person", :foreign_key => 'person_id', :include => :bookings_review
   belongs_to :offense
   belongs_to :hour_72_judge, :class_name => "Option", :foreign_key => 'hour_72_judge_id'
   belongs_to :creator, :class_name => "User", :foreign_key => "created_by"
   belongs_to :updater, :class_name => "User", :foreign_key => "updated_by"
   has_many :district_charges, :class_name => 'Charge', :foreign_key => 'd_arrest_id', :dependent => :destroy
   has_many :city_charges, :class_name => 'Charge', :foreign_key => 'c_arrest_id', :dependent => :destroy
   has_many :other_charges, :class_name => 'Charge', :foreign_key => 'o_arrest_id', :dependent => :destroy
   has_and_belongs_to_many :district_warrants, :class_name => 'Warrant', :join_table => 'district_warrants'
   has_and_belongs_to_many :city_warrants, :class_name => 'Warrant', :join_table => 'city_warrants'
   has_and_belongs_to_many :other_warrants, :class_name => 'Warrant', :join_table => 'other_warrants'
   has_many :holds, :dependent => :destroy
   has_many :bonds
   has_many :revocations
   has_many :da_charges
   
   attr_accessor :parish_probation
   attr_accessor :state_probation

   # this sets up sphinx indices for this model
   define_index do
      indexes [person.lastname, person.firstname, person.middlename, person.suffix], :as => :name
      indexes case_no
      indexes arrest_date
      indexes officer1
      indexes officer1_unit
      indexes officer2
      indexes officer2_unit
      indexes officer3
      indexes officer3_unit
      indexes agency
      indexes dwi_test_officer
      indexes dwi_test_officer_unit
      indexes attorney
      indexes hour_72_judge.long_name, :as => :judge
      indexes remarks
      indexes district_charges.charge, :as => :dchgs
      indexes city_charges.charge, :as => :cchgs
      indexes other_charges.charge, :as => :ochgs

      has updated_at
   end

   before_validation :adjust_case_no, :fix_times, :lookup_contacts, :check_72_hour
   after_save :check_creator, :check_and_generate_case, :process_72_hour, :process_warrants, :process_holds
   after_create :check_probation
   after_update :save_charges
   before_destroy :restore_warrants

   validates_presence_of :agency, :officer1
   validates_numericality_of :arrest_type, :only_integer => true, :greater_than_or_equal_to => 0, :less_than_or_equal_to => 2
   validates_date_in_past :arrest_date
   validates_time :arrest_time
   validates_date_in_past :hour_72_date, :allow_nil => true
   validates_time :hour_72_time, :allow_nil => true
   validates_presence_of :hour_72_time, :if => :hour_72_date?
   validates_presence_of :hour_72_date, :if => :hour_72_time?
   validates_numericality_of :ward, :district, :only_integer => true, :allow_nil => true
   validates_numericality_of :bond_amt, :message => 'must be a number (no blanks allowed)!'
   validates_person :person_id
   validates_offense :offense_id, :allow_nil => true

   # a short description of the model used for the DatabaseController index
   # view.
   def self.desc
      'Arrests database'
   end

   # returns the number of items to be reviewed by supplied user_id
   def self.review_count(user_id)
      return 0 unless user = User.find_by_id(user_id)
      if user.last_arrest.nil? || !user.last_arrest.is_a?(Integer)
         user.update_attribute(:last_arrest, 0)
      end
      count(:conditions => ['id > ?',user.last_arrest])
   end

   # returns true if any record uses the supplied option id in any of its
   # option type fields.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(hour_72_judge_id = '#{oid}')
      count(:conditions => cond) > 0
   end

   # returns true if person is in use in any capacity
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end

   # returns base perms required to View records in this model
   def self.base_perms
      Perm[:view_arrest]
   end

   def self.hours_served(*arr_ids)
      # TODO: rewrite this to take temporary releases into consideration
      arrs = []
      arr_ids.flatten.compact.uniq.each do |a|
         if arr = Arrest.find_by_id(a)
            arrs.push(arr)
         end
      end
      return 0 if arrs.empty?
      ranges = []
      holds = arrs.collect{|a| a.holds}.flatten.compact
      return 0 if holds.empty?
      holds.each do |h|
         hold_datetime = get_datetime(h.hold_date, h.hold_time)
         if h.booking.nil?
            ranges.push([hold_datetime, get_datetime(h.cleared_date, h.cleared_time)])
         else
            # sometimes we end up with hold dates that don't agree with booking dates,
            # in this case we want to go with the booking date.
            booking_datetime = get_datetime(h.booking.booking_date, h.booking.booking_time)
            datetime = [booking_datetime, hold_datetime].sort.pop
            ranges.push([datetime, get_datetime(h.booking.release_date, h.booking.release_time)])
         end
      end
      return 0 if ranges.empty?
      calculate_time_served(ranges, :return => 'hours')
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["officer1_id", "officer2_id", "officer3_id", "dwi_test_officer_id", "agency_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # summarizes arrests for specified date period
   def self.arrest_summary(start=nil, stop=nil, agency_id=nil)
      return [] unless (start.nil? || start.is_a?(Date)) && (stop.nil? || stop.is_a?(Date)) && (agency_id.nil? || agency_id.is_a?(Integer))
      if start.nil?
         start_fragment = ""
      else
         start_fragment = " AND arrest_date >= '#{start.to_s(:db)}'"
      end
      if stop.nil? || stop == Date.today
         stop_fragment = ""
      else
         stop_fragment = " AND arrest_date <= '#{stop.to_s(:db)}'"
      end
      # if agency is included, set additional conditions for query
      officer1_fragment = ""
      officer2_fragment = ""
      officer3_fragment = ""
      unless agency_id.blank?
         oids = Contact.find(:all, :conditions => {:agency_id => agency_id}).collect{|o| o.id.to_s}.join(',')
         unless oids.empty?
            officer1_fragment = " AND officer1_id in (#{oids})"
            officer2_fragment = " AND officer2_id in (#{oids})"
            officer3_fragment = " AND officer3_id in (#{oids})"
         end
      end
      # ActiveRecord does not do unions, so this will have to be find_by_sql
      # TODO: this statement is quite likely MySQL compatible only.  eventually need to find a better way
      sql_statement = %(SELECT DISTINCT officer_id, contacts.fullname as officer, contacts.unit as unit, arrest_date, arrest_time, person_id, arrest_type FROM
      (
         SELECT officer1_id AS officer_id, arrest_date, arrest_time, person_id, arrest_type FROM arrests
            WHERE officer1_id IS NOT NULL #{officer1_fragment} #{start_fragment} #{stop_fragment}
         UNION ALL SELECT officer2_id AS officer_id, arrest_date, arrest_time, person_id, arrest_type FROM arrests
            WHERE officer2_id IS NOT NULL #{officer2_fragment} #{start_fragment} #{stop_fragment}
         UNION ALL SELECT officer3_id AS officer_id, arrest_date, arrest_time, person_id, arrest_type FROM arrests
            WHERE officer3_id IS NOT NULL #{officer3_fragment} #{start_fragment} #{stop_fragment}
      ) AS a1
      LEFT OUTER JOIN contacts ON contacts.id = a1.officer_id
      ORDER BY officer, a1.officer_id, a1.arrest_date;)
      find_by_sql(sql_statement)
   end
   
   # returns a name for arrest_type
   def arrest_type_name
      case arrest_type
      when 0
         "District Charges"
      when 1
         "City Charges"
      when 2
         "Other Charges"
      else
         "Invalid"
      end
   end

   # accepts an array of charge attributes from the ArrestController
   # edit view and creates charge records linked to this arrest.
   def new_district_charges=(charge_attributes)
      return true unless arrest_type == 0
      charge_attributes.each do |attributes|
         unless attributes[:charge].blank?
            district_charges.build(attributes)
         end
      end
      return true
   end

   # accepts an array of charge attributes from the ArrestController
   # edit view. It cycles through all existing charges for the
   # arrest. If the charge is included in the charge attributes, the
   # charge is updated with the attributes, otherwise the charge
   # record is destroyed.
   def existing_district_charges=(charge_attributes)
      district_charges.reject(&:new_record?).each do |charge|
         attributes = charge_attributes[charge.id.to_s]
         if attributes
            attributes[:updated_by] = updated_by
            charge.attributes = attributes
         else
            district_charges.delete(charge)
         end
      end
      return true
   end

   # accepts an array of charge attributes from the ArrestController
   # edit view and creates charge records linked to this arrest.
   def new_city_charges=(charge_attributes)
      return true unless arrest_type == 1
      charge_attributes.each do |attributes|
         unless attributes[:charge].blank?
            city_charges.build(attributes)
         end
      end
      return true
   end

   # accepts an array of charge attributes from the ArrestController
   # edit view. It cycles through all existing charges for the
   # arrest. If the charge is included in the charge attributes, the
   # charge is updated with the attributes, otherwise the charge
   # record is destroyed.
   def existing_city_charges=(charge_attributes)
      city_charges.reject(&:new_record?).each do |charge|
         attributes = charge_attributes[charge.id.to_s]
         if attributes
            attributes[:updated_by] = updated_by
            charge.attributes = attributes
         else
            city_charges.delete(charge)
         end
      end
      return true
   end

   # accepts a list of warrants and associates them and their charges.
   def assigned_district_warrants=(selected_warrants)
      district_warrants(true).each do |w|
         w.update_attribute(:dispo_date, nil)
         w.update_attribute(:disposition_id, nil)
      end
      self.district_warrants.clear
      if arrest_type == 0
         selected_warrants.each do |w|
            self.district_warrants << Warrant.find_by_id(w) unless w.blank?
         end
      end
      return true
   end
   
   # accepts a list of warrants and associates them and their charges.
   def assigned_city_warrants=(selected_warrants)
      city_warrants(true).each do |w|
         w.update_attribute(:dispo_date, nil)
         w.update_attribute(:disposition_id, nil)
      end
      self.city_warrants.clear
      if arrest_type == 1
         selected_warrants.each do |w|
            self.city_warrants << Warrant.find_by_id(w) unless w.blank?
         end
      end
      return true
   end
   
   # accepts a list of warrants and associates them and their charges.
   def assigned_other_warrants=(selected_warrants)
      other_warrants(true).each do |w|
         w.update_attribute(:dispo_date, nil)
         w.update_attribute(:disposition_id, nil)
      end
      self.other_warrants.clear
      if arrest_type == 2
         selected_warrants.each do |w|
            self.other_warrants << Warrant.find_by_id(w) unless w.blank?
         end
      end
      return true
   end
   
   # This returns a single text string listing all charges and their counts
   # (summarizes district + city + other charges)
   def charge_summary
      text = ''
      unless district_charges.empty?
         text << "District: "
         text << district_charges.collect{|c| c.charge + "(#{c.count})"}.join(", ")
      end
      unless city_charges.empty?
         unless text.blank?
            text << " -- City: "
         else
            text << "City: "
         end
         text << city_charges.collect{|c| (c.agency.nil? ? (c.arrest_warrant.nil? ? '' : '[' + c.arrest_warrant.jurisdiction.fullname + '] ') : '[' + c.agency.fullname + '] ') + c.charge + "(#{c.count})"}.join(", ")
      end
      other_charges.each do |c|
         unless text.blank?
            text << " -- Other: "
         else
            text << "Other: "
         end
         text << other_charges.collect{|c| (c.arrest_warrant.nil? ? '' : '[' + c.arrest_warrant.jurisdiction.fullname + '] ') + c.charge + "(#{c.count})"}.join(", ")
      end
      if text.blank?
         return "No Charges"
      end
      return text
   end
   
   # This returns a single text string listing all charges and their counts
   # produced by cycling through all associated charge records.
   def district_charge_summary
      text = ''
      district_charges.each do |c|
         unless text.blank?
            text << ", "
         end
         text << "#{c.charge}(#{c.count})"
      end
      if text.blank?
         return "No Charges"
      end
      return text
   end
   
   # This returns a single text string listing all charges and their counts
   # produced by cycling through all associated charge records.
   def city_charge_summary
      text = ''
      city_charges.each do |c|
         unless text.blank?
            text << ", "
         end
         text << "#{c.charge}(#{c.count})#{c.agency.nil? ? (c.arrest_warrant.nil? ? '' : (c.arrest_warrant.jurisdiction.nil? ? '' : '[' + c.arrest_warrant.jurisdiction.fullname + ']')) : '[' + c.agency.fullname + ']'}"
      end
      if text.blank?
         return "No Charges"
      end
      return text
   end
   
   # This returns a single text string listing all charges and their counts
   # produced by cycling through all associated charge records.
   def other_charge_summary
      text = ''
      other_charges.each do |c|
         unless text.blank?
            text << ", "
         end
         text << "#{c.charge}(#{c.count})#{c.arrest_warrant.nil? ? '' : (c.arrest_warrant.jurisdiction.nil? ? '' : '[' + c.arrest_warrant.jurisdiction.fullname + ']')}"
      end
      if text.blank?
         return "No Charges"
      end
      return text
   end
   
   def warrants
      district_warrants + city_warrants + other_warrants
   end
   
   # returns a list of warrants served on an arrest
   def warrant_summary
      text = ''
      warrants.each do |w|
         unless text.blank?
            text << ", "
         end
         text << "##{w.warrant_no}(ID: #{w.id}): #{w.jurisdiction.nil? ? 'Unknown' : w.jurisdiction.fullname}"
      end
      if text.blank?
         return "No Warrants"
      end
      return text
   end
   
   # this returns the 48 hour hold record for this arrest if one exists
   def find_48(last=false)
      if last == true
         hold_list = holds(true).reverse
      else
         hold_list = holds(true)
      end
      hold_list.each do |h|
         if !h.hold_type.nil? && h.hold_type.long_name == "48 Hour"
            return h
         end
      end
      return nil
   end

   # this returns the first (or last) 72 hour hold record for this arrest if one exists
   def find_72(last=false)
      if last == true
         hold_list = holds(true).reverse
      else
         hold_list = holds(true)
      end
      hold_list.each do |h|
         if !h.hold_type.nil? && h.hold_type.long_name == "72 Hour"
            return h
         end
      end
      return nil
   end
   
   def dockets
      adocs = []
      dacs = []
      dacs.concat(da_charges)
      district_charges.each do |c|
         unless c.da_charges.empty?
            dacs.concat(c.da_charges)
         end
      end
      city_charges.each do |c|
         unless c.da_charges.empty?
            dacs.concat(c.da_charges)
         end
      end
      other_charges.each do |c|
         unless c.da_charges.empty?
            dacs.concat(c.da_charges)
         end
      end
      dacs.flatten.uniq.compact.each do |d|
         unless d.docket.nil?
            adocs.push(d.docket)
         end
      end
      adocs.uniq.compact
   end
   
   def current_holds
      holds.reject{|h| h.cleared_date?}.collect{|h| h.hold_type.nil? ? 'Unknown' : h.hold_type.long_name}.join(', ')
   end
   
   def officer1_string
      if officer1?
         "#{officer1_unit? ? officer1_unit + ' ' : ''}#{officer1}#{officer1_badge? ? ' (' + officer1_badge + ')' : ''}"
      else
         ""
      end
   end

   def officer2_string
      if officer2?
         "#{officer2_unit? ? officer2_unit + ' ' : ''}#{officer2}#{officer2_badge? ? ' (' + officer2_badge + ')' : ''}"
      else
         ""
      end
   end

   def officer3_string
      if officer3?
         "#{officer3_unit? ? officer3_unit + ' ' : ''}#{officer3}#{officer3_badge? ? ' (' + officer3_badge + ')' : ''}"
      else
         ""
      end
   end
   
   def officers
      [officer1_string, officer2_string, officer3_string].reject{|o| o.blank?}.join(", ")
   end
   
   private
   
   # processes linked warrants
   def process_warrants
      served = Option.lookup("Warrant Disposition","Served",nil,nil,true)
      if arrest_type == 0
         district_warrants.each do |w|
            next if w.disposition_id == served
            w.charges.each do |c|
               Charge.create(
                  :d_arrest_id => id,
                  :arrest_warrant_id => w.id,
                  :charge => c.charge,
                  :count => c.count,
                  :created_by => updated_by,
                  :updated_by => updated_by
               )
            end
            w.update_attribute(:dispo_date, arrest_date)
            w.update_attribute(:disposition_id, served)
         end
      elsif arrest_type == 1
         city_warrants.each do |w|
            next if w.disposition_id == served
            w.charges.each do |c|
               Charge.create(
                  :c_arrest_id => id,
                  :arrest_warrant_id => w.id,
                  :charge => c.charge,
                  :count => c.count,
                  :created_by => updated_by,
                  :updated_by => updated_by
               )
            end
            w.update_attribute(:dispo_date, arrest_date)
            w.update_attribute(:disposition_id, served)
         end
      elsif arrest_type == 2
         other_warrants.each do |w|
            next if w.disposition_id == served
            w.charges.each do |c|
               Charge.create(
                  :o_arrest_id => id,
                  :arrest_warrant_id => w.id,
                  :charge => c.charge,
                  :count => c.count,
                  :created_by => updated_by,
                  :updated_by => updated_by
               )
            end
            w.update_attribute(:dispo_date, arrest_date)
            w.update_attribute(:disposition_id, served)
         end
      end
      return true
   end
   
   # restores served warrants to their active state before destroying this
   # arrest.  Also clears the HABTM warrant relationships
   def restore_warrants
      district_warrants.each do |w|
         w.update_attribute(:dispo_date, nil)
         w.update_attribute(:disposition_id, nil)
      end
      self.district_warrants.clear
      city_warrants.each do |w|
         w.update_attribute(:dispo_date, nil)
         w.update_attribute(:disposition_id, nil)
      end
      self.city_warrants.clear
      other_warrants.each do |w|
         w.update_attribute(:dispo_date, nil)
         w.update_attribute(:disposition_id, nil)
      end
      self.other_warrants.clear
      return true
   end

   # strips all characters from case_no except for numbers, letters, and -
   def adjust_case_no
      self.case_no = fix_case(case_no)
      return true
   end

   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   def save_charges
      district_charges.each{|c| c.save(false)}
      city_charges.each{|c| c.save(false)}
      other_charges.each{|c| c.save(false)}
      return true
   end

   # this checks to see if the arrested person has an active probation and
   # adds a probation hold if so.
   def check_probation
      unless person.nil?
         unless person.probations.empty?
            on_probation = false
            person.probations.each do |p|
               unless p.status.nil?
                  if (p.status.long_name == 'Pending' || p.status.long_name == 'Active') && p.hold_if_arrested?
                     on_probation = true
                  end
               end
            end
            if on_probation
               probation_hold = self.holds.build(
                  :created_by => updated_by,
                  :updated_by => updated_by,
                  :hold_type_id => Option.lookup("Hold Type","Probation (Parish)",nil,nil,true),
                  :hold_by_id => updater.contact_id,
                  :hold_by => updater.name,
                  :hold_by_unit => updater.unit,
                  :hold_by_badge => updater.badge,
                  :booking_id => person.bookings.last.id
               )
               probation_hold.hold_date, probation_hold.hold_time = get_date_and_time
               probation_hold.save(false)
            end
         end
      end
      return true
   end

   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_contacts
      if officer1_id?
         if ofc = Contact.find_by_id(officer1_id)
            self.officer1 = ofc.fullname
            self.officer1_badge = ofc.badge_no
            self.officer1_unit = ofc.unit
         end
      end
      if officer2_id?
         if ofc = Contact.find_by_id(officer2_id)
            self.officer2 = ofc.fullname
            self.officer2_badge = ofc.badge_no
            self.officer2_unit = ofc.unit
         end
      end
      if officer3_id?
         if ofc = Contact.find_by_id(officer3_id)
            self.officer3 = ofc.fullname
            self.officer3_badge = ofc.badge_no
            self.officer3_unit = ofc.unit
         end
      end
      if dwi_test_officer_id?
         if ofc = Contact.find_by_id(dwi_test_officer_id)
            self.dwi_test_officer = ofc.fullname
            self.dwi_test_officer_badge = ofc.badge_no
            self.dwi_test_officer_unit = ofc.unit
         end
      end
      if agency_id?
         if agc = Contact.find_by_id(agency_id)
            self.agency = agc.fullname
         end
      end
      return true
   end

   # checks to make sure all 72 hour information is present if 72 hour is
   # filled in
   def check_72_hour
      retval = true
      if hour_72_date? || hour_72_time? || hour_72_judge_id? || condition_of_bond? || bond_amt?
         unless hour_72_date?
            self.errors.add(:hour_72_date, "cannot be blank!")
            retval = false
         end
         unless hour_72_time?
            self.errors.add(:hour_72_time, "cannot be blank!")
            retval = false
         end
         unless hour_72_judge_id?
            self.errors.add(:hour_72_judge_id, "cannot be blank!")
            retval = false
         end
      end
      return retval
   end

   # If the 72 hour information on the arrest is filled out, this will
   # close out an existing 72 hour hold and replace it with either a
   # bondable hold with bond information, or a no bond hold.
   # note that I'm reloading the arrest and processing that instance
   # because the associations haven't been updated at this time so this
   # will fail if performed during the initial after_save period.
   def process_72_hour
      if hour_72_date?
         if h72 = find_72(true)
            if book = Booking.find_by_id(h72.booking_id)
               unless book.release_date?
                  unless h72.cleared_date?
                     # update attorney field
                     unless book.attorney?
                        book.update_attribute(:attorney, attorney)
                     end
                     h72.cleared_date = hour_72_date
                     h72.cleared_time = hour_72_time
                     h72.cleared_by_unit = updater.unit
                     h72.cleared_by_badge = updater.badge
                     h72.cleared_by = updater.name
                     h72.cleared_by_id = updater.contact_id
                     h72.explanation = "Automatic Clear by 72 Hour"
                     h72.cleared_because_id = Option.lookup("Release Type","72 Hour Hearing Completed",nil,nil,true)
                     h72.remarks = "#{h72.remarks? ? h72.remarks : ''}"
                     unless h72.remarks.blank?
                        h72.remarks << "\n"
                     end
                     h72.remarks << "Per Judge #{hour_72_judge.long_name}#{condition_of_bond? ? ': ' + condition_of_bond : ''}"
                     h72.save(false)
                     if bondable?
                        newhold = Hold.add(book.id, id, "Bondable")
                     else
                        newhold = Hold.add(book.id, id, "No Bond")
                     end
                     if who = User.find_by_id(newhold.updated_by)
                        newhold.hold_by_id = who.contact_id
                        newhold.hold_by = who.name
                        newhold.hold_by_unit = who.unit
                        newhold.hold_by_badge = who.badge
                     end
                     if condition_of_bond?
                        newhold.remarks = "Per Judge #{hour_72_judge.long_name}: #{condition_of_bond}"
                     end
                     newhold.save(false)
                     # also process 48 if it hasn't been done already
                     # this also happens for preset bonds
                     if h48 = find_48
                        unless h48.cleared_date?
                           h48.cleared_date = hour_72_date
                           h48.cleared_time = hour_72_time
                           h48.cleared_by_id = updater.contact_id
                           h48.cleared_by_unit = updater.unit
                           h48.cleared_by_badge = updater.badge
                           h48.cleared_by = updater.name
                           h48.explanation = "Automatic Clear by 72 Hour"
                           h48.cleared_because_id = Option.lookup("Release Type", "72 Hour Hearing Completed",nil,nil,true)
                           h48.remarks = "Clearing the 72 Hour Hold automatically clears this one."
                           h48.save(false)
                        end
                     end
                  end
               end
            end
         end
      end
      return true
   end
   
   # this processes the required holds depending on information from the arrest
   def process_holds
      current = holds(true).collect{|h| h.hold_type.long_name}
      return false if person.nil?
      booking = person.bookings.last
      return false if booking.nil? || booking.release_date?
      # first, process district charges
      unless district_charges(true).empty?
         # need to add a 48 and a 72 unless they already exist
         unless current.include?('48 Hour')
            Hold.add(booking.id, id, '48 Hour')
         end
         unless current.include?('72 Hour')
            Hold.add(booking.id, id, '72 Hour')
         end
      end
      unless city_charges(true).empty?
         agencies = []
         city_charges.collect do |c|
            if !c.agency_id?
               if !(c.arrest_warrant.nil? || !c.arrest_warrant.jurisdiction_id?)
                  agencies.push(c.arrest_warrant.jurisdiction_id)
               end
            else
               agencies.push(c.agency_id)
            end
         end
         city_agencies = holds.reject{|h| h.cleared_date? || !h.agency_id? || h.hold_type.nil? || h.hold_type.long_name != 'Hold for City'}.collect{|h| h.agency_id}.compact.uniq
         agencies.compact.uniq.each do |city|
            unless city_agencies.include?(city)
               Hold.add(booking.id, id, 'Hold for City', city)
            end
         end
      end
      unless other_charges(true).empty?
         agencies = []
         other_charges.collect do |c|
            if !(c.arrest_warrant.nil? || !c.arrest_warrant.jurisdiction_id?)
               agencies.push(c.arrest_warrant.jurisdiction_id)
            end
         end
         other_agencies = holds.reject{|h| h.cleared_date? || !h.agency_id? || h.hold_type.nil? || h.hold_type.long_name != 'Hold for Other Agency'}.collect{|h| h.agency_id}.compact.uniq
         agencies.compact.uniq.each do |other|
            unless other_agencies.include?(other)
               Hold.add(booking.id, id, 'Hold for Other Agency', other)
            end
         end
      end
      if parish_probation == '1'
         unless current.include?('Probation (Parish)')
            Hold.add(booking.id, id, 'Probation (Parish)')
         end
      end
      if state_probation == '1'
         unless current.include?('Probation/Parole (State)')
            Hold.add(booking.id, id, 'Probation/Parole (State)')
         end
      end
      return true
   end

   # Checks to see if the saved arrest has a case number assigned. If not, and
   # a bond or warrant id are specified, it will look up the case number from
   # the bond or warrant if possible.  Otherwise, it will generate a new call sheet
   # showing the arrest information and save it, then will get the case number from
   # the call sheet.
   def check_and_generate_case
      return true if legacy?
      unless case_no?
         unless offense.nil?
            if offense.case_no?
               self.update_attribute(:case_no, offense.case_no)
            end
         end
      end
      unless case_no?
         call = nil
         # look up or create options
         via_id = Option.lookup("Call Via","Arrest",nil,nil,true)
         if via_id.nil?
            if ot = OptionType.find_by_name("Call Via")
               if o = Option.create(:option_type_id => ot.id, :long_name => 'Arrest', :disabled => true, :created_by => self.updated_by, :updated_by => self.updated_by)
                  via_id = o.id
               end
            end
         end
         signal_id = Option.lookup("Signal Code","999","short_name",nil,true)
         if signal_id.nil?
            if ot = OptionType.find_by_name("Signal Code")
               if o = Option.create(:option_type_id => ot.id, :long_name => 'Miscellaneous', :short_name => "999", :disabled => true, :created_by => self.updated_by, :updated_by => self.updated_by)
                  signal_id = o.id
               end
            end
         end
         dispo_id = Option.lookup("Call Disposition", "Report Only",nil,nil,true)
         if dispo_id.nil?
            if ot = OptionType.find_by_name("Call Disposition")
               if o = Option.create(:option_type_id => ot.id, :long_name => 'Report Only', :disabled => true, :created_by => self.updated_by, :updated_by => self.updated_by)
                  dispo_id = o.id
               end
            end
         end
         det = "*** Auto-generated Call Sheet for Arrest of: #{person.nil? ? "Unknown Person" : person.full_name} ***\n"
         unless district_charges.empty?
            det << "District Charges: #{district_charge_summary}\n"
         end
         unless city_charges.empty?
            det << "City Charges: #{city_charge_summary}\n"
         end
         unless other_charges.empty?
            det << "Out of Parish/State Charges: #{other_charge_summary}\n"
         end
         det << "Arresting Agency: #{agency}\n"
         det << "Arresting Officers: #{officers}\n"
         # generate a call sheet
         unless person.nil? # this would be a validation violation and should not occur
            unless updater.nil? # this should also never happen
               call = Call.new(
                  :case_no => Case.get_next_case_number,
                  :followup_no => '',
                  :received_via_id => via_id,
                  :received_by_id => updater.contact_id,
                  :received_by => updater.name,
                  :received_by_unit => updater.unit,
                  :received_by_badge => updater.badge,
                  :disposition_id => dispo_id,
                  :dispatcher_id => updater.contact_id,
                  :dispatcher => updater.name,
                  :dispatcher_unit => updater.unit,
                  :dispatcher_badge => updater.badge,
                  :ward => ward,
                  :district => district,
                  :remarks => remarks,
                  :call_date => arrest_date,
                  :call_time => arrest_time,
                  :created_by => updated_by,
                  :updated_by => updated_by,
                  :signal_code_id => signal_id,
                  :details => det
               )
               call.save(false)
               call.call_subjects.create(
                  :subject_type_id => Option.lookup("Subject Type","Arrested",nil,nil,true),
                  :name => person.full_name,
                  :address1 => person.physical_line1,
                  :address2 => person.physical_line2,
                  :home_phone => person.home_phone,
                  :cell_phone => person.cell_phone,
                  :created_by => updated_by,
                  :updated_by => updated_by
               )
            end
         end
         if call.nil?
            # something went wrong with call, fall back on self-generated no
            self.update_attribute(:case_no, Case.get_next_case_number)
         else
            self.update_attribute(:case_no, call.case_no)
         end
      end
      return true
   end

   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      numtypes = ["person_id","offense_id","victim_notify"]

      query.each do |key, value|
         if key == 'name'
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.firstname like '#{nameparts[:first]}'"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.middlename like '#{nameparts[:middle]}'"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.lastname like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.suffix like '#{nameparts[:suffix]}'"
            end
         elsif key == "arrest_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "arrests.arrest_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "arrest_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "arrests.arrest_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "charge"
            unless condition.empty?
               condition << " and "
            end
            condition << "charges.charge like '%#{value}%'"
         elsif key == "officer"
            unless condition.empty?
               condition << " and "
            end
            condition << "(arrests.officer1 like '%#{value}%' OR arrests.officer2 like '%#{value}%' OR arrests.officer3 like '%#{value}%')"
         elsif key == "officer_badge"
            unless condition.empty?
               condition << " and "
            end
            condition << "(arrests.officer1_badge like '#{value}' OR arrests.officer2_badge like '#{value}' OR arrests.officer3_badge like '#{value}')"
         elsif key == "officer_unit"
            unless condition.empty?
               condition << " and "
            end
            condition << "(arrests.officer1_unit like '#{value}' OR arrests.officer2_unit like '#{value}' OR arrests.officer3_unit like '#{value}')"
         elsif key == "warrant_no"
            unless condition.empty?
               condition << " and "
            end
            condition << "warrants.warrant_no like '#{value}'"
         else
            unless condition.empty?
               condition << " and "
            end
            if numtypes.include?(key)
               condition << "arrests.#{key} = '#{value}'"
            else
               condition << "arrests.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end

   # this is a helper that calls the valid_time() method with each time
   # field for an arrest to insert any missing colons (:) into the time values
   def fix_times
      self.arrest_time = valid_time(arrest_time)
      self.hour_72_time = valid_time(hour_72_time)
      return true
   end
end
