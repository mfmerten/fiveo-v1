# == Schema Information
#
# Table name: users
#
#  id                      :integer(4)      not null, primary key
#  login                   :string(255)
#  locked                  :boolean(1)      default(FALSE)
#  hashed_password         :string(255)
#  salt                    :string(255)
#  items_per_page          :integer(4)      default(15)
#  expires                 :date
#  created_at              :datetime
#  contact_id              :integer(4)
#  created_by              :integer(4)      default(1)
#  updated_by              :integer(4)      default(1)
#  updated_at              :datetime
#  disabled_text           :string(255)
#  user_photo_file_name    :string(255)
#  user_photo_content_type :string(255)
#  user_photo_file_size    :integer(4)
#  user_photo_updated_at   :datetime
#  send_email              :boolean(1)      default(FALSE)
#  no_self_bug_messages    :boolean(1)      default(TRUE)
#  last_call               :integer(4)      default(0)
#  last_pawn               :integer(4)      default(0)
#  last_arrest             :integer(4)      default(0)
#  last_bug                :integer(4)      default(0)
#  last_forbid             :integer(4)      default(0)
#  review_forbid           :boolean(1)      default(TRUE)
#  review_arrest           :boolean(1)      default(TRUE)
#  review_pawn             :boolean(1)      default(TRUE)
#  review_bug              :boolean(1)      default(TRUE)
#  review_call             :boolean(1)      default(TRUE)
#  last_warrant            :integer(4)      default(0)
#  review_warrant          :boolean(1)      default(TRUE)
#  show_tips               :boolean(1)      default(TRUE)
#  notify_release          :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
require 'digest/sha1'

# User Model
# application user records.
class User < ActiveRecord::Base
    restful_easy_messages
    
    has_and_belongs_to_many :groups
    belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
    belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
    belongs_to :contact, :include => :phones, :dependent => :destroy
    has_and_belongs_to_many :ignored_announcements, :class_name => 'Announcement', :join_table => 'announce_ignores'
    
    attr_protected :id, :salt, :expires, :newpass

    # attributes used for password manipulation
    attr_accessor :password, :password_confirmation, :keep_pass, :old_password
    
    # user photo attribute
    has_attached_file :user_photo,
       :url => "/system/:attachment/:id/:style/:basename.:extension",
       :path => ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
       :default_url => "/images/missing_:style.jpg",
       :styles => {
          :thumb => "80x80",
          :original => "600x600",
          :medium => "300x300",
          :small => "150x150" }
 
    before_destroy :check_and_destroy_dependencies
    after_save :check_creator
    
    validates_length_of :login, :within => 3..40
    validates_length_of :password, :within => 5..40, :unless => :keep_pass
    validates_presence_of :login, :salt
    validates_presence_of :password, :password_confirmation, :unless => :keep_pass
    validates_uniqueness_of :login
    validates_confirmation_of :password, :unless => :keep_pass

    # a short description for the DatabaseController index view.
    def self.desc
       "User login accounts"
    end
    
    # returns the current user record if supplied login and password match
    def self.authenticate(login, pass)
        u=find(:first, :conditions=>["login = ?", login])
        return nil if u.nil?
        return u if User.encrypt(pass, u.salt)==u.hashed_password
        nil
    end
    
    # returns all users id and name values in an array of arrays suitable for
    # use in a select control.  Disabled users are not included, although
    # locked accounts are.
    def self.users_for_select(allow_disabled=false)
       if allow_disabled == true
          find(:all, :include => :contact).sort{|a,b| a.name(true) <=> b.name(true) }.collect{|u| ["#{u.name(true)} (#{u.login})", u.id]}
       else
          find(:all, :include => :contact, :conditions => "disabled_text = '' OR disabled_text is NULL").sort{|a,b| a.name(true) <=> b.name(true) }.collect{|u| ["#{u.name(true)} (#{u.login})", u.id]}
       end
    end
    
    # returns list of current site administrators
    def self.admins_for_select
       find(:all, :include => [:contact, :groups], :conditions => "groups.name = 'Admin' AND users.login != 'mfmerten' AND users.login != 'admin'").collect{|u| ["#{u.name} (#{u.login})", u.id]}
    end
     
    # returns base permissions required to view users
    def self.base_perms
       Perm[:view_user]
    end
    
    def self.swap_contact(bad_id, good_id, user_id)
       return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
       rec_count = 0
       ["contact_id"].each do |field|
          rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
       end
       return rec_count
    end
    
    # returns an array of user ids that have the notify_release preference set
    # notify_release is true if user wishes to get a message whenever anyone is
    # released from jail
    def self.notify_release_users
       find(:all, :conditions => 'notify_release IS TRUE').collect{|u| u.id}
    end
    
    # If a linked contact record exists, returns the contact full name.
    # reverse paramter is retained for compatibility, but is ignored.
    # Otherwise, returns the login name.
    def name(reverse = false)
       contact.nil? ? login : contact.fullname
    end
    
    # if a linked contact record exists, returns the contact unit, otherwise
    # returns blank.
    def unit
       contact.nil? ? '' : contact.unit
    end
    
    # if a linked contact record exists, returns the badge number, otherwise
    # returns blank.
    def badge
       contact.nil? ? '' : contact.badge_no
    end
    
    def fullname
       "#{unit.blank? ? '' : unit + ' '}#{name}#{badge.blank? ? '' : ' (' + badge + ')'}"
    end
    
    # used to encrypt and store the users password and set the default
    # password expiration.  If the users password is 'pa$$word' (which means
    # a new or reset account), the account is expired immediately.
    def password=(pass)
        @password=pass
        self.salt = User.random_string(10)
        self.hashed_password = User.encrypt(@password, salt)
        if @password == "pa$$word"
            self.expires = Date.today
        else
            self.expires = Date.today + 180.days
        end
    end
    
    # gets collection of permissions via group memberships
    def pids
       groups.collect{|g| g.permissions}.flatten.compact.uniq.collect{|p| p.to_i}
    end
    
    # accepts an array of group attributes and links them to the user
    # using the groups association after clearing any old links.
    def assigned_groups=(selected_groups)
       self.groups.clear
       selected_groups.each do |g|
          self.groups << Group.find_by_id(g) unless g.empty?
       end
    end
    
    private
    
    # checks that if created_by or updated_by are nil, 0 or non-integer after
    # the record is saved, they get set to the default value of 1 (admin).
    def check_creator
       if !created_by? || creator.nil?
          self.update_attribute(:created_by, 1)
       end
       if !updated_by? || updater.nil?
          self.update_attribute(:updated_by, 1)
       end
       return true
    end

    # ensures that all group, role and permission associations are cleared
    # prior to destroying the user record.
    def check_and_destroy_dependencies
        self.groups.clear
        self.ignored_announcements.clear
        return true
    end

    # accepts a password and salt value and encrypts the password using SHA1
    # and the provided salt.
    def self.encrypt(pass, salt)
        Digest::SHA1.hexdigest(pass+salt)
    end

    # generates a random string using a-z, A-Z and 0-9 characters of the
    # specified length.
    def self.random_string(len)
        #generat a random password consisting of strings and digits
        chars = ("a".."z").to_a + ("A".."Z").to_a + ("0".."9").to_a
        newpass = ""
        1.upto(len) { |i| newpass << chars[rand(chars.size-1)] }
        return newpass
    end
    
    # accepts a query hash and converts it to a SQL partial suitable for use
    # in a WHERE clause.
    def self.condition_string(fullquery)
       return nil unless query = quoted(fullquery)
       numtypes = ["locked"]
       condition = ""
       query.each do |key, value|
          if key == 'disabled'
             unless condition.blank?
                condition << " and "
             end
             if value == '0'
                condition << "(users.disabled_text = '' or users.disabled_text is null)"
             elsif value == '1'
                condition << "users.disabled_text > ''"
             end
         elsif key == 'group'
            unless condition.blank?
               condition << " and "
            end
            condition << "groups.id = '#{value}'"
          elsif key == 'full_name'
             unless condition.blank?
                condition << " and "
             end
             condition << "contacts.fullname like '%#{value}%'"
          else
             unless condition.empty?
                condition << " and "
             end
             if numtypes.include?(key)
                condition << "users.#{key} = '#{value}'"
             else
                condition << "users.#{key} like '%#{value}%'"
             end
          end
       end
       return sanitize_sql_for_conditions(condition)
    end
 end
