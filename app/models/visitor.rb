# == Schema Information
#
# Table name: visitors
#
#  id              :integer(4)      not null, primary key
#  booking_id      :integer(4)
#  name            :string(255)
#  street          :string(255)
#  city            :string(255)
#  state_id        :integer(4)
#  zip             :string(255)
#  phone           :string(255)
#  visit_date      :date
#  sign_in_time    :string(255)
#  sign_out_time   :string(255)
#  relationship_id :integer(4)
#  remarks         :text
#  created_by      :integer(4)
#  updated_by      :integer(4)
#  created_at      :datetime
#  updated_at      :datetime
#  legacy          :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Visitor Model
# jail visitor log
class Visitor < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :state, :class_name => 'Option', :foreign_key => 'state_id'
   belongs_to :relationship, :class_name => 'Option', :foreign_key => 'relationship_id'
   belongs_to :booking
   
   before_validation :fix_times
   after_save :check_creator
   
   validates_presence_of :name, :street, :city
   validates_zip :zip
   validates_phone :phone
   validates_date :visit_date
   validates_time :sign_in_time
   validates_time :sign_out_time, :allow_nil => true
   validates_booking :booking_id
   validates_option :state_id, :option_type => 'State'
   validates_option :relationship_id, :option_type => 'Associate Type'
   
   # a short description for the DatabaseController index view
   def self.desc
      'Jail Visitor Records'
   end
   
   # returns true if the specified option id is used in the relationship id
   # field of any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(relationship_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
      
   # calls valid_time() with each of the time type fields. (valid_time()
   # inserts a colon between hours and minutes if omitted).
   def fix_times
      self.sign_in_time = valid_time(sign_in_time)
      self.sign_out_time = valid_time(sign_out_time)
   end
end
