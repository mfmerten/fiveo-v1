# == Schema Information
#
# Table name: commissaries
#
#  id               :integer(4)      not null, primary key
#  person_id        :integer(4)
#  officer          :string(255)
#  officer_unit     :string(255)
#  deposit          :decimal(12, 2)  default(0.0)
#  withdraw         :decimal(12, 2)  default(0.0)
#  created_by       :integer(4)
#  updated_by       :integer(4)
#  created_at       :datetime
#  updated_at       :datetime
#  officer_id       :integer(4)
#  officer_badge    :string(255)
#  report_datetime  :datetime
#  category_id      :integer(4)
#  receipt_no       :string(255)
#  memo             :string(255)
#  transaction_date :date
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Commissary Model
# This records transactions for the booking commissary.
class Commissary < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :category, :class_name => 'Option', :foreign_key => 'category_id'
   belongs_to :person
   
   before_validation :check_amounts, :lookup_officers
   after_save :check_creator
   before_destroy :check_deletable
   
   validates_presence_of :officer
   validates_date :transaction_date
   validates_numericality_of :deposit, :withdraw, :allow_nil => true
   validates_option :category_id, :option_type => 'Commissary Type'
   validates_person :person_id
   
   # a short description of the model for the DatabaseController index view.
   def self.desc
      'Jail Commissary Records'
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_commissary]
   end
   
   # returns true if the specified option id is used in any record for any
   # option type field.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(category_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   # returns person array for transactions in specified date range
   def self.all_people(start_date=Date.today.beginning_of_month, end_date=Date.today, facility=nil)
      if facility.nil?
         trans = find(:all, :include => :person, :conditions => ["transaction_date >= ? AND transaction_date <= ?",start_date,end_date])
      else
         trans = find(:all, :include => :person, :conditions => ["transaction_date >= ? AND transaction_date <= ? AND people.commissary_facility_id = ?",start_date,end_date,facility])
      end
      trans.collect{|t| t.person}.uniq.compact.sort{|a,b| a.full_name + a.id.to_s <=> b.full_name + b.id.to_s}
   end
   
   # returns all unposted transactions
   def self.all_unposted(facility = nil)
      if facility.nil?
         find(:all, :include => [:person, :category], :order => 'people.lastname, people.firstname, commissaries.id', :conditions => 'report_datetime is null')
      else
         find(:all, :include => [:person, :category], :order => 'people.lastname, people.firstname, commissaries.id', :conditions => ['report_datetime is null AND people.commissary_facility_id = ?', facility])
      end
   end
   
   # returns unposted deposits
   def self.deposit_unposted(facility = nil)
      if facility.nil?
         find(:all, :include => :person, :order => 'people.lastname, people.firstname, commissaries.id', :conditions => 'report_datetime is null and deposit > 0')
      else
         find(:all, :include => :person, :order => 'people.lastname, people.firstname, commissaries.id', :conditions => ['report_datetime is null and deposit > 0 AND people.commissary_facility_id = ?', facility])
      end
   end
   
   # returns unposted withdrawals
   def self.withdraw_unposted(facility = nil)
      if facility.nil?
         find(:all, :include => :person, :order => 'people.lastname, people.firstname, commissaries.id', :conditions => 'report_datetime is null and withdraw > 0')
      else
         find(:all, :include => :person, :order => 'people.lastname, people.firstname, commissaries.id', :conditions => ['report_datetime is null and withdraw > 0 AND people.commissary_facility_id = ?', facility])
      end
   end
   
   # returns deposits posted on the specified datetime
   def self.deposit_posted(facility = nil, posting_datetime = nil)
      return [] if posting_datetime.nil?
      if facility.nil?
         find(:all, :include => :person, :order => 'people.lastname, people.firstname, commissaries.id', :conditions => ['report_datetime = ? and deposit > 0', posting_datetime])
      else
         find(:all, :include => :person, :order => 'people.lastname, people.firstname, commissaries.id', :conditions => ['report_datetime = ? and deposit > 0 AND people.commissary_facility_id = ?', posting_datetime, facility])
      end
   end
   
   # returns withdrawals posted on the specified datetime
   def self.withdraw_posted(facility = nil, posting_datetime = nil)
      return [] if posting_datetime.nil?
      if facility.nil?
         find(:all, :include => :person, :order => 'people.lastname, people.firstname, commissaries.id', :conditions => ['report_datetime = ? and withdraw > 0', posting_datetime])
      else
         find(:all, :include => :person, :order => 'people.lastname, people.firstname, commissaries.id', :conditions => ['report_datetime = ? and withdraw > 0 AND people.commissary_facility_id = ?', posting_datetime, facility])
      end
   end
   
   # returns the last 10 posting datetimes as an array of arrays for use in
   # a select control
   def self.posting_dates
      postings = find(:all, :select => 'DISTINCT report_datetime', :order => 'report_datetime', :conditions => "report_datetime is not null").reject{|c| c.report_datetime.nil?}.collect{|c| [format_date(c.report_datetime),c.report_datetime]}
      if postings.size > 10
         return postings.slice(-10,10).reverse
      else
         return postings.reverse
      end
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      update_all("person_id = #{good_id.to_i}, updated_by = #{user_id.to_i}", "person_id = #{bad_id.to_i}")
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end

   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if officer_id?
         if ofc = Contact.find_by_id(officer_id)
            self.officer = ofc.fullname
            self.officer_badge = ofc.badge_no
            self.officer_unit = ofc.unit
         end
      end
   end

   # This verifies that one and only one of deposit and withdraw
   # are entered, and that the entered amount is greater than zero.
   def check_amounts
      unless (withdraw? && withdraw > 0) || (deposit? && deposit > 0)
         self.errors.add(:base, "Must enter either a withdraw amount or a deposit amount!")
         return false
      end
      if (withdraw? && withdraw > 0) && (deposit? && deposit > 0)
         self.errors.add(:base, "Must enter a withdraw amount or a deposit amount, not both!")
         return false
      end
      if withdraw? && withdraw < 0
         self.errors.add(:withdraw, "cannot be negative!")
         return false
      end
      if deposit? && deposit < 0
         self.errors.add(:deposit, "cannot be negative!")
         return false
      end
      return true
   end
   
   # checks that transaction has not been posted before allowing delete
   def check_deletable
      return false if report_datetime?
      return true
   end
end
