# == Schema Information
#
# Table name: jail3s
#
#  id                    :integer(4)      not null, primary key
#  person_id             :integer(4)
#  cash_at_booking       :decimal(12, 2)  default(0.0)
#  cash_receipt          :string(255)
#  booking_officer       :string(255)
#  booking_officer_unit  :string(255)
#  booking_date          :date
#  booking_time          :string(255)
#  cell_id               :integer(4)
#  remarks               :text
#  phone_called          :string(255)
#  intoxicated           :boolean(1)
#  sched_release_date    :date
#  release_date          :date
#  release_time          :string(255)
#  afis                  :boolean(1)
#  release_officer       :string(255)
#  release_officer_unit  :string(255)
#  fingerprint_class     :string(255)
#  created_by            :integer(4)
#  updated_by            :integer(4)
#  created_at            :datetime
#  updated_at            :datetime
#  booking_officer_id    :integer(4)
#  booking_officer_badge :string(255)
#  release_officer_id    :integer(4)
#  release_officer_badge :string(255)
#  good_time             :boolean(1)      default(TRUE)
#  disable_timers        :boolean(1)      default(FALSE)
#  dna_profile           :string(255)
#  attorney              :string(255)
#  legacy                :boolean(1)      default(FALSE)
#  released_because_id   :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Jail 3 inmate records
class Jail3 < ActiveRecord::Base

   # a short description of the model for use by the DatabaseController index
   # view.
   def self.desc
      'Jail 3 Booking Records'
   end
   
   def self.base_perms
      Perm[:view_jail3]
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
end
