# == Schema Information
#
# Table name: histories
#
#  id         :integer(4)      not null, primary key
#  key        :string(255)
#  name       :string(255)
#  type_id    :integer(4)
#  details    :text
#  created_at :datetime
#  updated_at :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class History < ActiveRecord::Base
    belongs_to :type, :class_name => 'Option', :foreign_key => 'type_id'
    
    # this sets up sphinx indices for this model
    define_index do
       indexes :key
       indexes :name
       indexes details

       has updated_at
    end
    
    before_save :refuse
    
    # a short description for the DatabaseController index view.
    def self.desc
       "History (legacy data)"
    end
    
    # base permissions required to View these objects
    def self.base_perms
       Perm[:view_history]
    end
     
    # returns true if the specified option id is in use in any of the option
    # type fields in any record.
    def self.uses_option(oid)
       oid ||= 0
       oid = oid.to_i
       cond = %(type_id = '#{oid}')
       count(:conditions => cond) > 0
    end
    
    private
    
    # accepts a query hash and converts it to a SQL partial suitable for use
    # in a WHERE clause
    def self.condition_string(fullquery)
       return nil unless query = quoted(fullquery)
       intvals = ['id', 'type_id']
       condition = ""
       query.each do |key, value|
          unless condition.empty?
             condition << " and "
          end
          if intvals.include?(key)
             conditions << "histories.#{key} = '#{value}'"
          else
             condition << "histories.#{key} like '%#{value}%'"
          end
       end
       return sanitize_sql_for_conditions(condition)
    end
    
    # prevents saving or updating records
    def refuse
       return false
    end
end
