# == Schema Information
#
# Table name: announcements
#
#  id          :integer(4)      not null, primary key
#  subject     :string(255)
#  body        :text
#  created_at  :datetime
#  updated_at  :datetime
#  created_by  :integer(4)      default(1)
#  updated_by  :integer(4)      default(1)
#  unignorable :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Announcement Model
# These records are short news items to display on the home page.
class Announcement < ActiveRecord::Base
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   has_and_belongs_to_many :ignorers, :class_name => 'User', :join_table => 'announce_ignores'

   after_save :check_creator
   before_destroy :check_and_destroy_dependencies
   
   validates_presence_of :subject, :body

   # provides a short description of the model for use in the
   # DatabaseController index view
   def self.desc
      "Front page announcements"
   end
   
   # returns base permissions required to View objects in this model
   def self.base_perms
      Perm[:view_announcement]
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   def check_and_destroy_dependencies
      self.ignorers.clear
      return true
   end
   
   # accepts a query hash and turns it into a partial SQL query string
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ["created_by"]
      condition = ""
      query.each do |key, value|
         if key == 'text'
            unless condition.empty?
               condition << " and "
            end
            condition << "(announcements.body like '%#{value}%' or announcements.subject like '%#{value}%')"
         else
            unless condition.empty?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "announcements.#{key} = '#{value}'"
            else
               condition << "announcements.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
