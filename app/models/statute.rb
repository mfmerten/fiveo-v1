# == Schema Information
#
# Table name: statutes
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  created_by :integer(4)      default(1)
#  updated_by :integer(4)      default(1)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Statute Model
# State and local statutes (laws) for use as charges on arrests and dockets.
class Statute < ActiveRecord::Base
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'

   after_save :check_creator

   validates_presence_of :name

   # a short description for the DatabaseController index view.
   def self.desc
      "Statutes (laws)"
   end

   # returns the first record that contains the supplied string.
   def self.find_first_by_name_partial(name)
      return nil if name.nil? || name.empty?
      find(:first, :order => "name", :conditions => ["name like ?", '%' + name + '%'])
   end
   
   # returns base permissions required to view statutes
   def self.base_perms
      Perm[:view_statute]
   end
   
   private

   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end

   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      query.each do |key, value|
         unless condition.empty?
            condition << " and "
         end
         condition << "statutes.#{key} like '%#{value}%'"
      end
      return sanitize_sql_for_conditions(condition)
   end
end
