# == Schema Information
#
# Table name: message_logs
#
#  id               :integer(4)      not null, primary key
#  receiver_deleted :boolean(1)
#  receiver_purged  :boolean(1)
#  sender_deleted   :boolean(1)
#  sender_purged    :boolean(1)
#  read_at          :datetime
#  receiver_id      :integer(4)
#  sender_id        :integer(4)
#  subject          :string(255)
#  body             :text
#  created_at       :datetime
#  updated_at       :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
class MessageLog < ActiveRecord::Base
   belongs_to :sender, :class_name => 'User', :foreign_key => 'sender_id'
   belongs_to :receiver, :class_name => 'User', :foreign_key => 'receiver_id'
   
   # no validations, these messages are recorded as-is
   
   def self.desc
      'Message Logs'
   end
   
   # remove all records > SiteConfig.message_retention days old
   def self.purge
      if SiteConfig.message_retention.blank?
         ret_policy = 0
      else
         ret_policy = SiteConfig.message_retention
      end
      # allow 0 or blank to indicate message retention forever
      unless ret_policy == 0
         exp_datetime = ret_policy.days.ago
         delete_all(["created_at < ?",exp_datetime])
      end
   end
   
   private
   
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      # build an sql WHERE string
      condition = ""
      query.each do |key, value|
         if key == "created_from"
            # convert date to y-m-d for mysql use or discard it if invalid
            if myval = valid_date(value)
               # must be ok now
               unless condition.empty?
                  condition << " and "
               end
               condition << "message_logs.created_at >= '#{myval} 00:00'"
            else
               fullquery.delete(key)
            end
         elsif key == "created_to"
            # convert date to y-m-d for mysql use or discard it if invalid
            if myval = valid_date(value)
               # must be ok now
               unless condition.empty?
                  condition << " and "
               end
               condition << "message_logs.created_at <= '#{myval} 23:59'"
            else
               fullquery.delete(key)
            end
         elsif key == "user_id"
            unless condition.empty?
               condition << " and "
            end
            condition << "(message_logs.receiver_id = '#{value}' OR message_logs.sender_id = '#{value}')"
         else
            unless condition.empty?
               condition << " and "
            end
            condition << "message_logs.#{key} like '%#{value}%'"
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
   
end
