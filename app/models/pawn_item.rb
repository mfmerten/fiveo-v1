# == Schema Information
#
# Table name: pawn_items
#
#  id           :integer(4)      not null, primary key
#  pawn_id      :integer(4)
#  pawn_type_id :integer(4)
#  model_no     :string(255)
#  serial_no    :string(255)
#  description  :string(255)
#  created_by   :integer(4)      default(1)
#  updated_by   :integer(4)      default(1)
#  created_at   :datetime
#  updated_at   :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Pawn Item Model
# These are line items for pawn tickets.
class PawnItem < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :pawn
   belongs_to :pawn_type, :class_name => 'Option', :foreign_key => 'pawn_type_id'
   
   after_save :check_creator
   
   validates_presence_of :description
   validates_option :pawn_type_id, :option_type => 'Pawn Type'
   
   
   # a short description for the DatabaseController index view.
   def self.desc
      "Pawn Ticket Items"
   end
   
   # returns true if the specified option id is in use in any of the option
   # type fields for any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(pawn_type_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   
end
