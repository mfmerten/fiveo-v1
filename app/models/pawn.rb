# == Schema Information
#
# Table name: pawns
#
#  id             :integer(4)      not null, primary key
#  pawn_co_id     :integer(4)
#  ticket_no      :string(255)
#  ticket_date    :date
#  ticket_type_id :integer(4)
#  case_no        :string(255)
#  remarks        :text
#  created_at     :datetime
#  updated_at     :datetime
#  created_by     :integer(4)      default(1)
#  updated_by     :integer(4)      default(1)
#  full_name      :string(255)
#  oln            :string(255)
#  oln_state_id   :integer(4)
#  ssn            :string(255)
#  address1       :string(255)
#  address2       :string(255)
#  legacy         :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Pawn Model
# Pawn shop tickets.
class Pawn < ActiveRecord::Base
   belongs_to :pawn_co, :class_name => "Option", :foreign_key => 'pawn_co_id'
   belongs_to :ticket_type, :class_name => "Option", :foreign_key => "ticket_type_id"
   belongs_to :oln_state, :class_name => "Option", :foreign_key => "oln_state_id"
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   has_many :pawn_items, :dependent => :destroy, :include => :pawn_type

   # this sets up sphinx indices for this model
   define_index do
      indexes pawn_co.long_name, :as => :pawnco
      indexes full_name
      indexes address1
      indexes address2
      indexes ssn
      indexes oln
      indexes ticket_no
      indexes ticket_date
      indexes case_no
      indexes remarks
      indexes pawn_items.model_no, :as => :item_models
      indexes pawn_items.serial_no, :as => :item_serials
      indexes pawn_items.description, :as => :item_descs
      
      has updated_at
   end
   
   before_validation :adjust_case_no, :fix_name
   after_update :save_items
   after_save :check_creator
   
   validates_presence_of :ticket_no, :full_name
   validates_date_in_past :ticket_date
   validates_option :pawn_co_id, :option_type => 'Pawn Company'
   validates_option :ticket_type_id, :option_type => 'Pawn Ticket Type'
   validates_option :oln_state_id, :option_type => 'State', :allow_nil => true
   validates_presence_of :oln_state_id, :if => :oln?
   validates_ssn :ssn, :allow_blank => true
   
   # a short description for the DatabaseController index view.
   def self.desc
      "Pawn Tickets"
   end
   
   # returns true if the specified option id is in use in any of the option
   # type fields for any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(pawn_co_id = '#{oid}' OR ticket_type_id = '#{oid}' OR oln_state_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns the number of items to be reviewed by supplied user_id
   def self.review_count(user_id)
      return 0 unless user = User.find_by_id(user_id)
      if user.last_pawn.nil? || !user.last_pawn.is_a?(Integer)
         user.update_attribute(:last_pawn, 0)
      end
      count(:conditions => ['id > ?',user.last_pawn])
   end
   
   # returns base permissions required to View pawn tickets
   def self.base_perms
      Perm[:view_pawn]
   end

   # accepts an array of new pawn item attributes and builds records for
   # them using the pawn_items association.
   def new_pawn_item_attributes=(pawn_item_attributes)
      pawn_item_attributes.each do |attributes|
         unless attributes[:description].blank?
            pawn_items.build(attributes)
         end
      end
   end

   # accepts an array of pawn item attributes and compares them to the
   # existing pawn_items association.  If item does not exist in the attribute
   # list, it will be destroyed, otherwise the existing record is updated.
   def existing_pawn_item_attributes=(pawn_item_attributes)
      pawn_items.reject(&:new_record?).each do |p|
         attributes = pawn_item_attributes[p.id.to_s]
         if attributes
            p.attributes = attributes
         else
            pawn_items.delete(p)
         end
      end
   end
   
   # returns a test string describing pawn ticket items
   def item_summary
      pawn_items.collect{|i| "#{i.description}#{i.model_no? ? ' (Model: ' + i.model_no + ')' : ''}#{i.serial_no? ? ' (Serial: ' + i.serial_no + ')' : ''}"}.to_sentence(:last_word_connector => " and ")
   end
   
   private
   
   # strips all characters from case_no except for numbers, letters, and -
   def adjust_case_no
      self.case_no = fix_case(case_no)
      return true
   end
   
   # break up the name then put it back together again (normalize it)
   def fix_name
      unless full_name.blank?
         self.full_name = unparse_name(parse_name(full_name), true)
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end

   # ensures that the associated pawn items are saved upon record updates.
   def save_items
      pawn_items.each do |i|
         i.save(false)
      end
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intvals = ['pawn_co_id','ticket_type_id','oln_state_id']
      condition = ""
      query.each do |key, value|
         if key == 'desc'
            unless condition.empty?
               condition << " and "
            end
            condition << "(pawn_items.model_no like '%#{value}%' or pawn_items.serial_no like '%#{value}%' or pawn_items.description like '%#{value}%')"
         elsif key == "from"
            if myval = valid_date(value)
              unless condition.empty?
                 condition << " and "
              end
               condition << "pawns.ticket_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "pawns.ticket_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else
            unless condition.empty?
               condition << " and "
            end
            if intvals.include?(key)
               condition << "pawns.#{key} = '#{value}'"
            else
               condition << "pawns.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
