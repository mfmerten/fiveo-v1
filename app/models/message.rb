# == Schema Information
#
# Table name: messages
#
#  id               :integer(4)      not null, primary key
#  receiver_deleted :boolean(1)
#  receiver_purged  :boolean(1)
#  sender_deleted   :boolean(1)
#  sender_purged    :boolean(1)
#  read_at          :datetime
#  receiver_id      :integer(4)
#  sender_id        :integer(4)
#  subject          :string(255)     not null
#  body             :text
#  created_at       :datetime
#  updated_at       :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Message Model
# This is the model for the RESTfulEasyMessages plugin.
class Message < ActiveRecord::Base
    belongs_to :sender, :class_name => "User", :foreign_key => "sender_id"
    belongs_to :receiver, :class_name => "User", :foreign_key => "receiver_id"
    
    before_create :check_for_bug_replies
    after_create :write_to_log, :send_any_emails
    
    validates_presence_of :subject, :body
    validates_user :receiver_id, :sender_id
    
    # a short description for the DatabaseController index view.
    def self.desc
       "User to User messages"
    end
    
    # if the specified user is the receiver of the message, this marks the
    # message as having been read.    
    def mark_message_read(user)
        if user.id == receiver_id
            self.read_at = DateTime.now
            self.save false
        end
    end

    # Performs a hard delete of a message.  Only called from destroy
    def purge
        if sender_purged && receiver_purged
            self.destroy
        end
    end

    private
    
    # returns false if the message is detected to be a reply to a bug report
    # message.
    def check_for_bug_replies
       return false if subject =~ /^Re:.*Bug#\d+/
       return true
    end
    
    def write_to_log
       m = MessageLog.new
       m.update_attributes(attributes)
       m.save(false)
    end
    
    # this checks to see if the user wants email about the new message
    # and sends it if so.  Note, because of the way this message is called,
    # it is important to never create a message and THEN update the contents
    # because this only sends email upon initial creation, not for updates.
    def send_any_emails
       # check for a valid receiver...
       unless receiver.nil?
          # if the user has the preference set...
          if receiver.send_email?
             # next, the receiver must have a valid contact record...
             unless receiver.contact.nil?
                # AND the pri_email field must be entered
                if receiver.contact.pri_email?
                   # finally!  send the email
                   MessageMailer.deliver_message_email(SiteConfig.agency,self)
                end
             end
          end
       end
    end
end
