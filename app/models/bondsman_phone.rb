# == Schema Information
#
# Table name: bondsman_phones
#
#  id          :integer(4)      not null, primary key
#  bondsman_id :integer(4)
#  label_id    :integer(4)
#  value       :string(255)
#  note        :string(255)
#  created_by  :integer(4)
#  updated_by  :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# BondsmanPhone Model
# Phone records for Bondsmen
class BondsmanPhone < ActiveRecord::Base
   belongs_to :bondsman
   belongs_to :label, :class_name => "Option", :foreign_key => 'label_id'
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   
   after_save :check_creator
   
   validates_phone :value
   validates_option :label_id, :option_type => 'Phone Type'
   
   # a short description of the model for use by the DatabaseController index
   # view.
   def self.desc
      "Phone numbers for Bondsmen"
   end
   
   # returns true if the indicated option id is used by any record in any
   # option type field.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(label_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_option]
   end
   
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
end
