# == Schema Information
#
# Table name: forbids
#
#  id                      :integer(4)      not null, primary key
#  call_id                 :integer(4)
#  case_no                 :string(255)
#  forbidden_id            :integer(4)
#  request_date            :date
#  receiving_officer       :string(255)
#  receiving_officer_unit  :string(255)
#  signed_date             :date
#  serving_officer         :string(255)
#  serving_officer_unit    :string(255)
#  recall_date             :date
#  details                 :text
#  remarks                 :text
#  created_by              :integer(4)
#  updated_by              :integer(4)
#  created_at              :datetime
#  updated_at              :datetime
#  receiving_officer_id    :integer(4)
#  receiving_officer_badge :string(255)
#  serving_officer_id      :integer(4)
#  serving_officer_badge   :string(255)
#  complainant             :string(255)
#  comp_street             :string(255)
#  comp_city_state_zip     :string(255)
#  comp_phone              :string(255)
#  forbid_last             :string(255)
#  forbid_first            :string(255)
#  forbid_suffix           :string(255)
#  forbid_street           :string(255)
#  forbid_city_state_zip   :string(255)
#  forbid_phone            :string(255)
#  legacy                  :boolean(1)      default(FALSE)
#  forbid_middle1          :string(255)
#  forbid_middle2          :string(255)
#  forbid_middle3          :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Forbid Model
# Documents where one person "forbids" another person to enter or remain upon
# the specified property.  ("Forbidden To Enter" forms)
class Forbid < ActiveRecord::Base
   belongs_to :creator, :class_name => "User", :foreign_key => "created_by"
   belongs_to :updater, :class_name => "User", :foreign_key => "updated_by"
   belongs_to :call
   has_and_belongs_to_many :offenses
   belongs_to :person, :class_name => "Person", :foreign_key => "forbidden_id"
   has_and_belongs_to_many :exemptions, :class_name => 'Person', :join_table => 'forbid_exemptions'
   
   # used to allow entering full name on form but saving it as individual name
   # parts in the record.
   attr_accessor :name
   
   # this sets up sphinx indices for this model
   define_index do
      indexes [forbid_last, forbid_first, forbid_middle1, forbid_middle2, forbid_middle3, forbid_suffix], :as => :name
      indexes forbid_last
      indexes forbid_first
      indexes forbid_middle1
      indexes forbid_middle2
      indexes forbid_middle3
      indexes forbid_street
      indexes forbid_city_state_zip
      indexes complainant
      indexes comp_street
      indexes comp_city_state_zip
      indexes case_no
      indexes receiving_officer
      indexes receiving_officer_unit
      indexes serving_officer
      indexes serving_officer_unit
      indexes details
      indexes remarks
      
      has updated_at
   end
   
   before_validation :split_name, :adjust_case_no, :lookup_officers
   after_save :check_creator
   before_destroy :check_and_destroy_dependencies
   
   validates_presence_of :receiving_officer, :details, :complainant, :forbid_last, :forbid_first, :comp_street, :comp_city_state_zip
   validates_date :request_date
   validates_date :signed_date, :recall_date, :allow_nil => true
   validates_presence_of :serving_officer, :if => :signed_date?
   validates_presence_of :forbidden_id, :if => :signed_date?, :message => 'is required for signed forbids.'
   validates_person :forbidden_id, :allow_nil => true
   validates_phone :comp_phone
   validates_phone :forbid_phone, :allow_blank => true
   validates_call :call_id, :allow_nil => true
   
   # a short description for use in the DatabaseController index view.
   def self.desc
      "Forbidden To Enter forms"
   end
   
   # returns base permissions required to View forbids
   def self.base_perms
      Perm[:view_forbid]
   end

   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(forbidden_id = '#{p_id}')
      count(:conditions => cond) > 0
   end

   # returns all forbids for the given person identified by id
   def self.forbids_for(person_id, include_linked = true)
      # interested in non-linked stuff, so can't search directly by
      # person_id
      return [] unless p = Person.find_by_id(person_id)
      conditions = ""
      # consider first and middle names together
      allnames = [p.firstname]
      allnames.concat(p.middlename? ? p.middlename.split : [])
      # get initials from all first and middle names
      allinits = allnames.collect{|n| n.slice(0,1)}
      # build a "set" string for SQL
      allnamestring = allnames.collect{|n| "'#{n}'"}.join(',')
      allinitstring = allinits.collect{|i| "'#{i}'"}.join(',')
      # do the same thing for any part of the name that was only an initial.
      initstring = allnames.reject{|n| n.size > 1}.collect{|n| "'#{n}'"}.join(',')
      if p.lastname?
         # check last name, also check middle names and any initials to check for married names
         conditions << "(forbid_last = '' OR forbid_last IS NULL OR forbid_last LIKE '#{p.lastname}')"
      end
      unless allnamestring.blank?
         unless conditions.blank?
            conditions << " AND "
         end
         # rest of names could be nil, an actual name or an initial and could be in any position
         # first name
         conditions << "(forbid_first = '' OR forbid_first IS NULL OR (LENGTH(forbid_first) > 1 AND (forbid_first IN (#{allnamestring})#{initstring.blank? ? '' : ' OR LEFT(forbid_first,1) IN (' + initstring + ')'})) OR (LENGTH(forbid_first) = 1 AND forbid_first IN (#{allinitstring})))"
         conditions << " AND "
         # middle name 1
         conditions << "(forbid_middle1 = '' OR forbid_middle1 IS NULL OR (LENGTH(forbid_middle1) > 1 AND (forbid_middle1 IN (#{allnamestring})#{initstring.blank? ? '' : ' OR LEFT(forbid_middle1,1) IN (' + initstring + ')'})) OR (LENGTH(forbid_middle1) = 1 AND forbid_middle1 IN (#{allinitstring})))"
         conditions << " AND "
         # middle name 2
         conditions << "(forbid_middle2 = '' OR forbid_middle2 IS NULL OR (LENGTH(forbid_middle2) > 1 AND (forbid_middle2 IN (#{allnamestring})#{initstring.blank? ? '' : ' OR LEFT(forbid_middle2,1) IN (' + initstring + ')'})) OR (LENGTH(forbid_middle2) = 1 AND forbid_middle2 IN (#{allinitstring})))"
         conditions << " AND "
         # middle name 3
         conditions << "(forbid_middle3 = '' OR forbid_middle3 IS NULL OR (LENGTH(forbid_middle3) > 1 AND (forbid_middle3 IN (#{allnamestring})#{initstring.blank? ? '' : ' OR LEFT(forbid_middle3,1) IN (' + initstring + ')'})) OR (LENGTH(forbid_middle3) = 1 AND forbid_middle3 IN (#{allinitstring})))"
      end
      if p.suffix?
         unless conditions.blank?
            conditions << " AND "
         end
         conditions << "(forbid_suffix LIKE '#{p.suffix}' OR forbid_suffix = '')"
      end
      unless conditions.blank?
         conditions << " AND "
      end
      conditions << "recall_date IS NULL"
      if include_linked
         conditions << " AND (forbidden_id = #{p.id} OR forbidden_id IS NULL)"
      else
         conditions << " AND forbidden_id IS NULL"
      end
      # do the search
      conditions = sanitize_sql_for_conditions(conditions)
      find(:all, :conditions => conditions)
   end
   
   # returns the number of items to be reviewed by supplied user_id
   def self.review_count(user_id)
      return 0 unless user = User.find_by_id(user_id)
      if user.last_forbid.nil? || !user.last_forbid.is_a?(Integer)
         user.update_attribute(:last_forbid, 0)
      end
      count(:conditions => ['id > ?',user.last_forbid])
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["receiving_officer_id", "serving_officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["forbidden_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      # catch the HABTM tables for people
      # TODO: find a better way to do this
      rec_count += count(:all, :include => :exemptions, :conditions => "forbid_exemptions.person_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE forbid_exemptions SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      return rec_count
   end
   
   # returns the person full name, optionally in last-name-first order.
   def full_name(reverse = true)
      nameparts = HashWithIndifferentAccess.new
      nameparts[:first] = forbid_first
      nameparts[:middle] = forbid_middle
      nameparts[:last] = forbid_last
      nameparts[:suffix] = forbid_suffix
      unparse_name(nameparts, reverse)
   end
   
   def forbid_middle
      "#{forbid_middle1} #{forbid_middle2} #{forbid_middle3}".strip
   end
   
   private
   
   # converts the name attribute to the underlying firstname, middlename,
   # lastname and suffix parts.
   def split_name
      unless name.nil? || name.blank?
         nameparts = parse_name(name)
         self.forbid_first = nameparts[:first]
         if nameparts[:middle].blank?
            self.forbid_middle1, self.forbid_middle2, self.forbid_middle3 = []
         else
            self.forbid_middle1, self.forbid_middle2, self.forbid_middle3 = nameparts[:middle].split
         end
         self.forbid_last = nameparts[:last]
         self.forbid_suffix = nameparts[:suffix]
      end
      # while we're at it, normalize the complainant, if present
      if complainant?
         self.complainant = unparse_name(parse_name(complainant), true)
      end
      return true
   end
   
   # strips all characters from case_no except for numbers, letters, and -
   def adjust_case_no
      self.case_no = fix_case(case_no)
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if receiving_officer_id?
         if ofc = Contact.find_by_id(receiving_officer_id)
            self.receiving_officer = ofc.fullname
            self.receiving_officer_badge = ofc.badge_no
            self.receiving_officer_unit = ofc.unit
         end
      end
      if serving_officer_id?
         if ofc = Contact.find_by_id(serving_officer_id)
            self.serving_officer = ofc.fullname
            self.serving_officer_badge = ofc.badge_no
            self.serving_officer_unit = ofc.unit
         end
      end
   end
   
   def check_and_destroy_dependencies
      self.offenses.clear
      return true
   end
   
   # accepts a query hash and creates a SQL partial suitable for use in a
   # WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      intvals = ['forbidden_id']
      query.each do |key, value|
         if key == 'complainant'
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "forbids.complainant like '%#{nameparts[:first]}%'"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "forbids.complainant like '%#{nameparts[:middle]}%'"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "forbids.complainant like '%#{nameparts[:last]}%'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "forbids.complainant like '%#{nameparts[:suffix]}%'"
            end
         elsif key == 'name'
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               init = nameparts[:first].slice(0,1).upcase
               unless condition.blank?
                  condition << " AND "
               end
               condition << "(forbids.forbid_first LIKE '#{nameparts[:first]}' OR LEFT(forbids.forbid_first,1) LIKE '#{init}' OR forbids.forbid_middle1 LIKE '#{nameparts[:first]}' OR LEFT(forbids.forbid_middle1,1) LIKE '#{init}' OR forbids.forbid_middle2 LIKE '#{nameparts[:first]}' OR LEFT(forbids.forbid_middle2,1) LIKE '#{init}' OR forbids.forbid_middle3 LIKE '#{nameparts[:first]}' OR LEFT(forbids.forbid_middle3,1) LIKE '#{init}')"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " AND "
               end
               parts = nameparts[:middle].split.collect{|p| "'#{p}'"}.join(',')
               inits = nameparts[:middle].split.collect{|p| "'#{p.slice(0,1)}'"}.join(',')
               condition << "(forbids.forbid_middle1 IN (#{parts}) OR LEFT(forbids.forbid_middle1,1) IN (#{inits}) OR forbids.forbid_middle2 IN (#{parts}) OR LEFT(forbids.forbid_middle2,1) IN (#{inits}) OR forbids.forbid_middle3 IN (#{parts}) OR LEFT(forbids.forbid_middle3,1) IN (#{inits}) OR forbids.forbid_first IN (#{parts}) OR LEFT(forbids.forbid_first,1) IN (#{inits}))"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "forbid_last like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "forbid_suffix like '#{nameparts[:suffix]}'"
            end
         elsif key == 'address'
            unless nameparts[:suffix].blank?
               condition << " and "
            end
            condition << "(forbids.comp_street like '%#{value}%' OR forbids.comp_city_state_zip like '%#{value}%' OR forbids.forbid_street like '%#{value}%' OR forbids.forbid_city_state_zip like '%#{value}%')"
         elsif key == "requested_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "forbids.requested_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "requested_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "forbids.requested_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "signed_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "forbids.signed_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "signed_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "forbids.signed_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "recall_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "forbids.recall_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "recall_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "forbids.recall_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "receiving_officer"
            unless condition.empty?
               condition << " and "
            end
            condition << "(forbids.receiving_officer like '%#{value}%' or forbids.receiving_officer_unit like '%#{value}%')"
         elsif key == "serving_officer"
            unless condition.empty?
               condition << " and "
            end
            condition << "(forbids.serving_officer like '%#{value}%' or forbids.serving_officer_unit like '%#{value}%')"
         else
            unless condition.empty?
               condition << " and "
            end
            if intvals.include?(key)
               condition << "forbids.#{key} = '#{value}'"
            else
               condition << "forbids.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
end
