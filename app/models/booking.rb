# == Schema Information
#
# Table name: bookings
#
#  id                    :integer(4)      not null, primary key
#  person_id             :integer(4)
#  cash_at_booking       :decimal(12, 2)  default(0.0)
#  cash_receipt          :string(255)
#  booking_officer       :string(255)
#  booking_officer_unit  :string(255)
#  booking_date          :date
#  booking_time          :string(255)
#  facility_id           :integer(4)
#  cell_id               :integer(4)
#  remarks               :text
#  phone_called          :string(255)
#  intoxicated           :boolean(1)
#  sched_release_date    :date
#  release_date          :date
#  release_time          :string(255)
#  afis                  :boolean(1)
#  release_officer       :string(255)
#  release_officer_unit  :string(255)
#  created_by            :integer(4)
#  updated_by            :integer(4)
#  created_at            :datetime
#  updated_at            :datetime
#  booking_officer_id    :integer(4)
#  booking_officer_badge :string(255)
#  release_officer_id    :integer(4)
#  release_officer_badge :string(255)
#  good_time             :boolean(1)      default(TRUE)
#  disable_timers        :boolean(1)      default(FALSE)
#  attorney              :string(255)
#  legacy                :boolean(1)      default(FALSE)
#  released_because_id   :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Booking Model
# Jail inmate records
class Booking < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :facility, :class_name => 'Option', :foreign_key => 'facility_id'
   belongs_to :cell, :class_name => 'Option', :foreign_key => 'cell_id'
   belongs_to :person
   has_many :holds, :dependent => :destroy
   has_many :holds_review, :class_name => 'Hold', :foreign_key => 'booking_id', :include => :hold_type
   has_many :medicals, :dependent => :destroy
   has_many :properties, :dependent => :destroy
   has_many :visitors, :dependent => :destroy
   has_and_belongs_to_many :absents, :include => :leave_type
   has_many :transfers
   belongs_to :released_because, :class_name => 'Option', :foreign_key => 'released_because_id'
   has_many :booking_logs
   
   # this sets up sphinx indices for this model
   define_index do
      indexes [person.lastname, person.firstname, person.middlename, person.suffix], :as => :name
      indexes booking_date
      indexes remarks
      indexes properties.description, :as => :props
      
      has updated_at
   end
   
   before_validation :fix_times, :lookup_officers, :check_person
   after_save :check_creator, :update_person, :check_timers
   after_update :save_properties
   before_destroy :check_and_destroy_dependencies
   
   validates_presence_of :booking_officer
   validates_date :booking_date
   validates_time :booking_time
   validates_date :sched_release_date, :release_date, :allow_nil => true
   validates_time :release_time, :allow_nil => true
   validates_presence_of :release_time, :if => :release_date?
   validates_presence_of :release_date, :if => :release_time?
   validates_presence_of :released_because_id, :if => :release_date?
   validates_presence_of :release_date, :if => :released_because_id?
   validates_presence_of :release_officer, :if => :release_date?
   validates_option :released_because_id, :option_type => 'Release Reason', :allow_nil => true
   validates_phone :phone_called, :allow_blank => true
   validates_numericality_of :cash_at_booking, :message => 'must be a number!'
   validates_option :facility_id, :option_type => 'Facility'
   validates_option :cell_id, :option_type => 'Cell'
   validates_person :person_id
   
   # a short description of the model for use by the DatabaseController index
   # view.
   def self.desc
      'Booking Records'
   end
   
   # returns all records (or only current records) where the associated person
   # record matches a name partial search query
   def self.locate_by_name(name, show_all = false)
      return [] unless name.is_a?(String)
      names = quoted(name.split)
      cond = ''
      names.each do |n|
         unless cond.empty?
            cond << ' OR '
         end
         cond << "people.firstname like '%#{n}%' OR people.middlename like '%#{n}%' OR people.lastname like '%#{n}%' OR people.suffix like '%#{n}%'"
      end
      if show_all == true
         find(:all, :include => :person, :order => 'people.lastname, people.firstname', :conditions => sanitize_sql_for_conditions(cond))
      else
         find(:all, :include => :person, :order => 'people.lastname, people.firstname', :conditions => sanitize_sql_for_conditions("(#{cond}) AND release_date IS NULL"))
      end
   end
   
   # returns true if any record uses the specified option id in any of its
   # option type fields.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(facility_id = '#{oid}' OR cell_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   # accepts a date value, a status, and optionally a facility id. Returns an
   # array of all records that should be included on the nightly headcount report
   # for that day and status.
   def self.headcount(date = Date.yesterday, status = nil, facility_id = nil)
      # requires a valid date
      return [] if !date.is_a?(Date) || date >= Date.today
      return [] unless ['Temporary Release', 'DOC', 'DOC Billable', 'City', 'Parish', 'Other', 'ALL', 'Not Billable'].include?(status)
      # get the facility if there is one
      facility = Option.find_by_id(facility_id)
      # get all inmates that were held on the specified date
      if facility.nil?
         list = find(:all, :include => [:holds, :person], :order => 'people.lastname, people.firstname, people.middlename, people.suffix, bookings.id', :conditions => ['bookings.booking_date <= ? and (bookings.release_date is null or bookings.release_date >= ?)', date, date])
      else
         list = find(:all, :include => [:holds, :person], :order => 'people.lastname, people.firstname, people.middlename, people.suffix, bookings.id', :conditions => ['bookings.booking_date <= ? and (bookings.release_date is null or bookings.release_date >= ?) and facility_id = ?',date,date,facility])
      end
      # exclude any that produce an error
      if status == 'ALL'
         other = []
         doc = []
         doc_bill = []
         city = []
         parish = []
         temp = []
         nb = []
         err = []
         list.each do |b|
            stat = b.billing_status(date)
            case stat
            when 'Error'
               err.push(b)
            when 'DOC'
               doc.push(b)
            when 'DOC Billable'
               doc_bill.push(b)
            when 'City'
               city.push(b)
            when 'Parish'
               parish.push(b)
            when 'Temporary Release'
               temp.push(b)
            when 'Not Billable'
               nb.push(b)
            else
               other.push(b)
            end
         end
         return [temp, doc, doc_bill, city, parish, other, nb]
      elsif status == 'Other'
         list.reject{|b| ['Error', 'DOC', 'DOC Billable', 'City', 'Parish','Temporary Release', 'Not Billable'].include?(b.billing_status(date)) }
      else
         list.reject{|b| b.billing_status(date) != status }
      end
   end
   
   # this method is called from a rake task by cron once an hour and is used to
   # increment the hours_served and optionally the hours_goodtime fields in each
   # active sentence hold for all active booking records.
   def self.update_sentences_by_hour
      Activity.create(:section => "Admin", :description => "Beginning jail time served update scan...", :user_id => 1)
      # loop through all current booking records
      sentence_hold = Option.lookup("Hold Type","Serving Sentence",nil,nil,true)
      fines_hold = Option.lookup("Hold Type","Fines/Costs",nil,nil,true)
      find(:all, :conditions => 'release_date IS NULL').each do |book|
         # loop through all holds that have not been cleared
         book.holds.reject{|h| h.cleared_date? || (h.hold_type_id != sentence_hold && h.hold_type_id != fines_hold) || h.active_blockers?}.each do |hold|
            hold.update_hour_counters
         end
      end
      # Additionally, check for any Probations that have timed out
      Probation.update_probation_status
   end
   
   # this method added to update Cashless Systems INC database with current
   # active booking list when called from rake task.
   def self.update_cashless_systems
     require 'csv'
     inmate_list = []
     find(:all, :conditions => 'release_date IS NULL').each do |b|
       inmate_resident_id = b.id
       inmate_personal_id = b.person.id
       inmate_lastname = b.person.lastname
       inmate_firstname = b.person.firstname
       inmate_middlename = b.person.middlename
       classification = b.billing_status
       case classification
       when "Parish"
         inmate_class_id = "PAR"
       when "DOC Billable" , "DOC"
         inmate_class_id = "DOC"
       when "City"
         inmate_class_id = "CTY"
       else
         inmate_class_id = "UNK"
       end
       inmate_ssn = nil
       if b.person.date_of_birth?
         inmate_dob = b.person.date_of_birth.to_s(:us)
       else
         inmate_dob = nil
       end
       inmate_race_id = nil
       inmate_gender = b.person.sex_name(true)
       if !b.facility.nil?
         inmate_unit_id = b.facility.short_name
       else
         inmate_unit_id = 'UNK'
       end
       inmate_bldg_id = nil
       inmate_pod_id = nil
       inmate_section_id = nil
       inmate_bay_id = nil
       if !b.cell.nil?
         inmate_bed = b.cell.long_name
       else
         inmate_bed = 'UNK'
       end
       inmate_address = nil
       inmate_list.push([
         inmate_resident_id, inmate_personal_id, inmate_lastname, inmate_firstname, inmate_middlename,
         inmate_class_id, inmate_ssn, inmate_dob, inmate_race_id, inmate_gender, inmate_unit_id, inmate_bldg_id,
         inmate_pod_id, inmate_section_id, inmate_bay_id, inmate_bed, inmate_address
       ])
     end
     inmate_list.unshift([
       "ResidentID","PersonalId","LastName","FirstName","MiddleName","ClassId","Ssn","Dob","RaceId","Gender",
       "UnitId","BldgId","PodId","SectionId","BayId","Bed","Address"
     ])
     # generate the csv file
     csvstring = ''
     CSV::Writer.generate(csvstring) do |csv|
       inmate_list.each do |l|
         csv << l
       end
     end
     fname = "/tmp/sabineinmates.txt"
     outfile = File.open(fname,'w')
     outfile.puts(csvstring)
     outfile.close
     if ENV['RAILS_ENV'] == 'production'
       # FTP the result to Cashless Systems, Inc
       require 'net/ftp'
       ftp=Net::FTP.new
       ftp.connect("sabine.cactas.com",21)
       ftp.login("sabine","cactas4sabine")
       ftp.putbinaryfile("/tmp/sabineinmates.txt")
       ftp.close
     end
   end
   
   # base permissions required to View these objects
   def self.base_perms
      Perm[:view_booking]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["booking_officer_id", "release_officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # return a list of booking ids that need attention
   def self.needs_attention
      bookings = find(:all, :include => [{:holds => :hold_type}], :conditions => 'release_date IS NULL')
      problems = []
      sentence_hold = Option.lookup("Hold Type","Serving Sentence",nil,nil,true)
      fortyeight = Option.lookup("Hold Type","48 Hour",nil,nil,true)
      seventytwo = Option.lookup("Hold Type","72 Hour",nil,nil,true)
      bookings.each do |b|
         current_holds = b.holds.reject(&:cleared_date?)
         reasons = []
         # no holds
         if current_holds.empty?
            reasons.push("No Holds")
         end
         # time served
         current_holds.reject{|h| h.hold_type_id != sentence_hold || h.hours_total.nil? || h.hours_total <= 0}.each do |h|
            if (h.hours_total - (h.hours_served? && h.hours_served >= 0 ? h.hours_served : 0) - (h.hours_goodtime? && h.hours_goodtime >= 0 ? h.hours_goodtime.round : 0) - (h.hours_time_served? && h.hours_time_served >= 0 ? h.hours_time_served : 0)) <= 0
               reasons.push("Sentence Time Served")
            end
         end
         # expired 48 or 72
         current_holds.reject{|h| h.hold_type_id != fortyeight && h.hold_type_id != seventytwo}.each do |h|
            hold_datetime = get_datetime(h.hold_date, h.hold_time)
            if (h.hold_type_id == fortyeight && hours_diff(DateTime.now, hold_datetime) > 48) || (h.hold_type_id == seventytwo && hours_diff(DateTime.now, hold_datetime) > 72)
               reasons.push("48/72 Expired")
            end
         end
         # doc missing information
         current_holds.reject{|h| !h.doc? }.each do |h|
            next if h.doc_record.nil?
            if !h.doc_record.conviction? || h.doc_record.to_serve_string.blank? || !h.doc_record.doc_number?
               reasons.push("DOC Needs Info")
            end
         end
         unless reasons.empty?
            problems.push([b, reasons.uniq.join(', ')])
         end
      end
      return problems
   end
   
   # accepts an array of property attributes from the BookingsController
	# edit view and creates property records linked to this booking.
   def new_property_attributes=(property_attributes)
      property_attributes.each do |attributes|
         unless attributes[:description].blank?
            properties.build(attributes)
         end
      end
   end

   # accepts an array of property attributes from the BookingsController
   # edit view. It cycles through all existing property associations for the
   # booking. If the property is included in the property attributes, the
   # property is updated with the attributes, otherwise the property record is
   # destroyed.
   def existing_property_attributes=(property_attributes)
      properties.reject(&:new_record?).each do |property|
         attributes = property_attributes[property.id.to_s]
         if attributes
            attributes[:updated_by] = updated_by
            property.attributes = attributes
         else
            properties.delete(property)
         end
      end
   end
   
   def temporary_release?(tdate=nil)
      return false unless tdate.nil? || tdate.is_a?(Date)
      if tdate.nil?
         holds.reject{|h| h.hold_type.nil? || h.cleared_date? || h.hold_type.long_name != 'Temporary Release'}.size > 0
      else
         holds.reject{|h| h.hold_type.nil? || h.hold_date > tdate || (h.cleared_date? && h.cleared_date < tdate) || h.hold_type.long_name != 'Temporary Release'}.size > 0
      end
   end

   def temporary_release_reason(tdate=nil)
      return nil unless tdate.nil? || tdate.is_a?(Date)
      if tdate.nil?
         trelease = holds.reject{|h| h.hold_type.nil? || h.cleared_date? || h.hold_type.long_name != 'Temporary Release'}.first
      else
         trelease = holds.reject{|h| h.hold_type.nil? || h.hold_date > tdate || (h.cleared_date? && h.cleared_date < tdate) || h.hold_type.long_name != 'Temporary Release'}.first
      end
      unless trelease.nil?
         unless trelease.temp_release_reason.nil?
            return trelease.temp_release_reason.long_name
         end
      end
      nil
   end

   def stop_time(userid)
      return false unless u = User.find_by_id(userid)
      l = booking_logs.last
      unless l.nil? || l.stop_datetime?
         # there appears to be an active time log entry... stop it
         l.update_attributes(:stop_datetime => DateTime.now, :stop_officer => u.name, :stop_officer_unit => u.unit, :stop_officer_badge => u.badge)
         l.save
         self.update_attribute(:disable_timers, true)
      end
      # of course, if there was not an active log entry, don't worry about it.
   end
   
   def start_time(userid)
      return false unless u = User.find_by_id(userid)
      l = booking_logs.last
      if l.nil? || l.stop_datetime?
         # there appears to be no active time log entry... add one
         self.booking_logs.create(:start_datetime => DateTime.now, :start_officer => u.name, :start_officer_unit => u.unit, :start_officer_badge => u.badge)
         self.update_attribute(:disable_timers, false)
      end
      # of course, if there was an active log entry, don't worry about it.
   end
   
   def time_served_by_log
      total = 0
      booking_logs.each do |l|
         total += l.hours_served
      end
      hours_to_sentence(total)
   end
   
   def simple_time_served_by_log
      total = 0
      booking_logs.each do |l|
         total += l.hours_served
      end
      hours_to_string(total)
   end
   
   def hours_served_since(time)
      # calculate the total time logged since the time specified
      return 0 unless time.is_a?(Time) || time.is_a?(DateTime)
      return 0 if booking_logs.empty?
      hrs = 0
      BookingLog.find(:all, :conditions => ['booking_id = ? AND (start_datetime >= ? OR (start_datetime < ? AND (stop_datetime IS NULL OR stop_datetime >= ?)))',id,time,time,time]).each do |l|
         hrs += l.hours_served(time)
      end
      if hrs < 0
         hrs = 0
      end
      return hrs
   end
   
   def minutes_served_since(time)
      # calculate the total time logged since the time specified
      return 0 unless time.is_a?(Time) || time.is_a?(DateTime)
      return 0 if booking_logs.empty?
      mins = 0
      BookingLog.find(:all, :conditions => ['booking_id = ? AND (start_datetime >= ? OR (start_datetime < ? AND (stop_datetime IS NULL OR stop_datetime >= ?)))',id,time,time,time]).each do |l|
         mins += l.minutes_served(time)
      end
      if mins < 0
         mins = 0
      end
      return mins
   end
   
   def hold_for_probation?
      holds.reject{|h| h.hold_type.nil? || h.cleared_date? || (h.hold_type.long_name != "Probation (Parish)" && h.hold_type.long_name != "Probation/Parole (State)")}.size > 0
   end
   
   # returns true if any of the active holds currently linked to this booking
   # has the doc flag set.
   def doc?
      return false if holds.reject(&:cleared_date?).empty?
      holds.reject(&:cleared_date?).each do |h|
         return true if h.doc?
      end
      return false
   end
   
   # returns true if any of the active holds currently linked to this booking
   # has the doc_billable flag set.
   def doc_billable?
      return false if holds.reject(&:cleared_date?).empty?
      holds.reject(&:cleared_date?).each do |h|
         return true if h.doc_billable?
      end
      return false
   end
   
   def doc_incomplete?
      doc_holds = holds.reject{|h| h.cleared_date? || !h.doc?}
      return false if doc_holds.empty?
      doc_holds.each do |h|
         next if h.doc_record.nil?
         if !h.doc_record.conviction? || h.doc_record.to_serve_string.blank? || !h.doc_record.doc_number?
            return true
         end
      end
      return false
   end
   
   # cycles through all associated property records and builds a text string
   # listing the property and quanty.
   def property_list
      prop = ""
      properties.each do |p|
         unless prop.blank?
            prop << ", "
         end
         prop << "#{p.quantity} #{p.description}"
      end
      if prop.blank?
         "None"
      else
         prop
      end
   end
   
   # examines all pertinent holds and returns a billing status indicating
   # the agency that should be billed for the specified date. If released
   # after the date, bill.  If released on the date, bill except for DOC.
   def billing_status(billing_date = Date.yesterday)
      return 'Error' unless billing_date.is_a?(Date) && billing_date < Date.today
      return 'Error' if billing_date < booking_date
      return 'Error' if release_date? && billing_date > release_date
      released_today = (billing_date == release_date)
      status = []
      holds.each do |h|
         # some holds get start dates before actual booking.  For these, need
         # to consider only actual booking time so get the later date.
         hold_start_date = [h.hold_date, booking_date].sort.pop
         # check hold dates for billing_date
         hold_here_today = (billing_date >= hold_start_date && (!h.cleared_date || billing_date <= h.cleared_date))
         hold_here_yesterday = (billing_date > hold_start_date && hold_here_today)
         hold_here_midnight = (billing_date >= hold_start_date && (!h.cleared_date || billing_date < h.cleared_date))
         hold_cleared_today = (hold_here_today && !hold_here_midnight)
         # determine return value for this hold
         if hold_here_today
            if h.doc_billable? && h.bill_from_date? && (h.bill_from_date <= billing_date)
               # only push DOC billable if here all day or released today
               unless hold_cleared_today && !released_today
                  status.push('DOC Billable')
               end
            elsif h.hold_type.long_name == 'Temporary Release'
               # override all other status if temporary release for entire day only
               if hold_here_yesterday && hold_here_midnight
                  return 'Temporary Release'
               end
            elsif h.doc?
               status.push('DOC')
            elsif h.hold_type.long_name == 'Hold for City'
               status.push("City")
            elsif h.hold_type.long_name == 'Temporary Transfer'
               if h.other_billable?
                  # these get billed to whomever is getting billed normally
                  # therefore do not add a status for this hold
               else
                  if hold_here_yesterday && hold_here_midnight
                     # these should not be billed regardless of their other status
                     # however, only if transferred the entire day
                     return "Not Billable"
                  else
                     # not transferred the whole day, don't add a status
                  end
               end
            elsif h.hold_type.long_name == 'Hold for Other Agency' || h.hold_type.long_name == 'Temporary Housing'
               status.push("#{h.agency.blank? ? 'Unknown' : h.agency}")
            else
               status.push('Parish')
            end
         end
      end
      status = status.uniq
      if status.nil? || status.empty?
         return "Parish"
      elsif status.include?("DOC Billable")
         return "DOC Billable"
      elsif status.include?("DOC")
         return "DOC"
      elsif status.include?("Parish")
         return "Parish"
      elsif status.include?("City")
         return "City"
      elsif !status.include?("Not Billable")
         return status.join(',')
      else
         return "Not Billable"
      end
   end
   
   # checks active holds to determine the current status of an inmate
   def status
      # released takes precedence
      if release_date?
         if released_because.nil?
            return "[Released]"
         else
            return "[Released: #{released_because.long_name}]"
         end
      end
      if holds.reject(&:cleared_date?).empty?
         return "[No Holds]"
      end
      hs = holds.reject(&:cleared_date?)
      statuses = []
      hs.each do |h|
         unless h.hold_type.nil?
            if ["Temporary Transfer","Temporary Housing"].include?(h.hold_type.long_name)
               statuses.push("<span class='notice'>[#{h.hold_type.long_name}]</span>")
            elsif h.hold_type.long_name == "Serving Sentence"
               stl = "color:black;background-color:aquamarine;"
               hrs = h.hours_remaining
               if hrs > 0
                  statuses.push("<span style='#{stl}'>[#{hours_to_string(hrs)}]</span>")
               else
                  if !h.hours_total || h.hours_total <= 0
                     statuses.push("<span style='#{stl}'>[Pending Sentence]</span>")
                  else
                     statuses.push("<span style='#{stl}'>[Time Served]</span>")
                  end
               end
            elsif ["Hold for City","Hold for Other Agency", "No Bond"].include?(h.hold_type.long_name)
               statuses.push("<span class='error'>[#{h.hold_type.long_name}]</span>")
            elsif ["Probation (Parish)","Probation/Parole (State)"].include?(h.hold_type.long_name)
               statuses.push("<span class='error'>[Probation Hold]</span>")
            elsif ["48 Hour", "72 Hour", "Bondable"].include?(h.hold_type.long_name)
               statuses.push("<span style='color:blue;background-color:yellow;'>[#{h.hold_type.long_name}#{h.expired? ? ' EXP' : ''}]</span>")
            elsif h.hold_type.long_name == "Temporary Release"
               statuses.push("<span class='message'><b>[Temp Release]</b></span>")
            else
               statuses.push("[#{h.hold_type.long_name}]")
            end
         end
      end
      return statuses.join(" ")
   end
      
   # checks to see if a transfer is in effect.  Returns agency name if true, nil otherwise
   def is_transferred(datetime = DateTime.now)
      return nil unless datetime.is_a?(Time) || datetime.is_a?(DateTime)
      datetime = get_datetime(datetime)
      transfer_hold = get_option("Hold Type","Temporary Transfer")
      holds.each do |h|
         if h.hold_type_id == transfer_hold
            hold_date = get_datetime(h.hold_date, h.hold_time)
            if h.cleared_date?
               clr_date = get_datetime(h.cleared_date, h.cleared_time)
            else
               clr_date = nil
            end
            if hold_date <= datetime && (clr_date.nil? || clr_date > datetime)
               if h.agency.nil?
                  return "Unknown"
               else
                  return h.agency
               end
            end
         end
      end
      return nil
   end
   
   # checks to see if an inmate is being housed temporarily
   def is_housing
      housing_hold = Option.lookup("Hold Type", "Temporary Housing",nil,nil,true)
      !holds.reject{|h| h.cleared_date? || h.hold_type_id != housing_hold}.empty?
   end
   
   # this method returns true if there is an open sentencing hold for the
   # booking record.
   def is_serving_time
      sentence_hold = Option.lookup("Hold Type","Serving Sentence",nil,nil,true)
      fines_hold = Option.lookup("Hold Type","Fines/Costs",nil,nil,true)
      !holds.reject{|h| h.cleared_date? || (h.hold_type_id != sentence_hold && h.hold_type_id != fines_hold)}.empty?
   end
   
   # returns true if ANY sentencing hold has 0 time left and needs to be
   # processed.
   def is_time_served
      sentence_hold = Option.lookup("Hold Type","Serving Sentence",nil,nil,true)
      fines_hold = Option.lookup("Hold Type","Fines/Costs",nil,nil,true)
      !holds.reject{|h| h.cleared_date? || (h.hold_type_id != sentence_hold && h.hold_type_id != fines_hold)}.reject{|h| !h.hours_total? || h.hours_total <= 0 || h.hours_remaining > 0}.empty?
   end
   
   # checks for any expired 48 hour or 72 hour hold
   def is_expired
      !holds.reject{|h| h.cleared_date? || !h.expired?}.empty?
   end
   
   def last_booking_logs
      booking_logs.last(5).reverse
   end
   
   # this method calculates the total time remaining until all sentences are
   # complete, taking into consideration consecutive and concurrent sentencing
   def total_remaining_hours
      sentence_hold = Option.lookup("Hold Type","Serving Sentence",nil,nil,true)
      fines_hold = Option.lookup("Hold Type","Fines/Costs",nil,nil,true)
      hour_array = []
      holds.reject{|h| h.cleared_date? || h.hold_type.nil? || (h.hold_type_id != sentence_hold && h.hold_type_id != fines_hold) || h.active_blockers?}.each{|h| hour_array.concat(h.find_hours_total(0))}
      return 0 if hour_array.empty?
      return hour_array.sort.last
   end
   
   def total_remaining_days
      hrs = total_remaining_hours
      return 0 if hrs <= 0
      (hrs / 24.0).round
   end
   
   def total_remaining_text
      if total_remaining_hours > 0
         hours_to_sentence(total_remaining_hours, :returns => 'string')
      else
         "0 Hours (All Time Served)"
      end
   end
   
   # this returns a text string listing currently active holds
   def hold_summary
      holds.reject(&:cleared_date?).collect{|h| (h.hold_type.nil? ? '[Invalid]' : '[' + h.hold_type.long_name + ']')}.uniq.to_sentence(:last_word_connector => ' and ')
   end
   
   # returns true if booking has bondable hold (district charges)
   def allow_arraignment_notice?
      holds.reject{|h| h.hold_type.nil? || h.hold_type.long_name != 'Bondable'}.size > 0
   end
   
   # This generates the "Inmate has been released" user message to the specified
   # user.
   def send_release_messages
      receivers = User.notify_release_users
      receivers.each do |r|
         txt = "*This is an Automated Message*</p>" +
         "<p>You are receiving this message because you checked the <b>Inmate Released</b> notification setting in your user preferences.</p>" +
         "<p>Inmate *#{person.nil? ? 'Invalid Person' : '&#91;' + person.id.to_s + '&#93; ' + person.full_name}* has been released.</p>" +
         "<table><tr><th style='width:150px;'>Booking ID:</th><td>#{id}</td></tr>" +
         "<tr><th>Facility:</th><td>#{facility.nil? ? 'Unknown' : facility.long_name}</td></tr>" +
         "<tr><th>Released Date/Time:</th><td>#{format_date(release_date, release_time)}</td></tr>" +
         "<tr><th>Release Reason:</th><td>#{released_because.nil? ? 'Unknown' : released_because.long_name}</td></tr>" +
         "<tr><th>Released By:</th><td>#{release_officer_unit} #{release_officer}</td></tr>" +
         "</table><p>"
         Message.create(:receiver_id => r, :sender_id => updated_by, :subject => "Inmate Released: #{person.nil? ? 'Invalid Person' : person.full_name}", :body => txt)
      end
      return true
   end
   
   private
   
   def check_timers
      if release_date?
         l = booking_logs.last
         unless l.nil? || l.stop_datetime?
            # there appears to be an active time log entry... stop it
            l.update_attributes(:stop_datetime => get_datetime(release_date, release_time), :stop_officer => updater.name, :stop_officer_unit => updater.unit, :stop_officer_badge => updater.badge)
            l.save
         end
         # of course, if there was not an active log entry, don't worry about it.
      else
         # don't create time record if disable timers is set when booking created.
         unless disable_timers?
            l = booking_logs.last
            if l.nil? || l.stop_datetime?
               # last entry doesn't exist or is stopped, create a new one
               self.booking_logs.create(:start_datetime => DateTime.now, :start_officer => updater.name, :start_officer_unit => updater.unit, :start_officer_badge => updater.badge)
            end
         end
      end
   end
   
   def update_person
      if facility_id?
         unless facility.nil?
            unless person.nil?
               unless person.commissary_facility_id == facility_id
                  self.person.update_attribute(:commissary_facility_id, facility_id)
               end
            end
         end
      end
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # validates the person
   def check_person
      if !person.nil? && person.bookings.reject{|b| b.release_date?}.length > 1
         self.errors.add(:person_id, "cannot be used. Already has an open Booking!")
         return false
      end
      return true
   end
   
   # checks to make sure linked records are unlinked prior to delete
   def check_and_destroy_dependencies
      return false unless holds.empty? && transfers.empty?
      self.absents.clear
      return true
   end
   
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_officers
      if booking_officer_id?
         if ofc = Contact.find_by_id(booking_officer_id)
            self.booking_officer = ofc.fullname
            self.booking_officer_badge = ofc.badge_no
            self.booking_officer_unit = ofc.unit
         end
      end
      if release_officer_id?
         if ofc = Contact.find_by_id(release_officer_id)
            self.release_officer = ofc.fullname
            self.release_officer_badge = ofc.badge_no
            self.release_officer_unit = ofc.unit
         end
      end
   end
	
   # ensures that all associated property records are saved when the booking
   # record is updated.
   def save_properties
      properties.each do |p|
         p.save(false)
      end
   end
   
   # accepts a query hash and converts it to an SQL partial suitable in a where
   # clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ["id", "person_id", "facility_id", "cell_id", "signal_code_id", "afis", 'released_because_id']
      # build an sql WHERE string
      condition = ""
      query.each do |key, value|
         if key == "id"
            unless condition.empty?
               condition << " and "
            end
            condition << "bookings.id = '#{value}'"
         elsif key == 'name'
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.firstname like '#{nameparts[:first]}'"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.middlename like '#{nameparts[:middle]}'"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.lastname like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.suffix like '#{nameparts[:suffix]}'"
            end
         elsif key == 'doc'
            unless condition.empty?
               condition << " and "
            end
            condition << "holds.doc is true"
         elsif key == 'doc_billable'
            unless condition.empty?
               condition << " and "
            end
            condition << "holds.doc_billable is true"
         elsif key == "booking_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bookings.booking_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "booking_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bookings.booking_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "sched_release_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bookings.sched_release_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "sched_release_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bookings.sched_release_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "release_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bookings.release_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "release_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "bookings.release_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else
            unless condition.empty?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "bookings.#{key} = '#{value}'"
            else
               condition << "bookings.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
   
   # this is a helper that calls the valid_time() method with each time
   # field for a booking to insert any missing colons (:) into the time values
   def fix_times
      self.booking_time = valid_time(booking_time)
      self.release_time = valid_time(release_time)
   end
end
