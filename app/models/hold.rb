# == Schema Information
#
# Table name: holds
#
#  id                     :integer(4)      not null, primary key
#  booking_id             :integer(4)
#  hold_date              :date
#  hold_time              :string(255)
#  cleared_date           :date
#  cleared_time           :string(255)
#  hold_type_id           :integer(4)
#  hold_by                :string(255)
#  hold_by_unit           :string(255)
#  cleared_by             :string(255)
#  cleared_by_unit        :string(255)
#  remarks                :text
#  created_by             :integer(4)
#  updated_by             :integer(4)
#  created_at             :datetime
#  updated_at             :datetime
#  cleared_because_id     :integer(4)
#  arrest_id              :integer(4)
#  hold_by_id             :integer(4)
#  hold_by_badge          :string(255)
#  cleared_by_id          :integer(4)
#  cleared_by_badge       :string(255)
#  doc                    :boolean(1)      default(FALSE)
#  probation_id           :integer(4)
#  billing_report_date    :date
#  agency                 :string(255)
#  agency_id              :integer(4)
#  transfer_officer       :string(255)
#  transfer_officer_id    :integer(4)
#  transfer_officer_badge :string(255)
#  transfer_officer_unit  :string(255)
#  fines                  :decimal(12, 2)  default(0.0)
#  datetime_counted       :datetime
#  doc_billable           :boolean(1)      default(FALSE)
#  hours_served           :integer(4)      default(0)
#  hours_goodtime         :float           default(0.0)
#  sentence_id            :integer(4)
#  transfer_id            :integer(4)
#  explanation            :string(255)
#  bill_from_date         :date
#  legacy                 :boolean(1)      default(FALSE)
#  revocation_id          :integer(4)
#  billed_retroactive     :date
#  hours_total            :integer(4)      default(0)
#  hours_time_served      :integer(4)      default(0)
#  temp_release_reason_id :integer(4)
#  deny_goodtime          :boolean(1)      default(FALSE)
#  other_billable         :boolean(1)      default(FALSE)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Hold Model
# Holds are reasons that prevent prisoners from being released from booking.
class Hold < ActiveRecord::Base
   include ActionView::Helpers::NumberHelper
   
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :hold_type, :class_name => 'Option', :foreign_key => 'hold_type_id'
   belongs_to :cleared_because, :class_name => 'Option', :foreign_key => 'cleared_because_id'
   belongs_to :booking
   belongs_to :arrest
   belongs_to :probation
   has_many :bonds
   has_and_belongs_to_many :blockers, :class_name => 'Hold', :join_table => 'hold_blocks', :foreign_key => 'blocker_id', :association_foreign_key => 'hold_id'
   has_and_belongs_to_many :blocks, :class_name => 'Hold', :join_table => 'hold_blocks', :foreign_key => 'hold_id', :association_foreign_key => 'blocker_id'
   belongs_to :transfer
   belongs_to :sentence
   belongs_to :revocation
   has_one :doc_record, :class_name => 'Doc'
   belongs_to :temp_release_reason, :class_name => "Option", :foreign_key => 'temp_release_reason_id'
   
   before_validation :fix_times, :lookup_contacts, :check_for_transfer, :check_for_other_agency, :check_for_fines
   before_destroy :clear_bonds
   before_save :fix_numbers
   after_save :check_creator
   
   validates_presence_of :hold_by
   validates_option :cleared_because_id, :option_type => 'Release Type', :allow_nil => true
   validates_presence_of :cleared_because_id, :explanation, :if => :cleared_date?
   validates_presence_of :cleared_date, :if => :cleared_because_id?
   validates_numericality_of :fines
   validates_option :hold_type_id, :option_type => 'Hold Type'
   validates_date :hold_date
   validates_time :hold_time
   validates_date :cleared_date, :bill_from_date, :allow_nil => true
   validates_time :cleared_time, :allow_nil => true
   validates_presence_of :cleared_time, :if => :cleared_date?
   validates_presence_of :cleared_date, :if => :cleared_time?
   validates_numericality_of :hours_served, :hours_total, :hours_time_served, :only_integer => true, :greater_than_or_equal_to => 0
   validates_numericality_of :hours_goodtime, :greater_than_or_equal_to => 0
   
   # a short description of the model for the DatabaseController index view.
   def self.desc
      'Booking Hold Records'
   end
   
   # returns true if the specified option id is used by any of the option
   # type fields in any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(hold_type_id = '#{oid}' OR cleared_because_id = '#{oid}' OR temp_release_reason_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # Adds a hold of the specified type to the specified booking record for the
   # specified arrest with all fields preset as appropriate for the type of
   # hold.
   def self.add(book,arr,type,agency=nil)
      retval = nil
      if ot = OptionType.find_by_name("Hold Type")
         if o = Option.find(:first, :conditions => {:long_name => type, :option_type_id => ot.id})
            # we do nothing unless there is a valid arrest specified
            if arrest = Arrest.find_by_id(arr)
               temp = new(
                  :hold_type_id => o.id,
                  :booking_id => book,
                  :hold_date => arrest.arrest_date,
                  :hold_time => arrest.arrest_time,
                  :hold_by => arrest.officer1,
                  :hold_by_unit => arrest.officer1_unit,
                  :hold_by_badge => arrest.officer1_badge,
                  :created_by => arrest.updated_by,
                  :updated_by => arrest.updated_by,
                  :arrest_id => arr
               )
               retval = temp
               unless agency.nil?
                  if ag = Contact.find_by_id(agency)
                     temp.agency_id = ag.id
                     temp.agency = ag.fullname
                  end
               end
               if type == "Bondable"
                  temp.fines = arrest.bond_amt
                  if arrest.hour_72_date?
                     temp.hold_date = arrest.hour_72_date
                     temp.hold_time = arrest.hour_72_time
                  end
                  # need to attach any active bond for the arrest to
                  # this hold
                  active_status = Option.lookup("Bond Status","Active",nil,nil,true)
                  arrest.bonds.each{|b| temp.bonds << b}
               elsif type == "No Bond"
                  if arrest.hour_72_date?
                     temp.hold_date = arrest.hour_72_date
                     temp.hold_time = arrest.hour_72_time
                  end
               end
               temp.save(false)
            end
         end
      end
      return retval
   end
   
   # returns base perms required to view holds
   def self.base_perms
      Perm[:view_booking]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["hold_by_id", "cleared_by_id", "agency_id", "transfer_officer_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # This clears all hold cleared information from the hold and flags it as
   # updated  by the specified user.
   def clear_release(user_id)
      return unless cleared_date?
      self.update_attributes(
         :cleared_date => nil,
         :cleared_time => nil,
         :cleared_by => nil,
         :cleared_by_unit => nil,
         :cleared_by_badge => nil,
         :updated_by => user_id,
         :cleared_because_id => nil,
         :explanation => nil
      )
      unless hold_type == 'Temporary Transfer'
         self.update_attributes(
            :agency => nil,
            :agency_id => nil,
            :transfer_officer => nil,
            :transfer_officer_id => nil,
            :transfer_officer_badge => nil,
            :transfer_officer_unit => nil
         )
      end
   end
   
   # calculates the number of hours remaining for sentence holds.  Returns nil
   # if hold is not a sentence
   def hours_remaining
      sentence_hold = Option.lookup("Hold Type","Serving Sentence",nil,nil,true)
      fines_hold = Option.lookup("Hold Type","Fines/Costs",nil,nil,true)
      return nil if hold_type.nil? || (hold_type_id != sentence_hold && hold_type_id != fines_hold)
      return 0 if cleared_date? || hours_total.nil? || hours_total <= 0
      return (hours_total - (hours_served? && hours_served > 0 ? hours_served : 0) - (hours_goodtime? && hours_goodtime > 0 ? hours_goodtime.round : 0) - (hours_time_served? && hours_time_served > 0 ? hours_time_served : 0))
   end
   
   def days_remaining
      hrs = hours_remaining
      return 0 if hrs <= 0
      (hrs / 24.0).round
   end
   
   # this updates the hours_served and hours_goodtime counters. It is called from
   # booking by a rake task once an hour
   def update_hour_counters
      return if booking.nil?
      return if booking.booking_logs.empty?
      runtime = DateTime.now
      unless datetime_counted?
         # just to handle existing records for the first run with the new
         # datetime_counted field.
         self.update_attribute(:datetime_counted, runtime - 1.hour)
      end
      minutes_since = booking.minutes_served_since(datetime_counted)
      if minutes_since < 30
         self.update_attribute(:datetime_counted, runtime)
         return
      end
      # at least 30 minutes have passed since last count
      # update the counters and the last count time
      hours_since = (minutes_since / 60.0).round
      # increment hours served by count_up_value
      if hours_served?
         self.hours_served += hours_since
      else
         self.hours_served = hours_since
      end
      # if the booking allows good time, increment hours_goodtime by count_up_value
      # except for fines/costs which don't get good time
      fines_hold = Option.lookup("Hold Type","Fines/Costs",nil,nil,true)
      if hold_type_id != fines_hold && !deny_goodtime? && booking.good_time? && !doc?
         unless SiteConfig.goodtime_hours_per_day_local.blank? || SiteConfig.goodtime_hours_per_day_local == 0
            multiplier = SiteConfig.goodtime_hours_per_day_local / 24.0
            if hours_goodtime?
               self.hours_goodtime += (hours_since.to_f * multiplier)
            else
               self.hours_goodtime = (hours_since.to_f * multiplier)
            end
         end
      end
      self.updated_by = 1
      self.datetime_counted = runtime
      self.save(false)
   end
   
   def hours_served_between(start_time, stop_time)
      # calculate the total time logged between start_time and stop time while
      # this hold was active
      return 0 unless (start_time.is_a?(Time) || start_time.is_a?(DateTime)) && (stop_time.is_a?(Time) || stop_time.is_a?(DateTime))
      return 0 if booking_logs.empty?
      hrs = 0
      BookingLog.find(:all, :conditions => ['(start_datetime >= ? AND start_datetime <= ?) OR (start_datetime < ? AND (stop_datetime IS NULL OR (stop_datetime >= ? AND stop_datetime <= ?))',start_time,stop_time,start_time,start_time,stop_time]).each do |l|
         hrs += l.hours_served(start_time,stop_time)
      end
      return hrs
   end
   
   def active_blockers?
      blockers.each{|h| return true unless h.cleared_date?}
      return false
   end
   
   # this returns an array of all possible hour totals considering every path
   # through the blocks association
   def find_hours_total(total)
      return 0 unless total.is_a?(Integer)
      return [ total + hours_remaining ] if blocks.empty?
      retarray = []
      blocks.each do |b|
         retarray.push(b.find_hours_total(total + hours_remaining))
      end
      return retarray.flatten
   end
   
   # this returns the label name for the reason (see below)
   def hold_reason_name
      case
      when hold_type.nil?
         return ""
      when hold_type.long_name == 'Fines/Costs'
         return "#{agency}:"
      when !sentence.nil? || !doc_record.nil? || !revocation.nil?
         return "Conviction:"
      when ['Temporary Housing', 'Hold for City', 'Hold for Other Agency', 'Temporary Transfer'].include?(hold_type.long_name)
         return "Agency:"
      when !arrest.nil?
         return "Charged:"
      when hold_type.long_name == 'Temporary Release'
         return "Reason:"
      else
         return ""
      end
   end
   
   
   # this returns the reason for the hold (charges, sentences, agencies, etc)
   def hold_reason
      case
      when hold_type.nil?
         return "Unknown"
      when hold_type.long_name == 'Fines/Costs'
         "Must pay #{number_to_currency(fines)} or serve #{hours_total} hours (approx. #{((hours_total? && hours_total > 0 ? hours_total : 0) / 24).round} days) in jail"
      when !sentence.nil?
         sentence.conviction + "(#{sentence.conviction_count})"
      when ['Temporary Housing', 'Hold for City', 'Hold for Other Agency', 'Temporary Transfer'].include?(hold_type.long_name)
         self.agency
      when !arrest.nil?
         arrest.district_charge_summary
      when !doc_record.nil?
         doc_record.conviction? ? doc_record.conviction : "Not On File"
      when hold_type.long_name == 'Temporary Release'
         temp_release_reason.nil? ? 'Unknown' : temp_release_reason.long_name
      when !revocation.nil?
         "Probation Revoked: #{revocation.docket.sentence_summary.blank? ? 'Convictions Not On File' : revocation.docket.sentence_summary}"
      else
         ""
      end
   end
   
   # returns true if this is a 48 hour or 72 hour hold and it has expired
   def expired?
      fortyeight = Option.lookup("Hold Type","48 Hour",nil,nil,true)
      seventytwo = Option.lookup("Hold Type","72 Hour",nil,nil,true)
      return false unless hold_type_id == fortyeight || hold_type_id == seventytwo
      hold_datetime = get_datetime(hold_date, hold_time)
      return true if hold_type_id == fortyeight && hours_diff(DateTime.now, hold_datetime) > 48
      return true if hold_type_id == seventytwo && hours_diff(DateTime.now, hold_datetime) > 72
      return false
   end
   
   private
   
   # makes sure all hours fields are not nil and not negative
   # for some reason, the database defaults are not working reliably
   def fix_numbers
      ['served','goodtime','total','time_served'].each do |t|
         if send("hours_#{t}").blank? || send("hours_#{t}") < 0
            self.send("hours_#{t}=",0)
         end
      end
      return true
   end
   
   # checks that agency is entered if hold type is hold for other agency
   def check_for_other_agency
      unless hold_type.nil?
         if hold_type.long_name == 'Hold for Other Agency'
            unless agency?
               self.errors.add(:agency, "must be entered if hold type is 'Hold for Other Agency'")
               return false
            end
         end
      end
      return true
   end
   
   def check_for_fines
      unless hold_type.nil?
         if hold_type.long_name == 'Fines/Costs'
            unless fines? && fines > BigDecimal.new("0.0",2)
               self.errors.add(:fines, 'must be a number greater than zero!')
               return false
            end
            unless agency?
               self.errors.add(:agency, "must be entered for 'Fines/Costs' holds'")
               return false
            end
            unless hours_total? && hours_total > 0
               self.errors.add(:hours_total, "must be a number greater than zero.")
               return false
            end
         end
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # checks to see that agency and transfer officer are entered when clearing
   # type is Transferred or hold type is Temporary Transfer.
   def check_for_transfer
      status = true
      unless hold_type.nil?
         if hold_type.long_name == 'Temporary Transfer' || hold_type.long_name == 'Temporary Housing'
            unless agency?
               self.errors.add(:agency, "cannot be blank for Temporary Transfers!")
               status = false
            end
            unless transfer_officer?
               self.errors.add(:transfer_officer, "cannot be blank for Temporary Transfers!")
               status = false
            end
         end
         # make sure other_billable is false unless temp transfer
         # should not generate an error, just force the value
         unless hold_type.long_name == 'Temporary Transfer'
            self.other_billable = false
         end
      end
      unless cleared_because.nil?
         if cleared_because.long_name == 'Transferred'
            unless agency?
               self.errors.add(:agency, "cannot be blank for Transfers!")
               status = false
            end
            unless transfer_officer?
               self.errors.add(:transfer_officer, "cannot be blank for Transfers!")
               status = false
            end
         end
      end
      return status
   end
   
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_contacts
      if hold_by_id?
         if ofc = Contact.find_by_id(hold_by_id)
            self.hold_by = ofc.fullname
            self.hold_by_badge = ofc.badge_no
            self.hold_by_unit = ofc.unit
         end
      end
      if cleared_by_id?
         if ofc = Contact.find_by_id(cleared_by_id)
            self.cleared_by = ofc.fullname
            self.cleared_by_badge = ofc.badge_no
            self.cleared_by_unit = ofc.unit
         end
      end
      if transfer_officer_id?
         if ofc = Contact.find_by_id(transfer_officer_id)
            self.transfer_officer = ofc.fullname
            self.transfer_officer_badge = ofc.badge_no
            self.transfer_officer_unit = ofc.unit
         end
      end
      if agency_id?
         if agc = Contact.find_by_id(agency_id)
            self.agency = agc.fullname
         end
      end
   end
      
   # clears all bond associations for the hold.
   def clear_bonds
      unless bonds.empty?
         bonds.each{|b| b.update_attribute(:hold_id, nil)}
      end
      return true
   end
   
   # accepts a search query hash and converts it into an SQL partial
   # suitable for use in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ['booking_id', 'arrest_id', 'hold_type_id', 'cleared_because_id', 'doc', 'doc_billable']
      condition = ""
      query.each do |key, value|
         if key == "hold_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "holds.hold_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "hold_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "holds.hold_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "cleared_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "holds.cleared_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "cleared_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "holds.cleared_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         else
            unless condition.blank?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "holds.#{key} = '#{value}'"
            else
               condition << "holds.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
   
   # calls valid_time() with each of the individual time type fields.
   # (valid_time inserts a : into the time field if it doesn't already have
   # one.
   def fix_times
      self.hold_time = valid_time(hold_time)
      self.cleared_time = valid_time(cleared_time)
   end
end
