# == Schema Information
#
# Table name: colorschemes
#
#  id                :integer(4)      not null, primary key
#  name              :string(255)
#  description       :string(255)
#  color_dark        :string(255)     default("#3665A3")
#  color_light       :string(255)     default("#D4E3F7")
#  color_alt_dark    :string(255)     default("#886655")
#  color_alt_light   :string(255)     default("#F0ECDB")
#  color_black       :string(255)     default("#000000")
#  color_white       :string(255)     default("#FFFFFF")
#  color_gray_dark   :string(255)     default("#333333")
#  color_gray_medium :string(255)     default("#777777")
#  color_gray_light  :string(255)     default("#CCCCCC")
#  color_message     :string(255)     default("#0000AA")
#  color_message_bg  :string(255)     default("#EEEEFF")
#  color_notice      :string(255)     default("#00AA00")
#  color_notice_bg   :string(255)     default("#EEFFEE")
#  color_error       :string(255)     default("#AA0000")
#  color_error_bg    :string(255)     default("#FFEEEE")
#  created_at        :datetime
#  updated_at        :datetime
#  color_gray_bg     :string(255)     default("#efefef")
#  color_tip_bg      :string(255)     default("#FFFFBB")
#  color_tip_border  :string(255)     default("#886655")
#  color_tip         :string(255)     default("#000000")
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Colorscheme Model
# These are color schemes for the application
class Colorscheme < ActiveRecord::Base
   
   before_destroy :check_destroyable
   
   # a short description for the DatabaseController index view.
   def self.desc
      "Colorschemes for site configuration"
   end
   
   # This loads the color scheme into memory. There is currently only one
   # available, but future updates may increase this to allow a selection.
   def self.load_config(scheme_id=nil)
      unless scheme_id.to_i > 0
         scheme_id = 1
      end
      if cscheme = find_by_id(scheme_id)
         return cscheme
      else
         find(1)
      end
   end
   
   def self.names_for_select
      find(:all).collect{|c| [c.name,c.id]}
   end
   
   def self.id_to_name(scheme_id)
      if cscheme = find_by_id(scheme_id.to_i)
         return cscheme.name
      else
         return "Unknown"
      end
   end
   
   private
   
   # disallow destroying default colorscheme
   def check_destroyable
      return false if name == "Default"
      return true
   end
end
