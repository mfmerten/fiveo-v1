# == Schema Information
#
# Table name: transports
#
#  id              :integer(4)      not null, primary key
#  person_id       :integer(4)
#  officer1        :string(255)
#  officer1_unit   :string(255)
#  officer2        :string(255)
#  officer2_unit   :string(255)
#  begin_date      :date
#  begin_time      :string(255)
#  begin_miles     :integer(4)
#  end_date        :date
#  end_time        :string(255)
#  end_miles       :integer(4)
#  estimated_miles :integer(4)
#  from            :string(255)
#  from_street     :string(255)
#  from_city       :string(255)
#  from_state_id   :integer(4)
#  from_zip        :string(255)
#  to              :string(255)
#  to_street       :string(255)
#  to_city         :string(255)
#  to_state_id     :integer(4)
#  to_zip          :string(255)
#  remarks         :text
#  created_by      :integer(4)
#  updated_by      :integer(4)
#  created_at      :datetime
#  updated_at      :datetime
#  officer1_id     :integer(4)
#  officer1_badge  :string(255)
#  officer2_id     :integer(4)
#  officer2_badge  :string(255)
#  from_id         :integer(4)
#  to_id           :integer(4)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Transport Model
# inmate transportation vouchers
class Transport < ActiveRecord::Base
   belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
   belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
   belongs_to :from_state, :class_name => 'Option', :foreign_key => 'from_state_id'
   belongs_to :to_state, :class_name => 'Option', :foreign_key => 'to_state_id'
   belongs_to :person
   
   before_validation :fix_times, :lookup_contacts
   after_save :check_creator
   
   validates_presence_of :officer1, :from, :to
   validates_numericality_of :begin_miles, :end_miles, :only_integer => true, :allow_nil => true
   validates_date :begin_date, :end_date, :allow_nil => true
   validates_time :begin_time, :end_time, :allow_nil => true
   validates_presence_of :begin_time, :if => :begin_date?
   validates_presence_of :begin_date, :if => :begin_time?
   validates_presence_of :end_time, :if => :end_date?
   validates_presence_of :end_date, :if => :end_time?
   validates_zip :from_zip, :to_zip, :allow_nil => true, :allow_blank => true
   validates_person :person_id
   validates_option :from_state_id, :to_state_id, :option_type => 'State', :allow_nil => true
   
   
   # a short description for the DatabaseController index view.
   def self.desc
      "Prisoner Transportation Database"
   end
   
   # returns true if the specified option id is used by any of the option
   # type fields in any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(from_state_id = '#{oid}' OR to_state_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   # returns base permissions required to view transportation vouchers
   def self.base_perms
      Perm[:view_transport]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["officer1_id", "officer2_id", "from_id", "to_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   # returns the difference in mileage between start and stop. If stop is
   # smaller than start, assumes an odometer rollover.
   def mileage
      return 0 unless begin_miles? && end_miles?
      stopm = end_miles
      if begin_miles > stopm
         # assume odometer roll-over ??
         digits = begin_miles.to_s.length
         roll = "1".ljust(digits + 1, "0")
         stopm + roll.to_i - begin_miles
      else
         end_miles - begin_miles
      end
   end
   
   # returns the time difference between start and stop date/times. value 
   # returned as a string indicating days, hours and minutes of duration.
   def duration
      return "0" unless begin_date? && begin_time? && end_date? && end_time?
      duration_in_words(get_datetime(end_date, end_time), get_datetime(begin_date, begin_time))
   end
   
   # returns the From: address line 1 (street)
   def from_address_line1
      return '' unless from_street?
      from_street
   end
   
   # returns the From: address line 2 (city, state zip)
   def from_address_line2
      name = ""
      if from_city
         name << from_city
      end
      if from_state_id?
         unless name.empty?
            name << ", "
         end
         name << from_state.short_name
      end
      if from_zip?
         unless name.empty?
            name << " "
         end
         name << from_zip
      end
      return name
   end
   
   # returns the To: address line 1 (street)
   def to_address_line1
      return '' unless to_street?
      to_street.titleize
   end
   
   # returns the To: address line 2 (city, state zip)
   def to_address_line2
      name = ""
      if to_city
         name << to_city.titleize
      end
      if to_state_id?
         unless name.empty?
            name << ", "
         end
         name << to_state.short_name
      end
      if to_zip?
         unless name.empty?
            name << " "
         end
         name << to_zip
      end
      return name
   end
         
   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # looks up any contact selected and inserts the name, unit and badge into
   # the appropriate fields prior to validation.
   def lookup_contacts
      if officer1_id?
         if ofc = Contact.find_by_id(officer1_id)
            self.officer1 = ofc.fullname
            self.officer1_badge = ofc.badge_no
            self.officer1_unit = ofc.unit
         end
      end
      if officer2_id?
         if ofc = Contact.find_by_id(officer2_id)
            self.officer2 = ofc.fullname
            self.officer2_badge = ofc.badge_no
            self.officer2_unit = ofc.unit
         end
      end
      if from_id?
         if agc = Contact.find_by_id(from_id)
            self.from = agc.fullname
            self.from_street = agc.street
            self.from_city = agc.city
            self.from_state_id = agc.state_id
            self.from_zip = agc.zip
         end
      end
      if to_id?
         if agc = Contact.find_by_id(to_id)
            self.to = agc.fullname
            self.to_street = agc.street
            self.to_city = agc.city
            self.to_state_id = agc.state_id
            self.to_zip = agc.zip
         end
      end
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      numtypes = ["person_id"]
      condition = ""
      query.each do |key, value|
         if key == "officer"
            condition << "(transports.officer1 like '%#{value}%' or transports.officer2 like '%#{value}%')"
         elsif key == "officer_unit"
            unless condition.empty?
               condition << " and "
            end
            condition << "(transports.officer1_unit like '%#{value}%' or transports.officer2_unit like '%#{value}%')"
         elsif key == "officer_badge"
            unless condition.empty?
               condition << " and "
            end
            condition << "(transports.officer1_badge like '%#{value}%' or transports.officer2_badge like '%#{value}%')"
         elsif key == 'full_name'
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "(people.firstname like '#{nameparts[:first]}' OR people.middlename like '#{nameparts[:first]}')"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "(people.middlename like '#{nameparts[:middle]}' OR people.firstname like '#{nameparts[:middle]}')"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.lastname like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "people.suffix like '#{nameparts[:suffix]}'"
            end
         elsif key == "begin_date_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "transports.begin_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "begin_date_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "transports.begin_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == 'agency'
            unless condition.empty?
               condition << " and "
            end
            condition << "(transports.from like '%#{value}%' or transports.to like '%#{value}%')"
         elsif key == "city"
            unless condition.empty?
               condition << " and "
            end
            condition << "(transports.from_city like '%#{value}%' or transports.to_city  like '%#{value}%')"
         elsif key == "state_id"
            unless condition.empty?
               condition << " and "
            end
            condition << "(transports.from_state_id = '#{value}' or transports.to_state_id = '#{value}')"
         elsif key == "zip"
            unless condition.empty?
               condition << " and "
            end
            condition << "(transports.from_zip like '%#{value}%' or transports.to_zip like '%#{value}%')"
         else
            unless condition.empty?
               condition << " and "
            end
            if numtypes.include?(key)
               condition << "transports.#{key} = '#{value}'"
            else
               condition << "transports.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end
   
   # calls valid_time() with each of the time type fields.  (valid_time()
   # inserts a colon between hours and minutes if one is missing)
   def fix_times
      self.begin_time = valid_time(begin_time)
      self.end_time = valid_time(end_time)
   end
end
