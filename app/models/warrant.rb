# == Schema Information
#
# Table name: warrants
#
#  id                 :integer(4)      not null, primary key
#  person_id          :integer(4)
#  warrant_no         :string(255)
#  received_date      :date
#  issued_date        :date
#  disposition_id     :integer(4)
#  dispo_date         :date
#  remarks            :text
#  created_at         :datetime
#  updated_at         :datetime
#  bond_amt           :decimal(12, 2)  default(0.0)
#  payable            :decimal(12, 2)  default(0.0)
#  created_by         :integer(4)      default(1)
#  updated_by         :integer(4)      default(1)
#  jurisdiction_id    :integer(4)
#  ssn                :string(255)
#  dob                :date
#  street             :string(255)
#  oln                :string(255)
#  oln_state_id       :integer(4)
#  firstname          :string(255)
#  lastname           :string(255)
#  suffix             :string(255)
#  race_id            :integer(4)
#  sex                :integer(4)
#  special_type       :integer(4)      default(0)
#  city               :string(255)
#  state_id           :integer(4)
#  zip                :string(255)
#  investigation_id   :integer(4)
#  private            :boolean(1)      default(FALSE)
#  invest_crime_id    :integer(4)
#  owned_by           :integer(4)
#  invest_criminal_id :integer(4)
#  legacy             :boolean(1)      default(FALSE)
#  middle1            :string(255)
#  middle2            :string(255)
#  middle3            :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Warrant Model
# wants and warrants
class Warrant < ActiveRecord::Base
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   belongs_to :person
   belongs_to :disposition, :class_name => "Option", :foreign_key => 'disposition_id'
   belongs_to :oln_state, :class_name => 'Option', :foreign_key => 'oln_state_id'
   belongs_to :race, :class_name => 'Option', :foreign_key => 'race_id'
   belongs_to :state, :class_name => 'Option', :foreign_key => 'state_id'
   belongs_to :jurisdiction, :class_name => "Contact", :foreign_key => 'jurisdiction_id'
   belongs_to :investigation
   belongs_to :crime, :class_name => 'InvestCrime', :foreign_key => 'invest_crime_id'
   belongs_to :criminal, :class_name => 'InvestCriminal', :foreign_key => 'invest_criminal_id'
   belongs_to :owner, :class_name => 'User', :foreign_key => 'owned_by'
   has_many :charges, :dependent => :destroy
   has_many :arrest_charges, :class_name => 'Charge', :foreign_key => 'arrest_warrant_id'
   has_and_belongs_to_many :district_arrests, :class_name => 'Arrest', :join_table => 'district_warrants'
   has_and_belongs_to_many :city_arrests, :class_name => 'Arrest', :join_table => 'city_warrants'
   has_and_belongs_to_many :other_arrests, :class_name => 'Arrest', :join_table => 'other_warrants'
   has_and_belongs_to_many :exemptions, :class_name => 'Person', :join_table => 'warrant_exemptions'

   # used to allow entering full name on form but saving it as individual name
   # parts in the record.
   attr_accessor :name

   # this sets up sphinx indices for this model
   define_index do
      indexes [lastname, firstname, middle1, middle2, middle3, suffix], :as => :name
      indexes lastname
      indexes firstname
      indexes middle1
      indexes middle2
      indexes middle3
      indexes ssn
      indexes oln
      indexes dob
      indexes street
      indexes city
      indexes zip
      indexes warrant_no
      indexes jurisdiction.fullname, :as => :agency
      indexes charges.charge, :as => :chgs
      indexes remarks
      
      has updated_at
   end

   before_validation :split_name, :check_disposition
   after_save :check_and_generate_warrant_no, :check_creator
   after_update :save_charges
   before_destroy :check_dependencies

   validates_presence_of :lastname, :firstname, :jurisdiction_id
   validates_date :received_date, :issued_date
   validates_date :dispo_date, :if => :disposition_id?
   validates_numericality_of :payable, :bond_amt, :allow_nil => true
   validates_numericality_of :sex, :only_integer => true, :greater_than => 0, :less_than => 3, :allow_nil => true
   validates_person :person_id, :allow_nil => true
   validates_option :disposition_id, :option_type => 'Warrant Disposition', :allow_nil => true
   validates_option :state_id, :oln_state_id, :option_type => 'State', :allow_nil => true
   validates_option :race_id, :option_type => 'Race', :allow_nil => true
   validates_contact :jurisdiction_id

   # a short description used by the DatabasesController index view.
   def self.desc
      "Wants and Warrants"
   end

   # returns true if the specified option id is used as disposition id in 
   # any record.
   def self.uses_option(oid)
      oid ||= 0
      oid = oid.to_i
      cond = %(disposition_id = '#{oid}' OR oln_state_id = '#{oid}' OR state_id = '#{oid}')
      count(:conditions => cond) > 0
   end
   
   # returns true if person is in use (in any capacity)
   def self.uses_person(p_id)
      p_id ||= 0
      p_id = p_id.to_i
      cond = %(person_id = '#{p_id}')
      count(:conditions => cond) > 0
   end
   
   # returns base permissions required to View warrants
   def self.base_perms
      Perm[:view_warrant]
   end
   
   def self.swap_contact(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["jurisdiction_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      return rec_count
   end
   
   def self.swap_person(bad_id, good_id, user_id)
      return -1 unless bad_id.to_i > 0 && good_id.to_i > 0 && user_id.to_i > 0
      rec_count = 0
      ["person_id"].each do |field|
         rec_count += update_all("#{field} = #{good_id.to_i}, updated_by = #{user_id.to_i}", "#{field} = #{bad_id.to_i}")
      end
      # catch the HABTM tables for people
      # TODO: find a better way to do this
      rec_count += count(:all, :include => :exemptions, :conditions => "warrant_exemptions.person_id = #{bad_id.to_i}")
      self.connection.execute "UPDATE warrant_exemptions SET person_id = #{good_id.to_i} WHERE person_id = #{bad_id.to_i}"
      return rec_count
   end
   
   # returns all warrants for the given person identified by id
   def self.warrants_for(person_id, include_linked = true)
      # interested in non-linked stuff, so can't search directly by person_id
      return [] unless p = Person.find_by_id(person_id)
      conditions = ""
      # consider first and middle names together
      allnames = [p.firstname]
      allnames.concat(p.middlename? ? p.middlename.split : [])
      # get initials from all first and middle names
      allinits = allnames.collect{|n| n.slice(0,1)}
      # build a "set" string for SQL
      allnamestring = allnames.collect{|n| "'#{n}'"}.join(',')
      allinitstring = allinits.collect{|i| "'#{i}'"}.join(',')
      # do the same thing for any part of the name that was only an initial.
      initstring = allnames.reject{|n| n.size > 1}.collect{|n| "'#{n}'"}.join(',')
      if p.lastname?
         # check last name
         conditions << "(lastname = '' OR lastname IS NULL OR lastname LIKE '#{p.lastname}')"
      end
      unless allnamestring.blank?
         unless conditions.blank?
            conditions << " AND "
         end
         # rest of names could be nil, an actual name or an initial and could be in any position
         # first name
         conditions << "(firstname = '' OR firstname IS NULL OR (LENGTH(firstname) > 1 AND (firstname IN (#{allnamestring})#{initstring.blank? ? '' : ' OR LEFT(firstname,1) IN (' + initstring + ')'})) OR (LENGTH(firstname) = 1 AND firstname IN (#{allinitstring})))"
         conditions << " AND "
         # middle name 1
         conditions << "(middle1 = '' OR middle1 IS NULL OR (LENGTH(middle1) > 1 AND (middle1 IN (#{allnamestring})#{initstring.blank? ? '' : ' OR LEFT(middle1,1) IN (' + initstring + ')'})) OR (LENGTH(middle1) = 1 AND middle1 IN (#{allinitstring})))"
         conditions << " AND "
         # middle name 2
         conditions << "(middle2 = '' OR middle2 IS NULL OR (LENGTH(middle2) > 1 AND (middle2 IN (#{allnamestring})#{initstring.blank? ? '' : ' OR LEFT(middle2,1) IN (' + initstring + ')'})) OR (LENGTH(middle2) = 1 AND middle2 IN (#{allinitstring})))"
         conditions << " AND "
         # middle name 3
         conditions << "(middle3 = '' OR middle3 IS NULL OR (LENGTH(middle3) > 1 AND (middle3 IN (#{allnamestring})#{initstring.blank? ? '' : ' OR LEFT(middle3,1) IN (' + initstring + ')'})) OR (LENGTH(middle3) = 1 AND middle3 IN (#{allinitstring})))"
      end
      if p.suffix?
         unless conditions.blank?
            conditions << " AND "
         end
         conditions << "(suffix LIKE '#{p.suffix}' OR suffix = '')"
      end
      if p.ssn?
         unless conditions.blank?
            conditions << " AND "
         end
         conditions << "(ssn LIKE '#{p.ssn}' OR ssn = '')"
      end
      if p.oln?
         modoln = p.oln.clone.sub(/^0*/,'')
         unless conditions.blank?
            conditions << " AND "
         end
         conditions << "(oln LIKE '%#{Warrant.quoted(modoln)}%' OR oln = '')"
      end
      if p.oln_state_id?
         unless conditions.blank?
            conditions << " AND "
         end
         conditions << "(oln_state_id = #{p.oln_state_id} OR oln_state_id IS NULL)"
      end
      if p.date_of_birth?
         unless conditions.blank?
            conditions << " AND "
         end
         conditions << "(dob = '#{p.date_of_birth}' OR dob IS NULL)"
      end
      unless conditions.blank?
         conditions << " AND "
      end
      conditions << "dispo_date IS NULL"
      if include_linked
         conditions << " AND (person_id = #{p.id} OR person_id IS NULL)"
      else
         conditions << " AND person_id IS NULL"
      end
      # add in the privacy thing
      # no private warrants will ever match a person
      conditions << " AND private != 1"
      # do the search
      conditions = sanitize_sql_for_conditions(conditions)
      find(:all, :conditions => conditions)
   end
   
   # returns the number of items to be reviewed by supplied user_id
   def self.review_count(user_id)
      return 0 unless user = User.find_by_id(user_id)
      if user.last_warrant.nil? || !user.last_warrant.is_a?(Integer)
         user.update_attribute(:last_warrant, 0)
      end
      count(:conditions => ['private != 1 AND id > ?',user.last_warrant])
   end

   # accepts an array of charge attributes from the edit view and creates
   # charge records.
   def new_charge_attributes=(charge_attributes)
      charge_attributes.each do |attributes|
         unless attributes[:charge].blank?
            charges.build(attributes)
         end
      end
   end

   # accepts an array of charge attributes from the edit view and updates
   # existing charges and removes those in the array.
   def existing_charge_attributes=(charge_attributes)
      charges.reject(&:new_record?).each do |charge|
         attributes = charge_attributes[charge.id.to_s]
         if attributes
            attributes[:updated_by] = updated_by
            charge.attributes = attributes
         else
            charges.delete(charge)
         end
      end
   end

   # This returns a single text string listing all charges and their counts
   # produced by cycling through all associated charge records.
   def charge_summary
      text = ''
      charges.each do |c|
         unless text.blank?
            text << ", "
         end
         text << "#{c.charge}(#{c.count})"
      end
      if text.blank?
         return "No Charges"
      end
      return text
   end
   
   # returns the person full name, optionally in last-name-first order.
   def full_name(reverse = true)
      nameparts = HashWithIndifferentAccess.new
      nameparts[:first] = firstname
      nameparts[:middle] = middlename
      nameparts[:last] = lastname
      nameparts[:suffix] = suffix
      unparse_name(nameparts, reverse)
   end
   
   def middlename
      "#{middle1} #{middle2} #{middle3}".strip
   end
   
   # returns persons sex (stored as an integer) as a sex name string.
   def sex_name(short=false)
      if short
         return "M" if sex == 1
         return "F" if sex == 2
      else
         return "Male" if sex == 1
         return "Female" if sex == 2
      end
      return nil
   end
   
   def special_name(short=false)
      if short
         return "BW" if special_type == 1
         return "WRIT" if special_type == 2
         return "WAR"
      else
         return "Bench Warrant" if special_type == 1
         return "Writ" if special_type == 2
         return "Warrant"
      end
   end
   
   # returns line one of the physical address (street)
   def address1
      name = ""
      if street?
         name << street
      end
      return name
   end

   # returns line two of the physical address (city, ST ZIP)
   def address2
      name = ""
      if city
         name << city
      end
      if !state.nil?
         unless name.empty?
            name << ", "
         end
         name << state.short_name
      end
      if zip?
         unless name.empty?
            name << " "
         end
         name << zip
      end
      return name
   end
   
   # returns true if the warrant is "local" according to site config
   def is_local?
      local_zips = SiteConfig.local_zips
      return false if local_zips.blank?
      locals = local_zips.split(/[ ,]+/).compact.uniq
      return true unless zip?
      return true if local_zips.include?(zip)
      return false
   end
   
   # This returns a single text string listing all charges and their counts
   # produced by cycling through all associated charge records.
   def charge_summary
      text = ''
      charges.each do |c|
         unless text.blank?
            text << ", "
         end
         text << "#{c.charge}(#{c.count})"
      end
      if text.blank?
         return "No Charges"
      end
      return text
   end
   
   private
   
   # disallows destroying served warrants if they are attached to existing
   # arrests
   def check_dependencies
      unless district_arrests.empty? && city_arrests.empty? && other_arrests.empty?
         return false
      end
      return true
   end

   # converts the name attribute to the underlying firstname, middleX names,
   # lastname and suffix parts.
   def split_name
      unless name.nil? || name.blank?
         nameparts = parse_name(name)
         self.firstname = nameparts[:first]
         if nameparts[:middle].nil?
            self.middle1, self.middle2, self.middle3 = []
         else
            self.middle1, self.middle2, self.middle3 = nameparts[:middle].split
         end
         self.lastname = nameparts[:last]
         self.suffix = nameparts[:suffix]
      end
      return true
   end
   
   # this ensures that all associated charges get saved when updated.
   def save_charges
      charges.each do |c|
         c.save(false)
      end
      return true
   end
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # ensures that there cannot be a disposition date if there is no
   # disposition id.  Note that this could be expanded to include all
   # invalid combindations of disposition id, date and time values.
   def check_disposition
      unless disposition_id?
         if dispo_date?
            self.errors.add(:dispo_date, "cannot exist if there is no disposition!")
            return false
         end
      end
      return true
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE clause.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      intfields = ["person_id","disposition_id","oln_state_id","special_type","sex","race_id"]
      condition = ""
      query.each do |key, value|
         if key == "disposition_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "warrants.dispo_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "disposition_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "warrants.dispo_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == 'full_name'
            nameparts = parse_name(value)
            unless nameparts[:first].blank?
               init = nameparts[:first].slice(0,1).upcase
               unless condition.blank?
                  condition << " AND "
               end
               condition << "(warrants.firstname LIKE '#{nameparts[:first]}' OR LEFT(warrants.firstname,1) LIKE '#{init}' OR warrants.middle1 LIKE '#{nameparts[:first]}' OR LEFT(warrants.middle1,1) LIKE '#{init}' OR warrants.middle2 LIKE '#{nameparts[:first]}' OR LEFT(warrants.middle2,1) LIKE '#{init}' OR warrants.middle3 LIKE '#{nameparts[:first]}' OR LEFT(warrants.middle3,1) LIKE '#{init}')"
            end
            unless nameparts[:middle].blank?
               unless condition.blank?
                  condition << " AND "
               end
               parts = nameparts[:middle].split.collect{|p| "'#{p}'"}.join(',')
               inits = nameparts[:middle].split.collect{|p| "'#{p.slice(0,1)}'"}.join(',')
               condition << "(warrants.middle1 IN (#{parts}) OR LEFT(warrants.middle1,1) IN (#{inits}) OR warrants.middle2 IN (#{parts}) OR LEFT(warrants.middle2,1) IN (#{inits}) OR warrants.middle3 IN (#{parts}) OR LEFT(warrants.middle3,1) IN (#{inits}) OR warrants.firstname IN (#{parts}) OR LEFT(warrants.firstname,1) IN (#{inits}))"
            end
            unless nameparts[:last].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "warrants.lastname like '#{nameparts[:last]}'"
            end
            unless nameparts[:suffix].blank?
               unless condition.blank?
                  condition << " and "
               end
               condition << "warrants.suffix like '#{nameparts[:suffix]}'"
            end
         elsif key == "dob_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "warrants.dob >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "dob_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "warrants.dob <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "issued_from"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "warrants.issued_date >= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "issued_to"
            if myval = valid_date(value)
               unless condition.empty?
                  condition << " and "
               end
               condition << "warrants.issued_date <= '#{myval}'"
            else
               fullquery.delete(key)
            end
         elsif key == "address"
            unless condition.empty?
               condition << " and "
            end
            condition << "(warrants.street like '%#{value}%' OR warrants.city like '%#{value}%')"
         elsif key == 'local'
            # value is 0 for no, 1 for yes
            # query has a hidden field with a list of site configured local zips
            unless SiteConfig.local_zips.blank?
               local_zips = SiteConfig.local_zips.split(/[ ,]+/).compact.uniq.collect{|z| "'#{z}'"}.join(',')
               unless local_zips.empty?
                  unless condition.empty?
                     condition << " and "
                  end
                  if value == '0'
                     condition << "(warrants.zip NOT IN (#{local_zips}) AND warrants.zip IS NOT NULL)"
                  else
                     condition << "(warrants.zip IS NULL OR warrants.zip IN (#{local_zips}))"
                  end
               end
            end
         else
            unless condition.empty?
               condition << " and "
            end
            if intfields.include?(key)
               condition << "warrants.#{key} = '#{value}'"
            else
               condition << "warrants.#{key} like '%#{value}%'"
            end
         end
      end
      return sanitize_sql_for_conditions(condition)
   end

   # checks to see if a warrant number is entered, and if not, sets it to
   # the record id as a 6 digit string with leading zeros.
   def check_and_generate_warrant_no
      unless warrant_no?
         self.update_attribute(:warrant_no, id.to_s.rjust(6,'0'))
      end
      return true
   end
end
