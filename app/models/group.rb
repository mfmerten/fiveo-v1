# == Schema Information
#
# Table name: groups
#
#  id          :integer(4)      not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_by  :integer(4)      default(1)
#  updated_by  :integer(4)      default(1)
#  created_at  :datetime
#  updated_at  :datetime
#  permissions :string(255)
#

# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Group Model
# These are user groups... collections of roles and permissions that can be
# batch assigned to users.
class Group < ActiveRecord::Base
   has_and_belongs_to_many :users
   belongs_to :creator, :class_name => "User", :foreign_key => 'created_by'
   belongs_to :updater, :class_name => "User", :foreign_key => 'updated_by'
   
   serialize :permissions

   before_destroy :check_and_destroy_dependencies
   after_save :check_creator

   validates_presence_of :name, :description
   validates_uniqueness_of :name

   # a short description for the DatabaseController index view
   def self.desc
      "Groups of permissions"
   end

   # returns an array of arrays with group name and id suitable for use
   # in a select control.
   def self.names_for_select
      find(:all, :order => 'name').collect{|g| [g.name, g.id]}
   end
   
   # returns base permissions required to View groups
   def self.base_perms
      Perm[:view_group]
   end
   
   # I define the permissions field reader to return [] instead of nil
   # when there are no permissions set.
   def permissions
      tmp = super
      if tmp.nil?
         []
      else
         tmp
      end
   end
   
   # I define the permissions field writer to set [] instead of nil
   # when there are no permissions specified.  Also ensures permission
   # values are integer even when specified as numeric strings.
   def permissions=(perms)
      if perms.blank?
         values = []
      else
         values = perms.collect{|v| v.to_i}
      end
      super(values)
   end

   private
   
   # checks that if created_by or updated_by are nil, 0 or non-integer after
   # the record is saved, they get set to the default value of 1 (admin).
   def check_creator
      if !created_by? || creator.nil?
         self.update_attribute(:created_by, 1)
      end
      if !updated_by? || updater.nil?
         self.update_attribute(:updated_by, 1)
      end
      return true
   end
   
   # accepts a query hash and converts it to a SQL partial suitable for use
   # in a WHERE query.
   def self.condition_string(fullquery)
      return nil unless query = quoted(fullquery)
      condition = ""
      query.each do |key, value|
         unless condition.empty?
            condition << " and "
         end
         condition << "groups.#{key} like '%#{value}%'"
      end
      return sanitize_sql_for_conditions(condition)
   end

   # ensures that all permission, role and user associations are cleared
   # prior to destroying the group record.
   def check_and_destroy_dependencies
      return false if name == "Admin"
      self.users.clear
      return true
   end
end
