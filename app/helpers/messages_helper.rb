# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Messages Helper
# Helper for RESTful_Easy_Messages.
module MessagesHelper
   # Provides notice of any user messages.  Used in the application layout.
   def mail_notice
      if logged_in
         msgcnt = current_user.inbox_messages.count
         if msgcnt > 0
            msg = msgcnt == 1 ? " message" : " messages"
            path = inbox_user_messages_path(:user_id => session[:user])
            return link_to(msgcnt.to_s + msg, path)
         else
            return "No Messages"
         end
      else
         return "Not Logged In"
      end
   end
  
  # Dynamic label for the sender/receiver column in the messages.rhtml view
  def rezm_sender_or_receiver_label
    if params[:action] == "outbox"
      "Recipient"
    # Used for both inbox and trashbin
    else
      "Sender"
    end
  end
  
  # Checkbox for marking a message for deletion
  def rezm_delete_check_box(message)
    check_box_tag 'selected[]', message.id
  end
  
  # Link to view the message
  def rezm_link_to_message(message)
     link_to "#{h(rezm_subject_and_status(message))}", user_message_path(current_user, message)
  end
  
  # url to view the message
  def rezm_url_to_message(message)
     url_for user_message_path(current_user, message)
  end
  
  # Dynamic data for the sender/receiver column in the messages.rhtml view
  def rezm_sender_or_receiver(message)
    if params[:action] == "outbox"
      message.receiver.name
    # Used for both inbox and trashbin
    else
      message.sender.name
    end
  end
  
  # Pretty format for message sent date/time
  def rezm_sent_at(message)
    message.created_at.to_s(:us)
  end
  
  # Pretty format for message.subject which appeads the status
  # (Deleted/Unread)
  def rezm_subject_and_status(message)
    if message.receiver_deleted?
      message.subject + " (Deleted)" 
    elsif message.read_at.nil?
      message.subject + " (Unread)"  
    else 
      message.subject
    end
  end
end
