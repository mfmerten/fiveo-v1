# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Fiveo Form Helper
# This helper module contains methods used in views containing forms.
# See also:
# * ApplicationHelper
# * FiveoLinkHelper
# * MessagesHelper
#
# Referenced Permissions:
# * None
module FiveoFormHelper
   def tip(text, *opts)
      options = HashWithIndifferentAccess.new(:force => false, :tip_type => 'info')
      options.update(opts.pop) if opts.last.is_a?(Hash)
      # type defaults to info
      unless ['info','warning'].include?(options[:tip_type])
         options[:tip_type] = 'info'
      end
      text.is_a?(String) or return nil
      if options[:tip_type] == 'info'
         tip_image = icon("info",18)
      else
         tip_image = icon("required",18)
      end
      if session[:user_show_tips] == true || options[:force] == true
         "<div class='tip'><table class='hinted'><tr><td style='vertical-align:top;'>#{tip_image}</td><td style='width:100%'>#{options[:tip_type] == 'info' ? 'Tip:' : 'Caution:'} #{text}</td></tr></table></div>"
      else
         nil
      end
   end
   
   def input_note(*items)
      items = items.flatten if items.is_a?(Array)
      text = ''
      items.each do |i|
         unless i.blank? || !i.is_a?(String)
            text << "<div class='input'><table class='hinted'><tr><td>#{icon('info',14)}</td><td><span style='color:#{@colors.color_dark};'>#{i}</span></td></tr></table></div>"
         end
      end
      text
   end
   
   def remark_note
      "These remarks will not appear on any printed documentation. They are for internal #{SiteConfig.agency_short} use only and are not to be considered part of official record."
   end
   
   # A tip providing expandable help for textile enabled field
   # formatting rules.
   def textile_help
      more_link = link_to_function("special formatting rules", nil, :id => "more_textile_link"){|page| page.show 'textile_helper'}
      less_link = link_to_function("Hide Formatting Rules", nil, :id => 'less_textile_link'){|page| page.hide 'textile_helper'}
      %(The text areas with the pale green background have #{more_link}.<div id='textile_helper'
      style='display: none;'><h4>Paragraphs</h4><p>When entering text, don't hit [Enter] until you
      are ready to end a paragraph.  As you type, the line will automatically wrap when it reaches 
      the end of the input box. This way, when the display area size changes, the lines will grow 
      or shrink properly to fit. When you are finished with a paragraph, hit [Enter] twice (to insert
      a blank line).</p><h4>Bold and Italic Text</h4><p>For bold text, enclose the text in asterisks
      (*) like this:</p><blockquote><p>this is normal, but *this is bold*</p></blockquote><p>to 
      produce:</p><blockquote><p>this is normal, but <b>this is bold</b></p></blockquote><p>Italic 
      text is done the same way, except you use underscores (_) instead, like:</p><blockquote><p>this 
      is normal, but _this is italic_</p></blockquote><p>to produce:</p><blockquote><p>this is normal, 
      but <em>this is italic</em></p></blockquote><h4>Lists</h4><p>For bulleted lists, type:</p>
      <blockquote><p>* line item 1<br/>* line item 2<br/>* line item 3</p></blockquote><p>and you will
      see:</p><blockquote><ul><li>line item 1</li><li>line item 2</li><li>line item 3</li></ul>
      </blockquote><p>For a numbered list, use # instead of *, like:</p><blockquote><p># line item
      1<br/># line item 2<br/># line item 3</p></blockquote><p>which will render as:</p><blockquote>
      <ol><li>line item 1</li><li>line item 2</li><li>line item 3</li></ol></blockquote><h4>Links</h4>
      <p>To insert a link, put link name in double-quotes, then a colon (:) followed by the link URL
      (or address), like:</p><blockquote><p>&quot;Midlake Technical Services Website&quot;:http://midlaketech.com</p></blockquote>
      <p>and it will display as:</p><blockquote><p><a href='http://midlaketech.com'>Midlake Technical Services Website</a></p>
      </blockquote><h4>More Information</h4><p>For a complete list of formatting rules, check out the
      <a href='http://www.textism.com/tools/textile'>Textile Website</a>.</p><p>#{less_link}</p></div>)
   end
   
   # A tip that provides expandable help for formatting names so
   # that the software can properly parse them.
   def name_help
      more_link = link_to_function("certain rules", nil, :id => "more_name_link"){|page| page.show 'name_helper'}
      less_link = link_to_function("Hide Name Rules", nil, :id => 'less_name_link'){|page| page.hide 'name_helper'}
      %(Names must be entered according to #{more_link}.<div id='name_helper'
      style='display: none;'><h4>Entering Names</h4><p>When entering names, the program expects that they
      will be in one of two different formats:</p><p>1. First name first:  First Middle Last Suffix</p><p>2.
      Last name first:   Last, First Middle Suffix (the comma is required!)</p><p>In both of these formats,
      the Middle name and Suffix are optional. The Last name <b>must</b> be one word.  This means that for
      multiple part last names, such as Von Neumann or del Torro, you must enter the name without the spaces
      (like VonNeumann and delTorro).  If you forget to do this, the resulting name will not be saved correctly.
      There may be as many middle names as desired.</p><p>For the suffix, only the following variations will 
      be recognized:</p><ul><li>Sr or I or 1st</li><li>Jr or II or 2nd</li><li>III or 3rd</li><li>IV or 4th</li>
      <li>V or 5th</li></ul><p>Suffixes may be in upper or lower case, may have a period (.) at the end and may
      be separated from the name with a comma.  The only comma that is required is the one following the last
      name when the name is in last-name-first format.</p><p>#{less_link}</p></div>)
   end
   
   # A tip that provides photograph upload help.
   def photo_help
      %(Photos must be in JPG format. Photos are reduced in size to save space on
      the server... you must retain your original separately for future reference.)
   end
   
   # A tip that provides case number help.
   def case_help
      %(Leave Case Number blank to assign the next available case.)
   end
   
   # A tip telling the user how to identifiy form fields that are
   # required entry.
   def std_help
      %(Fields tagged with #{required} are required.)
   end
   
   # a tip reminding the user to make sure the officer select is
   # set to nil if they want to manually enter a name, badge and unit.
   def officer_help
      %(To manually enter an officer name, make sure the officer select is set to nil ('---') or your entry will be overwritten when you save.)
   end
   
   # The filter_help method is called from index views which have filter
   # search forms.
   def filter_help(name,range = false)
      name = "" unless name.is_a?(String)
      name = h(name)
      range = false unless range.is_a?(TrueClass)
      filter0 = %(Enter only the specific information you are searching for. The more information you enter here, the less likely you are to find what you are looking for!)
      filter1 = %(To find specific #{name}, enter your search terms in the appropriate boxes
         below. Only #{name} matching all of the information you enter will be shown in the list above
         once you click the <b>Set Filter</b> button.)
      filter2 = %(The filter will remain in effect until you clear it by clicking the <b>Clear
         Filter</b> button.)
      if range
         filter3 = %(When searching by date, putting a date only in <b>Date From:</b> will
            find #{name} for that day and after. Using only <b>Date To:</b> will find #{name} for
            that date or before. If you enter a different date in each box, it will find #{name}
            between the two dates (inclusive). Entering the same date in both boxes will limit
            the #{name} to that day only.)
      else
         filter3 = nil
      end
      "<div style='font-size:80%;'><b>Instructions:</b><br/><ul><li>#{filter0}</li><li>#{filter1}</li><li>#{filter2}</li>#{filter3.nil? ? '</ul>' : '<li>' + filter3 + '</li></ul>'}</div>"
   end
   
   # The filter_help method is called from index views which have simple filter
   # search forms.
   def filter_help_simple(name=false)
      filter1 = %(Enter your search term and click the <b>Set Filter</b> button.)
      filter2 = %(The filter will remain in effect until you clear it by clicking the <b>Clear
         Filter</b> button.)
      if name == true
         filter3 = %(When searching by name, you must enter <b>at least</b> the last name.  Do not enter initials.)
      else
         filter3 = nil
      end
      "<div style='font-size:80%;'><b>Instructions:</b><br/><ul><li>#{filter1}</li><li>#{filter2}</li>#{filter3.nil? ? '' : '<li>' + filter3 + '</li>'}</ul></div>"
   end
   
   # this begins the simple filter section for a search filter
   def filter_simple
      "<h3>Search Filter</h3>"
   end
   
   def filter_advanced_start
      link_to_function("Advanced Search"){|page| page.show 'advanced-search'} + 
      "<div id='advanced-search' style='display:none;'><h3>Alternate Search Filter (Advanced Users Only!)</h3>" +
      link_to_function("Hide"){|page| page.hide 'advanced-search'}
   end
   
   def filter_advanced_end
      "</div>"
   end
   
   # This is a shortcut method for displaying the "required" icon in form
   # labels labels for required fields
   def required
      "<span style='vertical-align: top;'>#{icon('required', 14)}</span>"
   end

   # Generates a form authenticity token for use in manually coded forms.
   def form_auth
      "<input name='#{request_forgery_protection_token.to_s}' type='hidden' value='#{form_authenticity_token}' />"
   end

   # Adds a standard "Set Filter" and "Clear Filter" button pair to manually
   # coded search filter forms used on index views.
   def filter_buttons
      "<div class='linkbar'>#{submit_tag("Set Filter")} #{submit_tag("Clear Filter")}</div>"
   end

   # Adds a standard "Submit" and "Cancel" button pair to edit forms. Can also
   # display a "Delete" button which is usually only added to photo upload
   # views so that existing photos can be removed.
   def form_buttons(*opts)
      options = HashWithIndifferentAccess.new(:delete => false, :preview => false)
      options.update(opts.pop) if opts.last.is_a?(Hash)
      "<div class='linkbar'><input type='submit' name='Enter' style='left:-100px;position:absolute;' onclick='return false;'/> #{submit_tag("Submit")} #{submit_tag("Cancel")} #{options[:delete] ? submit_tag("Delete") : ""} #{options[:preview] ? submit_tag("Preview") : ""}</div>"
   end

   # This duplicates the effects of the date_select helper method on manually
   # coded search filter forms because I could not figure out how to get the
   # helper to work without a direct reference to a form or model object.
   def filter_date_select(param, value)
      return nil unless param.is_a?(String) && (value.nil? || value.is_a?(String))
      "<input id='search_#{param}' name='search[#{param}]' size='10' type='text' value='#{value}' /> <img alt='Calendar' class='calendar_date_select_popup_icon' onclick='new CalendarDateSelect( $(this).previous(), {year_range:10} );' src='/images/calendar_date_select/calendar.gif' style='border:0px; cursor:pointer;' />"
   end

   # Generates a text input field on manually created forms like those used
   # for the search filters on index views.
   def filter_text_field(param, value, size = 30)
      value ||= ""
      return nil unless param.is_a?(String) && size.is_a?(Integer) && size > 0
      "<input type='text' name='search[#{param}]' size='#{size.to_s}' value='#{value}' />"
   end

   # Generates a select field on manually created forms like those used for
   # the search filters on index views. 
   def filter_select(param, value, option_type)
      return nil unless param.is_a?(String) && (value.nil? || value.is_a?(String)) && option_type.is_a?(String)
      options = options_for_select(get_opts(option_type, :allow_disabled => true), value.to_i)
      "<select name='search[#{param}]'><option value=''>---</option>#{options}</select>"
   end
   
   # Returns a set of radio buttons with the indicated values and labels with
   # an additional button for the nil value labeled "unknown". The button
   # corresponding to supplied value will be set.
   def filter_radio_buttons(param, value, options)
      value = "" if value.nil?
      return nil unless param.is_a?(String) && value.is_a?(String) && options.is_a?(Array)
      retval = ""
      options.each do |val, label|
         retval << "<input type='radio' name='search[#{param}]' value='#{val}' #{(value == val) ? "checked='checked'" : ""}/>#{label} "
      end
      retval << "<input type='radio' name='search[#{param}]' value='' #{value.empty? ? "checked='checked'" : ""}/>Don&apos;t Care"
      retval
   end
   
   # Returns selected values from the various option type tables in an array
   # suitable for use as options in a select field on a form.
   def get_opts(otype, *opts)
      return nil unless otype.is_a?(String)
      options = HashWithIndifferentAccess.new(:short_opts => false, :allow_nil => false, :allow_disabled => false, :manual_only => false)
      options.update(opts.pop) if opts.last.is_a?(Hash)
      retarray = []
      if options[:allow_nil] == true
         retarray << ['---',nil]
      end
      if otype == 'Option Type'
         optarray = OptionType.find(:all, :order => 'name').collect{|t| [t.name, t.id]}
      elsif otype == 'Users'
         optarray = User.users_for_select(options[:allow_disabled])
      elsif otype == 'Admins'
         optarray = User.admins_for_select
      elsif otype == 'Groups'
         optarray = Group.names_for_select
      elsif otype == 'Roles'
         optarray = Role.names_for_select
      elsif otype == 'Colorschemes'
         optarray = Colorscheme.names_for_select
      elsif otype == 'Physicians'
         optarray = Contact.physicians_for_select(options[:allow_disabled])
      elsif otype == 'Pharmacies'
         optarray = Contact.pharmacies_for_select(options[:allow_disabled])
      elsif otype == 'Officers'
         optarray = Contact.officers_for_select(options[:allow_disabled])
      elsif otype == 'Detectives'
         optarray = Contact.detectives_for_select(options[:allow_disabled])
      elsif otype == 'Contacts'
         optarray = Contact.contacts_for_select(options[:allow_disabled])
      elsif otype == 'All Contacts'
         optarray = Contact.contacts_and_companies_for_select(options[:allow_disabled])
      elsif otype == 'Agencies'
         optarray = Contact.agencies_for_select(options[:allow_disabled])
      elsif otype == 'Company Names'
         optarray = Contact.company_names_for_select(options[:allow_disabled])
      elsif otype == 'Local Agencies'
         if SiteConfig.local_agencies.blank?
            optarray = get_opts('Agencies')
         else
            optarray = []
            SiteConfig.local_agencies.split(',').each do |a|
               if Contact.find_by_id(a.to_i)
                  optarray.push([Contact.find_by_id(a).fullname, a.to_i])
               end
            end
            if optarray.empty?
               optarray = get_opts('Agencies')
            end
         end
      elsif otype == 'Companies'
         optarray = Contact.companies_for_select(options[:allow_disabled])
      elsif otype == 'Printable Companies'
         optarray = Contact.companies_for_select(options[:allow_disabled],true)
      elsif otype == 'Commissary Posting'
         optarray = Commissary.posting_dates
      elsif otype == 'Call Report'
         optarray = Call.reporting_dates
      elsif otype == 'Probation Posting'
         optarray = ProbationPayment.posting_dates
      elsif otype == 'Paper Posting'
         optarray = PaperPayment.posting_dates
       elsif otype == 'Paper Status'
         optarray = Paper.status_for_select
      elsif otype == 'Paper Customers'
         optarray = PaperCustomer.names_for_select(options[:allow_disabled])
      elsif otype == 'Paper Customers Common'
         optarray = PaperCustomer.common_names_for_select(options[:allow_disabled])
      elsif otype == 'Paper Type'
         optarray = PaperType.names_for_select(options[:allow_disabled])
      elsif otype == 'Time'
         optarray = [['Days',1],['Months',2],['Years',3]]
      elsif otype == 'Months'
         optarray = [['January',1],['February',2],['March',3],['April',4],['May',5],['June',6],['July',7],['August',8],['September',9],['October',10],['November',11],['December',12]]
      elsif otype == 'Years'
         this_year = Date.today.year
         optarray = []
         (this_year - 10..this_year).each do |y|
            optarray.push(["#{y}",y])
         end
      elsif otype == 'Warrant Type'
         optarray = [['Warrant',0],['Bench Warrant',1],['Writ',2]]
      elsif otype == 'Arrest Type'
         optarray = [['District Charges',0],['City Charges',1],['Other Charges',2]]
      elsif otype == 'Bondsmen'
         if options[:allow_disabled] == true
            optarray = Bondsman.find(:all, :order => 'name').collect{|b| [b.name, b.id]}
         else
            optarray = Bondsman.find(:all, :order => 'name', :conditions => 'disabled != 1').collect{|b| ["#{b.name} (#{b.company})", b.id]}
         end
      elsif otype == 'Hold Type'
         opt = OptionType.find_by_name('Hold Type')
         conditions = ""
         if options[:manual_only] == true
            conditions << "short_name = 'manual'"
         end
         if options[:allow_disabled] == false
            unless conditions.empty?
               conditions << " and "
            end
            conditions << "disabled != 1"
         end
         conditions = nil if conditions.blank?
         optarray = Option.find_all_by_option_type_id(opt.id, :order => 'long_name', :conditions => conditions).collect{|o| [o.long_name, o.id]}
      elsif options[:short_opts] == true
         opt = OptionType.find_by_name(otype)
         if options[:allow_disabled] == true
            optarray = Option.find_all_by_option_type_id(opt.id, :order => 'short_name').collect{|o| [o.short_name, o.id]}
         else
            optarray = Option.find_all_by_option_type_id(opt.id, :order => 'short_name', :conditions => 'disabled != 1').collect{|o| [o.short_name, o.id]}
         end
      else
         opt = OptionType.find_by_name(otype)
         if options[:allow_disabled] == true
            optarray = Option.find_all_by_option_type_id(opt.id, :order => 'long_name').collect{|o| [o.long_name, o.id]}
         else
            optarray = Option.find_all_by_option_type_id(opt.id, :order => 'long_name', :conditions => 'disabled != 1').collect{|o| [o.long_name, o.id]}
         end
      end
      return retarray.concat(optarray)
   end
   
   # This is a special version of get_opts. Rather than returning the option
   # name (long or short) paired with the option id, it returns the long_name
   # paired with the short_name from the options table.
   def get_short_opts(otype)
      return nil unless otype.is_a?(String)
      OptionType.find(:first, :conditions => ["name = ?", otype]).options.reject(&:disabled).collect{|o| [o.long_name, o.short_name]}
   end
   
   # Provides a table row with a booking id input field, a person name display
   # field and a 'find_by_name' button.  When the id field changes, the id
   # is validated and the person name is filled in automatically.
   # Alternatively, the 'find_by_name' button can be use to search for a
   # booking record by name, selecting the appropriate record from a popup
   # window.
   def booking_row_with_lookup(model, assoc_model, assoc_object)
      return nil unless model.is_a?(String) && assoc_model.is_a?(String) && assoc_object.is_a?(Object) && assoc_object.respond_to?('person')
      path = url_for(:controller => 'booking', :action => 'lookup')
      lookup_link = button_to_function('Find By Name', %(var caller_id = $(this).up('.#{assoc_model}').down('.#{assoc_model}_id').identify(); var caller_name = $(this).up('.#{assoc_model}').down('.booking_name').identify(); peopleLookup('#{path}',caller_id,caller_name)))
      assoc_models = assoc_model.pluralize
      unless assoc_object.person.nil?
         name_link = show_person(assoc_object.person)
      else
         name_link = "INVALID"
      end
      remove_link = button_to_function("Remove", "$(this).up('.#{assoc_model}').remove()")
      url = url_for(:controller => 'booking', :action => 'validate_id')
      at = form_authenticity_token
      lookup_function = %(var field = $(this).up('.#{assoc_model}').down('.booking_name').identify(); var adjusted_url = '#{url}?i=' + encodeURIComponent($(this).value) + '&amp;update=' + encodeURIComponent(field) + '&amp;authenticity_token=' + encodeURIComponent('#{at}'); new Ajax.Request(adjusted_url); return false;)
      %(<tr class='#{assoc_model}'><th>Inmate:</th><td><input type='text' name='#{model}[assigned_#{assoc_models}][]' size='6' value='#{assoc_object.id}' onchange="#{lookup_function}" class='#{assoc_model}_id'/>#{hint 'Booking ID'}</td><td><span class='booking_name'>#{name_link}</span></td><td>#{remove_link}</td><td>#{lookup_link}</td></tr>)
   end
   
   # Provides a link that when clicked will insert a person id and name into
   # a pre-existing form.  This is used by the popup window created by a
   # 'find_by_name' button click from various forms.
   def select_person_link(person, target_id, target_name, status, status_name)
      return nil unless person.is_a?(Person) && target_id.is_a?(String) && target_name.is_a?(String) && status.is_a?(String) && status_name.is_a?(String)
      name = person.full_name
      link_to_function(person.id.to_s, %Q(selectPerson('#{person.id}','#{target_id}','#{name}','#{target_name}','#{status}','#{status_name}')))
   end
   
   def select_signal_link(signal,name)
      return nil unless signal.is_a?(String) && name.is_a?(String)
      link_to_function(signal, %Q(selectSignal('#{signal}','#{name}')))
   end
   
   # Provides a link that when clicked will insert a booking id and person
   # name into a pre-exisiting form.  This is used by the popup window created
   # by a 'find_by_name' button click from an absent edit form.
   def select_booking_link(booking, target_id, target_name)
      return nil unless booking.is_a?(Booking) && target_id.is_a?(String) && target_name.is_a?(String)
      namelink = (booking.person.nil? ? 'Invalid Person' : (link_to booking.person.full_name, booking))
      link_to_function(booking.id.to_s, %Q(selectBooking('#{booking.id}','#{target_id}','#{namelink}','#{target_name}')))
   end
   
   # Javascript for the onchange event for the charge field on the
   # misc/charge partial, this calls validate_charge in
   # StatutesController with the value of the charge field. If the text
   # matches a known statute, it will be replaced with the full text of the
   # statute. 
   def js_charge_lookup
      url = url_for(:controller => 'statutes', :action => 'validate_charge')
      at = form_authenticity_token
      %(var field = $(this).identify(); new Ajax.Request('#{url}?u=' + encodeURIComponent($(this).value) + '&amp;update=' + encodeURIComponent(field) + '&amp;authenticity_token=' + encodeURIComponent('#{at}')); return false;)
   end
   
   # Javascript for the onchange event for the arrest warrant field on the
   # partial, this calls lookup in warrants_controller with the
   # value of the warrant_no field.  If the warrant number matches,
   # it will replace the warrant_id and agency fields from the warrant.
   def js_warrant_lookup
      url = url_for(:controller => 'warrants', :action => 'lookup')
      at = form_authenticity_token
      %(var war_id = $(this).up('.warrant').down('.warrant_id').identify(); var war_agency = $(this).up('.warrant').down('.agency').identify(); var war_bw = $(this).up('.warrant').down('.st').identify(); var war_name = $(this).up('.warrant').down('.name-div').identify(); var adjusted_url = '#{url}?i=' + encodeURIComponent($(this).value) + '&amp;id_id=' + encodeURIComponent(war_id) + '&amp;st_id=' + encodeURIComponent(war_bw) + '&amp;agency_id=' + encodeURIComponent(war_agency) + '&amp;name_id=' + encodeURIComponent(war_name) + '&amp;authenticity_token=' + encodeURIComponent('#{at}'); new Ajax.Request(adjusted_url); return false;)
   end
   
   # this helper is used to make all contact input fields uniform across the
   # application.  The string version of the main name field is passed, and
   # since all models use the same format, the 4 required fields (for Officers
   # and Contacts) or 2 required fields (for Agencies and Companies) can be
   # generated from that. Note: this generates a complete table row.
   def contact_row(object, field_name, *opts)
      return nil unless object.is_a?(Symbol) && field_name.is_a?(Symbol)
      options = HashWithIndifferentAccess.new(:getopts => 'Officers', :required => false, :title => field_name.id2name.humanize.titleize + ":")
      options.update(opts.pop) if opts.last.is_a?(Hash)
      select_id = "#{object.id2name}_#{field_name.id2name}_id"
      name_id = "#{object.id2name}_#{field_name.id2name}"
      if options[:getopts] == 'Officers' || options[:getopts] == 'Contacts' || options[:getopts] == 'Detectives'
         badge_id = "#{object.id2name}_#{field_name.id2name}_badge"
         unit_id = "#{object.id2name}_#{field_name.id2name}_unit"
      else
         badge_id = ""
         unit_id = ""
      end
      req = options[:required] ? required : ""
      txt = %(<tr><th>#{req}#{options[:title]}</th><td><table class='hinted'><tr><td>) +
      select(object, "#{field_name.id2name}_id".to_sym, get_opts(options[:getopts], :allow_nil => true),{}, {:onchange => "contactRowToggle('#{select_id}','#{name_id}','#{unit_id}','#{badge_id}')"}) + %(</td><td>#{icon('forward',16)}</td>)
      if options[:getopts] == 'Officers' || options[:getopts] == 'Contacts' || options[:getopts] == 'Detectives'
         txt << %(<td>) + text_field(object, "#{field_name.id2name}_badge".to_sym, :size => 6) + hint("Badge") + %(</td>) +
         %(<td>) + text_field(object, "#{field_name.id2name}_unit".to_sym, :size => 6) + hint("Unit") + %(</td>)
      end
      txt << %(<td>) + text_field(object, "#{field_name.id2name}".to_sym, :size => 25) + hint("Name") + %(</td>)
      txt << %(</tr></table></td></tr>)
      return txt
   end
   
   # this helper is used to make all person input fields uniform across the
   # application.
   def person_row(object, field_name, *opts)
      # OPTIMIZE: rewrite this to incorporate js_person_lookup method
      #           could also be a bit more readable - see warrant_row()
      options = HashWithIndifferentAccess.new(:model => nil, :remove => false, :required => false, :title => field_name.id2name.humanize.titleize + ":")
      options.update(opts.pop) if opts.last.is_a?(Hash)
      object_name = object.class.to_s.tableize.singularize
      assoc_name = field_name.id2name.slice(0..-4)
      if object.is_a?(Person)
         person = object
      else
         person = Person.find_by_id(object.send(field_name))
      end
      # disallow using merge_locked person records
      while !person.nil? && person.merge_locked?
         person = person.alias_for
      end
      req = (options[:required] ? required : "")
      if options[:remove]
         return "" if options[:model].nil?
         remove_link = "<td>" + button_to_function("Remove", "$(this).up('.#{assoc_name}').remove()") + "</td>"
         obj_str = "#{options[:model]}[assigned_#{assoc_name.pluralize}][]"
      else
         remove_link = ""
         obj_str = "#{object_name}[#{field_name.id2name}]"
      end
      %(<tr class="#{assoc_name}"><th>#{req}#{options[:title]}</th><td><table class='hinted'><tr><td>) +
      %(<input type='text' name="#{obj_str}" size='6' value='#{person.nil? ? nil : person.id}' onchange="#{js_person_validate(assoc_name, field_name, 'validate')}" class="#{field_name.id2name}"/>) + hint("ID") +
      %(</td><td><input type='text' class="#{field_name.id2name + '_full_name'}" size='25' value="#{person.nil? ? nil : person.full_name}" onchange="#{js_person_validate(assoc_name, field_name,'lookup')}"/>) + hint("Full Name") +
      %(</td>#{remove_link}<td><div class="#{field_name.id2name}_full_name_status">&nbsp;</div></td></tr></table></td></tr>)
   end
   
   # this is a special helper for the arrest form. will not work anywhere else
   # as written.  Provides a multi-row warrant select/input control.
   def warrant_row(object, type='warrant')
      remove_link = button_to_function("Remove", "$(this).up('.warrant').remove()")
      at = form_authenticity_token
      lookup_url = url_for(:controller => 'warrants', :action => 'lookup_list')
      lookup_script = %(
         var person = $('arrest_person_id').value;
         var id = $(this).up('.warrant').down('.warrant_id').identify();
         var status = $(this).up('.warrant').down('.warrant_info').identify();
         var number = $(this).value;
         var charges = $(this).up('.warrant').down('.warrant_charges').identify();
         var adjusted_url = '#{lookup_url}?warrant_no=' + encodeURIComponent(number) + '&amp;warrant_charges=' + encodeURIComponent(charges) + '&amp;warrant_status=' + encodeURIComponent(status) + '&amp;warrant_id=' + encodeURIComponent(id) + '&amp;person_id=' + encodeURIComponent(person) + '&amp;authenticity_token=' + encodeURIComponent('#{at}');
         window.open(adjusted_url, 'name', 'height=600,width=800,scrollbars=1')
         return false;
      )
      # return a table row
      %(
         <tr class="warrant">
            <th>Warrant:</th>
            <td>
               <table class='hinted'>
                  <tr>
                     <td>
                        <input type='hidden' name="arrest[assigned_#{type}s][]" value='#{object.id}' class="warrant_id"/>
                        <input type='text' size='25' value="#{object.warrant_no}" onchange="#{lookup_script}"/>
                        #{hint("Warrant No.")}
                     </td>
                     <td>#{remove_link}</td>
                     <td><div class="warrant_info">#{object.id.nil? ? 'Not On File' : '[' + (object.jurisdiction.nil? ? 'Invalid Agency]' : object.jurisdiction.fullname + ' warrant for ' + (object.person.nil? ? object.full_name : show_person(object.person)) + ']')}</div></td>
                  </tr>
                  <tr>
                     <td colspan='4'><div class='warrant_charges' style='font-size:85%;'><b>Charge(s):</b> #{object.nil? ? '' : object.charge_summary}</div></td>
                  </tr>
               </table>
            </td>
         </tr>
      )
   end
   
   # Provides a link that when clicked will insert a warrant id and info into
   # a pre-existing form.  This is used by the popup window created when
   # multiple warrants match a warrant_id entered on the arrest form.
   def select_warrant_link(warrant, warrant_id, warrant_status, warrant_charges)
      return nil unless (warrant.nil? || warrant.is_a?(Warrant))
      if warrant.nil?
         status = 'Not On File'
         id = nil
         charges = nil
      else
         status = "[#{warrant.jurisdiction.nil? ? '' : warrant.jurisdiction.fullname} warrant for #{warrant.person.nil? ? warrant.full_name : show_person(warrant.person)}]"
         id = warrant.id
         charges = "<b>Charge(s):</b> #{warrant.charge_summary}"
      end
      link_to_function(warrant.id.to_s, %Q(selectWarrant('#{id}','#{warrant_id}','#{status}','#{warrant_status}','#{charges}','#{warrant_charges}')))
   end
   
   # Javascript used by the person_row method in this helper
   # module.  This calls the validate method of the PeopleController when
   # the person id field is changed.
   def js_person_validate(table_row_class, field_name, type='validate')
      # return nil unless table_row_class.is_a?(String) && field_name.is_a?(String) && type.is_a?(String)
      at = form_authenticity_token
      if type == 'validate'
         url = url_for(:controller => 'people', :action => 'validate')
         %(
            var caller_status = $(this).up('.#{table_row_class}').down('.#{field_name.id2name}_full_name_status').identify();
            var caller_id = $(this).identify();
            var caller_name = $(this).up('.#{table_row_class}').down('.#{field_name.id2name}_full_name').identify();
            var adjusted_url = '#{url}?id=' + encodeURIComponent($(this).value) + '&amp;caller_status=' + encodeURIComponent(caller_status) + '&amp;caller_id=' + encodeURIComponent(caller_id) + '&amp;caller_name=' + encodeURIComponent(caller_name) + '&amp;authenticity_token=' + encodeURIComponent('#{at}');
            new Ajax.Request(adjusted_url);
            return false;
         )
      else
         url = url_for(:controller => 'people', :action => 'lookup')
         %(
            var caller_status = $(this).up('.#{table_row_class}').down('.#{field_name.id2name}_full_name_status').identify();
            var caller_name = $(this).identify();
            var caller_id = $(this).up('.#{table_row_class}').down('.#{field_name.id2name}').identify();
            var adjusted_url = '#{url}?name=' + encodeURIComponent($(this).value) + '&amp;caller_status=' + encodeURIComponent(caller_status) + '&amp;caller_id=' + encodeURIComponent(caller_id) + '&amp;caller_name=' + encodeURIComponent(caller_name) + '&amp;authenticity_token=' + encodeURIComponent('#{at}');
            new Ajax.Request(adjusted_url);
            return false;
         )
      end
   end

   # This is a shortcut for adding a small text hint beneath a form field.
   def hint(text)
      return nil unless text.is_a?(String)
      "<div class='hint'>#{text}</div>"
   end
   
   def nav_button_row(*opts)
      options = HashWithIndifferentAccess.new(:action => 'index', :include_alpha => false, :alpha_on_top => false)
      options.update(opts.pop) if opts.last.is_a?(Hash)
      action = options[:action]
      return if @page <= 0 || @pages <= 1 || action.nil? || !action.is_a?(String)
      txt = ""
      if options[:alpha_on_top] == true && options[:include_alpha] == true
         txt << "<div class='table-nav'>"
         ('A'..'Z').each{|a| txt << button_to_function(a, :class => 'anav'){|page| page.redirect_to(url_for(:action => action, :alpha => a))}}
         txt << "</div>"
      end   
      txt << "<div class='table-nav'>" +
      button_to_function("|<- First", :class => 'tnav', :disabled => (@page == 1)){|page| page.redirect_to(url_for(:action => action, :page => 1))} +
      " " +
      button_to_function("<< Jump 10", :class => 'tnav', :disabled => (@page < 10)){|page| page.redirect_to(url_for(:action => action, :page => @page - 10))} +
      " " +
      button_to_function("< Previous", :class => 'tnav', :disabled => (@page <= 1)){|page| page.redirect_to(url_for(:action => action, :page => @page - 1))} +
      " [Page #{@page} of #{@pages}] " +
      button_to_function("Next >", :class => 'tnav', :disabled => (@page >= @pages)){|page| page.redirect_to(url_for(:action => action, :page => @page + 1))} +
      " " +
      button_to_function("Jump 10 >>", :class => 'tnav', :disabled => (@page + 10 > @pages)){|page| page.redirect_to(url_for(:action => action, :page => @page + 10))} +
      " " +
      button_to_function('Last ->|', :class => 'tnav', :disabled => (@page == @pages)){|page| page.redirect_to(url_for(:action => action, :page => @pages))} +
      "</div>"
      if options[:alpha_on_top] == false && options[:include_alpha] == true
         txt << "<div class='table-nav'>"
         ('A'..'Z').each{|a| txt << button_to_function(a, :class => 'anav'){|page| page.redirect_to(url_for(:action => action, :alpha => a))}}
         txt << "</div>"
      end
      txt
   end
   
   # formats a number with thousands separator (,) and fixed to 2 digits but no currency symbol
   def number_to_money(value)
      number_to_currency(value, :unit => '')
   end
   
   # calls FormBuilder.text_field with extra parameters to properly format money fields
   # usage:  money_field @probation, :fund1_charged
   def money_field(object, field_name, *args)
      return nil unless object.is_a?(Object) && (field_name.is_a?(Symbol) || field_name.is_a?(String)) && object.respond_to?(field_name.to_s)
      object_name = ActionController::RecordIdentifier.singular_class_name(object)
      options = HashWithIndifferentAccess.new(:size => 12, :style => 'text-align:right;')
      options.update(args.pop) if args.last.is_a?(Hash)
      text_field_tag "#{object_name.downcase}[#{field_name.to_s}]", number_with_precision(object.send(field_name.to_s), :precision => 2), options.to_options
   end
end
