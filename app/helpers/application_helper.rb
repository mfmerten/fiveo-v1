# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Application Controller
# This is the main helper file for the fiveo project.  For additional helpers
# see:
# * FiveoFormHelper
# * FiveoLinkHelper
# * MessagesHelper
#
# Referenced Permissions:
# * None
module ApplicationHelper
  
   # plain version number
   def version_no
     "1.1.12"
   end
   
   module_function :version_no
   
   # Returns the current version of the software
   def software_version
      "Version: <span class='message'>#{version_no}</span>"
   end
   
   def time_to_12(t)
      return nil unless t =~ /^\d{2}:\d{2}$/
      hours, minutes = t.split(':')
      hours = hours.to_i
      ap = "AM"
      if hours > 12
         hours = hours - 12
         ap = "PM"
      end
      hours = hours.to_s
      return "#{hours}:#{minutes} #{ap}"
   end
   
   # Returns a string containing the specified number of HTML non-breaking
   # space characters. -- space(2) returns "&nbsp;&nbsp;"
   def space(count)
      return "&nbsp;" unless count
      return "&nbsp;" unless count.is_a?(Integer) && count > 0
      return "&nbsp;" * count
   end
   
   def htmlize(string)
      return nil if string.nil?
      return nil unless string.is_a?(String)
      txt = string.clone.sub(/^\s+/, space(8))
      txt = txt.gsub(/[\n\r]/,'<br/>')
      txt = txt.gsub(/<br\/>\s+/,'<br/>' + space(8))
      return txt
   end
   
   # Calculates current age from the supplied date (DOB).
   def age(dob)
      return nil unless dob.is_a?(Date)
      now = Date.today
      if (now.month < dob.month || now.month == dob.month) && (now.day < dob.day)
         return now.year - dob.year - 1
      else
         return now.year - dob.year
      end
   end
   
   # Trims the supplied text to the specified length (default 100 chars) and
   # returns an HTML span with the result. The 'summary' class causes the
   # text to display in small characters using dark gray color.  Used to
   # provide a "summary" of a large text field for display in limited space
   # such as an index entry.   
   def summary(text, size = 100)
      text = "" unless text.is_a?(String)
      size = 100 unless size.is_a?(Integer) && size > 0
      if text.length > size
         return "<span class='summary'>" + text.slice(0,size) + "...</span>"
      else
         return "<span class='summary'>#{text}</span>"
      end
   end

   # This looks up a call sheet and returns the details formatted using the
   # 'call_detail' class (bordered box with light gray background). This is
   # used on forms accepting call_id fields to validate the id entered.
   def call_detail_summary(call)
      if call.nil? || !call.is_a?(Call)
         %Q(<div class='call_detail'>Unknown</div>)
      else
         %Q(<div class='call_detail'>Call Details:<br/>) + textilize(call.details) + %Q(</div>)
      end
   end

   # Generates an HTML calendar for the month containing the indicated date
   # with optional text displayed to the left (previous) and right (next) of
   # the date name.  The current date will be highlighted, and all calendar
   # events for the month will be shown in the appropriate days.
   def build_calendar(display_date, previous_text = nil, next_text = nil)
      return nil unless display_date.is_a?(Date)
      unless previous_text.nil?
         previous_text = "" unless previous_text.is_a?(String)
      end
      unless next_text.nil?
         next_text = "" unless next_text.is_a?(String)
      end
      events = Event.get_current_events(display_date, session[:user])
      calendar({:year => display_date.year, :month => display_date.month, :previous_month_text => previous_text, :next_month_text => next_text}) do |d|
         cell_text = "<div class='dayNumber'>#{d.mday}</div>"
         cell_attrs = {:class => 'aday'}
         events.each do |e|
            if e[0] == d
               if e[1].details?
                  cell_text << "<div class='eventItem'>" + (e[1].private? ? "<span class='error'>" : "") + link_to(e[1].name, e[1]) + (e[1].private? ? "</span>" : "") + "</div>"
               else
                  cell_text << "<div class='eventItem'>" + (e[1].private? ? "<span class='error'>" : "") + e[1].name + (e[1].private? ? "</span>" : "") + "</div>"
               end
               cell_attrs[:class] = 'aspecialDay'
            end
         end
         [cell_text, cell_attrs]
      end
   end
   
   # Accepts an array or arrays of permission values and builds a 3-column
   # HTML table of permission names
   def perms_table(*objs)
      return "" if objs.nil? || objs.empty?
      perms = [objs].flatten.collect{|p| Perm[p.to_i].to_s.titleize}.sort
      rows = (perms.size / 3.0).ceil
      txt = "<table style='border: 1px solid #{@colors.color_alt_dark};'>"
      rows.times do
         txt << "<tr class='#{cycle('list-line-odd','list-line-even')}'>"
         3.times do
            txt << "<td style='white-space:nowrap;'>#{perms.shift}</td>"
         end
         txt << "</tr>"
      end
      txt << "</table>"
      txt
   end
   
   # accepts a da charge and optional arrest charge and formats a string
   # showing processing status
   #
   def charge_status_string(da_charge=nil, arr_charge=nil)
      return nil unless da_charge.is_a?(DaCharge)
      return nil unless arr_charge.nil? || arr_charge.is_a?(Charge)
      txt = ""
      if !arr_charge.nil? && !da_charge.nil? && da_charge.charge != arr_charge.charge
         txt << "Amended to (#{da_charge.count}) #{da_charge.charge}"
      end
      if da_charge.info_only?
         unless txt.blank?
            txt << " -- "
         end
         txt << "[Info Only]"
      elsif da_charge.sentence.nil? && !da_charge.dropped_by_da?
         unless txt.blank?
            txt << " -- "
         end
         txt << "Pending Court"
      elsif !da_charge.sentence.nil?
         unless txt.blank?
            txt << " -- "
         end
         txt << da_charge.sentence.sentence_summary
      end
      if da_charge.dropped_by_da?
         unless txt.blank?
            txt << " -- "
         end
         txt << "[Charge Dropped]"
      end
      txt
   end
   
   # set form focus to a specific element
   def set_focus_to_id(id)
     javascript_tag("$('#{id}').focus()");
   end
end