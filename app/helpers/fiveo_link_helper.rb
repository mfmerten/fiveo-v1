# FiveO Law Enforcement Software
# Copyright (C) 2010,2011 Michael Merten <mike@midlaketech.com>
#
# This file is part of FiveO.
# 
# FiveO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# FiveO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with FiveO. If not, see <http://www.gnu.org/licenses/>.
# 
# ===========================================================================
# Fiveo Link Helper
# This module contains various helpers related to links and buttons.
#
module FiveoLinkHelper

   # Displays an icon (from images/icons directory) in the specified size
   # (default 20x20 pixels). This is used to standardize icon usage.
   def icon(name, size = 20)
      # set size = standard view size
      return nil unless name.is_a?(String) && size.is_a?(Integer) && size > 0
      image_tag("icons/" + name + ".png", :alt => name, :height => size, :width => size, :style => 'border: 0;')
   end
   
   # This generates the specified menu and returns the results
   def generate_menu(name)
      return nil unless name.is_a?(String)
      if self.respond_to?("build_#{name}_menu")
         tools = self.send("build_#{name}_menu")
         return "" if tools.empty?
         # build a list item string
         toolstring = "<span class='lbhead'>#{name.capitalize}</span><div class='navbar'><ul>"
         tools.each do |tool|
            if @current_tab == tool[0]
               toolstring << "<li class='current'>"
            else
               toolstring << "<li>"
            end
            toolstring << link_to(tool[0], tool[1]) + "</li>"
         end
         toolstring << "</ul></div>"
      else
         return ""
      end
      return toolstring
   end
   
   # The Main Menu
   def build_main_menu
      tool_list = []
      tool_list.push(["Home", root_path])
      if has_permission Perm[:view_contact]
         tool_list.push(["Contacts", contacts_path])
      end
      if has_permission Perm[:view_person]
         tool_list.push(["People", people_path])
      end
      return tool_list
   end
   
   # The Civil Menu
   def build_civil_menu
      tool_list = []
      if has_permission Perm[:view_civil]
         tool_list.push(["Papers", papers_path])
      end
      return tool_list
   end
   
   # The Court Menu
   def build_court_menu
      tool_list = []
      if has_permission Perm[:view_court]
         tool_list.push(["Dockets", dockets_path])
      end
      if has_permission Perm[:view_probation]
         tool_list.push(["Probation", probations_path])
      end
      return tool_list
   end
   
   # The Dispatch Menu
   def build_dispatch_menu
      tool_list = []
      if has_permission Perm[:view_call]
         tool_list.push(["Calls", calls_path])
      end
      if has_permission Perm[:view_forbid] 
         tool_list.push(["Forbids", forbids_path])
      end
      if has_permission Perm[:view_pawn]
         tool_list.push(["Pawns", pawns_path])
      end
      if has_permission Perm[:view_warrant]
         tool_list.push(["Warrants", warrants_path])
      end
      return tool_list
   end
   
   # The Enforcement Menu
   def build_enforcement_menu
      tool_list = []
      if has_permission Perm[:view_case]
         tool_list.push(["Case Notes", case_notes_path])
      end
      if has_permission Perm[:view_citation]
         tool_list.push(["Citations", citations_path])
      end
      if has_permission Perm[:view_evidence]
         tool_list.push(["Evidence", evidences_path])
      end
      if has_permission(Perm[:view_history]) && SiteConfig.show_history
         tool_list.push(["History", histories_path])
      end
      if has_permission Perm[:view_investigation] 
         tool_list.push(["Investigation", investigations_path])
      end
      if has_permission Perm[:view_offense]
         tool_list.push(["Offense", offenses_path])
         tool_list.push(["Offense Photos", offense_photos_path])
      end
      if has_permission Perm[:view_stolen]
         tool_list.push(["Stolen", stolens_path])
      end
      if has_permission Perm[:view_transport]
         tool_list.push(["Transports", transports_path])
      end
      return tool_list
   end
   
   # The Jail Menu
   def build_jail_menu
      tool_list = []
      if has_permission(Perm[:view_jail1]) && SiteConfig.jail1_enabled
         tool_list.push([(SiteConfig.jail1_short_name.blank? ? 'Jail 1' : SiteConfig.jail1_short_name), jail1s_path])
      end
      if has_permission(Perm[:view_jail2]) && SiteConfig.jail2_enabled
         tool_list.push([(SiteConfig.jail2_short_name.blank? ? 'Jail 2' : SiteConfig.jail2_short_name), jail2s_path])
      end
      if has_permission(Perm[:view_jail3]) && SiteConfig.jail3_enabled
         tool_list.push([(SiteConfig.jail3_short_name.blank? ? 'Jail 3' : SiteConfig.jail3_short_name), jail3s_path])
      end
      if has_permission Perm[:view_booking]
         tool_list.push(["Absents", absents_path])
      end
      if has_permission Perm[:view_arrest]
         tool_list.push(["Arrests", arrests_path])
      end
      if has_permission Perm[:view_bond]
         tool_list.push(["Bonds", bonds_path])
      end
      if has_permission Perm[:view_option]
         tool_list.push(["Bondsmen", bondsmen_path])
      end
      if has_permission Perm[:view_booking]
         tool_list.push(["Booking", bookings_path])
      end
      if has_permission Perm[:view_commissary]
         tool_list.push(["Commissary", commissaries_path])
      end
      if has_permission Perm[:view_booking]
         tool_list.push(["DOC Inmates", docs_path])
         tool_list.push(["Holds", holds_path])
         tool_list.push(["Transfer Log", transfers_path])
      end
      return tool_list
   end
   
   # the Utility menu
   def build_utilities_menu
      tool_list = []
      if has_permission Perm[:view_announcement]
         tool_list.push(["Announcements", announcements_path])
      end
      if has_permission Perm[:view_event]
         tool_list.push(["Events", events_path])
      end
      tool_list.push(["FAX Covers", fax_covers_path])
      if has_permission Perm[:view_option]
         tool_list.push(["Options", options_path])
      end
      if has_permission Perm[:view_statute]
         tool_list.push(["Statutes", statutes_path])
      end
      if has_permission Perm[:view_option]
         tool_list.push(["Zip Codes", zips_path])
      end
      return tool_list
   end
   
   # the User menu
   def build_user_menu
      tool_list = []
      tool_list.push(["Preferences", edit_user_settings_path])
      tool_list.push(["New Password", url_for(:controller => "users", :action => "change_password")])
      tool_list.push(["New Message", new_user_message_path(:user_id => session[:user])])
      if SiteConfig.allow_user_photos
         tool_list.push(["User Photo", url_for(:controller => 'users', :action => 'photo', :id => session[:user])])
      end
      tool_list.push(["Inbox", inbox_user_messages_path(:user_id => session[:user])])
      tool_list.push(["Outbox", outbox_user_messages_path(:user_id => session[:user])])
      tool_list.push(["Trashbin", trashbin_user_messages_path(:user_id => session[:user])])
      return tool_list
   end
   
   # the Admin menu
   def build_admin_menu
      tool_list = []
      if has_permission Perm[:admin]
         tool_list.push(["Activity Log", activities_path])
      end
      if has_permission Perm[:view_database]
         tool_list.push(["Database", databases_path])
      end
      if has_permission Perm[:view_group]
         tool_list.push(["Groups", groups_path])
      end
      if has_permission Perm[:admin]
         tool_list.push(["Message Log", message_logs_path])
         tool_list.push(["Site", url_for(:controller => 'database', :action => 'show_config')])
      end
      if has_permission Perm[:view_user]
         tool_list.push(["Users", users_path])
      end
      return tool_list
   end
   
   # The Help menu
   def build_help_menu
      tool_list = []
      if has_permission Perm[:view_bug]
         tool_list.push(["Bug Reports", url_for(:controller => 'bugs', :action => 'index', :show_all => 1)])
      end
      tool_list.push(["Help Pages", helps_path])
      return tool_list
   end
   
   # The External Sites Menu
   def build_external_menu
      tool_list = []
      tool_list.push(["State Police", 'http://www.lsp.org'])
      tool_list.push(["FBI", 'http://www.fbi.gov'])
      tool_list.push(["NamUs", 'http://www.namus.gov'])
      tool_list.push(["NCMEC", 'http://www.missingkids.com'])
      return tool_list
   end
   
   # Javascript that uses a popup to confirm destructive actions. Used as a
   # button_to_function function primarily for the link_bar(s).
   def js_verify(path,method,confirm = false)
      # method should be 'delete'... anything else will use post... checked below
      # confirm = will accept anything that evaluates to true or false - make sure you know how
      #    what you pass here will be interpreted. checked below.
      txt = (confirm ? "if (confirm('Are you sure?')) {" : "")
      txt << %(var f = document.createElement('form'); f.style.display = 'none'; this.parentNode.appendChild(f); f.method = 'POST'; f.action = '#{url_for(path)}';)
      txt << (method == 'delete' ? "var m = document.createElement('input'); m.setAttribute('type', 'hidden'); m.setAttribute('name', '_method'); m.setAttribute('value', 'delete'); f.appendChild(m);" : "")
      txt << %(var s = document.createElement('input'); s.setAttribute('type', 'hidden'); s.setAttribute('name', '#{request_forgery_protection_token.to_s}'); s.setAttribute('value', '#{form_authenticity_token}'); f.appendChild(s); f.submit();)
      txt << (confirm ? "};" : "")
      txt << "return false;"
      txt
   end

   # Builds a button bar from the contents of the @links array. @links should
   # be an array of 2-4 element arrays with name & url entries - no
   # permissions are checked, do that before populating @links. All we do here
   # is string the links together, separate them with bullets and wrap it in a
   # <div>. Note, if option 3rd array element is true, link will include
   # standard delete confirmation and method using js_verify and the 4th
   # element will be used to specify the request method (usually POST).
   def link_bar
      return "" if @links.nil?
      return "" unless @links.is_a?(Array)
      return "" if @links.empty?
      # convert array of hashes to array of strings
      link_list = []
      @links.each do |l|
         next unless l.is_a?(Array)
         options = HashWithIndifferentAccess.new(:method => 'get', :disabled => false)
         options.update(l.pop) if l.last.is_a?(Hash)
         if options[:method] == 'get'
            link_list.push(button_to_function(l[0], :disabled => options[:disabled]){|page| page.redirect_to(l[1])})
         else
            link_list.push(button_to(l[0], l[1], options.to_options))
         end
      end
      txt = "<div class='linkbar'><table class='links'><tr>"
      btncntr = 0
      link_list.each do |link|
        btncntr += 1
        if btncntr > 7
          txt << "</tr><tr>"
          btncntr = 1
        end
        txt << "<td>#{link}</td>"
      end
      txt << "</tr></table></div>"
      return txt
   end

   # Builds a button bar from the contents of the @linksx array. @linksx
   # should be an array of 2-4 element arrays with name & url entries - no
   # permissions are checked, do that before populating @links. All we do here
   # is string the links together, separate them with bullets and wrap it in a
   # <div>. Note, if option 3rd array element is true, link will include
   # standard delete confirmation and method using js_verify and the 4th
   # element will be used to specify the request method (usually POST).
   # This is basically the same as link_bar but will look slightly
   # different and operates on a separate array, so both can be used on the
   # same view.
   def link_bar_extra
      # same as standard link bar but used for additional links on some pages
      return "" if @linksx.nil?
      return "" unless @linksx.is_a?(Array)
      return "" if @linksx.empty?
      # convert array of hashes to array of strings
      link_list = []
      @linksx.each do |l|
         next unless l.is_a?(Array)
         options = HashWithIndifferentAccess.new(:method => 'get', :disabled => false)
         options.update(l.pop) if l.last.is_a?(Hash)
         if options[:method] == 'get'
            # use button_to_function
            link_list.push(button_to_function(l[0], :disabled => options[:disabled]){|page| page.redirect_to(l[1])})
         else
            # use button_to so that method='post' and method='put' become available
            # TODO: figure out a way to make this work with GET and still pass url parameters!
            link_list.push(button_to(l[0], l[1], options.to_options))
         end
      end
      txt = "<div class='linkbarx'><table class='links'><tr>"
      btncntr = 0
      link_list.each do |link|
        btncntr += 1
        if btncntr > 7
          txt << "</tr><tr>"
          btncntr = 1
        end
        txt << "<td>#{link}</td>"
      end
      txt << "</tr></table></div>"
      return txt
   end
   
   def page_links
      return "" unless @page_links.nil? || @page_links.is_a?(Array)
      # convert array of hashes to array of strings
      link_list = []
      unless @page_links.nil? || @page_links.empty?
         @page_links.each do |l|
            if l.is_a?(Array)
               case l[0].class.name
               when 'Fixnum'
                  obj = Contact.find_by_id(l[0])
               when 'User'
                  obj = l[0].contact
               else
                  obj = l[0]
               end
               if l[1].blank?
                  txt = obj.class.name
               else
                  txt = l[1].titleize.gsub(/\s+/,'')
               end
            else
               case l.class.name
               when 'Fixnum'
                  obj = Contact.find_by_id(obj)
               when 'User'
                  obj = l.contact
               else
                  obj = l
               end
               txt = obj.class.name
            end
            next if obj.nil?
            if obj.class.name.constantize.respond_to?("base_perms")
               if has_permission(obj.class.name.constantize.base_perms)
                  permission = true
               else
                  permission = false
               end
            else
               permission = true
            end
            unless check_privacy(obj)
               permission = false
            end
            if permission
               link_list.push(button_to_function("#{txt}: #{obj.id}"){|page| page.redirect_to(url_for(obj))})
            end
         end
      end
      return "<div class='page-links'><span style='margin-left:-7.5em;float:left;'>Related Pages:</span>" + (link_list.empty? ? "<span style='color:#{@colors.color_gray_light};'><i>None</i></span>" : link_list.join) + "</div>"
   end
      
   # adds a Help link to a page which causes the associated help page to be
   # opened in a new window or tab depending on browser settings. Uses the
   # help_link CSS class to ensure that the help link always appears at the
   # top right of the current page.
   def help_link(page_id)
      return "" unless page_id.is_a?(Array) && page_id[0].is_a?(String) && page_id[1].is_a?(String)
      if help_page = Help.find(:first, :conditions => {:section => page_id[0], :page => page_id[1]})
         link_to "Instructions", {:controller => 'helps', :action => 'popup', :id => help_page.id}, {:class => 'help_link', :popup => popup_options(help_page.title, 500, 500)}
      else
         ""
      end
   end

   # Looks up the supplied case number.  If valid and user can view cases,
   # returns the case number linked to the case record.  Otherwise, returns
   # the case number without a link.
   def case_number_or_link(caseno)
      return nil unless caseno.is_a?(String)
      return "" if caseno.blank?
      if has_permission(Perm[:view_case])
         return button_to_function(caseno, :style => 'width:100px;'){|page| page.redirect_to(case_path(:case => caseno))}
      else
         return caseno
      end
   end
   
   # Creates a button used to add additional property lines to the booking
   # edit view.  Uses the bookings/_property partial.
   def add_property_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :properties, :partial => 'property', :object => Property.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   def add_watcher_link(name)
      button_to_function name do |page|
         page.insert_html :bottom, :watchers, :partial => 'watcher', :object => User.new()
      end
   end
   
   # creates a button used to add a new facility line to the commissary item
   # edit view.  Uses the commissary/facility partial.
   def add_facility_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :facilities, :partial => 'facility', :object => Option.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new charge lines to the current docket.
   def add_docket_charge_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :charges, :partial => 'docket_charge', :object => DaCharge.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end

   # creates a button to add a new docket line to the sentence
   def add_docket_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :dockets, :partial => 'consecutive_docket', :object => Docket.new()
      end
   end
   
   # creates a button to add a new docket line to the revocation
   def add_rev_docket_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :dockets, :partial => 'rev_consecutive_docket', :object => Docket.new()
      end
   end
   
   # creates a button used to add new charge lines to the current arrest.
   def add_district_charge_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :district_charges, :partial => 'district_charge', :object => Charge.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new warrant lines to the current arrest.
   def add_district_warrant_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :district_warrants, :partial => 'district_warrant', :object => Warrant.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new charge lines to the current arrest.
   def add_city_charge_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :city_charges, :partial => 'city_charge', :object => Charge.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new warrant lines to the current arrest.
   def add_city_warrant_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :city_warrants, :partial => 'city_warrant', :object => Warrant.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new warrant lines to the current arrest.
   def add_other_warrant_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :other_warrants, :partial => 'other_warrant', :object => Warrant.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new charge lines to the current warrant.
   def add_warrant_charge_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :charges, :partial => 'warrant_charge', :object => Charge.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new charge lines to the current arrest.
   def add_citation_charge_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :charges, :partial => 'citation_charge', :object => Charge.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add a contact to the investigation.
   def add_contact_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :contacts, :partial => 'contact', :object => Contact.new()
      end
   end
   
   # creates a button used to add new distinguishing mark lines to the person
   # edit view.  Uses the people/_mark partial.
   def add_mark_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :marks, :partial => 'mark', :object => Mark.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new known associates lines to the person
   # edit view.  Uses the people/_associate partial.
   def add_associate_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :associates, :partial => 'associate', :object => Associate.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   def add_schedule_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :schedules, :partial => 'schedule', :object => MedSchedule.new()
      end
   end
   
   # creates a button used to add new phone number lines to the contact edit
   # view.  Uses the contacts/_phone partial.
   def add_phone_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :phones, :partial => 'contacts/phone', :object => Phone.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add a new item to the pawn edit view.  Uses the
   # pawns/_pitem partial.
   def add_pawn_item_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :pitems, :partial => 'pitem', :object => PawnItem.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new phone number lines to the bondsman edit
   # view.  Uses the bondsmen/_phone partial.
   def add_bondsman_phone_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :bondsman_phones, :partial => 'phone', :object => BondsmanPhone.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new call unit lines to the call edit view.
   # Uses the calls/_unit partial.
   def add_unit_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :units, :partial => 'unit', :object => CallUnit.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new call subject lines to the call edit view.
   # Uses the calls/_subject partial.
   def add_subject_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :subjects, :partial => 'subject', :object => CallSubject.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new location lines to the evidence edit
   # view.  Uses the evidences/_location partial.
   def add_location_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :locations, :partial => 'location', :object => EvidenceLocation.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add new group lines to the user edit view.
   # Uses the users/_group partial.
   def add_group_link(name)
      return nil unless name.is_a?(String)
      button_to_function name, :style => 'width:100px;' do |page|
         page.insert_html :bottom, :groups, :partial => 'group', :object => Group.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add a new role line to the user or group edit
   # views.  Uses the users/_role or group/_role partial.
   def add_role_link(name)
      return nil unless name.is_a?(String)
      button_to_function name, :style => 'width:100px;' do |page|
         page.insert_html :bottom, :roles, :partial => 'role', :object => Role.new(:created_by => session[:user], :updated_by => session[:user])
      end
   end
   
   # creates a button used to add a new permission line to the user, group or
   # role edit views.  Uses the users/_perm, groups/_perm or roles/_perm
   # partial.
   def add_perm_link(name)
      return nil unless name.is_a?(String)
      button_to_function name, :style => 'width:100px;' do |page|
         page.insert_html :bottom, :perms, :partial => 'perm', :object => Perm.new
      end
   end
   
   # creates a button used to add a new person link to an edit view
   def add_person_link(partial_name,model_name)
      return nil unless partial_name.is_a?(String) && model_name.is_a?(String)
      title = "Add a #{partial_name.humanize.titleize}"
      button_to_function title do |page|
         page.insert_html :bottom, partial_name.pluralize, :partial => partial_name, :object => Person.new, :locals => {:model => model_name}
      end
   end
   
   # creates a button used to add a new prisoner line to the absent edit
   # view.  Uses the absents/_prisoner partial.
   def add_prisoner_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :prisoners, :partial => 'prisoner', :object => Booking.new
      end
   end
   
   def add_wrecker_link(name)
      return nil unless name.is_a?(String)
      button_to_function name do |page|
         page.insert_html :bottom, :wreckers, :partial => 'wrecker', :object => Wrecker.new
      end
   end
   
   # displays contact information in a standardized way
   def show_contact(obj, field, *opts)
      # check for options
      options = HashWithIndifferentAccess.new(:name_only => false, :include_badge => true, :include_unit => true, :invalid => 'Unknown')
      options.update(opts.pop) if opts.last.is_a?(Hash)
      
      return options[:invalid] if obj.nil?
      # all models in fiveo that have any possibility of having a
      # contact association will respond to "desc" class method
      return options[:invalid] unless obj.class.name.constantize.respond_to?("desc")
      
      unit = fullname = badge = nil
      if obj.is_a?(Contact)
         unit = obj.unit
         fullname = obj.fullname
         badge = obj.badge_no
      else
         return options[:invalid] unless field.is_a?(Symbol)
         # retrieve the values known from the object
         if obj.respond_to?("#{field.id2name}_id")
            field_id = obj.send("#{field.id2name}_id")
         end
         if obj.respond_to?("#{field.id2name}")
            field_name = obj.send("#{field.id2name}")
         end
         if obj.respond_to?("#{field.id2name}_unit")
            field_unit = obj.send("#{field.id2name}_unit")
         end
         if obj.respond_to?("#{field.id2name}_badge")
            field_badge = obj.send("#{field.id2name}_badge")
         end
         if field_id.to_i > 0
            if c = Contact.find_by_id(field_id)
               unit = c.unit
               fullname = c.fullname
               badge = c.badge_no
            end
         elsif c = Contact.find(:first, :conditions => {:fullname => field_name, :unit => field_unit, :badge_no => field_badge})
            unit = c.unit
            fullname = c.fullname
            badge = c.badge_no
         else
            unit = field_unit
            fullname = field_name
            badge = field_badge
         end
      end
      if fullname.blank?
         return options[:invalid]
      elsif options[:name_only] == true
         return fullname
      else
         return "#{options[:include_unit] == true && !unit.blank? ? unit + ' ' : ''}#{fullname}#{options[:include_badge] == true && !badge.blank? ? ' (' + badge + ')' : ''}"
      end
   end
   
   # displays user information in a standardized way
   def show_user(obj, *opts)
      # check for options
      options = HashWithIndifferentAccess.new(:name_only => false, :include_badge => true, :include_unit => true, :invalid => 'Unknown')
      options.update(opts.pop) if opts.last.is_a?(Hash)
      
      return options[:invalid] if obj.nil? || !obj.is_a?(User)

      if obj.name.blank?
         return options[:invalid]
      elsif options[:name_only] == true
         return obj.name
      else
         return "#{options[:include_unit] == true && !obj.unit.blank? ? obj.unit + ' ' : ''}#{obj.name}#{options[:include_badge] == true && !obj.badge.blank? ? ' (' + obj.badge + ')' : ''}"
      end
   end
   
   #
   # def show_person(person_id, *opts)
   #
   # I use this in controllers as well as views, so it is defined in application_controller
   # and exported as a helper method.
   #
   
   # Looks up a contact record using the supplied agency name. If the contact
   # is valid and the person can view contact records, returns the agency
   # name linked to the contact record.  Otherwise returns the agency name
   # without a link.
   def agency_name_or_link(agency)
      return nil unless agency.is_a?(String) && !agency.strip.blank?
      if has_permission(Perm[:view_contact])
         if cont = Contact.find(:first, :conditions => ['fullname like ? AND business = 1', agency])
            return button_to_function(agency, :style => 'width:100px;'){|page| page.redirect_to(url_for(cont))}
         end
      end
      return agency
   end
   
   # Looks up a docket using the supplied docket_no and person_id. If the
   # docket is valid and the user can view docket records, it returns the
   # docket number linked to the docket record.  Otherwise, it returns the
   # docket number without a link.
   def docket_number_or_link(docket_no, person_id = nil)
      return nil unless docket_no.is_a?(String) && !docket_no.blank? && person_id.is_a?(Integer) && person_id > 0
      return "" if docket_no.blank? || person_id.nil?
      if has_permission(Perm[:view_court])
         if docket = Docket.find(:first, :conditions => {:docket_no => docket_no, :person_id => person_id})
            return link_to(docket_no, docket)
         end
      end
      return docket_no
   end
   
   # Looks up a warrant by id.  If valid, and user can view warrants, it
   # returns the warrant_no (not necessarily the same as id) linked to the
   # warrant record.  Otherwise it returns the warrant number without the
   # link.
   def warrant_number_or_link(warrant_id)
      return nil unless warrant_id.is_a?(Integer) && warrant_id > 0
      if war = Warrant.find(warrant_id)
         number = war.warrant_no
         if has_permission(Perm[:view_warrant])
            return link_to(number, war)
         else
            return number
         end
      end
      return nil
   end
   
   # Looks up a booking record by id.  If valid, it determines the inmate
   # current location by first checking absent records for an active one. If
   # found it returns the absent name linked to the absent record. If no
   # valid absent, and the booking is current (not released) it returns the
   # facility/cell from the booking record (without a link). If the booking
   # has been closed, will instead use "Released" as the location.
   def booking_location_or_link(booking_id, *opts)
      return nil unless booking_id.is_a?(Integer) && booking_id > 0
      options = HashWithIndifferentAccess.new(:supervisor => true, :html => true)
      options.update(opts.pop) if opts.last.is_a?(Hash)
      if booking = Booking.find_by_id(booking_id)
         if booking.release_date?
            # if booking was released by transfer, show transfer agency, otherwise, just released.
            if !booking.transfers.empty? && booking.transfers.last.transfer_date == booking.release_date && booking.transfers.last.transfer_time == booking.release_time
               return "Transferred: #{booking.transfers.last.to_agency}"
            else
               return "Released"
            end
         elsif booking.temporary_release?
            txt = booking.temporary_release_reason
            return "Temporary Release#{txt.blank? ? '' : ': ' + txt}"
         elsif agency = booking.is_transferred
            return "Other: #{agency}"
         elsif booking.absents.empty? || booking.absents.last.return_date?
            txt = ""
            txt << (booking.facility.nil? ? "Invalid Facility!" : booking.facility.short_name)
            txt << " - "
            txt << (booking.cell.nil? ? "Invalid Cell!" : booking.cell.long_name)
            return txt
         else
            absent = booking.absents.last
            link = ""
            link << (options[:html] ? "<a href='" + absent_path(:id => absent.id) + "'>" : "")
            link << "Absent: "
            link << (absent.leave_type.nil? ? 'Unknown Leave Type' : absent.leave_type.long_name)
            link << (options[:html] ? '</a>' : '')
            if options[:supervisor]
               link << (options[:html] ? space(4) : '  ')
               link << "Supervisor: "
               link << show_contact(absent, :escort_officer)
            end
            if absent.leave_date?
               return link
            else
               txt = ""
               txt << (options[:html] ? "<span class='message'>" : "")
               txt << "Pending:"
               txt << (options[:html] ? "</span>" : "")
               txt << " "
               txt << link
               return txt
            end
         end
      end
      return nil
   end
   
   # This method generates a bar (generally used at the bottom of show views)
   # that displays the user that created the record (including date and time)
   # and the last user to update the record (including date and time).
   def created_by(object)
      return "" if object.nil?
      cname = uname = cdate = udate = ""
      if object.respond_to?('creator') && !object.creator.nil?
         cname = h(object.creator.name)
      else
         cname = "Unknown"
      end
      if object.respond_to?('updater') && !object.updater.nil?
         uname = h(object.updater.name)
      else
         uname = "Unknown"
      end
      if object.respond_to?('created_at') && !object.created_at.nil?
         cdate = format_date(object.created_at)
      else
         cdate = "Unknown"
      end
      if object.respond_to?('updated_at') && !object.updated_at.nil?
         udate = format_date(object.updated_at)
      else
         udate = "Unknown"
      end
      %(<div class='created_by'>Created #{cdate} by #{cname} --- last updated #{udate} by #{uname}.</div>)
   end
   
   # returns an HTML string from the photos for an investigation
   def photo_summary(object)
      return "" unless object.is_a?(Investigation) || object.is_a?(InvestCrime) || object.is_a?(InvestReport) || object.is_a?(InvestCriminal) || object.is_a?(InvestVehicle)
      return "" if object.private? && object.owned_by != session[:user]
      return "" unless has_permission(Perm[:view_investigation])
      return "" if object.photos.empty?
      object.photos.collect{|p| link_to(image_tag(p.invest_photo.url(:thumb)), p)}.join(' ')
   end
   
   # returns a string of docket numbers linked to docket records if user can view dockets
   # otherwise just returns the numbers
   def docket_links(docket_array)
      return "" unless docket_array.is_a?(Array)
      if has_permission(Perm[:view_court])
         docket_array.collect{|d| link_to d.docket_no, d }.join(", ")
      else
         docket_array.collect{|d| d.docket_no}.join(", ")
      end
   end
end